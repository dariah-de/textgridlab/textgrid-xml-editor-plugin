package info.textgrid.lab.xmleditor.utils;

import info.textgrid._import.RewriteMethod;
import info.textgrid.lab.core.importexport.model.AbstractImportEntryConfigurator;
import info.textgrid.lab.core.importexport.model.IExportEntryConfigurator;
import info.textgrid.lab.core.importexport.model.IImportEntryConfigurator;
import info.textgrid.lab.core.importexport.model.ImportEntry;
import info.textgrid.lab.core.importexport.model.RewriteSetup;
import info.textgrid.lab.xmleditor.mpeditor.Activator;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.ui.statushandlers.StatusManager;

public class SchemaImportConfigurator extends AbstractImportEntryConfigurator implements IImportEntryConfigurator,
		IExportEntryConfigurator {

	private boolean isSchema(ImportEntry entry) throws CoreException {
		return entry.getObject().getContentType(false).getId().equals("text/xsd+xml"); //$NON-NLS-1$
	}

	public void configureImport(ImportEntry entry, IProgressMonitor monitor) {
		try {
			if (isSchema(entry)) {
				entry.setRewriteSetup(RewriteSetup.of(RewriteMethod.XML, "internal:schema#xsd")); //$NON-NLS-1$
			}
		} catch (CoreException e) {
			StatusManager.getManager().handle(e, Activator.PLUGIN_ID);
		}
	}

	public void configureExport(ImportEntry entry, IProgressMonitor monitor) {
		try {
			if (isSchema(entry)) {
				entry.setRewriteSetup(RewriteSetup.of(RewriteMethod.XML, "internal:schema#xsd")); //$NON-NLS-1$
			}
		} catch (CoreException e) {
			StatusManager.getManager().handle(e, Activator.PLUGIN_ID);
		}
	}

}
