package info.textgrid.lab.xmleditor.dialogs;

import info.textgrid.lab.core.model.CrudServiceException;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.swtutils.DeferredListContentProvider.IDoneListener;
import info.textgrid.lab.core.swtutils.ExclusiveSelection;
import info.textgrid.lab.ui.core.utils.TextGridObjectTableViewer;
import info.textgrid.lab.ui.core.utils.TextGridObjectTableViewer.Column;
import info.textgrid.lab.xmleditor.mpeditor.Activator;

import java.net.URI;
import java.text.MessageFormat;
import java.util.EnumSet;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.statushandlers.StatusManager;

/**
 * A dialog to associate an adapter with a bunch of objects. Create it using the
 * {@linkplain #SetAdaptorDialog(Shell, boolean, TextGridObject...) constructor}
 * , display it using {@link #open()} and this class will take care of the rest.
 */
public class SetAdaptorDialog extends TitleAreaDialog {

	private TextGridObjectTableViewer adaptorViewer;
	private TextGridObject[] textGridObjects;
	private boolean forceMakePersistant = false;
	private Button makePersistentButton;

	/**
	 * Creates a new adaptor selection dialogue.
	 * 
	 * @param parentShell
	 *            The parent shell the dialog window will be transient to
	 * @param forceMakePersistant
	 *            The dialog contains a checkbox labelled 'make metadata
	 *            persistant': If checked, the objects' metadata (including the
	 *            adaptor association) will be
	 *            {@linkplain TextGridObject#makeMetadataPersistent(IProgressMonitor)
	 *            saved to the repository} on OK, otherwise the metadata must be
	 *            stored separately (e.g., together with the data in the
	 *            editor). If <var>forceMakePersistant</var> is
	 *            <code>true</code>, this checkbox cannot be unchecked.
	 * @param textGridObjects
	 *            The {@link TextGridObject}(s) that shall be associated with
	 *            the selected adaptor.
	 * 
	 */
	public SetAdaptorDialog(Shell parentShell,
			boolean forceMakePersistant, TextGridObject... textGridObjects) {
		super(parentShell);
		this.forceMakePersistant = forceMakePersistant;
		this.textGridObjects = textGridObjects;
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite dialogArea = new Composite(parent, SWT.NONE);
		GridLayoutFactory.fillDefaults().applyTo(dialogArea);
		GridDataFactory.fillDefaults().grab(true, true).hint(SWT.DEFAULT, 400).applyTo(dialogArea);
		
		PlatformUI.getWorkbench().getHelpSystem().setHelp(dialogArea, "info.textgrid.lab.xmleditor.mpeditor.SetAdaptorDialog"); //$NON-NLS-1$
		PlatformUI.getWorkbench().getHelpSystem().setHelp(parent, "info.textgrid.lab.xmleditor.mpeditor.SetAdaptorDialog"); //$NON-NLS-1$

		createTargetObjectsArea(dialogArea);
		createMakePersistentCheckbox(dialogArea);
		createAdaptorSelectionArea(dialogArea);

		
		setTitle(Messages.SetAdaptorDialog_DlgTitle);
		setMessage(Messages.SetAdaptorDialog_DlgMessage);
		if (getShell() != null)
			getShell().setText(Messages.SetAdaptorDialog_ShellTitle);

		
		return dialogArea;
	}

	private void createAdaptorSelectionArea(Composite dialogArea) {
		Label adaptorInfo = new Label(dialogArea, SWT.WRAP);
		adaptorInfo
				.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		adaptorInfo
				.setText(Messages.SetAdaptorDialog_AdapterInfo);
		adaptorViewer = new TextGridObjectTableViewer(dialogArea, SWT.SINGLE
				| SWT.FULL_SELECTION);
		
		adaptorViewer.setVisibleColumns(EnumSet.of(Column.TITLE, Column.PROJECT, Column.DATE, Column.OWNER));
		
		final Button noAdaptor = new Button(dialogArea, SWT.RADIO);
		noAdaptor.setLayoutData(new GridData(SWT.BEGINNING, SWT.CENTER, false, false));
		noAdaptor.setText(Messages.SetAdaptorDialog_6);
		ExclusiveSelection.on(noAdaptor, adaptorViewer.getTable());

		// Select the current adapter (if any)
		adaptorViewer.getDefaultContentProvider().addDoneListener(new IDoneListener() {

			public void loadDone(Viewer viewer) {
				if (textGridObjects.length >= 1 && textGridObjects[0] != null) {
					try {
						URI currentAdaptor = textGridObjects[0].getAdaptor();
						if (currentAdaptor == null) {
							noAdaptor.setSelection(true);
						} else {
							adaptorViewer.setSelection(new StructuredSelection(TextGridObject.getInstance(currentAdaptor, false)),
									true);
						}
					} catch (CrudServiceException e) {
						StatusManager.getManager().handle(e, Activator.PLUGIN_ID);
					}
				}
			}
		});

		adaptorViewer.queryMetadata(Messages.SetAdaptorDialog_7);
		GridDataFactory.fillDefaults().grab(true, true).applyTo(
				adaptorViewer.getControl());
		

	}

	private void createMakePersistentCheckbox(Composite dialogArea) {
		makePersistentButton = new Button(dialogArea, SWT.CHECK);
		makePersistentButton.setLayoutData(new GridData(SWT.FILL, SWT.FILL,
				true,
				false));
		makePersistentButton.setText(Messages.SetAdaptorDialog_8);
		makePersistentButton
				.setToolTipText(Messages.SetAdaptorDialog_9
						+ Messages.SetAdaptorDialog_10
						+ Messages.SetAdaptorDialog_11
						+ Messages.SetAdaptorDialog_12);
		if (forceMakePersistant) {
			makePersistentButton.setSelection(true);
			makePersistentButton.setEnabled(false);

		}
	}

	private void createTargetObjectsArea(Composite dialogArea) {
		final Label targetInfo = new Label(dialogArea, SWT.WRAP);
		targetInfo.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		targetInfo.setText(MessageFormat.format(
				Messages.SetAdaptorDialog_13, textGridObjects.length));
		
		final TextGridObjectTableViewer targetObjects = new TextGridObjectTableViewer(
				dialogArea, SWT.HIDE_SELECTION);
		targetObjects.setVisibleColumns(EnumSet.of(Column.TITLE, Column.PROJECT, Column.DATE, Column.OWNER));
		GridDataFactory.fillDefaults().grab(true, false).applyTo(
				targetObjects.getControl());
		targetObjects.setContentProvider(new ArrayContentProvider());
		targetObjects.setInput(textGridObjects);
		targetObjects.getTable().setEnabled(false);
		
	}

	@Override
	protected void okPressed() {
		final boolean makePersistant = makePersistentButton.getSelection();
		ISelection selection = adaptorViewer.getSelection();
		final TextGridObject adaptor = 
			selection.isEmpty() ? null :
			(TextGridObject) ((IStructuredSelection) selection)
					.getFirstElement();


		Job setAdaptors = new Job(Messages.SetAdaptorDialog_14) {

			@Override
			protected IStatus run(IProgressMonitor monitor) {
				SubMonitor progress = SubMonitor.convert(monitor, textGridObjects.length * 10);

				for (TextGridObject textGridObject : textGridObjects) {
					try {
						if (adaptor != null)
							textGridObject.setAdaptor(adaptor.getURI());
						else
							textGridObject.clearAdaptor();
						progress.worked(1);
						if (makePersistant) {
							textGridObject.makeMetadataPersistent(progress.newChild(9));
						} else {
							progress.worked(9);
						}
					} catch (CoreException e) {
						StatusManager.getManager().handle(e.getStatus(), StatusManager.LOG | StatusManager.SHOW);
					}
				}
				// TODO return the actual status
				progress.done();
				return Status.OK_STATUS;
			}
		};
		setAdaptors.setUser(true);
		setAdaptors.schedule();

		super.okPressed();
	}
}
