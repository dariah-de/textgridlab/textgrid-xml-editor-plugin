package info.textgrid.lab.xmleditor.dialogs;

import static info.textgrid.lab.core.swtutils.AdapterUtils.getAdapter;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.ui.core.dialogs.INewObjectPreparator;
import info.textgrid.lab.ui.core.dialogs.ITextGridWizard;
import info.textgrid.lab.ui.core.dialogs.ITextGridWizardPage;
import info.textgrid.lab.ui.core.utils.TextGridObjectTableViewer;
import info.textgrid.lab.ui.core.utils.TextGridObjectTableViewer.Column;
import info.textgrid.lab.xmleditor.mpeditor.Activator;

import java.util.EnumSet;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.wst.xml.core.internal.XMLCorePlugin;
import org.eclipse.wst.xml.core.internal.catalog.provisional.ICatalog;
import org.eclipse.wst.xml.core.internal.catalog.provisional.ICatalogEntry;
import org.eclipse.wst.xml.ui.internal.dialogs.SelectXMLCatalogIdPanel;

@SuppressWarnings("restriction")
public class SelectSchemaPage extends WizardPage implements
		ITextGridWizardPage, SelectionListener, ISelectionChangedListener {
	
	private SelectXMLCatalogIdPanel catalogIdPanel;
	private TextGridObjectTableViewer schemaTable;
	private NewXMLObjectPreparator preparator;
	private ICatalogEntry catalogEntry;
	private TextGridObject schemaObject;
	private Button noSchema;
	public SelectSchemaPage() {
		super(Messages.SelectSchemaPage_PageTitle, Messages.SelectSchemaPage_PageMessage, null);
	}

	

	public void finishPage() {
		preparator.setXMLCatalogEntry(catalogEntry);
		if (catalogEntry != null) {
			preparator.setGrammarURI(catalogEntry.getURI());
		}
		if (schemaObject != null) {
			preparator.setXMLCatalogEntry(null);
			preparator.setGrammarObject(schemaObject); 
		}
		if (catalogEntry == null && schemaObject == null) {
			preparator.setGrammarURI(null);
			preparator.setXMLCatalogEntry(null);
		}
	}

	public void init(ITextGridWizard wizard, INewObjectPreparator preparator) {
		if (preparator instanceof NewXMLObjectPreparator)
			this.preparator = (NewXMLObjectPreparator) preparator;
		else
			Activator
					.handleProblem(
							IStatus.WARNING,
							null,
							"Select schema page initialized from wizard {0} with a strange preparator: {1} (which is a {2}) ", //$NON-NLS-1$
							wizard, preparator, preparator != null ? preparator
									.getClass() : null);
		
	}

	public void loadObject(TextGridObject textGridObject) {
		// TODO Auto-generated method stub

	}

	public void createControl(Composite parent) {
		Composite control = new Composite(parent, SWT.NONE);
		GridDataFactory fillGrabData = GridDataFactory.fillDefaults();
		fillGrabData.grab(true, true);
		GridLayoutFactory.fillDefaults().applyTo(control);
		fillGrabData.applyTo(control);
		setControl(control);

		noSchema = new Button(control, SWT.RADIO);
		noSchema.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false));
		noSchema.setText(Messages.SelectSchemaPage_DoNotAssociateSchema);
		noSchema.setToolTipText(Messages.SelectSchemaPage_CreateEmptyXML);
		noSchema.addSelectionListener(this);
		noSchema.setSelection(true);
		
		Label repHeader = new Label(control, SWT.LEFT);
		repHeader.setText(Messages.SelectSchemaPage_SchemasFromRep);
		
		schemaTable = new TextGridObjectTableViewer(control, SWT.SINGLE | SWT.FULL_SELECTION | SWT.VIRTUAL);
		schemaTable.setVisibleColumns(EnumSet.of(Column.TITLE, Column.PROJECT, Column.AUTHORS));
		fillGrabData.applyTo(schemaTable.getControl());
		schemaTable.getTable().addSelectionListener(this);
		schemaTable.addSelectionChangedListener(this);
		schemaTable.queryMetadata("format:\"text/xsd+xml\""); //$NON-NLS-1$
		
		Label repInfo = new Label(control, SWT.WRAP);
		repInfo.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		repInfo.setText(Messages.SelectSchemaPage_ImportSchemaNote);
		Label separator = new Label(control, SWT.SEPARATOR | SWT.HORIZONTAL);
		separator
				.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		

		Label catInfo = new Label(control, SWT.WRAP);
		catInfo.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		catInfo.setText(Messages.SelectSchemaPage_BuiltinSchemaNote);
		
		
		ICatalog xmlCatalog = XMLCorePlugin.getDefault().getDefaultXMLCatalog();
		catalogIdPanel = new SelectXMLCatalogIdPanel(control, xmlCatalog);

		catalogIdPanel.getTableViewer().getTable().addSelectionListener(this);
		catalogIdPanel.getTableViewer().addSelectionChangedListener(this);

		setMessage(Messages.SelectSchemaPage_SchemaExplanation);
		
		validatePageComplete();

	}



	public void widgetDefaultSelected(SelectionEvent e) {
		// TODO Auto-generated method stub

	}

	public void widgetSelected(SelectionEvent e) {
		if (e.widget == catalogIdPanel.getTableViewer().getTable()) {
			schemaTable.getTable().deselectAll();
			noSchema.setSelection(false);
		} else if (e.widget == schemaTable.getTable()) {
			catalogIdPanel.getTableViewer().getTable().deselectAll();
			noSchema.setSelection(false);
		} else if (e.widget == noSchema && noSchema.getSelection()) {
			schemaTable.getTable().deselectAll();
			catalogIdPanel.getTableViewer().getTable().deselectAll();
			schemaObject = null;
			catalogEntry = null;
			validatePageComplete();
		}
	}

	public void selectionChanged(SelectionChangedEvent event) {
		ISelection selection = event.getSelection();
		if (selection instanceof IStructuredSelection) {
			IStructuredSelection sel = (IStructuredSelection) selection;
			Object object = sel.getFirstElement();
			if (object instanceof ICatalogEntry) {
				catalogEntry = (ICatalogEntry) object;
				schemaObject = null;
			}
			else {
				TextGridObject tgo = getAdapter(object, TextGridObject.class);
				if (tgo != null) {
					schemaObject = tgo;
					catalogEntry = null;
				}
			}
		}
		validatePageComplete();
	}



	private void validatePageComplete() {
		setPageComplete(schemaObject != null || catalogEntry != null || noSchema.getSelection());
	}



}
