package info.textgrid.lab.xmleditor.dialogs;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "info.textgrid.lab.xmleditor.dialogs.messages"; //$NON-NLS-1$
	public static String AssociateStylesheetDialog_DefaultStylesheetBtn;
	public static String AssociateStylesheetDialog_DialogTitle;
	public static String AssociateStylesheetDialog_ErrorTextGridURIsOnly;
	public static String AssociateStylesheetDialog_InvalidXMLStylesheet;
	public static String AssociateStylesheetDialog_SelectStylesheetMessage;
	public static String AssociateStylesheetDialog_UpdatingWYSIWYM;
	public static String NewXMLObjectPreparator_ErrorLocalSchemaCopy;
	public static String NewXMLObjectPreparator_FetchingSchema;
	public static String SelectSchemaPage_BuiltinSchemaNote;
	public static String SelectSchemaPage_CreateEmptyXML;
	public static String SelectSchemaPage_DoNotAssociateSchema;
	public static String SelectSchemaPage_ImportSchemaNote;
	public static String SelectSchemaPage_PageMessage;
	public static String SelectSchemaPage_PageTitle;
	public static String SelectSchemaPage_SchemaExplanation;
	public static String SelectSchemaPage_SchemasFromRep;
	public static String SetAdaptorDialog_10;
	public static String SetAdaptorDialog_11;
	public static String SetAdaptorDialog_12;
	public static String SetAdaptorDialog_13;
	public static String SetAdaptorDialog_14;
	public static String SetAdaptorDialog_6;
	public static String SetAdaptorDialog_7;
	public static String SetAdaptorDialog_8;
	public static String SetAdaptorDialog_9;
	public static String SetAdaptorDialog_AdapterInfo;
	public static String SetAdaptorDialog_DlgMessage;
	public static String SetAdaptorDialog_DlgTitle;
	public static String SetAdaptorDialog_ShellTitle;
	public static String SetSchemaDialog_CouldNotDetermineSchema;
	public static String SetSchemaDialog_CouldNotFetchTitle;
	public static String SetSchemaDialog_CurentSchemaMessage;
	public static String SetSchemaDialog_NoInfoMessage;
	public static String SetSchemaDialog_NoSchemaButton;
	public static String SetSchemaDialog_NoSchemaMessage;
	public static String SetSchemaDialog_NoSchemaTooltip;
	public static String SetSchemaDialog_Problems;
	public static String SetSchemaDialog_SaveWarning;
	public static String SetSchemaDialog_SchemaFailedLastTime;
	public static String SetSchemaDialog_SelectSchemaDlgMessage;
	public static String SetSchemaDialog_SelectSchemaDlgTitle;
	public static String SetSchemaDialog_SelectSchemaShellTitle;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
