/**
 * 
 */
package info.textgrid.lab.xmleditor.dialogs;

import info.textgrid.lab.core.model.Activator;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.ui.core.dialogs.INewObjectPreparator;
import info.textgrid.lab.ui.core.dialogs.ITextGridWizard;
import info.textgrid.lab.ui.core.dialogs.NewObjectWizard;

import java.io.File;
import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.osgi.util.NLS;
import org.eclipse.wst.xml.core.internal.catalog.provisional.ICatalogEntry;

/**
 * This is used to instrument the New Object wizard.
 */
@SuppressWarnings("restriction")
public class NewXMLObjectPreparator implements INewObjectPreparator {

	private ITextGridWizard wizard;
	private String grammarURI;
	private ICatalogEntry xmlCatalogEntry;
	private TextGridObject grammarObject;

	/* (non-Javadoc)
	 * @see info.textgrid.lab.ui.core.dialogs.INewObjectPreparator#initializeObject(info.textgrid.lab.core.model.TextGridObject)
	 */
	public void initializeObject(TextGridObject textGridObject) {
		// currently we do not prepare the object in any way
	}

	/* (non-Javadoc)
	 * @see info.textgrid.lab.ui.core.dialogs.INewObjectPreparator#performFinish(info.textgrid.lab.core.model.TextGridObject)
	 */
	public boolean performFinish(TextGridObject textGridObject) {
		
		// TODO replace this with a real implementation
		
		if (wizard instanceof NewObjectWizard)
			return ((NewObjectWizard) wizard).defaultPerformFinish();
		else
			return false;
	}

	/* (non-Javadoc)
	 * @see info.textgrid.lab.ui.core.dialogs.INewObjectPreparator#setWizard(info.textgrid.lab.ui.core.dialogs.ITextGridWizard)
	 */
	public void setWizard(ITextGridWizard wizard) {
		this.wizard = wizard;
	}

	public String getGrammarURI() {
		return grammarURI;
	}

	public void setGrammarURI(String uri) {
		this.grammarURI = uri;
	}

	public ICatalogEntry getXMLCatalogEntry() {
		return xmlCatalogEntry;
	}

	public void setXMLCatalogEntry(ICatalogEntry xmlCatalogEntry) {
		this.xmlCatalogEntry = xmlCatalogEntry;
		if (xmlCatalogEntry != null)
			grammarObject = null;
	}

	public void setGrammarObject(TextGridObject schemaObject) {
		this.grammarObject = schemaObject;
		try {
			wizard.getContainer().run(false, true, new IRunnableWithProgress() {

				public void run(IProgressMonitor monitor)
						throws InvocationTargetException, InterruptedException {
					
					SubMonitor progress = SubMonitor.convert(monitor,
							NLS.bind(
							Messages.NewXMLObjectPreparator_FetchingSchema, grammarObject), 100);

					File localFile;
					try {
						localFile = grammarObject.toLocalFile(progress
								.newChild(100,
										SubMonitor.SUPPRESS_SETTASKNAME), true);
						if (localFile != null) {
							setGrammarURI(localFile.toURI().toString());
						}
					} catch (CoreException e) {
						throw new InvocationTargetException(e);
					}

				}

			});
		} catch (InvocationTargetException e) {
			Activator.handleError(e.getCause(),
					Messages.NewXMLObjectPreparator_ErrorLocalSchemaCopy,
					schemaObject);
		} catch (InterruptedException e) {
			Activator.handleError(e,
					Messages.NewXMLObjectPreparator_ErrorLocalSchemaCopy,
					schemaObject);
		}
	}
	
	public TextGridObject getGrammarObject() {
		return grammarObject;
	}

}
