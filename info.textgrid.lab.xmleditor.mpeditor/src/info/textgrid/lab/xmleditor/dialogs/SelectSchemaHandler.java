/**
 * 
 */
package info.textgrid.lab.xmleditor.dialogs;

import static info.textgrid.lab.core.swtutils.AdapterUtils.getAdapter;
import info.textgrid.lab.core.model.TextGridObject;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.handlers.HandlerUtil;

public class SelectSchemaHandler extends AbstractHandler implements IHandler {
	
	public Object execute(ExecutionEvent event) throws ExecutionException {
		TextGridObject object = null;
		ISelection currentSelection = HandlerUtil.getCurrentSelection(event);
		if (currentSelection instanceof IStructuredSelection) {
			IStructuredSelection selection = (IStructuredSelection) currentSelection;
			object = getAdapter(selection
					.getFirstElement(), TextGridObject.class);
		}
		
		IEditorPart editor = HandlerUtil.getActiveEditor(event);
		if (object == null && editor != null) {
			IEditorInput input = editor.getEditorInput();
			object = getAdapter(input, TextGridObject.class);
		}

		if (object != null) {
			SetSchemaDialog dialog = new SetSchemaDialog(HandlerUtil
					.getActiveShell(event), object, editor);
			dialog.open();
		}
			

		return null;
	}

}
