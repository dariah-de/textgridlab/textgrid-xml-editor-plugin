package info.textgrid.lab.xmleditor.dialogs;

import org.eclipse.osgi.util.NLS;

public class XMLWizardsMessages extends NLS {
	private static final String BUNDLE_NAME = "info.textgrid.lab.xmleditor.dialogs.messages"; //$NON-NLS-1$
	public static String SelectRootElementPage_ErrorBuildingGrammar;
	public static String SelectRootElementPage_ErrorSerializingSkeleton;
	public static String SelectRootElementPage_LoadingSchema;
	public static String SelectRootElementPage_NoSchemaSelected;
	public static String SelectRootElementPage_SelectDifferentSchema;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, XMLWizardsMessages.class);
	}

	private XMLWizardsMessages() {
	}
}
