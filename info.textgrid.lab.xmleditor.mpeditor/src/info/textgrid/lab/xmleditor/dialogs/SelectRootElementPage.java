/**
 * This code is blatantly stolen from WST's {@link org.eclipse.wst.xml.ui.internal.wizards.NewXMLWizard}.
 * This is all but an elegant solution, but well, there are those restrictions ...
 */
package info.textgrid.lab.xmleditor.dialogs;

import static info.textgrid.lab.core.swtutils.AdapterUtils.getAdapter;
import info.textgrid.lab.core.model.Activator;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.ui.core.dialogs.INewObjectPreparator;
import info.textgrid.lab.ui.core.dialogs.ITextGridWizard;
import info.textgrid.lab.ui.core.dialogs.ITextGridWizardPage;

import java.io.ByteArrayOutputStream;
import java.text.Collator;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;

import javax.xml.XMLConstants;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.part.PageBook;
import org.eclipse.wst.common.uriresolver.internal.util.URIHelper;
import org.eclipse.wst.xml.core.internal.catalog.provisional.ICatalogEntry;
import org.eclipse.wst.xml.core.internal.contentmodel.CMDocument;
import org.eclipse.wst.xml.core.internal.contentmodel.CMElementDeclaration;
import org.eclipse.wst.xml.core.internal.contentmodel.CMNamedNodeMap;
import org.eclipse.wst.xml.core.internal.contentmodel.util.DOMContentBuilder;
import org.eclipse.wst.xml.core.internal.contentmodel.util.NamespaceInfo;
import org.eclipse.wst.xml.ui.internal.dialogs.NamespaceInfoErrorHelper;
import org.eclipse.wst.xml.ui.internal.nsedit.CommonEditNamespacesDialog;
import org.eclipse.wst.xml.ui.internal.wizards.NewXMLGenerator;
import org.eclipse.wst.xml.ui.internal.wizards.XMLWizardsMessages;

/**
 * SelectRootElementPage
 */
@SuppressWarnings("restriction")
public class SelectRootElementPage extends WizardPage implements
		ITextGridWizardPage,
		SelectionListener {
	protected Combo combo;
	protected Button[] radioButton;
	protected PageBook pageBook;
	protected XSDOptionsPanel xsdOptionsPanel;
	protected DTDOptionsPanel dtdOptionsPanel;
	private NewXMLGenerator generator = new NewXMLGenerator();
	private NewXMLObjectPreparator preparator;
	private ITextGridWizard wizard;
	private String cmDocumentErrorMessage;

	public SelectRootElementPage() {
		super("SelectRootElementPage"); //$NON-NLS-1$
	}
	
	public static GridLayout createOptionsPanelLayout() {
		GridLayout gridLayout = new GridLayout();
		gridLayout.marginWidth = 0;
		gridLayout.horizontalSpacing = 0;
		return gridLayout;
	}

	class XSDOptionsPanel extends Composite {
		protected String errorMessage = null;
		protected SelectRootElementPage parentPage;
		protected CommonEditNamespacesDialog editNamespaces;

		public XSDOptionsPanel(SelectRootElementPage parentPage,
				Composite parent) {
			super(parent, SWT.NONE);
			this.parentPage = parentPage;

			setLayout(createOptionsPanelLayout());
			setLayoutData(new GridData(GridData.FILL_BOTH));

			Composite co = new Composite(this, SWT.NONE);
			co.setLayout(new GridLayout());

//			if ((newFilePage != null)
//					&& (newFilePage.getContainerFullPath() != null)) {
//				// todo... this is a nasty mess. I need to revist this code.
//				//
//				String resourceURI = "platform:/resource" + newFilePage.getContainerFullPath().toString() + "/dummy"; //$NON-NLS-1$ //$NON-NLS-2$
//				String resolvedPath = URIHelper.normalize(resourceURI, null,
//						null);
//				if (resolvedPath.startsWith("file:/")) //$NON-NLS-1$
//				{
//					resolvedPath = resolvedPath.substring(6);
//				}
//				// end nasty messs
			
			TextGridObject object = wizard.getTextGridObject();
			if (object != null) {
				IFile file = (IFile) object.getAdapter(IFile.class);
				if (file != null) {
					IPath resolvedPath = file.getFullPath();
			
					String tableTitle = XMLWizardsMessages._UI_LABEL_NAMESPACE_INFORMATION;
					editNamespaces = new CommonEditNamespacesDialog(co,
							resolvedPath, tableTitle, true, true);
				}
			}
		}

		public void setNamespaceInfoList(List<NamespaceInfo> list) {
			editNamespaces.setNamespaceInfoList(fixNamespaceInfoList(list));
			editNamespaces.updateErrorMessage(list);
		}

		// XXX Work around TG-260 (which triggered TG-256). Remove this when the
		// underlying problem has been resolved, which probably is either in the
		// generator itself or in the resolver code.
		private List<NamespaceInfo> fixNamespaceInfoList(
				List<NamespaceInfo> list) {
			for (NamespaceInfo info : list) {
				if (info != null && info.uri != null && info.uri.equals(info.locationHint))
					info.locationHint = null;
			}

			return list;
		}

		public void updateErrorMessage(List<?> namespaceInfoList) {
			NamespaceInfoErrorHelper helper = new NamespaceInfoErrorHelper();
			errorMessage = helper.computeErrorMessage(namespaceInfoList, null);
			parentPage.updateErrorMessage();
		}

		public String computeErrorMessage() {
			return errorMessage;
		}
	}

	/**
	 * 
	 */
	public class DTDOptionsPanel extends Composite implements ModifyListener {
		protected Group group;
		protected Text systemIdField;
		protected Text publicIdField;
		protected SelectRootElementPage parentPage;

		public DTDOptionsPanel(SelectRootElementPage parentPage,
				Composite parent) {
			super(parent, SWT.NONE);
			this.parentPage = parentPage;
			setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

			setLayout(createOptionsPanelLayout());
			Group group = new Group(this, SWT.NONE);
			group.setText(XMLWizardsMessages._UI_LABEL_DOCTYPE_INFORMATION);

			GridLayout layout = new GridLayout();
			layout.numColumns = 2;
			group.setLayout(layout);
			group.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

			Label publicIdLabel = new Label(group, SWT.NONE);
			publicIdLabel.setText(XMLWizardsMessages._UI_LABEL_PUBLIC_ID);
			publicIdField = new Text(group, SWT.SINGLE | SWT.BORDER);
			publicIdField.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
			publicIdField.addModifyListener(this);
			// PlatformUI
			// .getWorkbench()
			// .getHelpSystem()
			// .setHelp(
			// publicIdField,
			// IXMLWizardHelpContextIds.XML_NEWWIZARD_SELECTROOTELEMENT5_HELPID);

			Label systemIdLabel = new Label(group, SWT.NONE);
			systemIdLabel.setText(XMLWizardsMessages._UI_LABEL_SYSTEM_ID);
			systemIdField = new Text(group, SWT.SINGLE | SWT.BORDER);
			systemIdField.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
			systemIdField.addModifyListener(this);
			// PlatformUI
			// .getWorkbench()
			// .getHelpSystem()
			// .setHelp(
			// systemIdField,
			// IXMLWizardHelpContextIds.XML_NEWWIZARD_SELECTROOTELEMENT6_HELPID);
		}

		@Override
		public void update() {
			String thePublicId = null;
			String theSystemId = null;
			ICatalogEntry xmlCatalogEntry = generator.getXMLCatalogEntry();

			if (xmlCatalogEntry != null) {
				if (xmlCatalogEntry.getEntryType() == ICatalogEntry.ENTRY_TYPE_PUBLIC) {
					thePublicId = xmlCatalogEntry.getKey();
					theSystemId = xmlCatalogEntry
							.getAttributeValue(ICatalogEntry.ATTR_WEB_URL);
					if (theSystemId == null) {
						theSystemId = generator.getGrammarURI().startsWith(
								"http:") ? generator.getGrammarURI() : URIHelper.getLastSegment(generator.getGrammarURI()); //$NON-NLS-1$
					}
				} else {
					theSystemId = xmlCatalogEntry.getKey();
				}
			} else {
				theSystemId = getDefaultSystemId();
			}

			publicIdField.setText(thePublicId != null ? thePublicId : ""); //$NON-NLS-1$
			systemIdField.setText(theSystemId != null ? theSystemId : ""); //$NON-NLS-1$
		}

		private String getDefaultSystemId() {
			// TODO Auto-generated method stub
			return ""; //$NON-NLS-1$
		}

		public void modifyText(ModifyEvent e) {
			generator.setSystemId(systemIdField.getText());
			generator.setPublicId(publicIdField.getText());
			parentPage.updateErrorMessage();
		}

		public String computeErrorMessage() {
			return null;
		}
	}

	public void createControl(Composite parent) {
		// container group
		Composite containerGroup = new Composite(parent, SWT.NONE);
		// PlatformUI
		// .getWorkbench()
		// .getHelpSystem()
		// .setHelp(
		// containerGroup,
		// IXMLWizardHelpContextIds.XML_NEWWIZARD_SELECTROOTELEMENT_HELPID);
		containerGroup.setLayout(new GridLayout());
		containerGroup.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		setControl(containerGroup);

		// select root element
		Label containerLabel = new Label(containerGroup, SWT.NONE);
		containerLabel.setText(XMLWizardsMessages._UI_LABEL_ROOT_ELEMENT);
		combo = new Combo(containerGroup, SWT.DROP_DOWN | SWT.READ_ONLY);
		combo.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		combo.addSelectionListener(this);

		// Options
		{
			Group group = new Group(containerGroup, SWT.NONE);
			group.setText(XMLWizardsMessages._UI_WIZARD_CONTENT_OPTIONS);

			GridLayout layout = new GridLayout();
			layout.numColumns = 1;
			layout.makeColumnsEqualWidth = true;
			layout.marginWidth = 0;
			group.setLayout(layout);
			group.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

			radioButton = new Button[4];

			radioButton[0] = new Button(group, SWT.CHECK);
			radioButton[0]
					.setText(XMLWizardsMessages._UI_WIZARD_CREATE_OPTIONAL_ATTRIBUTES);
			radioButton[0]
					.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
			radioButton[0].setSelection(false);
			// PlatformUI
			// .getWorkbench()
			// .getHelpSystem()
			// .setHelp(
			// radioButton[0],
			// IXMLWizardHelpContextIds.XML_NEWWIZARD_SELECTROOTELEMENT1_HELPID);

			radioButton[1] = new Button(group, SWT.CHECK);
			radioButton[1]
					.setText(XMLWizardsMessages._UI_WIZARD_CREATE_OPTIONAL_ELEMENTS);
			radioButton[1]
					.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
			radioButton[1].setSelection(false);
			// PlatformUI
			// .getWorkbench()
			// .getHelpSystem()
			// .setHelp(
			// radioButton[1],
			// IXMLWizardHelpContextIds.XML_NEWWIZARD_SELECTROOTELEMENT2_HELPID);

			radioButton[2] = new Button(group, SWT.CHECK);
			radioButton[2]
					.setText(XMLWizardsMessages._UI_WIZARD_CREATE_FIRST_CHOICE);
			radioButton[2]
					.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
			radioButton[2].setSelection(false);
			// PlatformUI
			// .getWorkbench()
			// .getHelpSystem()
			// .setHelp(
			// radioButton[2],
			// IXMLWizardHelpContextIds.XML_NEWWIZARD_SELECTROOTELEMENT3_HELPID);

			radioButton[3] = new Button(group, SWT.CHECK);
			radioButton[3]
					.setText(XMLWizardsMessages._UI_WIZARD_FILL_ELEMENTS_AND_ATTRIBUTES);
			radioButton[3]
					.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
			radioButton[3].setSelection(true);
			// PlatformUI
			// .getWorkbench()
			// .getHelpSystem()
			// .setHelp(
			// radioButton[3],
			// IXMLWizardHelpContextIds.XML_NEWWIZARD_SELECTROOTELEMENT4_HELPID);
			/*
			 * radioButton = new Button[2];
			 * 
			 * radioButton[0] = new Button(group, SWT.RADIO);
			 * radioButton[0].setText
			 * (XMLWizardsMessages.getString("_UI_WIZARD_CREATE_REQUIRED"));
			 * radioButton[0].setLayoutData(new
			 * GridData(GridData.FILL_HORIZONTAL));
			 * radioButton[0].setSelection(true);
			 * WorkbenchHelp.setHelp(radioButton[0],
			 * XMLBuilderContextIds.XMLC_CREATE_REQUIRED_ONLY);
			 * 
			 * radioButton[1] = new Button(group, SWT.RADIO);
			 * radioButton[1].setText
			 * (XMLWizardsMessages.getString("_UI_WIZARD_CREATE_OPTIONAL"));
			 * radioButton[1].setLayoutData(new
			 * GridData(GridData.FILL_HORIZONTAL));
			 * WorkbenchHelp.setHelp(radioButton[1],
			 * XMLBuilderContextIds.XMLC_CREATE_REQUIRED_AND_OPTION);
			 */
		}

		// add the grammar specific generation options
		//
		{
			pageBook = new PageBook(containerGroup, SWT.NONE);
			pageBook.setLayoutData(new GridData(GridData.FILL_BOTH));
			xsdOptionsPanel = new XSDOptionsPanel(this, pageBook);
			dtdOptionsPanel = new DTDOptionsPanel(this, pageBook);
			pageBook.showPage(xsdOptionsPanel);
		}
	}

	public void widgetSelected(SelectionEvent event) {
		int index = combo.getSelectionIndex();
		String rootElementName = (index != -1) ? combo.getItem(index) : null;
		generator.setRootElementName(rootElementName);
	}

	public void widgetDefaultSelected(SelectionEvent event) {
		// do nothing
	}

	@Override
	public void setVisible(boolean visible) {
		super.setVisible(visible);
		

		if (visible) {
			if (preparator.getGrammarURI() == null) {
				// no schema selected
				generator.setGrammarURI(null);
				generator.setCMDocument(null);
				generator.setXMLCatalogEntry(null);
				combo.setItems(new String[0]);
				pageBook.setEnabled(false);
				setMessage(info.textgrid.lab.xmleditor.dialogs.XMLWizardsMessages.SelectRootElementPage_NoSchemaSelected,
						INFORMATION);
			} else {
				pageBook.setEnabled(true);
				setMessage(null);
			}
			try {
				if (generator.getGrammarURI() == null
						|| !generator.getGrammarURI().equals(
								preparator.getGrammarURI())) {
					generator.setGrammarURI(preparator.getGrammarURI());
					generator.setXMLCatalogEntry(preparator
							.getXMLCatalogEntry());
					generator.setCMDocument(null);
				}
				if (generator.getGrammarURI() == null)
					return;

				if (generator.getCMDocument() == null) {
					final String[] errorInfo = new String[2];
					final CMDocument[] cmdocs = new CMDocument[1];
					IRunnableWithProgress r = new IRunnableWithProgress() {
						public void run(IProgressMonitor monitor) {
							SubMonitor.convert(monitor, info.textgrid.lab.xmleditor.dialogs.XMLWizardsMessages.SelectRootElementPage_LoadingSchema,
									IProgressMonitor.UNKNOWN);
							cmdocs[0] = NewXMLGenerator.createCMDocument(
									generator.getGrammarURI(), errorInfo);
						}
					};
					getContainer().run(true, false, r);

					generator.setCMDocument(cmdocs[0]);
					cmDocumentErrorMessage = errorInfo[1];
				}

				if ((generator.getCMDocument() != null)
						&& (cmDocumentErrorMessage == null)) {
					CMNamedNodeMap nameNodeMap = generator.getCMDocument()
							.getElements();
					Vector<String> nameNodeVector = new Vector<String>();

					for (int i = 0; i < nameNodeMap.getLength(); i++) {
						CMElementDeclaration cmElementDeclaration = (CMElementDeclaration) nameNodeMap
								.item(i);
						Object value = cmElementDeclaration
								.getProperty("Abstract"); //$NON-NLS-1$
						if (value != Boolean.TRUE) {
							nameNodeVector.add(cmElementDeclaration
									.getElementName());
						}
					}

					Object[] nameNodeArray = nameNodeVector.toArray();
					if (nameNodeArray.length > 0) {
						Arrays.sort(nameNodeArray, Collator.getInstance());
					}

					String defaultRootName = (String) (generator
							.getCMDocument())
							.getProperty("http://org.eclipse.wst/cm/properties/defaultRootName"); //$NON-NLS-1$
					if (defaultRootName == null
							/*
					 * && "http://www.tei-c.org/ns/1.0".equals(generator
					 * .getCMDocument().getNamespace().getURI())
					 */)
						defaultRootName = "TEI"; //$NON-NLS-1$
					int defaultRootIndex = -1;
					combo.removeAll();

					for (int i = 0; i < nameNodeArray.length; i++) {
						String elementName = (String) nameNodeArray[i];

						combo.add(elementName);
						if ((defaultRootName != null)
								&& defaultRootName.equals(elementName)) {
							defaultRootIndex = i;
						}
					}

					if (nameNodeArray.length > 0) {
						defaultRootIndex = defaultRootIndex != -1 ? defaultRootIndex
								: 0;
						combo.select(defaultRootIndex);
						generator.setRootElementName(combo
								.getItem(defaultRootIndex));
					}
				}

				if (generator.getGrammarURI().endsWith("xsd") || preparator.getGrammarObject() != null && preparator.getGrammarObject().getContentType(false).getId().equals("text/xsd+xml")) //$NON-NLS-1$ //$NON-NLS-2$
				{
					pageBook.showPage(xsdOptionsPanel);
					generator.setDefaultSystemId(getDefaultSystemId());
					generator.createNamespaceInfoList();

					// Provide default namespace prefix if none
					boolean usedDefaultNamespaceYet = false;
					for (int i = 0; i < generator.namespaceInfoList.size(); i++) {
						NamespaceInfo nsinfo = (NamespaceInfo) generator.namespaceInfoList
								.get(i);
						if (((nsinfo.prefix == null) || (nsinfo.prefix.trim()
								.length() == 0))
								&& ((nsinfo.uri != null) && (nsinfo.uri.trim()
										.length() != 0))) {
							nsinfo.prefix = getDefaultPrefix(generator.namespaceInfoList);
						}
						
						// TG-190 /
						// https://bugs.eclipse.org/bugs/show_bug.cgi?id=203301
						// FIXME remove this fix after upgrading to WST 3.0
						// (where the bug above
						// should be resolved)
						if (XMLConstants.XML_NS_URI.equals(nsinfo.uri))
							nsinfo.prefix = XMLConstants.XML_NS_PREFIX;	
						
						if (!nsinfo.isPrefixRequired
								&& !usedDefaultNamespaceYet) {
							nsinfo.prefix = null;
							usedDefaultNamespaceYet = true;
						}
					}
					xsdOptionsPanel
							.setNamespaceInfoList(generator.namespaceInfoList);
				} else if (generator.getGrammarURI().endsWith("dtd")) //$NON-NLS-1$
				{
					pageBook.showPage(dtdOptionsPanel);
					dtdOptionsPanel.update();
				}
			} catch (Exception e) {
				IStatus status = Activator
						.handleError(
								e,
								info.textgrid.lab.xmleditor.dialogs.XMLWizardsMessages.SelectRootElementPage_ErrorBuildingGrammar,
								e.getClass().getSimpleName(), generator
										.getGrammarURI(), e.getMessage());
				cmDocumentErrorMessage = status.getMessage();
			}

			/*
			 * String errorMessage = computeErrorMessage(); if (errorMessage ==
			 * null) super.setVisible(visible);
			 */

			updateErrorMessage();
		}
	}

	private String getDefaultSystemId() {
		// TODO Auto-generated method stub
		return null;
	}

	private String getDefaultPrefix(List<?> nsInfoList) {
		String defaultPrefix = "p"; //$NON-NLS-1$
		if (nsInfoList == null) {
			return defaultPrefix;
		}

		Vector<String> v = new Vector<String>();
		for (int i = 0; i < nsInfoList.size(); i++) {
			NamespaceInfo nsinfo = (NamespaceInfo) nsInfoList.get(i);
			if (nsinfo.prefix != null) {
				v.addElement(nsinfo.prefix);
			}
		}

		if (v.contains(defaultPrefix)) {
			String s = defaultPrefix;
			for (int j = 0; v.contains(s); j++) {
				s = defaultPrefix + Integer.toString(j);
			}
			return s;
		}
		return defaultPrefix;
	}

	@Override
	public boolean isPageComplete() {

		if (preparator.getGrammarURI() == null)
			return true; // no schema selected

		boolean complete = ((generator.getRootElementName() != null) && (generator
				.getRootElementName().length() > 0))
				&& (getErrorMessage() == null);

		if (complete) {
			/*
			 * int buildPolicy = radioButton[0].getSelection() ?
			 * DOMContentBuilder.BUILD_ONLY_REQUIRED_CONTENT :
			 * DOMContentBuilder.BUILD_ALL_CONTENT;
			 */
			int buildPolicy = 0;
			if (radioButton[0].getSelection()) {
				buildPolicy = buildPolicy
						| DOMContentBuilder.BUILD_OPTIONAL_ATTRIBUTES;
			}
			if (radioButton[1].getSelection()) {
				buildPolicy = buildPolicy
						| DOMContentBuilder.BUILD_OPTIONAL_ELEMENTS;
			}
			if (radioButton[2].getSelection()) {
				buildPolicy = buildPolicy
						| DOMContentBuilder.BUILD_FIRST_CHOICE
						| DOMContentBuilder.BUILD_FIRST_SUBSTITUTION;
			}
			if (radioButton[3].getSelection()) {
				buildPolicy = buildPolicy | DOMContentBuilder.BUILD_TEXT_NODES;
			}

			generator.setBuildPolicy(buildPolicy);
		}

		return complete;
	}

	public String computeErrorMessage() {
		
		String errorMessage = null;

		if (cmDocumentErrorMessage != null) {
			errorMessage = cmDocumentErrorMessage + info.textgrid.lab.xmleditor.dialogs.XMLWizardsMessages.SelectRootElementPage_SelectDifferentSchema;
		} else if (preparator.getGrammarURI() != null
				&& ((generator.getRootElementName() == null) || (generator.getRootElementName().length() == 0))) {
			errorMessage = XMLWizardsMessages._ERROR_ROOT_ELEMENT_MUST_BE_SPECIFIED;
		}

		return errorMessage;
	}

	public void updateErrorMessage() {
		String errorMessage = computeErrorMessage();
		if (errorMessage == null) {
			if (xsdOptionsPanel.isVisible()) {

				errorMessage = xsdOptionsPanel.computeErrorMessage();
			} else if (dtdOptionsPanel.isVisible()) {
				errorMessage = dtdOptionsPanel.computeErrorMessage();
			}
		}
		setErrorMessage(errorMessage);
		setPageComplete(isPageComplete());
	}

	public void finishPage() {

		Assert.isTrue(isPageComplete(), "Page not complete!?"); // calculate //$NON-NLS-1$
		// some stuff

		TextGridObject object = wizard.getTextGridObject();
		IFile file = getAdapter(object, IFile.class);

		TextGridObject grammarObject = preparator.getGrammarObject();
		if (grammarObject != null) {
			object.setSchema(grammarObject.getURI());
		}

		try {
			if (preparator.getGrammarURI() != null) {
				ByteArrayOutputStream documentStream = generator.createXMLDocument(file.getName(), "UTF-8"); //$NON-NLS-1$
				object.setInitialContent(documentStream.toByteArray());
			} else {
				object.setInitialContent("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n".getBytes("UTF-8")); //$NON-NLS-1$ //$NON-NLS-2$
			}
		} catch (Exception e) {
			IStatus status = Activator.handleError(e, info.textgrid.lab.xmleditor.dialogs.XMLWizardsMessages.SelectRootElementPage_ErrorSerializingSkeleton,
					e.getClass().getSimpleName(), object, file, e.getLocalizedMessage());
			setErrorMessage(status.getMessage());
		}

	}

	public void init(ITextGridWizard wizard, INewObjectPreparator preparator) {
		this.wizard = wizard;
		if (preparator instanceof NewXMLObjectPreparator)
			this.preparator = (NewXMLObjectPreparator) preparator;
		else
			Activator.handleError(null,
					"SelectRootElementPage without a NewXMLObjectPreparator!?"); //$NON-NLS-1$
	}

	public void loadObject(TextGridObject textGridObject) {
		// TODO Auto-generated method stub

	}
}