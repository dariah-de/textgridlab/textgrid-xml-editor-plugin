package info.textgrid.lab.xmleditor.dialogs;

import static info.textgrid.lab.core.swtutils.AdapterUtils.getAdapter;
import info.textgrid.lab.core.model.CrudServiceException;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.ui.core.utils.TextGridObjectTableViewer;
import info.textgrid.lab.ui.core.utils.TextGridObjectTableViewer.Column;
import info.textgrid.lab.xmleditor.mpeditor.Activator;

import java.net.URI;
import java.text.MessageFormat;
import java.util.EnumSet;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.MultiPageEditorPart;
import org.eclipse.ui.statushandlers.StatusManager;
import org.eclipse.ui.texteditor.ITextEditor;

public class SetSchemaDialog extends TitleAreaDialog implements
		ISelectionChangedListener, SelectionListener {

	

	static final int TITLE_COLUMN = 0;
	static final int PROJECT_COLUMN = 1;
	private Composite dialogContent;
	private Button noSchema;

	private void createSchemaTable(Composite parent) {
		schemaTable = new TextGridObjectTableViewer(parent, SWT.SINGLE
				| SWT.FULL_SELECTION);
		
		schemaTable.setVisibleColumns(EnumSet.of(Column.TITLE, Column.PROJECT, Column.DATE, Column.OWNER));

		schemaTable.addSelectionChangedListener(this);
		GridDataFactory.fillDefaults().grab(true, true).hint(SWT.DEFAULT, 200)
				.applyTo(schemaTable.getTable());

		URI currentSchemaURI;
		try {
			currentSchemaURI = object.getSchemaURI();
			if (currentSchemaURI != null)
				schemaTable.setDefaultSelection(TextGridObject.getInstanceOffline(currentSchemaURI));
		} catch (CrudServiceException e) {
			Activator.handleProblem(IStatus.ERROR, e, Messages.SetSchemaDialog_CouldNotDetermineSchema, object, e.getMessage());
		}

		schemaTable.queryMetadata("( format:\"text/xsd+xml\" )"); //$NON-NLS-1$
	}

	@Override
	protected Control createDialogArea(Composite parent) {

		dialogContent = new Composite(parent, SWT.NONE);
		dialogArea = dialogContent;
		GridLayoutFactory.fillDefaults().applyTo(dialogContent);
		GridDataFactory.fillDefaults().hint(SWT.DEFAULT, 400).grab(true, true).applyTo(dialogContent);

		PlatformUI.getWorkbench().getHelpSystem().setHelp(dialogArea, "info.textgrid.lab.xmleditor.mpeditor.SetSchemaDialog"); //$NON-NLS-1$
		PlatformUI.getWorkbench().getHelpSystem().setHelp(parent, "info.textgrid.lab.xmleditor.mpeditor.SetSchemaDialog"); //$NON-NLS-1$

		Label currentSchema = new Label(dialogContent, SWT.WRAP);
		GridDataFactory.fillDefaults().grab(true, false).hint(400, SWT.DEFAULT).applyTo(
				currentSchema);
		URI schemaURI = null;
		try {
			schemaURI = object.getSchemaURI();
			if (schemaURI != null) {
				TextGridObject schema;
				schema = TextGridObject.getInstance(schemaURI, true);
				IFile schemaFile = getAdapter(schema, IFile.class);
				int severity = schemaFile == null ? IMarker.SEVERITY_ERROR : schemaFile.findMaxProblemSeverity(null, true,
						IResource.DEPTH_ZERO);
				if (severity >= IMarker.SEVERITY_ERROR) {
					currentSchema.setText(MessageFormat.format(
							Messages.SetSchemaDialog_SchemaFailedLastTime, schema, object));
				} else {
					currentSchema.setText(MessageFormat.format(
						Messages.SetSchemaDialog_CurentSchemaMessage, object.getTitle(),
						schema.getTitle(), schema.getProject(), schema.getURI()));
				}
			} else {
				currentSchema.setText(MessageFormat.format(Messages.SetSchemaDialog_NoSchemaMessage, object.getTitle()));
			}
		} catch (CoreException e) {
			Activator.handleProblem(IStatus.ERROR, e, Messages.SetSchemaDialog_NoInfoMessage, object, e.getMessage());
		}

		
		createSchemaTable(dialogContent);

		noSchema = new Button(dialogContent, SWT.RADIO);
		GridDataFactory.fillDefaults().applyTo(noSchema);
		noSchema.setText(Messages.SetSchemaDialog_NoSchemaButton);
		noSchema.addSelectionListener(this);
		noSchema
				.setToolTipText(Messages.SetSchemaDialog_NoSchemaTooltip);
		
		noSchema.setSelection(schemaURI == null);
		
		Label saveWarning = new Label(dialogContent, SWT.WRAP | SWT.LEAD);
		saveWarning.setImage(JFaceResources.getImage(DLG_IMG_MESSAGE_WARNING));
		 saveWarning
				.setText(Messages.SetSchemaDialog_SaveWarning);
		GridDataFactory.fillDefaults().grab(true, false).applyTo(saveWarning);

		getShell().setText(Messages.SetSchemaDialog_SelectSchemaShellTitle);
		setTitle(Messages.SetSchemaDialog_SelectSchemaDlgTitle);
		try {
			setMessage(NLS.bind(
					Messages.SetSchemaDialog_SelectSchemaDlgMessage,
					object
							.getTitle()));
		} catch (CoreException e) {
			Activator.handleProblem(IStatus.WARNING, e,
					Messages.SetSchemaDialog_CouldNotFetchTitle, object, e
							.getLocalizedMessage());
		}

		
		return dialogArea;
	}

	private TextGridObject object;
	private TextGridObjectTableViewer schemaTable;
	private IEditorPart editor = null;

	public SetSchemaDialog(Shell parentShell, TextGridObject object,
			IEditorPart editor) {
		super(parentShell);
		Assert.isNotNull(object,
				"Cannot associate a schema to a null object ..."); //$NON-NLS-1$
		this.object = object;
		this.editor = editor;
	}

	public void selectionChanged(SelectionChangedEvent event) {
		getButton(IDialogConstants.OK_ID).setEnabled(
				event.getSelection() != null);
		if (event.getSelectionProvider() == schemaTable
				&& !event.getSelection().isEmpty())
			noSchema.setSelection(false);

		if (!schemaTable.getSelection().isEmpty()) {
			IFile schemaFile = getAdapter(((IStructuredSelection) schemaTable.getSelection()).getFirstElement(), IFile.class);
			try {
				IMarker[] markers = schemaFile.findMarkers(null, true, IResource.DEPTH_INFINITE);
				StringBuilder errorMessage = new StringBuilder();
				for (IMarker marker : markers) {
					if (marker.getAttribute(IMarker.SEVERITY, IMarker.SEVERITY_INFO) >= IMarker.SEVERITY_ERROR) {
						errorMessage.append(marker.getAttribute(IMarker.MESSAGE)).append("\n"); //$NON-NLS-1$
					}
				}
				if (errorMessage.length() > 0)
					setErrorMessage(NLS.bind(Messages.SetSchemaDialog_Problems, errorMessage));
				else
					setErrorMessage(null);

			} catch (CoreException e) {
				StatusManager.getManager().handle(e, Activator.PLUGIN_ID);
			}

		}
	}


	public void widgetSelected(SelectionEvent e) {
		e.doit = true;
		if (e.widget == noSchema) {
			if (noSchema.getSelection()) {
				schemaTable.setSelection(new StructuredSelection());
			}
		}
	}

	public void widgetDefaultSelected(SelectionEvent e) {
	}

	@Override
	protected void okPressed() {
		
		if (noSchema.getSelection())
			object.deleteSchema();
		else {
			object
					.setSchema(((TextGridObject) ((IStructuredSelection) schemaTable
							.getSelection()).getFirstElement()).getURI());
		}
		IFile file = getAdapter(object, IFile.class);
		if (file != null)
			try {
				file.deleteMarkers(null, true, IResource.DEPTH_INFINITE);
			} catch (CoreException e) {
				StatusManager.getManager().handle(e, Activator.PLUGIN_ID);
			}
		if (editor != null) {
			if (editor instanceof MultiPageEditorPart) {
				IEditorPart[] editors = ((MultiPageEditorPart) editor)
						.findEditors(editor.getEditorInput());
				for (IEditorPart editorPage : editors) {
					markEditorChanged(editorPage);
				}
			} else
				markEditorChanged(editor);
		}
		super.okPressed();
	}

	private void markEditorChanged(IEditorPart editor) {
		if (editor instanceof ITextEditor) {
			ITextEditor textEditor = (ITextEditor) editor;
			try {
				// XXX Evil Hack to mark the document dirty. There is no API for
				// this :-(
				textEditor.getDocumentProvider().getDocument(
						editor.getEditorInput()).replace(0, 0, ""); //$NON-NLS-1$
			} catch (BadLocationException e) {
			}
		}
	}
}
