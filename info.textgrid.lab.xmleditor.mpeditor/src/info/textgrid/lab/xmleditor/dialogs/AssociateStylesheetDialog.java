package info.textgrid.lab.xmleditor.dialogs;

import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.swtutils.AdapterUtils;
import info.textgrid.lab.ui.core.utils.TextGridObjectTableViewer;
import info.textgrid.lab.xmleditor.mpeditor.MPXmlEditorPart;

import java.net.URISyntaxException;

import net.sf.vex.editor.VexEditorPage;
import net.sf.vex.editor.action.ShowInlineElementMarkersAction;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.progress.UIJob;
import org.eclipse.wst.sse.core.StructuredModelManager;
import org.eclipse.wst.sse.core.internal.provisional.text.IStructuredDocument;
import org.eclipse.wst.sse.ui.StructuredTextEditor;
import org.eclipse.wst.xml.core.internal.provisional.document.IDOMModel;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.ProcessingInstruction;


public class AssociateStylesheetDialog extends TitleAreaDialog {

	/**
	 * makes sure nobody selects both the radio button and an item in the viewer
	 */
	private final class SingleSelectionAdapter extends SelectionAdapter {
		@Override
		public void widgetSelected(final SelectionEvent e) {
			if (e.widget == defaultButton && defaultButton.getSelection()) {
				viewer.setSelection(new StructuredSelection());
			} else if (!viewer.getSelection().isEmpty()) {
				defaultButton.setSelection(false);
			}
		}
	}

	private Table table;
	private Button defaultButton;
	private TextGridObjectTableViewer viewer;
	private StructuredTextEditor sourceEditor;
	private VexEditorPage vexEditor;
	private TextGridObject currentStylesheet;
	private IEditorPart editor;
	private VexEditorPage.StylesheetInfo stylesheetInfo;

	/**
	 * Create the dialog.
	 * 
	 * @param parentShell
	 * @param editor
	 */
	public AssociateStylesheetDialog(final Shell parentShell, IEditorPart editor) {
		super(parentShell);
		this.editor = editor;
	}

	/**
	 * Create contents of the dialog.
	 * 
	 * @param parent
	 */
	@Override
	protected Control createDialogArea(final Composite parent) {

		findActiveStylesheet();

		setMessage(Messages.AssociateStylesheetDialog_SelectStylesheetMessage);
		setTitle(Messages.AssociateStylesheetDialog_DialogTitle);
		final Composite area = (Composite) super.createDialogArea(parent);
		final Composite container = new Composite(area, SWT.NONE);
		container.setLayout(new GridLayout(1, false));
		container.setLayoutData(new GridData(GridData.FILL_BOTH));

		PlatformUI.getWorkbench().getHelpSystem().setHelp(parent, "info.textgrid.lab.xmleditor.mpeditor.SetCSSDialog"); //$NON-NLS-1$

		viewer = new TextGridObjectTableViewer(container, SWT.NONE);
		table = viewer.getTable();
		GridData gd_table = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gd_table.minimumHeight = 200;
		table.setLayoutData(gd_table);
		viewer.queryMetadata("format:\"text/css\""); //$NON-NLS-1$


		defaultButton = new Button(container, SWT.RADIO);
		defaultButton.setText(Messages.AssociateStylesheetDialog_DefaultStylesheetBtn);
		SingleSelectionAdapter singleSelectionAdapter = new SingleSelectionAdapter();
		defaultButton.addSelectionListener(singleSelectionAdapter);
		table.addSelectionListener(singleSelectionAdapter);
		defaultButton.setSelection(true);

		if (currentStylesheet != null)
			viewer.setDefaultSelection(currentStylesheet);
		return area;
	}

	// Try to find the stylesheet
	@SuppressWarnings("restriction")
	protected void findActiveStylesheet() {
		if (editor == null)
			editor = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor();
		if (editor instanceof MPXmlEditorPart) {
			final MPXmlEditorPart xmlEditor = (MPXmlEditorPart) editor;
			sourceEditor = xmlEditor.getSourceEditor();
			vexEditor = xmlEditor.getWysiwymEditor();

			final IDocument document = sourceEditor.getDocumentProvider().getDocument(sourceEditor.getEditorInput());
			IDOMModel model = (IDOMModel) StructuredModelManager.getModelManager().getModelForRead((IStructuredDocument) document);
			try {
				stylesheetInfo = VexEditorPage.findActiveStylesheet(model);
				if (stylesheetInfo != null) {
					if (!"textgrid".equals(stylesheetInfo.stylesheetURI.getScheme())) //$NON-NLS-1$
						setErrorMessage(NLS.bind(Messages.AssociateStylesheetDialog_ErrorTextGridURIsOnly, stylesheetInfo.stylesheetURI));
					else
						currentStylesheet = TextGridObject.getInstanceOffline(stylesheetInfo.stylesheetURI);
				}
			} catch (final URISyntaxException e) {
				setErrorMessage(Messages.AssociateStylesheetDialog_InvalidXMLStylesheet);
			} finally {
				model.releaseFromRead();
			}
		}
	}

	@SuppressWarnings("restriction")
	protected void okPressed() {
		boolean useDefault = defaultButton.getSelection();
		TextGridObject newStylesheet = useDefault ? null : AdapterUtils.getAdapter(
				((IStructuredSelection) viewer.getSelection()).getFirstElement(), TextGridObject.class);
		
		// (1) write the processing instruction to the stylesheet
		final IDocument document = sourceEditor.getDocumentProvider().getDocument(sourceEditor.getEditorInput());
		IDOMModel model = (IDOMModel) StructuredModelManager.getModelManager().getModelForEdit((IStructuredDocument) document);
		try {
		final Document domDocument = model.getDocument();

			if (stylesheetInfo != null && stylesheetInfo.stylesheetNode != null) {
			// find and modify
			final NodeList childNodes = domDocument.getChildNodes();
			for (int i = 0; i < childNodes.getLength(); i++) {
				final Node node = childNodes.item(i);
					if (node.equals(stylesheetInfo.stylesheetNode)) {
					if (useDefault)
						domDocument.removeChild(node);
					else
						((ProcessingInstruction) node).setData("type=\"text/css\" href=\"" + newStylesheet.getLatestURI() + "\""); //$NON-NLS-1$ //$NON-NLS-2$
				}
			}
		} else if (!useDefault) {
			ProcessingInstruction pi = domDocument.createProcessingInstruction("xml-stylesheet", "type=\"text/css\" href=\"" //$NON-NLS-1$ //$NON-NLS-2$
					+ newStylesheet.getLatestURI() + "\""); //$NON-NLS-1$
				domDocument.insertBefore(pi, domDocument.getDocumentElement());
		} // else nothing to do
		
		} finally {
			model.releaseFromEdit();
		}

		// (2) update Vex if it's there
		if (vexEditor != null && vexEditor.getVexWidget() != null) {
			vexEditor.setStylesheet(useDefault? null : newStylesheet.getLatestURI());
			// XXX Workaround TG-1565
			UIJob tg1565 = new UIJob(Messages.AssociateStylesheetDialog_UpdatingWYSIWYM) {
				
				@Override
				public IStatus runInUIThread(IProgressMonitor monitor) {
					new ShowInlineElementMarkersAction().run(vexEditor.getVexWidget());
					new ShowInlineElementMarkersAction().run(vexEditor.getVexWidget());
					return Status.OK_STATUS;
				}
			};
			tg1565.schedule();
		}

		super.okPressed();
	}
}
