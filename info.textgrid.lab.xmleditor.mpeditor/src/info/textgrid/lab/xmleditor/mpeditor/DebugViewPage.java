/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2004 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package info.textgrid.lab.xmleditor.mpeditor;

import java.lang.reflect.Field;

import net.sf.vex.core.Caret;
import net.sf.vex.core.Rectangle;
import net.sf.vex.editor.VexEditor;
import net.sf.vex.editor.VexEditorPage;
import net.sf.vex.layout.Box;
import net.sf.vex.layout.BoxAndOffset;
import net.sf.vex.swt.VexWidget;
import net.sf.vex.widget.HostComponent;
import net.sf.vex.widget.VexWidgetImpl;

import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.ControlListener;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseMoveListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.part.IPageBookViewPage;
import org.eclipse.ui.part.IPageSite;

/**
 * Page in the debug view. 
 */
class DebugViewPage implements IPageBookViewPage {
    
    public DebugViewPage(MPXmlEditorPart vexEditor) {
        this.vexEditor = vexEditor;
    }

    public void createControl(Composite parent) {
        
        this.composite = new Composite(parent, SWT.NONE);
        this.composite.setLayout(new FillLayout());

        if (((VexEditorPage)this.vexEditor.getWysiwymEditor()).isLoaded()) {
            this.createDebugPanel();
        } else {
            this.loadingLabel = new Label(this.composite, SWT.NONE);
            this.loadingLabel.setText("Loading..."); //$NON-NLS-1$
        }

        this.vexEditor.getEditorSite().getSelectionProvider().addSelectionChangedListener(this.selectionListener);
    }
    
    public void dispose() {
        if (this.vexWidget != null && !this.vexWidget.isDisposed()) {
            this.vexWidget.removeMouseMoveListener(this.mouseMoveListener);
        }
        this.vexEditor.getEditorSite().getSelectionProvider().removeSelectionChangedListener(this.selectionListener);
        impl.setDebugRect(null);
        impl.repaint();
    }

    public Control getControl() {
        return this.composite;
    }

    public IPageSite getSite() {
        return this.site;
    }

    public void init(IPageSite site) throws PartInitException {
        this.site = site;
    }

    public void setActionBars(IActionBars actionBars) {
    }

    public void setFocus() {
    }

    //================================================== PRIVATE
    
    private static final int X = 1;
    private static final int Y = 2;
    private static final int WIDTH = 3;
    private static final int HEIGHT = 4;

    private static Field implField;
    private static Field rootBoxField;
    private static Field caretField;
    private static Field hostComponentField;
    
    static {
        try {
            implField = VexWidget.class.getDeclaredField("impl"); //$NON-NLS-1$
            implField.setAccessible(true);
            rootBoxField = VexWidgetImpl.class.getDeclaredField("rootBox"); //$NON-NLS-1$
            rootBoxField.setAccessible(true);
            caretField = VexWidgetImpl.class.getDeclaredField("caret"); //$NON-NLS-1$
            caretField.setAccessible(true);
            hostComponentField = VexWidgetImpl.class.getDeclaredField("hostComponent"); //$NON-NLS-1$
            hostComponentField.setAccessible(true);
        } catch (Exception e) {
            // TODO: handle exception
        }
    }
    
    private IPageSite site;
    private MPXmlEditorPart vexEditor;
    private VexWidget vexWidget;
    private VexWidgetImpl impl;
    private Composite composite;

    private Label loadingLabel;

    private Table table;
    private TableItem documentItem;
    private TableItem viewportItem;
    private TableItem caretOffsetItem;
    private TableItem caretAbsItem;
    private TableItem caretRelItem;
    private TableItem mouseAbsItem;
    private TableItem mouseRelItem;
    private TableItem currentBoxItem;

    private void createDebugPanel() {

        if (this.loadingLabel != null) {
            this.loadingLabel.dispose();
            this.loadingLabel = null;
        }
        
        this.vexWidget = ((VexEditorPage)this.vexEditor.getWysiwymEditor()).getVexWidget();
        try {
            this.impl = (VexWidgetImpl) implField.get(this.vexWidget);
        } catch (IllegalArgumentException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        GridLayout layout = new GridLayout();
        layout.numColumns = 1;
        composite.setLayout(layout);
        GridData gd;
        
        ScrolledComposite sc = new ScrolledComposite(this.composite, SWT.V_SCROLL);
        this.table = new Table(sc, SWT.NONE);
        this.table.setHeaderVisible(true);
        sc.setContent(table);
        sc.setExpandHorizontal(true);
        sc.setExpandVertical(true);
        gd = new GridData();
        gd.grabExcessHorizontalSpace = true;
        gd.horizontalAlignment = GridData.FILL;
        sc.setLayoutData(gd);
        
        TableColumn column;
        column = new TableColumn(this.table, SWT.LEFT);
        column.setText("Item"); //$NON-NLS-1$
        column = new TableColumn(this.table, SWT.RIGHT);
        column.setText("X"); //$NON-NLS-1$
        column = new TableColumn(this.table, SWT.RIGHT);
        column.setText("Y"); //$NON-NLS-1$
        column = new TableColumn(this.table, SWT.RIGHT);
        column.setText("Width"); //$NON-NLS-1$
        column = new TableColumn(this.table, SWT.RIGHT);
        column.setText(Messages.DebugViewPage_9);
        
        this.table.addControlListener(this.controlListener);
        
        this.documentItem = new TableItem(this.table, SWT.NONE);
        this.documentItem.setText(0, "Document"); //$NON-NLS-1$
        this.viewportItem = new TableItem(this.table, SWT.NONE);
        this.viewportItem.setText(0, "Viewport"); //$NON-NLS-1$
        this.caretOffsetItem = new TableItem(this.table, SWT.NONE);
        this.caretOffsetItem.setText(0, "Caret Offset"); //$NON-NLS-1$
        this.caretAbsItem = new TableItem(this.table, SWT.NONE);
        this.caretAbsItem.setText(0, "Caret Abs."); //$NON-NLS-1$
        this.caretRelItem = new TableItem(this.table, SWT.NONE);
        this.caretRelItem.setText(0, "Caret Rel."); //$NON-NLS-1$
        this.mouseAbsItem = new TableItem(this.table, SWT.NONE);
        this.mouseAbsItem.setText(0, "Mouse Abs."); //$NON-NLS-1$
        this.mouseRelItem = new TableItem(this.table, SWT.NONE);
        this.mouseRelItem.setText(0, "Mouse Rel."); //$NON-NLS-1$
        this.currentBoxItem = new TableItem (this.table, SWT.NONE);
        this.currentBoxItem.setText(0, "Cur. Box"); //$NON-NLS-1$
        
        
        Button updateButton = new Button(composite, SWT.PUSH);
        updateButton.setText("Refresh"); //$NON-NLS-1$
        updateButton.addSelectionListener(new SelectionListener() {
            public void widgetSelected(SelectionEvent e) {
                repopulate();
            }
            public void widgetDefaultSelected(SelectionEvent e) {
            }
        });

        this.composite.layout();
        
        this.vexWidget.addMouseMoveListener(this.mouseMoveListener);
        
        this.repopulate();
        
    }
    

    private ISelectionChangedListener selectionListener = new ISelectionChangedListener() {
        public void selectionChanged(SelectionChangedEvent event) {
            if (vexWidget == null) {
                createDebugPanel();
            }
            repopulate();
        }
    };

    private ControlListener controlListener = new ControlListener() {
        public void controlMoved(ControlEvent e) {
        }
        public void controlResized(ControlEvent e) {
            int width = table.getSize().x;
            int numWidth = Math.round(width * 0.125f); 
            table.getColumn(0).setWidth(width / 2);
            table.getColumn(1).setWidth(numWidth);
            table.getColumn(2).setWidth(numWidth);
            table.getColumn(3).setWidth(numWidth);
            table.getColumn(4).setWidth(numWidth);
            
        }
    };
    
    private MouseMoveListener mouseMoveListener = new MouseMoveListener() {

        public void mouseMove(MouseEvent e) {
            Rectangle rect = new Rectangle(e.x, e.y, 0, 0);
            Rectangle viewport = getViewport();
            setItemFromRect(mouseAbsItem, rect);
            setItemRel(mouseRelItem, viewport, rect);
            
            
            //the mouse position relative to the document
            int mouseX = e.x + viewport.getX();
            int mouseY = e.y + viewport.getY();
            
            setCurBoxItem(mouseX, mouseY);


        }

		private void setCurBoxItem(int x, int y) {

			BoxAndOffset cursorBox = impl.getBoxAt(x, y);

			if (cursorBox != null) {

				impl.setDebugRect(new Rectangle(cursorBox.box.getX()
						+ cursorBox.x, cursorBox.box.getY() + cursorBox.y,
						cursorBox.box.getWidth(), cursorBox.box.getHeight()));

				impl.repaint();

				String text = cursorBox == null ? "[none]" : cursorBox.box //$NON-NLS-1$
						.getClass().getSimpleName()
						+ ": " + cursorBox.box.toString(); //$NON-NLS-1$
				currentBoxItem.setText(1, text);
			}
		}
	};
    
    private Rectangle getCaretBounds() {
        Caret caret = (Caret) this.getFieldValue(caretField, this.impl);
        return caret.getBounds();
    }

    private Rectangle getRootBoxBounds() {
        Box rootBox = (Box) this.getFieldValue(rootBoxField, this.impl);
        return new Rectangle(rootBox.getX(), rootBox.getY(), rootBox.getWidth(), rootBox.getHeight());
    }

    private Rectangle getViewport() {
        HostComponent hc = (HostComponent) this.getFieldValue(hostComponentField, this.impl);
        return hc.getViewport();
    }
    
    private Object getFieldValue(Field field, Object o) {
        try {
            return field.get(o);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    
    private void repopulate() {
        setItemFromRect(this.documentItem, this.getRootBoxBounds());
        Rectangle viewport = this.getViewport();
        this.caretOffsetItem.setText(1, Integer.toString(this.impl.getCaretOffset()));
        
        
        setItemFromRect(this.viewportItem, viewport);
        setItemFromRect(this.caretAbsItem, this.getCaretBounds());
        setItemRel(this.caretRelItem, viewport, this.getCaretBounds());
    }
    
    private static void setItemFromRect(TableItem item, Rectangle rect) {
        item.setText(X, Integer.toString(rect.getX()));
        item.setText(Y, Integer.toString(rect.getY()));
        item.setText(WIDTH, Integer.toString(rect.getWidth()));
        item.setText(HEIGHT, Integer.toString(rect.getHeight()));
    }
    
    private static void setItemRel(TableItem item, Rectangle viewport, Rectangle rect) {
        //Original Code
        // Shows the mouse position relative to an element
        //item.setText(X, Integer.toString(rect.getX() - viewport.getX()));
        //item.setText(Y, Integer.toString(rect.getY() - viewport.getY()));
        
        //New code
        //Shows the absolute cursor position
        item.setText(X, Integer.toString(rect.getX() + viewport.getX()));
        item.setText(Y, Integer.toString(rect.getY() + viewport.getY()));
        item.setText(WIDTH, Integer.toString(rect.getWidth()));
        item.setText(HEIGHT, Integer.toString(rect.getHeight()));
    }

    
    
}