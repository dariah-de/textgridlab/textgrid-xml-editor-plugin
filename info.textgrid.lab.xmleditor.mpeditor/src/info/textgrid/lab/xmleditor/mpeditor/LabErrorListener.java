package info.textgrid.lab.xmleditor.mpeditor;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import javax.xml.transform.ErrorListener;
import javax.xml.transform.TransformerException;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.ui.statushandlers.StatusManager;

public class LabErrorListener implements ErrorListener {
	
	private List<TransformerException> exceptions = Collections.synchronizedList(new LinkedList<TransformerException>());

	public void warning(TransformerException exception)
			throws TransformerException {
		exceptions.add(exception);
		StatusManager.getManager().handle(new Status(IStatus.WARNING, Activator.PLUGIN_ID, exception.getMessageAndLocation(), exception));
	}

	public void error(TransformerException exception)
			throws TransformerException {
		exceptions.add(exception);
		StatusManager.getManager().handle(new Status(IStatus.ERROR, Activator.PLUGIN_ID, exception.getMessageAndLocation(), exception));
	}

	public void fatalError(TransformerException exception)
			throws TransformerException {
		exceptions.add(exception);
		StatusManager.getManager().handle(new Status(IStatus.ERROR, Activator.PLUGIN_ID, exception.getMessageAndLocation(), exception));
		throw exception;
	}
	
	public synchronized String toHTML(boolean clear) {
		if (exceptions.isEmpty())
			return ""; //$NON-NLS-1$
		StringBuilder builder = new StringBuilder("<ul>"); //$NON-NLS-1$
		
		for (TransformerException e : exceptions) {
			builder.append("<li>"); //$NON-NLS-1$
			if (e.getLocator() != null) {
				builder.append("<a href=\""); //$NON-NLS-1$
				builder.append(e.getLocator().getSystemId());
				builder.append("\">"); //$NON-NLS-1$
			}
			builder.append(e.getMessageAndLocation());
			if (e.getLocator() != null)
				builder.append("</a>"); //$NON-NLS-1$
			builder.append("</li>\n"); //$NON-NLS-1$
		}
		
		builder.append("</ul>\n"); //$NON-NLS-1$
		if (clear)
			clear();
		return builder.toString();
	}
	
	public void clear() {
		exceptions.clear();
	}

}
