/**
 *
 */
package info.textgrid.lab.xmleditor.mpeditor;

import static java.lang.Math.max;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.model.util.ResourceDeltaPrinter;
import info.textgrid.lab.core.swtutils.AdapterUtils;
import info.textgrid.lab.xmleditor.mpeditor.preferences.PreferenceConstants;

import javax.xml.XMLConstants;

import net.sf.vex.editor.VexEditorPage;
import net.sf.vex.swt.TextSelectionVex;
import net.sf.vex.swt.VexWidget;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IResourceChangeListener;
import org.eclipse.core.resources.IResourceDelta;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.ISchedulingRule;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IFindReplaceTarget;
import org.eclipse.jface.text.IFindReplaceTargetExtension;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.text.Region;
import org.eclipse.jface.viewers.IPostSelectionProvider;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.IEditorActionBarContributor;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.IPerspectiveDescriptor;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchPartReference;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.help.IWorkbenchHelpSystem;
import org.eclipse.ui.part.FileEditorInput;
import org.eclipse.ui.part.MultiPageEditorPart;
import org.eclipse.ui.part.MultiPageEditorSite;
import org.eclipse.ui.part.MultiPageSelectionProvider;
import org.eclipse.ui.progress.UIJob;
import org.eclipse.ui.statushandlers.StatusManager;
import org.eclipse.ui.views.contentoutline.IContentOutlinePage;
import org.eclipse.wst.sse.core.StructuredModelManager;
import org.eclipse.wst.sse.core.internal.provisional.IModelManager;
import org.eclipse.wst.sse.core.internal.provisional.IStructuredModel;
import org.eclipse.wst.sse.core.internal.provisional.IndexedRegion;
import org.eclipse.wst.sse.core.internal.provisional.text.IStructuredDocument;
import org.eclipse.wst.sse.core.internal.provisional.text.ITextRegion;
import org.eclipse.wst.sse.ui.StructuredTextEditor;
import org.eclipse.wst.sse.ui.internal.StructuredTextViewer;
import org.eclipse.wst.xml.core.internal.provisional.document.IDOMModel;
import org.eclipse.wst.xml.core.internal.provisional.document.IDOMNode;
import org.eclipse.wst.xml.ui.internal.Logger;
import org.eclipse.wst.xml.ui.internal.tabletree.AbstractXMLMultiPageEditorPart;
import org.eclipse.wst.xml.ui.internal.tabletree.XMLMultiPageEditorActionBarContributor;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.Text;
import org.w3c.dom.traversal.DocumentTraversal;
import org.w3c.dom.traversal.NodeFilter;
import org.w3c.dom.traversal.NodeIterator;

/**
 * The multi-page XML editor. This holds the design (e.g. table) and source
 * editors from WST as well as Vex for WYSIWYM editing.
 *
 * @author tv
 */
@SuppressWarnings("restriction")
public class MPXmlEditorPart extends AbstractXMLMultiPageEditorPart {

	public static final String ID = "info.textgrid.lab.xmleditor.mpeditor.MPEditorPart"; //$NON-NLS-1$
	private static final long MAX_FILE_SIZE = 8 * 1024 * 1024;

	/**
	 * A resource change listener that opens an editor for a new, non
	 *
	 * Hack to temporarily work around TG-167.
	 *
	 * @author tv
	 *
	 */
	private final class ReopenResourceChangeListener implements IResourceChangeListener {

		private final IFile file;
		private final TextGridObject textGridObject;

		public ReopenResourceChangeListener(final IFile file, final TextGridObject textGridObject) {
			this.file = file;
			this.textGridObject = textGridObject;
		}

		@Override
		public void resourceChanged(final IResourceChangeEvent event) {
			final IResourceDelta delta = event.getDelta().findMember(file.getFullPath());
			if (delta != null) {
				if (delta.getKind() == IResourceDelta.REMOVED) {
					final UIJob job = new UIJob(NLS.bind(Messages.MPXmlEditorPart_ReopeningX, textGridObject)) {

						@Override
						public IStatus runInUIThread(final IProgressMonitor monitor) {
							final IFile savedFile = AdapterUtils.getAdapter(textGridObject, IFile.class);
							if (savedFile != null) {
								try {
									getSite().getPage().openEditor(new FileEditorInput(savedFile), MPXmlEditorPart.ID, true);
									return Status.OK_STATUS;
								} catch (final PartInitException e) {
									return e.getStatus();
								}
							} else {
								return new Status(IStatus.ERROR, Activator.PLUGIN_ID, NLS.bind(
										"Tried to open a new editor for {0}, but could not make a resource out of it.", //$NON-NLS-1$
										textGridObject));
							}
						}
					};
					final ISchedulingRule rule = ResourcesPlugin.getWorkspace().getRuleFactory().refreshRule(file);
					job.setRule(rule);
					job.schedule(200);
				}
			}
		}

	}

	/*
	 * (non-Javadoc)
	 *
	 * This class always returns the source editor's content outline page, if an
	 * IContentOutlinePage is requested. Fixes TG-136.
	 *
	 * @see
	 * org.eclipse.wst.xml.ui.internal.tabletree.AbstractXMLMultiPageEditorPart
	 * #getAdapter(java.lang.Class)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Object getAdapter(final Class key) {

		Object result = null;

		if (IContentOutlinePage.class.isAssignableFrom(key)) {
			final StructuredTextEditor sourceEditor = getSourceEditor();
			if (sourceEditor != null)
				result = sourceEditor.getAdapter(key);
		}

		if (result == null)
			result = super.getAdapter(key);
		return result;
	}

	private final class TracingResourceChangeListener implements IResourceChangeListener {
		@Override
		public void resourceChanged(final IResourceChangeEvent event) {

			final IResource resource = event.getResource();
			final Object source = event.getSource();
			final int type = event.getType();

			final IResourceDelta delta = event.getDelta();
			final int kind = delta.getKind();

			System.out.println(event + "\n " + "  resource: " + resource + "\n" + "  source:   " + source + "\n" + "  type: " //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$
					+ type + ", kind: " + kind + "\n" + "  delta:    "); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			System.out.println(ResourceDeltaPrinter.build(delta));
			new Exception("Resource changed event above triggered by: ").printStackTrace(System.out); //$NON-NLS-1$

		}
	}

	private final class SelectionSyncListener implements ISelectionChangedListener {
		@Override
		public void selectionChanged(final SelectionChangedEvent event) {
			// System.out.println(event);
			// System.out.println(event.getSelection());

			if (event.getSource() instanceof VexWidget) {

				if (event.getSelection() instanceof TextSelectionVex) {
					final TextSelectionVex sel = (TextSelectionVex) event.getSelection();

					getSourceEditor().selectAndReveal(sel.getSourceStartOffset(),
							sel.getSourceEndOffset() - sel.getSourceStartOffset());
				} else if (event.getSelection() instanceof IStructuredSelection) {
					if (!((VexWidget) event.getSource()).hasSelection())
						return; /*
								 * Vex sends selection events for elements
								 * whenever the cursor moves from one element to
								 * the other. We just select something in the
								 * text editor when the user actually selects
								 * something in Vex. For cursor movement, we
								 * listen to the TextSelections.
								 */
					final IStructuredSelection sel = (IStructuredSelection) event.getSelection();
					selectInSourcePage(event, sel);
				}
			}
		}
	}

	private final class OutlineSelectionListener implements ISelectionListener {

		@Override
		public void selectionChanged(final IWorkbenchPart part, final ISelection selection) {

			final IWorkbenchPartReference activePartReference = getSite().getWorkbenchWindow().getActivePage().getActivePartReference();
			if (getActiveEditor() instanceof VexEditorPage && activePartReference != null
					&& activePartReference.getId().equals("org.eclipse.ui.views.ContentOutline")) { //$NON-NLS-1$
				/*
				 * Somebody selected something somewhere else, e.g. in the
				 * Outline view, and the WYSIWYM page is the active page -> do
				 * perform the source selection stuff.
				 */
				if (selection instanceof IStructuredSelection) {
					selectInWysiwymPage((IStructuredSelection) selection, false);
				}
			}
		}

	}

	/**
	 * Listens for selection from the source page, applying it to the design
	 * viewer.
	 */
	private class TextEditorPostSelectionAdapter extends UIJob implements ISelectionChangedListener {
		boolean forcePostSelection = false;
		ISelection selection = null;

		public TextEditorPostSelectionAdapter() {
			super(getTitle());
			setUser(true);
		}

		@Override
		public IStatus runInUIThread(final IProgressMonitor monitor) {
			if (selection != null) {
				getDesignViewer().getSelectionProvider().setSelection(fixSelection(selection));
			}
			return Status.OK_STATUS;
		}

		@Override
		public void selectionChanged(final SelectionChangedEvent event) {
			if ((getDesignViewer() != null)
					&& ((getActivePage() != getDesignPageIndex()) || !MPXmlEditorPart.this.equals(getSite().getPage().getActivePart()))) {
				if (forcePostSelection) {
					selection = event.getSelection();
					schedule(200);
				} else {
					getDesignViewer().getSelectionProvider().setSelection(fixSelection(event.getSelection()));
				}
			}
		}

		/**
		 * The design viewer is not able to handle {@link IStructuredSelection}s
		 * containing {@link Text} nodes. Thus, if a {@link Text} node is
		 * contained, we return a selection with the {@link Text}'s parent
		 * instead (otherwise the original selection will be returned).
		 *
		 * This fixes TG-136.
		 *
		 */
		protected ISelection fixSelection(ISelection selection) {
			// System.out.print(MessageFormat.format(
			// "{0,time} Fixing {1} ",
			// new Date(), selection));
			if (selection instanceof IStructuredSelection) {
				final IStructuredSelection sel = (IStructuredSelection) selection;
				if (sel.getFirstElement() instanceof Text)
					selection = new StructuredSelection(((Text) sel.getFirstElement()).getParentNode());
			}
			// System.out.println(MessageFormat.format(" to {0}", selection));
			return selection;
		}

	}

	private void selectInWysiwymPage(final IStructuredSelection selection, final boolean justCursor) {
		if (selection.isEmpty())
			return;
		final IDOMNode firstNode = AdapterUtils.getAdapter(selection.getFirstElement(), IDOMNode.class);
		final IDOMNode lastNode = AdapterUtils.getAdapter(selection.toList().get(max(0, selection.size() - 1)), IDOMNode.class);

		if (firstNode != null && lastNode != null) {
			final VexEditorPage vex = getWysiwymEditor();
			if (justCursor)
				vex.gotoDOMNode(firstNode);
			else
				vex.selectDOMNodes(firstNode, lastNode);
		}
	}

	private void selectInSourcePage(final SelectionChangedEvent event, final IStructuredSelection sel) {
		final Object firstElement = sel.getFirstElement();
		Object lastElement = firstElement;
		if (sel.size() > 1)
			lastElement = sel.toList().get(sel.size() - 1);

		final IndexedRegion firstIR = AdapterUtils.getAdapter(firstElement, IndexedRegion.class);
		final IndexedRegion lastIR = AdapterUtils.getAdapter(lastElement, IndexedRegion.class);

		if (firstIR != null && lastIR != null) {
			final int startOffset = firstIR.getStartOffset();
			final int length = lastIR.getEndOffset() - startOffset;
			getSourceEditor().selectAndReveal(startOffset, length);
		} else {
			Activator.handleProblem(IStatus.INFO, new Exception(), "Could not get IndexedRegions from selection {0} on event {1}.", //$NON-NLS-1$
					sel, event);
		}
	}

	/**
	 * Selects the element specified by the given <var>id</var>.
	 *
	 * @return the region where we're at or <code>null</code>.
	 */
	public final Region selectByID(final String id) {
		final IModelManager modelManager = StructuredModelManager.getModelManager();
		final IDOMModel domModel;
		final IDocument document = getDocument();

		if (document instanceof IStructuredDocument) {

			final IStructuredDocument sdocument = (IStructuredDocument) document;
			final IStructuredModel modelForRead = modelManager.getModelForRead(sdocument);
			try {
				if (modelForRead instanceof IDOMModel) {
					domModel = (IDOMModel) modelForRead;
					final Element element = getElementByIdNS(domModel.getDocument(), id);
					final IndexedRegion indexedRegion = AdapterUtils.getAdapter(element, IndexedRegion.class);
					if (indexedRegion != null) {
						final Region region = new Region(indexedRegion.getStartOffset(), indexedRegion.getEndOffset()
								- indexedRegion.getStartOffset());
						sourceEditor.selectAndReveal(region.getOffset(), region.getLength());
						return region;
					}
				}
			} finally {
				if (modelForRead != null)
					modelForRead.releaseFromRead();
			}
		}
		return null;
	}

	/**
	 * Finds the first matching occurance of <var>text</var> within the source
	 * code of the element identified by <var>id</var>, selects and returns
	 * that.
	 *
	 * This method first uses {@link #selectByID(String)} to identify the source
	 * code region corresponding to the element identified by <var>id</var>,
	 * then it
	 * {@linkplain IFindReplaceTarget#findAndSelect(int, String, boolean, boolean, boolean)
	 * finds} the <var>text</var> within this region.
	 *
	 * @param id
	 *            XML ID of an element in which to search
	 * @param text
	 *            text to find within that element
	 * @return the region corresponding to the text
	 */
	public Region selectByIDandText(final String id, final String text) {
		final Region parentRegion = selectByID(id);
		if (parentRegion == null)
			return null;

		final IFindReplaceTarget find = getSourceEditor().getTextViewer().getFindReplaceTarget();
		((IFindReplaceTargetExtension) find).beginSession();
		try {
			((IFindReplaceTargetExtension) find).setScope(parentRegion);
			final int findAndSelect = find.findAndSelect(0, text, true, true, false);
			if (findAndSelect >= 0) {
				final Region region = new Region(findAndSelect, text.length());
				// Hack around TG-1970. Appearently there is no way
				// to listen for the projection model to finish.
				new SelectAndRevealJob(region).schedule(2000);
				return region;
			}
			return null;
		} finally {
			((IFindReplaceTargetExtension) find).endSession();
			// until we end the session, the scope will be highlighted.
		}
	}

	private class SelectAndRevealJob extends UIJob {

		private final Region region;

		public SelectAndRevealJob(final Region region) {
			super(NLS.bind("Re-revealing {0} ...", region));
			this.region = region;
		}

		@Override
		public IStatus runInUIThread(final IProgressMonitor monitor) {
			final StructuredTextViewer viewer = getSourceEditor().getTextViewer();
			if (viewer.getControl() == null || viewer.getControl().isDisposed())
				return Status.CANCEL_STATUS;
			final Point currentSelection = viewer.getSelectedRange();
			if (currentSelection.x == region.getOffset() && currentSelection.y == region.getLength()) {
				viewer.revealRange(region.getOffset(), region.getLength());
			} else {
				StatusManager.getManager().handle(new Status(IStatus.WARNING, Activator.PLUGIN_ID, NLS.bind("Not re-revealing {0} since selection has changed to {1}", region, currentSelection)));
			}
			return Status.OK_STATUS;
		}
	}

	private Element getElementByIdNS(final Document document, final String id) {
		if (id == null)
			return null;
		final NodeIterator it = ((DocumentTraversal) document).createNodeIterator(document, NodeFilter.SHOW_ALL, null, false);
		if (it == null)
			return null;

		for (Node node = it.nextNode(); node != null; node = it.nextNode()) {
			if (node.getNodeType() != Node.ELEMENT_NODE)
				continue;
			final Element element = (Element) node;
			if (element.hasAttribute("id") && id.equals(element.getAttribute("id")) || element.hasAttributeNS(XMLConstants.XML_NS_URI, "id") && id.equals(element.getAttributeNS(XMLConstants.XML_NS_URI, "id"))) //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
				return element;
		}

		return null;
	}

	public final class WysiwymMPESite extends MultiPageEditorSite {

		public WysiwymMPESite(final MultiPageEditorPart multiPageEditor, final IEditorPart editor) {
			super(multiPageEditor, editor);
		}

		@Override
		public IEditorActionBarContributor getActionBarContributor() {
			IEditorActionBarContributor contributor = super.getActionBarContributor();
			final IEditorActionBarContributor multiContributor = getEditorSite().getActionBarContributor();
			if (multiContributor instanceof MPXmlEditorActionBarContributor) {
				contributor = ((MPXmlEditorActionBarContributor) multiContributor).getWysiwymActionBarContributor();
			}
			return contributor;
		}
	}

	private VexEditorPage wysiwymEditor;
	private int fWysiwymEditorIndex;
	private StructuredTextEditor sourceEditor;
	private IDocument document;
	private TextEditorPostSelectionAdapter fTextEditorSelectionListener;
	private ReopenResourceChangeListener reopenListener;

	private int fSourceEditorIndex;
	private int fDesignEditorIndex;
	private int fPreviewViewIndex = -1;
	private PreviewPage previewPage;

	protected void createAndAddWysiwymPage(final StructuredTextEditor textEditor, final IDocument document2)
			throws PartInitException {
		final VexEditorPage vexEditor = new VexEditorPage(textEditor, document);
		wysiwymEditor = vexEditor;
		fWysiwymEditorIndex = addPage(vexEditor, getEditorInput());
		setPageText(fWysiwymEditorIndex, Messages.MPXmlEditorPart_1);
		
//		getHelpSystem().setHelp(vexEditor.getVexWidget(), "info.textgrid.lab.xmleditor.mpeditor.WysiwymPage");
		

		final SelectionSyncListener listener = new SelectionSyncListener();
		/*
		 * WARNING: Do not register this with
		 * getEditorSite().getSelectionProvider() or you will produce a fine
		 * endless loop whenever somebody selects something in the WYSIWYM page.
		 */
		wysiwymEditor.getEditorSite().getSelectionProvider().addSelectionChangedListener(listener);

		getSite().getWorkbenchWindow().getSelectionService().addPostSelectionListener("org.eclipse.ui.views.ContentOutline", //$NON-NLS-1$
				new OutlineSelectionListener());
	}

	private IWorkbenchHelpSystem getHelpSystem() {
		return Activator.getDefault().getWorkbench().getHelpSystem();
	}

	/**
	 * Return the source editor for this multi-page editor.
	 */
	public StructuredTextEditor getSourceEditor() {
		if (sourceEditor == null)
			fetchUpstreamEditors();

		return sourceEditor;
	}

	private void fetchUpstreamEditors() {
		for (int i = 0; i < getPageCount(); i++) {
			final IEditorPart editor = getEditor(i);
			if (editor instanceof StructuredTextEditor)
				sourceEditor = (StructuredTextEditor) editor;

		}
	}

	public int getWysiwymEditorIndex() {
		return fWysiwymEditorIndex;
	}

	@Override
	protected void createPages() {
		super.createPages();
		final StructuredTextEditor textEditor = getSourceEditor();

		fSourceEditorIndex = getSourcePageIndex();
		fDesignEditorIndex = getDesignPageIndex();

		document = (IDocument) (textEditor != null ? textEditor.getAdapter(IDocument.class) : null);

		try {
			createAndAddWysiwymPage(textEditor, document);
		} catch (final PartInitException e) {
			Activator.handleProblem(IStatus.ERROR, e, NLS.bind("Could not create WYSIWYM editor page for ", //$NON-NLS-1$
					getEditorInput()));
		}

		if (Activator.getDefault().getPreferenceStore().getBoolean(PreferenceConstants.PREVIEW_PAGE)) {
			previewPage = new PreviewPage(this.getContainer(), SWT.NONE);
			fPreviewViewIndex = addPage(previewPage);
			setPageText(fPreviewViewIndex, Messages.MPXmlEditorPart_PreviewPageLabel);
			getHelpSystem().setHelp(previewPage, "info.textgrid.lab.xmleditor.mpeditor.PreviewPage");
		}
	}

	/**
	 * @see org.eclipse.ui.part.MultiPageEditorPart#createSite(org.eclipse.ui.IEditorPart)
	 */
	@Override
	protected IEditorSite createSite(final IEditorPart editor) {
		IEditorSite site = null;
		if (editor == wysiwymEditor) {
			site = new WysiwymMPESite(this, editor);
		} else if (editor instanceof StructuredTextEditor) {
			site = new MultiPageEditorSite(this, editor) {
				/**
				 * @see org.eclipse.ui.part.MultiPageEditorSite#getActionBarContributor()
				 */
				@Override
				public IEditorActionBarContributor getActionBarContributor() {
					IEditorActionBarContributor contributor = super.getActionBarContributor();
					final IEditorActionBarContributor multiContributor = MPXmlEditorPart.this.getEditorSite().getActionBarContributor();
					if (multiContributor instanceof XMLMultiPageEditorActionBarContributor) {
						contributor = ((MPXmlEditorActionBarContributor) multiContributor).getSourceViewerActionContributor();
					}
					return contributor;
				}

				@Override
				public String getId() {
					// sets this id so nested editor is considered xml
					// source
					// page
					return "org.eclipse.core.runtime.xml"; // FIXME //$NON-NLS-1$
					// ContentTypeIdForXML.ContentTypeID_XML
					// +
					// ".source"; //$NON-NLS-1$;
				}
			};
		} else {
			site = super.createSite(editor);
		}
		return site;
	}

	@Override
	protected void pageChange(final int newPageIndex) {

		try {

			if (newPageIndex == fWysiwymEditorIndex && wysiwymEditor instanceof VexEditorPage) {
				final Point selection = getSourceEditor().getTextViewer().getSelectedRange();
				final VexEditorPage vexEditorPage = wysiwymEditor;
				vexEditorPage.selectSourceRegion(new Region(selection.x, selection.y));
				vexEditorPage.notifyActivate();
				getSite().getPage().showActionSet("net.sf.vex.editor.appearanceActions"); //$NON-NLS-1$
			} else
				getSite().getPage().hideActionSet("net.sf.vex.editor.appearanceActions"); //$NON-NLS-1$

			final IActionBars actBar = getEditorSite().getActionBars();
			final IToolBarManager toolbarMgr = actBar.getToolBarManager();
			// System.out.println(""+newPageIndex
			// +" : "+fWysiwymEditorIndex+" : "+fSourceEditorIndex);
			// //PlatformUI.getWorkbench().getDisplay().getFocusControl() +
			// " : "+toolbarMgr.getItems().length);
			// if(PlatformUI.getWorkbench().getDisplay().getFocusControl().toString().indexOf("VexWidget")
			// != -1 ){
			if (newPageIndex == fWysiwymEditorIndex) {
				if (toolbarMgr.getItems().length > 0) {
					for (int i = 0; i < toolbarMgr.getItems().length; i++) {
						// if(!toolbarMgr.getItems()[i].isVisible())
						toolbarMgr.getItems()[i].setVisible(true);
					}
				}
				toolbarMgr.update(true);

			}
			// else
			// if(PlatformUI.getWorkbench().getDisplay().getFocusControl().toString().indexOf("StyledText")
			// != -1){
			else if (newPageIndex == fSourceEditorIndex) {
				if (toolbarMgr.getItems().length > 0) {
					for (int i = 0; i < toolbarMgr.getItems().length; i++) {

						if (toolbarMgr.getItems()[i].getId() == "Fonts") { //$NON-NLS-1$
							toolbarMgr.getItems()[i].setVisible(false);
						}

						else if (toolbarMgr.getItems()[i].getId() == "Style") { //$NON-NLS-1$
							toolbarMgr.getItems()[i].setVisible(false);
						} else if (toolbarMgr.getItems()[i].getId() == "F_Add") { //$NON-NLS-1$
							toolbarMgr.getItems()[i].setVisible(true);
						} else if (toolbarMgr.getItems()[i].getId() == "F_Rem") { //$NON-NLS-1$
							toolbarMgr.getItems()[i].setVisible(true);
						} else {
							if (toolbarMgr.getItems()[i].getId() != "Fonts" && toolbarMgr.getItems()[i].getId() != "Style") //$NON-NLS-1$ //$NON-NLS-2$
								toolbarMgr.getItems()[i].setVisible(true);
						}

					}
					toolbarMgr.update(true);
				}

			}
			// else
			// if(PlatformUI.getWorkbench().getDisplay().getFocusControl().toString().indexOf("Composite")
			// != -1){
			else if (newPageIndex == fDesignEditorIndex) {
				if (toolbarMgr.getItems().length > 0) {
					for (int i = 0; i < toolbarMgr.getItems().length; i++) {
						if (toolbarMgr.getItems()[i].isVisible())
							toolbarMgr.getItems()[i].setVisible(false);
					}
					toolbarMgr.update(true);
				}

			} else 		if (newPageIndex == fPreviewViewIndex) {
				reloadPreview();
			}



			// else
			// if(PlatformUI.getWorkbench().getDisplay().getFocusControl().toString().indexOf("StyledText")
			// != -1){
			// if(toolbarMgr.getItems().length > 0){
			// for(int i=0; i<toolbarMgr.getItems().length; i++){
			// if(toolbarMgr.getItems()[i].getId()=="Style" &&
			// toolbarMgr.getItems()[i].isVisible()){
			// toolbarMgr.getItems()[i].setVisible(false);
			// }
			// else{
			// if(toolbarMgr.getItems()[i].getId()!="Style" &&
			// toolbarMgr.getItems()[i].getId()!="Fonts")
			// toolbarMgr.getItems()[i].setVisible(true);
			// }
			//
			// }
			// toolbarMgr.update(true);
			// }
			//
			// }

		} finally {
			super.pageChange(newPageIndex);
		}

	}

	public void reloadPreview() {
		previewPage.setDocument(sourceEditor.getDocumentProvider().getDocument(getEditorInput()));
	}

	@Override
	protected void setInputWithNotify(final IEditorInput input) {
		Assert.isLegal(input != null);
		setInput(input);
		firePropertyChange(PROP_INPUT);
	}

	@Override
	protected void setInput(final IEditorInput input) {
		if (input instanceof IFileEditorInput) {
			if (Activator.isDebugging(Activator.DEBUG_RESOURCE)) {
				final IResourceChangeListener resourceChangeListener = new TracingResourceChangeListener();
				ResourcesPlugin.getWorkspace().addResourceChangeListener(resourceChangeListener);
			}
			final IFile file = ((IFileEditorInput) input).getFile();
			final TextGridObject textGridObject = AdapterUtils.getAdapter(file, TextGridObject.class);
			if (textGridObject != null && textGridObject.isNew()) {
				reopenListener = new ReopenResourceChangeListener(file, textGridObject);
				file.getWorkspace().addResourceChangeListener(reopenListener);

				checkMaxSize(null, input, textGridObject);
			}
		}

		super.setInput(input);

		adjustPerspective();
	}

	private boolean checkMaxSize(final IEditorSite site, final IEditorInput input, final TextGridObject textGridObject) {
		try {
			if (textGridObject.getSize() > MAX_FILE_SIZE) {
				final int choice = new MessageDialog(
						site.getShell(),
						NLS.bind(Messages.MPXmlEditorPart_OpeningX, textGridObject),
						null,
						NLS.bind(
								Messages.MPXmlEditorPart_TooLargeWarning,
								textGridObject), MessageDialog.QUESTION,
						new String[] { Messages.MPXmlEditorPart_Cancel, Messages.MPXmlEditorPart_OpenInTextEditor, Messages.MPXmlEditorPart_Continue }, 1).open();
				final IWorkbenchPage page = site.getPage();
				final UIJob closeJob = new UIJob(Messages.MPXmlEditorPart_ClosingEditor) {

					@Override
					public IStatus runInUIThread(final IProgressMonitor monitor) {
						page.closeEditor(MPXmlEditorPart.this, false);
						return Status.OK_STATUS;
					}

				};
				switch (choice) {
				case SWT.DEFAULT:
				case 0:
					closeJob.schedule();
					return false;
				case 1:
					site.getPage().openEditor(new FileEditorInput(AdapterUtils.getAdapter(textGridObject, IFile.class)),
							"org.eclipse.ui.DefaultTextEditor"); //$NON-NLS-1$
					closeJob.schedule();
					return false;
				}

			}
		} catch (final CoreException e) {
			StatusManager.getManager().handle(e, Activator.PLUGIN_ID);
		}
		return true;
	}

	@Override
	public void init(final IEditorSite site, final IEditorInput input) throws PartInitException {
		super.init(site, input);
		if (input instanceof IFileEditorInput) {
			final TextGridObject object = AdapterUtils.getAdapter(((IFileEditorInput) input).getFile(), TextGridObject.class);
			if (object != null) {
				if (!checkMaxSize(site, input, object))
					throw new PartInitException(Messages.MPXmlEditorPart_WillBeClosed);
			}
		}


	}

	private void adjustPerspective() {
		final IWorkbenchPage page = getSite().getPage();
		if (page == null)
			return;
		final IPerspectiveDescriptor perspective = page.getPerspective();
		if (perspective == null)
			return;

		if ("info.textgrid.lab.welcome.XMLEditorPerspective".equals(perspective.getId())) { //$NON-NLS-1$
			try {
				page.showView("info.textgrid.lab.core.metadataeditor.view"/*, null, IWorkbenchPage.VIEW_VISIBLE*/); //$NON-NLS-1$
			} catch (final PartInitException e) {
				StatusManager.getManager().handle(e, Activator.PLUGIN_ID);
			}
		}

	}

	/**
	 *
	 */
	public MPXmlEditorPart() {
		// TODO Auto-generated constructor stub
	}

	public VexEditorPage getWysiwymEditor() {
		return wysiwymEditor;
	}

	@Override
	public void setActivePage(final int pageIndex) {
		super.setActivePage(pageIndex);
	}

	/**
	 * Connects the design viewer with the viewer selection manager. Should be
	 * done after createSourcePage() is done because we need to get the
	 * ViewerSelectionManager from the TextEditor. setModel is also done here
	 * because getModel() needs to reference the TextEditor.
	 */
	@Override
	protected void connectDesignPage() {
		if (getDesignViewer() != null) {
			getDesignViewer().setDocument(getDocument());
		}

		/*
		 * Connect selection from the Design page to the selection provider for
		 * the XMLMultiPageEditorPart so that selection changes in the Design
		 * page will propagate across the workbench
		 */
		if (getDesignViewer().getSelectionProvider() instanceof IPostSelectionProvider) {
			((IPostSelectionProvider) getDesignViewer().getSelectionProvider()).addPostSelectionChangedListener(new ISelectionChangedListener() {
				@Override
				public void selectionChanged(final SelectionChangedEvent event) {
					((MultiPageSelectionProvider) getSite().getSelectionProvider()).firePostSelectionChanged(event);
				}
			});
		}
		getDesignViewer().getSelectionProvider().addSelectionChangedListener(new ISelectionChangedListener() {
			@Override
			public void selectionChanged(final SelectionChangedEvent event) {
				((MultiPageSelectionProvider) getSite().getSelectionProvider()).fireSelectionChanged(event);
			}
		});

		/*
		 * Connect selection from the Design page to the selection provider of
		 * the Source page so that selection in the Design page will drive
		 * selection in the Source page. Prefer post selection.
		 */
		if (getDesignViewer().getSelectionProvider() instanceof IPostSelectionProvider) {
			((IPostSelectionProvider) getDesignViewer().getSelectionProvider()).addPostSelectionChangedListener(new ISelectionChangedListener() {
				@Override
				public void selectionChanged(final SelectionChangedEvent event) {
					/*
					 * Only force selection update if source page is not
					 * active
					 */
					if (getActivePage() != getSourcePageIndex()) {
						getTextEditor().getSelectionProvider().setSelection(event.getSelection());
					}
					if (getDesignViewer().equals(event.getSource())) {
						try {
							updateStatusLine(event.getSelection());
						} catch (final Exception exception) {
							Logger.logException(exception);
						}
					}
				}
			});
		} else {
			getDesignViewer().getSelectionProvider().addSelectionChangedListener(new ISelectionChangedListener() {
				@Override
				public void selectionChanged(final SelectionChangedEvent event) {
					/*
					 * Only force selection update if source
					 * page is not active
					 */
					if (getActivePage() != getSourcePageIndex()) {
						getTextEditor().getSelectionProvider().setSelection(event.getSelection());
					}
				}
			});
		}

		/*
		 * Handle double-click in the Design page by selecting the corresponding
		 * amount of text in the Source page.
		 */
		getDesignViewer().getControl().addListener(SWT.MouseDoubleClick, new Listener() {
			@Override
			public void handleEvent(final Event event) {
				final ISelection selection = getDesignViewer().getSelectionProvider().getSelection();
				int start = -1;
				int length = -1;
				if (selection instanceof IStructuredSelection) {
					/*
					 * selection goes from the start of the first object
					 * to the end of the last
					 */
					final IStructuredSelection structuredSelection = (IStructuredSelection) selection;
					final Object o = structuredSelection.getFirstElement();
					Object o2 = null;
					if (structuredSelection.size() > 1) {
						o2 = structuredSelection.toArray()[structuredSelection.size() - 1];
					} else {
						o2 = o;
					}
					if (o instanceof IndexedRegion) {
						start = ((IndexedRegion) o).getStartOffset();
						length = ((IndexedRegion) o2).getEndOffset() - start;
					} else if (o2 instanceof ITextRegion) {
						start = ((ITextRegion) o).getStart();
						length = ((ITextRegion) o2).getEnd() - start;
					}
				} else if (selection instanceof ITextSelection) {
					start = ((ITextSelection) selection).getOffset();
					length = ((ITextSelection) selection).getLength();
				}
				if ((start > -1) && (length > -1)) {
					getTextEditor().selectAndReveal(start, length);
				}
			}
		});

		/*
		 * Connect selection from the Source page to the selection provider of
		 * the Design page so that selection in the Source page will drive
		 * selection in the Design page. Prefer post selection.
		 */

		final ISelectionProvider provider = getTextEditor().getSelectionProvider();
		if (fTextEditorSelectionListener == null) {
			fTextEditorSelectionListener = new TextEditorPostSelectionAdapter();
		}
		if (provider instanceof IPostSelectionProvider) {
			fTextEditorSelectionListener.forcePostSelection = false;
			((IPostSelectionProvider) provider).addPostSelectionChangedListener(fTextEditorSelectionListener);
		} else {
			fTextEditorSelectionListener.forcePostSelection = true;
			provider.addSelectionChangedListener(fTextEditorSelectionListener);
		}

	}

	@Override
	public void dispose() {
		super.dispose();
		if (reopenListener != null)
			ResourcesPlugin.getWorkspace().removeResourceChangeListener(reopenListener);
	}

	public PreviewPage getPreviewPage() {
		return previewPage;
	}

}
