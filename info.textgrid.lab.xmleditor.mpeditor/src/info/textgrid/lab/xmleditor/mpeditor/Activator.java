package info.textgrid.lab.xmleditor.mpeditor;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.eclipse.ui.statushandlers.StatusManager;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle
 */
public class Activator extends AbstractUIPlugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "info.textgrid.lab.xmleditor.mpeditor"; //$NON-NLS-1$

	public static final String DEBUG_RESOLVE = PLUGIN_ID + "/debug/resolve"; //$NON-NLS-1$

	public static final String DEBUG_RESOURCE = PLUGIN_ID
			+ "/debug/resource-change"; //$NON-NLS-1$

	// The shared instance
	private static Activator plugin;
	
	/**
	 * The constructor
	 */
	public Activator() {
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
	 */
	@Override
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	@Override
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static Activator getDefault() {
		return plugin;
	}

	public static IStatus handleProblem(int severity, Throwable e,
			String message, Object... arguments) {
		return getDefault().internalHandleProblem(severity, e, message,
				arguments);
	}

	protected IStatus internalHandleProblem(int severity, Throwable cause,
			String message, Object... arguments) {
		if (message == null || message.length() == 0)
			message = cause.getLocalizedMessage();
		else
			message = NLS.bind(message, arguments);
		
		Status status = new Status(severity, PLUGIN_ID, message, cause);
		StatusManager.getManager().handle(
				status);
		return status;
		
	}

	public static boolean isDebugging(String option) {
		return /*
				 * getDefault().isDebugging() &&
				 */"true".equalsIgnoreCase(Platform.getDebugOption(option)); //$NON-NLS-1$
	}

}
