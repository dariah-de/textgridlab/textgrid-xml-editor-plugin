package info.textgrid.lab.xmleditor.mpeditor.links;

import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.swtutils.AdapterUtils;

import java.net.URI;
import java.net.URISyntaxException;
import java.text.MessageFormat;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IAdapterFactory;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.ui.statushandlers.StatusManager;
import org.eclipse.wst.xml.core.internal.provisional.document.IDOMDocument;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

/**
 * This adapter factory is able to convert a DOM node of a document in an XML
 * editor to
 * <ul>
 * <li>the {@link IFile} that the node is in,</li>
 * <li>the corresponding {@link TextGridObject}</li>
 * <li>or a complete URI with document and fragment for the node.</li>
 * </ul>
 * 
 * @author vitt
 * @see FragmentXPointer
 * @category text link editor
 * 
 */
@SuppressWarnings( { "unchecked", "restriction" })
// Interface doesn't use generics :-(
public class NodeToLinkAdapterFactory implements IAdapterFactory {

	private static final Class[] ADAPTER_LIST = new Class[] { Document.class, IFile.class, TextGridObject.class, URI.class };

	public Object getAdapter(Object adaptableObject, Class adapterType) {

		Node node = AdapterUtils.getAdapter(adaptableObject, Node.class);
		if (node != null) {

			Document document = getDocument(node);
			if (adapterType.isAssignableFrom(document.getClass()))
				return document;

			IFile file = getFile(document);
			if (adapterType.isAssignableFrom(IFile.class))
				return file;

			// remaining cases: TextGridObject and URI
			URI documentURI = null;
			if (file != null) {
				TextGridObject textGridObject = AdapterUtils.getAdapter(file, TextGridObject.class);

				if (adapterType.isAssignableFrom(TextGridObject.class))
					return textGridObject;

				if (textGridObject != null)
					documentURI = textGridObject.getURI();
				else
					documentURI = file.getLocationURI();
			}
			if (adapterType.isAssignableFrom(URI.class)) {
				String fragment = FragmentXPointer.of(node).toString();
				try {
					return new URI(documentURI == null ? "" : documentURI.toString() + fragment); //$NON-NLS-1$
				} catch (URISyntaxException e) {
					StatusManager.getManager().handle(
						new Status(IStatus.ERROR, info.textgrid.lab.xmleditor.mpeditor.Activator.PLUGIN_ID, MessageFormat.format(
								Messages.NodeToLinkAdapterFactory_ErrorAssemblingLink,
								documentURI, fragment, e.getMessage()), e));
				}
			}
		}
		return null;

	}

	/**
	 * Returns a file for the given document, if the document is managed by WST
	 * 
	 * @param document
	 *            the document to check
	 * @return the {@link IFile} or <code>null</code> if none could be
	 *         determined
	 */
	private IFile getFile(Document document) {
		IDOMDocument doc = AdapterUtils.getAdapter(document, IDOMDocument.class);
		if (doc != null) {
			String baseLocation = doc.getModel().getBaseLocation();
			return ResourcesPlugin.getWorkspace().getRoot().getFile(new Path(baseLocation));
		}
		return null;
	}

	private Document getDocument(Node node) {
		Document document;
		if (node instanceof Document)
			document = (Document) node;
		else
			document = node.getOwnerDocument();
		return document;
	}

	public Class[] getAdapterList() {
		return ADAPTER_LIST;
	}

}
