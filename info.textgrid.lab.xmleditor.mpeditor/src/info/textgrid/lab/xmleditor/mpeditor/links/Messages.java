package info.textgrid.lab.xmleditor.mpeditor.links;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "info.textgrid.lab.xmleditor.mpeditor.links.messages"; //$NON-NLS-1$
	public static String CopyNodeURIHandler_CouldNotExtractURI;
	public static String NodeToLinkAdapterFactory_ErrorAssemblingLink;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
