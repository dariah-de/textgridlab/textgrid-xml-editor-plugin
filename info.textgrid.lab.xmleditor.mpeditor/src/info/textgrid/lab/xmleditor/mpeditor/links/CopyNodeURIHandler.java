package info.textgrid.lab.xmleditor.mpeditor.links;

import info.textgrid.lab.core.swtutils.AdapterUtils;
import info.textgrid.lab.ui.core.Activator;

import java.net.URI;
import java.text.MessageFormat;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.statushandlers.StatusManager;
import org.w3c.dom.Node;

/**
 * A handler that should be enabled on {@link Node}s in TextGrid documents. Will
 * paste an URI to the node to the {@link Clipboard} with the document's
 * (TextGrid) URI and an XPointer to the corresponding element as a fragment.
 * 
 * @author vitt
 * @category text link editor
 * 
 */
@SuppressWarnings("restriction")
public class CopyNodeURIHandler extends AbstractHandler implements IHandler {

	public Object execute(ExecutionEvent event) throws ExecutionException {

		ISelection currentSelection = HandlerUtil.getCurrentSelection(event);
		if (currentSelection instanceof IStructuredSelection) {
			URI uri = AdapterUtils.getAdapter(((IStructuredSelection) currentSelection).getFirstElement(), URI.class);
			if (uri != null) {
				Clipboard clipboard = new Clipboard(HandlerUtil.getActiveShell(event).getDisplay());
				clipboard.setContents(new String[] { uri.toString() }, new Transfer[] { TextTransfer.getInstance() });
			} else {
				StatusManager.getManager().handle(
						new Status(IStatus.ERROR, Activator.PLUGIN_ID, MessageFormat.format(
								Messages.CopyNodeURIHandler_CouldNotExtractURI, currentSelection)));
			}
		}

		return null;
	}

}
