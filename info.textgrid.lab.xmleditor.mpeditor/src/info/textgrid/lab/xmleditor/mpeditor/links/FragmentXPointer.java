package info.textgrid.lab.xmleditor.mpeditor.links;

import java.text.MessageFormat;
import java.util.Collections;
import java.util.List;

import javax.xml.XMLConstants;

import org.w3c.dom.Attr;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.google.common.collect.Lists;

/**
 * Represents an XPointer to a fragment of the document.
 * 
 * @author vitt
 * @category text link editor
 */
public class FragmentXPointer {

	private static final String ATTRIBUTE_LOCALNAME_ID = "id"; //$NON-NLS-1$
	private Node node;

	protected FragmentXPointer(Node node) {
		this.node = node;
	}

	public static FragmentXPointer of(Node node) {
		return new FragmentXPointer(node);
	}

	public Node getNode() {
		return node;
	}

	@Override
	public String toString() {

		List<Node> pathFromId = getPathFromID();

		if (pathFromId.size() == 1)
			return "#" + getID(node); //$NON-NLS-1$

		StringBuilder result = new StringBuilder("#xpointer("); //$NON-NLS-1$
		if (getID(pathFromId.get(0)) != null)
			result.append("id('").append(getID(pathFromId.get(0))).append("')"); //$NON-NLS-1$ //$NON-NLS-2$
		
		for (Node pathElement : pathFromId.subList(1, pathFromId.size())) {
			result.append('/');
			result.append(pathElement.getNodeName());
			result.append('[').append(getNamedPosition(pathElement)).append(']');
		}
		
		result.append(")"); //$NON-NLS-1$
		return result.toString();
	}

	/**
	 * @return a list of nodes along the ancestors-or-self axis, starting with
	 *         the nearest ancestor that has an id attribute, ending with the
	 *         current node.
	 */
	private List<Node> getPathFromID() {
		Node currentNode = node;
		List<Node> pathToId = Lists.newLinkedList();

		while (getID(currentNode) == null && currentNode.getParentNode() != null) {
			pathToId.add(currentNode);
			currentNode = currentNode.getParentNode();
		}
		pathToId.add(currentNode);

		List<Node> pathFromId = Lists.newLinkedList(pathToId);
		Collections.reverse(pathFromId);
		return pathFromId;
	}

	/**
	 * Returns the position of the given <var>element</var> within those
	 * siblings that share <var>element</var>'s name.
	 */
	public static int getNamedPosition(Node pathElement) {
		assert (pathElement.getParentNode() != null);
		
		int pos = 0;
		NodeList childNodes = pathElement.getParentNode().getChildNodes();
		for (int i = 0; i < childNodes.getLength(); i++) {
			if (childNodes.item(i).getNodeName().equals(pathElement.getNodeName())) {
				pos++;
				if (childNodes.item(i) == pathElement)
					return pos;
			}
		}

		throw new IllegalStateException(MessageFormat.format(
				"The element {0} wasn't found in the list of its parent node's ({1}) children", pathElement, pathElement //$NON-NLS-1$
						.getParentNode()));
	}

	/**
	 * Returns the id of the given {@link Node}, or <code>null</code> if there
	 * isn't any ID associated with it.
	 */
	private static String getID(Node node) {
		if (node instanceof Attr)
			node = ((Attr) node).getOwnerElement();
		NamedNodeMap attributes = node.getAttributes();
		if (attributes != null) {
			Attr idAttribute = (Attr) attributes.getNamedItemNS(XMLConstants.XML_NS_URI, ATTRIBUTE_LOCALNAME_ID);
			if (idAttribute == null)
				idAttribute = (Attr) attributes.getNamedItem(ATTRIBUTE_LOCALNAME_ID);
			if (idAttribute == null)
				idAttribute = (Attr) attributes.getNamedItem("xml:id"); //$NON-NLS-1$
			if (idAttribute != null)
				return idAttribute.getValue();
		}
		return null;
	}

}
