package info.textgrid.lab.xmleditor.mpeditor;

import java.text.MessageFormat;

import info.textgrid.lab.core.model.Activator;
import info.textgrid.lab.core.model.TextGridObject;
import org.eclipse.core.expressions.PropertyTester;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;

public class IsNoImagePropertyTester extends PropertyTester {

	final String IMAGE = "image/"; //$NON-NLS-1$
	
	public IsNoImagePropertyTester() {
		// TODO Auto-generated constructor stub
	}

	public boolean test(Object receiver, String property, Object[] args,
			Object expectedValue) {
		if(receiver != null && receiver instanceof TextGridObject) {
			try {
				if (((TextGridObject)receiver).getContentTypeID().contains(IMAGE))
					return false;
				else 
					return true;
			} catch (Exception e) {
				IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID,
						MessageFormat.format(
								Messages.IsNoImagePropertyTester_CouldNotRetrieveContentType,
								((TextGridObject)receiver).getURI().toString()), e);
				Activator.getDefault().getLog().log(status);
			}
		}
		return true;
	}

}
