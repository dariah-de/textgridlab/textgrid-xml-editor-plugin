/**
 * 
 */
package info.textgrid.lab.xmleditor.mpeditor;

import net.sf.vex.editor.VexEditor;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IAdapterFactory;
import org.eclipse.jface.text.source.ISourceViewer;

/**
 * This adaptor factory helps converting between the multi part editor and its
 * various classes, especially Vex.
 * 
 * @author tv
 * @see IAdaptorFactory
 * @see IAdaptable
 */
public class AdapterFactory implements IAdapterFactory {
	
	@SuppressWarnings("unchecked")
	private Class[] adapterList = new Class[] { VexEditor.class, MPXmlEditorPart.class };

	/* (non-Javadoc)
	 * @see org.eclipse.core.runtime.IAdapterFactory#getAdapter(java.lang.Object, java.lang.Class)
	 */
	@SuppressWarnings("unchecked")
	public Object getAdapter(Object adaptableObject, Class adapterType) {
		if (adaptableObject instanceof MPXmlEditorPart) {
			if (adapterType == VexEditor.class) 
				return ((MPXmlEditorPart) adaptableObject).getWysiwymEditor();
			else if (adapterType == MPXmlEditorPart.class)
				return adaptableObject;
			else if (adapterType == ISourceViewer.class)
				return ((MPXmlEditorPart) adaptableObject).getSourceEditor()
						.getTextViewer();
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.core.runtime.IAdapterFactory#getAdapterList()
	 */
	@SuppressWarnings("unchecked")
	public Class[] getAdapterList() {
		// TODO Auto-generated method stub
		return adapterList;
	}

}
