package info.textgrid.lab.xmleditor.mpeditor.actions;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.handlers.HandlerUtil;

import info.textgrid.lab.xmleditor.mpeditor.MPXmlEditorPart;
import info.textgrid.lab.xmleditor.mpeditor.PreviewPage;

public class ReloadPreviewStylesheetsHandler extends AbstractHandler implements IHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		IEditorPart editor = HandlerUtil.getActiveEditorChecked(event);
		if (editor instanceof MPXmlEditorPart) {
			MPXmlEditorPart xmlEditor = (MPXmlEditorPart) editor;
			PreviewPage.getStylesheetManager().clear();
			xmlEditor.reloadPreview();
		}
		
		return null;
	}

}
