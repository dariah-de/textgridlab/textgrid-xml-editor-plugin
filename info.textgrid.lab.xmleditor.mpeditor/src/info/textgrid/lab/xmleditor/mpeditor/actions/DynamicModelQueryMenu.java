package info.textgrid.lab.xmleditor.mpeditor.actions;

import static info.textgrid.lab.core.swtutils.AdapterUtils.getAdapter;
import info.textgrid.lab.xmleditor.mpeditor.MPXmlEditorPart;
import net.sf.vex.editor.VexEditor;
import net.sf.vex.swt.VexWidget;

import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.Region;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.actions.CompoundContributionItem;
import org.eclipse.wst.sse.core.internal.provisional.IndexedRegion;
import org.eclipse.wst.sse.ui.StructuredTextEditor;
import org.eclipse.wst.sse.ui.internal.StructuredTextViewer;
import org.eclipse.wst.sse.ui.internal.contentassist.ContentAssistUtils;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

@SuppressWarnings("restriction")
public abstract class DynamicModelQueryMenu extends CompoundContributionItem {

	public DynamicModelQueryMenu() {
		super();
	}

	public DynamicModelQueryMenu(String id) {
		super(id);
	}

	protected IEditorPart getActiveEditor() {
		IWorkbench workbench = PlatformUI.getWorkbench();
		if (workbench != null) {
			IWorkbenchWindow activeWorkbenchWindow = workbench
					.getActiveWorkbenchWindow();
			if (activeWorkbenchWindow != null) {
				IWorkbenchPage activePage = activeWorkbenchWindow
						.getActivePage();
				if (activePage != null) {
					return activePage.getActiveEditor();
				}
			}
		}
		return null;
	}
	
	protected static StructuredTextEditor getSourceEditor(
			IEditorPart activeEditor) {
		if (activeEditor instanceof MPXmlEditorPart) {
			MPXmlEditorPart mpEditor = (MPXmlEditorPart) activeEditor;
			return mpEditor.getSourceEditor();
		} else
			return null;
	}
	
	protected static StructuredTextViewer getSourceViewer(IEditorPart editor) {
		StructuredTextEditor sourceEditor = getSourceEditor(editor);
		if (sourceEditor == null)
			return null;
		else
			return sourceEditor.getTextViewer();
	}
	
	
	protected static VexWidget getVexWidget(IEditorPart activeEditor) {
		VexEditor vex = getAdapter(activeEditor, VexEditor.class);
		return vex == null ? null : vex.getVexWidget();
	}

	protected static Element getCurrentElementFromMPEditor(
			IEditorPart activeEditor) {
		Element element = null;
		StructuredTextEditor sourceEditor = null;
		if (activeEditor instanceof MPXmlEditorPart) {
			MPXmlEditorPart mpEditor = (MPXmlEditorPart) activeEditor;
			sourceEditor = mpEditor.getSourceEditor();
			StructuredTextViewer textViewer = sourceEditor.getTextViewer();
			IRegion highlightRange = sourceEditor.getHighlightRange();

			if (highlightRange == null)
				highlightRange = new Region(0, 0); // This always occurs when
			// first opening the editor. Work around TG-456. But
													// can we use IDocument
													// here?

			IndexedRegion region = ContentAssistUtils.getNodeAt(textViewer,
					highlightRange.getOffset());
			Node node = getAdapter(region, Node.class);
			if (node == null)
				return null;
			if (node instanceof Element)
				element = (Element) node;
			else {
				Node parentNode = node.getParentNode();
				if (parentNode != null && parentNode instanceof Element)
					element = (Element) parentNode;
			}
		}
		return element;
	}

}