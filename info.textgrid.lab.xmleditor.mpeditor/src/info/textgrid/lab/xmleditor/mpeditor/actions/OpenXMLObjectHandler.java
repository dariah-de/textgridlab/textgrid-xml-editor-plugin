package info.textgrid.lab.xmleditor.mpeditor.actions;

import info.textgrid.lab.core.model.CrudServiceException;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.swtutils.AdapterUtils;
import info.textgrid.lab.search.FullTextEntry;
import info.textgrid.lab.ui.core.Activator;
import info.textgrid.lab.ui.core.menus.OpenObjectService;

import java.net.URI;
import java.net.URISyntaxException;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.Command;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.commands.IParameter;
import org.eclipse.core.commands.NotEnabledException;
import org.eclipse.core.commands.NotHandledException;
import org.eclipse.core.commands.Parameterization;
import org.eclipse.core.commands.ParameterizedCommand;
import org.eclipse.core.commands.common.CommandException;
import org.eclipse.core.commands.common.NotDefinedException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.WorkbenchException;
import org.eclipse.ui.commands.ICommandService;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.handlers.IHandlerService;
import org.eclipse.ui.statushandlers.StatusManager;

public class OpenXMLObjectHandler extends AbstractHandler implements IHandler {
	private static final String PARAMETER_URI_ID = "info.textgrid.lab.xmleditor.mpeditor.openxmlobject.parameter.uri"; //$NON-NLS-1$
	private static final String PARAMETER_READONLY_ID = "info.textgrid.lab.xmleditor.mpeditor.openxmlobject.parameter.readonly"; //$NON-NLS-1$

	public Object execute(ExecutionEvent event) throws ExecutionException {
		String parameterURI = null;
		String parameterReadOnly = null;
		TextGridObject object = null; 
		final IHandlerService handlerService = (IHandlerService) PlatformUI.getWorkbench().getService(IHandlerService.class);
		ISelection selection = null;
		IStructuredSelection sel = null;
		
		//get parameter
		parameterURI = event.getParameter(PARAMETER_URI_ID);
		parameterReadOnly = event.getParameter(PARAMETER_READONLY_ID);
		
		//get TextGrid object
		if (parameterURI != null) {
			
			try {
				object = TextGridObject.getInstance(new URI(parameterURI), false);
				
				selection = HandlerUtil.getCurrentSelection(event);
				if (selection instanceof IStructuredSelection) {
					sel = (IStructuredSelection) selection;
				}
			} catch (CrudServiceException e) {
				StatusManager.getManager().handle(
						new Status(IStatus.ERROR, Activator.PLUGIN_ID, Messages.OpenXMLObjectHandler_CouldNotRetrieveTGO, e));
			} catch (URISyntaxException e) {
				StatusManager.getManager().handle(
						new Status(IStatus.ERROR, Activator.PLUGIN_ID, Messages.OpenXMLObjectHandler_CouldNotRetrieveTGO, e));
			}
		} /*else {
			selection = HandlerUtil.getCurrentSelection(event);
			if (selection instanceof IStructuredSelection) {
				sel = (IStructuredSelection) selection;
				Object obj = ((IStructuredSelection) selection).getFirstElement();
				object = AdapterUtils.getAdapter(obj, TextGridObject.class);
			}	
		}*/
		
		
		if (object != null) {
			// if the linkeditor perspective active, then add
			// this xml-object to the linkEditor
			if ("info.textgrid.lab.linkeditor.rcp_linkeditor.perspective" //$NON-NLS-1$
					.equals(PlatformUI.getWorkbench()
							.getActiveWorkbenchWindow()
							.getActivePage().getPerspective()
							.getId())) {
				try {
					handlerService.executeCommand("info.textgrid.lab.linkeditor.rcp_linkeditor.defaultOpen", null); //$NON-NLS-1$
				} catch (CommandException e) {
					StatusManager.getManager().handle(
							new Status(IStatus.ERROR, Activator.PLUGIN_ID, Messages.OpenXMLObjectHandler_CouldNotOpenLinkeditor, e));
				}
				return null;
			}
	
			// if the ttle perspective active, then add
			// this xml-object to the linkEditor
			if ("info.textgrid.lab.ttle.perspective" //$NON-NLS-1$
					.equals(PlatformUI.getWorkbench()
							.getActiveWorkbenchWindow()
							.getActivePage().getPerspective()
							.getId())) {
				try {
					handlerService.executeCommand("info.textgrid.lab.ttle.defaultOpen", null); //$NON-NLS-1$
				} catch (CommandException e) {
					StatusManager.getManager().handle(
							new Status(IStatus.ERROR, Activator.PLUGIN_ID, Messages.OpenXMLObjectHandler_CouldNotOpenTTLE, e));
				}
				return null;
			}
			
			try {
	
				PlatformUI
						.getWorkbench()
						.showPerspective(
								"info.textgrid.lab.welcome.XMLEditorPerspective", //$NON-NLS-1$
								PlatformUI.getWorkbench()
										.getActiveWorkbenchWindow());
			} catch (WorkbenchException e) {
				IStatus status = new Status(IStatus.ERROR,
						Activator.PLUGIN_ID,
						Messages.OpenXMLObjectHandler_CouldNotOpenPerspective, e);
				Activator.getDefault().getLog().log(status);
			}
			
			OpenObjectService.getInstance().openObject(object, 2, Boolean.parseBoolean(parameterReadOnly));
			
			// KWIC
			if (sel != null) {
				if (sel.getFirstElement() instanceof FullTextEntry) {
					callSelectByIDCommand((FullTextEntry) sel.getFirstElement());
				}
			}	
		}
			
		return null;
	}
	
	/**
	 * Calls the command to position the content of
	 * the XML editor to the given search entry. 
	 * @param entry
	 * 			represents a search hit
	 */
	private static void callSelectByIDCommand(FullTextEntry entry) {
		IHandlerService handlerService = (IHandlerService) PlatformUI
		.getWorkbench().getService(IHandlerService.class);
		
		ICommandService commandService = (ICommandService) PlatformUI.getWorkbench()
		.getActiveWorkbenchWindow().getService(ICommandService.class);
		
		Command command = commandService.getCommand("info.textgrid.lab.xmleditor.mpeditor.selectByID"); //$NON-NLS-1$
		
		IParameter parameterID;
		IParameter parameterSearchTerm;
		try {
			parameterID = command.getParameter("info.textgrid.lab.xmleditor.mpeditor.selectByID.ID"); //$NON-NLS-1$
			Parameterization parameter1 = new Parameterization(parameterID, entry.getXpath());
			parameterSearchTerm = command.getParameter("info.textgrid.lab.xmleditor.mpeditor.selectByID.SearchTerm"); //$NON-NLS-1$
			Parameterization parameter2 = new Parameterization(parameterSearchTerm, entry.getMatch());
	 		ParameterizedCommand parmCommand = new ParameterizedCommand(
	 				command, new Parameterization[] { parameter1, parameter2 });
	 		handlerService.executeCommand(
	 				parmCommand,
					null);
		} catch (NotDefinedException e) {
			Activator.handleError(e, Messages.OpenXMLObjectHandler_CouldNotSelectX, entry);
		} catch (ExecutionException e) {
			Activator.handleError(e, Messages.OpenXMLObjectHandler_CouldNotSelectX, entry);
		} catch (NotEnabledException e) {
			Activator.handleError(e, Messages.OpenXMLObjectHandler_CouldNotSelectX, entry);
		} catch (NotHandledException e) {
			Activator.handleError(e, Messages.OpenXMLObjectHandler_CouldNotSelectX, entry);
		}
	}

	
}
