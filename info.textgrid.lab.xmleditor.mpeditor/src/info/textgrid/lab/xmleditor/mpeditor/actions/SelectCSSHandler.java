package info.textgrid.lab.xmleditor.mpeditor.actions;

import info.textgrid.lab.xmleditor.dialogs.AssociateStylesheetDialog;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.handlers.HandlerUtil;

public class SelectCSSHandler extends AbstractHandler implements IHandler {

	public Object execute(ExecutionEvent event) throws ExecutionException {
		IEditorPart editor = HandlerUtil.getActiveEditor(event);
		Shell shell = HandlerUtil.getActiveShell(event);
		AssociateStylesheetDialog dialog = new AssociateStylesheetDialog(shell, editor);
		dialog.open();
		return null;
	}

}
