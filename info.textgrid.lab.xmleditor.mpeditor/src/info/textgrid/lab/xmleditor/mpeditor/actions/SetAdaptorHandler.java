package info.textgrid.lab.xmleditor.mpeditor.actions;

import static info.textgrid.lab.core.swtutils.AdapterUtils.getAdapter;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.swtutils.AdapterUtils;
import info.textgrid.lab.xmleditor.dialogs.SetAdaptorDialog;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.handlers.HandlerUtil;

public class SetAdaptorHandler extends AbstractHandler implements IHandler {

	public Object execute(ExecutionEvent event) throws ExecutionException {

		StringBuilder errorMsg = new StringBuilder();

		ISelection currentSelection = HandlerUtil.getCurrentSelection(event);
		if (currentSelection != null
				&& currentSelection instanceof IStructuredSelection) {
			IStructuredSelection selection = (IStructuredSelection) currentSelection;
			
			TextGridObject[] textGridObjects = AdapterUtils.getAdapters(
					selection.toArray(), TextGridObject.class, false);

			if (textGridObjects.length > 0) {

				SetAdaptorDialog dialog = new SetAdaptorDialog(HandlerUtil
						.getActiveShell(event), true, textGridObjects);
				dialog.open();
				return null;
			} else {
				errorMsg
						.append(Messages.SetAdaptorHandler_NoTGOInSelection);
			}
		} else {
			errorMsg.append(Messages.SetAdaptorHandler_NoTGOSelected);
		}

		IEditorPart editor = HandlerUtil.getActiveEditor(event);
		if (editor != null) {
			IEditorInput input = editor.getEditorInput();
			TextGridObject textGridObject = getAdapter(input,
					TextGridObject.class);
			if (textGridObject != null) {
				new SetAdaptorDialog(HandlerUtil.getActiveShell(event), false, textGridObject).open();
				return null;
			} else {
				errorMsg
						.append(Messages.SetAdaptorHandler_EditingNonTGO);
			}
		}

		errorMsg.append(Messages.SetAdaptorHandler_NothingToAssociate);
		MessageDialog.openError(HandlerUtil.getActiveShell(event),
				Messages.SetAdaptorHandler_SetAdapterTitle, errorMsg.toString());
		return null;
	}
}
