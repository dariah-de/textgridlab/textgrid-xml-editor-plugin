package info.textgrid.lab.xmleditor.mpeditor.actions;

import java.util.Arrays;
import java.util.Comparator;

import net.sf.vex.action.linked.AbstractModelQueryActionWrapper;
import net.sf.vex.action.linked.InsertAssistant;
import net.sf.vex.dom.linked.LinkedElement;
import net.sf.vex.swt.VexWidget;

import org.eclipse.jface.action.ActionContributionItem;
import org.eclipse.jface.action.IContributionItem;
import org.eclipse.ui.IEditorPart;
import org.w3c.dom.Element;

public class InsertElementsSubmenu extends DynamicModelQueryMenu {
	
	
	public InsertElementsSubmenu() {
		super();		
	}

	public InsertElementsSubmenu(String id) {
		super(id);
	}
	
	@Override
	protected IContributionItem[] getContributionItems() {
		
		ActionContributionItem[] result = new ActionContributionItem[0];
		
		IEditorPart activeEditor = getActiveEditor();
		VexWidget vexWidget = getVexWidget(activeEditor);
		Element element = getCurrentElementFromMPEditor(activeEditor);
		
		if (element == null && vexWidget != null
				&& (vexWidget.getCurrentElement() instanceof LinkedElement))
			element = ((LinkedElement) vexWidget.getCurrentElement()).getDomNode();
		
		    //System.out.println(element.getLocalName());
		

		if (element == null) {
			return new IContributionItem[0];
			// AbstractModelQueryActionWrapper[] insertActions =
			// InsertAssistant.getInsertActions(vexWidget, null, element); //
			// Leads to NPE in InsertAssistant:65
			// result = new IContributionItem[insertActions.length];
			//			
			// result[0] = new ActionContributionItem(insertActions[0]);
			//			
			//			
			// return result; // TODO add more flexibility
		}

		AbstractModelQueryActionWrapper[] insertActions = InsertAssistant.getInsertActions(vexWidget,
				getSourceViewer(activeEditor), element);
		result = new ActionContributionItem[insertActions.length];
		
		

		for (int i = 0; i < insertActions.length; i++) {
			
			//System.out.println(insertActions[i].getText());
			
			result[i] = new ActionContributionItem(insertActions[i]);
		}
        
		Arrays.sort(result, new Comparator<ActionContributionItem>() {
			public int compare(ActionContributionItem o1, ActionContributionItem o2) {
				return o1.getAction().getText().compareToIgnoreCase(o2.getAction().getText());
			}

		});
		
		
		return result;
	}
}
