package info.textgrid.lab.xmleditor.mpeditor.actions;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import net.sf.vex.action.linked.AbstractModelQueryActionWrapper;

import org.eclipse.jface.action.ActionContributionItem;
import org.eclipse.jface.action.IContributionItem;
import org.eclipse.ui.IEditorPart;
import org.eclipse.wst.xml.core.internal.contentmodel.CMElementDeclaration;
import org.eclipse.wst.xml.core.internal.contentmodel.modelquery.ModelQuery;
import org.eclipse.wst.xml.core.internal.contentmodel.modelquery.ModelQueryAction;
import org.eclipse.wst.xml.core.internal.modelquery.ModelQueryUtil;
import org.eclipse.wst.xml.core.internal.provisional.document.IDOMElement;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

@SuppressWarnings("restriction")
public class RenameElementsSubmenu extends DynamicModelQueryMenu {

	public RenameElementsSubmenu() {
	}

	public RenameElementsSubmenu(String id) {
		super(id);
	}

	@Override
	protected IContributionItem[] getContributionItems() {
		IContributionItem[] result = new IContributionItem[0];
		IEditorPart activeEditor = getActiveEditor();
		Element element = getCurrentElementFromMPEditor(activeEditor);
		
		if (element == null)
			return result;
		
		Node parentNode = element.getParentNode();
		if (!(parentNode instanceof IDOMElement))
			return result;
		IDOMElement parent = (IDOMElement) parentNode;

		ModelQuery modelQuery = ModelQueryUtil.getModelQuery(parent
				.getOwnerDocument());
		CMElementDeclaration parentDeclaration = modelQuery
				.getCMElementDeclaration(parent);
		List<ModelQueryAction> actionList = new LinkedList<ModelQueryAction>();
		List childList =  Arrays.asList(new Element[] { element });

		if (parent == null || parentDeclaration == null) // TG-1241 workaround
			return new ActionContributionItem[0];

		// no - that returns the children
		modelQuery.getReplaceActions(parent, parentDeclaration, childList,
				ModelQuery.INCLUDE_ENCLOSING_REPLACE_ACTIONS,
				ModelQuery.VALIDITY_PARTIAL,
				actionList);
		
		AbstractModelQueryActionWrapper[] actions = AbstractModelQueryActionWrapper
				.create(actionList, getVexWidget(activeEditor),
				getSourceViewer(activeEditor), element);
		
		result = new IContributionItem[actions.length];
		for (int i = 0; i < actions.length; i++) {
			result[i] = new ActionContributionItem(actions[i]);
		}

		return result;
	}

}
