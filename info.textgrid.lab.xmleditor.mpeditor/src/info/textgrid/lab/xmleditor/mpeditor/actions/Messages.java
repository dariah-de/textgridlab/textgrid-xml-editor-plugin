package info.textgrid.lab.xmleditor.mpeditor.actions;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "info.textgrid.lab.xmleditor.mpeditor.actions.messages"; //$NON-NLS-1$
	public static String OpenXMLObjectHandler_CouldNotOpenLinkeditor;
	public static String OpenXMLObjectHandler_CouldNotOpenPerspective;
	public static String OpenXMLObjectHandler_CouldNotOpenTTLE;
	public static String OpenXMLObjectHandler_CouldNotRetrieveTGO;
	public static String OpenXMLObjectHandler_CouldNotSelectX;
	public static String SelectByIDHandler_EnterID;
	public static String SelectByIDHandler_SelectByID;
	public static String SetAdaptorHandler_EditingNonTGO;
	public static String SetAdaptorHandler_NoTGOInSelection;
	public static String SetAdaptorHandler_NoTGOSelected;
	public static String SetAdaptorHandler_NothingToAssociate;
	public static String SetAdaptorHandler_SetAdapterTitle;
	public static String ShowValidationErrorsHandler_CouldNotDisplayValidationView;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
