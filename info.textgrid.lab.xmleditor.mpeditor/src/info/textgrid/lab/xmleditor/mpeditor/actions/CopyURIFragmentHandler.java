package info.textgrid.lab.xmleditor.mpeditor.actions;

import info.textgrid.lab.core.swtutils.AdapterUtils;
import info.textgrid.lab.xmleditor.mpeditor.links.FragmentXPointer;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.ui.handlers.HandlerUtil;
import org.w3c.dom.Node;

public class CopyURIFragmentHandler extends AbstractHandler implements IHandler {

	public Object execute(ExecutionEvent event) throws ExecutionException {
		ISelection currentSelection = HandlerUtil.getCurrentSelection(event);
		if (currentSelection instanceof IStructuredSelection) {
			Node node = AdapterUtils.getAdapter(((IStructuredSelection) currentSelection).getFirstElement(), Node.class);
			// TODO implement ranges
			String fragment = FragmentXPointer.of(node).toString();

			Clipboard clipboard = new Clipboard(HandlerUtil.getActiveShellChecked(event).getDisplay());
			clipboard.setContents(new String[] { fragment }, new TextTransfer[] { TextTransfer.getInstance() });
		}
		return null;
	}

}
