package info.textgrid.lab.xmleditor.mpeditor.actions;

import info.textgrid.lab.xmleditor.mpeditor.views.ValidationErrorsView;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.handlers.HandlerUtil;

public class ShowValidationErrorsHandler extends AbstractHandler implements IHandler {

	public Object execute(ExecutionEvent event) throws ExecutionException {
		try {
			HandlerUtil.getActiveWorkbenchWindowChecked(event).getActivePage().showView(ValidationErrorsView.ID);
		} catch (PartInitException e) {
			throw new ExecutionException(Messages.ShowValidationErrorsHandler_CouldNotDisplayValidationView, e);
		}
		return null;
	}

}
