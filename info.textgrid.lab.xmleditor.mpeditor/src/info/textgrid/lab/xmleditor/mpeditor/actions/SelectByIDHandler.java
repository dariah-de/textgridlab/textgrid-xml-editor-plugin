package info.textgrid.lab.xmleditor.mpeditor.actions;

import info.textgrid.lab.xmleditor.mpeditor.MPXmlEditorPart;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.handlers.HandlerUtil;

public class SelectByIDHandler extends AbstractHandler implements IHandler {

	public Object execute(ExecutionEvent event) throws ExecutionException {
		Object ID = event.getParameters().get("info.textgrid.lab.xmleditor.mpeditor.selectByID.ID"); //$NON-NLS-1$
		Object searchTerm = event.getParameters().get("info.textgrid.lab.xmleditor.mpeditor.selectByID.SearchTerm"); //$NON-NLS-1$
		IEditorPart editor = HandlerUtil.getActiveEditor(event);
		
		if (ID != null && searchTerm != null) {
			if (editor instanceof MPXmlEditorPart) {
				((MPXmlEditorPart) editor).selectByIDandText(ID.toString(), searchTerm.toString());
			}
		} else {
			if (ID != null) {	/*TG-2045*/
				InputDialog dialog = new InputDialog(
						HandlerUtil.getActiveShell(event),
						Messages.SelectByIDHandler_SelectByID,
						Messages.SelectByIDHandler_EnterID, null, null);
				int open = dialog.open();
				if (open == Dialog.OK) {
					String input = dialog.getValue().trim();
					String[] split = input.split(" ", 2); //$NON-NLS-1$
					if (editor instanceof MPXmlEditorPart) {
						if (split.length > 1)
							((MPXmlEditorPart) editor).selectByIDandText(
									split[0], split[1]);
						else
							((MPXmlEditorPart) editor).selectByID(input);
					}
				}
			}
		}	
		return null;
	}

}
