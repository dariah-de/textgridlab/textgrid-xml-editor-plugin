package info.textgrid.lab.xmleditor.mpeditor.preferences;

/**
 * Constant definitions for plug-in preferences
 */
public class PreferenceConstants {

	public static final String PREVIEW_PAGE = "previewPage"; //$NON-NLS-1$
	public static final String PREVIEW_DEFAULT_URL = "defaultPreviewXSLT"; //$NON-NLS-1$

}
