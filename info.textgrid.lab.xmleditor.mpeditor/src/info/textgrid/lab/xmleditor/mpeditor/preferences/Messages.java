package info.textgrid.lab.xmleditor.mpeditor.preferences;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "info.textgrid.lab.xmleditor.mpeditor.preferences.messages"; //$NON-NLS-1$
	public static String ExperimentalXMLPrefs_Description;
	public static String ExperimentalXMLPrefs_PreviewPage;
	public static String ExperimentalXMLPrefs_PreviewPageTooltip;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
