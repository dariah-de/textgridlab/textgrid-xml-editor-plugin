package info.textgrid.lab.xmleditor.mpeditor.preferences;

import info.textgrid.lab.xmleditor.mpeditor.Activator;

import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.StringFieldEditor;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;


public class ExperimentalXMLPrefs
extends FieldEditorPreferencePage
implements IWorkbenchPreferencePage {

	public ExperimentalXMLPrefs() {
		super(GRID);
		setPreferenceStore(Activator.getDefault().getPreferenceStore());
		setDescription(Messages.ExperimentalXMLPrefs_Description);
	}

	@Override
	public void createFieldEditors() {
		BooleanFieldEditor previewPage = new BooleanFieldEditor(PreferenceConstants.PREVIEW_PAGE, Messages.ExperimentalXMLPrefs_PreviewPage, getFieldEditorParent());
		previewPage.getDescriptionControl(getFieldEditorParent()).setToolTipText(Messages.ExperimentalXMLPrefs_PreviewPageTooltip);
		addField(previewPage);
		
		StringFieldEditor defaultXslt = new StringFieldEditor(PreferenceConstants.PREVIEW_DEFAULT_URL, "Default Stylesheet", getFieldEditorParent());
		addField(defaultXslt);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
	 */
	public void init(final IWorkbench workbench) {
	}

}