package info.textgrid.lab.xmleditor.mpeditor.preferences;

import info.textgrid.lab.xmleditor.mpeditor.Activator;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.jface.preference.IPreferenceStore;

/**
 * Class used to initialize default preference values.
 */
public class PreferenceInitializer extends AbstractPreferenceInitializer {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer#initializeDefaultPreferences()
	 */
	@Override
	public void initializeDefaultPreferences() {
		final IPreferenceStore store = Activator.getDefault().getPreferenceStore();
		store.setDefault(PreferenceConstants.PREVIEW_PAGE, true);
		store.setDefault(PreferenceConstants.PREVIEW_DEFAULT_URL, "https://raw.githubusercontent.com/TEIC/Stylesheets/master/html/html.xsl");	//$NON-NLS-1$
	}

}
