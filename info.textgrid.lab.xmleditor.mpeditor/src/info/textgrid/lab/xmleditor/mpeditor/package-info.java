/**
 * The multi-page TextGrid xml editor resides here. This package contains the
 * basic multi-page editor setup as well as XML editor functionality not fitting
 * anywhere else.
 * 
 * 
 * <h4>Other packages in this plugin</h4>
 * <ul>
 * <li>{@link info.textgrid.lab.xmleditor.dialogs}</li>
 * <li>{@link info.textgrid.lab.xmleditor.mpeditor}</li>
 * <li>{@link info.textgrid.lab.xmleditor.mpeditor.actions}</li>
 * <li>{@link info.textgrid.lab.xmleditor.mpeditor.views}</li>
 * <li>
 * {@link org.eclipse.wst.xml.ui.internal.tabletree.AbstractXMLMultiPageEditorPart}
 * here</li>
 * </ul>
 * 
 * <h4>Some relevant packages from other XML editor / Vex plug-ins</h4> <h5>
 * {@link net.sf.vex.editor}</h5>
 * <p>
 * Contains the {@link org.eclipse.ui.part.EditorPart} for the embedded vex
 * page: {@link net.sf.vex.editor.VexEditorPage}, as well as some auxillary
 * classes and some stuff for standalone Vex (which is not used inside
 * TextGrid).
 * 
 * <h5>{@link net.sf.vex.dom.linked}</h5>
 * <p>
 * Contains an implementation of Vex' Document Object Model that is linked to
 * the DOM of WST's source code editor.
 * </p>
 * <h5>{@link net.sf.vex.layout}</h5>
 * <p>
 * The rendering and layout mechanism for the WYSIWYM page.
 * </p>
 * 
 * 
 */
package info.textgrid.lab.xmleditor.mpeditor;