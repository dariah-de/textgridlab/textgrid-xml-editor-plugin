package info.textgrid.lab.xmleditor.mpeditor;

import static info.textgrid.lab.core.swtutils.AdapterUtils.getAdapter;
import static info.textgrid.lab.xmleditor.mpeditor.Activator.DEBUG_RESOLVE;
import static info.textgrid.lab.xmleditor.mpeditor.Activator.isDebugging;
import info.textgrid.lab.core.model.ITextGridModelConstants;
import info.textgrid.lab.core.model.TextGridObject;

import java.io.File;
import java.net.URI;
import java.text.MessageFormat;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.wst.common.uriresolver.internal.provisional.URIResolverExtension;

/**
 * This is a resolver for TextGrid's schema associations, it tries to determine
 * the schema associated with the given file (if it is a TextGridObject).
 * 
 * <p>
 * This class is not used by clients directly, but contributed to WST's
 * extension point
 * <code>org.eclipse.wst.common.uriresolver.resolverExtensions</code>.
 * </p>
 * 
 * 
 * @author Thorsten Vitt <vitt@linglit.tu-darmstadt.de> for TextGrid
 */
public class TextGridSchemaResolver implements URIResolverExtension {

	public TextGridSchemaResolver() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Resolve the schema URI to the given file, at least when it has been
	 * implemented.
	 * 
	 * Basically, we do the following:
	 * 
	 * <ol>
	 * <li>Try to determine the given <var>file</var>'s TextGridObject. Return
	 * null if this fails.</li>
	 * <li>Extract the associated schema, if any.</li>
	 * <li>If this fails: Extract the default associated schema from the project
	 * file. <strong>This is not yet implemented</strong>.</li>
	 * <li>Fetch the schema. I'm not sure whether we can simply return a EFS URI
	 * or if we need to download this and return a file:// URI.</li>
	 * </ol>
	 * 
	 * In future stages, we might also resolve cases when file is null (and
	 * handle this via the project file).
	 * 
	 * @see org.eclipse.wst.common.uriresolver.internal.provisional.URIResolverExtension#resolve(org.eclipse.core.resources.IFile,
	 *      java.lang.String, java.lang.String, java.lang.String)
	 * @todo implement.
	 */
	public String resolve(IFile file, String baseLocation, String publicId,
			String systemId) {
		// We sometimes receive a baseLocation of the form
		// 'file:///textgrid-efs://' (TG-690), which must be fixed.
		if (baseLocation != null
				&& (baseLocation.startsWith("file:///") && (baseLocation.startsWith(ITextGridModelConstants.SCHEMA_EFS, 8) //$NON-NLS-1$
						|| baseLocation.startsWith(ITextGridModelConstants.SCHEMA, 8) || baseLocation.startsWith(
						ITextGridModelConstants.SCHEMA_NEWFILE, 8))))
			baseLocation = baseLocation.substring(8);
		if (isDebugging(DEBUG_RESOLVE)) {
			System.out.println(MessageFormat.format(
					"TG schema resolve({0}, {1}, {2}, {3}) ...", file, //$NON-NLS-1$
					baseLocation, publicId, systemId));
		}
		// System.out
		// .println(MessageFormat
		// .format(
		// "I should resolve: \n   file {0},\n   baseLocation {1},\n   publicId {2},\n   systemId {3}.",
		// file, baseLocation, publicId, systemId));
		

		TextGridObject textGridObject = null;
		URI schemaURI = null;
		try {
			// if we get a system ID that is already a textgrid: URI, we can just resolve that stuff
			if (systemId != null && systemId.startsWith(ITextGridModelConstants.SCHEMA)) {
				schemaURI = URI.create(systemId);
			} else {
				// Object may be passed in via file or baseLocation
				if (file != null)
					textGridObject = getAdapter(file, TextGridObject.class);
				else if (baseLocation != null
						&& (baseLocation
								.startsWith(ITextGridModelConstants.SCHEMA_NEWFILE) || baseLocation
								.startsWith(ITextGridModelConstants.SCHEMA_EFS)))
					textGridObject = TextGridObject.getInstance(URI
							.create(baseLocation), false);
				if (textGridObject == null) {
					return null; // none of our business
				}

				schemaURI = textGridObject.getSchemaURI();
			}
			// TODO look in the project file
			if (isDebugging(DEBUG_RESOLVE))
				System.out.println(MessageFormat.format("... found schema {0}", //$NON-NLS-1$
						schemaURI));
			if (schemaURI == null)
				return null; // let someone else decide
			TextGridObject schemaObject = TextGridObject.getInstance(schemaURI,
					false);
			if (schemaObject.isAccessible()) { // TG-897
				File localSchema = schemaObject.toLocalFile(null, false);
				SchemaManager.getInstance().registerLocalSchema(schemaObject,
					localSchema.toURI());

				Activator.handleProblem(IStatus.OK, null,
					"Returned schema {0} for file {1}, object {2}", //$NON-NLS-1$
					localSchema, file, textGridObject);
				return localSchema.toURI().toString();
			}
		} catch (CoreException e) {
			IStatus status = Activator.handleProblem(IStatus.WARNING, e,
					Messages.TextGridSchemaResolver_FailedTryingToGetSchema,
					textGridObject, schemaURI, e.getMessage());
			if (isDebugging(DEBUG_RESOLVE)) {
				System.out.println(MessageFormat.format("... failed: {0}", //$NON-NLS-1$
						status));
			}
		}

		return null;
	}

}

