/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2004 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package info.textgrid.lab.xmleditor.mpeditor;

import net.sf.vex.editor.Messages;
import net.sf.vex.editor.VexEditor;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.part.IPage;
import org.eclipse.ui.part.IPageBookViewPage;
import org.eclipse.ui.part.IPageSite;
import org.eclipse.ui.part.PageBook;
import org.eclipse.ui.part.PageBookView;

/**
 * A view that shows stats about the current Vex editor as a debugging aid.
 */
public class DebugView extends PageBookView {

	protected IPage createDefaultPage(PageBook book) {
		IPageBookViewPage page = new IPageBookViewPage() {
			public void createControl(Composite parent) {
				this.label = new Label(parent, SWT.NONE);
				this.label.setText(Messages
						.getString("DebugView.noActiveEditor")); //$NON-NLS-1$
			}

			public void dispose() {
			}

			public Control getControl() {
				return this.label;
			}

			public IPageSite getSite() {
				return this.site;
			}

			public void init(IPageSite site) throws PartInitException {
				this.site = site;
			}

			public void setActionBars(IActionBars actionBars) {
			}

			public void setFocus() {
			}

			private IPageSite site;

			private Label label;
		};

		initPage(page);
		page.createControl(getPageBook());
		return page;
	}

	protected PageRec doCreatePage(IWorkbenchPart part) {
		DebugViewPage page = new DebugViewPage((MPXmlEditorPart) part);
		initPage(page);
		page.createControl(getPageBook());
		return new PageRec(part, page);
	}

	protected void doDestroyPage(IWorkbenchPart part, PageRec pageRecord) {
		pageRecord.page.dispose();
	}

	protected IWorkbenchPart getBootstrapPart() {
		// TODO Auto-generated method stub
		return null;
	}

	protected boolean isImportant(IWorkbenchPart part) {
		return (part instanceof MPXmlEditorPart);
	}
}
