package info.textgrid.lab.xmleditor.mpeditor;

import static info.textgrid.lab.core.swtutils.AdapterUtils.getAdapter;
import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.swtutils.AdapterUtils;

import java.net.URI;
import java.text.MessageFormat;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.text.IDocument;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.progress.UIJob;
import org.eclipse.ui.statushandlers.StatusManager;
import org.eclipse.ui.texteditor.IEditorStatusLine;
import org.eclipse.ui.texteditor.ITextEditor;
import org.eclipse.wst.sse.core.StructuredModelManager;
import org.eclipse.wst.sse.core.internal.provisional.IStructuredModel;
import org.eclipse.wst.sse.core.internal.provisional.text.IStructuredDocument;
import org.eclipse.wst.xml.core.internal.contentmodel.CMDocument;
import org.eclipse.wst.xml.core.internal.contentmodel.modelquery.CMDocumentManager;
import org.eclipse.wst.xml.core.internal.contentmodel.modelquery.ModelQuery;
import org.eclipse.wst.xml.core.internal.contentmodel.util.CMDocumentCache;
import org.eclipse.wst.xml.core.internal.contentmodel.util.CMDocumentCacheListener;
import org.eclipse.wst.xml.core.internal.modelquery.ModelQueryUtil;
import org.eclipse.wst.xml.core.internal.provisional.document.IDOMDocument;
import org.eclipse.wst.xml.core.internal.provisional.document.IDOMModel;

import com.google.common.base.Joiner;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

/**
 * The user should be informed about trouble with schemas etc. This class is a
 * hub for collecting and managing the information concerning schemas and their
 * associated documents in the currently open editors.
 * 
 * @author tv
 * @see TextGridSchemaResolver
 * 
 */
@SuppressWarnings("restriction")
public class SchemaManager implements CMDocumentCacheListener {

	private static SchemaManager instance = null;
	private Set<CMDocumentCache> knownCaches = Sets.newHashSet();
	private Map<URI, TextGridObject> localSchemas = Maps.newHashMap();

	public static SchemaManager getInstance() {
		if (instance == null)
			instance = new SchemaManager();

		return instance;
	}

	protected SchemaManager() {
	};

	/**
	 * Makes the manager track this editor. Consequences, if applicable (and
	 * when implemented):
	 * 
	 * <ul>
	 * <li>registers with the responsible {@link CMDocumentCache}, so changes in
	 * the schema load status will be tracked (implemented)</li>
	 * <li>registers the editor, so status information can be shown (TODO)</li>
	 * <li>TODO: Can we track the schema load status somehow?</li>
	 * </ul>
	 * 
	 * @param editor
	 */
	public void associateWith(IEditorPart editor) {

		ITextEditor textEditor = AdapterUtils.getAdapter(editor,
				ITextEditor.class);
		IDocument document = textEditor.getDocumentProvider().getDocument(
				textEditor.getEditorInput());

		if (document instanceof IStructuredDocument) {

			IStructuredModel model = StructuredModelManager.getModelManager()
			.getModelForRead((IStructuredDocument) document);
			try {
				if (model instanceof IDOMModel) {
					IDOMDocument domDocument = ((IDOMModel) model).getDocument();

					CMDocumentCache cache = ModelQueryUtil
					.getCMDocumentCache(domDocument);

					if (!knownCaches.contains(cache)) {
						knownCaches.add(cache);
						cache.addListener(this);
					}

					ModelQuery modelQuery = ModelQueryUtil.getModelQuery(domDocument);
					CMDocumentManager cmDocumentManager = modelQuery.getCMDocumentManager();
					// cmDocumentManager.setPropertyEnabled(CMDocumentManager.PROPERTY_AUTO_LOAD,
					// false);
					CMDocument correspondingCMDocument = modelQuery.getCorrespondingCMDocument(domDocument);
					cmDocumentManager.setPropertyEnabled(CMDocumentManager.PROPERTY_AUTO_LOAD, true);
					if (correspondingCMDocument != null) {
						System.out.println("YAY, found a corresponding CMDocument!" + correspondingCMDocument); //$NON-NLS-1$
					}
				}
			} finally {
				if (model != null)
					model.releaseFromRead();
			}
		}
	}

	public void cacheCleared(CMDocumentCache cache) {
	}

	public void cacheUpdated(CMDocumentCache cache, String uri, int oldStatus,
			int newStatus, CMDocument cmDocument) {
		TextGridObject schemaObject = null;
		if (uri != null)
			schemaObject = findSchemaObject(URI.create(uri));
		if (newStatus == CMDocumentCache.STATUS_ERROR) {
			handleBrokenSchema(uri, cmDocument, schemaObject);
		} else if (newStatus == CMDocumentCache.STATUS_LOADING) {
			showStatus(uri, schemaObject, Messages.SchemaManager_LoadingSchemaX, false);
		} else if (newStatus == CMDocumentCache.STATUS_LOADED) {
			showStatus(uri, schemaObject, Messages.SchemaManager_SchemaXLoaded, false);
		}
	}

	private void handleBrokenSchema(final String uri, final CMDocument cmDocument, final TextGridObject schemaObject) {
		StatusManager.getManager().handle(
				new Status(IStatus.ERROR, Activator.PLUGIN_ID, MessageFormat.format(
						Messages.SchemaManager_ContentModelFailed, uri, cmDocument,
						schemaObject)), StatusManager.LOG);

		try {
			showStatus(uri, schemaObject, NLS.bind(Messages.SchemaManager_FailedToLoadSchema, schemaObject.getTitle()), true);
		} catch (CoreException e1) {
			showStatus(uri, schemaObject, NLS.bind(Messages.SchemaManager_FailedToLoadSchema, uri), true);
		}

		IFile schemaFile = getAdapter(schemaObject, IFile.class);
		if (schemaFile != null) {
			try {
				IMarker marker = schemaFile.createMarker("org.eclipse.core.resources.marker.problemmarker"); //$NON-NLS-1$
				marker.setAttribute(IMarker.SEVERITY, IMarker.SEVERITY_ERROR);
				marker.setAttribute(IMarker.MESSAGE, Messages.SchemaManager_FailedToLoadParseSchema);
				IMarker[] markers = schemaFile.findMarkers(null, true, IResource.DEPTH_INFINITE);
				System.out.println(Joiner.on(", ").join(markers)); //$NON-NLS-1$
				PlatformUI.getWorkbench().getDecoratorManager().update(
						"info.textgrid.lab.debug.decorators.ResourceProblemDecorator"); //$NON-NLS-1$

			} catch (CoreException e) {
				StatusManager.getManager().handle(e, Activator.PLUGIN_ID);
			}
		}

	}

	private void showStatus(final String uri, final TextGridObject schemaObject, final String message, final boolean error) {
			new UIJob("") { //$NON-NLS-1$
				
				@Override
			public IStatus runInUIThread(IProgressMonitor monitor) {
				try {
					
					final int severity = error ? IStatus.ERROR : IStatus.INFO;

					String filledInMessage = NLS.bind(message,
							schemaObject != null ? schemaObject.getTitle()
									: uri);
					StatusManager.getManager().handle(
new Status(severity, Activator.PLUGIN_ID,
									filledInMessage));

					IEditorPart activeEditor = PlatformUI.getWorkbench()
							.getActiveWorkbenchWindow().getActivePage()
							.getActiveEditor();
					IEditorStatusLine editorStatusLine = AdapterUtils
							.getAdapter(activeEditor, IEditorStatusLine.class);

					if (editorStatusLine == null)
						return Status.CANCEL_STATUS;

					if (schemaObject != null) {
						editorStatusLine.setMessage(error, filledInMessage,
								schemaObject.getContentType(false).getImage(
										true));
					} else
						editorStatusLine.setMessage(error, filledInMessage,
								null);
				} catch (CoreException e) {
					StatusManager.getManager().handle(e, Activator.PLUGIN_ID);
				}
				return Status.OK_STATUS;
			}
		}.schedule();
	}

	/**
	 * Notifies the manager that the local file identified by
	 * <var>localURI</var> has been created as a local cached copy of the given
	 * <var>schemaObject</var>. Called by
	 * {@link TextGridSchemaResolver#resolve(org.eclipse.core.resources.IFile, String, String, String)}
	 */
	protected void registerLocalSchema(final TextGridObject schemaObject,
			final URI localURI) {
		localSchemas.put(localURI, schemaObject);
	}

	protected TextGridObject findSchemaObject(final URI localURI) {
		return localSchemas.get(localURI);
	}

}
