package info.textgrid.lab.xmleditor.mpeditor;

import java.io.FileNotFoundException;
import java.io.PrintStream;

import net.sf.vex.dom.linked.LinkedDocument;
import net.sf.vex.editor.VexEditorPage;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.statushandlers.StatusManager;

public class DebugDocumentHandler extends AbstractHandler implements IHandler {

	public Object execute(final ExecutionEvent event) throws ExecutionException {
		final IEditorPart editor = HandlerUtil.getActiveEditor(event);
		if (editor != null && editor instanceof MPXmlEditorPart) {
			final VexEditorPage vex = ((MPXmlEditorPart) editor).getWysiwymEditor();
			if (vex == null)
				return null;

			final FileDialog dialog = new FileDialog(HandlerUtil.getActiveShell(event), SWT.SAVE);
			dialog.setText(Messages.DebugDocumentHandler_SaveWYSIWYMDebug);
			dialog.setFileName(editor.getEditorInput().getName().concat(".txt")); //$NON-NLS-1$
			dialog.setFilterExtensions(new String[] { "*.txt" }); //$NON-NLS-1$
			dialog.setFilterNames(new String[] { Messages.DebugDocumentHandler_TextFiles });
			final String fileName = dialog.open();

			final LinkedDocument document = vex.getDoc();
			if (fileName != null) {
				try {
					final PrintStream printStream = new PrintStream(fileName);
					document.printModelCheckReport(printStream);
					document.printDocument(printStream);
					printStream.close();
				} catch (final FileNotFoundException e) {
					StatusManager.getManager().handle(
							new Status(IStatus.ERROR, Activator.PLUGIN_ID, NLS.bind(Messages.DebugDocumentHandler_FailedToSaveDebuggingOutput,
									fileName, e.getLocalizedMessage()), e), StatusManager.LOG | StatusManager.SHOW);
				}
			} else
				document.printModelCheckReport(System.out);
		}

		// TODO Auto-generated method stub
		return null;
	}

}
