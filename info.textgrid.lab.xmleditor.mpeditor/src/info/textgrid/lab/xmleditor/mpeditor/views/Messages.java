package info.textgrid.lab.xmleditor.mpeditor.views;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "info.textgrid.lab.xmleditor.mpeditor.views.messages"; //$NON-NLS-1$
	public static String ContentModelView_ContentModel;
	public static String ContentModelView_SelectSomething;
	public static String ValidationErrorsView_ErrorMsg;
	public static String ValidationErrorsView_InvalidXML;
	public static String ValidationErrorsView_NotAnnotation;
	public static String ValidationErrorsView_Offset;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
