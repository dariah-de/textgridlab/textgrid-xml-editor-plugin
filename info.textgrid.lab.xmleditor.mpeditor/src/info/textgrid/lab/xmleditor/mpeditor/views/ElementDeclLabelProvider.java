package info.textgrid.lab.xmleditor.mpeditor.views;

import org.eclipse.jface.viewers.IBaseLabelProvider;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;
import org.eclipse.wst.xml.core.internal.contentmodel.CMElementDeclaration;

@SuppressWarnings("restriction")
public class ElementDeclLabelProvider extends LabelProvider implements
		IBaseLabelProvider, ITableLabelProvider {

	public static int NAME_COLUMN = 0;
	public static int OCCURS_COLUMN = 1;
	public static int DOCUMENTATION_COLUMN = 2;

	public Image getColumnImage(Object element, int columnIndex) {
		// TODO Auto-generated method stub
		return null;
	}

	public String getColumnText(Object element, int columnIndex) {
		if (element instanceof CMElementDeclaration) {
			CMElementDeclaration ed = (CMElementDeclaration) element;
			if (columnIndex == NAME_COLUMN)
				return ed.getElementName();
			else if (columnIndex == OCCURS_COLUMN) {
				return ed.getMinOccur() + ".." //$NON-NLS-1$
						+ (ed.getMaxOccur() < 0 ? "*" : ed.getMaxOccur()); //$NON-NLS-1$
			}
		}
		return null;
	}

}
