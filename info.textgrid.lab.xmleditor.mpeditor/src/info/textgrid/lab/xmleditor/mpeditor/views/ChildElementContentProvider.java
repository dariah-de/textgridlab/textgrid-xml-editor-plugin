package info.textgrid.lab.xmleditor.mpeditor.views;

import java.util.List;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.wst.xml.core.internal.contentmodel.CMContent;
import org.eclipse.wst.xml.core.internal.contentmodel.CMElementDeclaration;
import org.eclipse.wst.xml.core.internal.contentmodel.CMGroup;

@SuppressWarnings("restriction")
public class ChildElementContentProvider implements IStructuredContentProvider {

	public Object[] getElements(Object inputElement) {
		if (inputElement instanceof List<?>) {
			return ((List<?>) inputElement).toArray();
		} else if (inputElement instanceof CMElementDeclaration) {
			CMElementDeclaration declaration = (CMElementDeclaration) inputElement;
			CMContent content = declaration.getContent();
			if (content instanceof CMElementDeclaration) {
				return new Object[] { content };
			} else if (content instanceof CMGroup) {
				CMGroup group = (CMGroup) content;
				group.getChildNodes();
			}

		}

		return null;
	}

	public void dispose() {
		// TODO Auto-generated method stub

	}

	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		// TODO Auto-generated method stub

	}

}
