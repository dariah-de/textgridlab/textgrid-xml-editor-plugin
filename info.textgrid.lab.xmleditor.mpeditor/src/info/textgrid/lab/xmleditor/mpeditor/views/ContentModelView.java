/**
 * 
 */
package info.textgrid.lab.xmleditor.mpeditor.views;

import info.textgrid.lab.core.browserfix.TextGridLabBrowser;

import java.text.MessageFormat;

import net.sf.vex.dom.linked.LinkedNode;

import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.wst.xml.core.internal.contentmodel.CMElementDeclaration;
import org.eclipse.wst.xml.core.internal.contentmodel.modelquery.ModelQuery;
import org.eclipse.wst.xml.core.internal.modelquery.ModelQueryUtil;
import org.eclipse.wst.xml.ui.internal.taginfo.MarkupTagInfoProvider;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

/**
 * @author tv
 *
 */
@SuppressWarnings("restriction")
public class ContentModelView extends ViewPart implements ISelectionListener {

	private Browser currentElementDoc;
	
	private MarkupTagInfoProvider infoProvider = new MarkupTagInfoProvider();
	
	/** Copied from org.eclipse.jface.internal.text.revisions.RevisionPainter.HoverInformationControlCreator */
	private static final String fgStyleSheet= "/* Font definitions */\n" + //$NON-NLS-1$
	"body, h1, h2, h3, h4, h5, h6, p, table, td, caption, th, ul, ol, dl, li, dd, dt {font-family: sans-serif; font-size: 9pt }\n" + //$NON-NLS-1$ 
	"pre				{ font-family: monospace; font-size: 9pt }\n" + //$NON-NLS-1$
	"\n" + //$NON-NLS-1$
	"/* Margins */\n" + //$NON-NLS-1$
	"body	     { overflow: auto; margin-top: 0; margin-bottom: 4; margin-left: 3; margin-right: 0 }\n" + //$NON-NLS-1$ 
	"h1           { margin-top: 5; margin-bottom: 1 }	\n" + //$NON-NLS-1$
	"h2           { margin-top: 25; margin-bottom: 3 }\n" + //$NON-NLS-1$
	"h3           { margin-top: 20; margin-bottom: 3 }\n" + //$NON-NLS-1$
	"h4           { margin-top: 20; margin-bottom: 3 }\n" + //$NON-NLS-1$
	"h5           { margin-top: 0; margin-bottom: 0 }\n" + //$NON-NLS-1$
	"p            { margin-top: 10px; margin-bottom: 10px }\n" + //$NON-NLS-1$
	"pre	         { margin-left: 6 }\n" + //$NON-NLS-1$
	"ul	         { margin-top: 0; margin-bottom: 10 }\n" + //$NON-NLS-1$ 
	"li	         { margin-top: 0; margin-bottom: 0 } \n" + //$NON-NLS-1$
	"li p	     { margin-top: 0; margin-bottom: 0 } \n" + //$NON-NLS-1$
	"ol	         { margin-top: 0; margin-bottom: 10 }\n" + //$NON-NLS-1$
	"dl	         { margin-top: 0; margin-bottom: 10 }\n" + //$NON-NLS-1$
	"dt	         { margin-top: 0; margin-bottom: 0; font-weight: bold }\n" + //$NON-NLS-1$ 
	"dd	         { margin-top: 0; margin-bottom: 0 }\n" + //$NON-NLS-1$
	"\n" + //$NON-NLS-1$
	"/* Styles and colors */\n" + //$NON-NLS-1$
	"a:link	     { color: #0000FF }\n" + //$NON-NLS-1$
	"a:hover	     { color: #000080 }\n" + //$NON-NLS-1$
	"a:visited    { text-decoration: underline }\n" + //$NON-NLS-1$
	"h4           { font-style: italic }\n" + //$NON-NLS-1$ 
	"strong	     { font-weight: bold }\n" + //$NON-NLS-1$
	"em	         { font-style: italic }\n" + //$NON-NLS-1$
	"var	         { font-style: italic }\n" + //$NON-NLS-1$
	"th	         { font-weight: bold }\n" + //$NON-NLS-1$
	"p.spec       { font-size: 7pt; color: gray; border-top: inset 2pt; }\n" //$NON-NLS-1$
			+
	""; //$NON-NLS-1$

	// private TableViewer elementTable;

	/* (non-Javadoc)
	 * @see org.eclipse.ui.part.WorkbenchPart#createPartControl(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	public void createPartControl(Composite parent) {
		currentElementDoc = TextGridLabBrowser.createBrowser(parent);
		setInfoText(Messages.ContentModelView_SelectSomething);
		GridDataFactory.fillDefaults().applyTo(currentElementDoc);

		// elementTable = new TableViewer(parent, SWT.NONE);
		// GridDataFactory.fillDefaults().applyTo(elementTable.getControl());
		// elementTable.setContentProvider(new ChildElementContentProvider());
		// elementTable.setLabelProvider(new ElementDeclLabelProvider());
		
		PlatformUI.getWorkbench().getActiveWorkbenchWindow()
				.getSelectionService().addPostSelectionListener(this);

	}

	/**
	 * @param fragment
	 *            an HTML fragment
	 */
	private void setInfoText(String fragment) {
		StringBuilder sb = new StringBuilder();
		sb.append("<html><head><title>"); //$NON-NLS-1$
		sb.append(Messages.ContentModelView_ContentModel);
		sb.append("</title><style type=\"text/css\">"); //$NON-NLS-1$
		sb.append(fgStyleSheet);
		sb.append("</style></head><body>"); //$NON-NLS-1$
		sb.append(fragment);
		sb.append("</body></html>"); //$NON-NLS-1$
		currentElementDoc.setText(sb.toString());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.part.WorkbenchPart#dispose()
	 */
	@Override
	public void dispose() {
		super.dispose();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ui.part.WorkbenchPart#setFocus()
	 */
	@Override
	public void setFocus() {
		// TODO Auto-generated method stub

	}

	public void selectionChanged(IWorkbenchPart part, ISelection selection) {
		if (selection instanceof IStructuredSelection) {
			IStructuredSelection sel = (IStructuredSelection) selection;
			if (!sel.isEmpty()) {
				Node node;
				if (sel.getFirstElement() instanceof LinkedNode) {
					LinkedNode linkedNode = (LinkedNode) sel.getFirstElement();
					node = linkedNode.getDomNode();
				} else if (sel.getFirstElement() instanceof Node) {
					node = (Node) sel.getFirstElement();
				} else
					return; // don't know how to handle anything else
				
				if (node.getNodeType() != Node.ELEMENT_NODE
						&& node.getParentNode() != null)
					node = node.getParentNode();
				
				/* if (node.getNodeType() == Node.ELEMENT_NODE) */{
					ModelQuery modelQuery = ModelQueryUtil.getModelQuery(node
							.getOwnerDocument());
					if (modelQuery != null) {
						CMElementDeclaration declaration = modelQuery
								.getCMElementDeclaration((Element) node);
						String info = infoProvider.getInfo(declaration);
						Object spec = modelQuery.getCorrespondingCMDocument(
								node).getProperty("spec"); //$NON-NLS-1$
						setInfoText(MessageFormat.format(
								"{0}\n\n<p class=\"spec\" style=\"font-size: 5pt;\">{1}</p>\n", //$NON-NLS-1$
										info,
								spec));
						System.err.println(MessageFormat.format("CMDocument: {0}, spec: {1}",  //$NON-NLS-1$
								modelQuery
								.getCorrespondingCMDocument(node).getProperty(
										"CMDocument"), spec)); //$NON-NLS-1$

						// List<?> availableContent = modelQuery
						// .getAvailableContent((Element) node,
						// declaration,
						// ModelQuery.INCLUDE_CHILD_NODES);
						// elementTable.setInput(availableContent);
						
					}
				}
			}
		}
	}

}
