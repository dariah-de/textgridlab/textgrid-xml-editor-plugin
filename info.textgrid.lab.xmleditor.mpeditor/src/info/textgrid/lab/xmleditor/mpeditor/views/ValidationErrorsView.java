package info.textgrid.lab.xmleditor.mpeditor.views;

import info.textgrid.lab.core.swtutils.annotations.ActiveEditorAnnotationViewerController;
import info.textgrid.lab.core.swtutils.annotations.AnnotationContentProvider;
import info.textgrid.lab.xmleditor.mpeditor.Activator;

import java.text.MessageFormat;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.source.Annotation;
import org.eclipse.jface.text.source.IAnnotationModel;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerComparator;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.ui.texteditor.ITextEditor;
import org.eclipse.wst.sse.core.StructuredModelManager;
import org.eclipse.wst.sse.core.internal.provisional.IStructuredModel;
import org.eclipse.wst.sse.core.internal.provisional.IndexedRegion;
import org.eclipse.wst.sse.core.internal.provisional.text.IStructuredDocument;
import org.eclipse.wst.xml.ui.internal.tabletree.XMLTableTreeContentProvider;

import com.google.common.base.Predicate;

@SuppressWarnings("restriction")
public class ValidationErrorsView extends ViewPart {

	public static final String ID = "info.textgrid.lab.xmleditor.mpeditor.views.ValidationErrors"; //$NON-NLS-1$
	private TableViewer viewer;
	private ActiveEditorAnnotationViewerController controller;

	public ValidationErrorsView() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void createPartControl(final Composite parent) {
		viewer = new TableViewer(parent, SWT.SINGLE | SWT.FULL_SELECTION);
		final AnnotationContentProvider contentProvider = new AnnotationContentProvider();
		contentProvider.setFilter(new Predicate<Annotation>() {

			public boolean apply(final Annotation input) {
				return input.getType().startsWith("org.eclipse.wst.sse.ui.temp."); //$NON-NLS-1$
			}
		});
		viewer.setContentProvider(contentProvider);
		viewer.getTable().setHeaderVisible(true);

		// First column: Error message itself. FIXME fancy markup
		TableViewerColumn messageColumn = new TableViewerColumn(viewer, SWT.LEFT);
		messageColumn.getColumn().setText(Messages.ValidationErrorsView_ErrorMsg);
		messageColumn.getColumn().setWidth(400);
		messageColumn.setLabelProvider(new ColumnLabelProvider() {

			@Override
			public Image getImage(Object element) {
				return JFaceResources.getImage(TitleAreaDialog.DLG_IMG_MESSAGE_ERROR);
			}

			@Override
			public String getText(Object element) {
				if (element instanceof Annotation) {
					Annotation annotation = (Annotation) element;
					return annotation.getText();
				}
				return MessageFormat.format(Messages.ValidationErrorsView_NotAnnotation, element, element.getClass());
			}

			@Override
			public String getToolTipText(Object element) {
				if (element instanceof Annotation)
					return ((Annotation) element).getText();
				else
					return super.getToolTipText(element);
			}
		});

		// Second column: associated text. FIXME show some context
		TableViewerColumn textColumn = new TableViewerColumn(viewer, SWT.LEFT);
		textColumn.getColumn().setText(Messages.ValidationErrorsView_InvalidXML);
		textColumn.getColumn().setWidth(100);
		textColumn.setLabelProvider(new ColumnLabelProvider() {


			ILabelProvider xmlLabelProvider = new XMLTableTreeContentProvider();

			@Override
			public String getText(Object element) {
				if (element instanceof Annotation) {
					Annotation annotation = (Annotation) element;
					IndexedRegion region = getIndexedRegion(annotation);
					final String nodeLabel = xmlLabelProvider.getText(region);
					if (!"".equals(nodeLabel)) //$NON-NLS-1$
						return nodeLabel;
					else
						return annotation.getText(); // FIXME

				}
				return null;
			}

			public Image getImage(Object element) {
				if (element instanceof Annotation) {
					IndexedRegion region = getIndexedRegion((Annotation) element);
					if (region != null)
						return xmlLabelProvider.getImage(region);
				}
				return super.getImage(element);
			}

			private IndexedRegion getIndexedRegion(Annotation annotation) {
				final Position position = controller.getAnnotationModel().getPosition(annotation);
				final IDocument currentDocument = controller.getCurrentDocument();
				if (currentDocument != null && position != null) { // TG-589
					final IStructuredModel model = StructuredModelManager.getModelManager().getModelForRead(
							(IStructuredDocument) currentDocument);
					final IndexedRegion region = model.getIndexedRegion(position.offset);
					model.releaseFromRead();
					return region;
				}
				Activator.handleProblem(
						IStatus.WARNING,
						new IllegalStateException(),
						"Something strange has happened: We should label annotation {0} in document {1} with position {2}, but something is null.", //$NON-NLS-1$
						annotation, currentDocument, position);
				return null;
			}

		});

		// Third column: character offset. FIXME use line number or sth. similar
		// here
		TableViewerColumn offsetColumn = new TableViewerColumn(viewer, SWT.RIGHT);
		offsetColumn.getColumn().setWidth(50);
		offsetColumn.getColumn().setText(Messages.ValidationErrorsView_Offset);
		offsetColumn.setLabelProvider(new ColumnLabelProvider() {

			@Override
			public String getText(Object element) {
				if (element instanceof Annotation && controller.getAnnotationModel() != null) {
					Position position = controller.getAnnotationModel().getPosition((Annotation) element);
					if (position != null)
						return Integer.toString(position.getOffset());
				}
				return null;
			}

		});

		// By default, sort by offset
		viewer.setComparator(new ViewerComparator() {

			@Override
			public int compare(Viewer viewer, Object e1, Object e2) {
				if (e1 instanceof Annotation && e2 instanceof Annotation) {
					Position position1 = controller.getAnnotationModel().getPosition((Annotation) e1);
					if (position1 == null)
						return 0;
					int offset1 = position1.offset;
					Position position2 = controller.getAnnotationModel().getPosition((Annotation) e2);
					if (position2 == null)
						return 0;
					int offset2 = position2.offset;
					return offset1 - offset2;
				}
				return super.compare(viewer, e1, e2);
			}

		});

		viewer.addDoubleClickListener(new IDoubleClickListener() {

			public void doubleClick(DoubleClickEvent event) {
				Annotation annotation = (Annotation) ((IStructuredSelection) event.getSelection()).getFirstElement();
				if (annotation != null) {
					Position position = controller.getAnnotationModel().getPosition(annotation);
					ITextEditor textEditor = controller.getTextEditor();

					if (position != null && textEditor != null) {
						textEditor.selectAndReveal(position.getOffset(), position.getLength());
					}
				}
			}
		});

		controller = new ActiveEditorAnnotationViewerController(viewer);
		getSite().getPage().addPartListener(controller);
		IEditorPart activeEditor = getSite().getPage().getActiveEditor();
		if (activeEditor != null) // TG-617
			controller.partActivated(activeEditor);

	}

	@Override
	public void dispose() {
		if (controller != null)
			getSite().getPage().removePartListener(controller);
		super.dispose();
	}

	@Override
	public void setFocus() {
		// TODO Auto-generated method stub

	}

}
