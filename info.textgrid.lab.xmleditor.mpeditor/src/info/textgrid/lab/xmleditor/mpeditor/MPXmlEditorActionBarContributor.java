/**
 * 
 */
package info.textgrid.lab.xmleditor.mpeditor;

import net.sf.vex.editor.VexEditorPage;
import net.sf.vex.editor.VexPageActionBarContributor;

import org.eclipse.ui.IActionBars;
import org.eclipse.ui.IEditorActionBarContributor;
import org.eclipse.ui.IEditorPart;
import org.eclipse.wst.sse.ui.internal.ISourceViewerActionBarContributor;
import org.eclipse.wst.xml.ui.internal.tabletree.IDesignViewerActionBarContributor;
import org.eclipse.wst.xml.ui.internal.tabletree.XMLMultiPageEditorActionBarContributor;

/**
 * @author tv
 * 
 */
@SuppressWarnings("restriction")
public class MPXmlEditorActionBarContributor extends
		XMLMultiPageEditorActionBarContributor {

	private VexPageActionBarContributor wysiwymViewerActionBarContributor;

	protected void initWysiwymViewerActionBarContributor(IActionBars actionBars) {
		if (wysiwymViewerActionBarContributor != null) {
			wysiwymViewerActionBarContributor.init(actionBars, getPage());
		}
	}
	
	protected IDesignViewerActionBarContributor getDesignViewerActionBarContributor() {
		if (designViewerActionBarContributor instanceof IDesignViewerActionBarContributor)
			return (IDesignViewerActionBarContributor) designViewerActionBarContributor;
		return null;
	}

	@Override
	public void init(IActionBars actionBars) {
		super.init(actionBars);
		initWysiwymEditorActionBars(actionBars);
	}

	protected void initWysiwymEditorActionBars(IActionBars actionBars) {
		getWysiwymActionBarContributor().init(actionBars, getPage());
	}

	// @Override
	// public void setActiveEditor(IEditorPart targetEditor) {
	// if (targetEditor != null) {
	// VexEditor targetVex = (VexEditor) targetEditor
	// .getAdapter(VexEditor.class);
	// if (targetVex != null)
	// targetVex.getEditorSite().getActionBarContributor()
	// .setActiveEditor(targetVex);
	// }
	// super.setActiveEditor(targetEditor);
	// }
	
	/**
	 * This returns the action bar contributor for the Vex-based WYSIWYM page.
	 */
	public VexPageActionBarContributor getWysiwymActionBarContributor() {
		if (wysiwymViewerActionBarContributor == null) {
			wysiwymViewerActionBarContributor = new VexPageActionBarContributor();
			// wysiwymViewerActionBarContributor.init(getActionBars(),
			// getPage());
		}
		return wysiwymViewerActionBarContributor;
	}

	public IEditorActionBarContributor getSourceViewerActionContributor() {
		return sourceViewerActionContributor;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.wst.xml.ui.internal.tabletree.SourceEditorActionBarContributor
	 * #setActivePage(org.eclipse.ui.IEditorPart)
	 */
	@Override
	public void setActivePage(IEditorPart activeEditor) {
		super.setActivePage(activeEditor);
		if (activeEditor instanceof VexEditorPage)
			activateWysiwymPage(activeEditor);
	}

	protected void activateWysiwymPage(IEditorPart activeEditor) {
		getWysiwymActionBarContributor().setActiveEditor(activeEditor);
		getDesignViewerActionBarContributor()
				.setViewerSpecificContributionsEnabled(false);
		((ISourceViewerActionBarContributor) getSourceViewerActionContributor())
				.setViewerSpecificContributionsEnabled(false);
		getWysiwymActionBarContributor().setViewerSpecificContributionsEnabled(
				true);
		
	}

}
