package info.textgrid.lab.xmleditor.mpeditor;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "info.textgrid.lab.xmleditor.mpeditor.messages"; //$NON-NLS-1$
	public static String DebugDocumentHandler_FailedToSaveDebuggingOutput;
	public static String DebugDocumentHandler_SaveWYSIWYMDebug;
	public static String DebugDocumentHandler_TextFiles;
	public static String DebugViewPage_9;
	public static String IsNoImagePropertyTester_CouldNotRetrieveContentType;
	public static String MPXmlEditorPart_1;
	public static String MPXmlEditorPart_Cancel;
	public static String MPXmlEditorPart_ClosingEditor;
	public static String MPXmlEditorPart_Continue;
	public static String MPXmlEditorPart_OpeningX;
	public static String MPXmlEditorPart_OpenInTextEditor;
	public static String MPXmlEditorPart_PreviewPageLabel;
	public static String MPXmlEditorPart_ReopeningX;
	public static String MPXmlEditorPart_TooLargeWarning;
	public static String MPXmlEditorPart_WillBeClosed;
	public static String PreviewPage_ErrorDetailsHTML;
	public static String PreviewPage_Loading;
	public static String PreviewPage_NoCause;
	public static String PreviewPage_NoStylesheetAssociated;
	public static String PreviewPage_NoStylesheetHTML;
	public static String PreviewPage_Updating;
	public static String PreviewPage_UpdatingBrowser;
	public static String PreviewPage_UsageMessageHTML;
	public static String SchemaManager_ContentModelFailed;
	public static String SchemaManager_FailedToLoadParseSchema;
	public static String SchemaManager_FailedToLoadSchema;
	public static String SchemaManager_LoadingSchemaX;
	public static String SchemaManager_SchemaXLoaded;
	public static String TextGridSchemaResolver_FailedTryingToGetSchema;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
