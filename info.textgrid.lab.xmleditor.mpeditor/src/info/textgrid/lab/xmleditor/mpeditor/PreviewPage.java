package info.textgrid.lab.xmleditor.mpeditor;

import info.textgrid.lab.authn.RBACSession;
import info.textgrid.lab.conf.OfflineException;
import info.textgrid.lab.conf.client.ConfClient;
import info.textgrid.lab.xmleditor.mpeditor.preferences.PreferenceConstants;

import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.text.MessageFormat;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.transform.Source;
import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.text.IDocument;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.progress.UIJob;
import org.eclipse.ui.statushandlers.StatusManager;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.cache.RemovalListener;
import com.google.common.cache.RemovalNotification;

public class PreviewPage extends Composite {

	private final Browser browser;
	protected static QName TEI = new QName("http://www.tei-c.org/ns/1.0", "TEI");

	/**
	 * Create the composite.
	 * 
	 * @param parent
	 * @param style
	 */
	public PreviewPage(final Composite parent, final int style) {
		super(parent, style);
		setLayout(new GridLayout(1, false));

		browser = new Browser(this, SWT.NONE);
		browser.setText(Messages.PreviewPage_Loading);
		browser.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));

	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

	public void setDocument(final IDocument document) {
		if (browser != null && !browser.isDisposed()) {
			new Job(Messages.PreviewPage_Updating) {

				@Override
				protected IStatus run(IProgressMonitor monitor) {
					SubMonitor progress = SubMonitor.convert(monitor, 100);
					progress.worked(5);
					String doc = document.get();
					Templates stylesheet;
					try {
						try {
							stylesheet = getStylesheetManager().getAssociatedStylesheet(doc);
							progress.worked(5);
						} catch (TransformerConfigurationException noStylesheetException) {
							if (stylesheetManager.supportsXSLT2()
									&& TEI.equals(getRootElement(new StreamSource(new StringReader(doc)), progress.newChild(5)))) {
								String stylesheetURL = Activator.getDefault().getPreferenceStore().getString(PreferenceConstants.PREVIEW_DEFAULT_URL);
								stylesheet = getStylesheetManager().getStylesheet(stylesheetURL); //$NON-NLS-1$
							} else {
								throw noStylesheetException;
							}
							progress.worked(15);
						}
						final Transformer transformer = stylesheet.newTransformer();
						progress.worked(15);

						String sid = RBACSession.getInstance().getSID(false);
						transformer.setParameter("sessionId", sid);
						try {
							final StringBuilder pattern = new StringBuilder();
							final String crudEndpoint = ConfClient.getInstance().getValue("tgcrud");
							pattern.append(crudEndpoint.substring(0, crudEndpoint.lastIndexOf('/') + 1));
							pattern.append("rest/@URI@/data");
							if (sid != null && !sid.isEmpty())
								pattern.append("?sessionId=").append(sid);
							transformer.setParameter("graphicsURLPattern", pattern.toString());
						} catch (OfflineException e) {
							StatusManager.getManager().handle(new Status(IStatus.WARNING, Activator.PLUGIN_ID, "Cannot determine TG-crud instance", e));
						}
						final StringReader reader = new StringReader(doc);
						final StreamSource source = new StreamSource(reader);
						final StringWriter writer = new StringWriter();
						final StreamResult result = new StreamResult(writer);
						progress.worked(10);
						transformer.transform(source, result);
						progress.worked(45);
						setText(writer.toString());
						progress.worked(5);
						progress.done();
					} catch (TransformerException e) {
						handleException(e, getStylesheetManager().getErrorListener());
						return Status.CANCEL_STATUS;	
					} catch (TransformerFactoryConfigurationError e) {
						handleException(e, getStylesheetManager().getErrorListener());
						return Status.CANCEL_STATUS;	
					} catch (ExecutionException e) {
						handleException(e.getCause(), getStylesheetManager().getErrorListener());
						return Status.CANCEL_STATUS;	
					} catch (XMLStreamException e) {
						handleException(e.getCause(), getStylesheetManager().getErrorListener());
						return Status.CANCEL_STATUS;	
					}
					return Status.OK_STATUS;
				}
			}.schedule();

			// browser.setText(document.get(), true);
		}
	}

	private void showUsageMessage(final String title, final String body) {
		final String message = Messages.PreviewPage_UsageMessageHTML;
		setText(NLS.bind(message, title, body));
	}

	private IStatus handleException(final Throwable e,
			LabErrorListener errorListener) {
		final StringWriter writer = new StringWriter();
		final PrintWriter printer = new PrintWriter(writer);
		e.printStackTrace(printer);
		showUsageMessage(e.getClass().getSimpleName(), MessageFormat.format(
				Messages.PreviewPage_ErrorDetailsHTML, e.getLocalizedMessage(),
				e.getCause() == null ? Messages.PreviewPage_NoCause : e
						.getCause().getLocalizedMessage(),
						errorListener == null? Messages.PreviewPage_NoCause : errorListener.toHTML(true),
						writer.toString()));

		IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID,
				e.getLocalizedMessage(), e);
		StatusManager.getManager().handle(status);
		return status;
	}

	public void setText(final String text) {
		new UIJob(Messages.PreviewPage_UpdatingBrowser) {

			@Override
			public IStatus runInUIThread(IProgressMonitor monitor) {
				if (browser != null && !browser.isDisposed())
					browser.setText(text, true);
				return Status.OK_STATUS;
			}

		}.schedule();
	}

	
	private static StylesheetManager stylesheetManager = null;
	public static StylesheetManager getStylesheetManager() {
		if (stylesheetManager == null)
			stylesheetManager = new StylesheetManager();
		return stylesheetManager;
	}
	
	public static class StylesheetManager {
		private TransformerFactory transformerFactory = null;
		private LoadingCache<String, Templates> cache;
		private LabErrorListener errorListener;
		
		private TransformerFactory getTransformerFactory()
				throws TransformerFactoryConfigurationError {
			if (transformerFactory == null) {
				try { 
					transformerFactory = TransformerFactory.newInstance("net.sf.saxon.TransformerFactoryImpl", null); //$NON-NLS-1$ 
					transformerFactory.setErrorListener(errorListener);
				} catch (final TransformerFactoryConfigurationError e) {
					StatusManager
					.getManager()
					.handle(new Status(
							IStatus.WARNING,
							Activator.PLUGIN_ID,
							"Could not instantiate saxon: XSLT support is limited to XSLT 1.0", e)); //$NON-NLS-1$
					transformerFactory = TransformerFactory.newInstance();
				}
			}
			return transformerFactory;
		}
		
		public boolean supportsXSLT2() {
			return getTransformerFactory().getClass().getCanonicalName().startsWith("net.sf.saxon"); //$NON-NLS-1$
		}

		public Templates getStylesheet(final String uri) throws ExecutionException {
			return cache.get(uri);
		}
		
		public Templates getStylesheet(final Source source) throws ExecutionException {
			return cache.get(source.getSystemId(), new Callable<Templates>() {

				@Override
				public Templates call() throws Exception {
					return getTransformerFactory().newTemplates(source);
				}
			});
		}

		public Templates getAssociatedStylesheet(String document)
				throws TransformerConfigurationException, ExecutionException {
			final StringReader anaReader = new StringReader(document);
			final StreamSource anaSource = new StreamSource(anaReader);
			final Source stylesheetSource = getTransformerFactory()
					.getAssociatedStylesheet(anaSource, null, null, null);
			return getStylesheet(stylesheetSource);
		}
		
		public LabErrorListener getErrorListener() {
			return errorListener;
		}
		
		public StylesheetManager() {
			errorListener = new LabErrorListener();
			cache = CacheBuilder.newBuilder()
					.recordStats()
					.expireAfterAccess(30, TimeUnit.MINUTES)
					.maximumSize(5)
					.removalListener(new RemovalListener<String, Templates>() {

						@Override
						public void onRemoval(RemovalNotification<String, Templates> notification) {
							StatusManager.getManager().handle(new Status(IStatus.INFO, Activator.PLUGIN_ID, notification.toString()));
						}
					})
					.build(new CacheLoader<String, Templates>() {

				@Override
				public Templates load(String systemId) throws Exception {
					Templates templates = getTransformerFactory().newTemplates(new StreamSource(systemId));
					StatusManager.getManager().handle(new Status(IStatus.INFO, Activator.PLUGIN_ID, "Loaded XSLT stylesheet from " + systemId));
					return templates;
				}
			});
		}

		public void clear() {
			cache.invalidateAll();
		}
	}
	
	private QName getRootElement(final StreamSource streamSource, IProgressMonitor monitor) throws XMLStreamException {
		SubMonitor progress = SubMonitor.convert(monitor, 5);
		XMLInputFactory factory = XMLInputFactory.newInstance();
		factory.setProperty(XMLInputFactory.IS_SUPPORTING_EXTERNAL_ENTITIES, false);
		factory.setProperty(XMLInputFactory.SUPPORT_DTD, false);
		XMLStreamReader reader = factory.createXMLStreamReader(streamSource);
		progress.worked(2);
		reader.nextTag(); // will move to the document element
		progress.worked(3);
		return reader.getName();
	}

}
