/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2004 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.core;

/**
 * Toolkit-independent specifier of a font. This class does not encapsulate
 * an actual font, but simply the information needed
 * for the toolkit to find an actual font.
 * 
 * <p>An array of font family names may be specified. If more than one name
 * is specified, the toolkit should select the first name that matches an
 * actual font on the platform.</p>
 */
public class FontSpec {
    
    public static final int PLAIN         = 0x0;
    public static final int BOLD          = 1<<0;
    public static final int ITALIC        = 1<<1;
    public static final int UNDERLINE     = 1<<2;
    public static final int OVERLINE      = 1<<3;
    public static final int LINE_THROUGH  = 1<<4;

    private String[] names;
    private float size;
    private int style;

    /**
     * Class constructor.
     * @param names Array of names of the font family.
     * @param style Bitwise-OR of the applicable style flages, e.g. BOLD | ITALIC
     * @param size Size of the font, in points.
     */
    public FontSpec(String[] names, int style, float size) {
        this.names = names;
        this.style = style;
        this.size = size;
    }
    
    /**
     * Returns the names of the font families that match the font.
     */
    public String[] getNames() {
        return names;
    }

    /**
     * Returns the size of the font in points.
     */
    public float getSize() {
        return size;
    }

    /**
     * Returns a bitwise-OR of the style flags. The following sample checks if
     * the font is bold.
     * 
     * <pre>
     * if (font.getStyle | VexFont.BOLD) {
     *     // do something bold...
     * }
     * </pre>
     */
    public int getStyle() {
        return style;
    }

}
