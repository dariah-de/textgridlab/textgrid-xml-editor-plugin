package net.sf.vex.core;

/**
 * A convenience class to report warnings and errors. Call
 * <code>setWarningOutput</code> and <code>setErrorOutput</code> early to
 * redirect messages where you want them.
 * 
 * @author Moritz Wissenbach
 * 
 */
public class Log {

	public interface MessageOutput {

		public void log(String message);

		public void log(String message, Throwable cause);
	};

	// Standard implementations
	private static MessageOutput warnings = new MessageOutput() {

		public void log(String message) {
			System.err.println(message);
		}

		public void log(String message, Throwable cause) {
			System.err.print(message + " caused by: ");
			cause.printStackTrace(System.err);
		}

	};

	private static MessageOutput errors = new MessageOutput() {
		public void log(String message) {
			System.err.println(message);

		}

		public void log(String message, Throwable cause) {
			System.err.print(message + " caused by: ");
			cause.printStackTrace(System.err);

		}
	};

	/**
	 * Specify how to report warnings.
	 * 
	 * @param warnings
	 */
	public static void setWarningOutput(MessageOutput warnings) {
		Log.warnings = warnings;
	}

	/**
	 * Specify how to report errors
	 * 
	 * @param warnings
	 */
	public static void setErrorOutput(MessageOutput errors) {
		Log.errors = errors;
	}

	public static void logWarning(String message) {
		warnings.log(message);
	}

	public static void logWarning(String message, Throwable cause) {
		warnings.log(message, cause);
	}

	public static void logError(String message) {
		errors.log(message);
	}

	public static void logError(String message, Throwable cause) {
		errors.log(message, cause);
	}

}
