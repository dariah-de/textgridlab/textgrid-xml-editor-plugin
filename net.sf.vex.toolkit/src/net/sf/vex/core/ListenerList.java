/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2005 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.core;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EventObject;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import net.sf.vex.VexToolkitPlugin;

import org.eclipse.core.runtime.IStatus;

/**
 * A collection of listener objects. The main point of this class is the 
 * fireEvent method, which takes care of the
 * tedium of iterating over the collection and catching exceptions generated
 * by listeners.
 */
public class ListenerList {

    /**
     * Class constructor.
     * @param listenerClass Class of the listener interface.
     * @param eventClass Class of the event objects passed to methods in the
     * listener interface.
     */
    public ListenerList(Class<?> listenerClass, Class<?> eventClass) {
        this.listenerClass = listenerClass;
        this.methodParams = new Class[] { eventClass };
    }
    
    /**
     * Adds a listener to the list. Rejects listeners that are not subclasses
     * of the listener class passed to the constructor.
     * @param listener Listener to be added.
     */
    public void add(Object listener) {
        if (!listenerClass.isInstance(listener)) {
            this.handleException(new IllegalArgumentException("" + listener
					+ " is not an instance of " + listenerClass),
					"Tried to add an invalid object to ListenerList");
        }
        this.listeners.add(listener);
    }
    
    /**
     * Calls the given method on each registered listener. Any exception
     * thrown from one of the called methods is passed to handleException, as
     * is any introspection error, e.g. if the given method doesn't exist.
     * 
     * @param methodName Listener method to call.
     * @param event Event to be passed to each call.
     */
    public void fireEvent(String methodName, EventObject event) {
        
        Method method = this.methods.get(methodName);
        if (method == null) {
            try {
                method = listenerClass.getMethod(methodName, methodParams);
                this.methods.put(methodName, method);
            } catch (Exception e) {
                this.handleException(e,
						"Could not extract method {0}({1}) from {2}",
						methodName, methodParams, listenerClass);
                return;
            }
        }
        
        Object[] args = new Object[] { event };
        for (Iterator<Object> it = this.listeners.iterator(); it.hasNext();) {
            Object listener = it.next();
            try {
                method.invoke(listener, args);
            } catch (Exception ex) {
                this.handleException(ex,
						"An {0} occured while calling {1} with listener {2}",
						ex.getClass().getSimpleName(), method, listener);
            }
        }
        
    }
    
    /**
     * Called from fireEvent whenever a called listener method throws an 
     * exception, or if there is a problem looking up the listener method
     * by reflection. By default, simply prints the stack trace to stdout.
     * Clients may override this method to provide a more suitable 
     * implementation. 
     * @param ex Exception thrown by the listener method.
     */
    public void handleException(Exception ex, String message, Object... args) {
		VexToolkitPlugin.log(IStatus.ERROR, ex, message, args);
    }
    
    /**
     * Removes a listener from the list.
     * @param listener Listener to remove.
     */
    public void remove(Object listener) {
        this.listeners.remove(listener);
    }
    
    //====================================================== PRIVATE
    
    private Class<?> listenerClass;
	private Class<?>[] methodParams;
    private Collection<Object> listeners = new ArrayList<Object>();
    
    // map methodName => Method object
    private Map<String, Method> methods = new HashMap<String, Method>();
}
