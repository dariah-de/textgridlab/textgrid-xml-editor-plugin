/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2004 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.core;

/**
 * Toolkit-independent insets.
 */
public class Insets {
    
    private int top;
    private int left;
    private int bottom;
    private int right;

    /** Zero insets */
    public static final Insets ZERO_INSETS = new Insets(0, 0, 0, 0);
    
    /**
     * Class constructor.
     * 
     * @param top Top inset.
     * @param left Left inset.
     * @param bottom Bottom inset.
     * @param right Right inset.
     */
    public Insets(int top, int left, int bottom, int right) {
        this.top = top;
        this.left = left;
        this.bottom = bottom;
        this.right = right;
    }

    /**
     * @return Returns the top.
     */
    public int getTop() {
        return top;
    }

    /**
     * @return Returns the left.
     */
    public int getLeft() {
        return left;
    }

    /**
     * @return Returns the bottom.
     */
    public int getBottom() {
        return bottom;
    }

    /**
     * Returns the right inset.
     */
    public int getRight() {
        return right;
    }

    public String toString() {
        StringBuffer sb = new StringBuffer(80);
        sb.append(Insets.class.getName());
        sb.append("[top=");
        sb.append(this.getTop());
        sb.append(",left=");
        sb.append(this.getLeft());
        sb.append(",bottom=");
        sb.append(this.getBottom());
        sb.append(",right=");
        sb.append(this.getRight());
        sb.append("]");
        return sb.toString();
    }

}
