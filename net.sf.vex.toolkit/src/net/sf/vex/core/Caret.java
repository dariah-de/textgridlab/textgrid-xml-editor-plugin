/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2004 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.core;

/**
 * Represents the caret, a line that indicates an insertion point in the 
 * document.
 */
public abstract class Caret {

    private int x;
    private int y;
    
    /**
     * Class constructor
     * @param x x-coordinate of the top left corner of the caret
     * @param y y-coordinate of the top left corner of the caret
     */
    public Caret(int x, int y) {
        this.x = x;
        this.y = y;
    }
    
    /**
     * Draws the caret in the given Graphics context.
     * @param g Graphics within which the caret should be drawn.
     * @param color Color with which the caret should be drawn.
     */
    public abstract void draw(Graphics g, Color color);
    
    /**
     * Returns the smallest rectangle that completely encloses the caret.
     */
    public abstract Rectangle getBounds();
    
    /**
     * Returns the x-coordinate of the top left corner of the caret
     */
    public int getX() {
        return this.x;
    }
   
    /**
     * Returns the y-coordinate of the top left corner of the caret
     */
    public int getY() {
        return this.y;
    }
   
    /**
     * Moves the caret by the given x and y distance.
     * @param x amount by which to move the caret to the right
     * @param y amount by which to move the caret down
     */
    public void translate(int x, int y) {
        this.x += x;
        this.y += y;
    }
}
