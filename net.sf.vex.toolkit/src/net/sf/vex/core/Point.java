/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2004 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.core;

/**
 * Toolkit-independent point.
 */
public class Point {
    
    private int x;
    private int y;

    /**
     * Class constructor.
     * @param x X-coordinate.
     * @param y Y-coordinate.
     */
    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }
    
    public String toString() {
        StringBuffer sb = new StringBuffer(80);
        sb.append(Point.class.getName());
        sb.append("[x=");
        sb.append(this.getX());
        sb.append(",y=");
        sb.append(this.getY());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Returns the x-coordinate.
     */
    public int getX() {
        return x;
    }

    /**
     * Returns the y-coordinate.
     */
    public int getY() {
        return y;
    }

}
