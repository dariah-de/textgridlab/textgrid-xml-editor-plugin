package net.sf.vex.core;

/**
 * Options on a per-editor-level
 */
public class EditorOptions {

	public enum InlineMarkerStyle {
		Unlabeled, Labeled
	};

	public enum BlockMarkerStyle {
		InlineStyle, Nested, Wide
	};
	
	private InlineMarkerStyle inlineMarkerStyle = InlineMarkerStyle.Labeled;
	
	private BlockMarkerStyle blockMarkerStyle = BlockMarkerStyle.Nested;

	private boolean showInlineMarkers = false;
	
	private boolean showBlockMarkers = false;
	
	
	public InlineMarkerStyle getInlineMarkerStyle() {
		return inlineMarkerStyle;
	}

	public void setInlineMarkerStyle(InlineMarkerStyle inlineMarkerStyle) {
		this.inlineMarkerStyle = inlineMarkerStyle;
	}

	public BlockMarkerStyle getBlockMarkerStyle() {
		return blockMarkerStyle;
	}

	public void setBlockMarkerStyle(BlockMarkerStyle blockMarkerStyle) {
		this.blockMarkerStyle = blockMarkerStyle;
	}

	public boolean showInlineMarkers() {
		return showInlineMarkers;
	}

	public void setShowInlineMarkers(boolean showInlineMarkers) {
		this.showInlineMarkers = showInlineMarkers;
	}

	public boolean showBlockMarkers() {
		return showBlockMarkers;
	}

	public void setShowBlockMarkers(boolean showBlockMarkers) {
		this.showBlockMarkers = showBlockMarkers;
	}
	
}
