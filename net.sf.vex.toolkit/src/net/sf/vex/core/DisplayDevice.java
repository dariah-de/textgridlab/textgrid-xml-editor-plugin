/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2005 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.core;

/**
 * Represents a device that can display graphics. This class is subclassed
 * for each target system.
 */
public abstract class DisplayDevice {

    /**
     * Class constructor. 
     */
    public DisplayDevice() {
    }

    /**
     * Returns the current display device.
     */
    public static DisplayDevice getCurrent() {
        return current;
    }
    /**
     * Returns the horizontal resolution of the device, in pixels-per-inch.
     */
    public abstract int getHorizontalPPI();
    

    /**
     * Returns the horizontal resolution of the device, in pixels-per-inch.
     */
    public abstract int getVerticalPPI();

    
    /**
     * Sets the current display device. This is typically called by the 
     * platform-specific widget; 
     * @param current The device to use as the current device.
     */
    public static void setCurrent(DisplayDevice current) {
        DisplayDevice.current = current;
    }
    
    //======================================================= PRIVATE
    
    private static DisplayDevice current;
}
