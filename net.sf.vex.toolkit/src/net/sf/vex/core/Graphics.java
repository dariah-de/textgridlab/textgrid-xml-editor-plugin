/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2004 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.core;

import org.eclipse.swt.graphics.Image;


/**
 * Interface through which Vex performs graphics operations. Implemented
 * by adapters to the java.awt.Graphics and org.eclipse.swt.graphics.GC
 * classes.
 */
public interface Graphics {
    
    public static final int LINE_SOLID = 0;
    public static final int LINE_DASH = 1;
    public static final int LINE_DOT = 2;
    
    public int charsWidth(char[] data, int offset, int length);
    public ColorResource createColor(Color rgb);
    public FontResource createFont(FontSpec fontSpec);
    public void dispose();
    public void drawChars(char[] chars, int offset, int length, int x, int y);
    public void drawLine(int x1, int y1, int x2, int y2);
    
    /**
     * Draw the given string at the given point using the current font.
     * @param s string to draw
     * @param x x-coordinate of the top left corner of the text box
     * @param y y-coordinate of the top left corner of the text box
     */
    public void drawString(String s, int x, int y);
    public void drawOval(int x, int y, int width, int height);
    public void drawRect(int x, int y, int width, int height);
    public void fillOval(int x, int y, int width, int height);
    public void fillRect(int x, int y, int width, int height);
    public Rectangle getClipBounds();
    public ColorResource getColor();
    public FontResource getFont();
    public int getLineStyle();
    public int getLineWidth();
    public ColorResource getSystemColor(int id);
    public FontMetrics getFontMetrics();
    public boolean isAntiAliased();
    public void setAntiAliased(boolean antiAliased);
    public ColorResource setColor(ColorResource color);
    public FontResource setFont(FontResource font);
    public void setLineStyle(int style);
    public void setLineWidth(int width);
    
    public int stringWidth(String s);
    
    /*
     * draws image from an image element e.g: <image src="xyz"/>
     */
    public void drawImage(Image image, int x, int y, int width, int height);
    
    
}
