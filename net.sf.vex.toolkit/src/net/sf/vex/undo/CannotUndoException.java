/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2004 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.undo;

/**
 * Thrown when an IUndoableEdit cannot be undone.
 */
public class CannotUndoException extends RuntimeException {

    /**
     * Class constructor. 
     */
    public CannotUndoException() {
    }

    /**
     * Class constructor.
     * @param message Message indicating the reason for the failure.
     */
    public CannotUndoException(String message) {
        super(message);
    }

    /**
     * Class constructor.
     * @param cause Root cause of the failure.
     */
    public CannotUndoException(Throwable cause) {
        super(cause);
    }

    /**
     * Class constructor.
     * @param message Message indicating the reason for the failure.
     * @param cause Root cause of the failure.
     */
    public CannotUndoException(String message, Throwable cause) {
        super(message, cause);
    }

}
