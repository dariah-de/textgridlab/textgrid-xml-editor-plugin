/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2004 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.undo;

import java.util.ArrayList;
import java.util.List;

/**
 * An undoable edit that is a composite of others.
 */
public class CompoundEdit implements IUndoableEdit {

    /**
     * Class constructor.
     */
    public CompoundEdit() {
    }

    /**
     * Adds an edit to the list.
     * @param edit Edit to be undone/redone as part of the compound group.
     */
    public void addEdit(IUndoableEdit edit) {
        edits.add(edit);
    }
    
    public boolean combine(IUndoableEdit edit) {
        return false;
    }

    /**
     * Calls redo() on each contained edit, in the order that they were added.
     */
    public void redo() {
        for (int i = 0; i < this.edits.size(); i++) {
            IUndoableEdit edit = (IUndoableEdit) this.edits.get(i);
            edit.redo();
        }
    }

    /**
     * Calls undo() on each contained edit, in reverse order from which they
     * were added.
     */
    public void undo() {
        for (int i = this.edits.size() - 1; i >= 0; i--) {
            IUndoableEdit edit = (IUndoableEdit) this.edits.get(i);
            edit.undo();
        }
    }

    //===================================================== PRIVATE
    
    private List edits = new ArrayList();
}
