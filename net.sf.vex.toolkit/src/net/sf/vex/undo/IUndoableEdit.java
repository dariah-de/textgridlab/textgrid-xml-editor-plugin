/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2004 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.undo;

/**
 * Represents a change to a document (an edit) that can be undone and redone.
 * Typically, the edit source (i.e. the document) will have a flag that is set 
 * by the edit to indicate that the edits being performed are part of an undo
 * or redo. The document can use this to supress events to any 
 * IUndoableEventListeners during undo/redo. 
 */
public interface IUndoableEdit {

    /**
     * Try to combine the given edit event with this one. The common use-case
     * involves a user typing sequential characters into the document: all 
     * such insertions should be undone in one go.
     * 
     * @param edit IUndoableEdit to be combined with this one.
     * @return True if the given edit was successfully combined into this one.
     */
    public boolean combine(IUndoableEdit edit);
    
    /**
     * Redo the edit.
     */
    public void redo() throws CannotRedoException;

    /**
     * Undo the edit.
     */
    public void undo() throws CannotUndoException;
}
