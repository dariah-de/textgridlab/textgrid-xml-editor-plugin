/*
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2006 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

package net.sf.vex.swing;

import java.awt.Toolkit;

import net.sf.vex.core.DisplayDevice;

/**
 * Swing implementation of the Display Device abstract class
 * @author Vincent Lambert, Matrox Imaging
 */
public class AwtDisplayDevice extends DisplayDevice{
    
    private boolean loaded = false;
    private int horizontalPPI = 72;
    private int verticalPPI = 72;
    
    /** Creates a new instance of AwtDisplayDevice */
    public AwtDisplayDevice() {}
    
    public int getHorizontalPPI(){
        if (!this.loaded) {
            this.load();
        }
        return this.horizontalPPI;
    }
    
    public int getVerticalPPI(){
        if (!this.loaded) {
            this.load();
        }
        return this.verticalPPI;
    }
    
    private void load() {
        Toolkit tk = Toolkit.getDefaultToolkit();
        
        this.horizontalPPI = tk.getScreenResolution();
        this.verticalPPI = tk.getScreenResolution();
        this.loaded = true;
    }
}
