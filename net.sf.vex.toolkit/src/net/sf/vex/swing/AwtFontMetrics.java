/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2004 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.swing;

import net.sf.vex.core.FontMetrics;

/**
 * Wrapper for the AWT FontMetrics class.
 */
public class AwtFontMetrics implements FontMetrics {
    
    private java.awt.FontMetrics awtFontMetrics;

    public AwtFontMetrics(java.awt.FontMetrics awtFontMetrics) {
        this.awtFontMetrics = awtFontMetrics;
    }
    
    /**
     * @see net.sf.vex.core.FontMetrics#getAscent()
     */
    public int getAscent() {
        return this.awtFontMetrics.getAscent();
    }

    /**
     * @see net.sf.vex.core.FontMetrics#getDescent()
     */
    public int getDescent() {
        return this.awtFontMetrics.getDescent();
    }

    /**
     * @see net.sf.vex.core.FontMetrics#getHeight()
     */
    public int getHeight() {
        return this.awtFontMetrics.getHeight();
    }

    /**
     * @see net.sf.vex.core.FontMetrics#getLeading()
     */
    public int getLeading() {
        return this.awtFontMetrics.getLeading();
    }

}
