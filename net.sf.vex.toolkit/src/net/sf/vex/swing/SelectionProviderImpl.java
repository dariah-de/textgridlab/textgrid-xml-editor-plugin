/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2004 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.swing;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Implementation of the {@link SelectionProvider} interface. Also acts as
 * a selection event multiplexor: any events received on its 
 * {@link SelectionListener} interface are relayed to any registered listeners. 
 */
public class SelectionProviderImpl 
    implements SelectionProvider, SelectionListener {

    private List listeners = new ArrayList();
    
    /**
     * @see net.sf.vex.core.SelectionProvider#addSelectionChangeListener(net.sf.vex.SelectionListener)
     */
    public void addSelectionListener(SelectionListener listener) {
        this.listeners.add(listener);

    }

    /**
     * Call <code>selectionChanged</code> on all registered listeners.
     * @param selection Selection that has changed.
     */
    public void fireSelectionChanged(Selection selection) {
        for (Iterator iter = listeners.iterator(); iter.hasNext(); ) {
            SelectionListener listener = (SelectionListener) iter.next();
            //long start = System.currentTimeMillis();
            listener.selectionChanged(selection);
            //long end = System.currentTimeMillis();
            //System.out.println("" + (end-start) + ": " + listener);
        }
    }
    
    /**
     * @see net.sf.vex.core.SelectionProvider#removeSelectionChangeListener(net.sf.vex.SelectionListener)
     */
    public void removeSelectionListener(SelectionListener listener) {
        this.listeners.remove(listener);
    }

    /**
     * @see net.sf.vex.core.SelectionListener#selectionChanged(net.sf.vex.Selection)
     */
    public void selectionChanged(Selection selection) {
        this.fireSelectionChanged(selection);
    }

}
