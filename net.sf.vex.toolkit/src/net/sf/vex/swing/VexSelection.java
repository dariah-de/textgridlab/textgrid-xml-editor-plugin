/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2004 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.swing;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

import net.sf.vex.dom.IVexDocumentFragment;
import net.sf.vex.dom.impl.DocumentFragment;

/**
 * Represents a selection of a Vex document, which can be viewed as plaintext
 * or as a document fragment.
 */
public class VexSelection extends StringSelection {

    /**
     * DataFlavor representing a Vex document fragment. 
     */
    public static final DataFlavor VEX_DOCUMENT_FRAGMENT_FLAVOR =
        new DataFlavor(DocumentFragment.class, IVexDocumentFragment.MIME_TYPE);

    private DataFlavor[] flavors;        
    private IVexDocumentFragment frag;
    
    /**
     * Class constructor.
     * @param s String representing the selection.
     * @param frag Document fragment representing the selection. 
     */
    public VexSelection(String s, IVexDocumentFragment frag) {
        super(s);
        this.frag = frag;
        
        DataFlavor[] superFlavors = super.getTransferDataFlavors();
        this.flavors = new DataFlavor[superFlavors.length + 1];
        System.arraycopy(superFlavors, 0, this.flavors, 0, superFlavors.length);
        this.flavors[this.flavors.length - 1] = VEX_DOCUMENT_FRAGMENT_FLAVOR;
    }

    public Object getTransferData(DataFlavor flavor)
        throws UnsupportedFlavorException, IOException {
            
        if (flavor.equals(VEX_DOCUMENT_FRAGMENT_FLAVOR)) {
            return this.frag;
        } else {
            return super.getTransferData(flavor);
        }
    }

    public DataFlavor[] getTransferDataFlavors() {
        return this.flavors;
    }

    public boolean isDataFlavorSupported(DataFlavor flavor) {
        if (flavor.equals(VEX_DOCUMENT_FRAGMENT_FLAVOR)) {
            return true;
        } else {
            return super.isDataFlavorSupported(flavor);
        }
    }

}
