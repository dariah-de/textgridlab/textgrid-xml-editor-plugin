/**
 * The Vex Toolkit plugin contains all WYSIWYM page and Vex relevant code except
 * for the immediate editor stuff (Vex Toolkit and Vex Editor are separated due
 * to the Swing based implementation that has existed in earlier standalone Vex
 * versions).
 * 
 * <h3>Packages</h3>
 * <ul>
 * <li>{@link net.sf.vex}</li>
 * <li>{@link net.sf.vex.action} – actions for text operations</li>
 * <li>{@link net.sf.vex.action.linked} … and its correspondant for the linked
 * data model</li>
 * <li>{@link net.sf.vex.core} – basic classes like
 * {@link javax.swing.text.Caret}, {@link java.awt.Rectangle},
 * {@link net.sf.vex.core.EditorOptions} etc.</li>
 * <li>{@link net.sf.vex.css} – Vex' CSS implementation</li>
 * <li>{@link net.sf.vex.dom} – Vex' data model (mainly interfaces)</li>
 * <li>{@link net.sf.vex.dom.impl} – Native implementation of Vex' data model</li>
 * <li><strong>{@link net.sf.vex.dom.linked} – implementation of Vex' data model
 * linked to WST's DOM</strong></li>
 * <li><strong>{@link net.sf.vex.layout} – layouting code for Vex</strong></li>
 * <li>{@link net.sf.vex.swing} – Swing specific UI, not relevant for TextGrid</li>
 * <li>{@link net.sf.vex.swt} – SWT specific UI</li>
 * <li>{@link net.sf.vex.undo} – Vex' undo implementation. Not used currently,
 * we want WST's model.</li>
 * <li>{@link net.sf.vex.widget} – The editor widget and helper classes.</li>
 * </ul>
 * 
 */
package net.sf.vex.toolkit;

