/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2004 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.layout;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.eclipse.swt.SWT;

import net.sf.vex.core.FontSpec;
import net.sf.vex.core.IntRange;
import net.sf.vex.css.CSS;
import net.sf.vex.css.StyleSheet;
import net.sf.vex.css.Styles;
import net.sf.vex.dom.Content;
import net.sf.vex.dom.IVexElement;
import net.sf.vex.dom.IVexNode;

/**
 * Tools for layout and rendering of CSS-styled boxes
 */
public class LayoutUtils {

    /**
     * Create a List of generated inline boxes for the given pseudo-element.
     * @param context LayoutContext in use
     * @param pseudoElement Element representing the generated content.
     */
		
    public static List createGeneratedInlines(LayoutContext context, IVexElement pseudoElement) {
        String text = getGeneratedContent(context, pseudoElement);
        List list = new ArrayList();
   
                 	
        if (text.length() > 0) {
        	list.add(new StaticTextBox(context, pseudoElement, text));
        }
        
        return list;
    }

    
    
    
    /*
     * for creating first-letter & first-line pseudo-elements boxes
     * 
     */
    
    
    public static List createFirstInlines(LayoutContext context, IVexElement pseudoElement) {
 
    	String text = ""; 
    	List list = new ArrayList();
    	
       if(pseudoElement.getName().equals("first-letter")){
    	   if(pseudoElement.getParent().getChildElements().length<=0){
        	if(pseudoElement.getParent().getText().length() > 1){
        		text = pseudoElement.getParent().getText().substring(1,2);
        	}
    	   }
    	   else{
    		   if(pseudoElement.getParent().getChildElements()[0].getText().length() > 1){
           		text = pseudoElement.getParent().getChildElements()[0].getText().substring(1,2);
           	   }
    	   }

      } 
      else  if(pseudoElement.getName().equals("first-line")){       	
      	  if(pseudoElement.getParent().getText().length() > 1){
      		int dot_pos = pseudoElement.getParent().getText().indexOf("."); 
      		if(dot_pos !=-1)
    		  text = pseudoElement.getParent().getText().substring(1,dot_pos);
    		
    		//System.out.println(text);
    		
    	  }  
      } 	  
          if (text.length() > 0) {
          	list.add(new StaticTextBox(context, pseudoElement, text));
          }
             
        
        return list;
    }
    
    
    
    
    
    /**
     * Returns true if the given offset falls within the given element or range.
     * 
     * @param elementOrRange Element or IntRange object representing a range 
     * of offsets.
     * @param offset Offset to test.
     */
    public static boolean elementOrRangeContains(Object elementOrRange, int offset) {
        if (elementOrRange instanceof IVexElement) {
            IVexElement element = (IVexElement) elementOrRange;
            return offset > element.getStartOffset() && offset <= element.getEndOffset();
        } else {
            IntRange range = (IntRange) elementOrRange;
            return offset >= range.getStart() && offset <= range.getEnd();
        }
    }
    
    /**
     * Creates a string representing the generated content for the given 
     * pseudo-element.
     * @param context LayoutContext in use
     * @param pseudoElement PseudoElement for which the generated content
     * is to be returned.
     */
    private static String getGeneratedContent(LayoutContext context, IVexElement pseudoElement) {
        Styles styles = context.getStyleSheet().getStyles(pseudoElement);
        List content = styles.getContent();
             
        StringBuffer sb = new StringBuffer();
        for (Iterator it = content.iterator(); it.hasNext(); ) {
        	 sb.append((String) it.next()); // TODO: change to ContentPart
            
        }
               
       
        return sb.toString();
    }
    
   
      

    /**
     * Call the given callback for each child matching one of the given
     * display styles. Any nodes that do not match one of the given display types
     * cause the onRange callback to be called, with a range covering all such
     * contiguous nodes.
     * 
     * @param context LayoutContext to use.
     * @param displayStyles Display types to be explicitly recognized.
     * @param element Element containing the children over which to iterate.
     * @param startOffset Starting offset of the range containing nodes in which we're interested.
     * @param endOffset Ending offset of the range containing nodes in which we're interested.
     * @param callback DisplayStyleCallback through which the caller is notified
     * of matching elements and non-matching ranges.
     */
    public static void iterateChildrenByDisplayStyle(StyleSheet styleSheet, Set displayStyles, IVexElement element, int startOffset, int endOffset, ElementOrRangeCallback callback) {
        
        List nonMatching = new ArrayList();
        
        IVexNode[] nodes = element.getChildNodes();
        for (int i = 0; i < nodes.length; i++) {
            if (nodes[i].getEndOffset() <= startOffset) {
                continue;
            } else if (nodes[i].getStartOffset() >= endOffset) {
                break;
            } else {
                IVexNode node = nodes[i];
    
                if (node instanceof IVexElement) {
                    IVexElement childElement = (IVexElement) node;
                    String display = styleSheet.getStyles(childElement).getDisplay();
                    if (displayStyles.contains(display)) {
                        if (nonMatching.size() > 0) {
                            IVexNode firstNode = (IVexNode) nonMatching.get(0);
                            IVexNode lastNode = (IVexNode) nonMatching.get(nonMatching.size() - 1);
                            if (lastNode instanceof IVexElement) {
                                callback.onRange(element, firstNode.getStartOffset(), lastNode.getEndOffset() + 1);
                            } else {
                                callback.onRange(element, firstNode.getStartOffset(), lastNode.getEndOffset());
                            }
                            nonMatching.clear();
                        }
                        callback.onElement(childElement, display);
                    } else {
                        nonMatching.add(node);
                    }
                } else {
                    nonMatching.add(node);
                }
            }
        }
        
        if (nonMatching.size() > 0) {
            IVexNode firstNode = (IVexNode) nonMatching.get(0);
            IVexNode lastNode = (IVexNode) nonMatching.get(nonMatching.size() - 1);
            if (lastNode instanceof IVexElement) {
                callback.onRange(element, firstNode.getStartOffset(), lastNode.getEndOffset() + 1);
            } else {
                callback.onRange(element, firstNode.getStartOffset(), lastNode.getEndOffset());
            }
        }
    }

    /**
     * Call the given callback for each child matching one of the given
     * display styles. Any nodes that do not match one of the given display types
     * cause the onRange callback to be called, with a range covering all such
     * contiguous nodes.
     * 
     * @param context LayoutContext to use.
     * @param displayStyles Display types to be explicitly recognized.
     * @param element Element containing the children over which to iterate.
     * @param callback DisplayStyleCallback through which the caller is notified
     * of matching elements and non-matching ranges.
     */
    public static void iterateChildrenByDisplayStyle(StyleSheet styleSheet, Set displayStyles, IVexElement element, ElementOrRangeCallback callback) {
        iterateChildrenByDisplayStyle(styleSheet, displayStyles, element, element.getStartOffset() + 1, element.getEndOffset(), callback);
    }

    /**
     * Returns true if the given styles represent an element that can be 
     * the child of a table element.
     * 
     * @param styleSheet StyleSheet to use.
     * @param element Element to test.
     */
    public static boolean isTableChild(StyleSheet styleSheet, IVexElement element) {
        String display = styleSheet.getStyles(element).getDisplay();
        return TABLE_CHILD_STYLES.contains(display);
    }

    public static void iterateTableRows(final StyleSheet styleSheet, final IVexElement element, int startOffset, int endOffset, final ElementOrRangeCallback callback) {
        
        iterateChildrenByDisplayStyle(styleSheet, nonRowStyles, element, startOffset, endOffset, new ElementOrRangeCallback() {
            public void onElement(IVexElement child, String displayStyle) {
                if (displayStyle.equalsIgnoreCase(CSS.TABLE_ROW_GROUP)
                        || displayStyle.equalsIgnoreCase(CSS.TABLE_HEADER_GROUP)
                        || displayStyle.equalsIgnoreCase(CSS.TABLE_FOOTER_GROUP)) {
        
                    // iterate rows in group
                    iterateChildrenByDisplayStyle(styleSheet, rowStyles, child, child.getStartOffset() + 1, child.getEndOffset(), callback);
                } else {
                    // other element's can't contain rows
                }
            }
            public void onRange(IVexElement parent, int startOffset, int endOffset) {
                // iterate over rows in range
                iterateChildrenByDisplayStyle(styleSheet, rowStyles, element, startOffset, endOffset, callback);
            }
        });
    
    }

    public static void iterateTableCells(StyleSheet styleSheet, IVexElement element, int startOffset, int endOffset, final ElementOrRangeCallback callback) {
        iterateChildrenByDisplayStyle(styleSheet, cellStyles, element, startOffset, endOffset, callback);
    }

    public static void iterateTableCells(StyleSheet styleSheet, IVexElement row, final ElementOrRangeCallback callback) {
        iterateChildrenByDisplayStyle(styleSheet, cellStyles, row, row.getStartOffset(), row.getEndOffset(), callback);
    }
    
    /**
     * Set of CSS display values that represent elements that can be children 
     * of table elements.
     */
    public static Set TABLE_CHILD_STYLES = new HashSet();
    
    private static Set nonRowStyles = new HashSet();
    private static Set rowStyles = new HashSet();
    private static Set cellStyles = new HashSet();

    
    static {
        nonRowStyles.add(CSS.TABLE_CAPTION);
        nonRowStyles.add(CSS.TABLE_COLUMN);
        nonRowStyles.add(CSS.TABLE_COLUMN_GROUP);
        nonRowStyles.add(CSS.TABLE_ROW_GROUP);
        nonRowStyles.add(CSS.TABLE_HEADER_GROUP);
        nonRowStyles.add(CSS.TABLE_FOOTER_GROUP);
        
        rowStyles.add(CSS.TABLE_ROW);
        
        cellStyles.add(CSS.TABLE_CELL);
        
        TABLE_CHILD_STYLES.addAll(nonRowStyles);
        TABLE_CHILD_STYLES.addAll(rowStyles);
        TABLE_CHILD_STYLES.addAll(cellStyles);
    }



}
