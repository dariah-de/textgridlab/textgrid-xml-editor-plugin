package net.sf.vex.layout;

import info.textgrid.lab.core.swtutils.AdapterUtils;
import info.textgrid.lab.core.swtutils.IntervalMultimap;
import info.textgrid.lab.core.swtutils.IntervalMultimap.Interval;

import java.text.MessageFormat;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.sf.vex.VexToolkitPlugin;
import net.sf.vex.dom.IVexNode;
import net.sf.vex.dom.linked.LinkedDocument;
import net.sf.vex.dom.linked.LinkedNode;

import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.PlatformObject;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.source.Annotation;
import org.eclipse.jface.text.source.AnnotationModelEvent;
import org.eclipse.jface.text.source.IAnnotationModel;
import org.eclipse.jface.text.source.IAnnotationModelListener;
import org.eclipse.jface.text.source.IAnnotationModelListenerExtension;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.statushandlers.StatusManager;
import org.eclipse.ui.texteditor.IDocumentProvider;
import org.eclipse.wst.sse.core.StructuredModelManager;
import org.eclipse.wst.sse.core.internal.provisional.IStructuredModel;
import org.eclipse.wst.sse.core.internal.provisional.IndexedRegion;
import org.eclipse.wst.sse.core.internal.provisional.text.IStructuredDocument;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
 * Tracks current annotations and their associated Vex nodes. Should be
 * instantiated once for each multi-page editor.
 * 
 * @author vitt
 */
@SuppressWarnings("restriction")
public class VexAnnotationTracker extends PlatformObject implements IAnnotationModelListener, IAnnotationModelListenerExtension {


	final private Object input;
	final private IDocumentProvider provider;
	final private IStructuredDocument document;
	final private IAnnotationModel annotationModel;
	final private BiMap<Annotation, IVexNode> annotationModelMap = HashBiMap.create();
	final private IntervalMultimap<Annotation> annotationPositionMap = new IntervalMultimap<Annotation>();
	final private VexAnnotationSupport support;

	final private Map<Annotation, AnnotationRenderingInfo> renderingInfoCache = Maps.newHashMap();

	/**
	 * Creates a new annotation tracker. Clients must dispose of this class
	 * using the {@link #dispose()} method when they no longer need it.
	 * 
	 * @param input
	 *            the input of the (structured) editor.
	 * @param provider
	 *            the document provider that can provide a
	 *            {@link IStructuredDocument} and an {@link IAnnotationModel}
	 *            for <var>input</var>
	 */
	public VexAnnotationTracker(final Object input, final IDocumentProvider provider) {
		super();
		this.input = input;
		this.provider = provider;

		final IDocument idocument = provider.getDocument(input);
		Assert.isLegal(idocument instanceof IStructuredDocument, "Can only track input that resolves to IStructuredDocuments.");
		document = (IStructuredDocument) idocument;
		annotationModel = provider.getAnnotationModel(input);
		annotationModel.addAnnotationModelListener(this);

		support = new VexAnnotationSupport();
	}

	public void dispose() {
		annotationModel.removeAnnotationModelListener(this);
	}

	public void modelChanged(IAnnotationModel model) {
	}

	public void modelChanged(AnnotationModelEvent event) {

		for (final Annotation annotation : event.getRemovedAnnotations()) {
			removeAnnotation(annotation);
		}
		Annotation[] addedAnnotations = event.getAddedAnnotations();
		Annotation[] changedAnnotations = event.getChangedAnnotations();
		if (addedAnnotations.length > 0 || changedAnnotations.length > 0) {
			IStructuredModel model = StructuredModelManager.getModelManager().getModelForRead(document);
			try {
				for (final Annotation annotation : addedAnnotations)
					putAnnotation(annotation, model);
				for (final Annotation annotation : changedAnnotations)
					putAnnotation(annotation, model);

			} finally {
				model.releaseFromRead();
			}
		}
	}

	private void removeAnnotation(final Annotation annotation) {
		if (VexToolkitPlugin.isDebugging(VexToolkitPlugin.DEBUG_ANNOTATIONS)
				&& annotation.getType().equalsIgnoreCase("org.eclipse.wst.sse.ui.temp.error"))
			System.out.println(MessageFormat.format("Removing a {1} annotation for {0}: {2}", annotationModelMap.get(annotation),
					annotation.getType(), annotation.getText()));
		annotationModelMap.remove(annotation);
		annotationPositionMap.remove(annotation);
		renderingInfoCache.remove(annotation);
	}

	private void putAnnotation(final Annotation annotation,
			final IStructuredModel model) {
		Position position = annotationModel.getPosition(annotation);
		if (position == null)
			position = new Position(0);
		final IndexedRegion region = model.getIndexedRegion(position.offset);
		final IVexNode linkedNode = AdapterUtils.getAdapter(region,
				IVexNode.class);
		if (linkedNode != null) {
			annotationModelMap.forcePut(annotation, linkedNode);
			LinkedDocument linkedDocument = ((LinkedNode) linkedNode).getDocument();
			if (linkedDocument != null) {
				Interval contentInterval = Interval.of(linkedDocument.contentRegionFor(Interval.of(position)));
				annotationPositionMap.add(contentInterval, annotation);
			} else {
				// TODO TG-803: What exactly is the precondition that leads to this situation?
				StatusManager.getManager().handle(
						new Status(
								IStatus.ERROR,
								VexToolkitPlugin.PLUGIN_ID,
								MessageFormat.format(
										"There was a problem adding some annotation: I have not found a linked document corresponding to the node's annotation."
												+ " Please help Thorsten by providing an exact description on how you got here (see TG-803):\n"
												+
						"  Annotation:\t {0}\n" +
						"  Indexed Region:\t {1}" +
 "  Linked Node:\t {2}\n",
										annotation, region, linkedNode), new IllegalStateException()),
						StatusManager.LOG | StatusManager.SHOW);
			}

			if (VexToolkitPlugin
					.isDebugging(VexToolkitPlugin.DEBUG_ANNOTATIONS)
			/*
			 * &&annotation.getType().equalsIgnoreCase(
			 * "org.eclipse.wst.sse.ui.temp.error" )
			 */) {
				System.out.println(MessageFormat.format(
						"Adding a {1} annotation for {0}: {2}", linkedNode,
						annotation.getType(), annotation.getText()));
			}
		} else {
			StatusManager.getManager().handle(new Status(IStatus.WARNING, VexToolkitPlugin.PLUGIN_ID, NLS.bind("Added an annotation for a region that cannot be mapped to a linked node.\n  Annotation: {0}, Region: {1}", annotation, region)));
		}

	}

	public Annotation getAnnotation(final IVexNode node) {
		return annotationModelMap.inverse().get(node);
	}

	/** returns all annotations that overlap with the given content interval. */
	public Set<Annotation> getOverlappingAnnotations(
			final Interval contentInterval) {
		return annotationPositionMap.getOverlappingValues(contentInterval);
	}

	public AnnotationRenderingInfo getRenderingInfo(final Annotation annotation) {
		if (annotation == null)
			return null;
		AnnotationRenderingInfo info = renderingInfoCache.get(annotation);
		if (info == null) {
			info = new AnnotationRenderingInfo(annotation, this,
					annotationModelMap.get(annotation));
			renderingInfoCache.put(annotation, info);
		}
		return info;
	}

	/**
	 * Extracts the rendering info for the given node from the given layout
	 * context, if possible. Return <code>null</code> if not found.
	 * 
	 * This function is merely syntactic sugar to avoid loads of
	 * <code>null</code> checks.
	 */
	public static AnnotationRenderingInfo getRenderingInfo(final LayoutContext context, final IVexNode node) {
		if (context == null || node == null)
			return null;
		VexAnnotationTracker tracker = context.getAnnotationTracker();
		if (tracker == null) { // TG-1104
			StatusManager.getManager().handle(
					new Status(IStatus.ERROR, VexToolkitPlugin.PLUGIN_ID, MessageFormat.format(
							"Trying to get annotation rendering info for node {0} but there is no tracker yet!?", node),
							new IllegalStateException()));
			return null;
		}
		return tracker.getRenderingInfo(tracker.getAnnotation(node));
	}

	public static List<AnnotationRenderingInfo> getRenderingInfo(final LayoutContext context, final IRegion region) {
		if (context == null || region == null)
			return ImmutableList.of();
		
		VexAnnotationTracker tracker = context.getAnnotationTracker();
		if (tracker == null) { // TG-1104
			StatusManager.getManager().handle(
					new Status(IStatus.ERROR, VexToolkitPlugin.PLUGIN_ID, MessageFormat.format(
							"Trying to get annotation rendering info for region {0} but there is no tracker yet!?", region),
							new IllegalStateException()));
			return ImmutableList.of();
		}
		Set<Annotation> annotations = tracker.getOverlappingAnnotations(Interval.of(region));
		List<AnnotationRenderingInfo> result = Lists.newArrayListWithCapacity(annotations.size()); 
		for (Annotation annotation : annotations) {
			result.add(tracker.getRenderingInfo(annotation));
		}
		return result;
	}

	public VexAnnotationSupport getSupport() {
		return support;
	}

	public IAnnotationModel getAnnotationModel() {
		return annotationModel;
	}
}
