package net.sf.vex.layout;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.PlatformObject;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.preference.PreferenceConverter;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.text.IPainter;
import org.eclipse.jface.text.source.Annotation;
import org.eclipse.jface.text.source.AnnotationPainter;
import org.eclipse.jface.text.source.IOverviewRuler;
import org.eclipse.jface.text.source.ISharedTextColors;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.ui.internal.editors.text.EditorsPlugin;
import org.eclipse.ui.texteditor.AnnotationPreference;
import org.eclipse.ui.texteditor.DefaultMarkerAnnotationAccess;
import org.eclipse.ui.texteditor.MarkerAnnotationPreferences;
import org.eclipse.ui.texteditor.SourceViewerDecorationSupport;

import com.google.common.collect.Maps;

/**
 * Helper class to model support for visualizing annotations in the layout
 * context.
 * 
 * This is modeled along {@link SourceViewerDecorationSupport}, and it contains
 * code coming from there. However, we don't support margins, cursor lines, the
 * overview ruler and matching pairs currently.
 * 
 * @author vitt
 * 
 */
@SuppressWarnings("restriction")
public class VexAnnotationSupport extends PlatformObject {



	private IPreferenceStore fPreferenceStore;
	private IPropertyChangeListener fPropertyChangeListener;
	private IOverviewRuler fOverviewRuler;
	private Map<Object, AnnotationPreference> fAnnotationTypeKeyMap = Maps.newLinkedHashMap();
	private IPropertyChangeListener fFontPropertyChangeListener;
	private AnnotationPainter fAnnotationPainter;
	private ISharedTextColors fSharedTextColors; // TODO initialize
	private DefaultMarkerAnnotationAccess access;

	private void loadSupport() {
		MarkerAnnotationPreferences preferences = new MarkerAnnotationPreferences();
		@SuppressWarnings("unchecked")
		List<AnnotationPreference> prefList = preferences.getAnnotationPreferences();
		for (AnnotationPreference annotationPreference : prefList)
			setAnnotationPreference(annotationPreference);
		install(EditorsPlugin.getDefault().getPreferenceStore());
	}

	public VexAnnotationSupport() {
		super();
		access = new DefaultMarkerAnnotationAccess();
		loadSupport();
	}
	/**
	 * Installs this decoration support on the given preference store. It
	 * assumes that this support has completely been configured.
	 * 
	 * @param store
	 *            the preference store
	 * @category undecided
	 */
	protected void install(IPreferenceStore store) {

		fPreferenceStore = store;
		if (fPreferenceStore != null) {
			fPropertyChangeListener = new IPropertyChangeListener() {
				public void propertyChange(PropertyChangeEvent event) {
					handlePreferenceStoreChanged(event);
				}
			};
			fPreferenceStore.addPropertyChangeListener(fPropertyChangeListener);
		}

		updateTextDecorations();
		updateOverviewDecorations();
	}

	/**
	 * Uninstalls this support from the preference store it has previously been
	 * installed on. If there is no such preference store, this call is without
	 * effect.
	 * 
	 * @category undecided
	 */
	protected void uninstall() {

		if (fPreferenceStore != null) {
			fPreferenceStore.removePropertyChangeListener(fPropertyChangeListener);
			fPropertyChangeListener = null;
			fPreferenceStore = null;
		}
	}

	/**
	 * Updates the text decorations for all configured annotation types.
	 * 
	 * @category undecided
	 */
	private void updateTextDecorations() {

		// FIXME ask for VexWidget here?
		// StyledText widget= fSourceViewer.getTextWidget();
		// if (widget == null || widget.isDisposed())
		// return;

		// if (areMatchingCharactersShown())
		// showMatchingCharacters();
		// else
		// hideMatchingCharacters();

		// if (isCursorLineShown())
		// showCursorLine();
		// else
		// hideCursorLine();
		//
		// if (isMarginShown())
		// showMargin();
		// else
		// hideMargin();

		Iterator<Object> e = fAnnotationTypeKeyMap.keySet().iterator();
		while (e.hasNext()) {
			Object type = e.next();
			if (areAnnotationsHighlighted(type) || areAnnotationsShown(type))
				showAnnotations(type, false);
			else
				hideAnnotations(type, false);

		}
		updateAnnotationPainter();
	}

	/**
	 * Returns the annotation decoration style used for the show in text
	 * preference for a given annotation type.
	 * 
	 * @param annotationType
	 *            the annotation type being looked up
	 * @return the decoration style for <code>type</code> or <code>null</code>
	 *         if highlighting
	 * @since 3.0
	 * @category lookup
	 */
	public String getAnnotationDecorationType(String annotationType) {
		if (areAnnotationsHighlighted(annotationType))
			return null;

		if (areAnnotationsShown(annotationType) && fPreferenceStore != null) {
			AnnotationPreference info = getPreference(annotationType);
			if (info != null) {
				String key = info.getTextStylePreferenceKey();
				if (key != null)
					return fPreferenceStore.getString(key);
				// legacy
				return AnnotationPreference.STYLE_SQUIGGLES;
			}
		}
		return AnnotationPreference.STYLE_NONE;
	}

	/**
	 * Updates the annotation overview for all configured annotation types.
	 * 
	 * @category ruler
	 */
	public void updateOverviewDecorations() {
		if (fOverviewRuler != null) {
			Iterator<Object> e = fAnnotationTypeKeyMap.keySet().iterator();
			while (e.hasNext()) {
				Object type = e.next();
				if (isAnnotationOverviewShown(type))
					showAnnotationOverview(type, false);
				else
					hideAnnotationOverview(type, false);
			}
			fOverviewRuler.update();
		}
	}

	/**
	 * Disposes this decoration support. Internally calls <code>uninstall</code>
	 * .
	 */
	public void dispose() {
		uninstall();
		updateTextDecorations();
		updateOverviewDecorations();

		if (fFontPropertyChangeListener != null) {
			JFaceResources.getFontRegistry().removeListener(fFontPropertyChangeListener);
			fFontPropertyChangeListener = null;
		}

		fOverviewRuler = null;

		// Painters got disposed in updateTextDecorations() or by the
		// PaintManager
		fAnnotationPainter = null;

		if (fAnnotationTypeKeyMap != null)
			fAnnotationTypeKeyMap.clear();
	}

	/**
	 * Sets the preference keys for the annotation painter.
	 * 
	 * @param type
	 *            the annotation type
	 * @param colorKey
	 *            the preference key for the color
	 * @param editorKey
	 *            the preference key for the presentation in the text area
	 * @param overviewRulerKey
	 *            the preference key for the presentation in the overview ruler
	 * @param layer
	 *            the layer
	 */
	public void setAnnotationPainterPreferenceKeys(Object type, String colorKey, String editorKey, String overviewRulerKey,
			int layer) {
		AnnotationPreference info = new AnnotationPreference(type, colorKey, editorKey, overviewRulerKey, layer);
		fAnnotationTypeKeyMap.put(type, info);
	}

	/**
	 * Sets the preference info for the annotation painter.
	 * 
	 * @param info
	 *            the preference info to be set
	 */
	public void setAnnotationPreference(AnnotationPreference info) {
		fAnnotationTypeKeyMap.put(info.getAnnotationType(), info);
	}

	/**
	 * Returns the annotation preference for the given key.
	 * 
	 * @param preferenceKey
	 *            the preference key string
	 * @return the annotation preference
	 */
	private AnnotationPreference getAnnotationPreferenceInfo(String preferenceKey) {
		Iterator<AnnotationPreference> e = fAnnotationTypeKeyMap.values().iterator();
		while (e.hasNext()) {
			AnnotationPreference info = e.next();
			if (info != null && info.isPreferenceKey(preferenceKey))
				return info;
		}
		return null;
	}

	/*
	 * @see AbstractTextEditor#handlePreferenceStoreChanged(PropertyChangeEvent)
	 */
	protected void handlePreferenceStoreChanged(PropertyChangeEvent event) {

		String p = event.getProperty();

		AnnotationPreference info = getAnnotationPreferenceInfo(p);
		if (info != null) {

			if (info.getColorPreferenceKey().equals(p)) {
				Color color = getColor(info.getColorPreferenceKey());
				if (fAnnotationPainter != null) {
					fAnnotationPainter.setAnnotationTypeColor(info.getAnnotationType(), color);
					fAnnotationPainter.paint(IPainter.CONFIGURATION);
				}
				setAnnotationOverviewColor(info.getAnnotationType(), color);
				return;
			}

			Object type = info.getAnnotationType();
			if ((info.getTextPreferenceKey().equals(p) || info.getTextStylePreferenceKey() != null
					&& info.getTextStylePreferenceKey().equals(p))
					|| (info.getHighlightPreferenceKey() != null && info.getHighlightPreferenceKey().equals(p))) {
				if (areAnnotationsHighlighted(type) || areAnnotationsShown(type))
					showAnnotations(type, true);
				else
					hideAnnotations(type, true);
				return;
			}

			if (info.getOverviewRulerPreferenceKey().equals(p)) {
				if (isAnnotationOverviewShown(info.getAnnotationType()))
					showAnnotationOverview(info.getAnnotationType(), true);
				else
					hideAnnotationOverview(info.getAnnotationType(), true);
				return;
			}
		}

	}

	/**
	 * Returns the shared color for the given key.
	 * 
	 * @param key
	 *            the color key string
	 * @return the shared color for the given key
	 */
	private Color getColor(String key) {
		if (fPreferenceStore != null) {
			RGB rgb = PreferenceConverter.getColor(fPreferenceStore, key);
			return getColor(rgb);
		}
		return null;
	}

	/**
	 * Returns the shared color for the given RGB.
	 * 
	 * @param rgb
	 *            the RGB
	 * @return the shared color for the given RGB
	 */
	private Color getColor(RGB rgb) {
		return fSharedTextColors.getColor(rgb);
	}

	/**
	 * Returns the color of the given annotation type.
	 * 
	 * @param annotationType
	 *            the annotation type
	 * @return the color of the annotation type
	 */
	private Color getAnnotationTypeColor(Object annotationType) {
		AnnotationPreference info = fAnnotationTypeKeyMap.get(annotationType);
		if (info != null)
			return getColor(info.getColorPreferenceKey());
		return null;
	}

	/**
	 * Returns the layer of the given annotation type.
	 * 
	 * @param annotationType
	 *            the annotation type
	 * @return the layer
	 */
	private int getAnnotationTypeLayer(Object annotationType) {
		AnnotationPreference info = fAnnotationTypeKeyMap.get(annotationType);
		if (info != null)
			return info.getPresentationLayer();
		return 0;
	}

	/**
	 * Enables annotations in the source viewer for the given annotation type.
	 * 
	 * @param annotationType
	 *            the annotation type
	 * @param updatePainter
	 *            if <code>true</code> update the annotation painter
	 * @since 3.0
	 */
	private void showAnnotations(Object annotationType, boolean updatePainter) {
//		if (fSourceViewer instanceof ITextViewerExtension2) {
//			if (fAnnotationPainter == null) {
//				fAnnotationPainter = createAnnotationPainter();
//				if (fSourceViewer instanceof ITextViewerExtension4)
//					((ITextViewerExtension4) fSourceViewer).addTextPresentationListener(fAnnotationPainter);
//				ITextViewerExtension2 extension = (ITextViewerExtension2) fSourceViewer;
//				extension.addPainter(fAnnotationPainter);
//			}
//			fAnnotationPainter.setAnnotationTypeColor(annotationType, getAnnotationTypeColor(annotationType));
//			Object decorationType = getAnnotationDecorationType(annotationType);
//			if (decorationType != null)
//				fAnnotationPainter.addAnnotationType(annotationType, decorationType);
//			else
//				fAnnotationPainter.addHighlightAnnotationType(annotationType);

			if (updatePainter)
				updateAnnotationPainter();
		// }
	}

	/**
	 * Creates and configures the annotation painter and configures.
	 * 
	 * @return an annotation painter
	 * @since 3.0
	 * @category source TODO we need something Vexy here
	 */
	protected AnnotationPainter createAnnotationPainter() {
		// AnnotationPainter painter = new AnnotationPainter(fSourceViewer,
		// fAnnotationAccess);
		//
		// /*
		// * XXX:
		// * Could provide an extension point for drawing strategies,
		// * see: https://bugs.eclipse.org/bugs/show_bug.cgi?id=51498
		// */
		// painter.addDrawingStrategy(AnnotationPreference.STYLE_NONE,
		// fgNullStrategy);
		// painter.addDrawingStrategy(AnnotationPreference.STYLE_IBEAM,
		// fgIBeamStrategy);
		//
		// painter.addTextStyleStrategy(AnnotationPreference.STYLE_SQUIGGLES,
		// fgSquigglesStrategy);
		// painter.addTextStyleStrategy(AnnotationPreference.STYLE_PROBLEM_UNDERLINE,
		// fgProblemUnderlineStrategy);
		// painter.addTextStyleStrategy(AnnotationPreference.STYLE_BOX,
		// fgBoxStrategy);
		// painter.addTextStyleStrategy(AnnotationPreference.STYLE_DASHED_BOX,
		// fgDashedBoxStrategy);
		// painter.addTextStyleStrategy(AnnotationPreference.STYLE_UNDERLINE,
		// fgUnderlineStrategy);
		//
		// return painter;
		return null;
	}

	/**
	 * Updates the annotation painter.
	 * 
	 * @since 3.0
	 */
	private void updateAnnotationPainter() {
		if (fAnnotationPainter == null)
			return;

		// fAnnotationPainter.paint(IPainter.CONFIGURATION);
		// if (!fAnnotationPainter.isPaintingAnnotations()) {
		// if (fSourceViewer instanceof ITextViewerExtension2) {
		// ITextViewerExtension2 extension = (ITextViewerExtension2)
		// fSourceViewer;
		// extension.removePainter(fAnnotationPainter);
		// }
		// if (fSourceViewer instanceof ITextViewerExtension4)
		// ((ITextViewerExtension4)
		// fSourceViewer).removeTextPresentationListener(fAnnotationPainter);
		//
		// fAnnotationPainter.deactivate(true);
		// fAnnotationPainter.dispose();
		// fAnnotationPainter = null;
		// }
	}

	/**
	 * Hides annotations in the source viewer for the given annotation type.
	 * 
	 * @param annotationType
	 *            the annotation type
	 * @param updatePainter
	 *            if <code>true</code> update the annotation painter
	 * @since 3.0
	 */
	private void hideAnnotations(Object annotationType, boolean updatePainter) {
		if (fAnnotationPainter != null) {
			fAnnotationPainter.removeAnnotationType(annotationType);

			if (updatePainter) {
				updateAnnotationPainter();
			}
		}
	}

	/**
	 * Tells whether annotations are shown in the source viewer for the given
	 * type.
	 * 
	 * @param annotationType
	 *            the annotation type
	 * @return <code>true</code> if the annotations are shown
	 */
	private boolean areAnnotationsShown(Object annotationType) {
		if (fPreferenceStore != null) {
			AnnotationPreference info = getPreference(annotationType);
			if (info != null) {
				String key = info.getTextPreferenceKey();
				return key != null && fPreferenceStore.getBoolean(key);
			}
		}
		return false;
	}

	/**
	 * Tells whether annotations are highlighted in the source viewer for the
	 * given type.
	 * 
	 * @param annotationType
	 *            the annotation type
	 * @return <code>true</code> if the annotations are highlighted
	 * @since 3.0
	 */
	private boolean areAnnotationsHighlighted(Object annotationType) {
		if (fPreferenceStore != null) {
			AnnotationPreference info = fAnnotationTypeKeyMap.get(annotationType);
			if (info != null)
				return info.getHighlightPreferenceKey() != null && fPreferenceStore.getBoolean(info.getHighlightPreferenceKey());
		}
		return false;
	}

	/**
	 * Tells whether annotation overview is enabled for the given type.
	 * 
	 * @param annotationType
	 *            the annotation type
	 * @return <code>true</code> if the annotation overview is shown
	 */
	private boolean isAnnotationOverviewShown(Object annotationType) {
		if (fPreferenceStore != null && fOverviewRuler != null) {
			AnnotationPreference info = fAnnotationTypeKeyMap.get(annotationType);
			if (info != null)
				return fPreferenceStore.getBoolean(info.getOverviewRulerPreferenceKey());
		}
		return false;
	}

	/**
	 * Enable annotation overview for the given annotation type.
	 * 
	 * @param annotationType
	 *            the annotation type
	 * @param update
	 *            <code>true</code> if the overview should be updated
	 */
	private void showAnnotationOverview(Object annotationType, boolean update) {
		if (fOverviewRuler != null) {
			fOverviewRuler.setAnnotationTypeColor(annotationType, getAnnotationTypeColor(annotationType));
			fOverviewRuler.setAnnotationTypeLayer(annotationType, getAnnotationTypeLayer(annotationType));
			fOverviewRuler.addAnnotationType(annotationType);
			if (update)
				fOverviewRuler.update();
		}
	}

	/**
	 * Hides the annotation overview for the given type.
	 * 
	 * @param annotationType
	 *            the annotation type
	 * @param update
	 *            <code>true</code> if the overview should be updated
	 */
	private void hideAnnotationOverview(Object annotationType, boolean update) {
		if (fOverviewRuler != null) {
			fOverviewRuler.removeAnnotationType(annotationType);
			if (update)
				fOverviewRuler.update();
		}
	}

	/**
	 * Hides the annotation overview.
	 */
	public void hideAnnotationOverview() {
		if (fOverviewRuler != null) {
			Iterator<Object> e = fAnnotationTypeKeyMap.keySet().iterator();
			while (e.hasNext())
				fOverviewRuler.removeAnnotationType(e.next());
			fOverviewRuler.update();
		}
	}

	/**
	 * Sets the annotation overview color for the given annotation type.
	 * 
	 * @param annotationType
	 *            the annotation type
	 * @param color
	 *            the color
	 * @category ruler
	 */
	private void setAnnotationOverviewColor(Object annotationType, Color color) {
		if (fOverviewRuler != null) {
			fOverviewRuler.setAnnotationTypeColor(annotationType, color);
			fOverviewRuler.update();
		}
	}

	public AnnotationPreference getPreference(final Object annotationType) {
		AnnotationPreference preference = fAnnotationTypeKeyMap.get(annotationType);
		if (preference == null) {
			Object[] supertypes = getAccess().getSupertypes(annotationType);
			for (Object supertype : supertypes) {
				AnnotationPreference superPreference = fAnnotationTypeKeyMap.get(supertype);
				if (superPreference != null) {
					return superPreference;
				}
			}
		}
		return preference;
	}

	public AnnotationPreference getPreference(final Annotation annotation) {
		return getPreference(annotation.getType());
	}

	public DefaultMarkerAnnotationAccess getAccess() {
		return access;
	}
}
