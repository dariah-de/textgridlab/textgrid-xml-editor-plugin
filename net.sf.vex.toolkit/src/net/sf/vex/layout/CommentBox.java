package net.sf.vex.layout;

import java.util.ArrayList;
import java.util.List;

import org.w3c.css.sac.LexicalUnit;

import net.sf.vex.core.Color;
import net.sf.vex.core.FontSpec;
import net.sf.vex.core.Insets;
import net.sf.vex.core.Rectangle;
import net.sf.vex.css.CSS;
import net.sf.vex.css.ColorProperty;
import net.sf.vex.css.IProperty;
import net.sf.vex.css.Styles;
import net.sf.vex.dom.IVexComment;
import net.sf.vex.dom.IVexElement;
import net.sf.vex.dom.IVexNonElement;
import net.sf.vex.layout.InlineBox.Pair;
import net.sf.vex.css.StyleSheet;
import net.sf.vex.css.RelativeLength;

/**
 * A box representing an XML comment. TODO a lot of copy & paste from
 * InlineElementBox. Extract a new common supertype.
 */
public class CommentBox extends NonElementBox {

	static Styles styles = new Styles() {
		{
			this.put(CSS.FONT_FAMILY, new String[] { "monospace" });
			this.put(CSS.FONT_SIZE, 10f);
			this.put(CSS.BORDER, 1f);
			this.put(CSS.COLOR, new Color(200, 200, 200));
			this.put(CSS.BACKGROUND_COLOR, new Color(165, 165, 0));
			this.put(CSS.BORDER_COLOR, new Color(200, 200, 200));

			this.put(CSS.LINE_HEIGHT, RelativeLength.createAbsolute(Math
					.round(this.getFontSize())));

			this.setFont(new FontSpec(this.getFontFamilies(), FontSpec.PLAIN,
					this.getFontSize()));

		}

	};

	static Styles markerStyles = new Styles() {
		{
			this.put(CSS.FONT_FAMILY, new String[] { "monospace" });
			this.put(CSS.FONT_SIZE, 10f);
			this.put(CSS.BORDER, 1f);
			this.put(CSS.COLOR, new Color(50, 50, 50));
			this.put(CSS.BACKGROUND_COLOR, new Color(165, 165, 0));
			this.put(CSS.BORDER_COLOR, new Color(2000, 200, 200));

			this.put(CSS.LINE_HEIGHT, RelativeLength.createAbsolute(Math
					.round(this.getFontSize())));

			this.setFont(new FontSpec(this.getFontFamilies(), FontSpec.PLAIN,
					this.getFontSize()));

		}

	};

	
	CommentBox(LayoutContext context, IVexComment comment, InlineBox[] children) {

		super(context, comment, children);
	}

	public CommentBox(LayoutContext context, IVexComment comment,
			int startOffset, int endOffset) {
		super(context, comment, startOffset, endOffset);
	}

	@Override
	protected Styles getStyles() {
		return styles;
	}

	@Override
	protected InlineBox getStartMarker(LayoutContext context) {
		return new StaticTextBox(context, element, markerStyles, "<!--",
				StaticTextBox.START_MARKER);
	}

	@Override
	protected InlineBox getEndMarker(LayoutContext context) {
		return new StaticTextBox(context, element, markerStyles, "-->",
				StaticTextBox.END_MARKER);
	}

}
