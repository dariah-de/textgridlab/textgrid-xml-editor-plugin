package net.sf.vex.layout;

import net.sf.vex.core.Color;
import net.sf.vex.core.ColorResource;
import net.sf.vex.core.FontMetrics;
import net.sf.vex.core.FontResource;
import net.sf.vex.core.FontSpec;
import net.sf.vex.core.Graphics;
import net.sf.vex.core.Insets;
import net.sf.vex.core.IntRange;
import net.sf.vex.core.Rectangle;
import net.sf.vex.css.Styles;
import net.sf.vex.dom.IVexElement;

/**
 * Draws a marker for an block element, labeled with the element's name.
 */
public class BlockElementWidget extends AbstractBox implements BlockBox,
		Clickable {

	private LayoutContext context;
	private BlockElementBox parent;

	// Some properties concerning the look

	protected static int size = 15;

	protected static Color fgColor = new Color(255, 255, 255);
	protected static Color bgColor = new Color(150, 150, 255);

	protected static String[] fontFamily = { "sans-serif" };

	/**
	 * 
	 */
	protected int width;

	// A button to collapse/expand the element
	protected static int collapseButtonSize = 11;

	/**
	 * Constructor.
	 * 
	 * @param width
	 * 
	 */
	public BlockElementWidget(LayoutContext context, BlockElementBox parent) {

		this.context = context;
		setWidth(width);
		setHeight(size);
		this.parent = parent;
	}

	public IVexElement getElement() {
		return parent.getElement();
	}

	public void paint(LayoutContext context, int x, int y, Rectangle area) {

		Graphics g = context.getGraphics();

		Styles styles = context.getStyleSheet().getStyles(
				this.parent.getElement());

		boolean drawSelected = false;

		FontMetrics fm = g.getFontMetrics();

		FontResource oldFont = g.getFont();
		ColorResource oldColor = g.getColor();

		FontResource font = context.getGraphics().createFont(
				new FontSpec(fontFamily, FontSpec.PLAIN, (int) (size * 0.8f)));


		// paint background
		ColorResource color;
		AnnotationRenderingInfo rendering = VexAnnotationTracker.getRenderingInfo(context, getElement());
		if (rendering != null && rendering.isShownInText())
			color = g.createColor(rendering.getColor());
		else
			color = g.createColor(bgColor);

		g.setColor(color);
		g.fillRect(x, y, width, size);

		color.dispose();
		color = g.createColor(fgColor);

		if (drawSelected) {
			Rectangle bounds = this.getBounds();
			g.setColor(g.getSystemColor(ColorResource.SELECTION_BACKGROUND));
			g.fillRect(x + bounds.getX(), y, bounds.getWidth(), styles
					.getLineHeight()
					- fm.getDescent());
			g.setColor(g.getSystemColor(ColorResource.SELECTION_FOREGROUND));
		} else {
			g.setColor(color);

		}

		g.setFont(font);

		g.setLineStyle(Graphics.LINE_SOLID);

		g.setLineWidth(1);

		// paint foreground

		int collapseButtonMargin = (size - collapseButtonSize) / 2;

		g.fillRect(x + collapseButtonMargin, y + collapseButtonMargin,
				collapseButtonSize, collapseButtonSize);

		int xPlusOffset = x + (collapseButtonMargin * 2 + collapseButtonSize);

		g.drawString(parent.getElement().getName(), xPlusOffset + 2, y
				+ (int) (size * 0.1f) - 2);

		g.drawLine(x, y, x, y + size);
		g.drawLine(x, y, x + width, y);
		g.drawLine(x, y + size, x + width, y + size);
		g.drawLine(x + width, y, x + width, y + size);

		// paint a "plus" or "minus" on the expand button

		color = g.createColor(bgColor);
		g.setColor(color);

		g.drawLine(x + (2 * collapseButtonMargin), y + collapseButtonMargin
				+ (collapseButtonSize / 2), x + collapseButtonSize, y
				+ collapseButtonMargin + (collapseButtonSize / 2));

		if (context.isCollapsed(parent.getElement())) {
			g.drawLine(x + collapseButtonMargin + (collapseButtonSize / 2), y
					+ (2 * collapseButtonMargin), x + collapseButtonMargin
					+ (collapseButtonSize / 2), y + collapseButtonSize);

		}
		// Reset
		g.setColor(oldColor);
		g.setFont(oldFont);

	}

	public Rectangle getBounds() {
		return new Rectangle(0, size, width + size, size);
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return "[BlockElementWidget]";
	}

	@Override
	public Insets getInsets(LayoutContext context, int containerWidth) {
		// A little space below the marker
		return new Insets(0, 0, 3, 0);
	}

	public void click(int x, int y) {

		if (insideExpandButton(x, y)) {
			boolean collapsed = false;
			if (context.isCollapsed(parent.getElement())) {
				context.expand(parent.getElement());
			} else {
				context.collapse(parent.getElement());
			}

			parent.findRoot().getVexWidget().reLayout();
		}

	}

	private boolean insideExpandButton(int x, int y) {

		int collapseButtonMargin = (size - collapseButtonSize) / 2;
		if (x > collapseButtonMargin
				&& x < collapseButtonMargin + collapseButtonSize
				&& y > collapseButtonMargin
				&& y < collapseButtonMargin + collapseButtonSize)
			return true;
		else
			return false;

	}

	public LineBox getFirstLine() {
		// TODO Auto-generated method stub
		return null;
	}

	public LineBox getLastLine() {
		// TODO Auto-generated method stub
		return null;
	}

	public int getLineEndOffset(int offset) {
		// TODO Auto-generated method stub
		return 0;
	}

	public int getLineStartOffset(int offset) {
		// TODO Auto-generated method stub
		return 0;
	}

	public int getMarginBottom() {
		// TODO Auto-generated method stub
		return 0;
	}

	public int getMarginTop() {
		// TODO Auto-generated method stub
		return 0;
	}

	public int getNextLineOffset(LayoutContext context, int offset, int x) {
		// TODO Auto-generated method stub
		return 0;
	}

	public BlockBox getParent() {
		// TODO Auto-generated method stub
		return null;
	}

	public int getPreviousLineOffset(LayoutContext context, int offset, int x) {
		// TODO Auto-generated method stub
		return 0;
	}

	public void invalidate(boolean direct) {
		// TODO Auto-generated method stub

	}

	public IntRange layout(LayoutContext context, int top, int bottom) {
		// TODO Auto-generated method stub
		return null;
	}

	public void setInitialSize(LayoutContext context) {
		// TODO Auto-generated method stub

	}

	public void setParent(BlockBox parent) {
		// TODO Auto-generated method stub

	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public static void setFgColor(Color fgColor) {
		BlockElementWidget.fgColor = fgColor;
	}

	public static void setBgColor(Color bgColor) {
		BlockElementWidget.bgColor = bgColor;
	}

	@Override
	public boolean hasContent() {
		return false;
	}

}
