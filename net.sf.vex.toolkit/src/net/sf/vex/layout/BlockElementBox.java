/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2004 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.layout;

import java.util.ArrayList;
import java.util.List;

import net.sf.vex.core.Drawable;
import net.sf.vex.core.Graphics;
import net.sf.vex.core.Insets;
import net.sf.vex.core.Rectangle;
import net.sf.vex.css.CSS;
import net.sf.vex.css.StyleSheet;
import net.sf.vex.css.Styles;
import net.sf.vex.dom.IVexElement;

import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

/**
 * A block box corresponding to a DOM Element. Block boxes lay their children
 * out stacked top to bottom. Block boxes correspond to the
 * <code>display: block;</code> CSS property.
 */
public class BlockElementBox extends AbstractBlockBox {

	private static boolean bspSpeedup = true;

	/** hspace btn. list-item bullet and block, as fraction of font-size */
	static final float BULLET_SPACE = 0.5f;

	/** vspace btn. list-item bullet and baseine, as fraction of font-size */
	// private static final float BULLET_LIFT = 0.1f;
	/** number of boxes created since VM startup, for profiling */
	private static int boxCount;

	BlockBox beforeMarker;

	// Shows the elements name, makes it collapsable etc.
	protected BlockBox elementWidget;

	// margins for the display of nesting element boxes
	private final static int MARKER_MARGIN_TOP = 15;

	private final static int MARKER_MARGIN_BOTTOM = 5;

	private final static int MARKER_MARGIN_LEFT = 3;

	private final static int MARKER_MARGIN_RIGHT = 3;

	
	
	static IWorkbenchWindow wind = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
	static Display Idisplay = wind.getShell().getDisplay();
	
	
	/**
	 * Class constructor. This box's children are not created here but in the
	 * first call to layout. Instead, we estimate the box's height here based on
	 * the given width.
	 * 
	 * @param context
	 *            LayoutContext used for this layout.
	 * @param parent
	 *            This box's parent box.
	 * @param element
	 *            Element to which this box corresponds.
	 */
	public BlockElementBox(LayoutContext context, BlockBox parent,
			IVexElement element) {
		super(context, parent, element);
	}

	/**
	 * Returns the number of boxes created since VM startup. Used for profiling.
	 */
	public static int getBoxCount() {
		return boxCount;
	}

	public int getEndOffset() {
		return this.getElement().getEndOffset();
	}

	public int getStartOffset() {
		return this.getElement().getStartOffset() + 1;
	}

	public boolean hasContent() {
		return true;
	}

	public void paint(LayoutContext context, int x, int y, Rectangle area) {

		// Is the element selected?
		boolean paintSelected = context.isElementSelected(this.getElement());

		if (paintSelected)
			// Fill the background
			this.paintSelectionFill(context, x, y);

		if (this.beforeMarker != null) {
			this.beforeMarker.paint(context, x + this.beforeMarker.getX(), y
					+ this.beforeMarker.getY(), area);
		}

		// paint children
		super.paint(context, x, y, area);

		if (context.showBlockMarkers())
			this.paintMarkerFrame(context, x, y, this.getParent().getWidth());

		if (paintSelected)
			paintSelectionFrame(context, x, y, this.getParent().getWidth());
	}

	protected int positionChildren(LayoutContext context) {

		int repaintStart = super.positionChildren(context);

		Styles styles = context.getStyleSheet().getStyles(this.getElement());
		if (this.beforeMarker != null) {
			int x = -this.beforeMarker.getWidth()
					- Math.round(BULLET_SPACE * styles.getFontSize());
			int y = this.getFirstLineTop(context);
			LineBox firstLine = this.getFirstLine();
			if (firstLine != null) {
				y += firstLine.getBaseline()
						- this.beforeMarker.getFirstLine().getBaseline();
			}

			this.beforeMarker.setX(x);
			this.beforeMarker.setY(y);
		}
		// FIXME bad hack! this depends on details of the layout implementation
		// somewhere else, thus may break easily (yay just happend!).
		// Think of a better solution.

		if (elementWidget != null) {
			int paddingLeft = styles.getPaddingLeft().get(this.getWidth());
			int paddingRight = styles.getPaddingRight().get(this.getWidth());
			int paddingTop = styles.getPaddingTop().get(this.getHeight());

			this.elementWidget.setX(-paddingRight);
			this.elementWidget.setY(-paddingTop);

			this.elementWidget.setWidth(this.getWidth() + paddingLeft
					+ paddingRight);
		}

		return repaintStart;
	}

	public String toString() {
		return "BlockElementBox: <" + this.getElement().getName() + ">" + "("
				+ getStartOffset() + ", " + getEndOffset() + ", "
				+ hasContent() + ")" + "[x=" + this.getX() + ",y="
				+ this.getY() + ",width=" + this.getWidth() + ",height="
				+ this.getHeight() + "]";
	}

	// ===================================================== PRIVATE

	/**
	 * Returns the space that a marker box for the block-formatted element
	 * takes. If marker display is disabled, it returns zero insets.
	 * 
	 * @param context
	 *            The Layout context
	 */
	public static Insets getMarkerInsets(LayoutContext context) {

		// Returns constants now, might return something dynamic if marker boxes
		// become customizable
		if (context.showBlockMarkers())
			return new Insets(MARKER_MARGIN_TOP, MARKER_MARGIN_LEFT,
					MARKER_MARGIN_BOTTOM, MARKER_MARGIN_RIGHT);
		else
			return Insets.ZERO_INSETS;
	}

	/**
	 * Lays out the children as vertically stacked blocks. Runs of text and
	 * inline elements are wrapped in DummyBlockBox's.
	 */
	
	protected List createChildren(LayoutContext context) {

		
		long start = System.currentTimeMillis();

		IVexElement element = this.getElement();
		int width = this.getWidth();

		context.subTask("Creating children for {0}", element.getName());

		List childList = new ArrayList();

		if (context.showBlockMarkers()) {
			this.elementWidget = new BlockElementWidget(context, this);
			elementWidget.setWidth(width);			
			childList.add(elementWidget);
		}

		if (context.isCollapsed(this.getElement())) {
			// the element is collapsed, do not draw children
		} else {

			StyleSheet ss = context.getStyleSheet();
			

			
			// element and styles for generated boxes
			IVexElement genElement;
			Styles genStyles;
			
			// :before content
			List beforeInlines = null;
			genElement = context.getStyleSheet().getBeforeElement(
					this.getElement());
			
			if (genElement != null) {					
				genStyles = ss.getStyles(genElement);
				if (genStyles.getDisplay().equalsIgnoreCase(CSS.INLINE)) {
					
					beforeInlines = new ArrayList();					
					beforeInlines.addAll(LayoutUtils.createGeneratedInlines(
							context, genElement));
				} else {
					childList.add(new BlockPseudoElementBox(context,
							genElement, this, width));
				}
				
			}
			
			// :after content
			Box afterBlock = null;
			List afterInlines = null;
			genElement = context.getStyleSheet().getAfterElement(
					this.getElement());
			if (genElement != null) {
				genStyles = context.getStyleSheet().getStyles(genElement);
				if (genStyles.getDisplay().equalsIgnoreCase(CSS.INLINE)) {
					
					afterInlines = new ArrayList();
					afterInlines.addAll(LayoutUtils.createGeneratedInlines(
							context, genElement));
				} else {
					afterBlock = new BlockPseudoElementBox(context, genElement,
							this, width);
				}
				
			}	
			
			/* under construction 
			 * first-letter & first-line 
			 */
					
			
			// :first-line content
			List firstlineInlines = null;
			genElement = context.getStyleSheet().getFirstLineElement(
						this.getElement());
			if (genElement != null) {
				genStyles = ss.getStyles(genElement.getParent());
				if (genStyles.getDisplay().equalsIgnoreCase(CSS.BLOCK) && genElement.getParent().getName().equals("p")) {
				
					firstlineInlines = new ArrayList();
					firstlineInlines.addAll(LayoutUtils.createFirstInlines(
							context, genElement));			
				} 
			}
		 	
			// :first-letter content
			List firstletterInlines = null;	
			genElement = context.getStyleSheet().getFirstLetterElement(
					this.getElement());			
			 if (genElement != null) {						
				genStyles = ss.getStyles(genElement.getParent());
				if (genStyles.getDisplay().equalsIgnoreCase(CSS.BLOCK) && genElement.getParent().getName().equals("p")) {			
					
					firstletterInlines = new ArrayList();
					firstletterInlines.addAll(LayoutUtils.createFirstInlines(
							context, genElement));
				}
			}
						
			
			if(firstletterInlines==null || firstletterInlines.size()==0 || firstlineInlines==null || firstlineInlines.size()==0){
				
				int startOffset = element.getStartOffset() + 1;
				int endOffset = element.getEndOffset();
			  if(ss.getStyles(element).getDisplay().equalsIgnoreCase("none")){
				  endOffset = startOffset;
			  }
			    //startOffset = element.getStartOffset() + 1;
				//endOffset = element.getEndOffset();
				childList.addAll(createBlockBoxes(context, startOffset, endOffset,
						width, beforeInlines, afterInlines));
				if (afterBlock != null) {
					childList.add(afterBlock);
				}
			 	
			} 			 
			else if(genElement != null && genElement.getName().equals("first-letter") && genElement.getParent().getName().equals("p")){
				
				if(element.getChildElements().length<=0){
				
				int st = element.getStartOffset() + 2;
				int end = element.getEndOffset();
				childList.addAll(createBlockBoxes(context, st, end,
						width, firstletterInlines,null));
				}
				else{
					int st = element.getChildElements()[0].getStartOffset() + 2;
					int end = element.getChildElements()[0].getEndOffset();
					childList.addAll(createBlockBoxes(context, st, end,
							width, firstletterInlines,null));
				}
				
			}			
			else if(genElement != null && genElement.getName().equals("first-line") && genElement.getParent().getName().equals("p")){
				
				int dot_pos = element.getText().indexOf(".");
				if(dot_pos != -1){
				int st = element.getStartOffset() + dot_pos;
				int end = element.getEndOffset();
			
				childList.addAll(createBlockBoxes(context, st, end,
						width, firstlineInlines,null));
				}
							
			}
		
			Styles styles = context.getStyleSheet()
					.getStyles(this.getElement());
						
			if (styles.getDisplay().equalsIgnoreCase(CSS.LIST_ITEM)
					&& !styles.getListStyleType().equals(CSS.NONE)) {
				this.createListMarker(context);
			}
			
			
			/* under construction 
			 * displaying images from <graphic> element 
			 */

			Styles st = context.getStyleSheet().getStyles(this.getElement());
			if(this.getElement().getName().equals("graphic") && !st.getDisplay().equalsIgnoreCase(CSS.NONE)){
				if(this.getElement() !=null) {
					this.callDrawImage(context, this.getElement(), st);
				}	
			}
			
			
//			Styles st = context.getStyleSheet().getStyles(this.getElement());
//			if(this.getElement().getName().equals("figure") && !st.getDisplay().equalsIgnoreCase(CSS.NONE)){
//				//System.out.println(this.getElement().getChildElements()[2].getName());
//				if(this.getElement().getChildElements()[2] !=null) 
//					st = context.getStyleSheet().getStyles(this.getElement().getChildElements()[2]);
//					this.testDrawImage(context, this.getElement().getChildElements()[2], st); 
//			}

	
/**************************************/			
			
			 // FIXME add debugging flag 
			long end = System.currentTimeMillis();
			if (end - start > 10) {
				System.out.println("BEB.layout for "
						+ this.getElement().getName() + " took "
						+ (end - start) + "ms");
			}
			 
			/*
			 * This will take the list of children that was just now created and
			 * arrange them in a binary tree. This makes navigating and editing
			 * lar- ge documents (in practice over 500KB) feasible. Note that
			 * this is by far no satisfying solution. The time to perform an
			 * insertion or layout is now logarithmic in the document's length
			 * rather than linear, where it really should be constant. The
			 * bspSpeedup switch shows how to easily turn off the bintree should
			 * you want to implement a more thorough solution.
			 */

			
			if (bspSpeedup) {   
				// create a binary tree of the children for speed
				List childBinTree = new ArrayList();
				childBinTree
						.add(new BinTree(childList, context, this, element));
				return childBinTree;
			}

		}
		
				
		
		return childList;
	}
	
	
	
	/* calls DrawImage method
	 * 
	 * 	
	 */

	private void callDrawImage(LayoutContext context, IVexElement element, Styles styles){
		
		String filename = element.getAttribute(styles.getBackgroundImage());
		if(filename != null){
			 
			 InlineBox markerInline;
			 
			 markerInline = DrawImage(element, styles, filename);			 
			 
			 this.beforeMarker = ParagraphBox.create(context, this.getElement(),
				new InlineBox[] { markerInline }, Integer.MAX_VALUE);
			 
		}
		
	}
	
	/* draws image using drawable method
	 * 
	 * 	
	 */
	
	private static InlineBox DrawImage(final IVexElement element, Styles styles, final String filename){ 
		
		
		//System.out.println(styles.getBackgroundImage());
		
		
		float w = styles.getElementWidth();
		float h = styles.getElementHeight();		
		//System.out.println(w+" : "+h);
		
		final int height = (int) h; 
		final int width = (int) w;
		
		final int offset=5;
				
		Drawable drawable = new Drawable() {
			
			public Rectangle getBounds() {
				return new Rectangle(0, -height, width, height);
			}
			
			public void draw(Graphics g, int x, int y) {
				
				final Image image = new Image(Idisplay,filename);				
				
				g.drawImage(image, g.getClipBounds().getX()+offset, g.getClipBounds().getY()-height, width, height);
				
			}
		};
		

		return new DrawableBox(drawable, element);
	}
	
	

	/**
	 * Creates a marker box for this primary box and puts it in the beforeMarker
	 * field.
	 */
	private void createListMarker(LayoutContext context) {

		Styles styles = context.getStyleSheet().getStyles(this.getElement());

		InlineBox markerInline;
		
		String type = styles.getListStyleType();
		if (type.equalsIgnoreCase(CSS.NONE)) {
			return;
		} else if (type.equalsIgnoreCase(CSS.CIRCLE)) {
			markerInline = createCircleBullet(this.getElement(), styles);
		} else if (type.equalsIgnoreCase(CSS.SQUARE)) {
			markerInline = createSquareBullet(this.getElement(), styles);
		} else if (isEnumeratedListStyleType(type)) {
			String item = this.getItemNumberString(type);
			markerInline = new StaticTextBox(context, this.getElement(), item
					+ ".");
		} 
		else {
			 markerInline = createDiscBullet(this.getElement(), styles);
		}
		
		this.beforeMarker = ParagraphBox.create(context, this.getElement(),
				new InlineBox[] { markerInline }, Integer.MAX_VALUE);

		
	}

	/**
	 * Returns a Drawable that draws a circle-style list item bullet.
	 */
	private static InlineBox createCircleBullet(IVexElement element,
			Styles styles) {
		
		final int size = Math.round(0.5f * styles.getFontSize());
		final int lift = Math.round(0.1f * styles.getFontSize());
		
		final int offset =5;  //element.getStartOffset(); // (Moha)
		
		Drawable drawable = new Drawable() {
			public void draw(Graphics g, int x, int y) {
				g.setLineStyle(Graphics.LINE_SOLID);
				g.setLineWidth(1);
				g.drawOval(x+offset, y - size - lift, size, size);
			}

			public Rectangle getBounds() {
				return new Rectangle(0, -size - lift, size, size);
			}
		};
		return new DrawableBox(drawable, element);
	}

	/**
	 * Returns a Drawable that draws a disc-style list item bullet.
	 */
		
	private static InlineBox createDiscBullet(IVexElement element, Styles styles) {
		final int size = Math.round(0.5f * styles.getFontSize());
		final int lift = Math.round(0.1f * styles.getFontSize());
		
		final int offset = 5;  //element.getStartOffset(); // (Moha)
				
		Drawable drawable = new Drawable() {
			public void draw(Graphics g, int x, int y) {
				
				g.fillOval(x+offset, y - size - lift, size, size);
			}

			public Rectangle getBounds() {
				return new Rectangle(0, -size - lift, size, size);
			}
		};
		
		
		return new DrawableBox(drawable, element);
	}
	

	/**
	 * Returns a Drawable that draws a square-style list item bullet.
	 */
	private static InlineBox createSquareBullet(IVexElement element,
			Styles styles) {
		final int size = Math.round(0.5f * styles.getFontSize());
		final int lift = Math.round(0.1f * styles.getFontSize());
		
		final int offset =5;  //element.getStartOffset(); // (Moha)
		
		Drawable drawable = new Drawable() {
			public void draw(Graphics g, int x, int y) {
				g.setLineStyle(Graphics.LINE_SOLID);
				g.setLineWidth(1);
				g.drawRect(x+offset, y - size - lift, size, size);
			}

			public Rectangle getBounds() {
				return new Rectangle(0, -size - lift, size, size);
			}
		};
		return new DrawableBox(drawable, element);
	}

	/**
	 * Returns the vertical distance from the top of this box to the top of its
	 * first line.
	 */
	int getFirstLineTop(LayoutContext context) {
		Styles styles = context.getStyleSheet().getStyles(this.getElement());
		int top = styles.getBorderTopWidth() + styles.getPaddingTop().get(0);
		Box[] children = this.getChildren();
		if (children != null && children.length > 0
				&& children[0] instanceof BlockElementBox) {
			return top
					+ ((BlockElementBox) children[0]).getFirstLineTop(context);
		} else {
			return top;
		}
	}

	/**
	 * Returns the item number of this box. The item number indicates the
	 * ordinal number of the corresponding element amongst its siblings starting
	 * with 1.
	 */
	private int getItemNumber() {
		IVexElement element = this.getElement();
		IVexElement parent = element.getParent();

		if (parent == null) {
			return 1;
		}

		int item = 1;
		IVexElement[] children = parent.getChildElements();
		for (int i = 0; i < children.length; i++) {
			if (children[i] == element) {
				return item;
			}
			if (children[i].getName().equals(element.getName())) {
				item++;
			}
		}

		throw new IllegalStateException();
	}

	private String getItemNumberString(String style) {
		int item = getItemNumber();
		if (style.equalsIgnoreCase(CSS.DECIMAL_LEADING_ZERO)) {
			if (item < 10) {
				return "0" + Integer.toString(item);
			} else {
				return Integer.toString(item);
			}
		} else if (style.equalsIgnoreCase(CSS.LOWER_ALPHA)
				|| style.equalsIgnoreCase(CSS.LOWER_LATIN)) {
			return this.getAlpha(item);
		} else if (style.equalsIgnoreCase(CSS.LOWER_ROMAN)) {
			return this.getRoman(item);
		} else if (style.equalsIgnoreCase(CSS.UPPER_ALPHA)
				|| style.equalsIgnoreCase(CSS.UPPER_LATIN)) {
			return this.getAlpha(item).toUpperCase();
		} else if (style.equalsIgnoreCase(CSS.UPPER_ROMAN)) {
			return this.getRoman(item).toUpperCase();
		} else {
			return Integer.toString(item);
		}
	}

	private String getAlpha(int n) {
		final String alpha = "abcdefghijklmnopqrstuvwxyz";
		return String.valueOf(alpha.charAt((n - 1) % 26));
	}

	private String getRoman(int n) {
		final String[] ones = { "", "i", "ii", "iii", "iv", "v", "vi", "vii",
				"viii", "ix" };
		final String[] tens = { "", "x", "xx", "xxx", "xl", "l", "lx", "lxx",
				"lxxx", "xc" };
		final String[] hundreds = { "", "c", "cc", "ccc", "cd", "d", "dc",
				"dcc", "dccc", "cm" };
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < n / 1000; i++) {
			sb.append("m");
		}
		sb.append(hundreds[(n / 100) % 10]);
		sb.append(tens[(n / 10) % 10]);
		sb.append(ones[n % 10]);
		return sb.toString();
	}

	private static boolean isEnumeratedListStyleType(String s) {
		return s.equalsIgnoreCase(CSS.ARMENIAN)
				|| s.equalsIgnoreCase(CSS.CJK_IDEOGRAPHIC)
				|| s.equalsIgnoreCase(CSS.DECIMAL)
				|| s.equalsIgnoreCase(CSS.DECIMAL_LEADING_ZERO)
				|| s.equalsIgnoreCase(CSS.GEORGIAN)
				|| s.equalsIgnoreCase(CSS.HEBREW)
				|| s.equalsIgnoreCase(CSS.HIRAGANA)
				|| s.equalsIgnoreCase(CSS.HIRAGANA_IROHA)
				|| s.equalsIgnoreCase(CSS.KATAKANA)
				|| s.equalsIgnoreCase(CSS.KATAKANA_IROHA)
				|| s.equalsIgnoreCase(CSS.LOWER_ALPHA)
				|| s.equalsIgnoreCase(CSS.LOWER_GREEK)
				|| s.equalsIgnoreCase(CSS.LOWER_LATIN)
				|| s.equalsIgnoreCase(CSS.LOWER_ROMAN)
				|| s.equalsIgnoreCase(CSS.UPPER_ALPHA)
				|| s.equalsIgnoreCase(CSS.UPPER_LATIN)
				|| s.equalsIgnoreCase(CSS.UPPER_ROMAN);
	}

	@Override
	public Insets getInsets(LayoutContext context, int containerWidth) {

		Insets markerInsets = getMarkerInsets(context);

		// The margin is retrieved from AbstractBlockBox, since this class
		// handels collapsing of margins.
		Insets ins = super.getInsets(context, containerWidth);

		Styles styles = context.getStyleSheet().getStyles(getElement());

		int top = styles.getPaddingTop().get(this.getWidth())
				+ styles.getBorderTopWidth() + markerInsets.getTop();

		int left = styles.getPaddingLeft().get(this.getWidth())
				+ styles.getBorderLeftWidth()
				+ styles.getMarginLeft().get(containerWidth)
				+ markerInsets.getLeft();

		int bottom = styles.getPaddingBottom().get(this.getWidth())
				+ styles.getBorderBottomWidth() + markerInsets.getBottom();

		int right = styles.getPaddingRight().get(this.getWidth())
				+ styles.getBorderRightWidth()
				+ styles.getMarginRight().get(containerWidth)
				+ markerInsets.getRight();

		return new Insets(ins.getTop() + top, ins.getLeft() + left, ins
				.getBottom()
				+ bottom, ins.getRight() + right);

	}

}
