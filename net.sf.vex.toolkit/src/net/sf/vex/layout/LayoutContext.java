/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2004 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.layout;

import java.text.MessageFormat;
import java.util.Map;
import java.util.concurrent.CancellationException;

import net.sf.vex.core.EditorOptions;
import net.sf.vex.core.EditorOptions.BlockMarkerStyle;
import net.sf.vex.core.EditorOptions.InlineMarkerStyle;
import net.sf.vex.core.Graphics;
import net.sf.vex.css.StyleSheet;
import net.sf.vex.dom.IVexDocument;
import net.sf.vex.dom.IVexElement;

import org.eclipse.core.runtime.IProgressMonitor;

/**
 * Encapsulation of all the resources needed to create a box tree. Most
 * operations on a box tree, such as creating the tree, painting the tree, and
 * converting between spatial and model coordinates, require the context.
 */
public class LayoutContext {

	private BoxFactory boxFactory;
	private IVexDocument document;
	private Graphics graphics;
	private StyleSheet styleSheet;
	private int selectionStart;
	private int selectionEnd;
	private long startTime = System.currentTimeMillis();
	private Map<IVexElement, Boolean> collapsed;
	private EditorOptions editorOptions;
	private VexAnnotationTracker annotationTracker;
	private IProgressMonitor progressMonitor;

	/**
	 * Class constructor.
	 */
	// TODO The constructor should take all the stuff that is contained in
	// Layout context as parameters. Currently, there is a chance that some
	// function will return null.
	public LayoutContext() {
	}

	/**
	 * Returns the BoxFactory used to generate boxes for the layout.
	 */
	public BoxFactory getBoxFactory() {
		return boxFactory;
	}

	/**
	 * Returns the document being layed out.
	 */
	public IVexDocument getDocument() {
		return document;
	}

	/**
	 * Returns the <code>Graphics</code> object used for layout. Box paint
	 * methods use this graphics for painting.
	 */
	public Graphics getGraphics() {
		return this.graphics;
	}

	/**
	 * Returns the time the layout was started. Actually, it's the time since
	 * this context was created, as returned by System.currentTimeMills().
	 */
	public long getStartTime() {
		return startTime;
	}

	/**
	 * Returns the <code>StyleSheet</code> used for this layout.
	 */
	public StyleSheet getStyleSheet() {
		return this.styleSheet;
	}

	/**
	 * Helper method that returns true if the given element is in the selected
	 * range.
	 * 
	 * @param element
	 *            Element to test. May be null, in which case this method
	 *            returns false.
	 */
	public boolean isElementSelected(IVexElement element) {
		return element != null
				&& element.getStartOffset() >= this.getSelectionStart()
				&& element.getEndOffset() + 1 <= this.getSelectionEnd();
	}

	/**
	 * Resets the start time to currentTimeMillis.
	 */
	public void resetStartTime() {
		this.startTime = System.currentTimeMillis();
	}

	/**
	 * Sets the BoxFactory used to generate boxes for this layout.
	 */
	public void setBoxFactory(BoxFactory factory) {
		boxFactory = factory;
	}

	/**
	 * Sets the document being layed out.
	 */
	public void setDocument(IVexDocument document) {
		this.document = document;
	}

	/**
	 * Sets the Graphics object used for this layout.
	 */
	public void setGraphics(Graphics graphics) {
		this.graphics = graphics;
	}

	/**
	 * Sets the stylesheet used for this layout.
	 */
	public void setStyleSheet(StyleSheet sheet) {
		styleSheet = sheet;
	}

	/**
	 * Returns the offset where the current selection ends.
	 */
	public int getSelectionEnd() {
		return selectionEnd;
	}

	/**
	 * Returns the offset where the current selection starts.
	 */
	public int getSelectionStart() {
		return selectionStart;
	}

	/**
	 * Sets the offset where the current selection ends.
	 * 
	 * @param i
	 *            the new value for selectionEnd
	 */
	public void setSelectionEnd(int i) {
		selectionEnd = i;
	}

	/**
	 * Sets the offset where the current selection starts.
	 * 
	 * @param i
	 *            the new value for selectionStart
	 */
	public void setSelectionStart(int i) {
		selectionStart = i;
	}

	public boolean isCollapsed(IVexElement element) {
		boolean coll = false;
		if (collapsed.containsKey(element))
			coll = true;
		return coll;
	}

	public void collapse(IVexElement element) {
		collapsed.put(element, true);
	}

	public void expand(IVexElement element) {
		collapsed.remove(element);
	}

	/*
	 * Sets a map of collapsed children. Note that this map is used like a set,
	 * i.e. if there is a mapping with key k, that means that k is a collapsed
	 * element.
	 */
	public void setCollapsedList(Map<IVexElement, Boolean> collapsed) {
		this.collapsed = collapsed;

	}

	public void setEditorOptions(EditorOptions globalOptions) {
		this.editorOptions = globalOptions;
	}

	// ---

	public InlineMarkerStyle getInlineMarkerStyle() {
		return editorOptions.getInlineMarkerStyle();
	}

	public void setInlineMarkerStyle(InlineMarkerStyle inlineMarkerStyle) {
		editorOptions.setInlineMarkerStyle(inlineMarkerStyle);
	}

	public BlockMarkerStyle getBlockMarkerStyle() {
		return editorOptions.getBlockMarkerStyle();
	}

	public void setBlockMarkerStyle(BlockMarkerStyle blockMarkerStyle) {
		editorOptions.setBlockMarkerStyle(blockMarkerStyle);
	}

	public boolean showInlineMarkers() {
		return editorOptions.showInlineMarkers();
	}

	public void setShowInlineMarkers(boolean showInlineMarkers) {
		editorOptions.setShowInlineMarkers(showInlineMarkers);
	}

	public boolean showBlockMarkers() {
		return editorOptions.showBlockMarkers();
	}

	public void setShowBlockMarkers(boolean showBlockMarkers) {
		editorOptions.setShowBlockMarkers(showBlockMarkers);

	}

	public void setAnnotationTracker(final VexAnnotationTracker annotationTracker) {
		this.annotationTracker = annotationTracker;
	}

	VexAnnotationTracker getAnnotationTracker() {
		return annotationTracker;
	}


	public void subTask(final String message, final Object... args) throws CancellationException {
		if (progressMonitor != null) {
			String parsedMessage = MessageFormat.format(message, args);
			// System.out.println(parsedMessage);
			if (progressMonitor.isCanceled())
				throw new CancellationException(parsedMessage);
			progressMonitor.subTask(parsedMessage);

		}
	}

	public void setProgressMonitor(final IProgressMonitor monitor) {
		this.progressMonitor = monitor;
	}

}
