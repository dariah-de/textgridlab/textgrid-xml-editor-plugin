/**
 * Data structure that encapsulates a Box and an offset.
 */
package net.sf.vex.layout;

public class BoxAndOffset {

	public BoxAndOffset(Box box, int x, int y) {
		this.box = box;
		this.x = x;
		this.y = y;
	}
	
	public Box box;
	public int x;
	public int y;
}
