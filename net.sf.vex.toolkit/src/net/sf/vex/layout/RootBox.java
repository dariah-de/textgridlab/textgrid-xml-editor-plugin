/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2004 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.layout;

import net.sf.vex.core.Caret;
import net.sf.vex.core.Insets;
import net.sf.vex.core.IntRange;
import net.sf.vex.core.Rectangle;
import net.sf.vex.dom.IVexElement;
import net.sf.vex.widget.IVexWidget;

/**
 * A wrapper for the top level <code>BlockElementBox</code> that applies
 * its margins.
 */
public class RootBox extends AbstractBox implements BlockBox {

    private IVexElement element;
    private BlockElementBox childBox;
    private Box[] children = new Box[1];
	private IVexWidget vexWidget;
    
    /**
     * Class constructor.
     * @param context LayoutContext used to create children.
     * @param element Element associated with this box.
     * @param width width of this box
     * @param vexWidget 
     */
    public RootBox(LayoutContext context, IVexElement element, int width, IVexWidget vexWidget) {
        this.element = element;
        this.setWidth(width);

        this.childBox = new BlockElementBox(context, this, this.element);
        
        Insets insets = this.getInsets(context, this.getWidth());
        this.childBox.setX(insets.getLeft());
        this.childBox.setY(insets.getTop());
        this.childBox.setInitialSize(context);
        this.children[0] = this.childBox;
        
        this.vexWidget = vexWidget;
    }
    
    /**
     * @see net.sf.vex.layout.Box#getCaret(net.sf.vex.layout.LayoutContext, int)
     */
    public Caret getCaret(LayoutContext context, int offset) {
        Caret caret = this.childBox.getCaret(context, offset);
        caret.translate(this.childBox.getX(), this.childBox.getY());
        return caret;
    }
    
    public Box[] getChildren() {
        return this.children;
    }

    /**
     * @see net.sf.vex.layout.Box#getElement()
     */
    public IVexElement getElement() {
        return this.element;
    }
    
    /**
     * @see net.sf.vex.layout.Box#getEndOffset()
     */
    public int getEndOffset() {
        return this.childBox.getEndOffset();
    }

    /**
     * @see net.sf.vex.layout.BlockBox#getFirstLine()
     */
    public LineBox getFirstLine() {
        return this.childBox.getFirstLine();
    }
    
    /**
     * @see net.sf.vex.layout.BlockBox#getLastLine()
     */
    public LineBox getLastLine() {
        return this.childBox.getLastLine();
    }
    
    public int getLineEndOffset(int offset) {
        return this.childBox.getLineEndOffset(offset);
    }
    
    public int getLineStartOffset(int offset) {
        return this.childBox.getLineStartOffset(offset);
    }
    
    public int getMarginBottom() {
        return 0;
    }
    
    public int getMarginTop() {
        return 0;
    }
    
    public int getNextLineOffset(LayoutContext context, int offset, int x) {
        return childBox.getNextLineOffset(context, offset, x - childBox.getX());
    }
    
    public BlockBox getParent() {
        throw new IllegalStateException("RootBox does not have a parent");
    }

    public int getPreviousLineOffset(LayoutContext context, int offset, int x) {
        return childBox.getPreviousLineOffset(context, offset, x - childBox.getX());
    }

    /**
     * @see net.sf.vex.layout.Box#getStartOffset()
     */
    public int getStartOffset() {
        return this.childBox.getStartOffset();
    }
    
    public void invalidate(boolean direct) {
        // do nothing. layout is always propagated to our child box.
    }
    
    public IntRange layout(LayoutContext context, int top, int bottom) {
        
        Insets insets = this.getInsets(context, this.getWidth());

		long start = System.currentTimeMillis();
        IntRange repaintRange = this.childBox.layout(context, top - insets.getTop(), bottom - insets.getBottom());
        // FIXME add debugging flag
		long end = System.currentTimeMillis();
		if (end - start > 50) {
			System.out.println("RootBox.layout took " + (end - start) + "ms");
		}
        
        this.setHeight(this.childBox.getHeight() + insets.getTop() + insets.getBottom());
        
        if (repaintRange != null) {
            return new IntRange(repaintRange.getStart() + this.childBox.getY(), repaintRange.getEnd() + this.childBox.getY());
        } else {
            return null;
        }
    }

    /* (non-Javadoc)
     * @see net.sf.vex.layout.ContentBox#viewToModel(net.sf.vex.layout.LayoutContext, int, int)
     */
    public int viewToModel(LayoutContext context, int x, int y) {
        return this.childBox.viewToModel(
            context, x - this.childBox.getX(), y - this.childBox.getY());
    }

    public void paint(LayoutContext context, int x, int y, Rectangle area) {
        Rectangle r = context.getGraphics().getClipBounds();
//        long start = System.currentTimeMillis();
        super.paint(context, x, y, area);
//        long end = System.currentTimeMillis();
//        if (end - start > 50) {
//            System.out.println("RootBox.paint " + r.getHeight() + " pixel rows in " + (end - start) + "ms");
//        }
    }

    public void setInitialSize(LayoutContext context) {
        throw new IllegalStateException();
    }

	public void setParent(BlockBox parent) {
		//do nothing		
	}

	public IVexWidget getVexWidget() {
		return vexWidget;
	}

}
