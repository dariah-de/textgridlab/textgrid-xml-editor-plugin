/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2004 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.layout;

import net.sf.vex.css.CSS;
import net.sf.vex.css.Styles;
import net.sf.vex.dom.IVexElement;


/**
 * Implementation of the BoxFactory interface that returns boxes that
 * represent CSS semantics.
 */
public class CssBoxFactory implements BoxFactory {

    private static final long serialVersionUID = -6882526795866485074L;

    /**
     * Class constructor.
     */
    public CssBoxFactory() {
    }

    public Box createBox(LayoutContext context, IVexElement element, BlockBox parent, int containerWidth) {
        Styles styles = context.getStyleSheet().getStyles(element);
        if (styles.getDisplay().equalsIgnoreCase(CSS.TABLE)) {
            return new TableBox(context, parent, element);
            // Treat display:none like normal block boxes
            //TODO (until we think of something better)
		} else if (styles.isBlock() || styles.getDisplay().equalsIgnoreCase(CSS.NONE)) {
            return new BlockElementBox(context, parent, element);
		} else {
			throw new RuntimeException("Unexpected display property: " + styles.getDisplay());
        }
    }

}

