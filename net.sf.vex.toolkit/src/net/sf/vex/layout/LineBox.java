/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2004 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.layout;

import net.sf.vex.core.Insets;
import net.sf.vex.dom.IVexElement;

/**
 * Represents a line of text and inline images.
 */
public class LineBox extends CompositeInlineBox {

	private IVexElement element;

	private InlineBox[] children;

	private InlineBox firstContentChild = null;

	private InlineBox lastContentChild = null;

	private int baseline;

	/**
	 * Class constructor.
	 * 
	 * @param context
	 *            LayoutContext for this layout.
	 * @param children
	 *            InlineBoxes that make up this line.
	 */
	public LineBox(LayoutContext context, IVexElement element, InlineBox[] children) {

		this.element = element;
		this.children = children;

		int height = 0;
		int x = 0;
		this.baseline = 0;
		for (int i = 0; i < children.length; i++) {
			InlineBox child = children[i];
			Insets ins = Insets.ZERO_INSETS;
			ins = child.getInsets(context, this.getWidth());

			child.setX(x);

			// child.setY(ins.getTop()); // TODO: do proper vertical alignment

			this.baseline = Math.max(this.baseline, child.getBaseline());
			x += child.getWidth() + ins.getRight() + ins.getLeft();

			height = Math.max(height, child.getHeight() + ins.getTop()
					+ ins.getBottom());

			if (child.hasContent()) {
				if (this.firstContentChild == null) {
					this.firstContentChild = child;
				}
				this.lastContentChild = child;
			}
		}
		for (int i = 0; i < children.length; i++) {
			// TODO: Proper vertical alignment
			// Right now, it centers the lines
			Insets ins = Insets.ZERO_INSETS;
			if (children[i] instanceof InlineElementBox)
				ins = children[i].getInsets(context, this.getWidth());
			children[i]
					.setY((height - children[i].getHeight() - ins.getTop() - ins
							.getBottom()) / 2);
			// children[i].setY(0);
		}

		this.setHeight(height);

		this.setWidth(x);
	}

	/**
	 * @see net.sf.vex.layout.InlineBox#getBaseline()
	 */
	public int getBaseline() {
		return this.baseline;
	}

	public Box[] getChildren() {
		return this.children;
	}

	/**
	 * @see net.sf.vex.layout.Box#getElement()
	 */
	public IVexElement getElement() {
		return this.element;
	}

	/**
	 * @see net.sf.vex.layout.Box#getEndOffset()
	 */
	public int getEndOffset() {
		return this.lastContentChild.getEndOffset();
	}

	/**
	 * @see net.sf.vex.layout.Box#getStartOffset()
	 */
	public int getStartOffset() {
		return this.firstContentChild.getStartOffset();
	}

	/**
	 * @see net.sf.vex.layout.Box#hasContent()
	 */
	public boolean hasContent() {
		return this.firstContentChild != null;
	}

	/**
	 * @see net.sf.vex.layout.CompositeInlineBox#split(net.sf.vex.layout.LayoutContext,
	 *      net.sf.vex.layout.InlineBox[], net.sf.vex.layout.InlineBox[])
	 */
	public Pair split(LayoutContext context, InlineBox[] lefts,
			InlineBox[] rights) {

		LineBox left = null;
		LineBox right = null;

		if (lefts.length > 0) {
			left = new LineBox(context, this.getElement(), lefts);
		}

		if (rights.length > 0) {
			right = new LineBox(context, this.getElement(), rights);
		}

		return new Pair(left, right);
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		Box[] children = this.getChildren();
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < children.length; i++) {
			sb.append(children[i]);
		}
		return sb.toString();
	}

	// ========================================================== PRIVATE

}
