/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2004 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.layout;

import net.sf.vex.core.Color;
import net.sf.vex.core.ColorResource;
import net.sf.vex.core.FontMetrics;
import net.sf.vex.core.FontResource;
import net.sf.vex.core.FontSpec;
import net.sf.vex.core.Graphics;
import net.sf.vex.core.Insets;
import net.sf.vex.core.Rectangle;
import net.sf.vex.css.Styles;
import net.sf.vex.dom.IVexElement;

/**
 * Draws a marker for an inline element, labeled with the element's name.
 */
public class LabeledInlineMarkerBox extends AbstractBox implements InlineBox,
		Clickable {

	private IVexElement element;

	// Some properties concerning the look

	protected int size;

	protected static final int[] colorSpec = new int[] { 150, 150, 255 };

	String[] fontFamily = { "sans-serif" };

	protected String elementName;

	protected int width;

	protected FontResource fr;

	protected boolean isLeftMarker;

	/**
	 * Constructor.
	 * 
	 * @param isLeftMarker
	 *            Whether the marker indicates an element beginning
	 */
	public LabeledInlineMarkerBox(LayoutContext context, float markerSize,
			IVexElement element, boolean isLeftMarker) {

		this.element = element;

		this.size = (int) markerSize;

		FontResource font = context.getGraphics().createFont(
				new FontSpec(fontFamily, FontSpec.PLAIN, size * 0.8f));

		FontResource oldFont = context.getGraphics().setFont(font);

		elementName = element.getName();

		width = context.getGraphics().charsWidth(elementName.toCharArray(), 0,
				elementName.length());

		this.isLeftMarker = isLeftMarker;

		setWidth(width + size);
		setHeight(size);

		context.getGraphics().setFont(oldFont);
		font.dispose();

	}

	/**
	 * @see net.sf.vex.layout.InlineBox#getBaseline()
	 */
	public int getBaseline() {
		return 0;
	}

	/**
	 * Returns the element that controls the styling for this text element.
	 */
	public IVexElement getElement() {
		return this.element;
	}

	public boolean isEOL() {
		return false;
	}

	/**
	 * @see net.sf.vex.layout.InlineBox#split(net.sf.vex.layout.LayoutContext,
	 *      int, boolean)
	 */
	public Pair split(LayoutContext context, int maxWidth, boolean force) {
		return new Pair(null, this);
	}

	public void paint(LayoutContext context, int x, int y, Rectangle area) {

		Graphics g = context.getGraphics();

		Styles styles = context.getStyleSheet().getStyles(this.element);

		boolean drawSelected = false;

		if (isLeftMarker) {
			drawSelected = this.getElement().getStartOffset() >= context
					.getSelectionStart()
					&& this.getElement().getStartOffset() + 1 <= context
							.getSelectionEnd();
		} else {
			drawSelected = this.getElement().getEndOffset() >= context
					.getSelectionStart()
					&& this.getElement().getEndOffset() + 1 <= context
							.getSelectionEnd();
		}

		FontMetrics fm = g.getFontMetrics();

		FontResource oldFont = g.getFont();
		ColorResource oldColor = g.getColor();

		FontResource font = context.getGraphics().createFont(
				new FontSpec(fontFamily, FontSpec.PLAIN, (int) (size * 0.8f)));

		ColorResource color;
		AnnotationRenderingInfo rendering = VexAnnotationTracker.getRenderingInfo(context, getElement());
		if (rendering != null && rendering.isShownInText())
			color = g.createColor(rendering.getColor());
		else
			color = g.createColor(new Color(colorSpec[0], colorSpec[1], colorSpec[2]));

		if (drawSelected) {
			Rectangle bounds = this.getBounds();
			g.setColor(g.getSystemColor(ColorResource.SELECTION_BACKGROUND));
			g.fillRect(x + bounds.getX(), y, bounds.getWidth(), styles
					.getLineHeight()
					- fm.getDescent());
			g.setColor(g.getSystemColor(ColorResource.SELECTION_FOREGROUND));
		} else {
			g.setColor(color);

		}

		g.setFont(font);

		g.setLineStyle(Graphics.LINE_SOLID);

		g.setLineWidth(1);

		if (isLeftMarker) {

			g.drawLine(x, y, x, y + size);
			g.drawLine(x, y, x + width, y);
			g.drawLine(x, y + size, x + width, y + size);

			g.drawLine(x + width, y + size, x + width + size - 1, y + size / 2);
			g.drawLine(x + width + size - 1, y + size / 2, x + width, y);

			g.drawString(elementName, x + 2, y + (int) (size * 0.1f) - 2);
		} else {

			g
					.drawString(elementName, x - 2 + size, y
							+ (int) (size * 0.1f) - 2);

			g.drawLine(x + width + size, y, x + width + size, y + size);
			g.drawLine(x + size, y, x + width + size, y);
			g.drawLine(x + size, y + size, x + width + size, y + size);

			g.drawLine(x + size, y + size, x + 1, y + size / 2);
			g.drawLine(x + 1, y + size / 2, x + size, y);

			g.setFont(oldFont);
			g.setColor(oldColor);
			font.dispose();
			color.dispose();
		}
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return "[shape]";
	}

	public Rectangle getBounds() {
		return new Rectangle(0, size, width + size, size);
	}

	@Override
	public Insets getInsets(LayoutContext context, int containerWidth) {
		// A little space around the marker
		return new Insets(3, 3, 3, 3);
	}

	public void click(int x, int y) {
    // TODO just for testing
	//	System.out.println("I have been clicked at " +x+", "+y);
	}

}
