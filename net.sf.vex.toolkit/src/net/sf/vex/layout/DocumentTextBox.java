/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2004 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.layout;

import static java.lang.Math.max;
import static java.lang.Math.min;
import info.textgrid.lab.core.swtutils.IntervalMultimap.Interval;

import java.text.MessageFormat;
import java.util.List;

import net.sf.vex.VexToolkitPlugin;
import net.sf.vex.core.ColorResource;
import net.sf.vex.core.FontMetrics;
import net.sf.vex.core.FontResource;
import net.sf.vex.core.Graphics;
import net.sf.vex.core.Rectangle;
import net.sf.vex.css.Styles;
import net.sf.vex.dom.IVexDocument;
import net.sf.vex.dom.IVexElement;
import net.sf.vex.dom.IVexNode;
import net.sf.vex.dom.IVexText;
import net.sf.vex.dom.linked.LinkedDocument;
import net.sf.vex.dom.linked.LinkedNode;
import net.sf.vex.toolkit.preferences.PreferenceConstants;

import org.eclipse.jface.text.Position;

/**
 * A TextBox that gets its text from the document.
 */
public class DocumentTextBox extends TextBox {

	private int startRelative;
    private int endRelative;

    /**
     * Class constructor, accepting a Text object.
     * @param context LayoutContext in use
     * @param node Node being used 
     * @param text
     */
    public DocumentTextBox(LayoutContext context, IVexElement element, IVexText text) {
        this(context, element, text.getStartOffset(), text.getEndOffset());
    }
    
    /**
     * Class constructor.
     * 
     * @param context LayoutContext used to calculate the box's size.
     * @param node Node that directly contains the text.
     * @param startOffset start offset of the text
     * @param endOffset end offset of the text
     */
    public DocumentTextBox(LayoutContext context, IVexElement element, int startOffset, int endOffset) {
        super(element);

        init (context, element, startOffset, endOffset);
    }
    
    
    public DocumentTextBox(LayoutContext context, IVexNode node,
			Styles styles, int startOffset, int endOffset) {
		super(styles, node);
		init (context, node, startOffset, endOffset);
	}   
    
    private void init(LayoutContext context, IVexNode node, int startOffset, int endOffset) {
    	if (startOffset > endOffset) {
    		throw new IllegalStateException("DocumentTextBox: startOffset (" + startOffset + ") >= endOffset (" + endOffset + ")");
    	}
    	
    	this.startRelative = startOffset - node.getStartOffset();
    	this.endRelative = endOffset - node.getStartOffset();
    	
    	this.calculateSize(context);
    	
//    	if (this.getText().length() < (endOffset - startOffset)) {
//    		throw new IllegalStateException(
//    				"DocumentTextBox: Length of internal text ("+
//    				this.getText().length() + "'" + this.getText() + "'" 
//    				+ ") less than excerpt from node's text ( "
//    				+
//    				(endOffset - startOffset) + ") for node " + node);
//    	}
    	
    }
    
    /**
	 * Calculates the start offset of the node that contains this text box's
	 * content.
	 */
	private int getNodeStartOffset() {
		IVexNode node = this.getNode();
		int nodeStartOffset;
		if (node instanceof IVexElement) {
			nodeStartOffset = ((IVexElement) node).getStartOffset();
		} else if (node instanceof LinkedNode) {
			nodeStartOffset = ((LinkedNode) node).getStartOffset();
		} else {
			throw new IllegalStateException("Model not expected, implement here!");
		}
		return nodeStartOffset;
	}

	/**
     * @see net.sf.vex.layout.Box#getEndOffset()
     */
    @Override
	public int getEndOffset() {
        if (this.endRelative == -1) {
            return -1;
        } else {
			return getNodeStartOffset() + this.endRelative - 1;
        }
    }

	/**
     * @see net.sf.vex.layout.Box#getStartOffset()
     */
    @Override
	public int getStartOffset() {
        if (this.startRelative == -1) {
            return -1;
        } else {
			return getNodeStartOffset() + startRelative;
        }
    }
    
    /**
     * @see net.sf.vex.layout.TextBox#getText()
     */
    @Override
	public String getText() {
        IVexNode node = this.getNode();
        IVexDocument doc;

        
        if (node instanceof IVexElement) {
        	doc = ((IVexElement)node).getDocument();
        } else if (node instanceof LinkedNode) {
        	doc = ((LinkedNode)node).getDocument(); 
        } else {
        	throw  new IllegalStateException("Model not expected, implement here!");
        }
    	
        return doc.getText(this.getStartOffset(), this.getEndOffset() + 1);
    }

    /**
     * @see net.sf.vex.layout.Box#hasContent()
     */
    @Override
	public boolean hasContent() {
        return true;
    }


	/**
     * @see net.sf.vex.layout.Box#paint(net.sf.vex.layout.LayoutContext, int, int)
     */
    @Override
	public void paint(LayoutContext context, int x, int y, Rectangle area) {
        
    	Styles styles = getStyles(context);
        Graphics g = context.getGraphics();
        
        FontResource font = g.createFont(styles.getFont());
        FontResource oldFont = g.setFont(font);
        ColorResource foreground = g.createColor(styles.getColor());
        ColorResource oldForeground = g.setColor(foreground);
//        ColorResource background = g.createColor(styles.getBackgroundColor());
//        ColorResource oldBackground = g.setBackgroundColor(background);
        
        char[] chars = this.getText().toCharArray();
        
		final int absStart = getStartOffset();
		if (chars.length < this.getEndOffset() - this.getStartOffset()) {
            throw new IllegalStateException(MessageFormat.format("DocumentTextBox has less chars ({0}: \"{1}\") than it should ({2}-{3}={4}",
            		chars.length, getText(), getEndOffset(),
 absStart, getEndOffset() - absStart));
        }
        
        if (chars.length == 0) {

        	//paint nothing
        	return;	
        }
        
		final int start = 0;
        int end = chars.length;
        if (isNewline(chars[end - 1])) {
            end--;
        }
        int selStart = context.getSelectionStart() - this.getStartOffset();
        selStart = Math.min(Math.max(selStart, start), end);
        int selEnd = context.getSelectionEnd() - this.getStartOffset();
        selEnd = Math.min(Math.max(selEnd, start), end);
        
        if (start < selStart) {
			// from node start to selection start
            g.drawChars(chars, start, selStart - start, x, y);
            String s = new String(chars, start, selStart - start);
			paintTextDecoration(context, styles, s, absStart + start, absStart + selStart, x, y);
        }
        
        if (selEnd < end) {
			// from selection end to node end
            int x1 = x + g.charsWidth(chars, 0, selEnd);
            g.drawChars(chars, selEnd, end - selEnd, x1, y);
            String s = new String(chars, selEnd, end - selEnd);
			paintTextDecoration(context, styles, s, absStart + selEnd, absStart + end, x1, y);
        }
        
        if (selStart < selEnd) {
			// inside selection
            String s = new String(chars, selStart, selEnd - selStart);
            int x1 = x + g.charsWidth(chars, 0, selStart);
            this.paintSelectedText(context, s, x1, y);
			paintTextDecoration(context, styles, s, absStart + selStart, absStart + selEnd, x1, y);
        }

        g.setFont(oldFont);
        g.setColor(oldForeground);
//        g.setBackgroundColor(oldBackground);
        font.dispose();
        foreground.dispose();
//        background.dispose();
    }
    
    
    @Override
	protected void paintTextAnnotations(final LayoutContext context,
 final int startOffset, final int endOffset, String s,
			final int x,
			final int y, final Graphics g, final FontMetrics fm) {

		if (!VexToolkitPlugin.getDefault().getPreferenceStore().getBoolean(PreferenceConstants.TEXT_ANNOTATIONS))
			return;

		final Interval contentInterval = Interval.of(startOffset, endOffset);
		final List<AnnotationRenderingInfo> renderingInfo = VexAnnotationTracker
				.getRenderingInfo(context, contentInterval);
		if (getElement() != null && getElement().getDocument() instanceof LinkedDocument) {
			final LinkedDocument linkedDocument = (LinkedDocument) getElement()
					.getDocument();

			for (final AnnotationRenderingInfo rendering : renderingInfo) {
				if (rendering.isShownInText()) {
					final Position annoContentPosition = rendering
							.getPositionIn(linkedDocument);
					final int annoStartOffset = max(annoContentPosition.offset,
 startOffset);
					final int annoEndOffset = min(annoContentPosition.offset
 + annoContentPosition.length, endOffset);
					if (annoStartOffset > annoEndOffset)
						return; // ?
					final String preText = linkedDocument.getText(
startOffset, annoStartOffset);
					final int startX = x + g.stringWidth(preText);
					final String annotatedText = linkedDocument.getText(
							annoStartOffset, annoEndOffset);
					final int lineWidth = fm.getAscent() / 12;
					final int ypos = y + fm.getAscent() + lineWidth;

					final ColorResource oldColor = g.getColor();
					final ColorResource color = g.createColor(rendering.getColor());
					paintBaseLine(g, annotatedText, startX, ypos);
					if (VexToolkitPlugin.isDebugging(VexToolkitPlugin.DEBUG_ANNOTATIONS)) {
						System.out.println(MessageFormat.format("s = |{0}|, |{1}+{2}| ", s, preText, annotatedText));
					}
					g.setColor(oldColor);
					color.dispose();
				}
			}
		}
	}

	/**
     * @see net.sf.vex.layout.TextBox#splitAt(int)
     */
    @Override
	public Pair splitAt(LayoutContext context, int offset) {

        if (offset < 0 || offset > (this.endRelative - this.startRelative)) {
            throw new IllegalStateException();
        }
        
        int split = this.getStartOffset() + offset;
                
        DocumentTextBox left;
        if (offset == 0) {
            left = null; 
        } else {
        	left = new DocumentTextBox(context, this.getNode(), this.getStyles(context), this.getStartOffset(), split);
        }
        
        InlineBox right;
        if (split == this.getEndOffset() + 1) {
            right = null; 
        } else {
            right = new DocumentTextBox(context, this.getNode(), this.getStyles(context), split, this.getEndOffset() + 1);
        }
        return new Pair(left, right);
    }

    /**
     * @see net.sf.vex.layout.Box#viewToModel(net.sf.vex.layout.LayoutContext, int, int)
     */
    @Override
	public int viewToModel(LayoutContext context, int x, int y) {
        
        Graphics g = context.getGraphics();
        Styles styles = getStyles(context);
        FontResource font = g.createFont(styles.getFont());
        FontResource oldFont = g.setFont(font);
        char[] chars = this.getText().toCharArray();
        
        if (this.getWidth() <= 0) {
            return this.getStartOffset();
        }
        
        // first, get an estimate based on x / width
        int offset = (x / this.getWidth()) * chars.length;
        offset = Math.max(0, offset);
        offset = Math.min(chars.length, offset);
        
        int delta = Math.abs(x - g.charsWidth(chars, 0, offset));

        // Search backwards
        while (offset > 0) {
            int newDelta = Math.abs(x - g.charsWidth(chars, 0, offset-1));
            if (newDelta > delta) {
                break;
            }
            delta = newDelta;
            offset--;
        }
        
        // Search forwards
        while (offset < chars.length-1) {
            int newDelta = Math.abs(x - g.charsWidth(chars, 0, offset+1));
            if (newDelta > delta) {
                break;
            }
            delta = newDelta;
            offset++;
        }

        g.setFont(oldFont);
        font.dispose();
        return this.getStartOffset() + offset;
    }

    
}
