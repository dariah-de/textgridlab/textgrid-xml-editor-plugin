package net.sf.vex.layout;

/**
 * Boxes that can be clicked.  
 */
public interface Clickable {
	
	void click(int x, int y);
}
