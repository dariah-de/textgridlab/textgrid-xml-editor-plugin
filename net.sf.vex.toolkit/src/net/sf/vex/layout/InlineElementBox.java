/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2004 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.layout;

import java.util.ArrayList;
import java.util.List;

import net.sf.vex.core.FontMetrics;
import net.sf.vex.core.FontResource;
import net.sf.vex.core.Graphics;
import net.sf.vex.core.Insets;
import net.sf.vex.core.Rectangle;
import net.sf.vex.css.Styles;
import net.sf.vex.dom.IVexElement;
import net.sf.vex.dom.IVexNode;
import net.sf.vex.dom.IVexNonElement;
import net.sf.vex.dom.IVexText;

/**
 * An inline box that represents an inline element. This box is responsible for
 * creating and laying out its child boxes.
 */
public class InlineElementBox extends CompositeInlineBox {

	private IVexElement element;

	private InlineBox[] children;

	private InlineBox firstContentChild = null;

	private InlineBox lastContentChild = null;

	private int baseline;

	private int halfLeading;

	/**
	 * Class constructor, called by the createInlineBoxes static factory method.
	 * 
	 * @param context
	 *            LayoutContext to use.
	 * @param element
	 *            Element that generated this box
	 * @param startOffset
	 *            Start offset of the range being rendered, which may be
	 *            arbitrarily before or inside the element.
	 * @param endOffset
	 *            End offset of the range being rendered, which may be
	 *            arbitrarily after or inside the element.
	 */
	private InlineElementBox(LayoutContext context, IVexElement element,
			int startOffset, int endOffset) {

		this.element = element;

		List childList = new ArrayList();

		Styles styles = context.getStyleSheet().getStyles(element);

		if (startOffset <= element.getStartOffset()) {

			// // space for the left margin/border/padding
			// int space = styles.getMarginLeft().get(0)
			// + styles.getBorderLeftWidth()
			// + styles.getPaddingLeft().get(0);
			//
			// if (space > 0) {
			// childList.add(new SpaceBox(space, 1));
			// }

			// :before content
			IVexElement beforeElement = context.getStyleSheet()
					.getBeforeElement(element);
			if (beforeElement != null) {
				childList.addAll(LayoutUtils.createGeneratedInlines(context,
						beforeElement));
			}

			// left marker

			if (context.showInlineMarkers()) {

				switch (context.getInlineMarkerStyle()) {
				case Unlabeled:
					UnlabeledInlineMarker leftMarker = new UnlabeledInlineMarker(
							true, (int) styles.getFontSize());
					childList.add(new DrawableBox(leftMarker, element,
							DrawableBox.START_MARKER));
					break;
				case Labeled:

					childList.add(new LabeledInlineMarkerBox(context, styles
							.getFontSize(), element, true));

					break;
				}
			} else {
				// insert a blank
				// TODO collapse blanks. currently, there is a blank inserted
				// for every element. so, if 10 elements are nested, the
				// space will be very wide.
				// XXX we don't want blanks in Pro<add>ble</add>m, cf. #11228
//				childList.add(new SpaceBox((int) (styles.getFontSize() * 0.3),
//						(int) styles.getFontSize()));
			}

		}

		InlineBoxes inlines = createInlineBoxes(context, element, startOffset,
				endOffset);
		childList.addAll(inlines.boxes);
		this.firstContentChild = inlines.firstContentBox;
		this.lastContentChild = inlines.lastContentBox;

		if (endOffset > element.getEndOffset()) {

			childList.add(new PlaceholderBox(context, element, element
					.getEndOffset()
					- element.getStartOffset()));

			if (context.showInlineMarkers()) {

				// trailing marker
				switch (context.getInlineMarkerStyle()) {
				case Unlabeled:
					UnlabeledInlineMarker uRightMarker = new UnlabeledInlineMarker(
							false, (int) styles.getFontSize());
					childList.add(new DrawableBox(uRightMarker, element,
							DrawableBox.END_MARKER));
					break;

				case Labeled:
					childList.add(new LabeledInlineMarkerBox(context, styles
							.getFontSize(), element, false));

					break;

				}
			}
			// :after content
			IVexElement afterElement = context.getStyleSheet().getAfterElement(
					element);
			if (afterElement != null) {
				childList.addAll(LayoutUtils.createGeneratedInlines(context,
						afterElement));
			}

			// // space for the right margin/border/padding
			// int space = styles.getMarginRight().get(0)
			// + styles.getBorderRightWidth()
			// + styles.getPaddingRight().get(0);
			//
			// if (space > 0) {
			// childList.add(new SpaceBox(space, 1));
			// }

		}

		this.children = (InlineBox[]) childList.toArray(new InlineBox[childList
				.size()]);
		this.layout(context);
	}

	/**
	 * Class constructor. This constructor is called by the split method.
	 * 
	 * @param context
	 *            LayoutContext used for the layout.
	 * @param element
	 *            Element to which this box applies.
	 * @param children
	 *            Child boxes.
	 */
	private InlineElementBox(LayoutContext context, IVexElement element,
			InlineBox[] children) {

		this.element = element;
		this.children = children;
		this.layout(context);

		for (int i = 0; i < children.length; i++) {
			InlineBox child = children[i];
			if (child.hasContent()) {
				if (this.firstContentChild == null) {
					this.firstContentChild = child;
				}
				this.lastContentChild = child;
			}
		}

	}

	/**
	 * @see net.sf.vex.layout.InlineBox#getBaseline()
	 */
	public int getBaseline() {
		return this.baseline;
	}

	/**
	 * @see net.sf.vex.layout.Box#getChildren()
	 */
	@Override
	public Box[] getChildren() {
		return this.children;
	}

	/**
	 * Returns the element associated with this box.
	 */
	@Override
	public IVexElement getElement() {
		return this.element;
	}

	/**
	 * @see net.sf.vex.layout.Box#getEndOffset()
	 */
	@Override
	public int getEndOffset() {
		if (this.lastContentChild == null) {
			return this.getElement().getEndOffset();
		} else {
			return this.lastContentChild.getEndOffset();
		}
	}

	/**
	 * @see net.sf.vex.layout.Box#getStartOffset()
	 */
	@Override
	public int getStartOffset() {
		if (this.firstContentChild == null) {
			return this.getElement().getStartOffset();
		} else {
			return this.firstContentChild.getStartOffset();
		}
	}

	/**
	 * Override to paint background and borders.
	 * 
	 * @see net.sf.vex.layout.AbstractBox#paint(net.sf.vex.layout.LayoutContext,
	 *      int, int)
	 */
	@Override
	public void paint(LayoutContext context, int x, int y, Rectangle area) {
		Insets ins = this.getInsets(context, this.getWidth());

		boolean drawSelected = context.isElementSelected(this.getElement());

		this.drawBox(context, x + ins.getLeft(), y + ins.getTop(), 0,
				drawSelected);

		// TODO
		// CSS
		// violation
		// TODO Also, adding the insets at this point is only a quick fix! It
		// should really be done in AbstractBox.drawBox()

		super.paint(context, x, y, area);
	}

	@Override
	public Pair split(LayoutContext context, InlineBox[] lefts,
			InlineBox[] rights) {

		context.subTask("Splitting in {0} ...", element.getName());

		InlineElementBox left = null;
		InlineElementBox right = null;

		if (lefts.length > 0 || rights.length == 0) {
			left = new InlineElementBox(context, this.getElement(), lefts);

		}

		if (rights.length > 0) {
			right = new InlineElementBox(context, this.getElement(), rights);
		}

		return new Pair(left, right);
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		if (this.getStartOffset() == this.getElement().getStartOffset() + 1) {
			sb.append("<");
			sb.append(this.getElement().getName());
			sb.append(">");
		}
		Box[] children = this.getChildren();
		for (int i = 0; i < children.length; i++) {
			sb.append(children[i]);
		}
		if (this.getEndOffset() == this.getElement().getEndOffset()) {
			sb.append("</");
			sb.append(this.getElement().getName());
			sb.append(">");
		}
		return sb.toString();
	}

	/**
	 * Holds the results of the createInlineBoxes method.
	 */
	static class InlineBoxes {

		/** List of generated boxes */
		public List boxes = new ArrayList();

		/** First generated box that has content */
		public InlineBox firstContentBox;

		/** Last generated box that has content */
		public InlineBox lastContentBox;
	}

	/**
	 * Creates a list of inline boxes given a range of offsets. This method is
	 * used when creating both ParagraphBoxes and InlineElementBoxes.
	 * 
	 * @param context
	 *            LayoutContext to be used.
	 * @param containingElement
	 *            Element containing both offsets
	 * @param startOffset
	 *            The start of the range to convert to inline boxes.
	 * @param endOffset
	 *            The end of the range to convert to inline boxes.
	 * @return
	 */
	static InlineBoxes createInlineBoxes(LayoutContext context,
			IVexElement containingElement, int startOffset, int endOffset) {

		InlineBoxes result = new InlineBoxes();

		IVexNode[] nodes = containingElement.getChildNodes();
		for (int i = 0; i < nodes.length; i++) {

			IVexNode node = nodes[i];
			InlineBox child;

			if (node.getStartOffset() >= endOffset) {
				break;
			} else if (node instanceof IVexText) {

				// This check is different for Text and Element, so we have to
				// do it here and below, too.
				if (node.getEndOffset() <= startOffset) {
					continue;
				}

				int start = Math.max(startOffset, node.getStartOffset());
				int end = Math.min(endOffset, node.getEndOffset());
				if (start == end)
					continue;

				child = new DocumentTextBox(context, containingElement, start,
						end);

			} else if (node instanceof IVexNonElement) {

				// if (node.getEndOffset() < startOffset) {
				// continue;
				// }
				//
				// int start = Math.max(startOffset, node.getStartOffset());
				// int end = Math.min(endOffset, node.getEndOffset());
				// if (start == end)
				// continue;
				//
				// child = new CommentBox(context, (IVexComment) node, start,
				// end);
				/*
				 * child = new DocumentTextBox(context, containingElement,
				 * start, end);
				 */

				if (node.getEndOffset() < startOffset) {
					continue;
				}

				IVexNonElement childElement = (IVexNonElement) node;
				InlineBox placeholder = new PlaceholderBox(context,
						containingElement, childElement.getStartOffset()
								- containingElement.getStartOffset());
				result.boxes.add(placeholder);
				if (result.firstContentBox == null) {
					result.firstContentBox = placeholder;
				}

				int start = Math.max(startOffset, node.getStartOffset());
				int end = Math.min(endOffset, node.getEndOffset());

				child = NonElementBox.createNonElementBox(context,
						childElement, start, end);

			} else {

				// Element
				if (node.getEndOffset() < startOffset) {
					continue;
				}

				IVexElement childElement = (IVexElement) node;
				InlineBox placeholder = new PlaceholderBox(context,
						containingElement, childElement.getStartOffset()
								- containingElement.getStartOffset());
				result.boxes.add(placeholder);
				if (result.firstContentBox == null) {
					result.firstContentBox = placeholder;
				}
				child = new InlineElementBox(context, childElement,
						startOffset, endOffset);
			}

			if (result.firstContentBox == null) {
				result.firstContentBox = child;
			}

			result.lastContentBox = child;

			result.boxes.add(child);
		}

		return result;
	}

	
	
	private void layout(LayoutContext context) {
		Graphics g = context.getGraphics();
		Styles styles = context.getStyleSheet().getStyles(element);
		FontResource font = g.createFont(styles.getFont());
		FontResource oldFont = g.setFont(font);
		FontMetrics fm = g.getFontMetrics();
		this.setHeight(styles.getLineHeight());
		this.halfLeading = (styles.getLineHeight() - fm.getAscent() - fm
				.getDescent()) / 2;

		Insets ins = this.getInsets(context, this.getWidth());

		int x = 0;
		for (int i = 0; i < this.children.length; i++) {

			InlineBox child = this.children[i];

			Insets childIns = child.getInsets(context, this.getWidth());

			child.setX(x + ins.getLeft());

			// controls leading and trailing spaces (Moha)
			x += child.getWidth() + childIns.getLeft(); // + childIns.getRight();

			this.setHeight(Math.max(this.getHeight(), child.getHeight()
					+ childIns.getTop() + childIns.getBottom()));

		}

		this.setWidth(x);

		for (InlineBox b : this.children) {
			// TODO: Proper vertical alignment
			// Right now, it centers the lines
			Insets childIns = Insets.ZERO_INSETS;
			if (b instanceof InlineElementBox)
				childIns = b.getInsets(context, this.getWidth());

			b
					.setY(ins.getTop()
							+ (this.getHeight() - b.getHeight()
									- childIns.getTop() - childIns.getBottom())
							/ 2);

		}

		g.setFont(oldFont);
		font.dispose();

	}

	@Override
	public Insets getInsets(LayoutContext context, int containerWidth) {

		// This test should not be necessary?
		IVexElement element = this.getElement();
		if (element == null) {
			return Insets.ZERO_INSETS;
		} else {
			return getInsets(context.getStyleSheet().getStyles(element),
					containerWidth);
		}

	}

}
