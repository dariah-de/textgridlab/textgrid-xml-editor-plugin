/*
 * VEX, a visual editor for XML
 * 
 * Copyright (c) 2004 John Krasnay
 * 
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package net.sf.vex.layout;

import java.text.MessageFormat;

import net.sf.vex.VexToolkitPlugin;
import net.sf.vex.core.Caret;
import net.sf.vex.core.ColorResource;
import net.sf.vex.core.FontMetrics;
import net.sf.vex.core.FontResource;
import net.sf.vex.core.FontSpec;
import net.sf.vex.core.Graphics;
import net.sf.vex.css.Styles;
import net.sf.vex.dom.IVexElement;
import net.sf.vex.dom.IVexNode;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.ui.statushandlers.StatusManager;

/**
 * An inline box containing text. The <code>getText</code> and
 * <code>splitAt</code> methods are abstract and must be implemented by
 * subclasses.
 */
public abstract class TextBox extends AbstractBox implements InlineBox {

	private IVexNode node;

	private Styles styles = null;

	private int baseline;

	public static final char NEWLINE_CHAR = 0xa;
	public static final String NEWLINE_STRING = "\n";

	public static boolean isNewline(final char ch) {
		return (ch == NEWLINE_CHAR || ch == '\n');
	}

	/**
	 * Class constructor.
	 * 
	 * @param pseudoElement
	 *            Element containing the text. This is used for styling
	 *            information.
	 */
	public TextBox(IVexElement pseudoElement) {
		this.node = pseudoElement;
	}

	/**
	 * Class constructor. No specific element is referenced.
	 * 
	 * @param styles
	 *            Styles controlling the appearance of the text.
	 */
	public TextBox(Styles styles, IVexNode node) {

		this.styles = styles;
		this.node = node;

	}

	protected Styles getStyles(LayoutContext context) {

		if (this.styles != null)
			return styles;

		else if (node instanceof IVexElement)
			return context.getStyleSheet().getStyles(this.getElement());

		else
			throw new IllegalStateException("I have got no style and no element!");

	}

	/**
	 * Causes the box to recalculate it size. Subclasses should call this from
	 * their constructors after they are initialized.
	 * 
	 * @param context
	 *            LayoutContext used to calculate size.
	 */
	protected void calculateSize(LayoutContext context) {
		String s = this.getText();
		if (s.endsWith(NEWLINE_STRING)) {
			s = s.substring(0, s.length() - 1);
		}

		Graphics g = context.getGraphics();

		Styles styles = getStyles(context);
		FontResource font = g.createFont(styles.getFont());
		FontResource oldFont = g.setFont(font);
		FontMetrics fm = g.getFontMetrics();
		this.setWidth(g.stringWidth(s));
		this.setHeight(styles.getLineHeight());
		int halfLeading = (this.getHeight() - (fm.getAscent() + fm.getDescent())) / 2;
		this.baseline = halfLeading + fm.getAscent();
		g.setFont(oldFont);
		font.dispose();
	}

	/**
	 * @see net.sf.vex.layout.InlineBox#getBaseline()
	 */
	public int getBaseline() {
		return this.baseline;
	}

	/**
	 * @see net.sf.vex.layout.Box#getCaret(net.sf.vex.layout.LayoutContext, int)
	 */
	@Override
	public Caret getCaret(LayoutContext context, int offset) {
		Graphics g = context.getGraphics();

		Styles styles = getStyles(context);
		FontResource oldFont = g.getFont();
		FontResource font = g.createFont(styles.getFont());
		g.setFont(font);
		char[] chars = this.getText().toCharArray();
		int x = g.charsWidth(chars, 0, offset - this.getStartOffset());
		g.setFont(oldFont);
		font.dispose();
		return new TextCaret(x, 0, this.getHeight());
	}

	/**
	 * Returns the element that controls the styling for this text element.
	 */
	@Override
	public IVexElement getElement() {
		if (node instanceof IVexElement)
			return (IVexElement) this.node;
		else
			return null;
	}

	public IVexNode getNode() {

		return node;
	}

	/**
	 * Return the text that comprises this text box. The actual text can come
	 * from the document content or from a static string.
	 */
	public abstract String getText();

	/**
	 * Returns true if the given character is one where a linebreak should
	 * occur, e.g. a space.
	 * 
	 * @param c
	 *            the character to test
	 */
	public static boolean isSplitChar(char c) {
		return Character.isWhitespace(c);
	}

	public boolean isEOL() {
		String s = this.getText();
		return s.length() > 0 && isNewline(s.charAt(s.length() - 1));
	}

	/**
	 * Paints a string as selected text.
	 * 
	 * @param context
	 *            LayoutContext to be used. It is assumed that the contained
	 *            Graphics object is set up with the proper font.
	 * @param s
	 *            String to draw
	 * @param x
	 *            x-coordinate at which to draw the text
	 * @param y
	 *            y-coordinate at which to draw the text
	 */
	protected void paintSelectedText(LayoutContext context, String s, int x, int y) {
		Graphics g = context.getGraphics();

		// Uncommented: differentiation between the whole block selected or
		// less.

		//
		// boolean inSelectedBlock = false;
		// Element e = this.getElement();
		// while (e != null) {
		// Styles styles = getStyles(context);
		// if (styles.isBlock()) {
		// if (context.isElementSelected(e)) {
		// inSelectedBlock = true;
		// }
		// break;
		// }
		// e = e.getParent();
		// }
		//
		// if (inSelectedBlock) {
		// g.setColor(g.getSystemColor(ColorResource.SELECTION_BACKGROUND));
		// g.drawString(s, x, y);
		// } else {
		int width = g.stringWidth(s);
		g.setColor(g.getSystemColor(ColorResource.SELECTION_BACKGROUND));
		g.fillRect(x, y, width, this.getHeight());
		g.setColor(g.getSystemColor(ColorResource.SELECTION_FOREGROUND));
		g.drawString(s, x, y);
		// }
	}

	/**
	 * @param context
	 *            The layout context to use
	 * @param styles
	 *            decoration styles
	 * @param s
	 *            the string to paint
	 * @param startOffset
	 *            start offset in the content area, or -1 if not applicable
	 * @param endOffset
	 *            end offset in the content area, or -1 if not applicable
	 * @param x
	 *            x coordinate on the context graphics
	 * @param y
	 *            y coordinate on the context graphics
	 */
	protected void paintTextDecoration(LayoutContext context, Styles styles, String s, int startOffset, int endOffset, int x, int y) {
		int fontStyle = styles.getFont().getStyle();
		Graphics g = context.getGraphics();
		FontMetrics fm = g.getFontMetrics();

		if ((fontStyle & FontSpec.UNDERLINE) != 0) {
			int lineWidth = fm.getAscent() / 12;
			int ypos = y + fm.getAscent() + lineWidth;
			paintBaseLine(g, s, x, ypos + 5);
		}
		if ((fontStyle & FontSpec.OVERLINE) != 0) {
			int lineWidth = fm.getAscent() / 12;
			int ypos = y + lineWidth / 2;
			paintBaseLine(g, s, x, ypos);
		}
		if ((fontStyle & FontSpec.LINE_THROUGH) != 0) {
			int ypos = y + fm.getHeight() / 2;
			paintBaseLine(g, s, x, ypos);
		}

		paintTextAnnotations(context, startOffset, endOffset, s, x, y, g, fm);
	}

	/**
	 * @param context
	 *            the layout context to use
	 * @param startOffset
	 *            character offset in the content area of the text to annotate
	 * @param endOffset
	 *            character offset in the content area of the end of the text to
	 *            annotate
	 * @param s
	 *            the text string to annotate
	 * @param x
	 *            x coordinate: where to start painting
	 * @param y
	 *            y coordinate: where to start painting
	 * @param g
	 *            the graphics object to paint on
	 * @param fm
	 *            the font metrics that were used for painting the text to
	 *            annotate
	 */
	protected void paintTextAnnotations(LayoutContext context, int startOffset, int endOffset, String s, int x, int y, Graphics g,
			FontMetrics fm) {
		AnnotationRenderingInfo rendering = VexAnnotationTracker.getRenderingInfo(context, getNode());
		if (rendering != null && rendering.isShownInText()) {
			ColorResource oldColor = g.getColor();
			ColorResource annotationColor = g.createColor(rendering.getColor());
			try {
				g.setColor(annotationColor);
				int lineWidth = fm.getAscent() / 12;
				int ypos = y + fm.getAscent() + lineWidth;
				paintBaseLine(g, s, x, ypos);
			} finally {
				g.setColor(oldColor);
				annotationColor.dispose();
			}
		}
	}

	/**
	 * Paint a line along the baseline of the text, for showing underline,
	 * overline and strike-through formatting.
	 * 
	 * @param context
	 *            LayoutContext to be used. It is assumed that the contained
	 *            Graphics object is set up with the proper font.
	 * @param x
	 *            x-coordinate at which to start drawing baseline
	 * @param y
	 *            x-coordinate at which to start drawing baseline (adjusted to
	 *            produce the desired under/over/though effect)
	 */
	protected void paintBaseLine(Graphics g, String s, int x, int y) {
		FontMetrics fm = g.getFontMetrics();
		int width = g.stringWidth(s);
		int lineWidth = fm.getAscent() / 12;
		g.setLineStyle(Graphics.LINE_SOLID);
		g.setLineWidth(lineWidth);
		g.drawLine(x, y, x + width, y);
	}

	/**
	 * @see net.sf.vex.layout.InlineBox#split(net.sf.vex.layout.LayoutContext,
	 *      int, boolean)
	 */
	public Pair split(LayoutContext context, int maxWidth, boolean force) {

		char[] chars = this.getText().toCharArray();

		if (chars.length == 0) {
			StatusManager.getManager().handle(
					new Status(
							IStatus.WARNING,
							VexToolkitPlugin.PLUGIN_ID,
							MessageFormat.format(
									"Someone tried to split a text box that does not contain text. Node: {0}, box start: {1}, end: {2}, text: {3}",
									getNode(), getStartOffset(), getEndOffset(), getText()),
							new IllegalStateException(
									"I don't want to be split! I'm only 0 characters long!!!\n   (Love, net.sf.vex.layout.TextBox)")));
			return new Pair(this, null);
		}

		Graphics g = context.getGraphics();
		Styles styles = getStyles(context);
		FontResource font = g.createFont(styles.getFont());
		FontResource oldFont = g.setFont(font);

		int split = 0;
		int next = 1;
		boolean eol = false; // end of line found
		while (next < chars.length) {
			if (isSplitChar(chars[next - 1])) {
				if (g.charsWidth(chars, 0, next) <= maxWidth) {
					split = next;
					if (isNewline(chars[next - 1])) {
						eol = true;
						break;
					}
				} else {
					break;
				}
			}
			next++;
		}

		if (force && split == 0) {
			// find some kind of split
			split = 1;
			while (split < chars.length) {
				if (g.charsWidth(chars, 0, split + 1) > maxWidth) {
					break;
				}
				split++;
			}

		}

		// TG-127 special treatment, since the loop above skips over sth like
		// content("\A");
		// TG-770: Here we have contents like '\n0', i.e. we newline +
		// additional content (e.g. before + after)
		for (int i = 0; i < chars.length; i++) {
			if (isNewline(chars[i])) {
				split = i + 1;
				eol = true;
			}
		}

		// include any trailing spaces in the split
		// this also grabs any leading spaces when split==0
		if (!eol) {
			while (split < chars.length - 1 && chars[split] == ' ') {
				split++;
			}
		}

		g.setFont(oldFont);
		font.dispose();

		return this.splitAt(context, split);
	}

	/**
	 * Return a pair of boxes representing a split at the given offset. If split
	 * is zero, then the returned left box should be null. If the split is equal
	 * to the length of the text, then the right box should be null.
	 * 
	 * @param context
	 *            LayoutContext used to calculate the sizes of the resulting
	 *            boxes.
	 * @param offset
	 *            location of the split, relative to the start of the text box.
	 * @return
	 */
	public abstract Pair splitAt(LayoutContext context, int offset);

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return this.getText();
	}

}
