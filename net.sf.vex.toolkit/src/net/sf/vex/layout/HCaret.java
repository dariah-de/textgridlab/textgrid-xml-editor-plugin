/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2004 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.layout;

import net.sf.vex.core.Caret;
import net.sf.vex.core.Color;
import net.sf.vex.core.ColorResource;
import net.sf.vex.core.Graphics;
import net.sf.vex.core.Rectangle;

/**
 * A horizontal caret representing the insertion point between two block boxes.
 */
public class HCaret extends Caret {

    private static final int LINE_WIDTH = 2;
    
    /**
     * Class constructor.
     * @param x x-coordinate of the top left corner of the caret
     * @param y y-coordinate of the top left corner of the caret
     * @param length Horizontal length of the caret.
     */
    public HCaret(int x, int y, int length) {
        super(x, y);
        this.length = length;
    }
    
    public void draw(Graphics g, Color color) {
        ColorResource newColor = g.createColor(color);
        ColorResource oldColor = g.setColor(newColor);
        g.fillRect(this.getX(), this.getY(), this.length, LINE_WIDTH);
        g.setColor(oldColor);
        newColor.dispose();
    }    

    /**
     * @see net.sf.vex.core.Caret#getBounds()
     */
    public Rectangle getBounds() {
        return new Rectangle(this.getX(), this.getY(), this.length, LINE_WIDTH);
    }
    
    
    //====================================================== PRIVATE
    
    private int length;
}
