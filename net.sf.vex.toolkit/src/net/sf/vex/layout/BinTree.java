/*
 * Created on 16.08.2007
 *
 */
package net.sf.vex.layout;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import net.sf.vex.core.Insets;
import net.sf.vex.dom.IVexElement;

/**
 * A binary tree to store boxes for faster layout. This is just an anonymous box
 * that will be created with no more than two children.
 * 
 * @author moz
 * 
 */

public class BinTree extends AbstractBlockBox {

	private boolean content = false;

	@Override
	protected List createChildren(LayoutContext context) {

		// TODO improve this if it works ok
		Box[] newChildren = this.children.clone();
		return Arrays.asList(newChildren);
	}

	/**
	 * 
	 * Construct a binary tree from a list of boxes. Note: This is not a very
	 * clean solution. It takes a list of boxes and reorders it, i.e. sets
	 * parent relations etc. There is no other (easy) way though.
	 * 
	 * Note that the children field only has two children maximum.
	 * 
	 * 
	 * @param boxes
	 *            A (geometrically) ordered list of boxes.
	 * @param context
	 * @param parent
	 * @param element
	 */
	public BinTree(List<BlockBox> boxes, LayoutContext context,
			BlockBox parent, IVexElement element) {
		super(context, parent, null);

		if (boxes.size() <= 0) {
			this.children = new Box[0];
			return;
		}

		this.startPosition = context.getDocument().createPosition(
				context.getDocument().getLength());
		this.endPosition = context.getDocument().createPosition(0);

		// 1 leaf
		if (boxes.size() == 1) {
			BlockBox topChild = boxes.get(0);
			topChild.setParent(this);
			this.children = new Box[] { topChild };

			// propagate the start- and end offset to all its parents
			// this is more robust than other solutions
			if (topChild.hasContent())
				backPropagateAttributes(context, topChild.getStartOffset(),
						topChild.getEndOffset(), topChild.hasContent());

			return;

		}

		int splitIndex = boxes.size() / 2;

		BlockBox topChild = new BinTree(boxes.subList(0, splitIndex), context,
				this, element);

		BlockBox bottomChild = new BinTree(boxes.subList(splitIndex, boxes
				.size()), context, this, element);

		this.children = new Box[] { topChild, bottomChild };

	}

	/**
	 * When a leaf in the binary tree (i.e. a non-BinTree BlockBox) is reached,
	 * its start and end offset will be propagated backwards through the tree,
	 * and all nodes above it updated accordingly.
	 * 
	 * @param context
	 * @param startOffset
	 * @param endOffset
	 */
	void backPropagateAttributes(LayoutContext context, int startOffset,
			int endOffset, boolean hasContent) {

		this.content = this.content || hasContent;

		if (this.startPosition.getOffset() > startOffset) {
			this.startPosition = context.getDocument().createPosition(
					startOffset);
			if (this.getParent() instanceof BinTree)
				((BinTree) this.getParent()).backPropagateAttributes(context,
						startOffset, endOffset, this.content);
		}
		if (this.endPosition.getOffset() < endOffset) {
			this.endPosition = context.getDocument().createPosition(endOffset);
			if (this.getParent() instanceof BinTree)
				((BinTree) this.getParent()).backPropagateAttributes(context,
						startOffset, endOffset, this.content);

		}

	}

	@Override
	public int getStartOffset() {
		return startPosition.getOffset();
	}

	@Override
	public int getEndOffset() {
		return endPosition.getOffset();
	}

	@Override
	public boolean isAnonymous() {
		return true;
	}

	@Override
	public String toString() {
		String s = "BinTree " + "(" + getStartOffset() + ", " + getEndOffset()
				+ ", " + content + ")" + "[";
		for (Box b : children) {
			s = s + b.toString() + " ,";
		}
		s = s + "]";
		return s;
	}

	@Override
	public Insets getInsets(LayoutContext context, int containerWidth) {
		return Insets.ZERO_INSETS;
	}

	public List<BlockBox> toList() {
		List<BlockBox> list = new ArrayList<BlockBox>();
		for (Box c : children) {
			if (c instanceof BinTree)
				list.addAll(((BinTree) c).toList());
			else if (c instanceof BlockBox)
				list.add((BlockBox) c);
			else
				throw new IllegalStateException(
						"A BinTree should only have children of type BlockBox!");

		}
		return list;

	}

	@Override
	public Box getFirstChild() {
		if (children.length == 0)
			return null;
		if (children[0] instanceof BinTree) {
			return ((BinTree) children[0]).getFirstChild();
		} else {
			return children[0];
		}
	}

	@Override
	public boolean hasContent() {
		return content;
	}

}
