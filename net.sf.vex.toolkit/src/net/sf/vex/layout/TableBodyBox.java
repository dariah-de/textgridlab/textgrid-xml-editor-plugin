/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2005 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.layout;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import net.sf.vex.core.Insets;
import net.sf.vex.core.Rectangle;
import net.sf.vex.css.CSS;
import net.sf.vex.css.Styles;
import net.sf.vex.dom.IVexElement;

/**
 * An anonymous box that contains the table row groups for a table. This box
 * is generated by a TableBox and assumes the margins and borders of the 
 * table element.
 */
public class TableBodyBox extends AbstractBlockBox {

    public TableBodyBox(LayoutContext context, TableBox parent, int startOffset, int endOffset) {
        super(context, parent, startOffset, endOffset);
    }

    protected List createChildren(final LayoutContext context) {
        // TODO Auto-generated method stub
        
        // Walk children:
        //     each table-*-group gets a non-anonymous TableRowGroupBox
        //     runs of others get anonymous TableRowGroupBox
        
        final List children = new ArrayList();
        
        this.iterateChildrenByDisplayStyle(context.getStyleSheet(), childDisplayStyles, new ElementOrRangeCallback() {
            public void onElement(IVexElement child, String displayStyle) {
                children.add(new TableRowGroupBox(context, TableBodyBox.this, child));
            }
            public void onRange(IVexElement parent, int startOffset, int endOffset) {
                children.add(new TableRowGroupBox(context, TableBodyBox.this, startOffset, endOffset));
            }
        });

        return children;
    }

    /**
     * Return the insets of the parent box.
     */
    public Insets getInsets(LayoutContext context, int containerWidth) {
        if (this.getParent().getElement() != null) {
            Styles styles = context.getStyleSheet().getStyles(this.getParent().getElement());
            return AbstractBox.getInsets(styles, containerWidth);
        } else {
            return Insets.ZERO_INSETS;
        }
    }

    public void paint(LayoutContext context, int x, int y, Rectangle area) {
        this.drawBox(context, this.getParent().getElement(), x, y, this.getParent().getWidth(), true);
        this.paintChildren(context, x, y, area);
    }
    
    
    //======================================================== PRIVATE

    private static Set childDisplayStyles = new HashSet();
    
    static {
        childDisplayStyles.add(CSS.TABLE_ROW_GROUP);
        childDisplayStyles.add(CSS.TABLE_HEADER_GROUP);
        childDisplayStyles.add(CSS.TABLE_FOOTER_GROUP);
    }

}
