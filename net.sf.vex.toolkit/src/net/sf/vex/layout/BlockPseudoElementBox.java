/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2004 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.layout;

import java.util.List;

import net.sf.vex.core.IntRange;
import net.sf.vex.core.Rectangle;
import net.sf.vex.css.Styles;
import net.sf.vex.dom.IVexElement;

/**
 * Implements a Block
 */
public class BlockPseudoElementBox extends AbstractBox implements BlockBox {

    private IVexElement pseudoElement;
    private BlockBox parent;
    private ParagraphBox para;
    
    private int marginTop;
    private int marginBottom;
    
    public BlockPseudoElementBox(LayoutContext context, IVexElement pseudoElement, BlockBox parent, int width) {
        
        this.pseudoElement = pseudoElement;
        this.parent = parent;
        
        Styles styles = context.getStyleSheet().getStyles(pseudoElement);
        
        this.marginTop = styles.getMarginTop().get(width);
        this.marginBottom = styles.getMarginBottom().get(width);

        int leftInset = styles.getMarginLeft().get(width)
            + styles.getBorderLeftWidth()
            + styles.getPaddingLeft().get(width);
        int rightInset = styles.getMarginRight().get(width)
            + styles.getBorderRightWidth()
            + styles.getPaddingRight().get(width);

        int childWidth = width - leftInset - rightInset;
        List inlines = LayoutUtils.createGeneratedInlines(context, pseudoElement);
        this.para = ParagraphBox.create(context, pseudoElement, inlines, childWidth);

        this.para.setX(0);
        this.para.setY(0);
        this.setWidth(width - leftInset - rightInset);
        this.setHeight(this.para.getHeight());
    }


    /**
     * Provide children for {@link AbstractBox#paint}.
     * @see net.sf.vex.layout.Box#getChildren()
     */
    public Box[] getChildren() {
        return new Box[] { this.para };    
    }

    /**
     * @see net.sf.vex.layout.Box#getElement()
     */
    public IVexElement getElement() {
        return this.pseudoElement;
    }

    /**
     * @see net.sf.vex.layout.BlockBox#getFirstLine()
     */
    public LineBox getFirstLine() {
        throw new IllegalStateException();
    }

    /**
     * @see net.sf.vex.layout.BlockBox#getLastLine()
     */
    public LineBox getLastLine() {
        throw new IllegalStateException();
    }

    /**
     * @see net.sf.vex.layout.BlockBox#getLineEndOffset(int)
     */
    public int getLineEndOffset(int offset) {
        throw new IllegalStateException();
    }

    /**
     * @see net.sf.vex.layout.BlockBox#getLineStartOffset(int)
     */
    public int getLineStartOffset(int offset) {
        throw new IllegalStateException();
    }

    public int getMarginBottom() {
        return this.marginBottom;
    }
    
    public int getMarginTop() {
        return this.marginTop;
    }
    
    /**
     * @see net.sf.vex.layout.BlockBox#getNextLineOffset(net.sf.vex.layout.LayoutContext, int, int)
     */
    public int getNextLineOffset(LayoutContext context, int offset, int x) {
        throw new IllegalStateException();
    }

    /**
     * Returns this box's parent.
     */
    public BlockBox getParent() {
        return this.parent;
    }
    
    /**
     * @see net.sf.vex.layout.BlockBox#getPreviousLineOffset(net.sf.vex.layout.LayoutContext, int, int)
     */
    public int getPreviousLineOffset(
        LayoutContext context,
        int offset,
        int x) {
            throw new IllegalStateException();
    }

    public IntRange layout(LayoutContext context, int top, int bottom) {
        return null;
    }
    
    public void invalidate(boolean direct) {
        throw new IllegalStateException("invalidate called on a non-element BlockBox");
    }


    /**
     * Draw boxes before painting our child.
     * @see net.sf.vex.layout.Box#paint(net.sf.vex.layout.LayoutContext, int, int)
     */
    public void paint(LayoutContext context, int x, int y, Rectangle area) {
        this.drawBox(context, x, y, this.getParent().getWidth(), true);
        super.paint(context, x, y, area);
    }

    public void setInitialSize(LayoutContext context) {
        // NOP - size calculated in the ctor
    }

    public void setParent(BlockBox parent) {
		this.parent = parent;
	}

}
