/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2005 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.layout;

import java.util.List;

import net.sf.vex.core.Rectangle;
import net.sf.vex.css.Styles;
import net.sf.vex.dom.IVexElement;

/**
 * Represents an element with display:table-cell, or a generated, anonymous
 * table cell.
 */
public class TableCellBox extends AbstractBlockBox {

    /**
     * Class constructor for non-anonymous table cells.
     * 
     * @param context LayoutContext to use.
     * @param parent Parent box.
     * @param element Element with which this box is associated.
     */
    public TableCellBox(LayoutContext context, BlockBox parent, IVexElement element, int width) {
        super(context, parent, element);
        Styles styles = context.getStyleSheet().getStyles(element);
        this.setWidth(width 
                - styles.getBorderLeftWidth() 
                - styles.getPaddingLeft().get(parent.getWidth())
                - styles.getPaddingRight().get(parent.getWidth())
                - styles.getBorderRightWidth());
    }

    public TableCellBox(LayoutContext context, BlockBox parent, int startOffset, int endOffset, int width) {
        super(context, parent, startOffset, endOffset);
        this.setWidth(width);
    }

    protected List createChildren(LayoutContext context) {
        return this.createBlockBoxes(context, this.getStartOffset(), this.getEndOffset(), this.getWidth(), null, null);
    }

    public void setInitialSize(LayoutContext context) {
        // we've already set width in the ctor
        // override to avoid setting width again
        this.setHeight(this.getEstimatedHeight(context));
    }

	@Override
	public void paint(LayoutContext context, int x, int y, Rectangle area) {
		super.paint(context, x, y, area);
		if (this.getElement() != null && context.showBlockMarkers())
			paintMarkerFrame(context, x, y, this.getParent().getWidth());
	}

    

    //======================================================= PRIVATE
    
}
