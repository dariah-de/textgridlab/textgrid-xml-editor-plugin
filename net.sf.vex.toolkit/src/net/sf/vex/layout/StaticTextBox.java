/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2004 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.layout;

import net.sf.vex.core.ColorResource;
import net.sf.vex.core.FontResource;
import net.sf.vex.core.Graphics;
import net.sf.vex.core.Rectangle;
import net.sf.vex.css.Styles;
import net.sf.vex.dom.IVexElement;
import net.sf.vex.dom.IVexNode;
import net.sf.vex.dom.impl.Element;

/**
 * A TextBox representing a static string.
 */
public class StaticTextBox extends TextBox {

    public static final byte NO_MARKER = 0;
    public static final byte START_MARKER = 1;
    public static final byte END_MARKER = 2;
    
    private String text;
    private byte marker;
    
    /**
     * Class constructor.
     * 
     * @param context LayoutContext used to calculate the box's size.
     * @param pseudoElement Element used to style the text.
     * @param text Static text to display
     */
    public StaticTextBox(LayoutContext context, IVexElement pseudoElement, String text) {
        this(context, pseudoElement, text, NO_MARKER);
        if (text.length() == 0) {
            throw new IllegalArgumentException("StaticTextBox cannot have an empty text string.");
        }
    }
    
    /**
     * Class constructor. This constructor is used when generating a static
     * text box representing a marker for the start or end of an inline element.
     * If the selection spans the related marker, the text is drawn in the
     * platform's text selection colours.
     * 
     * @param context LayoutContext used to calculate the box's size
     * @param pseudoElement Element used to style the text
     * @param text Static text to display
     * @param marker START_MARKER or END_MARKER, depending on whether the
     * text represents the start sentinel or the end sentinel of the element 
     */
    public StaticTextBox(LayoutContext context, IVexElement pseudoElement, String text, byte marker) {
        super(pseudoElement);
        this.text = text;
        this.marker = marker;
        this.calculateSize(context);
    }
    
    public StaticTextBox(LayoutContext context, IVexNode node, Styles styles, String text, byte marker) {
    	super (styles, node);
    	this.text = text;
    	this.marker = marker;
    	this.calculateSize(context);
    }
    
    /**
     * @see net.sf.vex.layout.TextBox#getText()
     */
    public String getText() {
        return this.text;    
    }
    
    /**
     * @see net.sf.vex.layout.Box#hasContent()
     */
    public boolean hasContent() {
        return false;
    }
    
    /**
     * @see net.sf.vex.layout.Box#paint(net.sf.vex.layout.LayoutContext, int, int)
     */
    public void paint(LayoutContext context, int x, int y, Rectangle area) {
        
        Styles styles = getStyles(context);
        Graphics g = context.getGraphics();
        
        boolean drawSelected = false;
        if (this.marker == START_MARKER) {
            drawSelected = 
                this.getNode().getStartOffset() >= context.getSelectionStart()
                && this.getNode().getStartOffset() + 1 <= context.getSelectionEnd();
        } else if (this.marker == END_MARKER) {
            drawSelected = 
                this.getNode().getEndOffset() >= context.getSelectionStart()
                && this.getNode().getEndOffset() + 1 <= context.getSelectionEnd();
        }
        
        FontResource font = g.createFont(styles.getFont());
        ColorResource color = g.createColor(styles.getColor());

        FontResource oldFont = g.setFont(font);
        ColorResource oldColor = g.setColor(color);
        
        String textToPaint = this.getText();
        if (textToPaint.length() > 0 && isNewline(textToPaint.charAt(textToPaint.length()-1)))
        	textToPaint = textToPaint.substring(0, textToPaint.length()-1);
		if (drawSelected) {
            this.paintSelectedText(context, textToPaint, x, y);
        } else {
            g.drawString(textToPaint, x, y);
        }
        paintTextDecoration(context, styles, textToPaint, -1, -1, x, y);

        g.setFont(oldFont);
        g.setColor(oldColor);
        font.dispose();
        color.dispose();
    }
    
    


    /**
     * @see net.sf.vex.layout.TextBox#splitAt(int)
     */
    public Pair splitAt(LayoutContext context, int offset) {
        
        StaticTextBox left;
        if (offset == 0) {
            left = null; 
        } else {
            left = new StaticTextBox(context, this.getNode(), this.getStyles(context), this.getText().substring(0, offset), this.marker);
        }
        
        StaticTextBox right;
        if (offset == this.getText().length()) {
            right = null; 
        } else {
            right = new StaticTextBox(context, this.getNode(), this.getStyles(context), this.getText().substring(offset), this.marker);
        }
        return new Pair(left, right);
    }

}
