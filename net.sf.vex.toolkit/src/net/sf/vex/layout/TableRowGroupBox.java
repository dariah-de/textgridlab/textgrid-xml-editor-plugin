/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2005 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.layout;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import net.sf.vex.core.Insets;
import net.sf.vex.core.Rectangle;
import net.sf.vex.css.CSS;
import net.sf.vex.css.Styles;
import net.sf.vex.dom.IVexElement;

/**
 * Container for TableRowBox objects. May correspond to an element with
 * display:table-row-group, display:table-head-group, display:table-foot-group,
 * or may be anonymous.
 */
public class TableRowGroupBox extends AbstractBlockBox {

    /**
     * Class constructor for non-anonymous table row groups.
     * 
     * @param context LayoutContext to use.
     * @param parent Parent of this box.
     * @param element Element that generated this box.
     */
    public TableRowGroupBox(LayoutContext context, BlockBox parent, IVexElement element) {
        super(context, parent, element);
    }

    
    /**
     * Class constructor for anonymous table row groups.
     *
     * @param context LayoutContext to use.
     * @param parent Parent of this box.
     * @param startOffset Start of the range encompassing the table.
     * @param endOffset End of the range encompassing the table.
     */
    public TableRowGroupBox(LayoutContext context, BlockBox parent, int startOffset, int endOffset) {
        super(context, parent, startOffset, endOffset);
        
    }


    protected List createChildren(final LayoutContext context) {
        // TODO Auto-generated method stub
        
        // Walk children in range
        //     - table-row children get non-anonymous TableRowBox
        //     - runs of others get anonymous TableRowBox
        
        final List children = new ArrayList();
        
        this.iterateChildrenByDisplayStyle(context.getStyleSheet(), childDisplayStyles, new ElementOrRangeCallback() {
            public void onElement(IVexElement child, String displayStyle) {
                children.add(new TableRowBox(context, TableRowGroupBox.this, child));
            }
            public void onRange(IVexElement parent, int startOffset, int endOffset) {
                children.add(new TableRowBox(context, TableRowGroupBox.this, startOffset, endOffset));
            }
        });

        return children;
    }

    public Insets getInsets(LayoutContext context, int containerWidth) {
        return Insets.ZERO_INSETS;
    }

    public int getMarginBottom() {
        return 0;
    }

    public int getMarginTop() {
        return 0;
    }
    
    public void paint(LayoutContext context, int x, int y, Rectangle area) {

        if (this.skipPaint(context, x, y)) {
            return;
        }
        
        this.paintChildren(context, x, y, area);
        
        if (this.getElement()!=null && context.showBlockMarkers())
        	this.paintMarkerFrame(context, x, y, this.getParent().getWidth());
        //this.paintSelectionFrame(context, x, y);
    }


    protected int positionChildren(LayoutContext context) {
        
        Styles styles = context.getStyleSheet().getStyles(this.findContainingElement());
        int spacing = styles.getBorderSpacing().getVertical();
        
        int childY = spacing;
        for (int i = 0; i < this.getChildren().length; i++) {
            
            TableRowBox child = (TableRowBox) this.getChildren()[i];
            // TODO must force table row margins to be zero
            Insets insets = child.getInsets(context, this.getWidth());
            
            childY += insets.getTop();
            
            child.setX(insets.getLeft());
            child.setY(childY);
            
            childY += child.getHeight() + insets.getBottom() + spacing;
        }
        this.setHeight(childY);
        
        return -1; // TODO revisit
    }


    //====================================================== PRIVATE

    private static Set childDisplayStyles = new HashSet();
    
    static {
        childDisplayStyles.add(CSS.TABLE_ROW);
    }

    

}
