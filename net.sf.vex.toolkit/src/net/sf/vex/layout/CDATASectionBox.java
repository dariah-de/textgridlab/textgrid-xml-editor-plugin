package net.sf.vex.layout;

import net.sf.vex.core.Color;
import net.sf.vex.core.FontSpec;
import net.sf.vex.css.CSS;
import net.sf.vex.css.RelativeLength;
import net.sf.vex.css.Styles;
import net.sf.vex.dom.IVexCDATASection;
import net.sf.vex.dom.IVexComment;

public class CDATASectionBox extends NonElementBox {

	static Styles styles = new Styles() {
		{
			this.put(CSS.FONT_FAMILY, new String[] { "monospace" });
			this.put(CSS.FONT_SIZE, 10f);
			this.put(CSS.BORDER, 1f);
			this.put(CSS.COLOR, new Color(100, 230, 100));
			this.put(CSS.BACKGROUND_COLOR, new Color(165, 165, 0));
			this.put(CSS.BORDER_COLOR, new Color(200, 200, 200));

			this.put(CSS.LINE_HEIGHT, RelativeLength.createAbsolute(Math
					.round(this.getFontSize())));

			this.setFont(new FontSpec(this.getFontFamilies(), FontSpec.PLAIN,
					this.getFontSize()));

		}

	};

	static Styles markerStyles = new Styles() {
		{
			this.put(CSS.FONT_FAMILY, new String[] { "monospace" });
			this.put(CSS.FONT_SIZE, 10f);
			this.put(CSS.BORDER, 1f);
			this.put(CSS.COLOR, new Color(0, 100, 0));
			this.put(CSS.BACKGROUND_COLOR, new Color(165, 165, 0));
			this.put(CSS.BORDER_COLOR, new Color(2000, 200, 200));

			this.put(CSS.LINE_HEIGHT, RelativeLength.createAbsolute(Math
					.round(this.getFontSize())));

			this.setFont(new FontSpec(this.getFontFamilies(), FontSpec.PLAIN,
					this.getFontSize()));

		}

	};

	CDATASectionBox(LayoutContext context, IVexCDATASection section,
			InlineBox[] children) {

		super(context, section, children);
	}

	public CDATASectionBox (LayoutContext context, IVexCDATASection section,
			int startOffset, int endOffset) {
		super(context, section, startOffset, endOffset);
	}

	@Override
	protected Styles getStyles() {
		return styles;
	}

	@Override
	protected InlineBox getStartMarker(LayoutContext context) {
		return new StaticTextBox(context, element, markerStyles, "<![CDATA[",
				StaticTextBox.START_MARKER);
	}

	@Override
	protected InlineBox getEndMarker(LayoutContext context) {
		return new StaticTextBox(context, element, markerStyles, "]]>",
				StaticTextBox.END_MARKER);
	}

}
