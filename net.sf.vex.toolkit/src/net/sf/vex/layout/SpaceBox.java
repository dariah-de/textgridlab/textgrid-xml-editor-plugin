/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2004 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.layout;


/**
 * An empty inline box that simply takes up space.
 */
public class SpaceBox extends AbstractBox implements InlineBox {

    /**
     * Class constructor.
     * @param width width of the box
     * @param height height of the box
     */
    public SpaceBox(int width, int height) {
        this.setWidth(width);
        this.setHeight(height);
    }
    
    /**
     * @see net.sf.vex.layout.InlineBox#getBaseline()
     */
    public int getBaseline() {
        return this.getHeight();
    }

    public boolean isEOL() {
        return false;
    }
    
    /**
     * @see net.sf.vex.layout.InlineBox#split(net.sf.vex.layout.LayoutContext, int, boolean)
     */
    public Pair split(LayoutContext context, int maxWidth, boolean force) {
        return new Pair(null, this);
    }

    /**
     * @see java.lang.Object#toString()
     */
    public String toString() {
        return "[spacer]";
    }

}
