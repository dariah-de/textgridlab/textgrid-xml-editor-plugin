/*
 * Created on 02.11.2006
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package net.sf.vex.layout;

import net.sf.vex.core.Color;
import net.sf.vex.core.Drawable;
import net.sf.vex.core.FontResource;
import net.sf.vex.core.FontSpec;
import net.sf.vex.core.Graphics;
import net.sf.vex.core.Rectangle;

class UnlabeledInlineMarker implements Drawable { 

	// Some constants concerning the look

	protected int size;


	protected static final int[] color = new int[] { 0, 100, 255 };




	protected boolean isLeftMarker;

	public UnlabeledInlineMarker(boolean setLeftMarker, float fontsize) {
		this.size = (int) (fontsize * 0.6);
		isLeftMarker = setLeftMarker;
	}

	public void draw(Graphics g, int x, int y) {

		
		boolean oldAntiAliased = g.isAntiAliased();
		g.setAntiAliased(true);
		g.setColor(g.createColor(new Color(color[0], color[1], color[2])));
		g.setLineStyle(Graphics.LINE_SOLID);
		g.setLineWidth(1);
		
		int width = (int)(size * 0.5);
		
		if (isLeftMarker) {

			g.drawLine(x, y, x, y + size);
			g.drawLine(x , y + size, x + width , y + size / 2);
			g.drawLine(x + width, y + size / 2, x , y);
		} else {

			g.drawLine(x +  width, y, x  + width, y + size);

			g.drawLine(x + width, y + size, x , y + size / 2);
			g.drawLine(x , y + size / 2, x + width, y);

		}

	g.setAntiAliased(oldAntiAliased);
	}

	public Rectangle getBounds() {
		return new Rectangle(0, -size, size, size);
	}

}
