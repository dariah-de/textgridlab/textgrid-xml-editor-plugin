/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2004 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.layout;

import java.io.Serializable;

import net.sf.vex.dom.IVexElement;


/**
 * Interface to an object that creates boxes from elements. Implementations
 * of this interface must be serializable.
 */
public interface BoxFactory extends Serializable {

    /**
     * Creates a box given an element. 
     * @param context CSS styles for the new element
     * @param element Element for which the box should be created.
     * @param parent Parent box for the new box.
     * @param containerWidth Width of the box to be created.
     */
    public Box createBox(LayoutContext context, IVexElement element, BlockBox parent, int containerWidth);

}

