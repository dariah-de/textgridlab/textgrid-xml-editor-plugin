package net.sf.vex.layout;

import static net.sf.vex.VexToolkitPlugin.DEBUG_ANNOTATIONS;
import static net.sf.vex.VexToolkitPlugin.isDebugging;

import java.text.MessageFormat;

import net.sf.vex.core.Color;
import net.sf.vex.dom.IVexNode;
import net.sf.vex.dom.linked.LinkedDocument;
import net.sf.vex.dom.linked.LinkedText;

import org.eclipse.core.runtime.PlatformObject;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.Region;
import org.eclipse.jface.text.source.Annotation;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.ui.texteditor.AnnotationPreference;

/**
 * Access information required to render one specific annotation. Class is
 * managed by VexAnnotationTracker.
 */
public class AnnotationRenderingInfo extends PlatformObject {

	private IVexNode vexNode;

	private VexAnnotationTracker tracker;

	AnnotationRenderingInfo(final Annotation annotation,
			final VexAnnotationTracker tracker, final IVexNode vexNode) {
		super();
		org.eclipse.core.runtime.Assert.isLegal(annotation != null, "Cannot create rendering info for null");
		org.eclipse.core.runtime.Assert.isLegal(tracker != null,
				"VexAnnotationSupport required");
		this.annotation = annotation;
		this.tracker = tracker;
		this.support = tracker.getSupport();
		this.vexNode = vexNode;
	}

	/** The annotation this rendering info describes. */
	public final Annotation annotation;

	/**
	 * The annotation support that will be used to lookup and manage information
	 * for this annotation.
	 */
	public final VexAnnotationSupport support;

	private AnnotationPreference preference;

	public AnnotationPreference getPreference() {
		if (preference == null)
			preference = support.getPreference(annotation);
		return preference;
	}

	/** 
	 * Should the annotation be visualized in the text area?
	 * @see AnnotationPreference#getTextPreferenceValue()
	 */
	public boolean isShownInText() {
		boolean result = getPreference().getTextPreferenceValue();
		if (result && isDebugging(DEBUG_ANNOTATIONS))
			System.out.println(MessageFormat.format("Showing in text: {0}", annotation.getText()));
		return result;
	}

	public String toString() {
		return "AnnotationRenderingInfo [annotation=" + annotation + ", vexNode=" + vexNode + "]";
	}

	/**
	 * @see AnnotationPreference#getColorPreferenceValue()
	 */
	public Color getColor() {
		RGB rgb = getPreference().getColorPreferenceValue();
		return new Color(rgb.red, rgb.green, rgb.blue);
	}

	/**
	 * Returns the range this annotation adresses in Vex' content buffer
	 * 
	 * @return
	 */
	public Position getContentPosition() {
		// TODO caching
		if (vexNode instanceof LinkedText) {
			LinkedText text = (LinkedText) vexNode;
			Position position = tracker.getAnnotationModel().getPosition(
					annotation);
			int contentOffset = text.getContentOffsetFor(position.offset);
			int contentLength = text.getContentOffsetFor(position.offset
					+ position.length)
					- contentOffset;
			return new Position(contentOffset, contentLength);
		} else
			return new Position(vexNode.getStartOffset(), vexNode
					.getEndOffset()
					- vexNode.getStartOffset());
	}

	public Position getPositionIn(final LinkedDocument document) {
		final Position sourcePosition = tracker.getAnnotationModel()
				.getPosition(annotation);
		final IRegion contentRegion = document.contentRegionFor(new Region(
				sourcePosition.offset, sourcePosition.length));
		return new Position(contentRegion.getOffset(), contentRegion
				.getLength()); // Hope we're not converting back!?
	}
}