package net.sf.vex.layout;

import java.util.ArrayList;
import java.util.List;

import net.sf.vex.core.FontMetrics;
import net.sf.vex.core.FontResource;
import net.sf.vex.core.Graphics;
import net.sf.vex.core.Insets;
import net.sf.vex.core.Rectangle;
import net.sf.vex.css.Styles;
import net.sf.vex.dom.IVexCDATASection;
import net.sf.vex.dom.IVexComment;
import net.sf.vex.dom.IVexNonElement;
import net.sf.vex.layout.InlineBox.Pair;

public abstract class NonElementBox extends CompositeInlineBox {

	protected IVexNonElement element;
	protected InlineBox[] children;
	protected InlineBox firstContentChild = null;
	protected InlineBox lastContentChild = null;

	public NonElementBox(LayoutContext context, IVexNonElement element,
			int startOffset, int endOffset) {

		this.element = element;

		List<InlineBox> childList = new ArrayList<InlineBox>();

		// start marker
		// UnlabeledInlineMarker leftMarker = new UnlabeledInlineMarker(true,
		// (int) styles.getFontSize());
		// childList.add(new DrawableBox(leftMarker, element,
		// DrawableBox.START_MARKER));

		childList.add(getStartMarker(context));

		DocumentTextBox text = new DocumentTextBox(context, element,
				getStyles(), startOffset + 1, endOffset + 1);
		childList.add(text);

		firstContentChild = text;
		lastContentChild = text;

		// end marker
		// UnlabeledInlineMarker uRightMarker = new UnlabeledInlineMarker(false,
		// (int) styles.getFontSize());
		// childList.add(new DrawableBox(uRightMarker, element,
		// DrawableBox.END_MARKER));

		childList.add(getEndMarker(context));

		this.children = (InlineBox[]) childList.toArray(new InlineBox[childList
				.size()]);

		this.layout(context);

	}

	public int getBaseline() {
		return 0;
	}

	public static NonElementBox createNonElementBox(LayoutContext context,
			IVexNonElement element, int startOffset, int endOffset) {
		if (element instanceof IVexComment) {
			return new CommentBox(context, (IVexComment) element, startOffset,
					endOffset);
		} else if (element instanceof IVexCDATASection) {
			return new CDATASectionBox(context, (IVexCDATASection) element,
					startOffset, endOffset);

		} else {
			throw new IllegalArgumentException(
					"Must be of type IVexComment or IVexCDATASection");
		}

	}

	protected static NonElementBox createNonElementBox(LayoutContext context,
			IVexNonElement element, InlineBox[] children) {

		if (element instanceof IVexComment) {
			return new CommentBox(context, (IVexComment) element, children);
		} else if (element instanceof IVexCDATASection) {
			return new CDATASectionBox(context, (IVexCDATASection) element,
					children);
		} else {
			throw new IllegalArgumentException(
					"Must be of type IVexComment or IVexCDATASection");
		}
	}

	protected NonElementBox(LayoutContext context, IVexNonElement element,
			InlineBox[] children) {

		this.element = element;
		this.children = children;

		for (InlineBox c : children) {
			if (firstContentChild == null && c.hasContent()) {
				firstContentChild = c;
			}

			if (c.hasContent()) {
				lastContentChild = c;
			}
		}

		this.layout(context);
	}

	@Override
	public Pair split(LayoutContext context, InlineBox[] lefts,
			InlineBox[] rights) {

		NonElementBox left = null;
		NonElementBox right = null;

		if (lefts.length > 0 || rights.length == 0) {
			left = createNonElementBox(context, this.element, lefts);

		}

		if (rights.length > 0) {
			right = createNonElementBox(context, this.element, rights);
		}

		return new Pair(left, right);
	}

	/**
	 * @see net.sf.vex.layout.Box#getChildren()
	 */
	@Override
	public Box[] getChildren() {
		return this.children;
	}

	/**
	 * @see net.sf.vex.layout.Box#getEndOffset()
	 */
	@Override
	public int getEndOffset() {
		if (this.lastContentChild == null) {
			return this.getElement().getEndOffset();
		} else {
			return this.lastContentChild.getEndOffset();
		}
	}

	/**
	 * @see net.sf.vex.layout.Box#getStartOffset()
	 */
	@Override
	public int getStartOffset() {
		if (this.firstContentChild == null) {
			return this.getElement().getStartOffset();
		} else {
			return this.firstContentChild.getStartOffset();
		}
	}

	protected void layout(LayoutContext context) {
		Graphics g = context.getGraphics();

		FontResource font = g.createFont(getStyles().getFont());
		FontResource oldFont = g.setFont(font);
		FontMetrics fm = g.getFontMetrics();
		this.setHeight(getStyles().getLineHeight());
		int halfLeading = (getStyles().getLineHeight() - fm.getAscent() - fm
				.getDescent()) / 2;

		Insets ins = this.getInsets(context, this.getWidth());

		int x = 0;
		for (int i = 0; i < this.children.length; i++) {

			InlineBox child = this.children[i];

			Insets childIns = child.getInsets(context, this.getWidth());

			child.setX(x + ins.getLeft());

			x += child.getWidth() + childIns.getLeft() + childIns.getRight();

			this.setHeight(Math.max(this.getHeight(), child.getHeight()
					+ childIns.getTop() + childIns.getBottom()));

		}

		this.setWidth(x);

		for (InlineBox b : this.children) {
			// TODO: Proper vertical alignment
			// Right now, it centers the lines
			Insets childIns = Insets.ZERO_INSETS;
			if (b instanceof InlineElementBox)
				childIns = b.getInsets(context, this.getWidth());

			b
					.setY(ins.getTop()
							+ (this.getHeight() - b.getHeight()
									- childIns.getTop() - childIns.getBottom())
							/ 2);

		}

		g.setFont(oldFont);
		font.dispose();

	}

	/**
	 * Override to paint background and borders.
	 * 
	 * @see net.sf.vex.layout.AbstractBox#paint(net.sf.vex.layout.LayoutContext,
	 *      int, int)
	 */
	@Override
	public void paint(LayoutContext context, int x, int y, Rectangle area) {
		Insets ins = this.getInsets(context, this.getWidth());

		boolean drawSelected = context.isElementSelected(this.getElement());

		this.drawBox(context, x + ins.getLeft(), y + ins.getTop(), 0,
				drawSelected);

		super.paint(context, x, y, area);
	}

	abstract protected Styles getStyles();

	abstract protected InlineBox getStartMarker(LayoutContext context);

	abstract protected InlineBox getEndMarker(LayoutContext context);
}
