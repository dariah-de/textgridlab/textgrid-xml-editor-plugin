/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2004 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.swt;

import org.eclipse.swt.events.KeyEvent;

/**
 * Represents a keystroke and a certain set of modifiers.
 */
public class KeyStroke {

    private char character;
    private int keyCode;
    private int stateMask;
    
    /**
     * Class constructor.
     * @param character the key character
     * @param keyCode the key code
     * @param stateMask the set of modifiers
     */
    public KeyStroke(char character, int keyCode, int stateMask) {
        this.character = character;
        this.keyCode = keyCode;
        this.stateMask = stateMask;
    }
    
    /**
     * Class constructor.
     * @param e a KeyEvent representing the key stroke
     */
    public KeyStroke(KeyEvent e) {
        this.character = e.character;
        this.keyCode = e.keyCode;
        this.stateMask = e.stateMask;
    }
    
    @Override
	public boolean equals(Object o) {
        if (o == null || !(o instanceof KeyStroke)) {
            return false;
        }
        KeyStroke other = (KeyStroke) o;
        return this.character == other.character
            && this.keyCode == other.keyCode
            && this.stateMask == other.stateMask;
    }
    
    @Override
	public String toString() {
		return String
				.format(
						"KeyStroke(U+%04X,%#x+%#x = %s)", (int) character,
				keyCode, stateMask, org.eclipse.jface.bindings.keys.KeyStroke
						.getInstance(stateMask,
								keyCode != 0 ? keyCode : character).format());
	}

	@Override
	public int hashCode() {
        return this.character + this.keyCode + this.stateMask;
    }

}
