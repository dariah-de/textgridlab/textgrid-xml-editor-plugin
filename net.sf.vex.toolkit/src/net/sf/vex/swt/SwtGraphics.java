/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2004 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.swt;

import net.sf.vex.core.Color;
import net.sf.vex.core.ColorResource;
import net.sf.vex.core.FontMetrics;
import net.sf.vex.core.FontResource;
import net.sf.vex.core.FontSpec;
import net.sf.vex.core.Graphics;
import net.sf.vex.core.Rectangle;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.widgets.Display;

/**
 * Implementation of the Vex Graphics interface, mapping it to a 
 * org.eclipse.swt.graphics.GC object.
 * 
 * <p>The GC given to us by SWT is that of the Canvas, which is just a viewport
 * into the document. This class therefore implements an "origin", which
 * represents the top-left corner of the document relative to the 
 * top-left corner of the canvas. The x- and y-coordinates of the origin
 * are always negative.</p>.
 */
public class SwtGraphics implements Graphics {

    private GC gc;
    private int originX;
    private int originY;
    
    /** Fix a bug where SWT canvas coordinates will overflow
     * at 2^16. This is not a perfect solution and so far only
     * implemented in a few functions. */
    private static final int FAR_OUT_FIX =30000;
    /**
     * Class constructor.
     * @param gc SWT GC to which we are drawing.
     */
    public SwtGraphics(GC gc) {
        this.gc = gc;
    }
    
    public void dispose() {
        this.gc.dispose();
    }
    
    public void drawChars(char[] chars, int offset, int length, int x, int y) {
        this.drawString(new String(chars, offset, length), x, y);

    }

    
   
    public void drawLine(int x1, int y1, int x2, int y2) {
        this.gc.drawLine(x1 + originX, y1 + originY, x2 + originX, y2 + originY);
    }

    public void drawOval(int x, int y, int width, int height) {
    	
        this.gc.drawOval(x + originX, y + originY, width, height);
    }

    public void drawRect(int x, int y, int width, int height) {
        
    	
    	//this.gc.drawRectangle(x + originX, y + originY, width, height);
    	//SWT canvas overflow fix
    	int startY = y + originY;
    	int endY= y + originY+height;
    	
    	if (startY < -FAR_OUT_FIX) {
    		startY = -FAR_OUT_FIX;
    	} else 
        	this.gc.drawLine(x+originX, startY, x+originX+width, startY);
    		
    	if (endY > FAR_OUT_FIX) {
    		endY = FAR_OUT_FIX;
    	} else
        	this.gc.drawLine(x+originX, endY, x+originX+width, 
        			endY);
    		
    	this.gc.drawLine(x+originX, startY, x+originX, endY);
    	this.gc.drawLine(x+originX+width, startY, x+originX+width,
    			endY);
    	
    }

    public void drawString(String s, int x, int y) {
    	//SWT Canvas overflow fix
    	int startY = y + originY;
    	
    	if (startY < -FAR_OUT_FIX) 
    		startY = -FAR_OUT_FIX;
    	if (startY > FAR_OUT_FIX) 
    		startY = FAR_OUT_FIX;

    	this.gc.drawString(s, x + originX, startY, true);
    }
    
    
    
    

    /* draws image
     * (non-Javadoc)
     * @see net.sf.vex.core.Graphics#drawImage(org.eclipse.swt.graphics.Image, int, int, int, int)
     */
	public void drawImage(Image image, int x, int y, int width, int height) {
	
//		ImageData data = image.getImageData();
//		int srcX = data.width / 2;
//        int srcY = data.height / 2;
//        int srcWidth = data.width / 2;
//        int srcHeight = data.height / 2;
//        int destWidth = 2 * srcWidth;
//        int destHeight = 2 * srcHeight;
        //gc.drawImage(image, srcX, srcY, srcWidth, srcHeight, width - destWidth, height - destHeight, destWidth, destHeight);
        
		
		gc.drawImage(image, x+ originX, y+ originY);		
        //gc.drawImage(image, 0, 0, width, height, x+ originX, y+ originY, width, height);
        
	}
        

    /**
     * Fills the given oval with the <em>foreground</em> color. This overrides
     * the default SWT behaviour to be more like Swing.
     */
    public void fillOval(int x, int y, int width, int height) {
    	    	
        this.gc.fillOval(x + originX, y + originY, width, height);
    }

    /**
     * Fills the given rectangle with the <em>foreground</em> color. This overrides
     * the default SWT behaviour to be more like Swing.
     */
    public void fillRect(int x, int y, int width, int height) {
    	
    	//SWT Canvas overflow fix
    	int startY = y + originY;
    	
    	if (startY < -FAR_OUT_FIX) 
    		startY = -FAR_OUT_FIX;
    	if (startY +height > FAR_OUT_FIX) 
    		height = FAR_OUT_FIX-startY;

    	
    	this.gc.fillRectangle(x + originX, startY, width, height);

    }

    public Rectangle getClipBounds() {
        org.eclipse.swt.graphics.Rectangle r = this.gc.getClipping();
        //return new Rectangle(r.x - this.originX, r.y - this.originY, r.width, r.height);
        return new Rectangle(r.x - this.originX, r.y - this.originY, r.width, r.height);
    }

    public ColorResource getColor() {
        return new SwtColor(this.gc.getForeground());
    }

    public FontResource getFont() {
        return new SwtFont(this.gc.getFont());
    }

    public FontMetrics getFontMetrics() {
        return new SwtFontMetrics(this.gc.getFontMetrics());
    }

    public int getLineStyle() {
        return this.lineStyle;
    }
    
    public int getLineWidth() {
        return this.gc.getLineWidth();
    }
    
    public boolean isAntiAliased() {
        return false;
    }

    public void setAntiAliased(boolean antiAliased) {
    //This is disabled for a good reason: It causes display error in Windows
    //so DON'T implement it!
    }

    public ColorResource setColor(ColorResource color) {
        ColorResource oldColor = this.getColor();
        this.gc.setForeground(((SwtColor) color).getSwtColor());
        this.gc.setBackground(((SwtColor) color).getSwtColor());
        return oldColor;
    }

    public FontResource setFont(FontResource font) {
        FontResource oldFont = this.getFont();
        this.gc.setFont(((SwtFont) font).getSwtFont());
        return oldFont;
    }

    public void setLineStyle(int lineStyle) {
        this.lineStyle = lineStyle;
        switch (lineStyle) {
        case LINE_DASH:
            this.gc.setLineStyle(SWT.LINE_DASH);
            break;
        case LINE_DOT:
            this.gc.setLineStyle(SWT.LINE_DOT);
            break;
        default:
            this.gc.setLineStyle(SWT.LINE_SOLID);
            break;
        }
    }

    public void setLineWidth(int lineWidth) {
        this.gc.setLineWidth(lineWidth);
    }
    
    public int charsWidth(char[] data, int offset, int length) {
        return this.stringWidth(new String(data, offset, length));
    }

    public ColorResource createColor(Color rgb) {
        return new SwtColor( 
            new org.eclipse.swt.graphics.Color(
                null, rgb.getRed(), rgb.getGreen(), rgb.getBlue())); 
    }

    public FontResource createFont(FontSpec fontSpec) {
        int style = SWT.NORMAL;
        if ((fontSpec.getStyle() & FontSpec.BOLD) > 0) {
            style |= SWT.BOLD;
        }
        if ((fontSpec.getStyle() & FontSpec.ITALIC) > 0) {
            style |= SWT.ITALIC;
        }
        int size = Math.round(fontSpec.getSize() * 72 / 90); // TODO: fix. SWT uses pts, AWT uses device units
        String[] names = fontSpec.getNames();
        FontData[] fd = new FontData[names.length];
        for (int i = 0; i < names.length; i++) {
            fd[i] = new FontData(names[i], size, style);
        }
        return new SwtFont(new org.eclipse.swt.graphics.Font(null, fd));
    }

    public ColorResource getSystemColor(int id) {
        
        if (id == ColorResource.SELECTION_BACKGROUND) {
            return new SwtColor(Display.getCurrent().getSystemColor(SWT.COLOR_LIST_SELECTION));
        } else if (id == ColorResource.SELECTION_FOREGROUND) {
            return new SwtColor(Display.getCurrent().getSystemColor(SWT.COLOR_LIST_SELECTION_TEXT));
        } else {
            return new SwtColor(Display.getCurrent().getSystemColor(-1));
        }
    }

    /**
     * Sets the origin of this graphics object. See the class description
     * for more details.
     * 
     * @param x x-coordinate of the origin, relative to the viewport.
     * @param y y-coordinate of the origin, relative to the viewport.
     */
    public void setOrigin(int x, int y) {
        this.originX = x;
        this.originY = y;
    }
    
    public int stringWidth(String s) {
        return this.gc.stringExtent(s).x;
    }

    
    //========================================================== PRIVATE
    
    private int lineStyle = LINE_SOLID;
    
    

}
