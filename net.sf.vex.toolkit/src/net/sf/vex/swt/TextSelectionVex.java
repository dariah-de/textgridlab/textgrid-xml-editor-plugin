package net.sf.vex.swt;

import static net.sf.vex.VexToolkitPlugin.DEBUG_SELECTION;
import net.sf.vex.VexToolkitPlugin;
import net.sf.vex.dom.IVexDocument;
import net.sf.vex.dom.IVexNode;
import net.sf.vex.dom.linked.LinkedComment;
import net.sf.vex.dom.linked.LinkedElement;
import net.sf.vex.dom.linked.LinkedNode;
import net.sf.vex.dom.linked.LinkedNonElement;
import net.sf.vex.dom.linked.LinkedText;
import net.sf.vex.widget.IVexWidget;

import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.text.Region;
import org.eclipse.wst.sse.core.internal.provisional.IndexedRegion;
import org.eclipse.wst.xml.core.internal.provisional.document.IDOMNode;

/**
 * A Text selection in Vex.
 * 
 * TODO optimize this for time.
 * 
 * @see ITextSelection
 */
@SuppressWarnings("restriction")
public class TextSelectionVex implements ITextSelection {

	private int startOffset;
	private int length = 0;
	private String text = null;
	private IVexWidget vexWidget;
	private String word = null;
	private IVexNode[] nodes;
	private int sourceStartOffset = -1;
	private int sourceEndOffset = -1;

	/**
	 * Creates a new non-empty Vex selection.
	 * 
	 * @param vexWidget
	 * @param startOffset
	 * @param length
	 * @param text
	 *            the selected text.
	 * @param nodes
	 *            the nodes covered by this selection.
	 */
	public TextSelectionVex(VexWidget vexWidget, int startOffset, int length,
			String text, IVexNode[] nodes) {
		this.vexWidget = vexWidget;
		this.startOffset = startOffset;
		this.length = length;
		this.text = text;
		this.nodes = nodes;
	}

	/**
	 * Creates a new empty Vex selection.
	 * 
	 * @param startOffset
	 * @param node
	 */
	public TextSelectionVex(VexWidget vexWidget, int startOffset, IVexNode node) {
		this.vexWidget = vexWidget;
		this.startOffset = startOffset;
		this.length = 0;
		this.nodes = new IVexNode[] { node };
	}

	/**
	 * Creates a new Vex selection object and extracts all relevant information
	 * from the given vexWidget.
	 * 
	 * @param vexWidget
	 */
	public TextSelectionVex(IVexWidget vexWidget) {
		this.vexWidget = vexWidget;
		this.startOffset = vexWidget.getSelectionStart();
		if (vexWidget.hasSelection()) {
			this.length = vexWidget.getSelectionEnd() - startOffset;
			this.text = vexWidget.getSelectedText();
			this.nodes = vexWidget.getDocument().getNodes(startOffset,
					startOffset + length);
			
		} else {
			this.length = 0;
			this.nodes = new IVexNode[] { vexWidget.getCurrentNode() };
		}
	}

	/**
	 * Tries to calculate this selection's offsets in the corresponding source
	 * code editor, if applicable.
	 */
	protected void calculateSourcePositions() {
		if (nodes == null || nodes.length < 1
				|| !(nodes[0] instanceof LinkedNode))
			return; // not applicable

		LinkedNode firstNode = (LinkedNode) nodes[0];
		LinkedNode lastNode = (LinkedNode) nodes[nodes.length - 1];

		int sourceStartCand = ((IndexedRegion) firstNode
				.getAdapter(IndexedRegion.class)).getStartOffset();
		int sourceEndCand = ((IndexedRegion) lastNode
				.getAdapter(IndexedRegion.class)).getEndOffset();

		int relStartOffset = 0;
		if (firstNode.getStartOffset() != startOffset) {
			relStartOffset = startOffset - firstNode.getStartOffset();
			sourceStartCand += relStartOffset;
		}
		if (firstNode instanceof LinkedNonElement) {
			sourceStartCand += ((LinkedNonElement) firstNode)
					.getCorrectionForContentPos();

		}

		else if (firstNode instanceof LinkedText) {
			sourceStartCand += ((LinkedText) firstNode)
					.getCorrectionForContentPos(relStartOffset);
		} else if (firstNode instanceof LinkedElement) {
			IDOMNode domNode = (IDOMNode) firstNode.getAdapter(IDOMNode.class);
			if (domNode != null) {
				if (relStartOffset == 1) { // after start tag
					sourceStartCand += domNode.getFirstStructuredDocumentRegion().getLength() - 1;
				} else if (startOffset == firstNode.getEndOffset()) { // before
																		// end
																		// tag
					sourceStartCand = domNode.getLastStructuredDocumentRegion()
							.getStartOffset();
				}
			}
		}
		if (length == 0)
			sourceEndOffset = sourceStartCand;
		else {
			int relEndOffset = 0;
			// TODO refactor this to more readable and less redundant code
			if (lastNode.getEndOffset() != startOffset + length) {
				sourceEndCand = ((IndexedRegion) lastNode
						.getAdapter(IndexedRegion.class)).getStartOffset();
				relEndOffset = (startOffset + length)
						- lastNode.getStartOffset();
				sourceEndCand += relEndOffset;
			}
			if (lastNode instanceof LinkedNonElement) {
				sourceEndCand += ((LinkedNonElement) lastNode)
						.getCorrectionForContentPos();

			}

			else if (lastNode instanceof LinkedText) {
				sourceEndCand += ((LinkedText) lastNode)
						.getCorrectionForContentPos(relEndOffset);
			}
			sourceEndOffset = sourceEndCand;
		}
		sourceStartOffset = sourceStartCand;
		if (VexToolkitPlugin.isDebugging(DEBUG_SELECTION)) {
			System.out.println("TextSelectionVex(" + startOffset + "+" + length
					+ "): source(" + sourceStartOffset + ".." + sourceEndOffset
					+ ") (" + firstNode + ".." + lastNode + ")");
		}
	}

	/**
	 * If the current editor is embedded (ie. uses the linked model), return an
	 * approximation of the start position
	 * 
	 * @return
	 */
	public int getSourceStartOffset() {
		if (sourceStartOffset == -1)
			calculateSourcePositions();

		return sourceStartOffset;
	}

	public int getSourceEndOffset() {
		if (length == 0)
			return getSourceStartOffset();

		if (sourceEndOffset == -1)
			calculateSourcePositions();

		return sourceEndOffset;
	}

	/**
	 * Try to extract the word at the selection's start position.
	 * 
	 * @return
	 */
	public String getCurrentWord() {
		if (word != null)
			return word;

		/*
		 * // Elements are always word boundaries -> only look in current
		 * element. IVexElement element = vexWidget.getCurrentElement(); String
		 * text = element.getText(); int localOffset = Math.max(0, startOffset -
		 * element.getStartOffset() - 1); // FIXME: this may yield wrong
		 * results. Why?
		 * 
		 * 
		 * int left = localOffset; int right = localOffset; int length =
		 * text.length();
		 * 
		 * while (right < length && Character.isLetter(text.codePointAt(right)))
		 * right++; while (left > 0 &&
		 * Character.isLetter(text.codePointBefore(left))) left--; word =
		 * text.substring(left, right);
		 */

		IVexDocument document = vexWidget.getDocument();
		int left = startOffset - 1;
		int right = left;
		int length = document.getLength();

		while (right < length
				&& Character.isLetter(document.getCharacterAt(right)))
			right++;
		while (left > 0 && Character.isLetter(document.getCharacterAt(left)))
			left--;
		if (left < right)
			left++;

		word = document.getRawText(left, right);
		
		
		// DEBUG: System.err.println(text.substring(0, left) + "[" +
		// text.substring(left, localOffset) + "|" + text.substring(localOffset,
		// right)+ "]"+ text.substring(right));

		return word;
	}

	public int getEndLine() {
		// TODO Auto-generated method stub
		return 0;
	}

	public int getLength() {
		return length;
	}

	public int getOffset() {
		return startOffset;
	}

	public int getStartLine() {
		// TODO Auto-generated method stub
		return 0;
	}

	public String getText() {
		return text;
	}

	public boolean isEmpty() {
		return (length < 1);
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return super.toString() + ": @" + startOffset
				+ (isEmpty() ? " (empty)" : " " + text + " (" + length + ")");
	}

}
