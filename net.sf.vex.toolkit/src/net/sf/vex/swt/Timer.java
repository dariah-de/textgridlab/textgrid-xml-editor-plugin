/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2004 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.swt;

import org.eclipse.swt.widgets.Display;

/**
 * Periodic timer, built using the Display.timerExec method.
 */
public class Timer {

    /**
     * Class constructor. The timer must be explicitly started using the
     * start() method.
     * @param periodMs Milliseconds between each invocation.
     * @param runnable Runnable to execute when the period expires.
     */
    public Timer(int periodMs, Runnable runnable) {
        this.periodMs = periodMs;
        this.runnable = runnable;
    }

    /**
     * Reset the timer so that it waits another period before firing.
     */
    public void reset() {
        if (this.started) {
            this.stop();
            this.start();
        }
    }

    /**
     * Start the timer. 
     */
    public void start() {
        if (!this.started) {
            this.innerRunnable = new InnerRunnable();
            Display.getCurrent().timerExec(this.periodMs, this.innerRunnable);
            this.started = true;
        }
    }

    /**
     * Stop the timer.
     */
    public void stop() {
        if (this.started) {
            this.innerRunnable.discarded = true;
            this.innerRunnable = null;
            this.started = false;
        }
    }
    
    //==================================================== PRIVATE
    
    private Runnable runnable;
    private int periodMs;
    private boolean started = false;
    private InnerRunnable innerRunnable;
    
    private class InnerRunnable implements Runnable {
        public boolean discarded = false;
        public void run() {
            if (!discarded) {
                runnable.run();
                Display display = Display.getCurrent();
                Display.getCurrent().timerExec(periodMs, this);
            }
        }
    }
}
