/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2005 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.swt;

import net.sf.vex.core.DisplayDevice;

import org.eclipse.swt.widgets.Display;

/**
 * Adapts the DisplayDevice display to the current SWT display.
 */
public class SwtDisplayDevice extends DisplayDevice {

    /**
     * Class constructor.
     */
    public SwtDisplayDevice() {
        // We used to do it like this, but it turns out sometimes we did it 
        // too early and getCurrent() returned null, so now the convoluted stuff below.
//        Display display = Display.getCurrent();
//        this.horizontalPPI = display.getDPI().x;
//        this.verticalPPI = display.getDPI().y;
    }

    public int getHorizontalPPI() {
        if (!this.loaded) {
            this.load();
        }
        return this.horizontalPPI;
    }

    public int getVerticalPPI() {
        if (!this.loaded) {
            this.load();
        }
        return this.verticalPPI;
    }

    private boolean loaded = false;
    private int horizontalPPI = 72;
    private int verticalPPI = 72;
    
    private void load() {
        Display display = Display.getCurrent();
        if (display != null) {
            this.horizontalPPI = display.getDPI().x;
            this.verticalPPI = display.getDPI().y;
            this.loaded = true;
        }        
    }
    
}
