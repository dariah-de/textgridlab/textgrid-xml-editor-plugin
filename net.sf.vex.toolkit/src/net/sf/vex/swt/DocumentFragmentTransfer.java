/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2004 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.swt;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import net.sf.vex.dom.IVexDocumentFragment;

import org.eclipse.swt.dnd.ByteArrayTransfer;
import org.eclipse.swt.dnd.TransferData;

/**
 * Transfer object that handles Vex DocumentFragments. 
 */
public class DocumentFragmentTransfer extends ByteArrayTransfer {

    /**
     * Returns the singleton instance of the DocumentFragmentTransfer.
     */
    public static DocumentFragmentTransfer getInstance() {
        if (instance == null) {
            instance = new DocumentFragmentTransfer();
        }
        return instance;
    }
    
    protected String[] getTypeNames() {
        return typeNames;
    }

    protected int[] getTypeIds() {
        return typeIds;
    }

    public void javaToNative (Object object, TransferData transferData) {
        if (object == null || !(object instanceof IVexDocumentFragment)) return;
    
        if (isSupportedType(transferData)) {
            IVexDocumentFragment frag = (IVexDocumentFragment) object;   
            try {
                // write data to a byte array and then ask super to convert to pMedium
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                ObjectOutputStream oos = new ObjectOutputStream(out);
                oos.writeObject(frag);
                byte[] buffer = out.toByteArray();
                oos.close();
                super.javaToNative(buffer, transferData);
            } catch (IOException e) {
            }
        }
     }

     public Object nativeToJava(TransferData transferData){ 
 
        if (isSupportedType(transferData)) {
            byte[] buffer = (byte[])super.nativeToJava(transferData);
            if (buffer == null) return null;
        
            try {
                ByteArrayInputStream in = new ByteArrayInputStream(buffer);
                ObjectInputStream ois = new ObjectInputStream(in);
                Object object = ois.readObject();
                ois.close();
                return object;
            } catch (ClassNotFoundException ex) {
                return null;
            } catch (IOException ex) {
                return null;
            }
        }
 
        return null;
     }

    //=================================================== PRIVATE
    
    private static final String[] typeNames = { IVexDocumentFragment.MIME_TYPE };
    private static final int[] typeIds = { 
        ByteArrayTransfer.registerType(IVexDocumentFragment.MIME_TYPE) 
    };
    
    private static DocumentFragmentTransfer instance;
    
    private DocumentFragmentTransfer() {
    }
}
