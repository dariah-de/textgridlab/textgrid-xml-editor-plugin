/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2005 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.css;

import org.w3c.css.sac.LexicalUnit;

/**
 * The CSS list-style-type property.
 */
public class ListStyleTypeProperty extends AbstractProperty {

    public ListStyleTypeProperty() {
        super(CSS.LIST_STYLE_TYPE);
    }

    public Object calculate(LexicalUnit lu, Styles parentStyles, Styles styles) {
        if (isListStyleType(lu)) {
            return lu.getStringValue();
        } else {
            if (parentStyles == null) {
                return CSS.DISC;
            } else {
                return parentStyles.getListStyleType();
            }
        }
        
    }

    private static boolean isListStyleType(LexicalUnit lu) {
        
        if (lu == null || lu.getLexicalUnitType() != LexicalUnit.SAC_IDENT) {
            return false;
        }
        
        String s = lu.getStringValue();
        return s.equalsIgnoreCase(CSS.ARMENIAN)
            || s.equalsIgnoreCase(CSS.CIRCLE)
            || s.equalsIgnoreCase(CSS.CJK_IDEOGRAPHIC)
            || s.equalsIgnoreCase(CSS.DECIMAL)
            || s.equalsIgnoreCase(CSS.DECIMAL_LEADING_ZERO)
            || s.equalsIgnoreCase(CSS.DISC)
            || s.equalsIgnoreCase(CSS.GEORGIAN)
            || s.equalsIgnoreCase(CSS.HEBREW)
            || s.equalsIgnoreCase(CSS.HIRAGANA)
            || s.equalsIgnoreCase(CSS.HIRAGANA_IROHA)
            || s.equalsIgnoreCase(CSS.KATAKANA)
            || s.equalsIgnoreCase(CSS.KATAKANA_IROHA)
            || s.equalsIgnoreCase(CSS.LOWER_ALPHA)
            || s.equalsIgnoreCase(CSS.LOWER_GREEK)
            || s.equalsIgnoreCase(CSS.LOWER_LATIN)
            || s.equalsIgnoreCase(CSS.LOWER_ROMAN)
            || s.equalsIgnoreCase(CSS.NONE)
            || s.equalsIgnoreCase(CSS.SQUARE)
            || s.equalsIgnoreCase(CSS.UPPER_ALPHA)
            || s.equalsIgnoreCase(CSS.UPPER_LATIN)
            || s.equalsIgnoreCase(CSS.UPPER_ROMAN);
    }

}
