/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2004 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.css;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;

import net.sf.vex.core.FontSpec;
import net.sf.vex.dom.IVexElement;

import org.w3c.css.sac.LexicalUnit;

import com.google.common.collect.Sets;

/**
 * Represents a CSS style sheet.
 */
public class StyleSheet extends GeneratedContentPropertiesImpl implements Serializable {

    /**
     * Standard CSS properties.
     */
    private static final IProperty[] CSS_PROPERTIES = new IProperty[] {
            new DisplayProperty(),
            new LineHeightProperty(),
            new ListStyleTypeProperty(),
            new TextAlignProperty(),
            new WhiteSpaceProperty(),
            
            new FontFamilyProperty(),
            new FontSizeProperty(),
            new FontStyleProperty(),
            new FontWeightProperty(),
            new TextDecorationProperty(),
            
            new ColorProperty(CSS.COLOR),
            new ColorProperty(CSS.BACKGROUND_COLOR),
            
            new LengthProperty(CSS.MARGIN_BOTTOM, IProperty.AXIS_VERTICAL),
            new LengthProperty(CSS.MARGIN_LEFT, IProperty.AXIS_HORIZONTAL),
            new LengthProperty(CSS.MARGIN_RIGHT, IProperty.AXIS_HORIZONTAL),
            new LengthProperty(CSS.MARGIN_TOP, IProperty.AXIS_VERTICAL),
            
            new LengthProperty(CSS.PADDING_BOTTOM, IProperty.AXIS_VERTICAL),
            new LengthProperty(CSS.PADDING_LEFT, IProperty.AXIS_HORIZONTAL),
            new LengthProperty(CSS.PADDING_RIGHT, IProperty.AXIS_HORIZONTAL),
            new LengthProperty(CSS.PADDING_TOP, IProperty.AXIS_VERTICAL),
            
            new ColorProperty(CSS.BORDER_BOTTOM_COLOR),
            new ColorProperty(CSS.BORDER_LEFT_COLOR),
            new ColorProperty(CSS.BORDER_RIGHT_COLOR),
            new ColorProperty(CSS.BORDER_TOP_COLOR),
            new BorderStyleProperty(CSS.BORDER_BOTTOM_STYLE),
            new BorderStyleProperty(CSS.BORDER_LEFT_STYLE),
            new BorderStyleProperty(CSS.BORDER_RIGHT_STYLE),
            new BorderStyleProperty(CSS.BORDER_TOP_STYLE),
            new BorderWidthProperty(CSS.BORDER_BOTTOM_WIDTH, CSS.BORDER_BOTTOM_STYLE, IProperty.AXIS_VERTICAL),
            new BorderWidthProperty(CSS.BORDER_LEFT_WIDTH, CSS.BORDER_LEFT_STYLE, IProperty.AXIS_HORIZONTAL),
            new BorderWidthProperty(CSS.BORDER_RIGHT_WIDTH, CSS.BORDER_RIGHT_STYLE, IProperty.AXIS_HORIZONTAL),
            new BorderWidthProperty(CSS.BORDER_TOP_WIDTH, CSS.BORDER_TOP_STYLE, IProperty.AXIS_VERTICAL),
            new BorderSpacingProperty(),
            
            new ImageHeightProperty(),
            new ImageWidthProperty(),
            new BackgroundImageProperty(),
    };

    /**
     * The properties to calculate. This can be changed by the app.
     */
    private static IProperty[] properties = CSS_PROPERTIES;

    /**
     * Style sheet is the default for the renderer.
     */
    public static final byte SOURCE_DEFAULT = 0;

    /**
     * Style sheet was provided by the document author.
     */
    public static final byte SOURCE_AUTHOR = 1;

    /**
     * Style sheet was provided by the user.
     */
    public static final byte SOURCE_USER = 2;

    /**
     * The rules that comprise the stylesheet.
     */
    private Rule[] rules;
    
    /**
     * Computing styles can be expensive, e.g. we have to calculate the styles
     * of all parents of an element. We therefore cache styles in a map of
     * element => WeakReference(styles). A weak hash map is used to avoid
     * leaking memory as elements are deleted. By using weak references to
     * the values, we also ensure the cache is memory-sensitive.
     * 
     * This must be transient to prevent it from being serialized, as
     * WeakHashMaps are not serializable.
     */
	private transient Map<IVexElement, Styles> styleMap = null;
    
    /**
     * Class constructor.
     *
     * @param rules Rules that constitute the style sheet.
     */
    public StyleSheet(Rule[] rules) {
	this.rules = rules;
	
	
    }

    /**
     * Flush any cached styles for the given element.
     * @param element Element for which styles are to be flushed.
     */
    public void flushStyles(IVexElement element) {    	
        this.getStyleMap().remove(element);
    }
    
    /**
     * Removes <em>all</em> elements from the document cache, causing them to be recreated.
     * Call this if you discard your document.
     */
    public void clearDocumentCache() {
		if (styleMap != null)
			styleMap.clear();
	}

	/**
     * Returns a pseudo-element representing content to be displayed
     * after the given element, or null if there is no such content.
     * @param element Parent element of the pseudo-element.
     */
    public IVexElement getAfterElement(IVexElement element) {
        PseudoElement pe = new PseudoElement(element, PseudoElement.AFTER);
        Styles styles = this.getStyles(pe);
        if (styles == null) {
            return null;    
        } else {
            return pe;
        }
    }
    
    /**
     * Returns a pseudo-element representing content to be displayed
     * before the given element, or null if there is no such content.
     * @param element Parent element of the pseudo-element.
     */
    public IVexElement getBeforeElement(IVexElement element) {
        PseudoElement pe = new PseudoElement(element, PseudoElement.BEFORE);
        Styles styles = this.getStyles(pe);
        if (styles == null) {
            return null;    
        } else {        
            return pe;
        }
    }
    
    
    
    /**
     * Returns a pseudo-element representing the first letter of the sentence 
     */
    public IVexElement getFirstLetterElement(IVexElement element) {
    	//PseudoElement pf = null;
//    	if(element.getName().equals("p")){
		PseudoElement pf = new PseudoElement(element, PseudoElement.First_Letter);

		Styles styles = this.getStyles(pf);

		// System.out.println("this: "+styles);

		if (styles == null) {
            return null;    
		} else {
			// System.out.println(styles.getDisplay());
			return pf;
		}
        
//    	}
//    	return pf;
    	
    }
    
    
    /**
     * Returns a pseudo-element representing the first line of the paragraph 
     */
    public IVexElement getFirstLineElement(IVexElement element) {
		PseudoElement pf = new PseudoElement(element, PseudoElement.First_Line);
		Styles styles = this.getStyles(pf);
		if (styles == null) {
            return null;    
		} else {
			return pf;
		}
    }
    
//    /*
//     * setting counter flag
//     * 
//     */
//    public void setCounterFlag(boolean flag){    	
//    	this.counterflag = flag;
//    }
//    /*
//     * getting counter flag
//     * 
//     */
//    public boolean getCounterFlag(){    	
//    	return this.counterflag;
//    }
    
    
    /**
     * Returns the array of standard CSS properties.
     */
    public static IProperty[] getCssProperties() {
        return CSS_PROPERTIES;
    }

    /**
     * Returns the styles for the given element. The styles are cached to
     * ensure reasonable performance.
     *
     * @param element Element for which to calculate the styles.
     */
    public Styles getStyles(IVexElement element) {
    	
    	
		Styles styles = getStyleMap().get(element);
        
        if (styles != null) {
                return styles;
		} else if (this.getStyleMap().containsKey(element)) {
			// this must be a pseudo-element with no content
//            return null;
		}
        
        if(element.getName().equals("first-letter") || element.getName().equals("first-line")){
        	styles = calculateFirst_Letter_Line_Styles(element);
        }
        else{        	
           styles = calculateStyles(element);
        
        }
        if (styles == null) {   

            // Yes, they can be null if element is a PseudoElement with no content property
            this.getStyleMap().put(element, null);            
            
        } else {  
			this.getStyleMap().put(element, styles);
            
        }

        return styles;
    }

    
    private Styles calculateFirst_Letter_Line_Styles(IVexElement element) {
    	
    	 Styles styles = new Styles();
         Styles parentStyles = null;
         if (element.getParent() != null) {         	
         	parentStyles = this.getStyles(element.getParent());        	
         }
        
 	     Map<String, LexicalUnit> decls = this.getApplicableDecls(element);	

		if (decls.isEmpty())
			return null;

 	     LexicalUnit lu;
 	 	   
 	    for (int i = 0; i < properties.length; i++) {
			IProperty property = properties[i];
			lu = decls.get(property.getName());
			Object value = property.calculate(lu, parentStyles, styles);
			styles.put(property.getName(), value);
		
		}		
		int styleFlags = FontSpec.PLAIN;
		String fontStyle = styles.getFontStyle();
		if (fontStyle.equalsIgnoreCase(CSS.ITALIC) || fontStyle.equalsIgnoreCase(CSS.OBLIQUE)) {
			styleFlags |= FontSpec.ITALIC;
		}
		if (styles.getFontWeight() > 550) {
			// 550 is halfway btn normal (400) and bold (700)
			styleFlags |= FontSpec.BOLD;
		}
		String textDecoration = styles.getTextDecoration();
		if (textDecoration.equalsIgnoreCase(CSS.UNDERLINE)) {
			styleFlags |= FontSpec.UNDERLINE;
		} else if (textDecoration.equalsIgnoreCase(CSS.OVERLINE)) {
			styleFlags |= FontSpec.OVERLINE;
		} else if (textDecoration.equalsIgnoreCase(CSS.LINE_THROUGH)) {
			styleFlags |= FontSpec.LINE_THROUGH;
		}

		styles.setFont(new FontSpec(styles.getFontFamilies(), styleFlags, Math.round(styles.getFontSize())));
    	
    	return styles;
    }
    
    
    
	
    private Styles calculateStyles(IVexElement element) {
      	    	
        Styles styles = new Styles();
        Styles parentStyles = null;
        if (element.getParent() != null) {
        	
        	parentStyles = this.getStyles(element.getParent());        	
        }
       
	Map<String, LexicalUnit> decls = this.getApplicableDecls(element);	
	LexicalUnit lu;
	
	
		
        // If we're finding a pseudo-element, look at the 'content' property
        // first, since most of the time it'll be empty and we'll return null.     

              
        List<String> content = new ArrayList<String>();
		if (element instanceof PseudoElement) {
					
			if(element.getName().equals("before")){
				generateCounterBefore(element);
				  
		    }
	
			lu = decls.get(CSS.CONTENT);

			if (lu == null) {
				return null;
			}
			
			String myStringValue = "";
			
			// XXX this is TEI specific
			while (lu != null) {

				if (lu.getLexicalUnitType() == LexicalUnit.SAC_COUNTER_FUNCTION) {
					
					String val = lu.getParameters().getStringValue();						

					if(val.equalsIgnoreCase("div1")){
						if(element.getParent().getParent().getName().equalsIgnoreCase("div")){	
							   //System.out.println("stylesheet: "+mc_div);
							   content.add(Integer.toString(mc_div));
						}
						else{
					      content.add(Integer.toString(mc1));
						}  
					}
					else if(val.equalsIgnoreCase("div2")){						   
						   content.add(Integer.toString(mc2));
					}
					else if(val.equalsIgnoreCase("div3")){						  
						   content.add(Integer.toString(mc3));
					}


				} else if (lu.getLexicalUnitType() == LexicalUnit.SAC_ATTR) {
					String attributeName = lu.getStringValue();
					String value = element.getParent().getAttribute(attributeName);
					if (value != null)
						content.add(value);
				}


				if (lu.getLexicalUnitType() == LexicalUnit.SAC_STRING_VALUE) { 
					myStringValue = lu.getStringValue();
					content.add(myStringValue);
				}

				String re = lu.getStringValue();
				if (re != null) {
					if (re.equalsIgnoreCase("open-quote") || re.equalsIgnoreCase("close-quote")) {
						content.add("\"");
					}
				}				
				lu = lu.getNextLexicalUnit();
			}
			
			styles.setContent(content);
		}
		

		for (int i = 0; i < properties.length; i++) {
			IProperty property = properties[i];
			lu = decls.get(property.getName());
			
			Object value = property.calculate(lu, parentStyles, styles);
			
			if(element.equals(element.getDocument().getRootElement())){
				if(property.getName().equals("display") && value.equals("inline")){
					styles.put(property.getName(), "block");
				}
				else{
					styles.put(property.getName(), value);	
				}
			}
			else{
				styles.put(property.getName(), value);
			}
			
		}
		// TG-1025 convert inline elements that contain block elements to block elements
		if (parentStyles != null && styles.isBlock() && !parentStyles.isBlock()) {
			IVexElement ancestor = element.getParent();
			do {
				Styles ancestorStyles = getStyles(ancestor);
				if (ancestorStyles.isBlock())
					break;
				ancestorStyles.put(CSS.DISPLAY, CSS.BLOCK);
				reportDisplayFix(ancestor);
				ancestor = ancestor.getParent();
			} while (ancestor != null);
		}
		if (parentStyles == null)
			styles.put(CSS.DISPLAY, CSS.BLOCK);

		// Now, map font-family, font-style, font-weight, and font-size onto
		// an AWT font.

		int styleFlags = FontSpec.PLAIN;
		String fontStyle = styles.getFontStyle();
		if (fontStyle.equalsIgnoreCase(CSS.ITALIC) || fontStyle.equalsIgnoreCase(CSS.OBLIQUE)) {
			styleFlags |= FontSpec.ITALIC;
		}
		if (styles.getFontWeight() > 550) {
			// 550 is halfway btn normal (400) and bold (700)
			styleFlags |= FontSpec.BOLD;
		}
		String textDecoration = styles.getTextDecoration();
		if (textDecoration.equalsIgnoreCase(CSS.UNDERLINE)) {
			styleFlags |= FontSpec.UNDERLINE;
		} else if (textDecoration.equalsIgnoreCase(CSS.OVERLINE)) {
			styleFlags |= FontSpec.OVERLINE;
		} else if (textDecoration.equalsIgnoreCase(CSS.LINE_THROUGH)) {
			styleFlags |= FontSpec.LINE_THROUGH;
		}

		styles.setFont(new FontSpec(styles.getFontFamilies(), styleFlags, Math.round(styles.getFontSize())));
		
		return styles;
	}
    
    
 
    private Set<IVexElement> elementsWithDisplayFix = Sets.newHashSet();
    private void reportDisplayFix(final IVexElement ancestor) {
		elementsWithDisplayFix.add(ancestor);
		// System.err.println("Applied display:block fix for element " +
		// ancestor);
	}

	/**
     * Returns the list of properties to be parsed by StyleSheets in this app.
     */
    public static IProperty[] getProperties() {
        return StyleSheet.properties;
    }
    
    /**
     * Returns the rules comprising this stylesheet.
     */
    public Rule[] getRules() {
        return this.rules;
    }


    /**
     * Sets the list of properties to be used by StyleSheets in this application.
     * @param properties New array of IProperty objects to be used.
     */
    public static void setProperties(IProperty[] properties) {
        StyleSheet.properties = properties;
    }
    
    //========================================================= PRIVATE
    
    
    /**
     * Returns all the declarations that apply to the given element.
     */
    public Map<String, LexicalUnit> getApplicableDecls(IVexElement element) {
   		
    		    	
	// Find all the property declarations that apply to this element.
	List<PropertyDecl> declList = new ArrayList<PropertyDecl>();
	Rule[] rules = this.getRules();
	
		
	for (int i = 0; i < rules.length; i++) {
	    Rule rule = rules[i];	    
	   
	    if (rule.matches(element)) { 
	    	
	    	PropertyDecl[] ruleDecls = rule.getPropertyDecls();
	    	for (int j = 0; j < ruleDecls.length; j++) {			
	    		declList.add(ruleDecls[j]);
	    	}
	    }
	}

	// Sort in cascade order. We can then just stuff them into a
	// map and get the right values since higher-priority values
	// come later and overwrite lower-priority ones.
	Collections.sort(declList);

	Map<String, PropertyDecl> decls = new HashMap<String, PropertyDecl>();
	Iterator<PropertyDecl> iter = declList.iterator();
	while (iter.hasNext()) {
	    PropertyDecl decl = iter.next();
	    PropertyDecl prevDecl = decls.get(decl.getProperty());
	    if (prevDecl == null || !prevDecl.isImportant() || decl.isImportant()) {
	        decls.put(decl.getProperty(), decl);
	    }
	}

	Map<String, LexicalUnit> values = new HashMap<String, LexicalUnit>();
	for (Iterator<String> it = decls.keySet().iterator(); it.hasNext();) {
	    PropertyDecl decl = decls.get(it.next());
	    values.put(decl.getProperty(), decl.getValue());	   
	}

	return values;
    }


	private Map<IVexElement, Styles> getStyleMap() {
        if (this.styleMap == null) {
			this.styleMap = Collections.synchronizedMap(new WeakHashMap<IVexElement, Styles>());
        }
        return this.styleMap;
    }
}


