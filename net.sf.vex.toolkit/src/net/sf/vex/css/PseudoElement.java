/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2004 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.css;

import net.sf.vex.dom.IVexElement;
import net.sf.vex.dom.impl.Element;

/**
 * Represents a :before or :after or: first-letter or first-line pseudo-element.
 * 
 * FIXME: What do we do with <em>this</em> when we replace net.sf.vex.impl.*?
 */
public class PseudoElement extends Element {
    
    public static final String AFTER = "after";
    public static final String BEFORE = "before";
    
    
    public static final String First_Letter = "first-letter";
    public static final String First_Line = "first-line";
    

    /**
     * Class constructor.
     * @param parent Parent element to this pseudo-element.
     * @param name Name of this pseudo-element, e.g. PseudoElement.BEFORE.
     */    
    public PseudoElement(IVexElement parent, String name) {
        super(name);
        this.setParent(parent);
    }
    
    /**
     * @see java.lang.Object#equals(java.lang.Object)
     */
    public boolean equals(Object o) {
        if (o == null || o.getClass() != this.getClass()) {
            return false;
        }
        PseudoElement other = (PseudoElement) o;
        return this.getParent() == other.getParent()
            && this.getName().equals(other.getName());
    }
    
    /**
     * @see java.lang.Object#hashCode()
     */
    public int hashCode() {
        return this.getParent().hashCode() + this.getName().hashCode();
    }
}
