/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2005 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.css;

import java.util.ArrayList;
import java.util.List;

import org.w3c.css.sac.LexicalUnit;

/**
 * The CSS 'font-family' property.
 */
public class FontFamilyProperty extends AbstractProperty {

    /**
     * Class constructor.
     */
    public FontFamilyProperty() {
        super(CSS.FONT_FAMILY);
    }

    /**
     *
     */

    public Object calculate(LexicalUnit lu, Styles parentStyles,
            Styles styles) {
        if (isFontFamily(lu)) {
            return getFontFamilies(lu);
        } else {
            // not specified, "inherit", or some other value
            if (parentStyles != null) {
                return parentStyles.getFontFamilies();
            } else {
                return DEFAULT_FONT_FAMILY;
            }
        }
    }

    //================================================= PRIVATE
    
    private static final String[] DEFAULT_FONT_FAMILY = new String[] { "sans-serif" };

    private static boolean isFontFamily(LexicalUnit lu) {
        return lu != null 
        && (lu.getLexicalUnitType() == LexicalUnit.SAC_STRING_VALUE
                || lu.getLexicalUnitType() == LexicalUnit.SAC_IDENT);
    }

    private static String[] getFontFamilies(LexicalUnit lu) {
        List list = new ArrayList();
        while (lu != null) {
            if (lu.getLexicalUnitType() == LexicalUnit.SAC_STRING_VALUE
                    || lu.getLexicalUnitType() == LexicalUnit.SAC_IDENT) {
                
                list.add(lu.getStringValue());
            }
            lu = lu.getNextLexicalUnit();
        }
        return (String[]) list.toArray(new String[list.size()]);
    }

}
