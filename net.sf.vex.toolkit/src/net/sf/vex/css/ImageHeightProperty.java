/* 
 * created on 28.7.2010 
 * @author: Mohamadou Nassourou
 */
package net.sf.vex.css;

import net.sf.vex.core.DisplayDevice;

import org.w3c.css.sac.LexicalUnit;

/**
 * The CSS height property.
 */
public class ImageHeightProperty extends AbstractProperty {

    private static final float Image_Height_NORMAL = 50.0f;

    /**
     * Class constructor.
     */
    public ImageHeightProperty() {
        super(CSS.HEIGHT);
    }

    /**
     * Calculates the value of the property given a LexicalUnit. Returns
     * the value.
     */
    public Object calculate(LexicalUnit lu, Styles parentStyles, Styles styles) {
    	
    	if(lu!=null){
    		Float fObj = new Float(Math.max(Image_Height_NORMAL, lu.getFloatValue()));
    	    //System.out.println("inside imageheight: "+fObj);
            return fObj;
    	}
    	
    	return Image_Height_NORMAL; 
       
       
    }

}
