package net.sf.vex.css;


/*
 * class responsible for implementing counters, first-line, first-letter properties
 * 
 * @author Mohamadou Nassourou <mohamadou.nassourou@uni-wuerzburg.de> for TextGrid
 * 
 * 
 */



import java.util.List;

import net.sf.vex.dom.IVexElement;



public abstract class GeneratedContentPropertiesImpl{

	    public int mc1=0;
		public int mc2=0;
		public int mc3=0;
		public int mc_div=0;
		
		public int Cfirst_letter=0;
		public int Cfirst_line=0;
		
		public boolean counterflag=true;
		
		//public  List<String> content_dummy = new ArrayList<String>();
		
		
	public GeneratedContentPropertiesImpl() {
		
	}
	
	  /*
     * implements counter() 
     */
    
    public void generateCounterBefore(IVexElement element){
    	
		if(element.getParent().getName().equalsIgnoreCase("body")){
			mc1=0;
			mc2=0;
			mc3=0;
			mc_div=0;
			
		}
		else if(element.getParent().getName().equalsIgnoreCase("div1")){
			mc1++;	
			mc2=0;
			mc3=0;
			
		}
		else if(element.getParent().getName().equalsIgnoreCase("div2")){
			mc2++;
			mc3=0;
		}
		else if(element.getParent().getName().equalsIgnoreCase("div3")){
			mc3++;					   
		}
		else if(element.getParent().getName().equalsIgnoreCase("div")){			
			mc_div++;
		}
	
    	
   }
    
    /*
     * implements first-letter 
     */
    
    public String generateFirstLetter(IVexElement element){
    
    	String letter ="";
    	if(element.getParent().getName().equalsIgnoreCase("p")){
    		if(element.getParent().getChildNodes().length > 0){
    			letter = element.getParent().getChildNodes()[0].getText().substring(0,1);
    			//System.out.println("item: "+letter);
        		//content_dummy.add(letter);
    			
//    			IVexElement child = element.clone();
//				Content content = element.getParent().getChildNodes()[0].getContent();
//				
//				System.out.println(content.getString(0, content.getLength()));
//				
//				content.insertString(element.getStartOffset(), letter);
//				int startOffset = element.getParent().getChildNodes()[0].getStartOffset();
//				int endOffset = element.getParent().getChildNodes()[0].getEndOffset();
//				child.setContent(content, startOffset, endOffset);
//				//element.getParent().addChild(child);
//				
//				element = child.clone();
    		}
    		
    		
    	}
    return letter;
    
    }	
    	
    
	
}
