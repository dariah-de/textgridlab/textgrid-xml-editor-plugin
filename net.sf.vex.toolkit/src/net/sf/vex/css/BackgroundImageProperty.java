/* 
 * created on 28.7.2010 
 * @author: Mohamadou Nassourou
 */
package net.sf.vex.css;

import org.w3c.css.sac.LexicalUnit;

/**
 * The CSS background-image property.
 * 
 * @author Mohamadou Nassourou 
 */
public class BackgroundImageProperty extends AbstractProperty {

    private static final String bImage = "";

    /**
     * Class constructor.
     */
    public BackgroundImageProperty() {
        super(CSS.BACKGROUND_IMAGE);
    }

    /**
     * Calculates the value of the property given a LexicalUnit. Returns
     * the value.
     */
    public Object calculate(LexicalUnit lu, Styles parentStyles, Styles styles) {
    	
    	if(lu!=null){
    		if (lu.getLexicalUnitType() == LexicalUnit.SAC_ATTR) {	
    			String sObj = new String(lu.getStringValue());
    			return sObj;
    		} 
    	}
    	
    	return bImage; 
       
       
    }

}
