/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2005 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.css;

import net.sf.vex.core.DisplayDevice;

import org.w3c.css.sac.LexicalUnit;

/**
 * The border-XXX-width CSS property. Since the value of this property
 * depends on the corresponding border-XXX-style property, the style
 * property must be calculated first and placed in the styles, and its
 * name given to the constructor of this class.
 */
public class BorderWidthProperty extends AbstractProperty {


    /**
     * Class constructor.
     * @param name Name of the property.
     * @param borderStyleName Name of the corresponding border style
     * property. For example, if name is CSS.BORDER_TOP_WIDTH, then
     * borderStyleName should be CSS.BORDER_TOP_STYLE.
     * @param axis AXIS_HORIZONTAL (for left and right borders) or 
     * AXIS_VERTICAL (for top and bottom borders). 
     */
    public BorderWidthProperty(String name, String borderStyleName, byte axis) {
        super(name);
        this.borderStyleName = borderStyleName;
        this.axis = axis;
    }

    /**
     * Returns true if the given lexical unit represents a border width.
     *
     * @param lu LexicalUnit to check.
     */
    public static boolean isBorderWidth(LexicalUnit lu) {
        if (lu == null) {
            return false;
        } else if (isLength(lu)) {
            return true;
        } else if (lu.getLexicalUnitType() == LexicalUnit.SAC_IDENT) {
            String s = lu.getStringValue();
            return s.equalsIgnoreCase(CSS.THIN)
            || s.equalsIgnoreCase(CSS.MEDIUM)
            || s.equalsIgnoreCase(CSS.THICK);
        } else {
            return false;
        }
    }
    
    public Object calculate(LexicalUnit lu, Styles parentStyles, Styles styles) {
        return new Integer(this.calculateInternal(lu, parentStyles, styles));
    }

    private int calculateInternal(LexicalUnit lu, Styles parentStyles, Styles styles) {
        
        DisplayDevice device = DisplayDevice.getCurrent();
        int ppi = this.axis == AXIS_HORIZONTAL ? device.getHorizontalPPI() : device.getVerticalPPI();
        
        String borderStyle = (String) styles.get(this.borderStyleName);
        
        if (borderStyle.equalsIgnoreCase(CSS.NONE) || borderStyle.equals(CSS.HIDDEN)) {
            return 0;
        } else if (isBorderWidth(lu)) {
            return getBorderWidth(lu, styles.getFontSize(), ppi);
        } else if (isInherit(lu) && parentStyles != null) {
            return ((Integer) parentStyles.get(this.getName())).intValue();
        } else {
            // not specified, "none", or other unknown value
            return BORDER_WIDTH_MEDIUM;
        }
    }

    //=================================================== PRIVATE

    // Name of the corresponding border style property
    private String borderStyleName;

    // Axis along which the border width is measured.
    private byte axis;

    // named border widths
    private static final int BORDER_WIDTH_THIN = 1;
    private static final int BORDER_WIDTH_MEDIUM = 3;
    private static final int BORDER_WIDTH_THICK = 5;


    private static int getBorderWidth(LexicalUnit lu, float fontSize, int ppi) {
        if (isLength(lu)) {
            return getIntLength(lu, fontSize, ppi);
        } else if (lu.getLexicalUnitType() == LexicalUnit.SAC_IDENT) {
            String s = lu.getStringValue();
            if (s.equalsIgnoreCase(CSS.THIN)) {
                return BORDER_WIDTH_THIN;
            } else if (s.equalsIgnoreCase(CSS.MEDIUM)) {
                return BORDER_WIDTH_MEDIUM;
            } else if (s.equalsIgnoreCase(CSS.THICK)) {
                return BORDER_WIDTH_THICK;
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }
    
    
}
