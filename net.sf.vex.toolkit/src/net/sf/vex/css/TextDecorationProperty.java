/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2005 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.css;

import org.w3c.css.sac.LexicalUnit;

/**
 * The CSS text-decoration property.
 */
public class TextDecorationProperty extends AbstractProperty {

    /**
     * Class constructor.
     */
    public TextDecorationProperty() {
        super(CSS.TEXT_DECORATION);
    }

    public Object calculate(LexicalUnit lu, Styles parentStyles, Styles styles) {
        if (isTextDecoration(lu)) {
            return lu.getStringValue();
        } else {
            // not specified, "inherit", or some other value
            if (parentStyles != null) {
                return parentStyles.getTextDecoration();
            } else {
                return CSS.NONE;
            }
        }
    }

    //=================================================== PRIVATE
    
    /**
     * Returns true if the given lexical unit represents a text decoration.
     *
     * @param lu LexicalUnit to check.
     */
    private static boolean isTextDecoration(LexicalUnit lu) {
        if (lu == null) {
            return false;
        } else if (lu.getLexicalUnitType() == LexicalUnit.SAC_IDENT) {
            String s = lu.getStringValue();
            return s.equalsIgnoreCase(CSS.NONE)
                || s.equalsIgnoreCase(CSS.UNDERLINE)
                || s.equalsIgnoreCase(CSS.OVERLINE)
                || s.equalsIgnoreCase(CSS.LINE_THROUGH)
                || s.equalsIgnoreCase(CSS.BLINK);
        } else {
            return false;
        }
    }

}
