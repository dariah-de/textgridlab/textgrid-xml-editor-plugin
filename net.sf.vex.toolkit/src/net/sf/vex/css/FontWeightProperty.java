/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2005 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.css;

import org.w3c.css.sac.LexicalUnit;

/**
 * The CSS font-weight property.
 */
public class FontWeightProperty extends AbstractProperty {

    private static final int FONT_WEIGHT_NORMAL = 400;
    private static final int FONT_WEIGHT_BOLD = 700;

    /**
     * Class constructor.
     */
    public FontWeightProperty() {
        super(CSS.FONT_WEIGHT);
    }

    public Object calculate(LexicalUnit lu, Styles parentStyles, Styles styles) {
        return new Integer(this.calculateInternal(lu, parentStyles, styles));
    }

    public int calculateInternal(LexicalUnit lu, Styles parentStyles, Styles styles) {
        if (isFontWeight(lu)) {
            return getFontWeight(lu, parentStyles);
        } else {
            // not specified, "inherit", or some other value
            if (parentStyles != null) {
                return parentStyles.getFontWeight();
            } else {
                return FONT_WEIGHT_NORMAL;
            }
        }
        
    }

    /**
     * Returns true if the given lexical unit represents a font weight.
     *
     * @param lu LexicalUnit to check.
     */
    public static boolean isFontWeight(LexicalUnit lu) {
    if (lu == null) {
        return false;
    } else if (lu.getLexicalUnitType() == LexicalUnit.SAC_INTEGER) {
        return true;
    } else if (lu.getLexicalUnitType() == LexicalUnit.SAC_IDENT) {
        String s = lu.getStringValue();
        return s.equalsIgnoreCase(CSS.NORMAL)
    	|| s.equalsIgnoreCase(CSS.BOLD)
    	|| s.equalsIgnoreCase(CSS.BOLDER)
    	|| s.equalsIgnoreCase(CSS.LIGHTER);
    } else {
        return false;
    }
    }

    private static int getFontWeight(LexicalUnit lu, Styles parentStyles) {
        if (lu == null) {
            return FONT_WEIGHT_NORMAL;
        } else if (lu.getLexicalUnitType() == LexicalUnit.SAC_INTEGER) {
            return lu.getIntegerValue();
        } else if (lu.getLexicalUnitType() == LexicalUnit.SAC_IDENT) {
            String s = lu.getStringValue();
            if (s.equalsIgnoreCase(CSS.NORMAL)) {
                return FONT_WEIGHT_NORMAL;
            } else if (s.equalsIgnoreCase(CSS.BOLD)) {
                return FONT_WEIGHT_BOLD;
            } else if (s.equalsIgnoreCase(CSS.BOLDER)) {
                if (parentStyles != null) {
                    return parentStyles.getFontWeight() + 151;
                } else {
                    return FONT_WEIGHT_BOLD;
                }
            } else if (s.equalsIgnoreCase(CSS.LIGHTER)) {
                if (parentStyles != null) {
                    return parentStyles.getFontWeight() - 151;
                } else {
                    return FONT_WEIGHT_NORMAL;
                }
            } else {
                return FONT_WEIGHT_NORMAL;
            }
        } else {
            return FONT_WEIGHT_NORMAL;
        }
    }
    
}
