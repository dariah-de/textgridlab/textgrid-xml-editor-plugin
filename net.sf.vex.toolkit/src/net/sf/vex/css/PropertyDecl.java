/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2004 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.css;

import java.io.Serializable;

import org.w3c.css.sac.LexicalUnit;

/**
 * Represents a particular CSS property declaration.
 */
public class PropertyDecl implements Comparable, Serializable {

    private static final long serialVersionUID = 1L;
    
    public static final byte SOURCE_DEFAULT = 0;
    public static final byte SOURCE_AUTHOR = 1;
    public static final byte SOURCE_USER = 2;

    private Rule rule;
    private String property;
    private LexicalUnit value;
    private boolean important;

    /**
     * Class constructor.
     */
    public PropertyDecl(Rule rule,
			String property,
			LexicalUnit value,
			boolean important) {
	this.rule = rule;
	this.property = property;
	this.value = value;
	this.important = important;
    }

    /**
     * Implementation of <code>Comparable.compareTo(Object)</code>
     * that implements CSS cascade ordering.
     */
    public int compareTo(Object o) {
	PropertyDecl other = (PropertyDecl) o;
	int thisWeight = this.getWeight();
	int otherWeight = other.getWeight();
	if (thisWeight != otherWeight) {
	    return thisWeight - otherWeight;
	}

	int thisSpec = this.getRule().getSpecificity();
	int otherSpec = other.getRule().getSpecificity();

	return thisSpec - otherSpec;
    }

    /**
     * Return the value of the <code>important</code> property.
     */
    public boolean isImportant() {
        return this.important;
    }

    /**
     * Return the value of the <code>property</code> property.
     */
    public String getProperty() {
        return this.property;
    }

    /**
     * Return the value of the <code>rule</code> property.
     */
    public Rule getRule() {
        return this.rule;
    }

    /**
     * Return the value of the <code>value</code> property.
     */
    public LexicalUnit getValue() {
    	
    	//if(value.getParameters() !=null)
    	//System.out.println(this.value.getFunctionName() + " : "+this.value.getParameters().getStringValue());
    	
        return this.value;
    }


    //===================================================== PRIVATE

    /**
     * Returns the weight of this declaration, as follows...
     *
     * <pre>
     * 4 => user stylesheet, important decl
     * 3 => author stylesheet, important decl
     * 2 => author stylesheet, not important
     * 1 => user stylesheet, not important
     * 0 => default stylesheet
     * </pre>
     */
    private int getWeight() {
        int source = this.getRule().getSource();
        if (this.isImportant() && source == StyleSheet.SOURCE_USER) {
            return 4;
        } else if (this.isImportant() && source == StyleSheet.SOURCE_AUTHOR) {
            return 3;
        } else if (!this.isImportant() && source == StyleSheet.SOURCE_AUTHOR) {
            return 2;
        } else if (!this.isImportant() && source == StyleSheet.SOURCE_USER) {
            return 1;
        } else {
            return 0;
        }
    }
}

