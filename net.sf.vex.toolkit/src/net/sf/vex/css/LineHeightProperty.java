/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2005 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.css;

import net.sf.vex.core.DisplayDevice;

import org.w3c.css.sac.LexicalUnit;

/**
 * The CSS line-height property.
 */
public class LineHeightProperty extends AbstractProperty {

    private static final float LINE_HEIGHT_NORMAL = 1.2f;

    /**
     * Class constructor.
     */
    public LineHeightProperty() {
        super(CSS.LINE_HEIGHT);
    }

    /**
     * Calculates the value of the property given a LexicalUnit. Returns
     * a RelativeLength that is relative to the current font size.
     */
    public Object calculate(LexicalUnit lu, Styles parentStyles, Styles styles) {
        
        int ppi = DisplayDevice.getCurrent().getVerticalPPI();
        
        if (isLength(lu)) {
            return RelativeLength.createAbsolute(Math.round(getIntLength(lu, styles.getFontSize(), ppi) / styles.getFontSize()));
        } else if (isNumber(lu)) {
            if (getNumber(lu) <= 0) {
                return RelativeLength.createRelative(LINE_HEIGHT_NORMAL);
            } else {
                return RelativeLength.createRelative(getNumber(lu));
            }
        } else if (isPercentage(lu)) {
            if (lu.getFloatValue() <= 0) {
                return RelativeLength.createRelative(LINE_HEIGHT_NORMAL);
            } else {
                return RelativeLength.createRelative(lu.getFloatValue() / 100);
            }
        } else {
            // not specified, "inherit", or other unknown value
            if (parentStyles == null) {
                return RelativeLength.createRelative(LINE_HEIGHT_NORMAL);
            } else {
                return (RelativeLength) parentStyles.get(CSS.LINE_HEIGHT);
            }
        }
    }

}
