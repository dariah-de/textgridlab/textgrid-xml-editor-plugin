/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2005 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.css;

import org.w3c.css.sac.LexicalUnit;

/**
 * The border-XXX-style CSS property.
 */
public class BorderStyleProperty extends AbstractProperty {

    /**
     * Class constructor.
     * @param name Name of the property.
     */
    public BorderStyleProperty(String name) {
        super(name);
    }

    /**
     * Returns true if the given lexical unit represents a border style.
     *
     * @param lu LexicalUnit to check.
     */
    public static boolean isBorderStyle(LexicalUnit lu) {
        if (lu == null) {
            return false;
        } else if (lu.getLexicalUnitType() == LexicalUnit.SAC_IDENT) {
            String s = lu.getStringValue();
            return s.equalsIgnoreCase(CSS.NONE)
            || s.equalsIgnoreCase(CSS.HIDDEN)
            || s.equalsIgnoreCase(CSS.DOTTED)
            || s.equalsIgnoreCase(CSS.DASHED)
            || s.equalsIgnoreCase(CSS.SOLID)
            || s.equalsIgnoreCase(CSS.DOUBLE)
            || s.equalsIgnoreCase(CSS.GROOVE)
            || s.equalsIgnoreCase(CSS.RIDGE)
            || s.equalsIgnoreCase(CSS.INSET)
            || s.equalsIgnoreCase(CSS.OUTSET);
        }
        
        return false;
    }

    public Object calculate(LexicalUnit lu, Styles parentStyles, Styles styles) {
        if (isBorderStyle(lu)) {
            return lu.getStringValue();
        } else if (isInherit(lu) && parentStyles != null) {
            return parentStyles.get(this.getName());
        } else {
            return CSS.NONE;
        }
    }

}
