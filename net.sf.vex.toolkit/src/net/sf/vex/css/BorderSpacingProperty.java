/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2005 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.css;

import net.sf.vex.core.DisplayDevice;

import org.w3c.css.sac.LexicalUnit;

/**
 * The CSS 'border-spacing' property.
 */
public class BorderSpacingProperty extends AbstractProperty {

    /**
     * Represents the computed value of border-spacing, which is a pair
     * of values representing vertical and horizontal spacing.
     */
    public static class Value {

        private int horizontal;
        private int vertical;

        public static final Value ZERO = new Value(0, 0);
        
        public Value(int horizontal, int vertical) {
            this.horizontal = horizontal;
            this.vertical = vertical;
        }
        
        /**
         * Returns the horizontal spacing, in pixels.
         */
        public int getHorizontal() {
            return this.horizontal;
        }
        
        /**
         * Returns the vertical spacing, in pixels.
         */
        public int getVertical() {
            return this.vertical;
        }
    }
    
    /**
     * Class constructor.
     */
    public BorderSpacingProperty() {
        super(CSS.BORDER_SPACING);
    }

    public Object calculate(LexicalUnit lu, Styles parentStyles, Styles styles) {

        int horizontal = 0;
        int vertical = 0;
        
        DisplayDevice device = DisplayDevice.getCurrent();

        if (isLength(lu)) {
            horizontal = getIntLength(lu, styles.getFontSize(), device.getHorizontalPPI());
            lu = lu.getNextLexicalUnit();
            if (isLength(lu)) {
                vertical = getIntLength(lu, styles.getFontSize(), device.getVerticalPPI());
            } else {
                vertical = horizontal;
            }
            return new Value(horizontal, vertical);
        } else {
            // 'inherit' or an invalid value
            if (parentStyles == null) {
                return Value.ZERO;
            } else {
                return parentStyles.getBorderSpacing();
            }
        }
    }

}
