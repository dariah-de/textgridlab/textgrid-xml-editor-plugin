/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2004 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.css;

import java.io.Serializable;

/**
 * A length that may be expressed as an absolute or relative value.
 */
public class RelativeLength implements Serializable {

	private static final long serialVersionUID = 1L;

	private float percentage;

	private int absolute;

	boolean isAbsolute;

	private static RelativeLength ZERO = new RelativeLength(0, 0, true);

	/**
	 * Create a relative length representing an absolute value.
	 * 
	 * @return the new RelativeLength value.
	 */
	public static RelativeLength createAbsolute(int value) {
		if (value == 0) {
			return ZERO;
		} else {
			return new RelativeLength(0, value, true);
		}
	}

	/**
	 * Create a relative length representing a relative value.
	 * 
	 * @return the new RelativeLength value.
	 */
	public static RelativeLength createRelative(float percentage) {
		return new RelativeLength(percentage, 0, false);
	}

	/**
	 * Return the value of the length given a reference value. If this object
	 * represents an absolute value, that value is simply returned. Otherwise,
	 * returns the given reference length multiplied by the given percentage and
	 * rounded to the nearest integer.
	 * 
	 * @param referenceLength
	 *            reference length by which percentage lengths will by
	 *            multiplied.
	 * @return the actual value
	 */
	public int get(int referenceLength) {
		if (this.isAbsolute) {
			return this.absolute;
		} else {
			return Math.round(this.percentage * referenceLength);
		}
	}

	// ==================================================== PRIVATE

	private RelativeLength(float percentage, int absolute, boolean isAbsolute) {
		this.percentage = percentage;
		this.absolute = absolute;
		this.isAbsolute = isAbsolute;
	}
}
