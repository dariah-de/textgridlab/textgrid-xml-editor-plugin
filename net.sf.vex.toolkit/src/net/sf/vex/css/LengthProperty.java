/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2005 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.css;

import net.sf.vex.core.DisplayDevice;

import org.w3c.css.sac.LexicalUnit;

/**
 * A property that represents lengths, such as a margin or padding.
 */
public class LengthProperty extends AbstractProperty {

    public LengthProperty(String name, byte axis) {
        super(name);
        this.axis = axis;
    }

    public Object calculate(LexicalUnit lu, Styles parentStyles, Styles styles) {

        DisplayDevice device = DisplayDevice.getCurrent();
        int ppi = this.axis == AXIS_HORIZONTAL ? device.getHorizontalPPI() : device.getVerticalPPI();
        
        if (isLength(lu)) {
            int length = getIntLength(lu, styles.getFontSize(), ppi);
            return RelativeLength.createAbsolute(length);
        } else if (isPercentage(lu)) {
            return RelativeLength.createRelative(lu.getFloatValue() / 100);
        } else if (isInherit(lu) && parentStyles != null) {
            return parentStyles.get(this.getName());
        } else {
            // not specified, "auto", or other unknown value
            return RelativeLength.createAbsolute(0);
        }
    }


    //============================================================ PRIVATE

    private int axis;
}
