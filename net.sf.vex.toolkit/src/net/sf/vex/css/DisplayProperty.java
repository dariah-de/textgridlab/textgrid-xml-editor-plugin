/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2005 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.css;

import org.w3c.css.sac.LexicalUnit;

/**
 * The CSS 'display' property.
 */
public class DisplayProperty extends AbstractProperty {

    /**
     * Class constructor.
     */
    public DisplayProperty() {
        super(CSS.DISPLAY);
    }

    public Object calculate(LexicalUnit lu, Styles parentStyles,
            Styles styles) {
        
        if (isDisplay(lu)) {
            return lu.getStringValue();
        } else if (isInherit(lu) && parentStyles != null) {
            return parentStyles.getDisplay();
        } else {
            // not specified or other unknown value
            return CSS.INLINE;
        }
    }

    //======================================================== PRIVATE
    
    /**
     * Returns true if the value of the given LexicalUnit represents 
     * a valid value for this property.
     * @param lu LexicalUnit to inspect.
     */
    private static boolean isDisplay(LexicalUnit lu) {
        if (lu == null) {
            return false; 
        } else if (lu.getLexicalUnitType() == LexicalUnit.SAC_IDENT) {
            String s = lu.getStringValue();
            return s.equalsIgnoreCase(CSS.BLOCK)
            || s.equalsIgnoreCase(CSS.INLINE)
            || s.equalsIgnoreCase(CSS.INLINE_BLOCK)
            || s.equalsIgnoreCase(CSS.INLINE_TABLE)
            || s.equalsIgnoreCase(CSS.LIST_ITEM)
            || s.equalsIgnoreCase(CSS.NONE)
            || s.equalsIgnoreCase(CSS.RUN_IN)
            || s.equalsIgnoreCase(CSS.TABLE)
            || s.equalsIgnoreCase(CSS.TABLE_CAPTION)
            || s.equalsIgnoreCase(CSS.TABLE_CELL)
            || s.equalsIgnoreCase(CSS.TABLE_COLUMN)
            || s.equalsIgnoreCase(CSS.TABLE_COLUMN_GROUP)
            || s.equalsIgnoreCase(CSS.TABLE_FOOTER_GROUP)
            || s.equalsIgnoreCase(CSS.TABLE_HEADER_GROUP)
            || s.equalsIgnoreCase(CSS.TABLE_ROW)
            || s.equalsIgnoreCase(CSS.TABLE_ROW_GROUP);
        } else {
            return false;
        }
    }
    
}
