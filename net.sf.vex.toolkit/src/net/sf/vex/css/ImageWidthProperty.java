/* 
 * created on 28.7.2010 
 * @author: Mohamadou Nassourou
 */
package net.sf.vex.css;

import net.sf.vex.core.DisplayDevice;

import org.w3c.css.sac.LexicalUnit;

/**
 * The CSS width property.
 */
public class ImageWidthProperty extends AbstractProperty {

    private static final float Image_Width_NORMAL = 50.0f;

    /**
     * Class constructor.
     */
    public ImageWidthProperty() {
        super(CSS.WIDTH);
    }

    /**
     * Calculates the value of the property given a LexicalUnit. Returns
     * the value.
     */
    public Object calculate(LexicalUnit lu, Styles parentStyles, Styles styles) {
    	
    	if(lu!=null){
    		Float fObj = new Float(Math.max(Image_Width_NORMAL, lu.getFloatValue()));
    	    //System.out.println("inside imagewidth: "+fObj);
            return fObj;
    	}
    	
    	return Image_Width_NORMAL; 
       
       
    }

}
