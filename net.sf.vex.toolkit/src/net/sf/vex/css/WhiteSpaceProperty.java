/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2005 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.css;

import org.w3c.css.sac.LexicalUnit;

/**
 * The CSS white-space property.
 */
public class WhiteSpaceProperty extends AbstractProperty {

    /**
     * Class constructor.
     */
    public WhiteSpaceProperty() {
        super(CSS.WHITE_SPACE);
    }

    /**
     *
     */

    public Object calculate(LexicalUnit lu, Styles parentStyles, Styles styles) {
        if (isWhiteSpace(lu)) {
            return lu.getStringValue();
        } else {
            // not specified, "inherit", or some other value
            if (parentStyles != null) {
                return parentStyles.getWhiteSpace();
            } else {
                return CSS.NORMAL;
            }
        }
        
    }

    /**
     * Returns true if the given lexical unit represents a white space value.
     *
     * @param lu LexicalUnit to check.
     */
    public static boolean isWhiteSpace(LexicalUnit lu) {
    if (lu == null) {
        return false;
    } else if (lu.getLexicalUnitType() == LexicalUnit.SAC_IDENT) {
        String s = lu.getStringValue();
        return s.equalsIgnoreCase(CSS.NORMAL)
    	|| s.equalsIgnoreCase(CSS.PRE)
    	|| s.equalsIgnoreCase(CSS.NOWRAP);
    } else {
        return false;
    }
    }
    
}
