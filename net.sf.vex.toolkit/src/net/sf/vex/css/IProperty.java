/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2005 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.css;

import org.w3c.css.sac.LexicalUnit;

/**
 * Represents a CSS property.
 */
public interface IProperty {
    
    /** Constant indicating the length is along the horizontal axis. */
    public static final byte AXIS_HORIZONTAL = 0;

    /** Constant indicating the length is along the vertical axis. */
    public static final byte AXIS_VERTICAL = 1;


    /**
     * Returns the name of the property.
     */
    public String getName();

    /**
     * Calculates the value of a property given a LexicalUnit.
     * @param lu LexicalUnit to interpret.
     * @param parentStyles Styles of the parent element. These are used
     * when the property inherits a value.
     * @param styles Styles currently in effect. Often, the calculated
     * value depends on previously calculated styles such as font size 
     * and color.
     */
    public Object calculate(LexicalUnit lu, Styles parentStyles, Styles styles);

}
