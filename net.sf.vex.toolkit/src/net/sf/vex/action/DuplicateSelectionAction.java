/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2005 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.action;

import net.sf.vex.dom.IVexElement;
import net.sf.vex.widget.IVexWidget;

/**
 * Duplicates current selection or element.
 */
public class DuplicateSelectionAction extends AbstractVexAction {

    public void run(final IVexWidget vexWidget) {
        
        vexWidget.doWork(new Runnable() {
            public void run() {
                if (!vexWidget.hasSelection()) {
                    IVexElement element = vexWidget.getCurrentElement();
                    if (element.getParent() == null) {
                        // Can't dup the root element
                        return;
                    }
                    vexWidget.moveTo(element.getStartOffset());
                    vexWidget.moveTo(element.getEndOffset() + 1, true);
                }

                vexWidget.copySelection();
                int startOffset = vexWidget.getSelectionEnd(); 
                vexWidget.moveTo(startOffset);
                vexWidget.paste();
                int endOffset = vexWidget.getCaretOffset();
                vexWidget.moveTo(startOffset);
                vexWidget.moveTo(endOffset, true);
            }
        });
    }

}
