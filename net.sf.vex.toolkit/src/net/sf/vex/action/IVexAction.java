/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2005 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.action;

import net.sf.vex.widget.IVexWidget;

/**
 * Interface implemented by command objects that can act on a VexWidget.
 */
public interface IVexAction {
   
    /**
     * Performs the action on the VexWidget.
     * @param vexWidget IVexWidget on which the action is to be performed.
     */
    public void run(IVexWidget vexWidget);
    
    /**
     * Returns true if the action is valid for the given VexWidget.
     * @param vexWidget IVexWidget against which to test validity.
     */
    public boolean isEnabled(IVexWidget vexWidget);
}
