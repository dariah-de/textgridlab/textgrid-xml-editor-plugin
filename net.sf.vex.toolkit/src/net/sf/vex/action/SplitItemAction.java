/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2005 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.action;

import net.sf.vex.css.CSS;
import net.sf.vex.css.StyleSheet;
import net.sf.vex.dom.IVexElement;
import net.sf.vex.layout.Box;
import net.sf.vex.layout.TableRowBox;
import net.sf.vex.widget.IBoxFilter;
import net.sf.vex.widget.IVexWidget;

/**
 * Splits the nearest enclosing table row or list item. If a table row is being
 * split, empty versions of the current row's cells are created.
 */
public class SplitItemAction extends AbstractVexAction {

    public void run(IVexWidget vexWidget) {

        final StyleSheet ss = vexWidget.getStyleSheet();
        
        // Item is either a TableRowBox or a BlockElementBox representing
        // a list item
        Box item = vexWidget.findInnermostBox(new IBoxFilter() {
            public boolean matches(Box box) {
                if (box instanceof TableRowBox) {
                    return true;
                } else {
                    IVexElement element = box.getElement();
                    return element != null && ss.getStyles(element).getDisplay().equalsIgnoreCase(CSS.LIST_ITEM);
                }
            }
        });

        if (item instanceof TableRowBox) {
            insertRowBelowAction.run(vexWidget);
            //ActionUtils.duplicateTableRow(vexWidget, (TableRowBox) item);
        } else if (item != null) {
            SplitAction.splitElement(vexWidget, item.getElement());
        }
    }
    
    private static InsertRowBelowAction insertRowBelowAction = new InsertRowBelowAction();
    
}
