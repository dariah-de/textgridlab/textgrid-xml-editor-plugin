/*
 * class responsible for creating and opening favorite window for the VexEditor input
 * F+ and F- button images are added to the toolbar for adding and removing elements respectively
 * @author Mohamadou Nassourou <mohamadou.nassourou@uni-wuerzburg.de> for TextGrid
 * 
 * 
 */

package net.sf.vex.action;

import java.util.LinkedList;
import java.util.List;

import net.sf.vex.action.linked.AbstractModelQueryActionWrapper;
import net.sf.vex.action.linked.InsertAssistant;
import net.sf.vex.dom.IVexElement;
import net.sf.vex.dom.linked.LinkedElement;
import net.sf.vex.swt.VexWidget;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.ActionContributionItem;
import org.eclipse.jface.action.IContributionItem;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ShellAdapter;
import org.eclipse.swt.events.ShellEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.w3c.dom.Element;


/*
 * class responsible for opening favorite elements windows
 */

public class ConfigureFavorite {

	public Shell favoriteShell = new Shell();

	public Shell allElementShell = new Shell();

	private LinkedList availElement = new LinkedList();

	public String favoriteItem = ""; // items in the first window

	public String favoriteItemConf = ""; // items in the second window

	public Table tableFavorite = new Table(favoriteShell, SWT.BORDER_SOLID
			| SWT.V_SCROLL);

	public boolean shell_open = false;

	public Button addElement;
	
	VexWidget vex = null;
	
	public List favoriteElementArray = new LinkedList();



	public ConfigureFavorite(VexWidget vex){

		this.vex = vex;
	}
	public boolean configure(final Display display, final String[][] data) {


		// System.out.println("data: " + data.length);

		favoriteShell = new Shell(display, SWT.BORDER_SOLID | SWT.CLOSE
				| SWT.LINE_SOLID | SWT.SHADOW_NONE | SWT.ON_TOP);
		favoriteShell.setBounds(10, 10, 200, 300);
		favoriteShell.setText("Available tags");

		favoriteShell.addShellListener(new ShellAdapter() {
			@Override
			public void shellDeactivated(ShellEvent e) {
				if (!allElementShell.isVisible()) {
					((Shell) e.getSource()).setVisible(false);
				}
else {

				}

			}
		});




		Point absolutePosition = this.vex.getDisplay().getCurrent()
				.getCursorLocation();

		favoriteShell.setLocation(absolutePosition.x, absolutePosition.y);

		favoriteShell.setBackground(display
				.getSystemColor(SWT.COLOR_WIDGET_LIGHT_SHADOW));
		favoriteShell
				.setForeground(display.getSystemColor(SWT.COLOR_BLACK));

		tableFavorite = new Table(favoriteShell, SWT.BORDER_SOLID
				| SWT.V_SCROLL);

		tableFavorite.setBounds(0, 0, 200, 200);


		if (availElement.size() <= 0) {
			availElement.add("TEI");
			availElement.add("teiHeader");
			availElement.add("facsimile");
			availElement.add("text");
			availElement.add("body");
			availElement.add("div");
			availElement.add("head");
			availElement.add("p");
			availElement.add("note");
			availElement.add("rs");

		}

		for (int i = 0; i < availElement.size(); i++) {
			new TableItem(tableFavorite, SWT.NONE).setText(availElement
					.get(i).toString());
		}

		final Button addToToolbar = new Button(favoriteShell, SWT.NONE);
		addToToolbar.setText("Add to Toolbar");
		addToToolbar.setBounds(95, 210, 90, 25);
		addToToolbar.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event arg0) {
				if (favoriteItemConf != "" && favoriteItemConf != null) {
					availElement.add(favoriteItemConf);
				}
				if (favoriteItem != "" && favoriteItemConf != null) {
					addToToolbar(favoriteItem);
				}

				if (favoriteShell.isVisible()) {
					favoriteShell.setVisible(false);
				}
			}

		});
		addToToolbar.setEnabled(false);

		tableFavorite.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event event) {

				favoriteItem = event.detail == SWT.CHECK ? "Checked"
						: tableFavorite.getItem(
								tableFavorite.getSelectionIndex())
								.getText();
				addToToolbar.setEnabled(true);

			}
		});

		tableFavorite.addListener(SWT.DefaultSelection, new Listener() {
			public void handleEvent(Event e) {
				if (favoriteShell.isVisible()) {
					favoriteShell.setVisible(false);
					if (favoriteItemConf != "" && favoriteItemConf != null) {
						availElement.add(favoriteItemConf);
					}
					if (favoriteItem != "" && favoriteItemConf != null) {
						addToToolbar(favoriteItem);
					}
				}
			}
		});

		addElement = new Button(favoriteShell, SWT.NONE);
		addElement.setText("Add Element");
		addElement.setBounds(10, 210, 70, 25);
		addElement.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event arg0) {
				if (!allElementShell.isVisible())
					getAllConfigurableElements(display, data);
				allElementShell.forceFocus();
			}
		});

		favoriteShell.addListener(SWT.Traverse, new Listener() {
			public void handleEvent(Event event) {
				switch (event.detail) {
				case SWT.TRAVERSE_ESCAPE:
					if (favoriteShell.isVisible()) {
						favoriteShell.setVisible(false);
						allElementShell.setVisible(false);
					}
					event.detail = SWT.TRAVERSE_NONE;
					event.doit = false;
					break;
				}
			}
		});

		shell_open = true;
		favoriteShell.setVisible(true);
		favoriteShell.forceFocus();

		while (!favoriteShell.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}

		return shell_open;
	}

	public String getAllConfigurableElements(Display display,
			String[][] data) {

		allElementShell = new Shell(display, SWT.BORDER_SOLID | SWT.CLOSE
				| SWT.SHADOW_NONE | SWT.ON_TOP);
		allElementShell.setBounds(10, 10, 200, 260);
		allElementShell.setText("Elements");

		allElementShell.addShellListener(new ShellAdapter() {
			@Override
			public void shellDeactivated(ShellEvent e) {
				((Shell) e.getSource()).setVisible(false);

			}
		});

		Point absolutePosition = this.vex.getDisplay().getCurrent()
				.getCursorLocation();

		allElementShell.setLocation(absolutePosition.x, absolutePosition.y);

		allElementShell.setBackground(display
				.getSystemColor(SWT.COLOR_WIDGET_LIGHT_SHADOW));
		allElementShell.setForeground(display
				.getSystemColor(SWT.COLOR_BLACK));

		final Table table = new Table(allElementShell, SWT.BORDER_SOLID
				| SWT.V_SCROLL);

		table.setBounds(0, 0, 200, 200);

		String[] listFavorite = new String[data[0].length];

		for (int i = 0; i < listFavorite.length; i++) {
			new TableItem(table, SWT.NONE).setText(data[0][i]);
		}

		table.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event event) {

				favoriteItemConf = event.detail == SWT.CHECK ? "Checked"
						: table.getItem(table.getSelectionIndex())
								.getText();
			}
		});

		table.addListener(SWT.DefaultSelection, new Listener() {
			public void handleEvent(Event e) {

				TableItem item = new TableItem(tableFavorite, SWT.NONE,
						tableFavorite.getItemCount());

				// availElement.add(favoriteItemConf);

				if (allElementShell.isVisible()) {
					allElementShell.setVisible(false);

					for (int j = 0; j < tableFavorite.getItemCount(); j++) {
						if (tableFavorite.getItem(j).getText() == favoriteItemConf) {
							// System.out.println(tableFavorite.getItem(j)
							// .getText()
							// + " : " + favoriteItemConf);

							tableFavorite.remove(j);

						}
					}
					item.setText(favoriteItemConf);

				}
			}
		});

		allElementShell.addListener(SWT.Traverse, new Listener() {
			public void handleEvent(Event event) {
				switch (event.detail) {
				case SWT.TRAVERSE_ESCAPE:
					if (allElementShell.isVisible()) {
						favoriteShell.setVisible(false);
						allElementShell.setVisible(false);
					}
					event.detail = SWT.TRAVERSE_NONE;
					event.doit = false;
					break;
				}
			}
		});

		allElementShell.setVisible(true);


		return favoriteItemConf;
	}
	
	/*
	 * add selected elements to the toolbar
	 */
	public String addToToolbar(final String element) {

		IWorkbenchWindow window = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow();

		IActionBars actBar = window.getActivePage().getActiveEditor()
				.getEditorSite().getActionBars();
		final IToolBarManager toolbarMgr = actBar.getToolBarManager();
		Action actionElement = new Action("&" + element) {
			@Override
			public void run() {

				GEditorContentAssistant contentWindow = new GEditorContentAssistant(
						vex);
				contentWindow.Favorite(element);
			}
		};

		boolean found = false;
		for (Object x : favoriteElementArray.toArray()) {
			if (x == element) {
				found = true;
				break;
			}
		}
		if (found) {

			MessageDialog.openInformation(
					Display.getDefault().getActiveShell(), "Favorite", element
							+ " already shown on the toolbar");
		} else {
			toolbarMgr.add(actionElement);

			IContributionItem[] items = toolbarMgr.getItems();
			((ActionContributionItem) items[1]).getAction().setEnabled(true);

			toolbarMgr.update(true);

		}

		favoriteElementArray.add(element);

		return element;
	}


	/*
	 * get all the elements of the document
	 */

	public String[][] getDocumentElements() {

		String infoData[][] = null;

		Element element = null;
		IVexElement currentElement = this.vex.getCurrentElement();

		//System.out.println("data: " + currentElement);

		if (currentElement instanceof LinkedElement) {

			LinkedElement linkedElement = (LinkedElement) currentElement;
			element = linkedElement.getDomNode();

			AbstractModelQueryActionWrapper[] insertActions = InsertAssistant
					.getInsertActions(this.vex, null, element);

			// System.out.println(insertActions.length + " : "
			// + currentElement.getName());

			if (insertActions.length > 0) {
				infoData = new String[2][insertActions.length];

				for (int i = 0; i < 2; i++) {
					for (int j = 0; j < insertActions.length; j++) {
						if (i == 0) {
							infoData[i][j] = insertActions[j].getText();

						} else {
							infoData[i][j] = insertActions[j].getDescription();
						}
						// System.out.println(infoData[0][j]);
					}
				}
			} else {
				String comment = "";
				infoData = new String[2][1];
				infoData[0][0] = "#comment";
				infoData[1][0] = comment;
			}

		}
		return infoData;

	}



}

