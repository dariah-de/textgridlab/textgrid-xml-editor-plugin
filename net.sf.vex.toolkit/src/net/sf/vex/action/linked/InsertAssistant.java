/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2004 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.action.linked;

import java.util.LinkedList;
import java.util.List;

import net.sf.vex.action.ContentAssistant;
import net.sf.vex.dom.IVexElement;
import net.sf.vex.dom.impl.WrongModelException;
import net.sf.vex.dom.linked.LinkedElement;
import net.sf.vex.swt.VexWidget;

import org.eclipse.jface.action.IAction;
import org.eclipse.jface.text.source.ISourceViewer;
import org.eclipse.wst.xml.core.internal.contentmodel.CMElementDeclaration;
import org.eclipse.wst.xml.core.internal.contentmodel.modelquery.ModelQuery;
import org.eclipse.wst.xml.core.internal.contentmodel.modelquery.ModelQueryAction;
import org.eclipse.wst.xml.core.internal.modelquery.ModelQueryUtil;
import org.w3c.dom.Element;


/**
 * Content assistant that shows valid elements to be inserted at the current
 * point.
 */
@SuppressWarnings("restriction")
public class InsertAssistant extends ContentAssistant {
    
    @Override
	public IAction[] getActions(VexWidget vexWidget) {

		IVexElement currentElement = vexWidget.getCurrentElement();
		if (currentElement instanceof LinkedElement) {
			LinkedElement linkedElement = (LinkedElement) currentElement;
			Element element = linkedElement.getDomNode();
			return getInsertActions(vexWidget, null, element);
		} else
			throw new WrongModelException(currentElement.getClass(),
					LinkedElement.class);
    }

	public static AbstractModelQueryActionWrapper[] getInsertActions(
			VexWidget vexWidget,
			ISourceViewer viewer,
			Element element) {
		ModelQuery modelQuery = ModelQueryUtil.getModelQuery(element.getOwnerDocument());
		CMElementDeclaration declaration = modelQuery.getCMElementDeclaration(element);
		List<ModelQueryAction> actionList = new LinkedList<ModelQueryAction>();

		if (declaration != null) // TG-456. If we don't have a model yet, just
									// return an empty list.
			modelQuery.getInsertActions(element, declaration,
				0 /*
															 * FIXME actual
															 * insert index
															 */,
				ModelQuery.INCLUDE_CHILD_NODES,
				ModelQuery.VALIDITY_PARTIAL, actionList);
		return AbstractModelQueryActionWrapper.create(actionList, vexWidget, viewer, null);
	}
	
	


    @Override
	public String getTitle(VexWidget vexWidget) {
        return "Insert element";
    }
}
