package net.sf.vex.action.linked;

import java.text.MessageFormat;

import net.sf.vex.widget.IVexWidget;

import org.eclipse.jface.action.IAction;
import org.eclipse.jface.text.source.ISourceViewer;
import org.eclipse.wst.sse.core.internal.provisional.text.IStructuredDocumentRegion;
import org.eclipse.wst.xml.core.internal.contentmodel.CMNode;
import org.eclipse.wst.xml.core.internal.contentmodel.modelquery.ModelQueryAction;
import org.eclipse.wst.xml.core.internal.provisional.document.IDOMElement;
import org.eclipse.wst.xml.core.internal.provisional.document.IDOMModel;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

/**
 * An action to rename an element.
 * 
 * @author tv
 */
@SuppressWarnings("restriction")
public class ReplaceNodeAction extends AbstractModelQueryActionWrapper implements IAction {

	private Element element;

	@Override
	public void run() {
		ModelQueryAction action = getModelQueryAction();
		/*
		 * Renaming a node isn't implemented in WST's DOM implementation. So
		 * we'll have to work on the source view, by identifying the start and
		 * end tags and replacing their contents correspondingly. Wonder how Vex
		 * will react.
		 */
		if (action.getParent().getNodeType() == Node.ELEMENT_NODE) {
			if (element instanceof IDOMElement) {
				IDOMElement domElement = (IDOMElement) element;
				IDOMModel model = domElement.getModel();
				String oldName = domElement.getNodeName();
				String newName = getCMNode().getNodeName();
				setStructuredDocumentRegionElementName(model, domElement.getEndStructuredDocumentRegion(), oldName, newName);
				setStructuredDocumentRegionElementName(model, domElement.getStartStructuredDocumentRegion(), oldName, newName);
			}
		}

	}

	/**
	 * @deprecated Use {@link #ReplaceNodeAction(ModelQueryAction,CMNode,IVexWidget,ISourceViewer, Element)} instead
	 */
	public ReplaceNodeAction(ModelQueryAction mqa, IVexWidget vexWidget,
			ISourceViewer sourceViewer) {
				this(mqa, null, vexWidget, sourceViewer, null);
			}

	/**
	 * @deprecated Use {@link #ReplaceNodeAction(ModelQueryAction,CMNode,IVexWidget,ISourceViewer,Element)} instead
	 */
	public ReplaceNodeAction(ModelQueryAction mqa, IVexWidget vexWidget,
			ISourceViewer sourceViewer, Element element) {
				this(mqa, null, vexWidget, sourceViewer, element);
			}

	public ReplaceNodeAction(ModelQueryAction mqa, CMNode cmNode,
			IVexWidget vexWidget, ISourceViewer sourceViewer, Element element) {
		if (mqa.getKind() != ModelQueryAction.REPLACE) {
			throw new IllegalArgumentException(
					MessageFormat
							.format(
									"Tried to create a replace node actionn from an inappropriate model query action (kind {0} instead of {1})",
									mqa.getKind(), ModelQueryAction.REPLACE));

		}
		this.element = element;
		init(mqa, cmNode, vexWidget, sourceViewer);
		
	}

	// From org.eclipse.wst.xml.ui.internal.actions.EditElementAction
	protected void setStructuredDocumentRegionElementName(IDOMModel model,IStructuredDocumentRegion flatNode, String oldName, String newName) {
		if (flatNode != null) {
			String string = flatNode.getText();
			
			System.out.println("elt: "+ flatNode.getText());
			
			
			int index = string.indexOf(oldName);
			if (index != -1) {
				index += flatNode.getStart();
				model.getStructuredDocument().replaceText(this, index, oldName.length(), newName);
			}
		}
	}


}
