package net.sf.vex.action.linked;

import org.eclipse.jface.action.IAction;

/*
import static info.textgrid.lab.core.swtutils.AdapterUtils.getAdapter;
import info.textgrid.lab.core.swtutils.AdapterUtils;
import info.textgrid.lab.xmleditor.mpeditor.MPXmlEditorPart;

import org.eclipse.wst.sse.ui.StructuredTextEditor;


import java.text.MessageFormat;

import net.sf.vex.action.linked.InsertNodeAction.XMLContentModelGeneratorExtension;
import net.sf.vex.dom.linked.LinkedNode;
import net.sf.vex.widget.IVexWidget;

import org.eclipse.jface.action.IAction;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.BadPositionCategoryException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.source.ISourceViewer;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.texteditor.ITextEditor;
import org.eclipse.wst.sse.core.internal.provisional.IndexedRegion;
import org.eclipse.wst.sse.core.internal.provisional.text.IStructuredDocument;
import org.eclipse.wst.sse.core.internal.provisional.text.IStructuredDocumentRegion;
import org.eclipse.wst.sse.ui.StructuredTextEditor;
import org.eclipse.wst.sse.ui.internal.StructuredTextViewer;
import org.eclipse.wst.sse.ui.internal.contentassist.ContentAssistUtils;
import org.eclipse.wst.xml.core.internal.contentmodel.CMElementDeclaration;
import org.eclipse.wst.xml.core.internal.contentmodel.modelquery.ModelQueryAction;
import org.eclipse.wst.xml.core.internal.provisional.document.IDOMElement;
import org.eclipse.wst.xml.core.internal.provisional.document.IDOMModel;
import org.eclipse.wst.xml.core.internal.provisional.document.IDOMNode;
import org.eclipse.wst.xml.ui.internal.contentassist.XMLContentModelGenerator;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

*/

/**
 * to rename an element.
 * 
 * @author TextGrid / Uni Würzburg / Mohamadou Nassourou
 *         <mohamadou.nassourou@uni-wuerzburg.de>
 */

  
@SuppressWarnings("restriction")
public class ReplaceNodeActionContentAssist extends AbstractModelQueryActionWrapper implements IAction {

/*

	@Override
	public void run() {
		ModelQueryAction action = getModelQueryAction();

		
		if (action.getParent().getNodeType() == Node.ELEMENT_NODE) {
			Element parent = (Element) action.getParent();
			if (parent instanceof IDOMElement) {
				IDOMElement domElement = (IDOMElement) parent;
				
				
				IDOMElement domElement_child = null;
				IDOMModel model_child = null;
				String oldName_child = null;

				
				 IWorkbenchPage page;
				 page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
				 IEditorPart part = page.getActiveEditor();
				 ITextEditor editor = AdapterUtils.getAdapter(part, ITextEditor.class);
			
				 Element element = getCurrentElementFromMPEditor(part);
			 
				if (editor != null) {
				
					//ITextSelection selection = (ITextSelection) editor.getSelectionProvider().getSelection();
					
					 for(int i=0; i<domElement.getLength(); i++){	
							Node test = domElement.getChildNodes().item(i);
							if (test != null && test.getNodeName().equalsIgnoreCase(element.getNodeName())){								
								domElement_child = (IDOMElement) test;
								model_child = domElement_child.getModel();
								oldName_child = domElement_child.getNodeName();
							}
					 }	

					 
						String newName = action.getCMNode().getNodeName(); 
						setStructuredDocumentRegionElementName(model_child, domElement_child.getEndStructuredDocumentRegion(), oldName_child, newName);
						setStructuredDocumentRegionElementName(model_child, domElement_child.getStartStructuredDocumentRegion(), oldName_child, newName);


				}
			}
		}

	}
 

	protected static Element getCurrentElementFromMPEditor(IEditorPart activeEditor) {
		Element element = null;
		StructuredTextEditor sourceEditor = null;
		if (activeEditor instanceof MPXmlEditorPart) {
			MPXmlEditorPart mpEditor = (MPXmlEditorPart) activeEditor;
			sourceEditor = mpEditor.getSourceEditor();
			StructuredTextViewer textViewer = sourceEditor.getTextViewer();
			IndexedRegion region = ContentAssistUtils.getNodeAt(textViewer,	sourceEditor.getHighlightRange().getOffset());
			Node node = getAdapter(region, Node.class);
			if (node instanceof Element)
				element = (Element) node;
			else {
				Node parentNode = node.getParentNode();
				if (parentNode != null && parentNode instanceof Element)
					element = (Element) parentNode;
			}
		}
		return element;
	}	
	
	
	
	public class XMLContentModelGeneratorExtension extends XMLContentModelGenerator {
		public void generateEndTag(Node parentNode,
			CMElementDeclaration elementDecl, StringBuffer buffer) {
			String tagName = getRequiredName(parentNode, elementDecl);
			super.generateEndTag(tagName, parentNode, elementDecl, buffer);
		}

		public void generateStartTag(Node parentNode,
			CMElementDeclaration elementDecl, StringBuffer buffer) {
			String tagName = getRequiredName(parentNode, elementDecl);
			super.generateStartTag(tagName, parentNode, elementDecl, buffer);
		}
	}
	
	private XMLContentModelGeneratorExtension contentModelGenerator;
	private XMLContentModelGeneratorExtension getContentModelGenerator() {
		if (contentModelGenerator == null)
			contentModelGenerator = new XMLContentModelGeneratorExtension();
		return contentModelGenerator;
	}
	
	

	public ReplaceNodeActionContentAssist(ModelQueryAction mqa, IVexWidget vexWidget,
			ISourceViewer sourceViewer) {
		if (mqa.getKind() != ModelQueryAction.REPLACE) {
			throw new IllegalArgumentException(
					MessageFormat
							.format(
									"Tried to create a replace node actionn from an inappropriate model query action (kind {0} instead of {1})",
									mqa.getKind(), ModelQueryAction.REPLACE));

		}
		init(mqa, vexWidget, sourceViewer);
		
	}

	// From org.eclipse.wst.xml.ui.internal.actions.EditElementAction
	protected void setStructuredDocumentRegionElementName(IDOMModel model,IStructuredDocumentRegion flatNode, String oldName, String newName) {
		if (flatNode != null) {
			String string = flatNode.getText();
			int index = string.indexOf(oldName);
			
			System.out.println("oldName: "+ flatNode.getText()+" index: "+index);
			
			if (index != -1) {
				index += flatNode.getStart();
				model.getStructuredDocument().replaceText(this, index, oldName.length(), newName);
			}
		}
	}

*/
}
