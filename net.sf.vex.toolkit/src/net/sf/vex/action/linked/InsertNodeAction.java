/**
 * 
 */
package net.sf.vex.action.linked;

import static info.textgrid.lab.core.swtutils.AdapterUtils.getAdapter;

import java.text.MessageFormat;

import net.sf.vex.VexToolkitPlugin;
import net.sf.vex.widget.IVexWidget;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.Region;
import org.eclipse.jface.text.source.ISourceViewer;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.wst.sse.core.internal.provisional.text.IStructuredDocument;
import org.eclipse.wst.xml.core.internal.contentmodel.CMElementDeclaration;
import org.eclipse.wst.xml.core.internal.contentmodel.modelquery.ModelQueryAction;
import org.eclipse.wst.xml.ui.internal.contentassist.XMLContentModelGenerator;
import org.w3c.dom.Node;

/**
 * Performs the Act of actually inserting an element at the current position.
 * These actions are, e.g., members of the InsertAssistant.
 * 
 * @author Thorsten Vitt <vitt@linglit.tu-darmstadt.de> for <a
 *         href="http://www.textgrid.info/">TextGrid</a> (tv)
 */
@SuppressWarnings("restriction")
public class InsertNodeAction extends AbstractModelQueryActionWrapper {
	
	@Override
	public ISourceViewer getSourceViewer() {
		ISourceViewer viewer = super.getSourceViewer();
		
		//System.out.println("viewer: "+viewer.getSelectedRange());

		if (viewer == null) {
			IEditorPart activeEditor = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor();
			if (activeEditor != null) {
				viewer = getAdapter(activeEditor, ISourceViewer.class);
				if (viewer != null)
					setSourceViewer(viewer);				    
			}
		}
		return viewer;
	}
	

	public class XMLContentModelGeneratorExtension extends
			XMLContentModelGenerator {
		public void generateEndTag(Node parentNode,
				CMElementDeclaration elementDecl, StringBuffer buffer) {
			String tagName = getRequiredName(parentNode, elementDecl);
			super.generateEndTag(tagName, parentNode, elementDecl, buffer);
		}

		public void generateStartTag(Node parentNode,
				CMElementDeclaration elementDecl, StringBuffer buffer) {
			String tagName = getRequiredName(parentNode, elementDecl);
			super.generateStartTag(tagName, parentNode, elementDecl, buffer);
		}
	}

	private XMLContentModelGeneratorExtension contentModelGenerator;

	private XMLContentModelGeneratorExtension getContentModelGenerator() {
		if (contentModelGenerator == null)
			contentModelGenerator = new XMLContentModelGeneratorExtension();
		return contentModelGenerator;
	}

	public InsertNodeAction(ModelQueryAction action, IVexWidget vexWidget,
			ISourceViewer sourceViewer) {
		if (action.getKind() != ModelQueryAction.INSERT)
			throw new IllegalArgumentException(
					MessageFormat
							.format(
									"Tried to create an insert node action from a model query action ({0}) of kind {1}, not {2} (= INSERT)",
									action, action.getKind(),
									ModelQueryAction.INSERT));
		init(action, null, vexWidget, sourceViewer);
		
		setText(action.getCMNode().getNodeName());
		
		//System.out.println(action.getCMNode().getNodeName());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.action.Action#run()
	 */
	@Override
	public void run() {
		ModelQueryAction action = getModelQueryAction();
		StringBuffer sb = new StringBuffer();
		IRegion selection = getSelectedSourceRegion();
		
		IStructuredDocument document = (IStructuredDocument) getSourceViewer().getDocument();

		try {
			
						
			if (selection.getLength() == 0) {
			
				getContentModelGenerator().generateTag(action.getParent(),(CMElementDeclaration) action.getCMNode(), sb);
				document.replace(selection.getOffset(), 0, sb.toString());
												
				

				// getVexWidget().reLayout(); // needed before selection
				//getVexWidget().moveBy(+1); // move inside the new element
				
				//getVexWidget().moveToLineEnd(false);
				
				//IVexElement elt = getVexWidget().getCurrentElement();
				//IVexNode nod = getVexWidget().getCurrentNode();
				//getVexWidget().moveBy(elt.getEndPosition().getOffset()); 
				//System.out.println("elt: "+ nod.getEndPosition());
				
				
				
				
			} else {
				
				String oldSource = document.get(selection.getOffset(),selection.getLength());
				getContentModelGenerator().generateStartTag(action.getParent(),	(CMElementDeclaration) action.getCMNode(), sb);
				int startTagLength = sb.length();
				sb.append(oldSource);
				
				getContentModelGenerator().generateEndTag(action.getParent(),(CMElementDeclaration) action.getCMNode(), sb);
				document.replace(selection.getOffset(), selection.getLength(),sb.toString());
				
				//System.out.println("offset: "+selection.getOffset()+" length: "+ " : "+ selection.getLength() +" text: "+ sb.toString());
				
				
				if (getSourceViewer() != null)
					getSourceViewer().setSelectedRange(
							selection.getOffset() + startTagLength,
							selection.getLength());
				
				if (getVexWidget() != null) { // FIXME do we need this if we
												// have a source viewer?
					getVexWidget().reLayout(); // needed before selection
					getVexWidget().selectSourceRegion(
							new Region(selection.getOffset() + startTagLength,
									selection.getLength()));
				}
				
				// FIXME does this trigger Vex as well?

				// // move selection right by 1 (that's the new \0): // doesn't
				// // work
				// getVexWidget().moveTo(vexSelection.getOffset() + 1);
				// getVexWidget().moveBy(vexSelection.getLength(), true);
			}
//			((LinkedDocument) getVexWidget().getDocument())
//					.printDocument(System.out); // XXX DEBUG
			if (getVexWidget() != null)
				getVexWidget().reLayout(); // FIXME do we need this?
		
		} catch (BadLocationException e) {
			VexToolkitPlugin
					.log(
							IStatus.ERROR,
							e,
							"Could not insert element string ({0}) into the source document at position {1}: bad location",
							sb.toString(), selection);
		}
	}
}
