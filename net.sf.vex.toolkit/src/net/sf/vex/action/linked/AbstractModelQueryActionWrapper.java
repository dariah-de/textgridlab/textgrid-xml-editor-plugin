/**
 * 
 */
package net.sf.vex.action.linked;

import info.textgrid.lab.core.swtutils.AdapterUtils;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import net.sf.vex.swt.TextSelectionVex;
import net.sf.vex.widget.IVexWidget;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.text.Region;
import org.eclipse.jface.text.source.ISourceViewer;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.texteditor.IDocumentProvider;
import org.eclipse.ui.texteditor.ITextEditor;
import org.eclipse.wst.xml.core.internal.contentmodel.CMGroup;
import org.eclipse.wst.xml.core.internal.contentmodel.CMNode;
import org.eclipse.wst.xml.core.internal.contentmodel.CMNodeList;
import org.eclipse.wst.xml.core.internal.contentmodel.modelquery.ModelQueryAction;
import org.eclipse.wst.xml.ui.internal.taginfo.MarkupTagInfoProvider;
import org.w3c.dom.Element;



/**
 * 
 * @author Thorsten Vitt &lt;vitt@linglit.tu-darmstadt.de> for <a
 *         href="http://www.textgrid.info/">TextGrid</a> (tv)
 * @see ModelQueryAction
 */
@SuppressWarnings("restriction")
public abstract class AbstractModelQueryActionWrapper extends Action {
	

	private static final MarkupTagInfoProvider MARKUP_TAG_INFO_PROVIDER = new MarkupTagInfoProvider();
	private ModelQueryAction modelQueryAction = null;
	private IVexWidget vexWidget;
	private CMNode cmNode;
	private ISourceViewer sourceViewer;
	private static final Pattern REMOVE_TAGS = Pattern.compile("<[^>]+>");
	private static final Pattern CLEAN_SPACE = Pattern.compile("  +");

	/**
	 * This should be called from the constructor.
	 * 
	 * @param modelQueryAction
	 * @param vexWidget
	 * @param sourceViewer
	 * @deprecated Use {@link #init(ModelQueryAction,CMNode,IVexWidget,ISourceViewer)} instead
	 */
	protected void init(ModelQueryAction modelQueryAction,IVexWidget vexWidget, ISourceViewer sourceViewer) {
		init(modelQueryAction, null, vexWidget, sourceViewer);
	}

	/**
	 * This should be called from the constructor.
	 * 
	 * @param modelQueryAction
	 * @param cmNode TODO
	 * @param vexWidget
	 * @param sourceViewer
	 */
	protected void init(ModelQueryAction modelQueryAction,CMNode cmNode, IVexWidget vexWidget, ISourceViewer sourceViewer) {
		this.modelQueryAction = modelQueryAction;
		this.vexWidget = vexWidget;
		this.setSourceViewer(sourceViewer);
		this.setCMNode(cmNode == null ? modelQueryAction.getCMNode() : cmNode);
		setText(getCMNode().getNodeName());
		
		String infoFragment = MARKUP_TAG_INFO_PROVIDER.getInfo(getCMNode());
		infoFragment = CLEAN_SPACE.matcher(REMOVE_TAGS.matcher(infoFragment).replaceAll("")).replaceAll(" ");
		if (!"".equals(infoFragment)) {
			setToolTipText(infoFragment);
			setDescription(infoFragment);
//			System.out.println(infoFragment);
		}
		System.out.println(MessageFormat.format("{0}\n  parent: {1}\n  CM Node: {2}\n   user data: {3}",
				this.getClass().getSimpleName(), modelQueryAction.getParent(), modelQueryAction.getCMNode(),
				modelQueryAction.getUserData()));
	}

	@Override
	public String getToolTipText() {
		if (super.getToolTipText() == null) {
		}
		return super.getToolTipText();
	}
	
	/**
	 * @return the vexWidget
	 */
	public IVexWidget getVexWidget() {
		return vexWidget;
	}

	public ModelQueryAction getModelQueryAction() {
		return modelQueryAction;
	}

	/**
	 * @deprecated Use {@link #create(ModelQueryAction,CMNode,IVexWidget,ISourceViewer, Element)} instead
	 */
	public static AbstractModelQueryActionWrapper create(ModelQueryAction mqa,
			IVexWidget vexWidget, ISourceViewer sourceViewer) {
		return create(mqa, null, vexWidget, sourceViewer, null);
			}

	/**
	 * @deprecated Use {@link #create(ModelQueryAction,CMNode,IVexWidget,ISourceViewer,Element)} instead
	 */
	public static AbstractModelQueryActionWrapper create(ModelQueryAction mqa,
			IVexWidget vexWidget, ISourceViewer sourceViewer, Element element) {
				return create(mqa, null, vexWidget, sourceViewer, element);
			}

	public static AbstractModelQueryActionWrapper create(ModelQueryAction mqa,
			CMNode cmNode, IVexWidget vexWidget, ISourceViewer sourceViewer, Element element) {
		
		switch (mqa.getKind()) {
		case ModelQueryAction.INSERT:
			return new InsertNodeAction(mqa, vexWidget, sourceViewer);
		case ModelQueryAction.REPLACE:
			return new ReplaceNodeAction(mqa, cmNode, vexWidget, sourceViewer, element);
			/*
			 * TODO implement remove 
			 * 
			 * case ModelQueryAction.REMOVE: return new RemoveNodeAction(mqa);
			 */
		}
		return null;
	}

	/**
	 * @deprecated Use {@link #create(List<ModelQueryAction>,IVexWidget,ISourceViewer,Element)} instead
	 */
	public static AbstractModelQueryActionWrapper[] create(
			List<ModelQueryAction> mqas, IVexWidget vexWidget,
			ISourceViewer sourceViewer) {
				return create(mqas, vexWidget, sourceViewer, null);
			}

	public static AbstractModelQueryActionWrapper[] create(
			List<ModelQueryAction> mqas, IVexWidget vexWidget,
			ISourceViewer sourceViewer, Element element) {
		ArrayList<AbstractModelQueryActionWrapper> result = new ArrayList<AbstractModelQueryActionWrapper>(
				mqas.size());
		for (ModelQueryAction mqa : mqas) {
			expand(mqa, null, result, vexWidget, sourceViewer, element);
		}
		return result.toArray(new AbstractModelQueryActionWrapper[0]);
	}

	/**
	 * @deprecated Use {@link #expand(ModelQueryAction,CMNode,ArrayList<
	 *             AbstractModelQueryActionWrapper
	 *             >,IVexWidget,ISourceViewer,Element)} instead
	 */
	private static void expand(ModelQueryAction mqa, ArrayList<AbstractModelQueryActionWrapper> result, IVexWidget vexWidget,
			ISourceViewer sourceViewer, Element element) {
		expand(mqa, null, result, vexWidget, sourceViewer, element);
	}

	private static void expand(ModelQueryAction mqa, CMNode cmNode, ArrayList<AbstractModelQueryActionWrapper> result,
			IVexWidget vexWidget, ISourceViewer sourceViewer, Element element) {
		if (cmNode == null)
			cmNode = mqa.getCMNode();

		if (cmNode instanceof CMGroup) {
			int operator = ((CMGroup) cmNode).getOperator();
			if (operator != CMGroup.SEQUENCE) {
				final CMNodeList childNodes = ((CMGroup) cmNode).getChildNodes();
				for (int i = 0; i < childNodes.getLength(); i++)
					expand(mqa, childNodes.item(i), result, vexWidget, sourceViewer, element);
			}
		} else {
			final AbstractModelQueryActionWrapper amqaw = AbstractModelQueryActionWrapper.create(mqa, cmNode, vexWidget,
					sourceViewer, element);
			if (amqaw != null) {
				result.add(amqaw);
			}
		}
	}

	public void setSourceViewer(ISourceViewer sourceViewer) {
		this.sourceViewer = sourceViewer;
	}

	public ISourceViewer getSourceViewer() {
		return sourceViewer;
	}
	
	
	
	
	
	/**
	 * Returns the region selected in the source editor or <code>null</code>.
	 */
	public IRegion getSelectedSourceRegion() {
		
		 IWorkbenchPage page;
		 page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
		 IEditorPart part = page.getActiveEditor();
		 ITextEditor editor = AdapterUtils.getAdapter(part, ITextEditor.class);
		if (editor != null) {
			IDocumentProvider dp = editor.getDocumentProvider();
			IDocument doc = dp.getDocument(editor.getEditorInput());
			ITextSelection selection = (ITextSelection) editor.getSelectionProvider().getSelection();
			int length = selection.getLength();
			int offset = selection.getOffset();
			
			//editor.getSelectionProvider().setSelection(new TextSelection(offset+1,0));
			
			return new Region(offset, length);
		
		}
		
		
	/*	
		if (sourceViewer != null) {
			Point selectedRange = sourceViewer.getSelectedRange();
			return new Region(selectedRange.x, selectedRange.y);
		} else if (vexWidget != null) {
	 */		
			
		else if (vexWidget != null) {
			TextSelectionVex vexSelection = new TextSelectionVex(getVexWidget());
			int startOffset = vexSelection.getSourceStartOffset();
			int endOffset = vexSelection.getSourceEndOffset();		
			
			return new Region(startOffset, endOffset - startOffset);
		}
		return null;
	}

	public void setCMNode(CMNode cmNode) {
		this.cmNode = cmNode;
	}

	public CMNode getCMNode() {
		return cmNode;
	}
}
