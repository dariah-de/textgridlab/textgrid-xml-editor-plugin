/**
 * This package contains {@linkplain org.eclipse.jface.action.Action actions}
 * specific for the {@link net.sf.vex.editor.VexEditorPage} with its linked 
 * DOM implementation.  
 * 
 * 
 * <h3>Kinds of Actions</h3>
 * 
 * <p>
 * a {@linkplain org.eclipse.wst.xml.core.internal.contentmodel.modelquery.ModelQueryAction ModelQueryAction} 
 * is an WST interface that describes possible edit operations. Lists of ModelQueryActions
 * can be created by {@link org.eclipse.wst.xml.core.internal.contentmodel.modelquery.ModelQuery}.
 * </p>
 */
package net.sf.vex.action.linked;

