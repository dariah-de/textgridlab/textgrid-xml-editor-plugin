/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2005 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.action;

import net.sf.vex.layout.Box;
import net.sf.vex.layout.TableRowBox;
import net.sf.vex.widget.IBoxFilter;
import net.sf.vex.widget.IVexWidget;

/**
 * Moves the caret to the next table cell. The contents of the cell
 * are selected. If the current cell is the last cell in the table, 
 * the current row is duplicated.
 */
public class NextTableCellAction extends AbstractVexAction {

    public void run(final IVexWidget vexWidget) {
        
        final TableRowBox tr = (TableRowBox) vexWidget.findInnermostBox(new IBoxFilter() {
            public boolean matches(Box box) {
                return box instanceof TableRowBox;
            }
        });

        if (tr == null) {
            // not in a table row
            return;
        }
        
        int offset = vexWidget.getCaretOffset();
        
        Box[] cells = tr.getChildren();
        for (int i = 0; i < cells.length; i++) {
            if (cells[i].getStartOffset() > offset) {
                vexWidget.moveTo(cells[i].getStartOffset());
                vexWidget.moveTo(cells[i].getEndOffset(), true);
                return;
            }
        }
        
        // No next cell found in this row
        // Find the next row
        Box[] rows = tr.getParent().getChildren(); 
        for (int i = 0; i < rows.length; i++) {
            if (rows[i].getStartOffset() > offset) {
                cells = rows[i].getChildren();
                if (cells.length > 0) {
                    Box cell = cells[0];
                    vexWidget.moveTo(cell.getStartOffset());
                    vexWidget.moveTo(cell.getEndOffset(), true);
                } else {
                    System.out.println("TODO - dup row into new empty row");
                }
                return;
            }
        }
        

        // We didn't find a "next row", so let's dup the current one
        ActionUtils.duplicateTableRow(vexWidget, tr);
 
    }
}
