/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2005 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.action;

import java.util.ArrayList;
import java.util.List;

import net.sf.vex.css.CSS;
import net.sf.vex.css.Styles;
import net.sf.vex.dom.IVexComment;
import net.sf.vex.dom.IVexDocument;
import net.sf.vex.dom.IVexDocumentFragment;
import net.sf.vex.dom.IVexElement;
import net.sf.vex.dom.IVexNode;
import net.sf.vex.dom.IVexNonElement;
import net.sf.vex.widget.IVexWidget;

/**
 * Splits the current block element.
 */
public class SplitAction extends AbstractVexAction {

    public void run(final IVexWidget vexWidget) {

		IVexNode node = vexWidget.getCurrentNode();

		//element
		if (node instanceof IVexElement) {

			Styles styles = vexWidget.getStyleSheet().getStyles((IVexElement)node);
			while (!styles.isBlock()) {
				node = ((IVexElement)node).getParent();
				styles = vexWidget.getStyleSheet().getStyles((IVexElement)node);
			}
			splitElement(vexWidget, ((IVexElement)node));
		}

		// comment
		else if (node instanceof IVexNonElement) {
			insertNewLine (vexWidget);
		}
		else {
			// we don't know what to do
			// ...so we better do nothing at all

		}
	}


    private static void insertNewLine (IVexWidget vexWidget) {
    	
    	IVexDocument doc = vexWidget.getDocument();
    	int offset = vexWidget.getCaretOffset();
    	doc.insertText(offset, "\n");
    	vexWidget.moveTo(offset + 1);
    }
    
    /**
	 * Splits the given element.
	 * 
	 * @param vexWidget
	 *            IVexWidget containing the document.
	 * @param element
	 *            Element to be split.
	 */
    public static void splitElement(final IVexWidget vexWidget, final IVexElement element) {
        
        vexWidget.doWork(new Runnable() {
            public void run() {

                long start = System.currentTimeMillis();
                
                Styles styles = vexWidget.getStyleSheet().getStyles(element);
                
                if (styles.getWhiteSpace().equalsIgnoreCase(CSS.PRE)) {
                    // can't call vexWidget.insertText() or we'll get an infinite loop
                	insertNewLine(vexWidget);
                } else {
                    
                    // There may be a number of child elements below the given
                    // element. We cut out the tails of each of these elements
                    // and put them in a list of fragments to be reconstructed when
                    // we clone the element.
                    List children = new ArrayList();
                    List frags = new ArrayList();
                    IVexElement child = vexWidget.getCurrentElement();
                    while (true) {
                        children.add(child);
                        vexWidget.moveTo(child.getEndOffset(), true);
                        frags.add(vexWidget.getSelectedFragment());
                        vexWidget.deleteSelection();
                        vexWidget.moveTo(child.getEndOffset() + 1);
                        if (child == element) {
                            break;
                        }
                        child = child.getParent();
                    }

                    for (int i = children.size() - 1; i >= 0; i--) {
                        child = (IVexElement) children.get(i);
                        IVexDocumentFragment frag = (IVexDocumentFragment) frags.get(i);
                        vexWidget.insertElement((IVexElement) child.clone());
                        int offset = vexWidget.getCaretOffset();
                        if (frag != null) {
                            vexWidget.insertFragment(frag);
                        }
                        vexWidget.moveTo(offset);
                    }
                }

                if (vexWidget.isDebugging()) {
                    long end = System.currentTimeMillis();
                    System.out.println("split() took " + (end - start) + "ms");
                }

            }
            
        });
    }
}
