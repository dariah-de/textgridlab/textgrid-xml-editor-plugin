/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2005 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.action;

import net.sf.vex.core.IntRange;
import net.sf.vex.widget.IVexWidget;

/**
 * Moves the current table row down below its next sibling.
 */
public class MoveRowDownAction extends AbstractVexAction {

    public void run(final IVexWidget vexWidget) {

        final ActionUtils.SelectedRows selected = ActionUtils.getSelectedTableRows(vexWidget);
        
        if (selected.getRows() == null || selected.getRowAfter() == null) {
            return;
        }
        
        vexWidget.doWork(true, new Runnable() {
            public void run() {
                IntRange range = ActionUtils.getOuterRange(selected.getRowAfter());
                vexWidget.moveTo(range.getStart());
                vexWidget.moveTo(range.getEnd(), true);
                vexWidget.cutSelection();
                
                Object firstRow = selected.getRows().get(0);
                vexWidget.moveTo(ActionUtils.getOuterRange(firstRow).getStart());
                vexWidget.paste();
            }
        });
    }

    public boolean isEnabled(IVexWidget vexWidget) {
        ActionUtils.SelectedRows selected = ActionUtils.getSelectedTableRows(vexWidget);
        return selected.getRows() != null && selected.getRowAfter() != null;
    }
}
