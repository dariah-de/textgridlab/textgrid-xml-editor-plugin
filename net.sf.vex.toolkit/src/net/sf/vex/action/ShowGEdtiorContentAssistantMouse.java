/*
 * this class calls the GEditorContentAssistant class on mouse right click  
 * @author Mohamadou Nassourou <mohamadou.nassourou@uni-wuerzburg.de> for TextGrid
 * 
 * 
 */

package net.sf.vex.action;

import net.sf.vex.swt.VexWidget;

public class ShowGEdtiorContentAssistantMouse {

	
    
	public void show(VexWidget vexWidget){
	

		 GEditorContentAssistant ContentWindow = new GEditorContentAssistant(vexWidget);
		 ConfigureFavorite f_elements = new ConfigureFavorite(vexWidget);
		 String[][] data = f_elements.getDocumentElements();
		 ContentWindow.contentAssistMain(vexWidget.getDisplay(), data, "false");
	    
	}
	
}