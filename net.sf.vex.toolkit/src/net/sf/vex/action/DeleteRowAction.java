/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2005 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.action;

import java.util.List;

import net.sf.vex.widget.IVexWidget;

/**
 * Delete selected table rows.
 */
public class DeleteRowAction extends AbstractVexAction {

    public void run(final IVexWidget vexWidget) {

        final List rows = ActionUtils.getSelectedTableRows(vexWidget).getRows();
        
        if (rows == null) {
            return;
        }

        vexWidget.doWork(new Runnable() {
            public void run() {
                int startOffset = ActionUtils.getOuterRange(rows.get(0)).getStart();
                int endOffset = ActionUtils.getOuterRange(rows.get(rows.size() - 1)).getEnd();
                
                vexWidget.moveTo(startOffset);
                vexWidget.moveTo(endOffset, true);
                vexWidget.deleteSelection();
            }
        });

    }

    public boolean isEnabled(IVexWidget vexWidget) {
        return ActionUtils.getSelectedTableRows(vexWidget).getRows() != null;
    }

}
