/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2005 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.action;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import net.sf.vex.dom.IVexElement;
import net.sf.vex.widget.IVexWidget;

/**
 * Inserts a single table column before the current one.
 */
public class InsertColumnBeforeAction extends AbstractVexAction {

    public void run(final IVexWidget vexWidget) {
        
        vexWidget.doWork(new Runnable() {
            public void run() {
                
                final int indexToDup = ActionUtils.getCurrentColumnIndex(vexWidget);
                if (indexToDup == -1) {
                    return;
                }
                
                final List cellsToDup = new ArrayList();
                ActionUtils.iterateTableCells(vexWidget, new TableCellCallback() {
                    public void startRow(Object row, int rowIndex) {
                    }
                    public void onCell(Object row, Object cell, int rowIndex, int cellIndex) {
                        if (cellIndex == indexToDup && cell instanceof IVexElement) {
                            cellsToDup.add(cell);
                        }
                    }
                    public void endRow(Object row, int rowIndex) {
                    }
                });
                
                int finalOffset = -1;
                for (Iterator it = cellsToDup.iterator(); it.hasNext();) {
                    IVexElement element = (IVexElement) it.next();
                    if (finalOffset == -1) {
                        finalOffset = element.getStartOffset() + 1;
                    }
                    vexWidget.moveTo(element.getStartOffset());
                    vexWidget.insertElement((IVexElement) element.clone());
                }
                
                if (finalOffset != -1) {
                    vexWidget.moveTo(finalOffset);
                }

            }
        });
        
    }

    public boolean isEnabled(IVexWidget vexWidget) {
        return ActionUtils.getCurrentColumnIndex(vexWidget) != -1;
    }

}
