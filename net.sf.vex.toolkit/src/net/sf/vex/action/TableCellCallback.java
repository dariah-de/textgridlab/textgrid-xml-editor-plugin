/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2005 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.action;

/**
 * Callback interface called from LayoutUtils.iterateTableCells.
 */
public interface TableCellCallback {

    /**
     * Called before the first cell in a row is visited.
     * 
     * @param row Element or IntRange representing the row.
     * @param rowIndex Zero-based index of the row.
     */
    public void startRow(Object row, int rowIndex);
    
    /**
     * Called when a cell is visited.
     * 
     * @param row Element or IntRange representing the row.
     * @param cell Element or IntRange representing the cell.
     * @param rowIndex Zero-based index of the current row.
     * @param cellIndex Zero-based index of the current cell.
     */
    public void onCell(Object row, Object cell, int rowIndex, int cellIndex);

    /**
     * Called after the last cell in a row is visited.
     * 
     * @param row Element or IntRange representing the row.
     * @param rowIndex Zero-based index of the row.
     */
    public void endRow(Object row, int rowIndex);
    
}
