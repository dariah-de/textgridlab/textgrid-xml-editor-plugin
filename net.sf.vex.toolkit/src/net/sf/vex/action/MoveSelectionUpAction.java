/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2005 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.action;

import net.sf.vex.dom.IVexElement;
import net.sf.vex.layout.BlockBox;
import net.sf.vex.layout.Box;
import net.sf.vex.widget.IBoxFilter;
import net.sf.vex.widget.IVexWidget;

/**
 * Moves the current selection or block element above the previous sibling.
 * WORK IN PROGRESS.
 */
public class MoveSelectionUpAction extends AbstractVexAction {

    public void run(final IVexWidget vexWidget) {

        // First we determine whether we should expand the selection
        // to contain an entire block box.
        
        // Find the lowest block box that completely contains the
        // selection
        Box box = vexWidget.findInnermostBox(new IBoxFilter() {
            public boolean matches(Box box) {
                return box instanceof BlockBox
                && box.getElement() != null
                && box.getStartOffset() <= vexWidget.getSelectionStart()
                && box.getEndOffset() >= vexWidget.getSelectionEnd();
            }
        });
        
        Box[] children = box.getChildren();
        if (children.length > 0 && children[0] instanceof BlockBox) {
            // The found box contains other block children, so we
            // do NOT have to expand the selection
        } else {
            // Expand selection to the containing box
            
            // (Note: This "if" is caused by the fact that getStartOffset is treated
            // differently between elements and boxes. Boxes own their startOffset,
            // while elements don't own theirs. Perhaps we should fix this by having
            // box.getStartOffset() return box.getStartPosition() + 1, but this would
            // be a VERY large change.)
            System.out.println("Box is " + box);
            IVexElement element = box.getElement();
            if (element != null) {
                vexWidget.moveTo(element.getEndOffset());
                vexWidget.moveTo(element.getStartOffset(), true);
                
            } else {
                vexWidget.moveTo(box.getEndOffset());
                vexWidget.moveTo(box.getStartOffset(), true);
            }
        }
        
        final int previousSiblingStart = ActionUtils.getPreviousSiblingStart(vexWidget);
        
//        vexWidget.doWork(new IRunnable() {
//            public void run() throws Exception {
//                vexWidget.cutSelection();
//                vexWidget.moveTo(previousSiblingStart);
//                vexWidget.paste();
//                vexWidget.moveTo(previousSiblingStart, true);
//            }
//        });
        
            
    }

}
