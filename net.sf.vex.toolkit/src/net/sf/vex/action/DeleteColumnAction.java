/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2005 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.action;

import java.util.ArrayList;
import java.util.List;

import net.sf.vex.core.IntRange;
import net.sf.vex.widget.IVexWidget;

/**
 * Delete the table column containing the caret.
 */
public class DeleteColumnAction extends AbstractVexAction {

    public void run(final IVexWidget vexWidget) {
        
        vexWidget.doWork(new Runnable() {
            public void run() {
                
                final ActionUtils.RowColumnInfo rcInfo = ActionUtils.getRowColumnInfo(vexWidget);
                
                if (rcInfo == null) {
                    return;
                }

                final List cellsToDelete = new ArrayList();
                ActionUtils.iterateTableCells(vexWidget, new TableCellCallback() {
                    public void startRow(Object row, int rowIndex) {
                    }
                    public void onCell(Object row, Object cell, int rowIndex, int cellIndex) {
                        if (cellIndex == rcInfo.cellIndex) {
                            cellsToDelete.add(cell);
                        }
                    }
                    public void endRow(Object row, int rowIndex) {
                    }
                });

                // Iterate the deletions in reverse, so that we don't mess up
                // offsets that are in anonymous cells, which are not stored
                // as Positions.
                for (int i = cellsToDelete.size() - 1; i >= 0; i--) {
                    Object cell = cellsToDelete.get(i);
                    IntRange range = ActionUtils.getOuterRange(cell);
                    vexWidget.moveTo(range.getStart());
                    vexWidget.moveTo(range.getEnd(), true);
                    vexWidget.deleteSelection();
                }
            }
        });
        
    }

    public boolean isEnabled(IVexWidget vexWidget) {
        return ActionUtils.getCurrentColumnIndex(vexWidget) != -1;
    }

}
