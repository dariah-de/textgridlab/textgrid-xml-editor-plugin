/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2005 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.action;

import java.util.ArrayList;
import java.util.List;

import net.sf.vex.core.IntRange;
import net.sf.vex.widget.IVexWidget;

/**
 * Moves the current table column to the left.
 */
public class MoveColumnLeftAction extends AbstractVexAction {

    public void run(final IVexWidget vexWidget) {

        final ActionUtils.RowColumnInfo rcInfo = ActionUtils.getRowColumnInfo(vexWidget);

        if (rcInfo == null || rcInfo.cellIndex < 1) {
            return;
        }
        
        vexWidget.doWork(true, new Runnable() {
            public void run() {

                // Cells to the left of the current column
                final List sourceCells = new ArrayList();
                
                // Cells in the current column
                final List destCells = new ArrayList();

                ActionUtils.iterateTableCells(vexWidget, new TableCellCallback() {
                    Object prevCell = null;
                    public void startRow(Object row, int rowIndex) {
                    }
                    public void onCell(Object row, Object cell, int rowIndex, int cellIndex) {
                        if (cellIndex == rcInfo.cellIndex) {
                            sourceCells.add(this.prevCell);
                            destCells.add(cell);
                        } else if (cellIndex == rcInfo.cellIndex - 1) {
                            this.prevCell = cell;
                        }
                    }
                    public void endRow(Object row, int rowIndex) {
                    }
                });

                // Iterate the deletions in reverse, so that we don't mess up
                // offsets that are in anonymous cells, which are not stored
                // as Positions.
                //
                // Also, to preserve the current caret position, we don't cut
                // and paste the current column. Instead, we cut the column
                // to the left of the current column and paste it on the right.
                for (int i = sourceCells.size() - 1; i >= 0; i--) {

                    Object source = sourceCells.get(i);
                    final IntRange sourceRange = ActionUtils.getOuterRange(source);
                    
                    Object dest = destCells.get(i);
                    vexWidget.moveTo(ActionUtils.getOuterRange(dest).getEnd());

                    vexWidget.savePosition(new Runnable() {
                        public void run() {
                            vexWidget.moveTo(sourceRange.getStart());
                            vexWidget.moveTo(sourceRange.getEnd(), true);
                            vexWidget.cutSelection();
                        }
                    });
                    
                    vexWidget.paste();
                }
            }
        });
    }

    public boolean isEnabled(IVexWidget vexWidget) {
        ActionUtils.RowColumnInfo rcInfo = ActionUtils.getRowColumnInfo(vexWidget);
        return rcInfo != null && rcInfo.cellIndex > 0;
    }
}
