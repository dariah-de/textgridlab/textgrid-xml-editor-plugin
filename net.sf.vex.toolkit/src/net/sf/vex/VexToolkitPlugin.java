package net.sf.vex;

import net.sf.vex.dom.linked.AdaptorFactory;

import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.eclipse.ui.statushandlers.StatusManager;

/**
 * The activator is only needed for the {@link AdaptorFactory} to be loaded. Oh
 * yeah, that's what I call documented API, Mr. Eclipse ...
 * 
 * @author tv
 * 
 */
public class VexToolkitPlugin extends AbstractUIPlugin {
	
	private static VexToolkitPlugin instance = null;

	public static final String PLUGIN_ID = "net.sf.vex.toolkit";
	public static final String DEBUG = PLUGIN_ID + "/debug"; 
	public static final String DEBUG_NODEADAPTER = PLUGIN_ID
			+ "/debug/nodeadapter"; 
	public static final String DEBUG_SELECTION = PLUGIN_ID + "/debug/selection"; 
	public static final String DEBUG_SYNCEVENTS = PLUGIN_ID
 + "/debug/syncevents";
	public static final String DEBUG_ANNOTATIONS = PLUGIN_ID + "/debug/annotations";
	public static final String DEBUG_VALIDATE_MODEL = PLUGIN_ID + "/debug/validate-model";

	public VexToolkitPlugin() {
		instance = this;
	}

	public static Status log(int severity, Throwable cause, String message,
			Object... arguments) {
		Status status = new Status(severity, PLUGIN_ID, NLS.bind(message,
				arguments), cause);
		StatusManager.getManager().handle(status, StatusManager.LOG);
		return status;
	}
	
	public static boolean isDebugging(String option) {
		return getDefault().isDebugging()
				&& "true".equals(Platform.getDebugOption(option));
	}

	public static VexToolkitPlugin getDefault() {
		return instance;
	}

}
