package net.sf.vex.dom;


/**
 * @author tv
 *
 */
public interface IVexRootElement extends IVexElement {

	/**
	 * @return The document associated with this element.
	 */
	public IVexDocument getDocument();

	/**
	 * Sets the document to which this element is associated.
	 * This is called by the document constructor, so it need not
	 * be called by client code.
	 * @param document Document to which this root element is
	 * associated.
	 */
	public void setDocument(IVexDocument document);

	public void setContent(Content content, int startOffset, int endOffset);

}