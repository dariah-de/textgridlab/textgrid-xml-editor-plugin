package net.sf.vex.dom;

/**
 * Base type for nodes that are no elements: Comments, CDATA sections
 * 
 * @author <a href="mailto:m.wissenbach@stud.tu-darmstadt.de">Moritz Wissenbach</a>
 * 
 */
public interface IVexNonElement extends IVexNode {

}
