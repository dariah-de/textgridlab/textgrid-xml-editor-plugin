package net.sf.vex.dom;

import java.util.Collection;
import java.util.Iterator;

public interface IVexElement extends IVexNode {

	/**
	 * Adds the given child to the end of the child list. Sets the parent
	 * attribute of the given element to this element.
	 */
	public void addChild(IVexElement child);

	/**
	 * Returns the value of an attribute given its name. If no such attribute
	 * exists, returns null.
	 * 
	 * @param name
	 *            Name of the attribute.
	 */
	public String getAttribute(String name);

	/**
	 * Returns an array of names of the attributes in the element.
	 */
	public String[] getAttributeNames();

	/**
	 * Returns an iterator over the children. Used by
	 * <code>Document.delete</code> to safely delete children.
	 */
	public Iterator getChildIterator();

	/**
	 * Returns an array of the elements children. Implementations
	 * {@linkplain Collection#toArray(Object[]) copy the array} of children, so
	 * if you are free to mess around with the result.
	 * 
	 * @see #getChildIterator()
	 * @see #getChildNodes()
	 */
	public IVexElement[] getChildElements();

	/**
	 * Returns an array of nodes representing the content of this element. The
	 * array includes child elements and runs of text returned as
	 * <code>Text</code> objects.
	 */
	public IVexNode[] getChildNodes();

	/**
	 * @return The document to which this element belongs. Returns null if this
	 *         element is part of a document fragment.
	 */
	public IVexDocument getDocument();

	/**
	 * Returns the name of the element.
	 */
	public String getName();

	/**
	 * Returns the parent of this element, or null if this is the root element.
	 */
	public IVexElement getParent();

	public String getText();

	/**
	 * Returns true if the element has no content.
	 */
	public boolean isEmpty();

	/**
	 * Removes the given attribute from the array.
	 * 
	 * @param name
	 *            name of the attribute to remove.
	 */
	public void removeAttribute(String name) throws DocumentValidationException;

	/**
	 * Sets the value of an attribute for this element.
	 * 
	 * @param name
	 *            Name of the attribute to be set.
	 * @param value
	 *            New value for the attribute. If null, this call has the same
	 *            effect as removeAttribute(name).
	 */
	public void setAttribute(String name, String value)
			throws DocumentValidationException;

	/**
	 * Sets the parent of this element.
	 * 
	 * @param parent
	 *            Parent element.
	 */
	public void setParent(IVexElement parent);

	public String toString();

	public IVexElement clone();

	/**
	 * Internal method to insert the given element as a child at the given child
	 * index. Sets the parent attribute of the given element to this element.
	 * 
	 * <p>
	 * <strong>This method is not API. It may only be used internally inside a
	 * DOM Operation.</strong> Clients should use
	 * {@link net.sf.vex.dom.IVexDocument#insertElement(int, IVexElement)}
	 * instead.
	 * 
	 * 
	 * @param index
	 * @param newElement
	 */
	public void internalInsertChild(int index, IVexElement newElement);

	public boolean isPre();
}