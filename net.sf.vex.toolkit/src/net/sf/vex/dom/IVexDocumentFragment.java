package net.sf.vex.dom;


public interface IVexDocumentFragment {

	/** 
	 * Mime type representing document fragments: 
	 * "text/x-vex-document-fragment" 
	 */
	public static final String MIME_TYPE = "application/x-vex-document-fragment";

	/**
	 * Returns the Content object holding this fragment's content.
	 */
	public Content getContent();

	/**
	 * Returns the number of characters, including sentinels, represented
	 * by the fragment.
	 */
	public int getLength();

	/**
	 * Returns the elements that make up this fragment.
	 */
	public IVexElement[] getElements();

	/**
	 * Returns an array of element names and Validator.PCDATA representing
	 * the content of the fragment.
	 */
	public String[] getNodeNames();

	/**
	 * Returns the nodes that make up this fragment, including 
	 * elements and <code>Text</code> objects.
	 */
	public IVexNode[] getNodes();

}