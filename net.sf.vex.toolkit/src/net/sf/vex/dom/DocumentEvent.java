/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2004 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.dom;

import java.util.EventObject;

import net.sf.vex.undo.IUndoableEdit;

/**
 * Encapsulation of the details of a document change
 */
public class DocumentEvent extends EventObject {

    private IVexDocument document;
    private IVexElement parentElement;
    private int offset;
    private int length;
    private String attributeName;
    private String oldAttributeValue;
    private String newAttributeValue;
    private IUndoableEdit undoableEdit;

    /**
     * Class constructor.
     *
     * @param document Document that changed.
     * @param parentElement Element containing the change.
     * @param offset offset at which the change occurred.
     * @param length length of the change.
     * @param undoableEdit IUndoableEdit that can be used to undo the change.
     */
    public DocumentEvent(IVexDocument document,
                         IVexElement parentElement,
			 int offset,
			 int length,
                         IUndoableEdit undoableEdit) {

        super(document);
        this.document = document;
	this.parentElement = parentElement;
	this.offset = offset;
	this.length = length;
	this.undoableEdit = undoableEdit;
    }


    /**
     * Class constructor used when firing an attributeChanged event.
     * 
     * @param document Document that changed.
     * @param parentElement element containing the attribute that
     * changed
     * @param attributeName name of the attribute that changed
     * @param oldAttributeValue value of the attribute before the
     * change.
     * @param newAttributeValue value of the attribute after the change.
     * @param undoableEdit IUndoableEdit that can be used to undo the change.
     */
    public DocumentEvent(IVexDocument document,
                         IVexElement parentElement,
                         String attributeName,
                         String oldAttributeValue,
                         String newAttributeValue,
                         IUndoableEdit undoableEdit) {
    
                             super(document);
        this.document = document;
        this.parentElement = parentElement;
        this.attributeName = attributeName;
        this.oldAttributeValue = oldAttributeValue;
        this.newAttributeValue = newAttributeValue;                         
        this.undoableEdit = undoableEdit;
    }
    
    /**
     * Returns the length of the change.
     */
    public int getLength() {
	return this.length;
    }

    /**
     * Returns the offset at which the change occurred.
     */
    public int getOffset() {
	return this.offset;
    }

    /**
     * Returns the element containing the change.
     */
    public IVexElement getParentElement() {
	return this.parentElement;
    }
    /**
     * @return the value of the attribute before the change.
     * If null, indicates that the attribute was removed.
     */
    public String getNewAttributeValue() {
        return newAttributeValue;
    }

    /**
     * @return the value of the attribute after the change.
     * If null, indicates the attribute did not exist before
     * the change.
     */
    public String getOldAttributeValue() {
        return oldAttributeValue;
    }

    /**
     * @return the name of the attribute that was changed.
     */
    public String getAttributeName() {
        return attributeName;
    }

    /**
     * @return the document for which this event was generated
     */
    public IVexDocument getDocument() {
        return document;
    }

    /**
     * Returns the undoable edit that can be used to undo the action.
     * May be null, in which case the action cannot be undone.
     */
    public IUndoableEdit getUndoableEdit() {
        return undoableEdit;
    }
}
