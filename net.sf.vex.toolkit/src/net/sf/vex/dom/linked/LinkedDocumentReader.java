/**
 * LinkedDocumentReader.java, part of project vex-toolkit
 * 
 * (C) 2008 by Thorsten Vitt for TextGrid
 */
package net.sf.vex.dom.linked;

import java.text.MessageFormat;
import java.util.LinkedList;
import java.util.Queue;

import net.sf.vex.dom.Content;
import net.sf.vex.dom.DocumentReader;
import net.sf.vex.dom.IVexDocument;
import net.sf.vex.dom.IVexElement;
import net.sf.vex.dom.IVexNode;
import net.sf.vex.dom.IVexText;
import net.sf.vex.dom.IWhitespacePolicy;
import net.sf.vex.dom.IWhitespacePolicyFactory;
import net.sf.vex.dom.impl.SwingGapContentWrapper;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jface.text.IDocument;
import org.eclipse.wst.sse.core.StructuredModelManager;
import org.eclipse.wst.sse.core.internal.provisional.IModelManager;
import org.eclipse.wst.sse.core.internal.provisional.IStructuredModel;
import org.eclipse.wst.sse.core.internal.provisional.IndexedRegion;
import org.eclipse.wst.sse.core.internal.provisional.text.IStructuredDocument;
import org.eclipse.wst.xml.core.internal.provisional.document.IDOMDocument;
import org.eclipse.wst.xml.core.internal.provisional.document.IDOMModel;
import org.w3c.dom.Attr;
import org.w3c.dom.CDATASection;
import org.w3c.dom.Comment;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

/**
 * This is a {@link DocumentReader} that builds a Vex data model linked with a
 * WST {@link IDocument}.
 * 
 * <p>
 * <strong>LinkedDocumentReaders are throwaway classes</strong>, you may only
 * read one document using one reader.
 * </p>
 * 
 * <h3>Implementation Notes</h3>
 * 
 * <p>
 * Basically, our task is to build a hierarchy of {@link IVexNode}s where every
 * {@link IVexNode} is linked to a corresponding DOM {@link Node} (which has
 * been ripped off the source editor).
 * </p>
 * 
 * <p>
 * The basic problem is that we need to have a {@link Content} structure
 * representing the <em>text content</em> only. This is needed for the
 * visualization and editing features of our WYSIWYM editor.
 * </p>
 * 
 * <p>
 * {@link #read(IDOMModel)} is the entry point for creating such a structure in
 * the first place.
 * 
 * It retrieves the {@link IDOMDocument} from the {@link IDOMModel}, creates a
 * matching {@link LinkedDocument} (i.e. a{@link IVexDocument)} and the
 * corresponding {@link Content} class.
 * 
 * The rest of the structure is built using the addChildren and the various
 * <code>add</code> methods, each of which gets the parent of the combined
 * model and the child of the source (i.e. DOM) model.
 * </p>
 * 
 * 
 * @author Thorsten Vitt <vitt@linglit.tu-darmstadt.de> (tv) for TextGrid
 * 
 * 
 */
@SuppressWarnings("restriction")
public class LinkedDocumentReader extends DocumentReader {

	
	/**
	 * Runtime exception thrown when the user cancels reading a document. 
	 * 
	 * @author tv
	 *
	 */
	public class ReadCanceledException extends RuntimeException {

		private Node node;

		public ReadCanceledException(Node node) {
			super(MessageFormat.format("Read canceled while reading node {0}", node));
			this.node = node;
		}

		public ReadCanceledException() {
			super("Read canceled.");
		}

		public Node getNode() {
			return node;
		}

	}

	private LinkedDocument doc;
	private IDOMDocument document;
	private Content content = new SwingGapContentWrapper(100);
	private IWhitespacePolicy whitespacePolicy;
	
	
	private SubMonitor progress = null;
	private int lastProgress = 0;
	
	/**
	 * Report progress to our user.
	 * 
	 * @param node The node that is about to be parsed.
	 * @throws ReadCanceledException when the user cancels reading the document.
	 */
	protected void reportProgress(final Node node) throws ReadCanceledException {
		if (progress != null) {
			if (progress.isCanceled())
				throw new ReadCanceledException(node);
			if (node instanceof IndexedRegion) {
				int startOffset = ((IndexedRegion) node).getStartOffset();
				progress.worked(startOffset - lastProgress);
				lastProgress = startOffset;
			}
		}
	}

	/**
	 * Reports progress, if a progress monitor is configured.
	 * @param worked the amount of work to report
	 * @throws ReadCanceledException if the user has canceled reading.
	 * @see IProgressMonitor#worked(int)
	 */
	private void reportProgress(int worked) throws ReadCanceledException {
		if (progress != null) {
			if (progress.isCanceled())
				throw new ReadCanceledException();
			progress.worked(worked);
		}
	}

	/**
	 * Reads a document, reporting progress to the given progress monitor.
	 * 
	 * @param domModel
	 *            The DOM model that should be parsed
	 * @param monitor
	 *            a progress monitor to report progress to
	 * @return a new Vex {@link LinkedDocument} that is linked to the given
	 *         model
	 * @throws ReadCanceledException
	 * 			when the user cancels the reading process using the monitor.
	 * @see {@link #read(IDOMModel)}
	 */
	public LinkedDocument read(final IDOMModel domModel, final IProgressMonitor monitor) throws  ReadCanceledException {
		progress = SubMonitor.convert(monitor);
		LinkedDocument readDocument = read(domModel);
		progress.done();
		progress = null;
		return readDocument;
	}
	
	public LinkedDocument read(IDOMModel domModel) {

		document = domModel.getDocument();
		if (progress != null) {
			progress.setWorkRemaining(document.getLength() + 100);
		}
		
		if (document.getDocumentElement() == null) {
			Element newRoot = document.createElement("TEI");
			document.appendChild(newRoot);
		}
		
		reportProgress(25);

		doc = new LinkedDocument(document);
		doc.setPublicID(document.getDocumentTypeId());

		// side effect: set the document style
		whitespacePolicy = getWhitespacePolicyFactory().getPolicy(
				doc.getPublicID());

		doc.setWhitespacePolicy(whitespacePolicy);
		
		reportProgress(25);
		
		appendContent("\0");

		addChildren(doc.getRootElement(), document.getDocumentElement()
				.getChildNodes());
		
		reportProgress(10);

		// doc.getRootElement().setContent(content, 0, appendContent("\0") - 1);
		scheduleSetContent(doc.getRootElement(), content, 1,
				appendContent("\0"));
		doc.setContent(content);
		
		reportProgress(10);
							
		if(document.getSource().indexOf("stylesheet type=\"text/css\"") !=-1){
			// FIXME use standard API?
			int beg_css = document.getSource().indexOf("stylesheet type=\"text/css\"");
		 	int st_css = document.getSource().indexOf("href=\"",beg_css);
		 	int end_css = document.getSource().indexOf("\"",st_css+6);
		 	String cssfile_location = document.getSource().substring(st_css+6, end_css); 
			//System.out.println(cssfile_location);
		 	doc.setCSSfileLocation(cssfile_location);
		} 
		
		reportProgress(15);
		
		flushSetContent();
		
		reportProgress(20);

		return doc;
	}

	protected class SetContentCache {
		/**
		 * @param content
		 * @param end
		 * @param node
		 * @param start
		 */
		protected SetContentCache(IVexNode node, Content content, int start,
				int end) {
			super();
			this.content = content;
			this.end = end;
			this.node = node;
			this.start = start;
		}

		IVexNode node;
		Content content;
		int start;
		int end;
	}

	protected Queue<SetContentCache> cache = new LinkedList<SetContentCache>();

	/**
	 * Schedules a call to the
	 * {@linkplain IVexNode#setContent(Content, int, int) <var>node</var>'s
	 * setContent} method.
	 * 
	 * <p>
	 * After adding a node while parsing a document, we need to append the
	 * contained text to the {@link Content} object and afterwards call the
	 * node's setContent method. Unfortunately, when we call
	 * {@link IVexNode#setContent(Content, int, int)} with the end parameter set
	 * to {@link Content#getLength()}, this would create a position at the
	 * Content's end which would move with the next insertion, causing all
	 * following nodes
	 * 
	 * @see IVexNode#setContent(Content, int, int)
	 * @see #flushSetContent()
	 */
	protected void scheduleSetContent(IVexNode node, Content content,
			int start, int end) {
		cache.add(new SetContentCache(node, content, start, end));
	}

	/**
	 * Performs all currently cached setContents operations.
	 * 
	 * @see scheduleSetContent
	 */
	private void flushSetContent() {
		for (SetContentCache sc = cache.poll(); sc != null; sc = cache.poll())
			sc.node.setContent(sc.content, sc.start, sc.end);
	}

	protected void addChildren(IVexNode parent, NodeList children) {
		for (int i = 0; i < children.getLength(); i++) {
			add(parent, children.item(i));
		}
	}

	protected IVexNode add(IVexNode parent, Node item) {
		
		reportProgress(item);

		// This should have worked with overloading, but it didn't. So we do
		// it manually.
		if (item instanceof Element)
			return addElement(parent, (Element) item);
		else if (item instanceof Attr)
			addAttr(parent, (Attr) item);
		else if (item instanceof Comment)
			return addComment(parent, (Comment) item);
		// watch out, CDATASectionImpl is also a subtype of Text, so order is
		// important here
		else if (item instanceof CDATASection)
			return addCDATASection(parent, (CDATASection) item);
		else if (item instanceof Text)
			return addText(parent, (Text) item);

		// else if (item instanceof ProcessingInstruction)
		// // TODO: Implement
		// ;
		// else if (item instanceof EntityReference)
		// // TODO: implement
		// ;
		else {

			LinkedModelException.handleError(
					"Adding a node is too abstract for me.\n"
							+ "Please write an add method for {0}s, thanks.\n"
							+ "(Offending node: {1}, Parent: {2})", null, item
							.getClass(), item, parent);

			// FIXME: Trying to recover …
			addChildren(parent, item.getChildNodes());
		}

		return null;
	}

	protected IVexElement addElement(IVexNode parent, Element element) {
		LinkedElement linkedElement = new LinkedElement(parent, element);
		((IVexElement) parent).addChild(linkedElement);
		int start = appendContent("\0") - 1;
		addChildren(linkedElement, element.getChildNodes());
		int end = appendContent("\0") - 1; // ???
		// linkedElement.setContent(content, start, end - 1);
		scheduleSetContent(linkedElement, content, start, end); // -1?
		return linkedElement;
	}

	/**
	 * Appends some string to the current content object. May only be called
	 * during {@link #read(IDOMModel)}.
	 * 
	 * @param string
	 *            The string to append
	 * @return The content's new length
	 */
	private int appendContent(String string) {
		content.insertString(content.getLength(), string);
		int length = content.getLength();
		return length;
	}

	protected void addAttr(IVexNode parent, Attr attribute) {
		if (parent instanceof LinkedElement) {
			LinkedElement element = (LinkedElement) parent;
			element.setAttribute(attribute.getName(), attribute
					.getTextContent());
			// FIXME loses attribute order. This may not be such a good idea,
			// but we need to fix the attribute editor.
		}
		// else sth went wrong.
	}

	protected IVexText addText(IVexNode parent, Text text) {
		LinkedText linkedText = new LinkedText(parent, text, content);

		int start = content.getLength();
		int end = appendContent(linkedText.getWSNormalizedString()); // ???
		scheduleSetContent(linkedText, content, start, end);

		((LinkedElement) parent).addChild(linkedText);

		return linkedText;
	}

	protected LinkedComment addComment(IVexNode parent, Comment item) {

		LinkedComment linkedComment = new LinkedComment(item);
		((LinkedElement) parent).addChild(linkedComment);

		int start = appendContent("\0") - 1;
		appendContent(item.getData());
		int end = appendContent("\0") - 1; // ???
		scheduleSetContent(linkedComment, content, start, end); // -1?
		return linkedComment;

	}

	protected LinkedCDATASection addCDATASection(IVexNode parent,
			CDATASection item) {

		LinkedCDATASection linkedCS = new LinkedCDATASection(item);
		((LinkedElement) parent).addChild(linkedCS);

		int start = appendContent("\0") - 1;
		appendContent(item.getData());
		int end = appendContent("\0") - 1; // ???
		scheduleSetContent(linkedCS, content, start, end); // -1?
		return linkedCS;

	}

	/**
	 * Creates a new VexDocument based on the given IDocument
	 * 
	 * @param document
	 *            the input document. Must be an {@link IStructuredDocument}.
	 * @param wsFactory
	 *            the whitespace factory to use for creating the document.
	 * @return a new {@link IVexDocument}. Will be a {@link LinkedDocument}.
	 * @throws IllegalArgumentException
	 *             if we fail to get a {@link IDOMModel} for the input document.
	 * @deprecated Use {@link #createVexDocument(IDocument,IWhitespacePolicyFactory,IProgressMonitor)} instead
	 */
	public static LinkedDocument createVexDocument(final IDocument document, final IWhitespacePolicyFactory wsFactory)
			throws IllegalArgumentException {
				return createVexDocument(document, wsFactory, null);
			}

	/**
	 * Creates a new VexDocument based on the given IDocument
	 * 
	 * @param document
	 *            the input document. Must be an {@link IStructuredDocument}.
	 * @param wsFactory
	 *            the whitespace factory to use for creating the document.
	 * @param monitor a progress monitor to use.
	 * @return a new {@link IVexDocument}. Will be a {@link LinkedDocument}.
	 * @throws IllegalArgumentException
	 *             if we fail to get a {@link IDOMModel} for the input document.
	 */
	public static LinkedDocument createVexDocument(final IDocument document, final IWhitespacePolicyFactory wsFactory, final IProgressMonitor monitor)
			throws IllegalArgumentException {
		final IModelManager modelManager = StructuredModelManager.getModelManager();
		final IDOMModel domModel;
	
		if (document instanceof IStructuredDocument) {
		
			final IStructuredDocument sdocument = (IStructuredDocument) document;
			final IStructuredModel modelForRead = modelManager.getModelForRead(sdocument);
			if (modelForRead instanceof IDOMModel) {
								
				domModel = (IDOMModel) modelForRead;
				final LinkedDocument vexDocument;
				try {
					final LinkedDocumentReader reader = new LinkedDocumentReader();
					reader.setWhitespacePolicyFactory(wsFactory);
					vexDocument = reader.read(domModel, monitor);
				} finally {
					domModel.releaseFromRead();
				}
				return vexDocument;
			} else
				throw new IllegalArgumentException(
						MessageFormat.format(
								"Cannot create linked document: Input document {0} must be associated with an IDOMModel (associated model: {1}",
								document, modelForRead));
		} else
			throw new IllegalArgumentException(MessageFormat.format(
					"Cannot create linked document: Input document {0} must be an IStructuredDocument.", document));
	}

}
