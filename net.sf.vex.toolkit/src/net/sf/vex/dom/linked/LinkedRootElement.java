package net.sf.vex.dom.linked;

import net.sf.vex.dom.IVexDocument;
import net.sf.vex.dom.IVexRootElement;

import org.w3c.dom.Element;

public class LinkedRootElement extends LinkedElement implements IVexRootElement {

	private LinkedDocument document;

	public LinkedRootElement(Element domElement) {
		super(domElement);
	}
	
	public void setDocument(IVexDocument document) {
		this.document = (LinkedDocument) document;
	}

	@Override
	public LinkedDocument getDocument() {
		return document;
	}

}
