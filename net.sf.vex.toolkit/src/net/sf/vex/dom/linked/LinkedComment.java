package net.sf.vex.dom.linked;

import static net.sf.vex.VexToolkitPlugin.DEBUG_SELECTION;

import java.util.Iterator;

import net.sf.vex.VexToolkitPlugin;
import net.sf.vex.dom.IVexComment;
import net.sf.vex.dom.IVexNode;

import org.w3c.dom.Comment;

/**
 * Represents a comment in the DOM part of the data model. This is not really
 * represented in the original Vex model.
 * 
 * @author Thorsten Vitt <vitt@linglit.tu-darmstadt.de> for TextGrid
 * 
 */

public class LinkedComment extends LinkedNonElement implements IVexComment {

	public LinkedComment(Comment domNode) {
		super(domNode);
	}

	public int getCorrectionForSourcePos() {
		return -3;
	}

	public int getCorrectionForContentPos() {
		return 3;
	}

}
