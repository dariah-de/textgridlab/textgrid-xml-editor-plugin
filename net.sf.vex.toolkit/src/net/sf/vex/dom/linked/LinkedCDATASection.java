package net.sf.vex.dom.linked;

import static net.sf.vex.VexToolkitPlugin.DEBUG_SELECTION;

import java.util.Iterator;

import net.sf.vex.VexToolkitPlugin;
import net.sf.vex.dom.IVexCDATASection;
import net.sf.vex.dom.IVexComment;
import net.sf.vex.dom.IVexNode;

import org.w3c.dom.CDATASection;
import org.w3c.dom.Comment;

/**
 * Represents a CDATA section in the DOM part of the data model.
 * 
 */

public class LinkedCDATASection extends LinkedNonElement implements IVexCDATASection{

	public LinkedCDATASection(CDATASection domNode) {
		super(domNode);
	}

	public int getCorrectionForSourcePos() {
		//TODO evaluate 
		return -7;
	}

	public int getCorrectionForContentPos() {
		//TODO evaluate
		return 7;
	}

}
