package net.sf.vex.dom.linked;

import static net.sf.vex.VexToolkitPlugin.DEBUG_NODEADAPTER;
import static net.sf.vex.VexToolkitPlugin.DEBUG_SYNCEVENTS;
import static net.sf.vex.VexToolkitPlugin.isDebugging;
import info.textgrid.lab.core.swtutils.AdapterUtils;
import info.textgrid.util.collection.Filter;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import net.sf.vex.VexToolkitPlugin;
import net.sf.vex.dom.DocumentValidationException;
import net.sf.vex.dom.IVexElement;
import net.sf.vex.dom.IVexNode;
import net.sf.vex.dom.IVexText;
import net.sf.vex.dom.IWhitespacePolicy;
import net.sf.vex.dom.impl.WrongModelException;
import net.sf.vex.dom.linked.LinkedDocument.ModelError;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.ui.statushandlers.StatusManager;
import org.eclipse.wst.sse.core.internal.provisional.INodeNotifier;
import org.eclipse.wst.sse.core.internal.provisional.IndexedRegion;
import org.w3c.dom.Attr;
import org.w3c.dom.CDATASection;
import org.w3c.dom.Comment;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

import com.google.common.base.Joiner;

@SuppressWarnings("restriction")
public class LinkedElement extends LinkedNode implements IVexElement {

	/**
	 * @category experimental
	 */
	public boolean equals(Object obj) {
		boolean result = super.equals(obj);
		if (result) {
			if (obj instanceof LinkedElement) {
				LinkedElement other = (LinkedElement) obj;
				return (getName().equals(other.getName())) && getDomNode().equals(other.getDomNode());
			} else
				return false;
		}
		return result;
	}

	@Override
	public int hashCode() {
		return super.hashCode() ^ getName().hashCode();
	}

	private List<IVexNode> children = Collections
			.synchronizedList(new ArrayList<IVexNode>());
	private Filter<IVexElement> childElements = new Filter<IVexElement>(
			children, IVexElement.class);

	@Override
	public IVexElement clone() {
		return new LinkedElement(getDomNode().cloneNode(false));
	}

	@Override
	public Element getDomNode() {
		return (Element) super.getDomNode();
	}

	public LinkedElement(Node domNode) {
		super(domNode);

	}

	public LinkedElement(IVexNode parent, Element element)
			throws LinkedModelException {
		this(element);
		if (parent instanceof IVexElement)
			setParent((IVexElement) parent);
		else
			throw new LinkedModelException(
					"Could not create a Vex element from {0}: The provided parent, {1}, is not an IVexElement",
					null, element, parent);
	}

	public void addChild(IVexNode child) {
		WrongModelException.throwIfNeeded(child, LinkedNode.class);

		children.add(child);
		((LinkedNode) child).setParent(this);
	}

	public String getAttribute(String name) {
		Attr attributeNode = getDomNode().getAttributeNode(name);
		return attributeNode == null ? null : attributeNode.getValue();
	}

	public String[] getAttributeNames() {
		NamedNodeMap attributes = getDomNode().getAttributes();
		String[] names = new String[attributes.getLength()];
		for (int i = 0; i < names.length; i++)
			names[i] = attributes.item(i).getNodeName();
		return names;
	}

	public IVexElement[] getChildElements() {
		List<IVexElement> elements = new LinkedList<IVexElement>();
		for (IVexNode child : children) {
			if (child instanceof IVexElement) {
				elements.add((IVexElement) child);
			}
		}
		return elements.toArray(new IVexElement[0]);
	}

	/**
	 * Returns an {@link Iterator} over the child <em>elements</em>. To
	 * iterate over child nodes, you may use {@link #getChildNodeIterator()}.
	 * 
	 * @see net.sf.vex.dom.IVexElement#getChildIterator()
	 * @see #getChildNodeIterator()
	 */
	public Iterator<IVexElement> getChildIterator() {
		return childElements.iterator();
	}

	public Iterator<IVexNode> getChildNodeIterator() {
		return children.iterator();
	}

	public IVexNode[] getChildNodes() {
		return children.toArray(new IVexNode[0]);
	}

	@Override
	public LinkedDocument getDocument() {
		IVexElement root = this;
		while (root.getParent() != null) {
			root = root.getParent();
		}
		if (root instanceof LinkedRootElement)
			return ((LinkedRootElement) root).getDocument();
		else
			return null;
	}

	public String getName() {
		return getDomNode().getTagName();
	}

	@Override
	public LinkedElement getParent() {
		return (LinkedElement) super.getParent();
	}

	public boolean isEmpty() {
		return getDomNode().getChildNodes().getLength() == 0;
	}

	public void removeAttribute(String name) throws DocumentValidationException {
		getDomNode().getAttributes().removeNamedItem(name);
	}

	public void setAttribute(String name, String value)
			throws DocumentValidationException {
		getDomNode().setAttribute(name, value);
	}

	/**
	 * INTERNAL – inserts the element at the given index in the child nodes
	 * list.
	 * 
	 * @see net.sf.vex.dom.IVexElement#internalInsertChild(int,
	 *      net.sf.vex.dom.IVexElement)
	 */
	public void internalInsertChild(int index, IVexElement newElement) {
		// TODO avoid vicious circle after adding sync from DOM node
		if (index < children.size()) {
			getDomNode().insertBefore(
					((LinkedElement) newElement).getDomNode(),
					((LinkedNode) children.get(index)).getDomNode());
		} else {
			getDomNode().appendChild(((LinkedNode) newElement).getDomNode());
		}
		children.add(index, newElement);
		newElement.setParent(this);
	}

	public void addChild(IVexElement child) {
		children.add(child);
		child.setParent(this);
	}

	@Override
	public boolean hasChildren() {
		return !children.isEmpty();
	}

	@Override
	public boolean hasValidContent() {
		boolean valid = true;
		if (!"\0".equals(getContent().getString(getStartOffset(), 1))) {
			valid = false;
			System.err
					.println("Element's Content must start with \\0! " + this);
		}

		for (IVexNode child : children) {
			valid &= ((LinkedNode) child).hasValidContent();
		}

		if (getEndOffset() < getContent().getLength()
				&& !"\0".equals(getContent().getString(getEndOffset(), 1))) {
			valid = false;
			System.err.println("Element's Content must end with \\0! " + this);
		}
		return valid;
	}

	public boolean isPre() {
		IWhitespacePolicy policy = getDocument().getWhitespacePolicy();
		if (policy != null)
			return policy.isPre(this);
		else
			return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.sf.vex.dom.linked.LinkedNode#notifyChanged(org.eclipse.wst.sse.core
	 *      .internal.provisional.INodeNotifier, int, java.lang.Object,
	 *      java.lang.Object, java.lang.Object, int)
	 */
	@Override
	public void notifyChanged(INodeNotifier notifier, int eventType,
			Object changedFeature, Object oldValue, Object newValue, int pos) {
		

		if (isDebugging(DEBUG_NODEADAPTER)) {
			// getDocument().printDocument();
			System.out.println("Processing: "
					+ formatNotification(notifier, eventType, changedFeature,
							oldValue, newValue, pos));
		}
		switch (eventType) {
		case INodeNotifier.REMOVE:
			removeDomChild((Node) changedFeature);
			break;
		case INodeNotifier.ADD:
			if (newValue instanceof Node) {
				addDomChild((Node) newValue);
			}
			break;
		case INodeNotifier.CONTENT_CHANGED:
			getDomNode().normalize(); // joins subsequent Text nodes
			break;
				
		default:
			super.notifyChanged(notifier, eventType, changedFeature, oldValue, newValue, pos);
			break;
		}

		if (VexToolkitPlugin.isDebugging(VexToolkitPlugin.DEBUG_VALIDATE_MODEL)) {
			LinkedList<ModelError> errors = new LinkedList<LinkedDocument.ModelError>();
			if (!LinkedDocument.isSubtreeConsistent(getParent(), errors, false)) {
				StatusManager.getManager().handle(new Status(IStatus.ERROR, 
						VexToolkitPlugin.PLUGIN_ID, 
						"Part of the linked document is broken: \n" +  
							Joiner.on("\n").join(errors) 
								+ ".\n   Detected after processing this model change: " + 
								formatNotification(notifier, eventType, changedFeature, oldValue, newValue, pos)));
			}
		}
	}

	/**
	 * Removes the child corresponding to the given DOM node.
	 * 
	 * @param node
	 *            the W3C DOM node to match
	 * @throws IllegalArgumentException
	 *             if there is no matching child for node
	 */
	protected void removeDomChild(Node node) {
		Iterator<IVexNode> iterator = children.iterator();
		while (iterator.hasNext()) {
			LinkedNode candidate = (LinkedNode) iterator.next();
			if (candidate.getDomNode() == node) {

				iterator.remove();

				int startOffset = candidate.getStartOffset();
				int contentLength = candidate.getEndOffset() - startOffset;

				if (candidate instanceof LinkedElement)
					contentLength++; // delete the following '\0'

				if (VexToolkitPlugin.isDebugging(DEBUG_NODEADAPTER))
					System.err.println(MessageFormat.format(
							"Removing ''{0}'' from {1}''s content",
							getContent().getString(startOffset, contentLength)
									.replace("\0", "\\0"), this));

				if (contentLength > 0)
					candidate.getContent().remove(startOffset, contentLength);
				if (VexToolkitPlugin.isDebugging(DEBUG_NODEADAPTER))
					System.err.println("  ... which resulted in " + this);
				
				if (VexToolkitPlugin.isDebugging(DEBUG_SYNCEVENTS))
					System.out.println(MessageFormat.format(
							"deleted child {1} from {0}", this, node));
					
				return;

			}
		}
		throw new IllegalArgumentException(MessageFormat.format(
				"Cannot remove {0}: No corresponding child found in {1}", node,
				this));
	}

	/**
	 * Adds a new DOM child to this node, ie, create corresponding
	 * {@link LinkedNode}s for the given DOM child node.
	 * <p>
	 * This method is used for tracking changes in the linked W3C DOM. Don't
	 * call it from outside
	 * {@linkplain #notifyChanged(INodeNotifier, int, Object, Object, Object, int)
	 * notifyChanged}.
	 * </p>
	 * 
	 * @param node
	 *            The DOM node to add. If it's an element and it has children,
	 *            corresponding LinkedNodes are created for the children, as
	 *            well.
	 * @return the newly created linked node for <var>node</var>.
	 */
	protected LinkedNode addDomChild(Node node) {

		switch (node.getNodeType()) {
		case Node.ELEMENT_NODE:
			return addDomChildElement((Element) node);
			// Order is important
		case Node.CDATA_SECTION_NODE:
			return addDomChildCDATASection((CDATASection) node);
		case Node.TEXT_NODE:
			return addDomChildText((Text) node);
		case Node.COMMENT_NODE:
			return addDomChildComment((Comment) node);
		default:
			return addDomChildGeneric(node);
		}

	}

	private LinkedNode addDomChildGeneric(Node node) {
		VexToolkitPlugin
				.log(
						IStatus.WARNING,
						new IllegalArgumentException(
								"addDomChild called with unknown node type."),
						"I don't know what corresponding LinkedNode to create for the {0} {1} has been added in the source code editor.",
						node.getClass().getSimpleName(), node);
		if (isDebugging(DEBUG_SYNCEVENTS)) {
			System.err.println(MessageFormat.format(
					"don't know how to add {1} to {0}", this, node));
		}
		return null;
	}

	private LinkedNode addDomChildComment(Comment node) {

		int position = findInsertPositionFor(node); // FIXME does this work?
		LinkedComment linkedComment = new LinkedComment(node);

		int startOffset;

		if (position == 0)
			startOffset = this.getStartOffset();
		else {
			startOffset = children.get(position - 1).getEndOffset();
		}

		String string = linkedComment.getDomNode().getData();

		getContent().insertString(startOffset + 1, "\0\0");

		getContent().insertString(startOffset + 2, string);

		linkedComment.setContent(this.getContent(), startOffset + 1,
				startOffset + 2 + string.length());

		children.add(position, linkedComment);

		linkedComment.setParent(this);
		
		if (isDebugging(DEBUG_SYNCEVENTS))
			System.out.println(MessageFormat.format("added comment {1} to {0}",
					this, linkedComment));

		return linkedComment;

	}

	private LinkedNode addDomChildCDATASection(CDATASection node) {
		int position = findInsertPositionFor(node); // FIXME does this work?
		LinkedCDATASection linkedCS = new LinkedCDATASection(node);

		int startOffset;

		if (position == 0)
			startOffset = this.getStartOffset();
		else {
			startOffset = children.get(position - 1).getEndOffset();
		}

		String string = linkedCS.getDomNode().getData();

		getContent().insertString(startOffset + 1, "\0\0");

		getContent().insertString(startOffset + 2, string);

		linkedCS.setContent(this.getContent(), startOffset + 1, startOffset + 2
				+ string.length());

		children.add(position, linkedCS);

		linkedCS.setParent(this);

		return linkedCS;

	}

	private LinkedNode addDomChildText(Text node) {
		int position = findInsertPositionFor(node);
		LinkedText linkedText = new LinkedText(this, node, getContent());

		int startOffset = findStartOffsetForPosition(position);

		String normalizedString = linkedText.getWSNormalizedString();
		getContent().insertString(startOffset, normalizedString);
		linkedText.setContent(getContent(), startOffset, startOffset
				+ normalizedString.length());

		children.add(position, linkedText);
		
		if (isDebugging(DEBUG_SYNCEVENTS))
			System.out.println(MessageFormat.format("added text {1} to {0}",
					this,
					linkedText));
		
		return linkedText;
	}

	/**
	 * Finds the start offset (in the {@linkplain #getContent() content}) for a
	 * new child to be inserted at the given position
	 * 
	 * @param position
	 *            the future index of the new node in {@link #children}
	 * @return
	 */
	private int findStartOffsetForPosition(int position) {
		int offset;
		if (position == 0)
			offset = getStartOffset() + 1;
		else {
			IVexNode previousSibling = children.get(position - 1);
			offset = previousSibling.getEndOffset() + 1;
			if (previousSibling instanceof IVexText)
				--offset; // Text's end position points after the content
		}
		return offset;
	}

	private LinkedNode addDomChildElement(Element node) {
		int position = findInsertPositionFor(node);
		LinkedElement linkedElement = new LinkedElement(node);

		int startOffset = findStartOffsetForPosition(position);

		getContent().insertString(startOffset, "\0\0"); // +1??
		if (startOffset == getEndOffset())
			setContent(getContent(), getStartOffset(), getEndOffset() + 2);
		if (position > 0) { // attention, hack for this stupid content thing ...
			IVexNode previousSibling = children.get(position - 1);
			if (previousSibling.getEndOffset() == startOffset + 2) // erroneously
				// moved as
				// well
				previousSibling.setContent(previousSibling.getContent(),
						previousSibling.getStartOffset(), startOffset);
		}
		linkedElement.setContent(this.getContent(), startOffset,
				startOffset + 1);

		children.add(position, linkedElement);
		linkedElement.setParent(this);
		
		NodeList grandChildren = node.getChildNodes();
		for (int i = 0; i < grandChildren.getLength(); i++) {
			linkedElement.addDomChild(grandChildren.item(i));
		}
		
		getDocument().fireElementChanged(this);
		
		if (isDebugging(DEBUG_SYNCEVENTS))
			System.out.println(MessageFormat.format("added child {1} to {0}",
					this, linkedElement));

		return linkedElement;
	}

	/**
	 * Finds a valid insert position in this node's list of children for the
	 * given DOM node. 
	 * 
	 * @param node
	 *            the W3C DOM node to insert
	 * @return the position to be passed to {@link #children}.
	 *         {@linkplain List#add(int, Object) add}
	 *         That's the index in the child node list, not some position.
	 * @throws IllegalArgumentException
	 *             if no position could be found
	 */
	private int findInsertPositionFor(Node node) {
		int result = -2;
		
		// Attempt 0: No children -> only one insert position possible
		if (children.size() < 1)
			result = 0;
		else {
			final IndexedRegion nodeRegion = AdapterUtils.getAdapter(node, IndexedRegion.class);
			final int nodeStartOffset = nodeRegion.getStartOffset();
			
			// Attempt 4: If the new node is behind the last node, add at end
			IVexNode[] childNodes = getChildNodes();
			LinkedNode lastNode = (LinkedNode) childNodes[childNodes.length-1];
			int endOffset = ((IndexedRegion) lastNode.getDomNode()).getEndOffset();
			if (endOffset <= nodeStartOffset)
				result = childNodes.length;
			
			// Attempt 1: Check previous sibling.
			if (result < 0) {
				final Node previousSibling = node.getPreviousSibling();
				if (previousSibling != null) {
					result = findChildIndex(new Criterion<IVexNode>() {
						public boolean matches(IVexNode candidate) {
							return ((LinkedNode) candidate).getDomNode().equals(
									previousSibling);
						}
					}) + 1;
				} 
			}
			
			
			// Attempt 2: Check following sibling.
			if (result < 0) {
				final Node nextSibling = node.getNextSibling();
				if (nextSibling != null) {
					result = findChildIndex(new Criterion<IVexNode>() {
						public boolean matches(IVexNode candidate) {
							return ((LinkedNode) candidate).getDomNode()
									.equals(nextSibling);
						}
					});
				}
			}
			
			// Attempt 3: try to iterate via source positions
			if (result < 0) { 
//					System.err.println("Parent region: " + getDomNode());
//					System.err.println("Node region:   " + nodeRegion);
					result = findChildIndex(new Criterion<IVexNode>() {
						public boolean matches(IVexNode child) {
							IndexedRegion childRegion = AdapterUtils.getAdapter(child, IndexedRegion.class);
//							System.err.println("Child region: " + childRegion);
							int childStartOffset = childRegion.getStartOffset();
							return (nodeStartOffset >= childStartOffset);
						}
						
					});
					
					
//					return 0;
//					throw new IllegalArgumentException(MessageFormat.format(
//							"Cannot find insert position for {0} in {1}", node,
//							this));
			}
			
			
			
			
			 }
		 
//		return max(0, min(result, children.size()-1));
		return Math.max(result, 0);
	}

	/**
	 * The criterion for {@link LinkedElement#findChildIndex(Criterion)}.
	 * 
	 * FIXME Extract this to .utils
	 * 
	 * @author tv
	 * @param <T>
	 *            the type of the argument passed to {@link #matches(Object)}
	 */
	public interface Criterion<T> {
		public boolean matches(T child);

	}

	/**
	 * Finds the first child node for which the given criterion holds true (i.e.
	 * its matches method returns true on this node).
	 * 
	 * @param c
	 *            The criterion to match
	 * @return the index of the given node, or -2 if none has been found.
	 */
	public int findChildIndex(Criterion<IVexNode> c) {
		int pos = 0;
		Iterator<IVexNode> iter = children.iterator();
		while (iter.hasNext())
			if (c.matches(iter.next()))
				return pos;
			else
				pos++;
		return -2;
	}

	public void setParent(IVexElement parent) {
		super.setParent(parent);
	}

}
