/**
 * LinkedText.java, part of project vex-toolkit
 * 
 * (C) 2008 by Thorsten Vitt for TextGrid
 */
package net.sf.vex.dom.linked;

import static info.textgrid.util.text.TextUtils.visualizeControlChars;
import static net.sf.vex.VexToolkitPlugin.DEBUG_NODEADAPTER;
import static net.sf.vex.VexToolkitPlugin.DEBUG_SELECTION;
import static net.sf.vex.VexToolkitPlugin.DEBUG_SYNCEVENTS;
import static net.sf.vex.VexToolkitPlugin.isDebugging;
import info.textgrid.util.text.TextUtils;

import java.text.MessageFormat;
import java.util.Iterator;
import java.util.LinkedList;

import net.sf.vex.VexToolkitPlugin;
import net.sf.vex.dom.Content;
import net.sf.vex.dom.IVexNode;
import net.sf.vex.dom.IVexText;
import net.sf.vex.dom.Position;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.wst.sse.core.internal.provisional.INodeNotifier;
import org.eclipse.wst.sse.core.internal.provisional.IndexedRegion;
import org.w3c.dom.Text;

/**
 * A text node. Unlike in the original Vex model, we store these nodes in order
 * to represent the corresponding DOM nodes.
 * 
 * <h4>Whitespace handling (TODO: Implement as described here ...)</h4>
 * <p>
 * Whitespace serves a double role in XML documents: It may be significant
 * content, or it may just exist to make the source file more readable and
 * should be discarded in the rendered view.
 * </p>
 * <p>
 * In the WYSIWYM view, we check for each Text node whether its containing
 * element is a <em>pre</em> node (ie. it is styled with
 * <code>white-space: preserve</code> in the CSS file, later we might check for
 * xml:space as well). For pre nodes, the {@linkplain Text#getData() text data}
 * is simply copied to the Content (and displayed) as is. For other elements,
 * special handling occurs:
 * </p>
 * <p>
 * On <strong>creating the node</strong>, we squeeze the whitespace
 * <em>for the {@linkplain #getContent() content}</em> according to the rules
 * outlined in {@link #getWSNormalizedString()}, but leave the source untouched.
 * When <strong>editing the node</strong> (i.e.
 * {@link #insertTextAbs(int, String)}, {@link #deleteTextAbs(int, int)}), we
 * perform the edit operation on the normalized content and replace the original
 * text content with the edited normalized text.
 * </p>
 * <p>
 * Suggestions for a better whitespace handling are taken, please open a
 * wishlist bug report.
 * </p>
 * 
 * 
 * 
 * @author Thorsten Vitt <vitt@linglit.tu-darmstadt.de> (tv) for TextGrid
 * @see Text
 * @see LinkedNode
 * @see LinkedDocument
 */
@SuppressWarnings("restriction")
public class LinkedText extends LinkedNode implements IVexText {

	private class IndentChange {
		/**
		 * @param contentPosition
		 * @param skippedWS
		 */
		public IndentChange(Position contentPosition, int skippedWS) {
			super();
			this.contentPosition = contentPosition;
			this.skippedWS = skippedWS;
		}

		public Position contentPosition;
		public int skippedWS;

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString() {
			return MessageFormat
					.format("<{0}+{1}>", contentPosition, skippedWS);
		}

	}

	private LinkedList<IndentChange> indentCorrections;

	/**
	 * The endSpaceHack field is used to allow for entering space characters at
	 * the end of an element.
	 * 
	 * <p>
	 * Normally, {@link #getWSNormalizedString()} would eat all trailing
	 * whitespace in this situation, which is fine in most situations. However,
	 * when typing text in WYSIWYM mode, completely eating trailing space is not
	 * desired, as it would disable typing text that contains whitespace when
	 * the point of insertion is at the end of the element.
	 * </p>
	 * 
	 * <p>
	 * The hack works as follows: {@link #insertTextAbs(int, String)} checks
	 * whether we want to append a whitespace character to the end of the
	 * element. If yes, it increments {@link #endSpaceHack}.
	 * {@link #getWSNormalizedString()} (which is called asynchronously via
	 * {@linkplain #notifyChanged(INodeNotifier, int, Object, Object, Object, int)
	 * notifyChanged}) re-appends an extra space after eating this if
	 * {@link #endSpaceHack} is positive and decrements {@link #endSpaceHack}.
	 * </p>
	 * 
	 * <p>
	 * This is ugly, but it seems to work.
	 * </p>
	 */
	private int endSpaceHack = 0;

	/**
	 * Creates a new LinkedText.
	 * 
	 * @param parent
	 *            The parent LinkedNode.
	 * @param text
	 *            The W3C DOM Text node to use.
	 * @param content
	 *            The Content buffer to use.
	 */
	public LinkedText(IVexNode parent, Text text, Content content) {
		super(text);
		setParent(parent);
		setContent(content);
	}

	@Override
	public Text getDomNode() {
		return (Text) super.getDomNode();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.sf.vex.dom.linked.LinkedNode#hasValidContent()
	 */
	@Override
	public boolean hasValidContent() {
		String textContent = getContent().getString(getStartOffset(),
				getEndOffset() - getStartOffset());
		int nulPos = textContent.indexOf('\0');
		if (nulPos > -1) {
			VexToolkitPlugin.log(IStatus.WARNING,
					new IllegalArgumentException(),
					"Text contains \\0 at pos. {0}! {1}", nulPos, this);
			return false;
		}
		return true;
	}

	public boolean equals(Object obj) {
		return super.equals(obj) && getText().equals(((LinkedText) obj).getText());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.sf.vex.dom.linked.LinkedNode#toString()
	 */
	@Override
	public String toString() {
		return MessageFormat.format(
				"{0}({1}..{2} ({5}, {6}): ''{3}'') <-> {4}", getClass()
						.getSimpleName(), getStartOffset(), getEndOffset(),
				visualizeControlChars(getContent().getString(getStartOffset(),
						getEndOffset() - getStartOffset()), true),
				visualizeControlChars(getDomNode().toString(), false),
				getStartPosition(), getEndPosition());
	}

	/**
	 * Removes the text from startOffset to endOffset from this node, the
	 * associated Contents object and the linked DOM Text. The given coordinates
	 * must be between {@link #getStartOffset()} and {@link #getEndOffset()}.
	 * 
	 * @param startOffset
	 *            Start offset of the text to delete, in absolute coordinates
	 *            (i.e. Document coordinates)
	 * @param endOffset
	 *            End offset of the text to delete, in absolute coordinates
	 *            (i.e. Document coordinates)
	 * @throws IllegalArgumentException
	 *             if the offsets are not contained within this node
	 */
	void deleteTextAbs(int startOffset, int endOffset) {
		if (startOffset < getStartOffset() || endOffset > getEndOffset())
			throw new IllegalArgumentException(
					MessageFormat
							.format(
									"The text fragment to delete ({0}..{1}) is not completely managed by this Text node ({2})",
									startOffset, endOffset, this));

		int relOffset = startOffset - getStartOffset();
		relOffset += getCorrectionForContentPos(relOffset);
		int relEndOffset = endOffset - getStartOffset();
		relEndOffset += getCorrectionForContentPos(relEndOffset);
		int length = relEndOffset - relOffset;

		 // Here comes a hack against TG-207:
		int domLength = getDomNode().getLength();
		if (length > domLength) {
			getContent().remove(startOffset, length - domLength);
			length = Math.min(length, domLength);
			VexToolkitPlugin
					.log(
							IStatus.WARNING,
							null,
							"The do-not-delete-over-the-end-of-text-nodes guard from TG-207 triggered trying to delete from {0} to {1} in {2}.",
							startOffset, endOffset, this);
		}
		if (length > 0)
			getDomNode().deleteData(relOffset, length);
		// getContent().remove(startOffset, length);
	}

	/**
	 * Inserts the given String into this node, i.e. the associated
	 * {@link Content} object and the linked {@linkplain Text DOM Text}.
	 * 
	 * @param offset
	 *            absolute offset where to insert stuff (i.e. from start of
	 *            document).
	 * @param s
	 *            the string to insert
	 * @throws IllegalArgumentException
	 *             if offset is off limits for this Text.
	 */
	public void insertTextAbs(int offset, String s) {
		int startOffset = getStartOffset();
		if (offset < startOffset || offset > getEndOffset())
			throw new IllegalArgumentException(MessageFormat.format(
					"Cannot insert {0} into {1} at offset {2}: Off limits", s,
					this, offset));
		int relOffset = offset - startOffset;
		relOffset += getCorrectionForContentPos(relOffset);
		if (offset == getEndOffset() && " ".equals(s))
			endSpaceHack++;
		getDomNode().insertData(relOffset, s); // Causes modification of our
		// internal stuff :-)
	}

	/**
	 * @return the node's parent element
	 */
	protected LinkedElement getParentElement() {
		LinkedNode node = this;
		do {
			node = node.getParent();
		} while (!(node instanceof LinkedElement));
		return (LinkedElement) node;
	}

	protected boolean isPre() {
		LinkedElement parentElement = getParentElement();
		return parentElement != null ? parentElement.isPre() : false;
	}

	/**
	 * Returns the normalized form of this element's string contents as it goes
	 * to the Contents.
	 * 
	 * <h4>Whitespace normalization spec (TODO: Implement as outlined here)</h4>
	 * 
	 * <p>
	 * This specification outlines the transformation from the original source
	 * code to the whitespace represented in the WYSIWYM mode's
	 * {@linkplain #getContent() Content buffer}.
	 * </p>
	 * 
	 * <p>
	 * If the code is considered preformatted (i.e. if it is styled as
	 * <code>whitespace: preserve</code>), there will be no normalization, the
	 * content buffer simply contains a verbatim copy of the original Text
	 * node's text content.
	 * 
	 * <dl>
	 * <dt>(1) leading whitespace:
	 * <dt>
	 * <dd><em>leading whitespace</em> is all whitespace from the beginning of
	 * the text node up to the first non-whitespace character, or up to the end
	 * of the text node's source text if it consists only of whitespace
	 * characters. If there is no leading whitespace, this processing step is
	 * skipped.
	 * <dl>
	 * <dt>(1a) if this text node is the first child of its parent node:
	 * <dt>
	 * <dd>drop all leading whitespace up to the first non-whitespace character</dd>
	 * <dt>(1b) else:</dt>
	 * <dd>replace all existing whitespace up to the first non-whitespace
	 * character with a single blank ' ' character
	 * </dl>
	 * <dt>(2) inner whitespace:</dt>
	 * <dd><em>inner whitespace</em> is all whitespace that is preceded as well
	 * as followed by non-whitespace characters inside this text node.
	 * <p>
	 * Every non-empty sequence of <em>inner whitespace</em> characters is
	 * replaced by a single space character (' ', U+20).
	 * </p>
	 * </dd>
	 * <dt>(3) trailing whitespace:</dt>
	 * <dd><em>trailing whitespace</em> is all whitespace from this text node's
	 * last non-whitespace character up to the end of the text node.</em></dd>
	 * <dl>
	 * <dt>(3a) if this text node is the last child of its parent node:</dt>
	 * <dd>drop all trailing whitespace.</dd>
	 * <dt>(3b) else:
	 * <dd>replace all trailing whitespace with a single space character (' ').</dt>
	 * </dl>
	 * 
	 * @return
	 */
	protected String getWSNormalizedString() {

		String orig = getDomNode().getData();
		indentCorrections = new LinkedList<IndentChange>();
		if (isPre()) {
			if (VexToolkitPlugin.isDebugging(VexToolkitPlugin.DEBUG_SELECTION))
				System.out.println("Did not normalize new text: "
						+ TextUtils.visualizeControlChars(orig, true));

			return orig; // TG-107
		}
		StringBuilder target = new StringBuilder(orig.length());

		int t = 0; // position in target;
		int skippedWS = 0;

		final int PREFIX = 0;
		final int CONTENT = 1;
		final int WS = 2;
		int state = PREFIX;

		for (int i = 0; i < orig.length(); i++) {
			char ch = orig.charAt(i);
			switch (state) {
			case PREFIX:
				if (Character.isWhitespace(ch) || Character.isISOControl(ch)) {
					++skippedWS;
					continue;
				} else {
					if (skippedWS > 0
							&& getDomNode().getPreviousSibling() != null) { // (1
						// b
						// )
						target.append(' ');
						--skippedWS;
					}
					target.append(ch);
					indentCorrections.add(new IndentChange(getContent()
							.createPosition(t), skippedWS));
					t++;
					state = CONTENT;
					continue;
				}
			case CONTENT:
				if (!(Character.isWhitespace(ch) || Character.isISOControl(ch))) {
					target.append(ch);
					t++;
					continue;
				} else {
					state = WS;
					++skippedWS;
					continue;
				}
			case WS: // (2)
				if (Character.isWhitespace(ch) || Character.isISOControl(ch)) {
					++skippedWS;
					continue;
				} else {
					target.append(' ');
					++t;
					--skippedWS;

					target.append(ch);
					if (indentCorrections.isEmpty()
							|| indentCorrections.getLast().skippedWS != skippedWS)
						indentCorrections.add(new IndentChange(getContent()
								.createPosition(t), skippedWS));
					++t;
					state = CONTENT;
					continue;
				}

			}

		}

			// non-empty (TG-207) whitespace-only text node:
		if (state == PREFIX && orig.length() > 0) { 
			/*
			 * Possible cases are as follows, underline == this text node: (i)
			 * <this>___</this> → <this> </this>: Text node has no siblings (ii)
			 * <parent>__<child>...</child></parent> → eat it: only following
			 * sibling (iii) <child>___</parent> → eat it: only preceding
			 * sibling (iv) <parent><child1/>___<child2/></parent>: siblings all
			 * around → eat?
			 */
			if (getDomNode().getPreviousSibling() == null
					&& getDomNode().getNextSibling() == null) {
				target.append(' ');
				++t;
				--skippedWS;
			}
			if (indentCorrections.isEmpty()
					|| indentCorrections.getLast().skippedWS != skippedWS)
				indentCorrections.add(new IndentChange(getContent()
						.createPosition(t + 1), skippedWS));
			/*
			 * This is slightly wrong since it maps the trailing whitespace to
			 * the word instead of to the space char on selections. But I have
			 * to think about this ... does this occur in case (2) as well?
			 */

		}

		// Trailing whitespace correction (3)
		else if (state == WS) {
			if (getDomNode().getNextSibling() != null) { // (3b)
				target.append(' ');
				++t;
				--skippedWS;
			}
			if (endSpaceHack > 0) {
				target.append(' ');
				++t;
				--skippedWS;
				--endSpaceHack;
			}
			if (indentCorrections.isEmpty()
					|| indentCorrections.getLast().skippedWS != skippedWS)
				indentCorrections.add(new IndentChange(getContent()
						.createPosition(t + 1), skippedWS));
			/*
			 * This is slightly wrong since it maps the trailing whitespace to
			 * the word instead of to the space char on selections. But I have
			 * to think about this ... does this occur in case (2) as well?
			 */
		}
		if (VexToolkitPlugin.isDebugging(VexToolkitPlugin.DEBUG_SELECTION)) {
			System.out.println("Mapped "
					+ TextUtils.visualizeControlChars(orig, true) + "\n    to "
					+ TextUtils.visualizeControlChars(target.toString(), true));
		}
		return target.toString();
	}

	public int getCorrectionForSourcePos(int sourcePosition) {
		IndentChange change = null;
		IndentChange next = null;

		Iterator<IndentChange> iterator = indentCorrections.iterator();
		while (iterator.hasNext()) {
			next = iterator.next();
			if (next.contentPosition.getOffset() + next.skippedWS > sourcePosition)
				break;
			change = next;
		}

		int result = 0;
		if (change == null) {
			if (next != null) {
				if (sourcePosition < next.skippedWS)
					result = -sourcePosition;
			}
		} else {
			result = -change.skippedWS;
		}

		if (VexToolkitPlugin.isDebugging(DEBUG_SELECTION)) {
			System.out.println("Correction for source pos " + sourcePosition
					+ ": " + change + " of " + indentCorrections + " -> "
					+ result);
			System.out.println(TextUtils.visualizeControlChars(getDomNode()
					.getData(), true));
			// System.out.println(TextUtils.makeRuler('-', '|', sourcePosition,
			// sourcePosition + result, change.contentPosition.getOffset()
			// - change.skippedWS));
		}
		return result;
	}

	public int getCorrectionForContentPos(int contentPosition) {
		IndentChange change = null;

		/*
		 * For empty text nodes (i.e. source code consists of whitespace only)
		 * we need special consideration: if we are before a start tag, put the
		 * cursor immediately before the start tag to avoid TG-214. If we are 
		 * before an end tag, however, this would break text entering :-(, so
		 * we don't want this correction.  
		 */
		boolean startTagCorrection = getEndOffset() == getStartOffset() && getDomNode().getNextSibling() != null;

		Iterator<IndentChange> iterator = indentCorrections.iterator();
		while (iterator.hasNext()) {
			IndentChange next = iterator.next();
			if (next.contentPosition.getOffset() > contentPosition && !startTagCorrection)
				break;
			change = next;
		}

		int result = change == null ? 0 : change.skippedWS;
		if (VexToolkitPlugin.isDebugging(DEBUG_SELECTION))
			System.out
					.println(MessageFormat
							.format(
									"Correction for content pos {0}: {1} of {2} ->  {3}; start tag correction: {5}; in {4}",
									contentPosition, change, indentCorrections,
									result, this, startTagCorrection));

		return result;
	}

	@Override
	public void notifyChanged(INodeNotifier notifier, int eventType,
			Object changedFeature, Object oldValue, Object newValue, int pos) {
		switch (eventType) {
		case INodeNotifier.ADD:
		case INodeNotifier.CHANGE:
			if (newValue instanceof String) {
				if (VexToolkitPlugin.isDebugging(DEBUG_NODEADAPTER))
					System.out.println("Processing: "
							+ formatNotification(notifier, eventType,
									changedFeature, oldValue, newValue, pos));
				int startOffset = getStartOffset(); // will not change.
				int endOffset = getEndOffset();
				int oldLength = endOffset - startOffset;
				getContent().remove(startOffset, oldLength);
				String newContent = getWSNormalizedString(); // (String)
				// newValue;
				int newLength = newContent.length();
				getContent().insertString(startOffset, newContent);
				setContent(getContent(), startOffset, startOffset + newLength);
				// setContent(getContent(), startOffset, startOffset +
				// newLength);
				if (isDebugging(DEBUG_NODEADAPTER)) {
					System.out.println("Processed "
							+ formatNotification(notifier, eventType,
									changedFeature, oldValue, newValue, pos));
					getDocument().printDocument();
				}
				if (isDebugging(DEBUG_SYNCEVENTS))
					System.out.println(MessageFormat.format(
							"changed text to {0}", this));
				return;
			}
		case INodeNotifier.CONTENT_CHANGED:
		}
		super.notifyChanged(notifier, eventType, changedFeature, oldValue,
				newValue, pos);
	}

	/**
	 * Returns the content offset for the given source code offset, if it's
	 * inside this node. Both values are absolute, and sourceOffset must be
	 * inside this node's source region.
	 * 
	 * @param sourceOffset
	 * @return
	 */
	public int getContentOffsetFor(int sourceOffset) {
		int nodeSourceOffset = ((IndexedRegion) getAdapter(IndexedRegion.class))
				.getStartOffset();
		int relOffset = sourceOffset - nodeSourceOffset;
		relOffset += getCorrectionForSourcePos(relOffset);
		return getStartOffset() + relOffset;
	}
}
