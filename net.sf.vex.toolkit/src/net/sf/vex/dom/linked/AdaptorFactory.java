package net.sf.vex.dom.linked;

import org.eclipse.core.runtime.IAdapterFactory;
import org.eclipse.wst.sse.core.internal.provisional.INodeNotifier;
import org.eclipse.wst.sse.core.internal.provisional.IndexedRegion;
import org.eclipse.wst.xml.core.internal.provisional.document.IDOMNode;
import org.w3c.dom.Node;

/**
 * Adapts LinkedNodes to W3C DOM {@link Node}s and to WST {@link IDOMNode}s.
 * 
 * @author tv
 */
@SuppressWarnings( { "unchecked", "restriction" })
public class AdaptorFactory implements IAdapterFactory {
	
	private final static Class[] ADAPTER_LIST = new Class[] { LinkedNode.class,
			Node.class, IDOMNode.class, IndexedRegion.class,
			INodeNotifier.class };
	// FIXME do we have to explicitly add IndexedRegion and INodeNotifier as
	// well?

	public Object getAdapter(Object adaptableObject, Class adapterType) {
		if (adaptableObject instanceof LinkedNode) {
			LinkedNode linkedNode = (LinkedNode) adaptableObject;
			if (adapterType.isAssignableFrom(linkedNode.getClass()))
				return linkedNode;
			else {
				Node domNode = linkedNode.getDomNode();
				if (adapterType.isAssignableFrom(domNode.getClass())) {
					return domNode;
				}
			}

		}
		return null;
	}

	public Class[] getAdapterList() {
		return ADAPTER_LIST;
	}

}
