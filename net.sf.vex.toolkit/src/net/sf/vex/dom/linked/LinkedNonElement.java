package net.sf.vex.dom.linked;

import java.text.MessageFormat;

import net.sf.vex.dom.Content;

import org.eclipse.wst.sse.core.internal.provisional.INodeNotifier;
import org.eclipse.wst.sse.core.internal.provisional.IndexedRegion;
import org.w3c.dom.CharacterData;
import org.w3c.dom.Text;

/**
 * Common base for comments and cdata sections
 * 
 * @author <a href="mailto:m.wissenbach@stud.tu-darmstadt.de">Moritz Wissenbach</a>
 * 
 */
//TODO derive a common base class with Text somehow?

public abstract class LinkedNonElement extends LinkedNode {

	protected LinkedNonElement(CharacterData cdata) {
			super(cdata);
	}

	@Override
	public CharacterData getDomNode() {
		return (CharacterData)super.getDomNode();
	}

	/**
	 * Removes the text from startOffset to endOffset from this node, the
	 * associated Contents object and the linked DOM Text. The given coordinates
	 * must be between {@link #getStartOffset()} and {@link #getEndOffset()}.
	 * 
	 * @param startOffset
	 *            Start offset of the text to delete, in absolute coordinates
	 *            (i.e. Document coordinates)
	 * @param endOffset
	 *            End offset of the text to delete, in absolute coordinates
	 *            (i.e. Document coordinates)
	 * @throws IllegalArgumentException
	 *             if the offsets are not contained within this node
	 */
	protected void deleteTextAbs(int startOffset, int endOffset) {
		if (startOffset < getStartOffset() || endOffset > getEndOffset())
			throw new IllegalArgumentException(
					MessageFormat
							.format(
									"The text fragment to delete ({0}..{1}) is not completely managed by this NonElement node ({2})",
									startOffset, endOffset, this));
	
		int relOffset = startOffset - getStartOffset();
		int length = endOffset - startOffset;
		// -1 is for the sentinels '\0'
		getDomNode().deleteData(relOffset + getCorrectionForContentPos() - 4 , length);
		// getContent().remove(startOffset, length);
	}

	/**
	 * Inserts the given String into this node, i.e. the associated
	 * {@link Content} object and the linked {@linkplain Text DOM Text}.
	 * 
	 * @param offset
	 *            absolute offset where to insert stuff (i.e. from start of
	 *            document).
	 * @param s
	 *            the string to insert
	 * @throws IllegalArgumentException
	 *             if offset is off limits for this Text.
	 */
	public void insertTextAbs(int offset, String s) {
		int startOffset = getStartOffset();
		offset -= 1;
		if (offset < startOffset || offset > getEndOffset())
			throw new IllegalArgumentException(MessageFormat.format(
					"Cannot insert {0} into {1} at offset {2}: Off limits", s,
					this, offset));
		int relOffset = offset - startOffset;
		// -1 is for the sentinels '\0'
		getDomNode().insertData(relOffset + getCorrectionForContentPos() -3, s);
	
		// getContent().insertString(offset, s);
	
		// inserting text at the start of this text's content moves the start
		// position marker after the inserted text. So we need to reset it to
		// its original position.
		if (relOffset == 0)
			setContent(getContent(), startOffset, getEndOffset());
	}

	@Override
	public void notifyChanged(INodeNotifier notifier, int eventType, Object changedFeature,
			Object oldValue, Object newValue, int pos) {
				switch (eventType) {
				case INodeNotifier.ADD:
				case INodeNotifier.CHANGE:
					if (newValue instanceof String) {
						// System.out.println("Processing: "
						// + formatNotification(notifier, eventType,
						// changedFeature, oldValue, newValue, pos));
						String newContent = (String) newValue;
						int newLength = newContent.length();
						int startOffset = getStartOffset(); // will not change.
						int endOffset = getEndOffset();
						int oldLength = endOffset - startOffset;
						getContent().insertString(endOffset, newContent);
						getContent().remove(startOffset + 1, oldLength - 1);
						// setContent(getContent(), startOffset, startOffset +
						// newLength);
						// System.out.println("Processed "
						// + formatNotification(notifier, eventType,
						// changedFeature, oldValue, newValue, pos));
						return;
					}
				case INodeNotifier.CONTENT_CHANGED:
				}
				super.notifyChanged(notifier, eventType, changedFeature, oldValue,
						newValue, pos);
			}

	/**
	 * Returns the content offset for the given source code offset, if it's
	 * inside this node. Both values are absolute, and sourceOffset must be
	 * inside this node's source region.
	 * 
	 * @param sourceOffset
	 * @return
	 */
	public int getContentOffsetFor(int sourceOffset) {
		int nodeSourceOffset = ((IndexedRegion) getAdapter(IndexedRegion.class))
				.getStartOffset();
		int relOffset = sourceOffset - nodeSourceOffset;
		relOffset += getCorrectionForSourcePos();
		return getStartOffset() + relOffset;
	}

	public abstract int getCorrectionForSourcePos();

	public abstract int getCorrectionForContentPos();


}
