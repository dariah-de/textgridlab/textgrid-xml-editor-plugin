/**
 * LinkedNode.java, part of project vex-toolkit
 * 
 * (C) 2008 by Thorsten Vitt for TextGrid
 */
package net.sf.vex.dom.linked;

import static info.textgrid.util.text.TextUtils.visualizeControlChars;
import static net.sf.vex.VexToolkitPlugin.DEBUG_SYNCEVENTS;
import static net.sf.vex.VexToolkitPlugin.isDebugging;

import java.text.MessageFormat;
import java.util.Iterator;

import net.sf.vex.VexToolkitPlugin;
import net.sf.vex.dom.Content;
import net.sf.vex.dom.IVexNode;
import net.sf.vex.dom.impl.Node;
import net.sf.vex.dom.impl.WrongModelException;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.wst.sse.core.internal.provisional.INodeAdapter;
import org.eclipse.wst.sse.core.internal.provisional.INodeNotifier;

import com.google.common.collect.Iterators;

/**
 * A node linked to its DOM representation.
 * 
 * @author Thorsten Vitt <vitt@linglit.tu-darmstadt.de> (tv) for TextGrid
 * @see IVexNode
 * @see org.w3c.dom.Node
 */
@SuppressWarnings("restriction")
public abstract class LinkedNode extends Node implements IVexNode,
		INodeAdapter, IAdaptable, Comparable<IVexNode> {
	
	private static final LinkedNode[] NO_CHILDREN = new LinkedNode[0];
	private org.w3c.dom.Node domNode;
	private LinkedNode parent;

	/**
	 * Creates a new linked node.
	 * 
	 * @param dom
	 *            the W3C DOM node associated with this node.
	 */
	public LinkedNode(org.w3c.dom.Node domNode) {
		super();
		if (domNode == null) {
			throw new IllegalArgumentException("domNode must not be null");
		}
		setDomNode(domNode); // also registers the as a INodeAdapter
	}

	/**
	 * Compares the nodes based on the associated start position in the content
	 * document.
	 * 
	 * Note: a.equals(b) -> a.compareTo(b) == 0, but the inversion is not
	 * guaranteed.
	 * 
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	public int compareTo(IVexNode o) {
		if (getStartPosition() != null && o.getStartPosition() != null)
			return getStartOffset() - o.getStartOffset();
		return 0;
	}

	/**
	 * Returns the associated DOM node.
	 * 
	 * @return The W3C DOM node associated with this object
	 */
	public org.w3c.dom.Node getDomNode() {
		return domNode;
	}

	/**
	 * Associates a new W3C DOM node with this node.
	 * 
	 * @param domNode
	 *            The W3C DOM node to associate.
	 */
	protected void setDomNode(org.w3c.dom.Node domNode) {
		this.domNode = domNode;
		if (domNode instanceof INodeNotifier) {
			((INodeNotifier) domNode).addAdapter(this);
		}
	}

	public void setParent(IVexNode parent) {
		WrongModelException.throwIfNeeded(parent, LinkedNode.class);
		this.parent = (LinkedNode) parent;
	}

	public LinkedNode getParent() {
		return parent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override 	
	public String toString() {
		try {
			return MessageFormat.format("{0}({1}..{2}, {3}): {4}", getClass()
				.getSimpleName(), getStartOffset(), getEndOffset(),
					visualizeControlChars(domNode.toString(), false),
					visualizeControlChars(
									getContent().getString(getStartOffset(),
											getEndOffset() - getStartOffset()),
									true));
		} catch (Exception e) {
			return MessageFormat.format(
					"{0}({1}) !!! Exception during toString: {2}", getClass()
							.getSimpleName(), getDomNode(), e);
		}
	}

	/**
	 * Returns true iff the content associated with this object is valid.
	 * 
	 * @return LinkedNode's implementation always returns true, but subclasses
	 *         override.
	 */
	public boolean hasValidContent() {
		return true;
	}
	
	
	public boolean hasChildren() {
		return false; // ordinary nodes don't have children, clients may
						// override
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.wst.sse.core.internal.provisional.INodeAdapter#isAdapterForType
	 * (java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	public boolean isAdapterForType(Object type) {
		if (type instanceof Class) {
			Class clazz = (Class) type;
			return (clazz.isInstance(this) || clazz.isInstance(getDomNode()));
		}
		return false;
	}

	/**
	 * Notifies this node of changes.
	 * 
	 * Clients should override and call the super implementation only for events
	 * they don't expect and don't know how to handle.
	 * 
	 * @see org.eclipse.wst.sse.core.internal.provisional.INodeAdapter#notifyChanged(org.eclipse.wst.sse.core.internal.provisional.INodeNotifier,
	 *      int, java.lang.Object, java.lang.Object, java.lang.Object, int)
	 */
	public void notifyChanged(INodeNotifier notifier, int eventType,
			Object changedFeature, Object oldValue, Object newValue, int pos) {
		 VexToolkitPlugin.log(IStatus.INFO, null,
				"Unhandled change notification: "
						+ formatNotification(notifier, eventType,
				changedFeature, oldValue, newValue, pos));
		 if (isDebugging(DEBUG_SYNCEVENTS)) {
			System.err.println(MessageFormat.format("ignored {1} in {0}", this,
					INodeNotifier.EVENT_TYPE_STRINGS[eventType]));
		}
	}

	/**
	 * Creates a string message describing the given notifyChanged parameters
	 * (for debugging purposes)
	 * 
	 * @see #notifyChanged(INodeNotifier, int, Object, Object, Object, int)
	 */
	protected String formatNotification(INodeNotifier notifier, int eventType,
			Object changedFeature, Object oldValue, Object newValue, int pos) {
		return MessageFormat.format(
				"notifyChanged({2}) on {0} with: "
//						+ "\n  notifier = {1},\n  eventType = {2},"
						+ "\n  changedFeature = {3},\n  oldValue = {4},"
						+ "\n  newValue = {5} (class {7}),\n  pos = {6}", this,
						notifier,
				INodeNotifier.EVENT_TYPE_STRINGS[eventType], changedFeature,
				oldValue, newValue, pos, newValue == null ? "null" : newValue
						.getClass().getSimpleName());
	}

	/**
	 * Calculates the document for this node.
	 * 
	 * @return the document or null if this element is not part of a document
	 */
	public LinkedDocument getDocument() {
		LinkedNode potentialRoot = this;
		while (!(potentialRoot instanceof LinkedRootElement))
			potentialRoot = potentialRoot.getParent();
		if (potentialRoot == null)
			return null;
		else
			return ((LinkedRootElement) potentialRoot).getDocument();
	}
	
	@SuppressWarnings("unchecked")
	public Object getAdapter(Class adapter) {
		return Platform.getAdapterManager().getAdapter(this, adapter);
	}

	@Override
	protected void setContent(Content content) {
		super.setContent(content);
	}
	
	protected Iterator<IVexNode> getChildNodeIterator() {
		return Iterators.emptyIterator();
	}

	protected IVexNode[] getChildNodes() {
		return NO_CHILDREN;
	}

	/**
	 * Recursively disposes of this node and its children.
	 */
	protected void dispose() {
		for (Iterator<IVexNode> iterator = getChildNodeIterator(); iterator.hasNext();) {
			IVexNode child = iterator.next();
			if (child instanceof LinkedNode)
				((LinkedNode) child).dispose();
			else
				VexToolkitPlugin.log(IStatus.WARNING, null,
						"Found a model inconsistency: child {0} of {1} is a {2} instead of a LinkedNode.", child, this,
						child.getClass());
		}

		if (domNode != null && domNode instanceof INodeNotifier)
			((INodeNotifier) domNode).removeAdapter(this);
	}

	/**
	 * clients should override
	 */
	public boolean equals(Object obj) {
		if (obj == null || !getClass().equals(obj.getClass()))
			return false;

		if (obj instanceof LinkedNode) {
			LinkedNode other = (LinkedNode) obj;
			if (getDomNode().equals(other.getDomNode())
					&& getStartOffset() == other.getStartOffset()
					&& getEndOffset() == other.getEndOffset())
				return true;
		}
		return false;
	}

	@Override
	public int hashCode() {
		int result = getClass().hashCode() ^ getDomNode().hashCode();
		if (getStartPosition() != null && getEndPosition() != null)
			result ^= (getStartOffset() << 4) ^ getEndOffset();
		return result;
	}

}
