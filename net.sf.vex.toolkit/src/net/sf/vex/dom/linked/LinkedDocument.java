/**
 * LinkedDocument.java, part of project vex-toolkit
 * 
 * (C) 2008 by Thorsten Vitt for TextGrid
 */
package net.sf.vex.dom.linked;

import static net.sf.vex.VexToolkitPlugin.DEBUG;
import info.textgrid.lab.core.swtutils.AdapterUtils;

import java.io.PrintStream;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import net.sf.vex.VexToolkitPlugin;
import net.sf.vex.dom.Content;
import net.sf.vex.dom.DocumentEvent;
import net.sf.vex.dom.DocumentValidationException;
import net.sf.vex.dom.IVexDocument;
import net.sf.vex.dom.IVexDocumentFragment;
import net.sf.vex.dom.IVexElement;
import net.sf.vex.dom.IVexNode;
import net.sf.vex.dom.IVexNonElement;
import net.sf.vex.dom.IVexText;
import net.sf.vex.dom.IWhitespacePolicy;
import net.sf.vex.dom.Validator;
import net.sf.vex.dom.impl.Document;
import net.sf.vex.dom.impl.WrongModelException;
import net.sf.vex.undo.IUndoableEdit;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.MultiStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.Region;
import org.eclipse.wst.sse.core.internal.provisional.IndexedRegion;
import org.eclipse.wst.xml.core.internal.provisional.document.IDOMDocument;
import org.w3c.dom.Text;

import com.google.common.collect.Lists;

/**
 * @author Thorsten Vitt <vitt@linglit.tu-darmstadt.de> (tv) for TextGrid
 * 
 */
@SuppressWarnings("restriction")
public class LinkedDocument extends Document implements IVexDocument {

	private IDOMDocument domDocument;
	private IWhitespacePolicy whitespacePolicy;
	

	public LinkedDocument(IDOMDocument domDocument) {
		super(new LinkedRootElement(domDocument.getDocumentElement()));
		this.setDomDocument(domDocument);
	}

	// @Override
	// public boolean canInsertFragment(int offset, IVexDocumentFragment
	// fragment) {
	// // TODO Override this with XSD task
	// return super.canInsertFragment(offset, fragment);
	// }

	// @Override
	// public boolean canInsertText(int offset) {
	// // TODO Override this with XSD task
	// return super.canInsertText(offset);
	// }

	private void setDomDocument(IDOMDocument domDocument) {
		this.domDocument = domDocument;
	}

	public IDOMDocument getDomDocument() {
		return domDocument;
	}

	@SuppressWarnings("unchecked")
	@Override
	public synchronized void delete(int startOffset, int endOffset)
			throws DocumentValidationException {
		// synchronizing on the whole document to avoid to TG-208
		

		// hasValidContent();
		// System.out.println(MessageFormat.format(
		// "LinkedDocument.delete({0}, {1}) starts", startOffset,
		// endOffset));
		// System.out.println(getContent().getString(0,
		// getContent().getLength()).replace("\0", "\\0").replace("\n", "\\n"));

		// This is basically the Vex Document#delete code copied and augmented
		// by DOM handling.

		LinkedElement e1 = (LinkedElement) this.getElementAt(startOffset);
		LinkedElement e2 = (LinkedElement) this.getElementAt(endOffset);
		if (e1 != e2) {
			throw new IllegalArgumentException("Deletion from " + startOffset
					+ " to " + endOffset + " is unbalanced");
		}

		// FIXME Replace this section by XSD validation code
		Validator validator = this.getValidator();
		if (validator != null) {
			String[] seq1 = this.getNodeNames(e1.getStartOffset() + 1,
					startOffset);
			String[] seq2 = this.getNodeNames(endOffset, e1.getEndOffset());
			if (!validator
					.isValidSequence(e1.getName(), seq1, seq2, null, true)) {
				throw new DocumentValidationException("Unable to delete from "
						+ startOffset + " to " + endOffset);
			}
		}

		// Grab the fragment for the undoable edit while it's still here
		IVexDocumentFragment frag = getFragment(startOffset, endOffset);

		this.fireBeforeContentDeleted(new DocumentEvent(this, e1, startOffset,
				endOffset - startOffset, null));

		synchronized (e1) { // FIXME we should synchronize over e1#children ...
			Iterator iter = e1.getChildNodeIterator();
			while (iter.hasNext()) {
				IVexNode child = (IVexNode) iter.next();

				// remove completely contained nodes:
				if (startOffset <= child.getStartOffset()
						&& child.getEndOffset() < endOffset) {

					if (child instanceof LinkedNode) {
						LinkedNode linkedChild = (LinkedNode) child;
						linkedChild.getDomNode().getParentNode().removeChild(
								linkedChild.getDomNode());
						// this will trigger the deletion in our own model.
					}
					// Order is important here
				} else if (child instanceof LinkedNonElement
						// TODO check, b/c of sentinels '\0'
						&& startOffset > child.getStartOffset()
						&& endOffset <= child.getEndOffset()) {
					((LinkedNonElement) child).deleteTextAbs(startOffset,
							endOffset);
				} else if (child instanceof LinkedText
						&& startOffset >= child.getStartOffset()
						&& endOffset <= child.getEndOffset()) {
					// remove partial text:
					((LinkedText) child).deleteTextAbs(startOffset, endOffset);
				}
			}
		}

		IUndoableEdit edit = this.isUndoEnabled() ? new DeleteEdit(startOffset,
				endOffset, frag) : null;

		this.fireContentDeleted(new DocumentEvent(this, e1, startOffset,
				endOffset - startOffset, edit));

		if (VexToolkitPlugin.isDebugging(DEBUG)) {

			hasValidContent();
			System.out.println(getContent().getString(0,
					getContent().getLength()).replace("\0", "\\0").replace(
					"\n", "\\n"));
		}

	}

	// @Override
	// public IVexDocumentFragment getFragment(int startOffset, int endOffset) {
	// // TODO This must reflect nodes different from text and elements.
	// return super.getFragment(startOffset, endOffset);
	// }

	// @Override
	// public IVexNode[] getNodes(int startOffset, int endOffset) {
	// // TODO This must reflect nodes different from text and elements.
	// return super.getNodes(startOffset, endOffset);
	// }

	@Override
	public void insertElement(int offset, IVexElement elementArg)
			throws DocumentValidationException {

		WrongModelException.throwIfNeeded(elementArg, LinkedElement.class);

		LinkedElement element = (LinkedElement) elementArg;

		if (offset < 1 || offset >= this.getLength()) {
			throw new IllegalArgumentException("Error inserting element <"
					+ element.getName() + ">: offset is " + offset
					+ ", but it must be between 1 and "
					+ (this.getLength() - 1));
		}

		// TODO adapt the following passage to the XSD based model
		Validator validator = this.getValidator();
		if (validator != null) {
			IVexElement parent = this.getElementAt(offset);
			String[] seq1 = this.getNodeNames(parent.getStartOffset() + 1,
					offset);
			String[] seq2 = new String[] { element.getName() };
			String[] seq3 = this.getNodeNames(offset, parent.getEndOffset());
			if (!validator.isValidSequence(parent.getName(), seq1, seq2, seq3,
					true)) {
				throw new DocumentValidationException("Cannot insert element "
						+ element.getName() + " at offset " + offset);
			}
		}

		// find the parent, and the index into its children at which
		// this element should be inserted
		IVexElement parent = this.getRootElement();
		int childIndex = -1;
		while (childIndex == -1) {
			boolean tryAgain = false;
			// FIXME consider other nodes here
			IVexElement[] children = parent.getChildElements();
			for (int i = 0; i < children.length; i++) {
				IVexElement child = children[i];
				if (offset <= child.getStartOffset()) {
					childIndex = i;
					break;
				} else if (offset <= child.getEndOffset()) {
					parent = child;
					tryAgain = true;
					break;
				}
			}
			if (!tryAgain && childIndex == -1) {
				childIndex = children.length;
				break;
			}
		}

		this.fireBeforeContentInserted(new DocumentEvent(this, parent, offset,
				2, null));

		this.getContent().insertString(offset, "\0\0");

		element.setContent(this.getContent(), offset, offset + 1);
		element.setParent(parent);
		parent.internalInsertChild(childIndex, element);

		IUndoableEdit edit = this.isUndoEnabled() ? new InsertElementEdit(
				offset, element) : null;

		this.fireContentInserted(new DocumentEvent(this, parent, offset, 2,
				edit));
	}

	@Override
	public void insertFragment(int offset, IVexDocumentFragment fragment)
			throws DocumentValidationException {

		if (offset < 1 || offset >= this.getLength()) {
			throw new IllegalArgumentException(
					"Error inserting document fragment");
		}

		IVexElement parent = this.getElementAt(offset);

		// FIXME replace this code with XSD model stuff
		if (this.getValidator() != null) {
			String[] seq1 = this.getNodeNames(parent.getStartOffset() + 1,
					offset);
			String[] seq2 = fragment.getNodeNames();
			String[] seq3 = this.getNodeNames(offset, parent.getEndOffset());
			if (!getValidator().isValidSequence(parent.getName(), seq1, seq2,
					seq3, true)) {

				throw new DocumentValidationException(
						"Cannot insert document fragment");
			}
		}

		this.fireBeforeContentInserted(new DocumentEvent(this, parent, offset,
				2, null));

		Content c = fragment.getContent();
		String s = c.getString(0, c.getLength());
		this.getContent().insertString(offset, s);

		// FIXME browse through child nodes instead of elements
		IVexElement[] children = parent.getChildElements();
		int index = 0;
		while (index < children.length
				&& children[index].getEndOffset() < offset) {
			index++;
		}

		IVexElement[] elements = fragment.getElements();
		for (int i = 0; i < elements.length; i++) {
			IVexElement newElement = this.cloneElement(elements[i], this
					.getContent(), offset, parent);
			parent.internalInsertChild(index, newElement);
			index++;
		}

		IUndoableEdit edit = this.isUndoEnabled() ? new InsertFragmentEdit(
				offset, fragment) : null;

		this.fireContentInserted(new DocumentEvent(this, parent, offset,
				fragment.getContent().getLength(), edit));

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.sf.vex.dom.impl.Document#insertText(int, java.lang.String)
	 */
	@Override
	public void insertText(int offset, String text)
			throws DocumentValidationException {
		if (offset < 1 || offset >= this.getLength()) {
			throw new IllegalArgumentException("Offset must be between 1 and "
					+ this.getLength());
		}

		IVexElement parent = this.getElementAt(offset);

		boolean isValid = false;
		if (this.getCharacterAt(offset - 1) != '\0') {
			isValid = true;
		} else if (this.getCharacterAt(offset) != '\0') {
			isValid = true;
		} else {
			// TODO Replace with XSD Validation code
			Validator validator = this.getValidator();
			if (validator != null) {
				String[] seq1 = this.getNodeNames(parent.getStartOffset() + 1,
						offset);
				String[] seq2 = new String[] { Validator.PCDATA };
				String[] seq3 = this
						.getNodeNames(offset, parent.getEndOffset());
				isValid = validator.isValidSequence(parent.getName(), seq1,
						seq2, seq3, true);
			} else {
				isValid = true;
			}
		}

		if (!isValid) {
			throw new DocumentValidationException("Cannot insert text '" + text
					+ "' at offset " + offset);
		}

		// Convert control chars to spaces
		StringBuffer sb = new StringBuffer(text);
		for (int i = 0; i < sb.length(); i++) {
			if (Character.isISOControl(sb.charAt(i)) && sb.charAt(i) != '\n') {
				sb.setCharAt(i, ' ');
			}
		}

		String s = sb.toString();

		this.fireBeforeContentInserted(new DocumentEvent(this, parent, offset,
				2, null));

		// this.content.insertString(offset, s);
		LinkedNode node = getNodeAt(offset);
		if (node instanceof LinkedNonElement) {
			((LinkedNonElement) node).insertTextAbs(offset, s);
		} else if (node instanceof LinkedText) {
			((LinkedText) node).insertTextAbs(offset, s);
		} else if (node instanceof LinkedElement) {
			LinkedElement element = (LinkedElement) node;
			Text domText = getDomDocument().createTextNode(s);
			/*
			 * When node instanceof LinkedElement, there are the following
			 * possibilities:
			 * 
			 * 1. <node>|</node> easy, node.appendChild()
			 * 
			 * 2. <node><child/>|</node> easy, node.appendChild()
			 * 
			 * 3. <node>|<child/></node> node.insertBefore(child)
			 * 
			 * 4. <node><child1/>|<child2/> node.insertBefore(child2)
			 * 
			 * So it boils down to: if there is a child following the cursor
			 * position, we do an insertBefore (cases 3, 4), otherwise its
			 * appendChild.
			 */

			if (node instanceof LinkedElement
					&& offset == node.getStartOffset()) {
				node.getParent().getDomNode().insertBefore(domText,
						node.getDomNode());
			} else {

				LinkedNode insertItBeforeMe = null;
				Iterator<IVexElement> children = element.getChildIterator();
				while (children.hasNext()) {
					IVexElement next = children.next();
					if (next.getStartOffset() == offset + 1) {
						insertItBeforeMe = (LinkedNode) next;
						break;
					}
				}

				if (insertItBeforeMe != null)
					element.getDomNode().insertBefore(domText,
							insertItBeforeMe.getDomNode());
				else
					element.getDomNode().appendChild(domText);
			}
		}

		// FIXME undo is probably broken.
		IUndoableEdit edit = isUndoEnabled() ? new InsertTextEdit(offset, s)
				: null;

		this.fireContentInserted(new DocumentEvent(this, parent, offset, s
				.length(), edit));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.sf.vex.dom.impl.Document#insertSpace(int, char)
	 */
	
	
	/**
	 * should be fired whenever an element has changed and no other event handles this
	 */
	protected void fireElementChanged(final IVexElement element) {
		DocumentEvent event = new DocumentEvent(this, element, element.getStartOffset(), element.getEndOffset()-element.getStartOffset(), null);
		getListeners().fireEvent("elementChanged", event);
	}

	/**
	 * Returns a node <var>n</var> containing the <var>offset</var> such that
	 * no child of <var>n</var> contains <var>offset</var>.
	 */
	public LinkedNode getNodeAt(int offset) {
		if (offset < 1 || offset >= this.getLength()) {
			throw new IllegalArgumentException("Illegal offset: " + offset
					+ ". Must be between 1 and n-1");
		}

		LinkedElement parent = (LinkedElement) getRootElement();
		Iterator<IVexNode> iter = parent.getChildNodeIterator();
		LinkedNode child;

		// TODO more readable implementation ...
		while (true) {
			if (!iter.hasNext())
				return parent;
			child = (LinkedNode) iter.next();
			if (child.getStartOffset() <= offset
					&& child.getEndOffset() >= offset) {
				// position inside here? either this or a descendant is the node
				// we look for
				if (child instanceof LinkedElement) {
					if (!child.hasChildren())
						return child;
					// otherwise go on looking
					parent = (LinkedElement) child;
					iter = parent.getChildNodeIterator();
					continue;
				} else
					return child;
			}
		}
	}

	/**
	 * Returns an array of Nodes containing the selected range. The given
	 * offsets must both be directly in the same element.
	 * 
	 * @FIXME when the given range is inside a (text) node, the super
	 *        implementation will return a {@linkplain IVexText text} node
	 *        matching exactly the given range. In the linked model, this would
	 *        mean that the getNodes() method would have to modify the model by
	 *        splitting text nodes at the given offsets. We do not want to do
	 *        this, maybe everything works as is? Evaluate.
	 * 
	 * 
	 * @see net.sf.vex.dom.impl.Document#getNodes(int, int)
	 */
	@Override
	public IVexNode[] getNodes(int startOffset, int endOffset) {

		IVexElement element = this.getElementAt(startOffset);
		if (element != this.getElementAt(endOffset)) {
			throw new IllegalArgumentException("Offsets are unbalanced: "
					+ startOffset + " is in " + element.getName() + ", "
					+ endOffset + " is in "
					+ this.getElementAt(endOffset).getName());
		}

		List<IVexNode> list = new ArrayList<IVexNode>();
		IVexNode[] nodes = element.getChildNodes();
		for (int i = 0; i < nodes.length; i++) {
			IVexNode node = nodes[i];
			if (node.getEndOffset() <= startOffset) {
				continue;
			} else if (node.getStartOffset() >= endOffset) {
				break;
			} else {
				if (node instanceof IVexElement) {
					list.add(node);
				} else if (node instanceof IVexText) {
					IVexText text = (IVexText) node;
					/*
					 * if (text.getStartOffset() < startOffset) {
					 * text.setContent(text.getContent(), startOffset, text
					 * .getEndOffset()); // FIXME does not work } else if
					 * (text.getEndOffset() > endOffset) {
					 * text.setContent(text.getContent(), text
					 * .getStartOffset(), endOffset); // FIXME does not // work }
					 */
					list.add(text);
				} else if (node instanceof IVexNonElement) {
					IVexNonElement ne = (IVexNonElement) node;
					list.add(ne);
				} else {
					throw new IllegalStateException(
							"Unknown node type. Implement right here.");

				}
			}
		}

		return list.toArray(new IVexNode[list.size()]);
	}
	

	public boolean hasValidContent() {
		return ((LinkedElement) getRootElement()).hasValidContent();
	}

	/**
	 * @deprecated Use {@link #printDocument(PrintStream)} instead
	 */
	@Deprecated
	public void printDocument() {
		printDocument(System.out);
	}

	public void printDocument(PrintStream output) {
		output.append("Document("
				+ getText(0, getLength()).replace("\0", "\\0").replace("\n",
						"\\n").replace("\r", "\\r") + ")");
		LinkedElement root = (LinkedElement) getRootElement();
		printElem(root, "", output);
	}

	/**
	 * @deprecated Use {@link #printElem(LinkedElement,String,PrintStream)}
	 *             instead
	 */
	@Deprecated
	protected static void printElem(LinkedElement element, String indent) {
		printElem(element, indent, System.out);
	}

	protected static void printElem(LinkedElement element, String indent,
			PrintStream output) {
		output.print(indent);
		output.println(element.toString() /*
											* + "|" +
											* element.getClass().getSimpleName() + ": " +
											* element.getName() + "(" +
											* element.getStartOffset() + ".." +
											* element.getEndOffset() + ")"
											*/);
		for (Iterator<IVexNode> i = element.getChildNodeIterator(); i.hasNext();) {
			IVexNode node = i.next();

			if (node instanceof IVexElement)
				printElem((LinkedElement) node, indent + " ", output);
			else {
				output.print(indent + " ");
				output.println(node.toString() /*
												* + "|" +
												* node.getClass().getSimpleName() + ": (" +
												* node.getStartOffset() + ".." +
												* node.getEndOffset() + ")"
												*/);// .append('\n');
			}
		}
	}

	/**
	 * Represents a consistency error in the linked data model.
	 */
	protected static class ModelError {
		private final String message;
		private final LinkedNode node;

		public ModelError(String message, LinkedNode node) {
			this.message = message;
			this.node = node;
		}

		public String toString() {
			return "Error: " + message + "\t in " + node;
		}
	}

	/**
	 * Performs a set of consistency checks on the subtree starting at
	 * <var>node</var>.
	 * 
	 * @param node
	 *            the node at which the check will start. If this is a
	 *            {@link LinkedElement}, also check the subtree.
	 * @param broken
	 *            a modifiable list that will receive a {@link ModelError} for
	 *            every consistency problem found.
	 * @param failFast
	 *            if true, abort as soon as one inconsistency has been found
	 * @return true if the subtree is consistent.
	 */
	protected static boolean isSubtreeConsistent(final LinkedNode node, final List<ModelError> broken,
			final boolean failFast) {
		// assertion: startOffset <= endOffset
		if (node.getStartOffset() > node.getEndOffset()) {
			broken.add(new ModelError("Start after end", node));
			if (failFast)
				return false;
		}
		if (node instanceof LinkedElement) {
			final LinkedElement element = (LinkedElement) node;

			if (!"\0".equals(element.getContent().getString(element.getStartOffset(), 1))) {
				broken.add(new ModelError("Element content doesn't start with marker", element));
				if (failFast)
					return false;
			}

			if (element.getEndOffset() < element.getContent().getLength()
					&& !"\0".equals(element.getContent().getString(element.getEndOffset(), 1))) {
				broken.add(new ModelError("Element content doesn't end with marker", element));
				if (failFast)
					return false;
			}

			// TG-846: Inside a consistent subtree, the following assertions should hold:
			// (D1) inside a child list, the DOM node offsets are monotonically increasing
			// (-D2-) the end offset of the DOM node equals the start offset of the following siblings DOM node -- happens frequently during document restructuring
			// (D3) the child regions should be inside the parent region
			// in the broken document in TG-846 we suddenly have nodes from the end of the 
			// DOM document at the beginning of the linked document
			int start = element.getStartOffset();
			IndexedRegion parentRegion = AdapterUtils.getAdapter(element.getDomNode(), IndexedRegion.class);
			/** previous sibling's region */
			IndexedRegion previousRegion = null;
			Iterator<IVexNode> iterator = element.getChildNodeIterator();
			while (iterator.hasNext()) {
				LinkedNode child = (LinkedNode) iterator.next();
				if (child.getStartOffset() < start) {
					broken.add(new ModelError("Starts before it should", child));
					if (failFast)
						return false;
				}
				final IndexedRegion currentRegion = AdapterUtils.getAdapter(child, IndexedRegion.class);
				// (D3)
				if (currentRegion.getStartOffset() < parentRegion.getStartOffset() || currentRegion.getEndOffset() > parentRegion.getEndOffset()) {
					broken.add(new ModelError("DOM node outside of parent's DOM node", child));
					if (failFast) return false;
				}
				if (previousRegion != null) {
					// (D1)
					if (currentRegion.getStartOffset() < previousRegion.getStartOffset()) {
						broken.add(new ModelError("source region before previous node's source region", child));
						if (failFast) return false;
					}
//					// (D2)
//					if (currentRegion.getStartOffset() != previousRegion.getEndOffset()) {
//						broken.add(new ModelError("source regions not continuous", child));
//						if (failFast) return false;
//					}
				}
				start = child.getStartOffset();
				boolean subCheck = isSubtreeConsistent(child, broken, failFast);
				if (failFast && !subCheck)
					return subCheck;

				if (child.getEndOffset() > node.getEndOffset()) {
					broken.add(new ModelError("End of bounds", child));
					if (failFast)
						return false;
				}
				previousRegion = currentRegion;
			}
			if (start > node.getEndOffset()) {
				broken.add(new ModelError("Bounds to tight", node));
				if (failFast)
					return false;
			}
		}
		return broken.isEmpty();
	}

	public void printModelCheckReport(final PrintStream output) {
		final List<ModelError> broken = Lists.newLinkedList();
		isSubtreeConsistent((LinkedElement) getRootElement(), broken, false);
		if (broken.isEmpty())
			output.println("Automatic consistency check didn't find any problems in the WYSIWYM model.");
		else {
			output.println(MessageFormat.format("Found {0} errors in the WYSIWYM model.", broken.size()));
			for (ModelError modelError : broken) {
				output.println(modelError);
			}
		}
	}

	@Override
	public void setWhitespacePolicy(IWhitespacePolicy whitespacePolicy) {
		this.whitespacePolicy = whitespacePolicy;
	}

	/**
	 * @return the whitespacePolicy
	 */
	@Override
	public IWhitespacePolicy getWhitespacePolicy() {
		return whitespacePolicy;
	}

	/**
	 * Retrieves the content region for the given source region.
	 * 
	 * TODO if the source region is, e.g., inside a start tag, the resulting
	 * content region currently covers the whole element including all its
	 * contents. This is probably not what we want?
	 * 
	 * @param sourceRegion
	 * @return
	 */
	public IRegion contentRegionFor(IRegion sourceRegion) {

		/*
		 * We basically have to:
		 * 
		 * 1. find v, the deepest node whose DOM node contains the
		 * sourceRegion's start offset
		 * 
		 * 1'. find v', the deepest node whose DOM node contains the
		 * sourceRegion's end offset. v' cannot be before v.
		 * 
		 * 2. calculate appropriate positions in the contents.
		 */

		int sourceOffset = sourceRegion.getOffset();

		/*
		 * 1. find nodeAtStart
		 */
		LinkedNode nodeAtStart = findNodeAtSourcePos(sourceOffset);
		if (nodeAtStart == null)
			return null; // not found
		IndexedRegion regionAtStart = (IndexedRegion) nodeAtStart
				.getAdapter(IndexedRegion.class);

		/*
		 * 1'. find nodeAtEnd
		 */
		LinkedNode nodeAtEnd;
		if (sourceOffset + sourceRegion.getLength() <= regionAtStart
				.getEndOffset()) {
			nodeAtEnd = nodeAtStart;
		} else {
			nodeAtEnd = findNodeAtSourcePos(sourceOffset
					+ sourceRegion.getLength());
			// TODO optimization: continue from nodeAtStart
		}

		/*
		 * 2. Calculate offsets
		 */

		/*
		 * it's rather easy:
		 * 
		 * If the node is an element node, we place the start offset at the
		 * start of the element and the end offset at it's end. If it's a text
		 * node, things are slightly more difficult:
		 */

		int contentStartOffset;
		int contentEndOffset;

		// Order is important
		if (nodeAtStart instanceof LinkedNonElement) {
			contentStartOffset = ((LinkedNonElement) nodeAtStart)
					.getContentOffsetFor(sourceRegion.getOffset());
		} else if (nodeAtStart instanceof LinkedText) {
			contentStartOffset = ((LinkedText) nodeAtStart)
					.getContentOffsetFor(sourceRegion.getOffset());
		} else {
			contentStartOffset = nodeAtStart.getStartOffset() + 1; // +1 to
			// move
			// behind
			// element
			// start tag
			// on <foo>|
		}

		if (sourceRegion.getLength() == 0)
			contentEndOffset = contentStartOffset;
		// Order is important
		else if (nodeAtEnd instanceof LinkedNonElement) {
			contentEndOffset = ((LinkedNonElement) nodeAtEnd)
					.getContentOffsetFor(sourceRegion.getOffset()
							+ sourceRegion.getLength());
		} else if (nodeAtEnd instanceof LinkedText) {
			contentEndOffset = ((LinkedText) nodeAtEnd)
					.getContentOffsetFor(sourceRegion.getOffset()
							+ sourceRegion.getLength());
		} else {
			contentEndOffset = nodeAtEnd.getEndOffset();
			// FIXME typical for the link editor's annotations is that
			// nodeAtStart and nodeAtEnd both resolve to an (the same) element,
			// which is strange ...
		}

		return new Region(contentStartOffset, contentEndOffset
				- contentStartOffset);
	}

	private LinkedNode findNodeAtSourcePos(int sourceOffset) {
		// initialization
		// TODO this probably takes some time ... implement some mapping!?
		Iterator<IVexNode> children = ((LinkedElement) getRootElement())
				.getChildNodeIterator();
		LinkedNode nodeAtStart = null;

		// DFS
		while (children.hasNext()) {
			LinkedNode linkedNode = (LinkedNode) children.next();
			IndexedRegion indexedRegion = (IndexedRegion) linkedNode
					.getAdapter(IndexedRegion.class);
			if (indexedRegion.getStartOffset() <= sourceOffset
					&& sourceOffset < indexedRegion.getEndOffset()) { // no ≤
				// here?
				nodeAtStart = linkedNode;
				if (nodeAtStart.hasChildren()) {
					children = ((LinkedElement) nodeAtStart)
							.getChildNodeIterator();
					continue;
				} else
					break;
			}
		}

		if (nodeAtStart == null) {
			// TG-615
			// The sourceOffset is outside the root element -> return the root
			// element (we want to return something!)
			return (LinkedNode) getRootElement();
		}

		return nodeAtStart;
	}

	public void dispose() {
		IVexElement rootElement = getRootElement();
		if (rootElement != null && rootElement instanceof LinkedNode)
			((LinkedNode) rootElement).dispose();
	}

	/**
	 * This method compares the nodes <var>left</var> and <var>right</var> nodes
	 * as well as their child lists, expecting equality.
	 * 
	 * @param left
	 *            a node that should be equal to <var>right</var>
	 * @param right
	 *            a node that should be equal to <var>left</var>
	 * @return an {@link IStatus} that will be either
	 *         {@linkplain IStatus#isOK()} or contain a record / description of
	 *         the non-matching nodes.
	 */
	public static IStatus deepCompare(final LinkedNode left, final LinkedNode right) {
		final MultiStatus result = new MultiStatus(VexToolkitPlugin.PLUGIN_ID, 0, MessageFormat.format(
				"There were differences between the nodes {0} and {1} or their children.", left, right), null);

		if (!left.equals(right)) {
			result.add(new Status(IStatus.ERROR, VexToolkitPlugin.PLUGIN_ID, MessageFormat.format(
					"The nodes themselves differ:\n l: {0}\n r: {1}",
					left, right)));
		}

		final Iterator<IVexNode> leftChildren = left.getChildNodeIterator();
		final Iterator<IVexNode> rightChildren = right.getChildNodeIterator();

		while (leftChildren.hasNext()) {
			IVexNode leftChild = leftChildren.next();
			if (rightChildren.hasNext()) {
				IVexNode rightChild = rightChildren.next();
				IStatus childStatus = deepCompare((LinkedNode) leftChild, (LinkedNode) rightChild);
				if (!childStatus.isOK())
					result.add(childStatus); // or should we also log equal
												// children?
			} else {
				result.add(new Status(IStatus.ERROR, VexToolkitPlugin.PLUGIN_ID, MessageFormat.format(
						"{0} has more children than {1}: No match for child {2}", left, right, leftChild)));
			}
		}
		while (rightChildren.hasNext()) {
			IVexNode rightChild = rightChildren.next();
			result.add(new Status(IStatus.ERROR, VexToolkitPlugin.PLUGIN_ID, MessageFormat.format(
					"{0} has less children than {1}: No match for child {2}", left, right, rightChild)));
		}
		return result.isOK() ? Status.OK_STATUS : result;
	}

	
	
}
