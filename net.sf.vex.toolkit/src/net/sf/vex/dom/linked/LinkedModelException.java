/**
 * LinkedModelException.java, part of project vex-toolkit
 * 
 * (C) 2008 by Thorsten Vitt for TextGrid
 */
package net.sf.vex.dom.linked;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.statushandlers.StatusManager;

/**
 * Exception that will be thrown when something with the linked model goes
 * wrong. Users should not get this exception. Creating a LinkedModelException
 * causes {@linkplain #handleError(String, Throwable, Object...) a log message
 * to be written}.
 * 
 * <p>
 * Since LinkedModelExceptions are {@link RuntimeException}, they do not have to
 * be declared.
 * </p>
 * 
 * 
 * @author Thorsten Vitt <vitt@linglit.tu-darmstadt.de> (tv) for TextGrid
 */
public class LinkedModelException extends RuntimeException {

	private static final long serialVersionUID = 6553158178586478115L;

	/**
	 * Creates a new {@link LinkedModelException}.
	 * 
	 * @param message
	 *            The error message. The message may contain strings like
	 *            <code>{0}</code>, <code>{1}</code> etc. which will be replaced
	 *            with string representations of the given arguments.
	 * @param cause
	 *            The original exception that caused this problem, or
	 *            <code>null</code>.
	 * @param arguments
	 *            Arguments that will be filled in for {0}, {1} etc. in the
	 *            message.
	 * @see NLS#bind(String, Object[])
	 */
	public LinkedModelException(String message, Throwable cause,
			Object... arguments) {
		super(handleError(message, cause, arguments).getMessage(), cause);
	}

	/**
	 * Handles the given message as an error. Usually, this means writing it to
	 * the log file.
	 * 
	 * @param message
	 *            The error message. The message may contain strings like
	 *            <code>{0}</code>, <code>{1}</code> etc. which will be replaced
	 *            with string representations of the given arguments.
	 * @param cause
	 *            The originl exception that caused this problem, or
	 *            <code>null</code>.
	 * @param arguments
	 *            Arguments that will be filled in for {0}, {1} etc. in the
	 *            message.
	 * @return a generates {@link IStatus} element for further processing
	 * @see NLS#bind(String, Object[])
	 * @see StatusManager
	 */
	public static IStatus handleError(String message, Throwable cause,
			Object... arguments) {
		Status status = new Status(IStatus.ERROR, "net.sf.vex.toolkit", NLS
				.bind(message, arguments), cause);

		StatusManager.getManager().handle(status);

		return status;
	}
	
	

}
