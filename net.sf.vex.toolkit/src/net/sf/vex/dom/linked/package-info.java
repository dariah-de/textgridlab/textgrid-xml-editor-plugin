/**
 * An implementation of Vex' Data Model that is linked to a W3C DOM implementation
 * as provided by WST's XML editor.
 * 
 * <h2>Vex' Document Object Model</h2>
 * Vex' Document Object Model consists basically of a tree of Elements 
 * (with Attributes) on the one hand and a Content object on the other. 
 * The content object contains the XML documents text contents plus
 * sentinels ('\0') where there were start- or end-tags in the original
 * document. Text nodes are generated on the fly, comments etc. are not
 * preserved.
 * 
 * <h2>Linked DOM</h2>
 * In the linked DOM, the nodes implement Vex' DOM interfaces plus the
 * additional LinkedNode interface. The latter adds a getDOMNode() method
 * by which the original DOM node may be retrieved, and it implements the {@link org.eclipse.wst.sse.core.internal.provisional.INodeAdapter} interface,
 * providing the method {@link LinkedNode#notifyChanged(org.eclipse.wst.sse.core.internal.provisional.INodeNotifier, int, Object, Object, Object, int)} 
 * which is called whenever the original DOM node is modified.
 *  
 * <p>
 * Additionally, we must preserve whitespace, comments, processing instructions 
 * etc. This leads to some changes.
 * 
 * <h3>modified getChildNodes() behaviour</h3>
 * In the original Vex model, each element stores only its child <em>elements</em>.
 * getChildNodes() generates IVexText nodes between the elements whenever 
 * getChildNodes() is called.
 * <p>
 * With the linked model, we store all child nodes (with their links to the
 * corresponding DOM nodes). getChildNodes returns them all, getChildElements
 * filters everything but the element nodes.
 *  
 * @author Thorsten Vitt <vitt@linglit.tu-darmstadt.de> for TextGrid, see <a href="http://www.textgrid.info/">www.textgrid.info</a>
 */
package net.sf.vex.dom.linked;

