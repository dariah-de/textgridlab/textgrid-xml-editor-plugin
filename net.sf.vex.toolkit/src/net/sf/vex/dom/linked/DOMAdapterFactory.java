/**
 * 
 */
package net.sf.vex.dom.linked;

import java.util.Iterator;

import net.sf.vex.dom.IVexNode;

import org.eclipse.core.runtime.IAdapterFactory;
import org.eclipse.wst.sse.core.internal.provisional.INodeAdapter;
import org.eclipse.wst.sse.core.internal.provisional.INodeNotifier;

/**
 * Can adapt IDOMNodes to LinkedNodes or IVexNodes, if they are registered with
 * the respective INodeNotifier list.
 * 
 * @author Thorsten Vitt for <a href="http://www.textgrid.de/">TextGrid</a> (tv)
 * 
 */
@SuppressWarnings("restriction")
public class DOMAdapterFactory implements IAdapterFactory {
	
	public static final Class<?>[] ADAPTER_LIST = { IVexNode.class }; 

	/* (non-Javadoc)
	 * @see org.eclipse.core.runtime.IAdapterFactory#getAdapter(java.lang.Object, java.lang.Class)
	 */
	@SuppressWarnings("unchecked")
	public Object getAdapter(Object adaptableObject, Class adapterType) {
		if (adaptableObject instanceof INodeNotifier) {
			INodeNotifier nodeNotifier = (INodeNotifier) adaptableObject;
			
			for (Iterator iterator = nodeNotifier.getAdapters().iterator(); iterator
					.hasNext();) {
				INodeAdapter nodeAdapter = (INodeAdapter) iterator.next();
				if (nodeAdapter instanceof IVexNode)
					return nodeAdapter;
				
			}
				
		}
		
		return null;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.core.runtime.IAdapterFactory#getAdapterList()
	 */
	@SuppressWarnings("unchecked")
	public Class[] getAdapterList() {
		return ADAPTER_LIST;
	}

}
