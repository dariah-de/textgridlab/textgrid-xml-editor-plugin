package net.sf.vex.dom;

/**
 * IVexNode represents a node in an XML document in Vex' own data model.
 */
public interface IVexNode {

	/**
	 * Returns the document associated with this node. Null if the node
	 * has not yet been inserted into a document.
	 */
	public Content getContent();

	/**
	 * Returns the character offset corresponding to the end of the
	 * node.
	 */
	public int getEndOffset();

	/**
	 * Returns the <code>Position</code> corresponding to the end of
	 * the node.
	 */
	public Position getEndPosition();

	/**
	 * Returns the character offset corresponding to the start of the
	 * node.
	 */
	public int getStartOffset();

	/**
	 * Returns the <code>Position</code> corresponding to the start of
	 * the node.
	 */
	public Position getStartPosition();

	/**
	 * Returns the text contained by this node. If this node is an element,
	 * the text in all child nodes is included.
	 */
	public String getText();
	
	
	void setContent(Content content, int startOffset, int endOffset);

}