/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2004 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.dom.impl;

import net.sf.vex.dom.IVexDocument;
import net.sf.vex.dom.IVexRootElement;

/**
 * The root element of a document. Keeps track of the document to which
 * it is associated. Any element can find the document to which it is
 * associated by following its parents to this root. This would be done,
 * for example, to notify document listeners that the document has changed
 * when the element changes.
 */
public class RootElement extends Element implements IVexRootElement {

    private IVexDocument document;
    
    /**
     * Class constructor
     * @param name Name of the element.
     */
    public RootElement(String name) {
        super(name);
    }

    /* (non-Javadoc)
	 * @see net.sf.vex.dom.IVexRootElement#getDocument()
	 */
    public IVexDocument getDocument() {
        return document;
    }

    /* (non-Javadoc)
	 * @see net.sf.vex.dom.IVexRootElement#setDocument(net.sf.vex.dom.impl.Document)
	 */
    public void setDocument(IVexDocument document) {
        this.document = document;
    }

}
