/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2004 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.dom.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;

import net.sf.vex.dom.DocumentEvent;
import net.sf.vex.dom.DocumentValidationException;
import net.sf.vex.dom.IVexDocument;
import net.sf.vex.dom.IVexElement;
import net.sf.vex.dom.IVexNode;
import net.sf.vex.dom.IVexRootElement;
import net.sf.vex.dom.IWhitespacePolicy;
import net.sf.vex.undo.IUndoableEdit;

/**
 * <code>Element</code> represents a tag in an XML document. Methods
 * are available for managing the element's attributes and children.
 */
public class Element extends Node implements Cloneable, IVexElement {

    private String name;
    private IVexElement parent = null;
    private List children = new ArrayList();
    private Map attributes = new HashMap();



    /**
     * Class constructor.
     * @param name element name
     */
    public Element(String name) {
	this.name = name;
    }

    /* (non-Javadoc)
	 * @see net.sf.vex.dom.IVexElement#addChild(net.sf.vex.dom.Element)
	 */
    public void addChild(IVexElement child) {
    	WrongModelException.throwIfNeeded(child, Element.class);
        this.children.add(child);
        ((Element) child).parent = this;
    }
    

    /* (non-Javadoc)
	 * @see net.sf.vex.dom.IVexElement#getAttribute(java.lang.String)
	 */
    public String getAttribute(String name) {
	return (String) attributes.get(name);
    }

    /* (non-Javadoc)
	 * @see net.sf.vex.dom.IVexElement#getAttributeNames()
	 */
    public String[] getAttributeNames() {
	Collection names = this.attributes.keySet();
	return (String[]) names.toArray(new String[names.size()]);
    }

    /* (non-Javadoc)
	 * @see net.sf.vex.dom.IVexElement#getChildIterator()
	 */
    public Iterator getChildIterator() {
	return this.children.iterator();
    }

    /* (non-Javadoc)
	 * @see net.sf.vex.dom.IVexElement#getChildElements()
	 */
    public IVexElement[] getChildElements() {
	int size = this.children.size();
	return (IVexElement[]) this.children.toArray(new IVexElement[size]);
    }

    /* (non-Javadoc)
	 * @see net.sf.vex.dom.IVexElement#getChildNodes()
	 */
    public IVexNode[] getChildNodes() {
        return Document.createNodeArray(
            this.getContent(),
            this.getStartOffset() + 1,
            this.getEndOffset(),
            this.getChildElements());
    }

    /* (non-Javadoc)
	 * @see net.sf.vex.dom.IVexElement#getDocument()
	 */
    public IVexDocument getDocument() {
        IVexElement root = this;
        while (root.getParent() != null) {
            root = root.getParent(); 
        }
        if (root instanceof IVexRootElement) {
            return ((IVexRootElement) root).getDocument();
        } else {
            return null;
        }
    }
    
    /**
	 * Clones the element and its attributes. The returned element has
	 * no parent or children.
	 */
	@Override
	public IVexElement clone() { 
	    try {
	        IVexElement element = new Element(this.getName());
	        for (Iterator it = this.attributes.keySet().iterator(); it
	                .hasNext();) {
	            String attrName = (String) it.next();
	            element.setAttribute(attrName, (String) this.attributes
	                    .get(attrName));
	        }
	        return element;
	        
	    } catch (DocumentValidationException ex) {
	        ex.printStackTrace();
	        return null;
	    }
	}

	/* (non-Javadoc)
	 * @see net.sf.vex.dom.IVexElement#getName()
	 */
    public String getName() {
	return this.name;
    }

    /* (non-Javadoc)
	 * @see net.sf.vex.dom.IVexElement#getParent()
	 */
    public IVexElement getParent() {
	return this.parent;
    }

    
    /* (non-Javadoc)
	 * @see net.sf.vex.dom.IVexElement#getText()
	 */
    @Override
	public String getText() {
        String s = super.getText();
        StringBuffer sb = new StringBuffer(s.length());
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (c != 0) {
                sb.append(c);
            }
        }
        return sb.toString();
    }
    
    /**
     * Inserts the given element as a child at the given child index.
     * Sets the parent attribute of the given element to this element.
     */
    public void internalInsertChild(int index, IVexElement child) {
    	this.children.add(index, child);
		child.setParent(this);
    }
    
    /* (non-Javadoc)
	 * @see net.sf.vex.dom.IVexElement#isEmpty()
	 */
    public boolean isEmpty() {
        return this.getStartOffset() + 1 == this.getEndOffset();
    }

    /* (non-Javadoc)
	 * @see net.sf.vex.dom.IVexElement#removeAttribute(java.lang.String)
	 */
    public void removeAttribute(String name) 
        throws DocumentValidationException {

        String oldValue = this.getAttribute(name);
        String newValue = null;            
	if (oldValue != null) {
	    this.attributes.remove(name);
	}
        Document doc = (Document) this.getDocument();
        if (doc != null) { // doc may be null, e.g. when we're cloning an element
                           // to produce a document fragment
            
            IUndoableEdit edit = doc.isUndoEnabled() ?
                    new AttributeChangeEdit(name, oldValue, newValue) : null;
                    
            doc.fireAttributeChanged(new DocumentEvent(
                    doc, this, name, oldValue, newValue, edit));
        }
    }

    /* (non-Javadoc)
	 * @see net.sf.vex.dom.IVexElement#setAttribute(java.lang.String, java.lang.String)
	 */
    public void setAttribute(String name, String value) 
        throws DocumentValidationException {
            
        String oldValue = this.getAttribute(name);
        
        if (value == null && oldValue == null) {
            return;
        } else if (value == null) {
            this.removeAttribute(name);
        } else if (value.equals(oldValue)) {
            return;
        } else {
            this.attributes.put(name, value);
            Document doc = (Document) this.getDocument();
            if (doc != null) { // doc may be null, e.g. when we're cloning an element
                               // to produce a document fragment
                
                IUndoableEdit edit = doc.isUndoEnabled() ?
                        new AttributeChangeEdit(name, oldValue, value) : null;
                        
                doc.fireAttributeChanged(new DocumentEvent(
                        doc, this, name, oldValue, value, edit));
            }
        }
        

    }

    /* (non-Javadoc)
	 * @see net.sf.vex.dom.IVexElement#setParent(net.sf.vex.dom.Element)
	 */
    public void setParent(IVexElement parent) {
	this.parent = parent;
    }

    /* (non-Javadoc)
	 * @see net.sf.vex.dom.IVexElement#toString()
	 */
    @Override
	public String toString() {
        
	StringBuffer sb = new StringBuffer();
	sb.append("<");
	sb.append(this.getName());
	String[] attrs = this.getAttributeNames();
	
	for (int i = 0; i < attrs.length; i++) {
	    if (i > 0) {
	    	sb.append(",");
	    }
	    sb.append(" ");
	    sb.append(attrs[i]);
	    sb.append("=\"");
	    sb.append(this.getAttribute(attrs[i]));
	    sb.append("\"");
	}
    
	sb.append("> (");
	sb.append(this.getStartPosition());
	sb.append(",");
	sb.append(this.getEndPosition());
	sb.append(")");
    
	return sb.toString();
    }
    
    public boolean isPre() {
		IWhitespacePolicy policy = getDocument().getWhitespacePolicy();
		if (policy != null)
			return policy.isPre(this);
		else
			return false;
	}

    //========================================================= PRIVATE

    
	private class AttributeChangeEdit implements IUndoableEdit {

        private String name;
        private String oldValue;
        private String newValue;
    
        public AttributeChangeEdit(String name, String oldValue, String newValue) {
            this.name = name;
            this.oldValue = oldValue;
            this.newValue = newValue;
        }

        public boolean combine(IUndoableEdit edit) {
            return false;
        }

        public void undo() throws CannotUndoException {
            IVexDocument doc = getDocument();
            try {
                doc.setUndoEnabled(false);
                setAttribute(name, oldValue);
            } catch (DocumentValidationException ex) {
                throw new CannotUndoException();
            } finally {
                doc.setUndoEnabled(true);
            }
        }

        public void redo() throws CannotRedoException {
            IVexDocument doc = getDocument();
            try {
                doc.setUndoEnabled(false);
                setAttribute(name, newValue);
            } catch (DocumentValidationException ex) {
                throw new CannotUndoException();
            } finally {
                doc.setUndoEnabled(true);
            }
        }
    }


}

