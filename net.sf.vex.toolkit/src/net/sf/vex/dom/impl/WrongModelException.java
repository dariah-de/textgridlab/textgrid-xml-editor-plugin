package net.sf.vex.dom.impl;

/**
 * This exception is thrown when you mix different implementations of
 * IVexElement etc.
 * 
 * Methods of classes in {@link net.sf.vex.dom.impl} use the respective
 * interface in their signatures, but often expect the arguments to be of the
 * net.sf.vex.dom.impl model. If you try to pass in an element of the wrong
 * model, you'll get this exception.
 * 
 * @author tv
 */
public class WrongModelException extends IllegalArgumentException {

	private static final long serialVersionUID = -2401502806869195419L;

	public WrongModelException(Class illegalClass, Class legalClass) {
		super(
				String
						.format(
								"Expected %s, got %s: You cannot mix different net.sf.vex.dom model implementations",
								legalClass, illegalClass));
	}

	/**
	 * Convenience method to check whether <var>objectToCheck</var> is an
	 * instance of the <var>expectedClass</var>. If it is not, then
	 * a @link{WrongModelException} is thrown.
	 * 
	 * @throws {@link WrongModelException}
	 */
	public static void throwIfNeeded(Object objectToCheck, Class expectedClass) {
		if (!expectedClass.isInstance(objectToCheck)) {
			throw new WrongModelException(objectToCheck.getClass(),
					expectedClass);
		}
	}

}
