/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2004 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.dom.impl;

import net.sf.vex.dom.Content;
import net.sf.vex.dom.IVexNode;
import net.sf.vex.dom.Position;

/**
 * <code>Node</code> represents a component of an XML document. .
 */
public class Node implements IVexNode {

    private Content content = null;
    private Position start = null;
    private Position end = null;

    /**
     * Class constructor.
     */
    public Node() {
    }


    /* (non-Javadoc)
	 * @see net.sf.vex.dom.IVexNode#getContent()
	 */
    public Content getContent() {
	return this.content;
    }
    
    /* (non-Javadoc)
	 * @see net.sf.vex.dom.IVexNode#getEndOffset()
	 */
    public int getEndOffset() {
	return this.end.getOffset();
    }

    /* (non-Javadoc)
	 * @see net.sf.vex.dom.IVexNode#getEndPosition()
	 */
    public Position getEndPosition() {
	return this.end;
    }

    /* (non-Javadoc)
	 * @see net.sf.vex.dom.IVexNode#getStartOffset()
	 */
    public int getStartOffset() {
	return this.start.getOffset();
    }

    /* (non-Javadoc)
	 * @see net.sf.vex.dom.IVexNode#getStartPosition()
	 */
    public Position getStartPosition() {
	return this.start;
    }

    /* (non-Javadoc)
	 * @see net.sf.vex.dom.IVexNode#getText()
	 */
    public String getText() {
	return this.content.getString(this.getStartOffset(),
				      this.getEndOffset() - this.getStartOffset());
    }

    /**
     * Sets the content of the node
     * 
     * FIXME: Visibility was package before refactoring -> cf. DocumentBuilder.java:101
     * 
     * @param content Content object holding the node's content
     * @param startOffset offset at which the node's content starts
     * @param endOffset offset at which the node's content ends
     */
    public void setContent(Content content, 
		    int startOffset, 
		    int endOffset) {

	this.content = content;
	if (startOffset > endOffset)
			startOffset = endOffset;
	
	this.start = content.createPosition(startOffset);
	this.end = content.createPosition(endOffset);
	
    }

	/**
	 * Sets this element's content element only, without fixing the positions.
	 * 
	 * @param content
	 */
	protected void setContent(Content content) {
		this.content = content;
	}

}

