/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2004 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.dom.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;

import net.sf.vex.core.ListenerList;
import net.sf.vex.dom.Content;
import net.sf.vex.dom.DocumentEvent;
import net.sf.vex.dom.DocumentListener;
import net.sf.vex.dom.DocumentValidationException;
import net.sf.vex.dom.IVexDocument;
import net.sf.vex.dom.IVexDocumentFragment;
import net.sf.vex.dom.IVexElement;
import net.sf.vex.dom.IVexNode;
import net.sf.vex.dom.IVexRootElement;
import net.sf.vex.dom.IVexText;
import net.sf.vex.dom.IWhitespacePolicy;
import net.sf.vex.dom.Position;
import net.sf.vex.dom.Validator;
import net.sf.vex.undo.IUndoableEdit;

/**
 * Represents an XML document.
 */
public class Document implements IVexDocument {

	
	private String cssfile_location;
	 
    private Content content;
    private IVexRootElement rootElement;
    private ListenerList listeners = new ListenerList(DocumentListener.class, DocumentEvent.class);
    protected ListenerList getListeners() {
		return listeners;
	}

	private boolean undoEnabled = true;
    
    private String publicID;
    private String systemID;
    private String encoding;
    private Validator validator;
	private IWhitespacePolicy whitespacePolicy;

    /**
     * Class constructor.
     * @param rootElement root element of the document. The
     * document property of this RootElement is set by this
     * constructor.
     */
    public Document(IVexRootElement rootElement) {
	this.content = new SwingGapContentWrapper(100);
	this.setRootElement(rootElement);
        rootElement.setDocument(this);
	this.content.insertString(0, "\0\0");
	rootElement.setContent(this.content, 0, 1);
    }


	/**
	 * Class constructor. This constructor is used by the document builder and
	 * assumes that the content and root element have bee properly set up.
	 * 
	 * @param content
	 *            Content object used to store the document's content.
	 * @param rootElement
	 *            RootElement of the document.
	 */
    public Document(Content content, RootElement rootElement) {
        this.content = content;
        this.setRootElement(rootElement);
    }
    
    /* (non-Javadoc)
	 * @see net.sf.vex.dom.IVexDocumnt#addDocumentListener(net.sf.vex.dom.DocumentListener)
	 */
    public void addDocumentListener(DocumentListener listener) {
        this.listeners.add(listener);
    }

    /* (non-Javadoc)
	 * @see net.sf.vex.dom.IVexDocumnt#canInsertFragment(int, net.sf.vex.dom.DocumentFragment)
	 */
    public boolean canInsertFragment(int offset, IVexDocumentFragment fragment) {
        
        if (this.validator == null) {
            return true;
        }
        
        IVexElement element = this.getElementAt(offset);
        String[] seq1 = this.getNodeNames(element.getStartOffset() + 1, offset);
        String[] seq2 = fragment.getNodeNames();
        String[] seq3 = this.getNodeNames(offset, element.getEndOffset());
        return this.validator.isValidSequence(
            element.getName(),
            seq1, seq2, seq3, true);
    }
    
    /* (non-Javadoc)
	 * @see net.sf.vex.dom.IVexDocumnt#canInsertText(int)
	 */
    public boolean canInsertText(int offset) {
        
        if (this.validator == null) {
            return true;
        }
        
        IVexElement element = this.getElementAt(offset);
        String[] seq1 = this.getNodeNames(element.getStartOffset() + 1, offset);
        String[] seq2 = new String[] { Validator.PCDATA };
        String[] seq3 = this.getNodeNames(offset, element.getEndOffset());
        
        return this.validator.isValidSequence(
            element.getName(), seq1, seq2, seq3, true);        
    }
    
    /* (non-Javadoc)
	 * @see net.sf.vex.dom.IVexDocumnt#createPosition(int)
	 */
    public Position createPosition(int offset) {
	return this.content.createPosition(offset);
    }

    /* (non-Javadoc)
	 * @see net.sf.vex.dom.IVexDocumnt#delete(int, int)
	 */
    public void delete(int startOffset, int endOffset)
	throws DocumentValidationException {

        IVexElement e1 = this.getElementAt(startOffset);
        IVexElement e2 = this.getElementAt(endOffset);
        if (e1 != e2) {
            throw new IllegalArgumentException("Deletion from " + startOffset + " to " + endOffset + " is unbalanced");
        }

	Validator validator = this.getValidator();
	if (validator != null) {
            String[] seq1 = this.getNodeNames(e1.getStartOffset() + 1, startOffset);
            String[] seq2 = this.getNodeNames(endOffset, e1.getEndOffset());
	    if (!validator.isValidSequence(
                    e1.getName(), seq1, seq2, null, true)) {
		throw new DocumentValidationException("Unable to delete from " + startOffset + " to " + endOffset);
	    }
	}
    
        // Grab the fragment for the undoable edit while it's still here
        IVexDocumentFragment frag = getFragment(startOffset, endOffset);

        this.fireBeforeContentDeleted( 
            new DocumentEvent(this, e1, startOffset, endOffset - startOffset, null));

	Iterator iter = e1.getChildIterator();
	while (iter.hasNext()) {
	    IVexElement child = (IVexElement) iter.next();
	    if (startOffset <= child.getStartOffset() &&
		child.getEndOffset() < endOffset) {
		iter.remove();
	    }
	}

	this.content.remove(startOffset, endOffset - startOffset);

        IUndoableEdit edit = this.undoEnabled ? 
                new DeleteEdit(startOffset, endOffset, frag) : null;
    
        this.fireContentDeleted( 
            new DocumentEvent(this, e1, startOffset, endOffset - startOffset, edit));
    }

    /* (non-Javadoc)
	 * @see net.sf.vex.dom.IVexDocumnt#findCommonElement(int, int)
	 */
    public IVexElement findCommonElement(int offset1, int offset2) {
	IVexElement element = this.getRootElement();
	for (;;) {
	    boolean tryAgain = false;
	    IVexElement[] children = element.getChildElements();
	    for (int i = 0; i < children.length; i++) {
		if (offset1 > children[i].getStartOffset() &&
		    offset2 > children[i].getStartOffset() &&
		    offset1 <= children[i].getEndOffset() &&
		    offset2 <= children[i].getEndOffset()) {
		    
		    element = children[i];
		    tryAgain = true;
		    break;
		}
	    }
	    if (!tryAgain) {
		break;
	    }
	}
	return element;
    }

    /* (non-Javadoc)
	 * @see net.sf.vex.dom.IVexDocumnt#getCharacterAt(int)
	 */
    public char getCharacterAt(int offset) {
	return this.content.getString(offset, 1).charAt(0);
    }

    /* (non-Javadoc)
	 * @see net.sf.vex.dom.IVexDocumnt#getElementAt(int)
	 */
    public IVexElement getElementAt(int offset) {
	if (offset < 1 || offset >= this.getLength()) {
	    throw new IllegalArgumentException("Illegal offset: " + offset + ". Must be between 1 and " + this.getLength());
	}
	IVexElement element = this.getRootElement();
	for (;;) {
	    boolean tryAgain = false;
	    IVexElement[] children = element.getChildElements();
	    for (int i = 0; i < children.length; i++) {
		IVexElement child = children[i];
		if (offset <= child.getStartOffset()) {
		    return element;
		} else if (offset <= child.getEndOffset()) {
		    element = child;
		    tryAgain = true;
		    break;
		}
	    }
	    if (!tryAgain) {
		break;
	    }
	}
	return element;
    }

    /* (non-Javadoc)
	 * @see net.sf.vex.dom.IVexDocumnt#getEncoding()
	 */
    public String getEncoding() {
	return this.encoding;
    }

    /* (non-Javadoc)
	 * @see net.sf.vex.dom.IVexDocumnt#getFragment(int, int)
	 */
    public IVexDocumentFragment getFragment(int startOffset, int endOffset) {
    
        assertOffset(startOffset, 0, this.content.getLength());
        assertOffset(endOffset, 0, this.content.getLength());
            
        if (endOffset <= startOffset) {
            throw new IllegalArgumentException(
                "Invalid range (" + startOffset + ", " + endOffset + ")");
        }
        
        IVexElement e1 = this.getElementAt(startOffset);
        IVexElement e2 = this.getElementAt(endOffset);
        if (e1 != e2) {
            throw new IllegalArgumentException(
                "Fragment from "
                    + startOffset
                    + " to "
                    + endOffset
                    + " is unbalanced");
        }

        IVexElement[] children = e1.getChildElements();
        
        Content newContent = new SwingGapContentWrapper(endOffset - startOffset);
        String s = this.content.getString(startOffset, endOffset - startOffset);
        newContent.insertString(0, s);
        List newChildren = new ArrayList();
        for (int i = 0; i < children.length; i++) {
            IVexElement child = children[i];
            if (child.getEndOffset() <= startOffset) {
                continue;
            } else if (child.getStartOffset() >= endOffset) {
                break;
            } else {
                newChildren.add(
                    this.cloneElement(child, newContent, -startOffset, null));
            }
        }
        
        IVexElement[] elementArray =
            (IVexElement[]) newChildren.toArray(new IVexElement[newChildren.size()]);
        return new DocumentFragment(newContent, elementArray);
    }
    
    /* (non-Javadoc)
	 * @see net.sf.vex.dom.IVexDocumnt#getLength()
	 */
    public int getLength() {
	return this.content.getLength();
    }

    /* (non-Javadoc)
	 * @see net.sf.vex.dom.IVexDocumnt#getNodeNames(int, int)
	 */
    public String[] getNodeNames(int startOffset, int endOffset) {

        IVexNode[] nodes = this.getNodes(startOffset, endOffset);
        String[] names = new String[nodes.length];
        
        for (int i = 0; i < nodes.length; i++) {
            IVexNode node = nodes[i];
            if (node instanceof IVexElement) {
                names[i] = ((IVexElement)node).getName();
            } else {
                names[i] = Validator.PCDATA;
            }
        }

        return names;
    }
    
    /* (non-Javadoc)
	 * @see net.sf.vex.dom.IVexDocumnt#getNodes(int, int)
	 */
    public IVexNode[] getNodes(int startOffset, int endOffset) {

        IVexElement element = this.getElementAt(startOffset);
        if (element != this.getElementAt(endOffset)) {
            throw new IllegalArgumentException(
                "Offsets are unbalanced: " + 
                startOffset + " is in " + element.getName() + ", " + 
                endOffset + " is in " + this.getElementAt(endOffset).getName());
        }

        List list = new ArrayList();
        IVexNode[] nodes = element.getChildNodes();
        for (int i = 0; i < nodes.length; i++) {
            IVexNode node = nodes[i];
            if (node.getEndOffset() <= startOffset) {
                continue;
            } else if (node.getStartOffset() >= endOffset) {
                break;
            } else {
                if (node instanceof IVexElement) {
                    list.add(node);
                } else {
                    IVexText text = (IVexText) node;
                    if (text.getStartOffset() < startOffset) {
                        text.setContent(text.getContent(), startOffset, text.getEndOffset());
                    } else if (text.getEndOffset() > endOffset) {
                        text.setContent(text.getContent(), text.getStartOffset(), endOffset);
                    }
                    list.add(text);
                }
            }
        }

        return (IVexNode[]) list.toArray(new IVexNode[list.size()]);
    }
    
    /**
     * Creates an array of nodes for a given run of content. The returned array includes the given child
     * elements and <code>Text</code> objects where text appears between elements.
     * 
     * @param content Content object containing the content
     * @param startOffset start offset of the run
     * @param endOffset end offset of the run
     * @param elements child elements that are within the run
     */
    static IVexNode[] createNodeArray(Content content, int startOffset, int endOffset, IVexElement[] elements) {

        List nodes = new ArrayList();
        int offset = startOffset;
        for (int i = 0; i < elements.length; i++) {
            int start = elements[i].getStartOffset();
            if (offset < start) {
                nodes.add(new Text(content, offset, start));
            }
            nodes.add(elements[i]);
            offset = elements[i].getEndOffset() + 1;
        }

        if (offset < endOffset) {
            nodes.add(new Text(content, offset, endOffset));
        }

        return (IVexNode[]) nodes.toArray(new IVexNode[nodes.size()]);
    }

    /* (non-Javadoc)
	 * @see net.sf.vex.dom.IVexDocumnt#getPublicID()
	 */
    public String getPublicID() {
	return this.publicID;
    }

    /* (non-Javadoc)
	 * @see net.sf.vex.dom.IVexDocumnt#getRawText(int, int)
	 */
    public String getRawText(int startOffset, int endOffset) {
	return this.content.getString(startOffset, 
				      endOffset - startOffset);
    }

    /* (non-Javadoc)
	 * @see net.sf.vex.dom.IVexDocumnt#getRootElement()
	 */
    public IVexElement getRootElement() {
        return this.rootElement;
    }

    /* (non-Javadoc)
	 * @see net.sf.vex.dom.IVexDocumnt#getSystemID()
	 */
    public String getSystemID() {
	return this.systemID;
    }

    /* (non-Javadoc)
	 * @see net.sf.vex.dom.IVexDocumnt#getText(int, int)
	 */
    public String getText(int startOffset, int endOffset) {
	String raw = this.content.getString(startOffset, 
					    endOffset - startOffset);
	StringBuffer sb = new StringBuffer(raw.length());
	for (int i = 0; i < raw.length(); i++) {
	    char c = raw.charAt(i);
	    if (c != '\0') {
		sb.append(c);
	    }
	}
	return sb.toString();
    }

    /* (non-Javadoc)
	 * @see net.sf.vex.dom.IVexDocumnt#getValidator()
	 */
    public Validator getValidator() {
        return this.validator;
    }

    public void setContent(Content content) {
		this.content = content;
	}

	protected Content getContent() {
		return content;
	}

	/* (non-Javadoc)
	 * @see net.sf.vex.dom.IVexDocumnt#insertElement(int, net.sf.vex.dom.Element)
	 */
    public void insertElement(int offset, IVexElement elementArg)
	throws DocumentValidationException {

    	WrongModelException.throwIfNeeded(elementArg, Element.class);
    
    IVexElement element = elementArg;
    
	if (offset < 1 || offset >= this.getLength()) {
	    throw new IllegalArgumentException("Error inserting element <" + element.getName() + ">: offset is " + offset + ", but it must be between 1 and " + (this.getLength() - 1));
	}

	Validator validator = this.getValidator();
	if (validator != null) {
            IVexElement parent = this.getElementAt(offset);
            String[] seq1 = this.getNodeNames(parent.getStartOffset() + 1, offset);
            String[] seq2 = new String[] { element.getName() };
            String[] seq3 = this.getNodeNames(offset, parent.getEndOffset());
            if (!validator.isValidSequence(
                    parent.getName(), seq1, seq2, seq3, true)) {
		throw new DocumentValidationException("Cannot insert element " + element.getName() + " at offset " + offset);
	    }		
	}

	// find the parent, and the index into its children at which
	// this element should be inserted
		IVexElement parent = this.getRootElement();
	int childIndex = -1;
	while (childIndex == -1) {
	    boolean tryAgain = false;
	    IVexElement[] children = parent.getChildElements();
	    for (int i = 0; i < children.length; i++) {
		IVexElement child = children[i];
		if (offset <= child.getStartOffset()) {
		    childIndex = i;
		    break;
		} else if (offset <= child.getEndOffset()) {
		    parent = child;
		    tryAgain = true;
		    break;
		}
	    }
	    if (!tryAgain && childIndex == -1) {
		childIndex = children.length;
		break;
	    }
	}

        this.fireBeforeContentInserted(new DocumentEvent(this, parent, offset, 2, null));
        
	this.content.insertString(offset, "\0\0");

	element.setContent(this.content, offset, offset + 1);
	element.setParent(parent);
	parent.internalInsertChild(childIndex, element);

        IUndoableEdit edit = this.undoEnabled ? 
                new InsertElementEdit(offset, element) : null;
    
	this.fireContentInserted(new DocumentEvent(this, parent, offset, 2, edit));
    }


    /* (non-Javadoc)
	 * @see net.sf.vex.dom.IVexDocumnt#insertFragment(int, net.sf.vex.dom.DocumentFragment)
	 */
    public void insertFragment(int offset, IVexDocumentFragment fragment)
        throws DocumentValidationException {
         
        if (offset < 1 || offset >= this.getLength()) {
            throw new IllegalArgumentException("Error inserting document fragment");
        }

        IVexElement parent = this.getElementAt(offset);

        if (this.validator != null) {
            String[] seq1 = this.getNodeNames(parent.getStartOffset() + 1, offset);
            String[] seq2 = fragment.getNodeNames();
            String[] seq3 = this.getNodeNames(offset, parent.getEndOffset());
            if (!validator.isValidSequence(
                    parent.getName(), seq1, seq2, seq3, true)) {
             
                throw new DocumentValidationException("Cannot insert document fragment");
            }
        }

        this.fireBeforeContentInserted(new DocumentEvent(this, parent, offset, 2, null));
        
        Content c = fragment.getContent();
        String s = c.getString(0, c.getLength());
        this.content.insertString(offset, s);
        
        IVexElement[] children = parent.getChildElements();
        int index = 0;
        while (index < children.length 
            && children[index].getEndOffset() < offset) {
            index++;
        }
        
        IVexElement[] elements = fragment.getElements();
        for (int i = 0; i < elements.length; i++) {
            IVexElement newElement = this.cloneElement(elements[i], this.content, offset, parent);
            parent.internalInsertChild(index, newElement);
            index++;
        }
        
        IUndoableEdit edit = this.undoEnabled ?
                new InsertFragmentEdit(offset, fragment) : null;
                
        this.fireContentInserted(
            new DocumentEvent(this, parent, offset, fragment.getContent().getLength(), edit));
    }
    
    /* (non-Javadoc)
	 * @see net.sf.vex.dom.IVexDocumnt#insertText(int, java.lang.String)
	 */
    public void insertText(int offset, String text)
	throws DocumentValidationException {

	if (offset < 1 || offset >= this.getLength()) {
	    throw new IllegalArgumentException("Offset must be between 1 and n-1");
	}

        IVexElement parent = this.getElementAt(offset);

	boolean isValid = false;
	if (this.getCharacterAt(offset-1) != '\0') {
	    isValid = true;
	} else if (this.getCharacterAt(offset) != '\0') {
	    isValid = true;
	} else {
	    Validator validator = this.getValidator();
	    if (validator != null) {
                String[] seq1 = this.getNodeNames(parent.getStartOffset() + 1, offset);
                String[] seq2 = new String[] { Validator.PCDATA };
                String[] seq3 = this.getNodeNames(offset, parent.getEndOffset());
                isValid = validator.isValidSequence(
                    parent.getName(), seq1, seq2, seq3, true);
	    } else {
		isValid = true;
	    }
	}

	if (!isValid) {
	    throw new DocumentValidationException("Cannot insert text '" + text + "' at offset " + offset);
	}
    
	// Convert control chars to spaces
	StringBuffer sb = new StringBuffer(text);
	for (int i = 0; i < sb.length(); i++) {
	    if (Character.isISOControl(sb.charAt(i)) && sb.charAt(i) != '\n') {
	        sb.setCharAt(i, ' ');
	    }
	}
    
	String s = sb.toString();

        this.fireBeforeContentInserted(new DocumentEvent(this, parent, offset, 2, null));
        
	this.content.insertString(offset, s);

	IUndoableEdit edit = this.undoEnabled ?
            new InsertTextEdit(offset, s) : null;
            
	this.fireContentInserted(
            new DocumentEvent(this, parent, offset, s.length(), edit));
    }

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.sf.vex.dom.IVexDocumnt#isUndoEnabled()
	 */
    public boolean isUndoEnabled() {
        return this.undoEnabled;
    }
    
    /* (non-Javadoc)
	 * @see net.sf.vex.dom.IVexDocumnt#removeDocumentListener(net.sf.vex.dom.DocumentListener)
	 */
    public void removeDocumentListener(DocumentListener listener) {
        this.listeners.remove(listener);
    }

    /* (non-Javadoc)
	 * @see net.sf.vex.dom.IVexDocumnt#setPublicID(java.lang.String)
	 */
    public void setPublicID(String publicID) {
	this.publicID = publicID;
    }

    /* (non-Javadoc)
	 * @see net.sf.vex.dom.IVexDocumnt#setSystemID(java.lang.String)
	 */
    public void setSystemID(String systemID) {
	this.systemID = systemID;
    }

    /* (non-Javadoc)
	 * @see net.sf.vex.dom.IVexDocumnt#setUndoEnabled(boolean)
	 */
    public void setUndoEnabled(boolean undoEnabled) {
        this.undoEnabled = undoEnabled;
    }
    
    /* (non-Javadoc)
	 * @see net.sf.vex.dom.IVexDocumnt#setValidator(net.sf.vex.dom.Validator)
	 */
    public void setValidator(Validator validator) {
	this.validator = validator;
    }

    //==================================================== PRIVATE
    
    /**
     * Represents a deletion from a document that can be undone
     * and redone.
     */
    public class DeleteEdit implements IUndoableEdit {

        private int startOffset;
        private int endOffset;
        private IVexDocumentFragment frag;
    
        public DeleteEdit(int startOffset, int endOffset, IVexDocumentFragment frag) {
            this.startOffset = startOffset;
            this.endOffset = endOffset;
            this.frag = frag; 
        }
        
        public boolean combine(IUndoableEdit edit) {
            return false;
        }

        public void undo() throws CannotUndoException {
            try {
                setUndoEnabled(false);
                insertFragment(this.startOffset, this.frag);
            } catch (DocumentValidationException ex) {
                throw new CannotUndoException();
            } finally {
                setUndoEnabled(true);
            }
        }

        public void redo() throws CannotRedoException {
            try {
                setUndoEnabled(false);
                delete(this.startOffset, this.endOffset);
            } catch (DocumentValidationException ex) {
                throw new CannotUndoException();
            } finally {
                setUndoEnabled(true);
            }
        }

    }


    /**
     * Represents an insertion of an element into the document.
     */
    public class InsertElementEdit implements IUndoableEdit {

        private int offset;
        private IVexElement element;
    
        public InsertElementEdit(int offset, IVexElement element) {
            this.offset = offset;
            this.element = element;
        }

        public boolean combine(IUndoableEdit edit) {
            return false;
        }

        public void undo() throws CannotUndoException {
            try {
                setUndoEnabled(false);
                delete(this.offset, this.offset + 2);
            } catch (DocumentValidationException ex) {
                throw new CannotUndoException();
            } finally {
                setUndoEnabled(true);
            }
        }

        public void redo() throws CannotRedoException {
            try {
                setUndoEnabled(false);
                insertElement(this.offset, this.element);
            } catch (DocumentValidationException ex) {
                throw new CannotUndoException();
            } finally {
                setUndoEnabled(true);
            }
        }

    }
    
     
    /**
     * Represents an insertion of a fragment into the document.
     */
    public class InsertFragmentEdit implements IUndoableEdit {

        private int offset;
        private IVexDocumentFragment frag;
    
        public InsertFragmentEdit(int offset, IVexDocumentFragment frag) {
            this.offset = offset;
            this.frag = frag;
        }

        public boolean combine(IUndoableEdit edit) {
            return false;
        }

        public void undo() throws CannotUndoException {
            try {
                setUndoEnabled(false);
                int length = this.frag.getContent().getLength();
                delete(this.offset, this.offset + length);
            } catch (DocumentValidationException ex) {
                throw new CannotUndoException();
            } finally {
                setUndoEnabled(true);
            }
        }

        public void redo() throws CannotRedoException {
            try {
                setUndoEnabled(false);
                insertFragment(this.offset, this.frag);
            } catch (DocumentValidationException ex) {
                throw new CannotUndoException();
            } finally {
                setUndoEnabled(true);
            }
        }

    }
    
   
    /**
     * Represents an insertion of text into the document.
     */
    public class InsertTextEdit implements IUndoableEdit {

        private int offset;
        private String text;
    
        public InsertTextEdit(int offset, String text) {
            this.offset = offset;
            this.text = text;
        }

        public boolean combine(IUndoableEdit edit) {
            if (edit instanceof InsertTextEdit) {
                InsertTextEdit ite = (InsertTextEdit) edit;
                if (ite.offset == this.offset + this.text.length()) {
                    this.text = this.text + ite.text;
                    return true;
                }
            }
            return false;
        }

        public void undo() throws CannotUndoException {
            try {
                setUndoEnabled(false);
                delete(this.offset, this.offset + this.text.length());
            } catch (DocumentValidationException ex) {
                throw new CannotUndoException();
            } finally {
                setUndoEnabled(true);
            }
        }

        public void redo() throws CannotRedoException {
            try {
                setUndoEnabled(false);
                insertText(this.offset, this.text);
            } catch (DocumentValidationException ex) {
                throw new CannotUndoException();
            } finally {
                setUndoEnabled(true);
            }
        }

    }
    
    /**
     * Assert that the given offset is within the given range,
     * throwing IllegalArgumentException if not.
     */
    private static void assertOffset(int offset, int min, int max) {
        if (offset < min || offset > max) {
            throw new IllegalArgumentException("Bad offset " + offset +
                               "must be between " + min +
                               " and " + max);
        }   
    }



    /**
     * Clone an element tree, pointing to a new Content object.
     * 
     * @param original Element to be cloned
     * @param content new Content object to which the clone will point
     * @param shift amount to shift offsets to be valid in the new Content.
     * @param parent parent for the cloned Element
     */
    protected IVexElement cloneElement(
        IVexElement original,
        Content content,
        int shift,
        IVexElement parent) {
            
        IVexElement clone = new Element(original.getName());
        clone.setContent(
            content,
            original.getStartOffset() + shift,
            original.getEndOffset() + shift);
        String[] attrNames = original.getAttributeNames();
        for (int i = 0; i < attrNames.length; i++) {
            try {
                clone.setAttribute(attrNames[i], original.getAttribute(attrNames[i]));
            } catch (DocumentValidationException ex) {
                throw new RuntimeException("Unexpected exception: " + ex);
            }
        }
        clone.setParent(parent);

        IVexElement[] children = original.getChildElements();
        for (int i = 0; i < children.length; i++) {
            IVexElement cloneChild =
                this.cloneElement(children[i], content, shift, clone);
            clone.internalInsertChild(i, cloneChild);
        }

        return clone;
    }
    
    void fireAttributeChanged(DocumentEvent e) {
        this.listeners.fireEvent("attributeChanged", e);
    }
    
    protected void fireBeforeContentDeleted(DocumentEvent e) {
        this.listeners.fireEvent("beforeContentDeleted", e);
    }

    protected void fireBeforeContentInserted(DocumentEvent e) {
        this.listeners.fireEvent("beforeContentInserted", e);
    }

    protected void fireContentDeleted(DocumentEvent e) {
        this.listeners.fireEvent("contentDeleted", e);
    }

    protected void fireContentInserted(DocumentEvent e) {
        this.listeners.fireEvent("contentInserted", e);
    }

	public void setRootElement(IVexRootElement rootElement) {
		this.rootElement = rootElement;
	}


	public IWhitespacePolicy getWhitespacePolicy() {
		return whitespacePolicy;
	}

	public void setWhitespacePolicy(IWhitespacePolicy policy) {
		this.whitespacePolicy = policy;
	}
		
	public void setCSSfileLocation(String filename) {
			this.cssfile_location = filename;
	}
	public String getCSSfileLocation() {
		return this.cssfile_location;
    }
	 


}