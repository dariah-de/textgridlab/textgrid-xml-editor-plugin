/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2004 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.dom.impl;

import net.sf.vex.dom.Content;
import net.sf.vex.dom.IVexText;

/**
 * <code>Text</code> represents a run of text in a document. Text
 * objects are not used in the internal document structure; they are
 * only returned as needed by the <code>Element.getContent</code>
 * method.
 */
public class Text extends Node implements IVexText {

    /**
     * Class constructor.
     * 
     * @param content Content object containing the text
     * @param startOffset character offset of the start of the run
     * @param endOffset character offset of the end of the run
     */
    public Text(Content content, int startOffset, int endOffset) {
	this.setContent(content, startOffset, endOffset);
    }

}
