/*
 * Created on 18.10.2007
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package net.sf.vex.dom.impl;

import javax.swing.text.BadLocationException;
import javax.swing.text.GapContent;

import net.sf.vex.dom.Content;
import net.sf.vex.dom.Position;

public class SwingGapContentWrapper implements Content {

	private class SwingPositionWrapper implements net.sf.vex.dom.Position {

		private javax.swing.text.Position swingPosition;

		public SwingPositionWrapper(javax.swing.text.Position pos) {
			swingPosition = pos;
		}

		public int getOffset() {
			return swingPosition.getOffset();
		}
		
		@Override
		public String toString() {
			return swingPosition.toString();
		}

	}

	private GapContent swingGapContent;

	public SwingGapContentWrapper(int initialCapacity) {
		swingGapContent = new GapContent(initialCapacity);
	}

	public Position createPosition(int offset) {

		try {
			return new SwingPositionWrapper(swingGapContent
					.createPosition(offset));
		} catch (BadLocationException e) {
			throw new IllegalArgumentException("Bad offset "
					+ e.offsetRequested() + "must be between " + 0 + " and "
					+ swingGapContent.length());
		}

	}

	public int getLength() {
		return swingGapContent.length();
	}

	public String getString(int offset, int length) {
		try {
			return swingGapContent.getString(offset, length);

		} catch (BadLocationException e) {
			throw new IllegalArgumentException("Bad offset "
					+ e.offsetRequested() + "must be between " + 0 + " and "
					+ swingGapContent.length());
		}

	}

	public void insertString(int offset, String s) {
		try {
			swingGapContent.insertString(offset, s);
		} catch (BadLocationException e) {
			throw new IllegalArgumentException("Bad offset "
					+ e.offsetRequested() + "must be between " + 0 + " and "
					+ swingGapContent.length());

		}
	}

	public void remove(int offset, int length) {
		try {
			swingGapContent.remove(offset, length);
		} catch (BadLocationException e) {
			throw new IllegalArgumentException("Bad offset "
					+ e.offsetRequested() + "must be between " + 0 + " and "
					+ swingGapContent.length());

		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		int length = getLength();
		return "SwingGapContentWrapper("
				+ getString(0, length).replace("\0", "\\0")
						.replace("\n", "\\n") + ")";
	}

}
