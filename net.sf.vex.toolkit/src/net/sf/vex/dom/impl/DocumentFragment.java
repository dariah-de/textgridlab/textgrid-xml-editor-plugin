/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2004 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.dom.impl;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import net.sf.vex.dom.Content;
import net.sf.vex.dom.DocumentValidationException;
import net.sf.vex.dom.IVexDocumentFragment;
import net.sf.vex.dom.IVexElement;
import net.sf.vex.dom.IVexNode;
import net.sf.vex.dom.IVexText;
import net.sf.vex.dom.Validator;

/**
 * Represents a fragment of an XML document.
 */
public class DocumentFragment implements Serializable, IVexDocumentFragment {

    private Content content;
    private IVexElement[] elements;

    /**
     * Class constructor.
     *
     * @param content Content holding the fragment's content.
     * @param elements Elements that make up this fragment.
     */
    public DocumentFragment(Content content, IVexElement[] elements) {
        this.content = content;
	this.elements = elements;
    }

    /* (non-Javadoc)
	 * @see net.sf.vex.dom.IVexDocumentFragment#getContent()
	 */
    public Content getContent() {
        return this.content;
    }
    
    /* (non-Javadoc)
	 * @see net.sf.vex.dom.IVexDocumentFragment#getLength()
	 */
    public int getLength() {
        return this.content.getLength();
    }
    
    /* (non-Javadoc)
	 * @see net.sf.vex.dom.IVexDocumentFragment#getElements()
	 */
    public IVexElement[] getElements() {
        return this.elements;
    }
    
    /* (non-Javadoc)
	 * @see net.sf.vex.dom.IVexDocumentFragment#getNodeNames()
	 */
    public String[] getNodeNames() {
        
        IVexNode[] nodes = this.getNodes();
        String[] names = new String[nodes.length];
        for (int i = 0; i < nodes.length; i++) {
            if (nodes[i] instanceof IVexText) {
                names[i] = Validator.PCDATA;
            } else {
                names[i] = ((IVexElement) nodes[i]).getName();
            }
        }
        
        return names;
    }

    /* (non-Javadoc)
	 * @see net.sf.vex.dom.IVexDocumentFragment#getNodes()
	 */
    public IVexNode[] getNodes() {
        return Document.createNodeArray(
            this.getContent(),
            0,
            this.getContent().getLength(),
            this.getElements());
    }
 
 
    //======================================================= PRIVATE
    
    /*
     * Custom Serialization Methods
     */

    private void writeObject(ObjectOutputStream out) throws IOException {
        out.writeUTF(this.content.getString(0, this.content.getLength()));
        out.writeInt(this.elements.length);
        for (int i = 0; i < this.elements.length; i++) {
            this.writeElement(this.elements[i], out);
        }
    }
    
    private void writeElement(IVexElement element, ObjectOutputStream out) 
        throws IOException {
            
        out.writeObject(element.getName());
        out.writeInt(element.getStartOffset());
        out.writeInt(element.getEndOffset());
        String[] attrNames = element.getAttributeNames();
        out.writeInt(attrNames.length);
        for (int i = 0; i < attrNames.length; i++) {
            out.writeObject(attrNames[i]);
            out.writeObject(element.getAttribute(attrNames[i]));
        }
        IVexElement[] children = element.getChildElements();
        out.writeInt(children.length);
        for (int i = 0; i < children.length; i++) {
            this.writeElement(children[i], out);
        }
    }
    
    private void readObject(ObjectInputStream in)
        throws IOException, ClassNotFoundException {
            
        String s = in.readUTF();
        this.content = new SwingGapContentWrapper(s.length());
        content.insertString(0, s);
        int n = in.readInt();
        this.elements = new IVexElement[n];
        for (int i = 0; i < n; i++) {
            this.elements[i] = this.readElement(in);
        }
    }
    
    private IVexElement readElement(ObjectInputStream in) 
        throws IOException, ClassNotFoundException {
            
        String name = (String) in.readObject();
        int startOffset = in.readInt();
        int endOffset = in.readInt();
        IVexElement element = new Element(name);
        element.setContent(this.content, startOffset, endOffset);
        
        int attrCount = in.readInt();
        for (int i = 0; i < attrCount; i++) {
            String key = (String) in.readObject();
            String value = (String) in.readObject();
            try {
                element.setAttribute(key, value);
            } catch (DocumentValidationException e) {
                // Should never happen; there ain't no document
                e.printStackTrace();
            }
        }
        
        int childCount = in.readInt();
        for (int i = 0; i < childCount; i++) {
            IVexElement child = this.readElement(in);
            child.setParent(element);
            element.internalInsertChild(i, child);
        }
        
        return element;
    }
}
