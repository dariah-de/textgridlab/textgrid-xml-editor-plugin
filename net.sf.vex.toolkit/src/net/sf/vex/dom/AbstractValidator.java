/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2004 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.dom;

import java.util.ArrayList;
import java.util.List;

/**
 * Partial implementation of the Validator interface.
 */
public abstract class AbstractValidator implements Validator {

    
    /**
     * @see Validator#isValidSequence
     */
    public boolean isValidSequence(
        String element,
        String[] seq1,
        String[] seq2,
        String[] seq3,
        boolean partial) {
            
        List list = new ArrayList();
        for (int i = 0; i < seq1.length; i++) {
            list.add(seq1[i]);
        }
        if (seq2 != null) {
            for (int i = 0; i < seq2.length; i++) {
                if (i == 0 && seq2[i].equals(Validator.PCDATA) && list.size() > 0
                    && list.get(list.size() - 1).equals(Validator.PCDATA)) {
                    // Avoid consecutive PCDATA's
                    continue;
                }
                list.add(seq2[i]);
            }
        }
        if (seq3 != null) {
            for (int i = 0; i < seq3.length; i++) {
                if (i == 0 && seq3[i].equals(Validator.PCDATA) && list.size() > 0
                    && list.get(list.size() - 1).equals(Validator.PCDATA)) {
                    // Avoid consecutive PCDATA's
                    continue;
                }
                list.add(seq3[i]);
            }
        }

        String[] nodes = (String[]) list.toArray(new String[list.size()]);
        return this.isValidSequence(element, nodes, partial);    
    }


}

