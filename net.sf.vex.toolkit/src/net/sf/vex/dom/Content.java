/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2004 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.dom;

/**
 * Interface for classes that manage a string of characters representing
 * the content of a document.
 */
public interface Content {

    /**
     * Creates a new Position object at the given initial offset.
     *
     * @param offset initial offset of the position
     */
    public Position createPosition(int offset);

	/**
	 * Insert a string into the content.
	 * 
	 * Attention: If there is a position at offset, the string is inserted
	 * <em>before</em> the position. If you don't want this, you have to reset
	 * the position afterwards.
	 * 
	 * @param offset
	 *            Offset at which to insert the string.
	 * @param s
	 *            String to insert.
	 */
    public void insertString(int offset, String s);

    /**
     * Deletes the given range of characters.
     *
     * @param offset Offset from which characters should be deleted.
     * @param length Number of characters to delete.
     */
    public void remove(int offset, int length);

    /**
     * Gets a substring of the content.
     *
     * @param offset Offset at which the string begins.
     * @param length Number of characters to return.
     */
    public String getString(int offset, int length);

    /**
     * Return the length of the content.
     */
    public int getLength();
}
