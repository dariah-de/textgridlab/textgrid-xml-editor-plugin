/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2004 John Krasnay, (c) 2007 Thorsten Vitt
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.dom;

import java.io.CharArrayReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.net.URL;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.ContentHandler;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.ext.LexicalHandler;


/**
 * Class for creating documents given a URL.
 */
public class DocumentReader {

    
    /**
     * Returns the debugging flag.
     */
    public boolean isDebugging() {
        return debugging;
    }

    /**
     * Returns the entity resolver for this reader.
     */
    public EntityResolver getEntityResolver() {
        return entityResolver;
    }
    
    /**
     * Returns the whitespace policy factory for this reader.
     */
    public IWhitespacePolicyFactory getWhitespacePolicyFactory() {
        return whitespacePolicyFactory;
    }
    
    /**
     * Reads a document given an {@link InputStream}. 
     *  
     * @param stream	The stream from which to load the document. The user is responsible for {@linkplain InputStream#close() closing} the stream.
     * @return			A constructed document.
     * @throws IOException
     * @throws ParserConfigurationException
     * @throws SAXException
     */
    public IVexDocument read(InputStream stream) throws IOException, ParserConfigurationException, SAXException {
    	return read(new InputSource(stream));
	}

	/**
     * Reads a document given a URL.
     *
     * @param url URL from which to load the document.
     */
    public IVexDocument read(URL url)
        throws IOException, ParserConfigurationException, SAXException {

        return read(new InputSource(url.toString()));
    }

    /**
     * Reads a document from a string. This is mainly used for short documents
     * in unit tests.
     * @param s String containing the document to be read.
     */
    public IVexDocument read(String s) 
            throws IOException, ParserConfigurationException, SAXException {

        Reader reader = new CharArrayReader(s.toCharArray());
        return this.read(new InputSource(reader));
    }
    
    /**
     * Reads a document given a SAX InputSource.
     *
     * @param is SAX InputSource from which to load the document.
     */
    public IVexDocument read(InputSource is)
	throws IOException, ParserConfigurationException, SAXException {

	SAXParserFactory factory = SAXParserFactory.newInstance();
        factory.setValidating(false); // TODO: experimental--SWT implementation
	XMLReader xmlReader = factory.newSAXParser().getXMLReader();
	//xmlReader.setFeature("http://xml.org/sax/features/validation", false);
	final net.sf.vex.dom.DocumentBuilder builder = 
	    new net.sf.vex.dom.DocumentBuilder(this.getWhitespacePolicyFactory());

	ContentHandler contentHandler = builder;
	LexicalHandler lexicalHandler = builder;
    
	if (this.isDebugging()) {
	    Object proxy = Proxy.newProxyInstance(
                this.getClass().getClassLoader(),
                new Class[] { ContentHandler.class, LexicalHandler.class },
                new InvocationHandler() {
                    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                        try {
                            return method.invoke(builder, args);
                        } catch (InvocationTargetException ex) {
                            ex.getCause().printStackTrace();
                            throw ex.getCause();
                        }
                    }
	        });
        
	    contentHandler = (ContentHandler) proxy;
	    lexicalHandler = (LexicalHandler) proxy;
	}
    
	xmlReader.setContentHandler(contentHandler);
	xmlReader.setProperty("http://xml.org/sax/properties/lexical-handler",
			      lexicalHandler);
	if (this.getEntityResolver() != null) {
	    xmlReader.setEntityResolver(this.getEntityResolver());
	}
	xmlReader.parse(is);
	IVexDocument document = builder.getDocument();
	document.setWhitespacePolicy(getWhitespacePolicyFactory().getPolicy(
				document.getPublicID()));
	return document;
    }
    
    /**
     * Sets the debugging flag.
     * @param debugging true if the component should log debugging info to stdout.
     */
    public void setDebugging(boolean debugging) {
        this.debugging = debugging;
    }

    /**
     * Sets the entity resolver for this reader.
     * @param entityResolver The entityResolver to set.
     */
    public void setEntityResolver(EntityResolver entityResolver) {
        this.entityResolver = entityResolver;
    }
    
    /**
     * Sets the whitespace policy factory for this reader. This factory is used
     * to obtain a whitespace policy once the public ID of the document is
     * known.
     * @param whitespacePolicyFactory The whitespacePolicyFactory to set.
     */
    public void setWhitespacePolicyFactory(
            IWhitespacePolicyFactory whitespacePolicyFactory) {
        this.whitespacePolicyFactory = whitespacePolicyFactory;
    }

    //======================================================= PRIVATE
    
    private boolean debugging;
    private EntityResolver entityResolver;
    private IWhitespacePolicyFactory whitespacePolicyFactory;

}
