/*
 * VEX, a visual editor for XML
 * 
 * Copyright (c) 2004 John Krasnay
 * 
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *  
 */
package net.sf.vex.samples;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.IImportWizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.actions.WorkspaceModifyOperation;
import org.osgi.framework.Bundle;

/**
 * Wizard for importing the Vex Samples into the workspace.
 */
public class SamplesImportWizard extends Wizard implements IImportWizard {

    public void addPages() {
        this.destPage = new SamplesDestinationPage(); 
        this.addPage(this.destPage);
        super.addPages();
    }

    public void init(IWorkbench workbench, IStructuredSelection selection) {
    }

    public boolean performFinish() {
            try {
                this.getContainer().run(true, true, new ImportSamplesOperation());
            } catch (InvocationTargetException e) {
                e.printStackTrace();
                return false;
            } catch (InterruptedException e) {
                e.printStackTrace();
                return false;
            }
        
        return true;
    }

    
    //========================================================= PRIVATE
    
    private SamplesDestinationPage destPage;
    
    
    private class ImportSamplesOperation extends WorkspaceModifyOperation {

        protected void execute(IProgressMonitor monitor) throws CoreException, InvocationTargetException, InterruptedException {
            monitor.beginTask("Importing samples", 100);
            ZipInputStream zip = null;
            try {
                IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
                IProject project = root.getProject(destPage.getProjectName());
                if (!project.exists()) {
                    project.create(monitor);
                    project.open(monitor);
                }
                monitor.worked(10);
                
                Bundle bundle = Platform.getBundle("net.sf.vex.samples");
                URL zipUrl = bundle.getEntry("samples.zip");
                zip = new ZipInputStream(zipUrl.openStream());
                for (;;) {
                    ZipEntry entry = zip.getNextEntry();
                    if (entry == null) {
                        break;
                    }

                    IPath path = new Path(entry.getName());
                    if (entry.isDirectory()) {
                        IFolder folder = project.getFolder(path);
                        if (!folder.exists()) {
                            folder.create(true, true, monitor);
                        }
                    } else {
                        IFile target = project.getFile(path);
                        InputStream noclose = new NoCloseInputStream(zip);
                        if (target.exists()) {
                            target.setContents(noclose, true, true, monitor);
                        } else {
                            target.create(noclose, true, monitor);
                        }
                    }
                }
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } finally {
                if (zip != null) {
                    try { zip.close(); } catch (IOException ex) { }
                }
                monitor.done();
            }
            
        }
        
    }
    
    /**
     * Wrapper InputStream that suppresses the close(0 call to the underlying
     * stream. This handles the annoying stream close in IFile.create and 
     * IFile.setContents().
     */
    private class NoCloseInputStream extends InputStream {
        private InputStream is;
        public NoCloseInputStream(InputStream is) {
            this.is = is;
        }
        public int available() throws IOException {
            return is.available();
        }
        public void close() throws IOException {
        }
        public boolean equals(Object obj) {
            return is.equals(obj);
        }
        public int hashCode() {
            return is.hashCode();
        }
        public void mark(int readlimit) {
            is.mark(readlimit);
        }
        public boolean markSupported() {
            return is.markSupported();
        }
        public int read() throws IOException {
            return is.read();
        }
        public int read(byte[] b) throws IOException {
            return is.read(b);
        }
        public int read(byte[] b, int off, int len) throws IOException {
            return is.read(b, off, len);
        }
        public void reset() throws IOException {
            is.reset();
        }
        public long skip(long n) throws IOException {
            return is.skip(n);
        }
        public String toString() {
            return is.toString();
        }
    }
}

