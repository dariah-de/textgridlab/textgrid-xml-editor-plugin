/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2004 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.samples;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;


/**
 * Wizard page that lets the user select the destination folder for the samples.
 */
public class SamplesDestinationPage extends WizardPage {

    private static final String PROJECT_NAME = "vex-samples";
    
    private Text destField;
    private Font boldFont;
    
    /**
     * Class constructor.
     */
    public SamplesDestinationPage() {
        super("Select Destination");
    }
    
    public void createControl(Composite parent) {
        
	Composite group = new Composite(parent,SWT.NONE);
	GridLayout layout = new GridLayout();
	layout.numColumns = 1;
	group.setLayout(layout);

	Label l1 = new Label(group, SWT.WRAP);
	l1.setText("Click Finish to create the project " + PROJECT_NAME
	        + " in the workspace.");
	
        IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
        IProject project = root.getProject(PROJECT_NAME);
	if (project.exists()) {
	    Label l2 = new Label(group, SWT.WRAP);
	    Font font = l2.getFont();
	    FontData fd = font.getFontData()[0];
	    fd.setStyle(SWT.BOLD);
	    this.boldFont = new Font(l2.getDisplay(), fd); 
	    l2.setFont(boldFont);
	    l2.setText("WARNING: the project " + PROJECT_NAME +
	            " already exists.  Existing files in the project " +
	            "may be overwritten.");
	}
	
        /*
	// container specification group
	Composite group = new Composite(parent,SWT.NONE);
	GridLayout layout = new GridLayout();
	layout.numColumns = 3;
	group.setLayout(layout);

	// container label
	Label resourcesLabel = new Label(group, SWT.NONE);
	resourcesLabel.setText("Into folder:");

	this.destField = new Text(group, SWT.SINGLE|SWT.BORDER);
	//containerNameField.addListener(SWT.Modify,this);
	GridData data = new GridData(GridData.FILL_HORIZONTAL);
	//data.widthHint = SIZING_TEXT_FIELD_WIDTH;
	this.destField.setLayoutData(data);
	this.destField.setText("vex-samples");

	// container browse button
	Button browseButton = new Button(group, SWT.PUSH);
	browseButton.setText("Browse...");
	browseButton.addSelectionListener(this.buttonListener);
	//setButtonLayoutData(browseButton);
	*/

	this.setControl(group);
	
        this.setTitle("Vex samples");
        this.setDescription("Import the Vex sample document types and styles.");

    }
    
    public void dispose() {
        super.dispose();
        if (this.boldFont != null) {
            this.boldFont.dispose();
            this.boldFont = null;
        }
    }

    /**
     * Returns the contents of the destination text entry widget.
     */
    public String getDestination() {
        return this.destField.getText();
    }
    
    /**
     * Returns the name of the project containing the samples. This is here
     * because a reasonable enhancement is to let the user specify this. 
     * @return
     */
    public String getProjectName() {
        return PROJECT_NAME;
    }

}
