/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2004 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.editor;

import java.text.MessageFormat;

import net.sf.vex.action.ContentAssistant;
import net.sf.vex.swt.VexWidget;

import org.eclipse.jface.action.IAction;


/**
 * Content assistant that shows valid elements to be inserted at the current
 * point.
 */
public class MorphAssistant extends ContentAssistant {
    
    public IAction[] getActions(VexWidget vexWidget) {
        return vexWidget.getValidMorphActions();
    }

    public String getTitle(VexWidget vexWidget) {
        String message = Messages.getString("ChangeElementAction.dynamic.label"); //$NON-NLS-1$
        String name = vexWidget.getCurrentElement().getName();
        return MessageFormat.format(message, new Object[] { name });
    }

}
