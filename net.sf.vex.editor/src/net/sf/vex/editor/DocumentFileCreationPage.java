/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2004 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.editor;

import java.io.InputStream;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.dialogs.WizardNewFileCreationPage;

/**
 * Wizard page for selecting the file name for a new Vex document.
 */
public class DocumentFileCreationPage extends WizardNewFileCreationPage {

    /**
     * Class constructor. Supplies a title and description to the superclass.
     * @param pageName name of the page
     * @param selection selection active when the wizard was started
     */
    public DocumentFileCreationPage(String pageName, IStructuredSelection selection) {
        super(pageName, selection);
        this.setTitle(Messages.getString("DocumentFileCreationPage.title")); //$NON-NLS-1$
        this.setDescription(Messages.getString("DocumentFileCreationPage.desc")); //$NON-NLS-1$
    }

    /**
     * Returns the initial contents of the file. The initial contents
     * are set by the wizard via the {@link setInitialContents} method.
     */
    protected InputStream getInitialContents() {
        return this.initialContents;
    }

    /**
     * Sets the initial contents to be used when the document is created.
     * @param initialContents initial contents for the new document.
     */
    public void setInitialContents(InputStream initialContents) {
        this.initialContents = initialContents;
    }
    
    private InputStream initialContents;
}
