/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2004 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.editor.config;

import org.eclipse.core.resources.ICommand;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IProjectNature;
import org.eclipse.core.runtime.CoreException;

/**
 * Project nature that defines Vex Plugin projects.
 */
public class PluginProjectNature implements IProjectNature {
    
    public static final String ID = "net.sf.vex.editor.pluginNature"; //$NON-NLS-1$

    public void configure() throws CoreException {
        this.registerBuilder();
    }

    public void deconfigure() throws CoreException {
        //System.out.println("deconfiguring " + project.getName());
        project.deleteMarkers(IMarker.PROBLEM, true, 1);
    }

    public IProject getProject() {
        return this.project;
    }

    public void setProject(IProject project) {
        this.project = project;
    }

    //====================================================== PRIVATE

    private IProject project;
    
    
    private void registerBuilder() throws CoreException {
        IProjectDescription desc = project.getDescription();
        ICommand[] commands = desc.getBuildSpec();
        boolean found = false;

        for (int i = 0; i < commands.length; ++i) {
           if (commands[i].getBuilderName().equals(PluginProjectBuilder.ID)) {
              found = true;
              break;
           }
        }
        if (!found) { 
           //add builder to project
           ICommand command = desc.newCommand();
           command.setBuilderName(PluginProjectBuilder.ID);
           ICommand[] newCommands = new ICommand[commands.length + 1];

           // Add it before other builders.
           System.arraycopy(commands, 0, newCommands, 1, commands.length);
           newCommands[0] = command;
           desc.setBuildSpec(newCommands);
           project.setDescription(desc, null);
        }


    }
}
