/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2004 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.editor.config;

import java.net.URL;

import net.sf.vex.editor.VexPlugin;

import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.IDecoration;
import org.eclipse.jface.viewers.ILightweightLabelDecorator;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.LabelProviderChangedEvent;

/**
 * Decorates Vex resources that build problems.
 */
public class BuildProblemDecorator extends LabelProvider implements ILightweightLabelDecorator {

    public static final String ID = "net.sf.vex.editor.config.buildProblemDecorator"; //$NON-NLS-1$
    
    public void decorate(Object element, IDecoration decoration) {
        
        if (this.errorIcon == null) {
            this.loadImageDescriptors();
        }
        
        if (element instanceof IResource) {
            try {
                IResource resource = (IResource) element;
                IMarker[] markers = resource.findMarkers(IMarker.PROBLEM, true, 0);
                if (markers.length > 0) {
                    decoration.addOverlay(this.errorIcon, IDecoration.BOTTOM_LEFT);
                }
            } catch (CoreException e) {
            }
        }
    }

    /**
     * Fire a change notification that the markers on the given resource has changed.
     * @param resources Array of resources whose markers have changed.
     */
    public void update(IResource resource) {
        this.fireLabelProviderChanged(new LabelProviderChangedEvent(this, resource));
    }
    
    /**
     * Fire a change notification that the markers on the given resources has changed.
     * @param resources Array of resources whose markers have changed.
     */
    public void update(IResource[] resources) {
        this.fireLabelProviderChanged(new LabelProviderChangedEvent(this, resources));
    }
    
    //======================================================== PRIVATE
    
    private ImageDescriptor errorIcon;
    
    private void loadImageDescriptors() {
        URL url = VexPlugin.getInstance().find(new Path("icons/error_co.gif")); //$NON-NLS-1$
        this.errorIcon = ImageDescriptor.createFromURL(url);
    }

}
