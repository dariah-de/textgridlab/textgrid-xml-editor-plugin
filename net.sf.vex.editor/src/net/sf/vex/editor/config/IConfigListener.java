/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2004 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.editor.config;

import java.util.EventListener;

/**
 * Interface through which Vex notifies UI components that configuration
 * items such as doctypes and styles have been added, removed, or changed. 
 * Implementations of this 
 * interface should be registered with the VexPlugin instance. All calls to
 * implementations occur on the UI thread.
 */
public interface IConfigListener extends EventListener {
	
    /**
     * Called when one or more configuration items are added, removed, or
     * changed. 
     * @param e ConfigEvent containing details of the change.
     */
    public void configChanged(ConfigEvent e);
    
    /**
     * Called when the Vex configuration is first loaded by the ConfigLoaderJob.
     * This method is guaranteed to be called before the first call to
     * configChanged.
     * @param e ConfigEvent containing details of the change.
     */
    public void configLoaded(ConfigEvent e);
}
