/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2004 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.editor.config;

import org.eclipse.core.runtime.IConfigurationElement;

/**
 * Wrapper for IConfigurationElement that implements IConfigElement.
 */
public class ConfigurationElementWrapper implements IConfigElement {

    /**
     * Class constructor.
     * @param element Element to be wrapped.
     */
    public ConfigurationElementWrapper(IConfigurationElement element) {
        this.element = element;
    }
    
    public String getAttribute(String name) {
        return this.element.getAttribute(name);
    }

    public String[] getAttributeNames() {
        return this.element.getAttributeNames();
    }

    public IConfigElement[] getChildren() {
        return convertArray(this.element.getChildren());
    }

    public IConfigElement[] getChildren(String name) {
        return convertArray(this.element.getChildren(name));
    }

    public String getName() {
        return this.element.getName();
    }

    public String getValue() {
        return this.element.getValue();
    }

    /**
     * Wraps each element in an array of IConfigurationElement objects with
     * a ConfigurationElementWrapper and returns the result.
     * @param elements Array of elements to be wrapped.
     */
    public static IConfigElement[] convertArray(IConfigurationElement[] elements) {
        IConfigElement[] ret = new IConfigElement[elements.length];
        for (int i = 0; i < elements.length; i++) {
            ret[i] = new ConfigurationElementWrapper(elements[i]);
        }
        return ret;
    }

    //=================================================== PRIVATE
    
    private IConfigurationElement element;
    
}
