/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2004 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.editor.config;

import java.net.URL;

import net.sf.vex.editor.VexPlugin;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.IDecoration;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.ILightweightLabelDecorator;

/**
 * Decorates Vex projects with the Vex logo.
 */
public class PluginProjectDecorator implements ILightweightLabelDecorator {

    public void decorate(Object element, IDecoration decoration) {
        
        if (this.vexIcon == null) {
            this.loadImageDescriptors();
        }
        
        if (element instanceof IProject) {
            try {
                IProject project = (IProject) element;
                if (project.hasNature(PluginProjectNature.ID)) {
                    decoration.addOverlay(this.vexIcon, IDecoration.TOP_RIGHT);
                }
            } catch (CoreException e) {
            }
        }
    }

    public void addListener(ILabelProviderListener listener) {
    }

    public void dispose() {
    }

    public boolean isLabelProperty(Object element, String property) {
        return false;
    }

    public void removeListener(ILabelProviderListener listener) {
    }
    
    //======================================================== PRIVATE
    
    private ImageDescriptor vexIcon;
    
    private void loadImageDescriptors() {
        URL url = VexPlugin.getInstance().find(new Path("icons/vex8.gif")); //$NON-NLS-1$
        this.vexIcon = ImageDescriptor.createFromURL(url);
    }

}
