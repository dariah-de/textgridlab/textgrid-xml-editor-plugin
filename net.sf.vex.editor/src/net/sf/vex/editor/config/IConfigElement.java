/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2004 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.editor.config;


/**
 * Represents the XML element for a Vex config item in plugin.xml.
 * Vex-specific replacement for the Eclipse IConfigurationElement class. 
 * We need this because we are not supposed to implement IConfigurationElement,
 * and in fact it changed and broke us from Eclipse 3.0 -> 3.1.
 */
public interface IConfigElement {

    /**
     * Returns the value of the given attribute.
     * @param name Name of the attribute for which to return a name.
     * @return
     */
    public String getAttribute(String name);

    /**
     * Returns an array of all the attributes defined by this element.
     */
    public String[] getAttributeNames();

    /**
     * Returns an array of the children of this element.
     */
    public IConfigElement[] getChildren();

    /**
     * Returns an array of the children of this element with the given name.
     * @param name Name of children to search for.
     */
    public IConfigElement[] getChildren(String name);

    /**
     * Returns the name of this element.
     */
    public String getName();

    /**
     * Returns the value of this element.
     */
    public String getValue();
}