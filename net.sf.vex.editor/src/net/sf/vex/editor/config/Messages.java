/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2005 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.editor.config;

import java.text.MessageFormat;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import net.sf.vex.editor.VexPlugin;

import org.eclipse.core.runtime.IStatus;

/**
 * Handler for language-specific strings in Vex.
 */
public class Messages {

    private static ResourceBundle resources;

    private Messages() {
    }

    /**
     * Returns the language-specific string for the given key,
     * or the key itself if not found.
     */
    public static String getString(String key) {
        if (resources == null) {
            resources = ResourceBundle.getBundle("net.sf.vex.editor.config.messages"); //$NON-NLS-1$
        }
        
        try {
            return resources.getString(key);
        } catch (MissingResourceException ex) {
            String message = Messages.getString("Messages.cantFindResource"); //$NON-NLS-1$
            VexPlugin.getInstance().log(IStatus.WARNING,
                    MessageFormat.format(message, new Object[] { key }));
            return key;
        }
    }

}
