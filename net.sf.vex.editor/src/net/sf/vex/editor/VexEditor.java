/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2004 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.editor;

//import org.eclipse.jface.fieldassist.ContentProposalAdapter.ContentProposalPopup;

import info.textgrid.lab.core.swtutils.AdapterUtils;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import net.sf.vex.action.ConfigureFavorite;
import net.sf.vex.action.DeleteColumnAction;
import net.sf.vex.action.DeleteRowAction;
import net.sf.vex.action.DuplicateSelectionAction;
import net.sf.vex.action.FontsList;
import net.sf.vex.action.GEditorContentAssistant;
import net.sf.vex.action.InsertColumnAfterAction;
import net.sf.vex.action.InsertColumnBeforeAction;
import net.sf.vex.action.InsertRowAboveAction;
import net.sf.vex.action.InsertRowBelowAction;
import net.sf.vex.action.MoveColumnLeftAction;
import net.sf.vex.action.MoveColumnRightAction;
import net.sf.vex.action.MoveRowDownAction;
import net.sf.vex.action.MoveRowUpAction;
import net.sf.vex.action.NextTableCellAction;
import net.sf.vex.action.PasteTextAction;
import net.sf.vex.action.PreviousTableCellAction;
import net.sf.vex.action.RemoveElementAction;
import net.sf.vex.action.RestoreLastSelectionAction;
import net.sf.vex.action.SplitAction;
import net.sf.vex.action.SplitItemAction;
import net.sf.vex.core.ListenerList;
import net.sf.vex.css.StyleSheet;
import net.sf.vex.css.StyleSheetReader;
import net.sf.vex.dom.DocumentReader;
import net.sf.vex.dom.DocumentWriter;
import net.sf.vex.dom.IVexDocument;
import net.sf.vex.dom.IVexElement;
import net.sf.vex.dom.IWhitespacePolicy;
import net.sf.vex.dom.IWhitespacePolicyFactory;
import net.sf.vex.dom.Validator;
import net.sf.vex.editor.VexEditor.NoRegisteredDoctypeException;
import net.sf.vex.editor.VexEditor.NoStyleForDoctypeException;
import net.sf.vex.editor.VexEditor.State;
import net.sf.vex.editor.action.ChangeElementAction;
import net.sf.vex.editor.action.InsertElementAction;
import net.sf.vex.editor.action.VexActionAdapter;
import net.sf.vex.editor.config.ConfigEvent;
import net.sf.vex.editor.config.ConfigRegistry;
import net.sf.vex.editor.config.DocumentType;
import net.sf.vex.editor.config.IConfigListener;
import net.sf.vex.editor.config.Style;
import net.sf.vex.swt.TextSelectionVex;
import net.sf.vex.swt.VexWidget;
import net.sf.vex.widget.CssWhitespacePolicy;

import org.eclipse.core.filesystem.EFS;
import org.eclipse.core.filesystem.URIUtil;
import org.eclipse.core.internal.runtime.Activator;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IResourceChangeListener;
import org.eclipse.core.resources.IResourceDelta;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.preferences.IPreferencesService;
import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.ActionContributionItem;
import org.eclipse.jface.action.ControlContribution;
import org.eclipse.jface.action.IContributionItem;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.text.Region;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.IKeyBindingService;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.dialogs.SaveAsDialog;
import org.eclipse.ui.editors.text.ILocationProvider;
import org.eclipse.ui.help.IWorkbenchHelpSystem;
import org.eclipse.ui.part.EditorPart;
import org.eclipse.ui.part.FileEditorInput;
import org.eclipse.ui.statushandlers.StatusManager;
import org.eclipse.ui.texteditor.ITextEditor;
import org.eclipse.ui.views.contentoutline.IContentOutlinePage;
import org.eclipse.ui.views.properties.IPropertySheetPage;
import org.eclipse.ui.views.properties.IPropertySource;
import org.eclipse.ui.views.properties.IPropertySourceProvider;
import org.eclipse.ui.views.properties.PropertySheetPage;
import org.osgi.service.prefs.BackingStoreException;
import org.osgi.service.prefs.Preferences;
import org.w3c.css.sac.CSSException;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

/**
 * Editor for editing XML file using the VexWidget.
 */
public class VexEditor extends EditorPart {

	private static final String CONTEXT_HELP_ID = "info.textgrid.lab.xmleditor.mpeditor.WysiwymPage";

	/**
	 * ID of this editor extension.
	 */
	public static final String ID = "net.sf.vex.editor.VexEditor"; //$NON-NLS-1$

	/**
	 * @return the selectionChangedListener
	 */
	public ISelectionChangedListener getSelectionChangedListener() {
		return selectionChangedListener;
	}

	/**
	 * @return the selectionProvider
	 */
	public SelectionProvider getSelectionProvider() {
		return selectionProvider;
	}

	/**
	 * Class constructor.
	 */
	public VexEditor() {
		setDebugging(VexPlugin.getInstance().isDebugging()
				&& "true".equalsIgnoreCase(Platform.getDebugOption(VexPlugin.ID + "/debug/layout"))); //$NON-NLS-1$ //$NON-NLS-2$
	}

	/**
	 * Add a VexEditorListener to the notification list.
	 * 
	 * @param listener
	 *            VexEditorListener to be added.
	 */
	public void addVexEditorListener(final IVexEditorListener listener) {
		vexEditorListeners.add(listener);
	}

	@Override
	public void dispose() {
		super.dispose();

		VexPlugin.getInstance().unregisterEditor(this);

		if (parentControl != null) {
			// createPartControl was called, so we must de-register from config
			// events
			ConfigRegistry.getInstance().removeConfigListener(
					configListener);
		}

		if (getEditorInput() instanceof IFileEditorInput) {
			ResourcesPlugin.getWorkspace().removeResourceChangeListener(
					resourceChangeListener);
		}

		// PreferenceChangeHandler.getInstance().removeVexEditor(this);

	}

	@Override
	public void doSave(final IProgressMonitor monitor) {

		final IEditorInput input = getEditorInput();
		OutputStream os = null;
		try {
			resourceChangeListener.setSaving(true);
			final DocumentWriter writer = new DocumentWriter();
			writer.setWhitespacePolicy(new CssWhitespacePolicy(style
					.getStyleSheet()));

			if (input instanceof IFileEditorInput) { // TODO: Add adaptor stuff
				final ByteArrayOutputStream baos = new ByteArrayOutputStream();
				writer.write(getDoc(), baos);
				baos.close();
				final ByteArrayInputStream bais = new ByteArrayInputStream(baos
						.toByteArray());
				((IFileEditorInput) input).getFile().setContents(bais, false,
						false, monitor);
			} else {
				os = EFS.getStore(
						URIUtil.toURI(((ILocationProvider) input)
								.getPath(input))).openOutputStream(EFS.NONE,
										monitor);
				writer.write(getDoc(), os);
			}

			setSavedUndoDepth(vexWidget.getUndoDepth());
			firePropertyChange(IEditorPart.PROP_DIRTY);

		} catch (final Exception ex) {
			monitor.setCanceled(true);
			final String title = Messages.getString("VexEditor.errorSaving.title"); //$NON-NLS-1$
			final String message = MessageFormat.format(Messages
					.getString("VexEditor.errorSaving.message"), //$NON-NLS-1$
					new Object[] { input.getName(), ex.getMessage() });
			MessageDialog.openError(getEditorSite().getShell(), title,
					message);
			VexPlugin.getInstance().log(IStatus.ERROR, message, ex);
		} finally {
			if (os != null) {
				try {
					os.close();
				} catch (final IOException e) {
				}
			}
			resourceChangeListener.setSaving(false);
		}
	}

	@Override
	public void doSaveAs() {
		final SaveAsDialog dlg = new SaveAsDialog(getSite().getShell());
		final int result = dlg.open();
		if (result == Window.OK) {
			final IPath path = dlg.getResult();
			try {
				resourceChangeListener.setSaving(true);
				final ByteArrayOutputStream baos = new ByteArrayOutputStream();
				final DocumentWriter writer = new DocumentWriter();
				writer.setWhitespacePolicy(new CssWhitespacePolicy(style
						.getStyleSheet()));
				writer.write(getDoc(), baos);
				baos.close();

				final ByteArrayInputStream bais = new ByteArrayInputStream(baos
						.toByteArray());
				final IFile file = ResourcesPlugin.getWorkspace().getRoot().getFile(
						path);
				file.create(bais, false, null);

				final IFileEditorInput input = new FileEditorInput(file);
				setInput(input);
				setSavedUndoDepth(vexWidget.getUndoDepth());

				firePropertyChange(IEditorPart.PROP_DIRTY);
				firePropertyChange(IEditorPart.PROP_INPUT);
				firePropertyChange(IWorkbenchPart.PROP_TITLE);

			} catch (final Exception ex) {
				final String title = Messages
				.getString("VexEditor.errorSaving.title"); //$NON-NLS-1$
				final String message = MessageFormat.format(Messages
						.getString("VexEditor.errorSaving.message"), //$NON-NLS-1$
						new Object[] { path, ex.getMessage() });
				MessageDialog.openError(getEditorSite().getShell(), title,
						message);
				VexPlugin.getInstance().log(IStatus.ERROR, message, ex);
			} finally {
				resourceChangeListener.setSaving(false);
			}
		}

	}

	/**
	 * Return a reasonable style for the given doctype.
	 * 
	 * @param publicId
	 *            Public ID for which to return the style.
	 */
	public static Style findStyleForDoctype(final String publicId) {

		final IPreferencesService preferences = Platform.getPreferencesService();
		final String key = getStylePreferenceKey(publicId);
		String preferredStyleId = preferences.getString(VexPlugin.ID, key,
				null, null);

		final Preferences prefs = new InstanceScope().getNode(VexPlugin.ID);
		preferredStyleId = prefs.get(key, null);

		Style firstStyle = null;
		final ConfigRegistry registry = ConfigRegistry.getInstance();
		final List styles = registry.getAllConfigItems(Style.EXTENSION_POINT);
		for (final Iterator it = styles.iterator(); it.hasNext();) {
			final Style style = (Style) it.next();
			if (style.appliesTo(publicId)) {
				if (firstStyle == null) {
					firstStyle = style;
				}
				if (style.getUniqueId().equals(preferredStyleId)) {
					return style;
				}
			}
		}
		return firstStyle;
	}

	/**
	 * Returns the DocumentType associated with this editor.
	 */
	public DocumentType getDocumentType() {
		return getDoctype();
	}

	/**
	 * Returns the Style currently associated with the editor. May be null.
	 */
	public Style getStyle() {
		return style;
	}

	/**
	 * Returns the VexWidget that implements this editor.
	 * 
	 */
	public VexWidget getVexWidget() {
		return vexWidget;
	}

	protected void setVexWidget(final VexWidget widget) {
		this.vexWidget = widget;
	}


	public void gotoMarker(final IMarker marker) {
		// TODO Auto-generated method stub

	}

	@Override
	public void init(final IEditorSite site, final IEditorInput input)
	throws PartInitException {

		setSite(site);
		setInput(input);

		getEditorSite().setSelectionProvider(selectionProvider);
		getEditorSite().getSelectionProvider()
		.addSelectionChangedListener(selectionChangedListener);

		if (input instanceof IFileEditorInput) {
			ResourcesPlugin.getWorkspace().addResourceChangeListener(
					resourceChangeListener,
					IResourceChangeEvent.POST_CHANGE);
		}


		// PreferenceChangeHandler.getInstance().addVexEditor(this);
		VexPlugin.getInstance().registerEditor(this);

	}

	protected enum State {
		UNLOADED, LOADING, LOADED
	};

	private State state = State.UNLOADED;

	protected State getState() {
		return state;
	}

	protected void loadInput() {

		if (vexWidget != null) {
			vexEditorListeners.fireEvent(
					"documentUnloaded", new VexEditorEvent(this)); //$NON-NLS-1$

		}

		loaded = false;

		final IEditorInput input = getEditorInput();

		try {
			final long start = System.currentTimeMillis();

			URI inputURI = null;

			if (input instanceof IFileEditorInput) {
				inputURI = ((IFileEditorInput) input).getFile()
				.getLocationURI();

			} else if (input instanceof ILocationProvider) {
				// Yuck, this a crappy way for Eclipse to do this
				// How about an exposed IJavaFileEditorInput, pleeze?
				final IPath workspacePath = ((ILocationProvider) input)
				.getPath(input);
				inputURI = URIUtil.toURI(workspacePath);
			} else {
				final String msg = MessageFormat.format(Messages
						.getString("VexEditor.unknownInputClass"), //$NON-NLS-1$
						new Object[] { input.getClass() });
				showLabel(msg);
				return;
			}

			final DocumentReader reader = new DocumentReader();
			reader.setDebugging(isDebugging());
			reader.setEntityResolver(entityResolver);
			reader.setWhitespacePolicyFactory(wsFactory);
			setDoctype(null);

			/*
			 * Do the actual reading.
			 * 
			 * There are two options: If the URI is known to the Eclipse
			 * Filesystem mechanism (EFS), we use this to acquire an InputStream
			 * which can be passed to the reader. Otherwise, we try to convert
			 * the URI to an (absolute) URL with a known protocol � I don't
			 * know if we ever need this. (-tv)
			 */

			try {
				final InputStream inputStream = EFS.getStore(inputURI)
				.openInputStream(EFS.NONE, null);
				if (inputStream == null) {
					throw new CoreException(new Status(IStatus.ERROR,
							VexPlugin.ID, "Could not open file " + inputURI));
				}
				try {
					setDoc(reader.read(inputStream));

				} finally {
					if (inputStream != null)
						inputStream.close();
				}
			} catch (final CoreException ce) {
				try {
					final URL inputURL = inputURI.toURL();
					setDoc(reader.read(inputURL));
				} catch (final MalformedURLException mue) {
					throw new CoreException(new Status(IStatus.ERROR,
							VexPlugin.ID, IStatus.ERROR,
							"Neither EFS nor java.net.URL can handle <URI:"
							+ inputURI + ">. \n"
							+ "URI.toURL() failed with "
							+ mue.getLocalizedMessage() + ". ", ce));
				}

			}

			if (isDebugging()) {
				final long end = System.currentTimeMillis();
				System.out
				.println("Parsed document in " + (end - start) + "ms"); //$NON-NLS-1$ //$NON-NLS-2$
			}

			// this.doctype is set either by wsPolicyFactory or entityResolver
			// this.style is set by wsPolicyFactory
			// Otherwise, a PartInitException would have been thrown by now

			final Validator validator = getDoctype().getValidator();
			if (validator != null) {
				getDoc().setValidator(validator);
				if (isDebugging()) {
					final long end = System.currentTimeMillis();
					System.out
					.println("Got validator in " + (end - start) + "ms"); //$NON-NLS-1$ //$NON-NLS-2$
				}
			}

			showVexWidget();

			vexWidget.setDebugging(isDebugging());

			vexWidget.setDocument(getDoc(), style
					.getStyleSheet());

			if (isUpdateDoctypeDecl()) {
				getDoc().setPublicID(getDoctype().getPublicId());
				getDoc().setSystemID(getDoctype().getSystemId());
				doSave(null);
			}

			loaded = true;
			setSavedUndoDepth(vexWidget.getUndoDepth());
			firePropertyChange(IEditorPart.PROP_DIRTY);
			setWasDirty(isDirty());

			vexEditorListeners.fireEvent(
					"documentLoaded", new VexEditorEvent(this)); //$NON-NLS-1$

		} catch (final SAXParseException ex) {

			if (ex.getException() instanceof NoRegisteredDoctypeException) {
				// TODO doc did not have document type and the user
				// declined to select another one. Should fail silently.
				String msg;
				final NoRegisteredDoctypeException ex2 = (NoRegisteredDoctypeException) ex
				.getException();
				if (ex2.getPublicId() == null) {
					msg = Messages.getString("VexEditor.noDoctype"); //$NON-NLS-1$
				} else {
					msg = MessageFormat.format(Messages
							.getString("VexEditor.unknownDoctype"), //$NON-NLS-1$
							new Object[] { ex2.getPublicId() });
				}
				showLabel(msg);
			} else if (ex.getException() instanceof NoStyleForDoctypeException) {
				final String msg = MessageFormat.format(Messages
						.getString("VexEditor.noStyles"), //$NON-NLS-1$
						new Object[] { getDoctype().getPublicId() });
				showLabel(msg);
			} else {
				String file = ex.getSystemId();
				if (file == null) {
					file = input.getName();
				}

				final String msg = MessageFormat.format(Messages
						.getString("VexEditor.parseError"), //$NON-NLS-1$
						new Object[] { new Integer(ex.getLineNumber()), file,
					ex.getLocalizedMessage() });

				showLabel(msg);

				VexPlugin.getInstance().log(IStatus.ERROR, msg, ex);
			}

		} catch (final Exception ex) {

			final String msg = MessageFormat.format(Messages
					.getString("VexEditor.unexpectedError"), //$NON-NLS-1$
					new Object[] { input.getName(),
				Platform.getLogFileLocation() });

			VexPlugin.getInstance().log(IStatus.ERROR, msg, ex);

			showLabel(msg);
		}
	}

	@Override
	public boolean isDirty() {
		if (vexWidget != null) {
			return getSavedUndoDepth() != vexWidget.getUndoDepth();
		} else {
			return false;
		}
	}

	/**
	 * Returns true if this editor has finished loading its document.
	 */
	public boolean isLoaded() {
		return loaded;
	}

	protected void setLoaded(final boolean loaded) {
		this.loaded = loaded;
	}

	@Override
	public boolean isSaveAsAllowed() {
		return true;
	}

	@Override
	public void createPartControl(final Composite parent) {

		parentControl = parent;
		showLabel(Messages.getString("VexEditor.loading")); //$NON-NLS-1$

		// initAndLoad(); // FIXME defer

		parent.addFocusListener(new FocusAdapter() {

			@Override
			public void focusGained(final FocusEvent e) {
				if (getState() == State.UNLOADED)
					initAndLoad();
			}
		});

	}

	public void initAndLoad() {

		if (getState() != State.UNLOADED)
			return;
		setState(State.LOADING);

		// from createPartControl:
		final ConfigRegistry registry = ConfigRegistry.getInstance();
		registry.addConfigListener(configListener);
		if (registry.isConfigLoaded()) {
			loadInput();
		} else {
			showLabel(Messages.getString("VexEditor.loading")); //$NON-NLS-1$
		}

	}

	/**
	 * Remove a VexEditorListener from the notification list.
	 * 
	 * @param listener
	 *            VexEditorListener to be removed.
	 */
	public void removeVexEditorListener(final IVexEditorListener listener) {
		vexEditorListeners.remove(listener);
	}

	protected ListenerList getVexEditorListeners() {
		return vexEditorListeners;
	}

	@Override
	public void setFocus() {
		if (vexWidget != null) {
			vexWidget.setFocus();
			setStatus(getLocation());
		}
	}

	@Override
	protected void setInput(final IEditorInput input) {

		super.setInput(input);
		setPartName(input.getName());
		setContentDescription(input.getName());
		setTitleToolTip(input.getToolTipText());


	}

	public void setStatus(final String text) {
		// this.statusLabel.setText(text);
		getEditorSite().getActionBars().getStatusLineManager().setMessage(
				text);
	}

	/**
	 * Sets the style for this editor.
	 * 
	 * @param style
	 *            Style to use.
	 */
	public void setStyle(final Style style) {
		this.style = style;
		if (vexWidget != null) {

			vexWidget.setStyleSheet(style.getStyleSheet());
			final Preferences prefs = new InstanceScope().getNode(VexPlugin.ID);
			final String key = getStylePreferenceKey(getDoc().getPublicID());
			prefs.put(key, style.getUniqueId());
			try {
				prefs.flush();
			} catch (final BackingStoreException e) {
				VexPlugin
				.getInstance()
				.log(
						IStatus.ERROR,
						Messages
						.getString("VexEditor.errorSavingStylePreference"), e); //$NON-NLS-1$
			}
		}
	}

	// ========================================================= PRIVATE

	private boolean debugging;

	private Composite parentControl;

	protected Composite getParentControl() {
		return parentControl;
	}

	private Label loadingLabel;

	private boolean loaded;
	private DocumentType doctype;
	public static IVexDocument doc;
	Style style;

	private VexWidget vexWidget;

	private int savedUndoDepth;
	private boolean wasDirty;
	// private Label statusLabel;

	// This is true if the document's doctype decl is missing or unrecognized
	// AND the user selected a new document type
	// AND the user wants to always use the doctype for this document
	private boolean updateDoctypeDecl;

	private final ListenerList vexEditorListeners = new ListenerList(
			IVexEditorListener.class, VexEditorEvent.class);

	private final SelectionProvider selectionProvider = new SelectionProvider();

	/**
	 * Returns the preference key used to access the style ID for documents with
	 * the same public ID as the current document.
	 */
	private static String getStylePreferenceKey(final String publicId) {
		return publicId + ".style"; //$NON-NLS-1$
	}

	protected void showLabel(final String message) {
		if (loadingLabel == null) {
			if (vexWidget != null) {
				vexWidget.dispose();
				vexWidget = null;
			}
			loadingLabel = new Label(parentControl, SWT.WRAP);
			loadingLabel.addPaintListener(new PaintListener() {

				public void paintControl(final PaintEvent e) {
					if (loadingLabel.isVisible() && getState() == State.UNLOADED) {
						initAndLoad();
					}
				}
			});
		}
		loadingLabel.setText(message);
		parentControl.layout(true);
	}

	protected void clearLoadingLabel() {
		loadingLabel.dispose();
		loadingLabel = null;
	}

	/*
	 * add favorite and fonts buttons to the toolbar
	 */

	public List favoriteElementArray = new LinkedList();

	public void Favorite() {

		final IActionBars actBar = getEditorSite().getActionBars();
		final IToolBarManager toolbarMgr = actBar.getToolBarManager();

		if (toolbarMgr.getItems().length == 0) {

			final Action f_Rem = new Action("", ImageDescriptor.createFromFile(
					null,
									getFilePathFromPluginForReading("icons/126-Loesche_Lieblingselement_in_XML.gif"))) {
				@Override
				public void run() {

					final IContributionItem[] items = toolbarMgr.getItems();
					if (items.length >= 4) {
						for (int i = 4; i < items.length; i++) {
							toolbarMgr.remove(items[i]);

						}
						toolbarMgr.update(true);

						final ConfigureFavorite f_elements = new ConfigureFavorite(
								getVexWidget());
						f_elements.favoriteElementArray = new LinkedList();

						favoriteElementArray = new LinkedList();

						((ActionContributionItem) items[1]).getAction()
						.setEnabled(
								false);
					}
				}
			};

			f_Rem.setEnabled(false);


			final Action f_Add = new Action(
					"",
					ImageDescriptor
					.createFromFile(
							null,
									getFilePathFromPluginForReading("icons/125-Einfuege_Lieblingselement_in_XML.gif"))) {
				@Override
				public void run() {


					String[][] data = null;
					final Display display = getVexWidget().getDisplay();
					final ConfigureFavorite confFavorite = new ConfigureFavorite(
							getVexWidget());

					final IContributionItem[] items = toolbarMgr.getItems();
					if (items.length >= 4) {
						for (int i = 4; i < items.length; i++) {
							favoriteElementArray.add(items[i]);

						}
					}
					confFavorite.favoriteElementArray = favoriteElementArray;

					data = confFavorite.getDocumentElements();

					// System.out.println("data: " + data.length);

					if (!confFavorite.favoriteShell.isVisible()) {
						confFavorite.configure(display, data);
					} else {
						confFavorite.favoriteShell.close();
						confFavorite.configure(display, data);
					}

				}
			};

			f_Add.setId("F_Add");
			f_Rem.setId("F_Rem");
			toolbarMgr.add(f_Add);
			toolbarMgr.add(f_Rem);

			// FontsButton dummyFontsbtn = new FontsButton("DummyFonts",
			// toolbarMgr);
			// toolbarMgr.add(dummyFontsbtn);


// final FontsButton fontsbtn = new FontsButton("Fonts",
			// toolbarMgr);
			// toolbarMgr.add(fontsbtn);

			// if (toolbarMgr.getItems()[1].getId() != "Fonts") {
			// toolbarMgr.remove("DummyFonts");
			// toolbarMgr.add(fontsbtn);
			// } else {
			// toolbarMgr.remove("DummyFonts");
			// toolbarMgr.find("Fonts").update();
			// }



			// XXX Disabled for beta release
			// final StyleSheetButton ssheetbtn = new StyleSheetButton("Style",
			// toolbarMgr);
			// toolbarMgr.add(ssheetbtn);

		}



	}

	boolean v_test = false;

	protected void showVexWidget() {

		clearLoadingLabel();

		if (vexWidget != null) {
			return;
		}

		// adding favorite

		Favorite();


		final GridLayout layout = new GridLayout();
		layout.numColumns = 1;
		layout.verticalSpacing = 0;
		layout.marginHeight = 0;
		layout.marginWidth = 0;
		parentControl.setLayout(layout);
		GridData gd;



		gd = new GridData();
		gd.grabExcessHorizontalSpace = true;
		gd.grabExcessVerticalSpace = true;
		gd.horizontalAlignment = GridData.FILL;
		gd.verticalAlignment = GridData.FILL;

		vexWidget = new VexWidget(parentControl, SWT.V_SCROLL);
		gd = new GridData();
		gd.grabExcessHorizontalSpace = true;
		gd.grabExcessVerticalSpace = true;
		gd.horizontalAlignment = GridData.FILL;
		gd.verticalAlignment = GridData.FILL;
		vexWidget.setLayoutData(gd);
		
		IWorkbenchHelpSystem helpSystem = getSite().getWorkbenchWindow().getWorkbench().getHelpSystem();
		helpSystem.setHelp(vexWidget, CONTEXT_HELP_ID); 
		if (helpSystem.isContextHelpDisplayed())
			helpSystem.displayHelp(CONTEXT_HELP_ID);

		// FIXME If we are e.g. part of a MultiPartEditor, then contributor
		// will be null. Not registering actions and context menus is better
		// than crashing, but we should implement some forwarding to the MPE's
		// contributor somewhere. -tv
		final IEditorSite editorSite = getEditorSite();

		/*
		 * if (editorSite instanceof MultiPageEditorSite) { MultiPageEditorSite
		 * multiSite = (MultiPageEditorSite) editorSite; editorSite =
		 * (IEditorSite) multiSite.getMultiPageEditor().getSite(); }
		 */

		final IVexActionBarContributor contributor = (IVexActionBarContributor) editorSite
		.getActionBarContributor();

		if (contributor == null) {
			StatusManager
			.getManager()
			.handle(
					new Status(
							IStatus.WARNING,
							VexPlugin.ID,
							"Could not get action bar contributor, thus unable to create actions.",
							new Exception()));
		} else {

			final MenuManager menuMgr = contributor.getContextMenuManager();
			getSite().registerContextMenu(menuMgr, vexWidget);
			vexWidget.setMenu(menuMgr.createContextMenu(vexWidget));

			setSavedUndoDepth(vexWidget.getUndoDepth());

			registerBindings();
		}

		/*
		 * call graphical editor content assistant
		 */

		final GEditorContentAssistant ContentWindow = new GEditorContentAssistant(
				getVexWidget());


		vexWidget.addSelectionChangedListener(selectionProvider);

		vexWidget.addKeyListener(new KeyListener() {

			@SuppressWarnings( { "restriction", "unchecked" })
			public void keyPressed(final KeyEvent event) {

				final Display display = getVexWidget().getDisplay();

				if (event.keyCode == '<') {

					// StyleSheet s = getVexWidget().getStyleSheet();
					//
					// Map values = s.getApplicableDecls(getVexWidget()
					// .getCurrentElement());
					//
					// for (int it = 0; it < values.size(); it++) {
					// System.out.println(values.get(getVexWidget()
					// .getCurrentElement()));
					// }



					// String p =
					// getFilePathFromPluginForReading("icons/teixlite-p4.css");
					// File f = new File(p);
					// String ssheet = readFileForCSS(f);
					// System.out.println(ssheet);
					//
					// StyleSheet s = null;
					//
					// try {
					// s = new StyleSheetReader().read(ssheet);
					// getVexWidget().setStyleSheet(s);
					// } catch (CSSException e1) {
					// e1.printStackTrace();
					// } catch (IOException e1) {
					// e1.printStackTrace();
					// }

					// String p =
					// getFilePathFromPluginForReading("icons/teixlite-p4.css");
					// File f = new File(p);
					// try {
					// getVexWidget().setStyleSheet(f.toURL());
					// } catch (MalformedURLException e) {
					// e.printStackTrace();
					// } catch (IOException e) {
					// e.printStackTrace();
					// }


					final ConfigureFavorite f_elements = new ConfigureFavorite(
							getVexWidget());
					final String[][] data = f_elements.getDocumentElements();
					ContentWindow.contentAssistMain(display, data, "true");

				}

			}

			public void keyReleased(final KeyEvent arg0) {
				// TODO Auto-generated method stub

			}

		});


		parentControl.layout(true);
		// FIXME an awkward place to do this
		PreferenceChangeHandler.getInstance().setAllPreferences();

	}

	protected void registerBindings() {
		final IKeyBindingService kbs = getSite().getKeyBindingService();
		kbs.setScopes(new String[] { "net.sf.vex.editor.VexEditorContext" }); //$NON-NLS-1$
		kbs
		.registerAction(new VexActionAdapter(this,
				new ChangeElementAction()));
		kbs
		.registerAction(new VexActionAdapter(this,
				new DeleteColumnAction()));
		kbs.registerAction(new VexActionAdapter(this, new DeleteRowAction()));
		kbs.registerAction(new VexActionAdapter(this,
				new DuplicateSelectionAction()));
		kbs.registerAction(new VexActionAdapter(this,
				new InsertColumnAfterAction()));
		kbs.registerAction(new VexActionAdapter(this,
				new InsertColumnBeforeAction()));
		kbs
		.registerAction(new VexActionAdapter(this,
				new InsertElementAction()));
		kbs.registerAction(new VexActionAdapter(this,
				new InsertRowAboveAction()));
		kbs.registerAction(new VexActionAdapter(this,
				new InsertRowBelowAction()));
		kbs.registerAction(new VexActionAdapter(this,
				new MoveColumnLeftAction()));
		kbs.registerAction(new VexActionAdapter(this,
				new MoveColumnRightAction()));
		kbs.registerAction(new VexActionAdapter(this, new MoveRowDownAction()));
		kbs.registerAction(new VexActionAdapter(this, new MoveRowUpAction()));
		kbs
		.registerAction(new VexActionAdapter(this,
				new NextTableCellAction()));
		kbs.registerAction(new VexActionAdapter(this, new PasteTextAction()));
		kbs.registerAction(new VexActionAdapter(this,
				new PreviousTableCellAction()));
		kbs
		.registerAction(new VexActionAdapter(this,
				new RemoveElementAction()));
		kbs.registerAction(new VexActionAdapter(this,
				new RestoreLastSelectionAction()));
		kbs.registerAction(new VexActionAdapter(this, new SplitAction()));
		kbs.registerAction(new VexActionAdapter(this, new SplitItemAction()));
	}

	private void handleResourceChanged(final IResourceDelta delta) {

		if (delta.getKind() == IResourceDelta.CHANGED) {
			if ((delta.getFlags() & IResourceDelta.CONTENT) != 0) {
				handleResourceContentChanged();
			}
		} else if (delta.getKind() == IResourceDelta.REMOVED) {
			if ((delta.getFlags() & IResourceDelta.MOVED_TO) != 0) {
				final IPath toPath = delta.getMovedToPath();
				final IFile file = ResourcesPlugin.getWorkspace().getRoot().getFile(
						toPath);
				setInput(new FileEditorInput(file));
			} else {
				if (!isDirty()) {
					getEditorSite().getPage().closeEditor(this, false);
				} else {
					handleResourceDeleted();
				}
			}
		}


	}

	private void handleResourceContentChanged() {

		if (!isDirty()) {
			loadInput();
		} else {

			final String message = MessageFormat.format(Messages
					.getString("VexEditor.docChanged.message"), //$NON-NLS-1$
					new Object[] { getEditorInput().getName() });

			final MessageDialog dlg = new MessageDialog(
					getSite().getShell(),
					Messages.getString("VexEditor.docChanged.title"), //$NON-NLS-1$
					null,
					message,
					MessageDialog.QUESTION,
					new String[] {
						Messages.getString("VexEditor.docChanged.discard"), //$NON-NLS-1$
						Messages
						.getString("VexEditor.docChanged.overwrite") }, //$NON-NLS-1$
						1);

			final int result = dlg.open();

			if (result == 0) { // Discard my changes
				loadInput();
			} else { // Overwrite other changes
				doSave(null);
			}
		}
	}

	private void handleResourceDeleted() {

		final String message = MessageFormat.format(Messages
				.getString("VexEditor.docDeleted.message"), //$NON-NLS-1$
				new Object[] { getEditorInput().getName() });

		final MessageDialog dlg = new MessageDialog(getSite().getShell(),
				Messages.getString("VexEditor.docDeleted.title"), //$NON-NLS-1$
				null, message, MessageDialog.QUESTION, new String[] {
			Messages.getString("VexEditor.docDeleted.discard"), //$NON-NLS-1$ 
			Messages.getString("VexEditor.docDeleted.save") }, //$NON-NLS-1$
			1);

		final int result = dlg.open();

		if (result == 0) { // Discard

			getEditorSite().getPage().closeEditor(this, false);

		} else { // Save

			doSaveAs();

			// Check if they saved or not. If not, close the editor
			if (!getEditorInput().exists()) {
				getEditorSite().getPage().closeEditor(this, false);
			}
		}
	}

	// Listen for stylesheet changes and respond appropriately
	private final IConfigListener configListener = new IConfigListener() {

		public void configChanged(final ConfigEvent e) {
			if (style != null) {
				final ConfigRegistry registry = ConfigRegistry.getInstance();
				final String currId = style.getUniqueId();
				final Style newStyle = (Style) registry.getConfigItem(
						Style.EXTENSION_POINT, currId);
				if (newStyle == null) {
					// Oops, style went bye-bye
					// Let's just hold on to it in case it comes back later
				} else {

					vexWidget.setStyleSheet(newStyle.getStyleSheet());
					style = newStyle;
				}
			}
		}

		public void configLoaded(final ConfigEvent e) {
			loadInput();
		}
	};

	private final ISelectionChangedListener selectionChangedListener = new ISelectionChangedListener() {
		public void selectionChanged(final SelectionChangedEvent event) {
			if (isDirty() != isWasDirty()) {
				firePropertyChange(IEditorPart.PROP_DIRTY);
				setWasDirty(isDirty());
			}
			setStatus(getLocation());
		}
	};

	private final EntityResolver entityResolver = new EntityResolver() {
		public InputSource resolveEntity(final String publicId, final String systemId)
		throws SAXException, IOException {

			// System.out.println("### Resolving publicId " + publicId +
			// ", systemId " + systemId);

			if (getDoctype() == null) {
				//
				// If doctype hasn't already been set, this must be the doctype
				// decl.
				//
				if (publicId != null) {
					setDoctype(DocumentType.getDocumentType(publicId));
				}

				if (getDoctype() == null) {
					final DocumentTypeSelectionDialog dlg = DocumentTypeSelectionDialog
					.create(getSite().getShell(), publicId);
					dlg.open();
					setDoctype(dlg.getDoctype());
					setUpdateDoctypeDecl(dlg.alwaysUseThisDoctype());

					if (getDoctype() == null) {
						throw new NoRegisteredDoctypeException(publicId);
					}
				}

				final URL url = getDoctype().getResourceUrl();

				if (url == null) {
					final String message = MessageFormat.format(Messages
							.getString("VexEditor.noUrlForDoctype"), //$NON-NLS-1$
							new Object[] { publicId });
					throw new RuntimeException(message);
				}

				return new InputSource(url.toString());
			} else {
				return null;
			}
		}
	};

	private final IWhitespacePolicyFactory wsFactory = new IWhitespacePolicyFactory() {
		public IWhitespacePolicy getPolicy(final String publicId) {

			if (getDoctype() == null) {
				final DocumentTypeSelectionDialog dlg = DocumentTypeSelectionDialog
				.create(getSite().getShell(), publicId);
				dlg.open();
				setDoctype(dlg.getDoctype());
				setUpdateDoctypeDecl(dlg.alwaysUseThisDoctype());

				if (getDoctype() == null) {
					throw new NoRegisteredDoctypeException(null);
				}
			}

			style = VexEditor.findStyleForDoctype(getDoctype().getPublicId());
			if (style == null) {
				throw new NoStyleForDoctypeException(getDoctype());
			}

			return new CssWhitespacePolicy(style.getStyleSheet());
		}

	};

	// FIXME produce some whitespace policy factory for the linked model
	protected IWhitespacePolicyFactory getWsFactory() {
		return wsFactory;
	}

	private class ResourceChangeListener implements IResourceChangeListener {

		public void resourceChanged(final IResourceChangeEvent event) {

			if (saving) {
				return;
			}

			final IPath path = ((IFileEditorInput) getEditorInput()).getFile()
			.getFullPath();

			final IResourceDelta delta = event.getDelta().findMember(path);
			if (delta != null) {
				Display.getDefault().asyncExec(new Runnable() {
					public void run() {
						handleResourceChanged(delta);
					}
				});
			}
		}

		public void setSaving(final boolean saving) {
			this.saving = saving;
		}

		// Set to true so we can ignore change events while we're saving.
		private boolean saving;
	};

	private final ResourceChangeListener resourceChangeListener = new ResourceChangeListener();

	//
	// wsFactory communicates failures back to init() through the XML parser
	// by throwing one of these exceptions
	//

	/**
	 * Indicates that no document type is registered for the public ID in the
	 * document, or that the document does not have a PUBLIC DOCTYPE decl, in
	 * which case publicId is null.
	 */
	class NoRegisteredDoctypeException extends RuntimeException {
		public NoRegisteredDoctypeException(final String publicId) {
			this.publicId = publicId;
		}

		public String getPublicId() {
			return publicId;
		}

		private final String publicId;
	}

	/**
	 * Indicates that the document was matched to a registered doctype, but that
	 * the given doctype does not have a matching style.
	 */
	class NoStyleForDoctypeException extends RuntimeException {

		public NoStyleForDoctypeException(final DocumentType doctype) {
			this.doctype = doctype;
		}

		public DocumentType getDoctype() {
			return doctype;
		}

		private final DocumentType doctype;
	}

	private String getLocation() {
		final List path = new ArrayList();
		IVexElement element = vexWidget.getCurrentElement();
		while (element != null) {
			path.add(element.getName());
			element = element.getParent();
		}
		Collections.reverse(path);
		final StringBuffer sb = new StringBuffer(path.size() * 15);
		for (int i = 0; i < path.size(); i++) {
			sb.append("/"); //$NON-NLS-1$
			sb.append(path.get(i));
		}

		return sb.toString();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Object getAdapter(final Class adapter) {

		if (adapter == IContentOutlinePage.class) {

			return new DocumentOutlinePage();

		} else if (adapter == IPropertySheetPage.class) {
			final PropertySheetPage page = new PropertySheetPage();
			page.setPropertySourceProvider(new IPropertySourceProvider() {
				public IPropertySource getPropertySource(final Object object) {
					if (object instanceof IVexElement) {
						final IStructuredSelection sel = (IStructuredSelection) vexWidget
						.getSelection();
						final boolean multi = (sel != null && sel.size() > 1);
						final Validator validator = vexWidget.getDocument()
						.getValidator();
						return new ElementPropertySource((IVexElement) object,
								validator, multi);
					} else {
						return null;
					}
				}
			});
			return page;
		} else {
			return super.getAdapter(adapter);
		}
	}

	protected void setDebugging(final boolean debugging) {
		this.debugging = debugging;
	}

	public boolean isDebugging() {
		return debugging;
	}

	protected void setDoc(final IVexDocument doc) {
		VexEditor.doc = doc;
	}

	public IVexDocument getDoc() {
		return doc;
	}

	protected void setUpdateDoctypeDecl(final boolean updateDoctypeDecl) {
		this.updateDoctypeDecl = updateDoctypeDecl;
	}

	public boolean isUpdateDoctypeDecl() {
		return updateDoctypeDecl;
	}

	protected void setDoctype(final DocumentType doctype) {
		this.doctype = doctype;
	}

	public DocumentType getDoctype() {
		return doctype;
	}

	protected void setSavedUndoDepth(final int savedUndoDepth) {
		this.savedUndoDepth = savedUndoDepth;
	}

	public int getSavedUndoDepth() {
		return savedUndoDepth;
	}

	protected void setWasDirty(final boolean wasDirty) {
		this.wasDirty = wasDirty;
	}

	public boolean isWasDirty() {
		return wasDirty;
	}

	public IRegion getSelectedSourceRegion() {

		IWorkbenchPage page;
		page = PlatformUI.getWorkbench().getActiveWorkbenchWindow()
		.getActivePage();
		final IEditorPart part = page.getActiveEditor();
		final ITextEditor editor = AdapterUtils.getAdapter(part, ITextEditor.class);
		if (editor != null) {
			final ITextSelection selection = (ITextSelection) editor
			.getSelectionProvider().getSelection();
			final int length = selection.getLength();
			final int offset = selection.getOffset();

			return new Region(offset, length);

		}

		else if (getVexWidget() != null) {

			final TextSelectionVex vexSelection = new TextSelectionVex(getVexWidget());
			final int startOffset = vexSelection.getSourceStartOffset();
			final int endOffset = vexSelection.getSourceEndOffset();

			return new Region(startOffset, endOffset - startOffset);
		}
		return null;
	}


	public String getFilePathFromPluginForReading(final String path) {

		String filepath = null;

		final URL filep = Activator.getDefault().getBundle("net.sf.vex.editor")
		.getEntry(path);

		// URL filep = Platform.getBundle("net.sf.vex.editor").getEntry(path);
		try {
			filepath = FileLocator.resolve(filep).getFile();
		} catch (final IOException e) {
			e.printStackTrace();
		}

		return filepath;
	}

	public String readFileForCSS(final File file) {

		String out = null;

		try {
			final BufferedReader reader = new BufferedReader(new FileReader(file));
			final StringBuffer buffer = new StringBuffer();
			String line = null;
			while ((line = reader.readLine()) != null) {
				buffer.append(line);
				buffer.append("\n");

			}

			out = buffer.toString();

		} catch (final IOException e) {

		}

		return out;

	}


	/*
	 * creates fonts button to be added to the toolbar
	 */

	class FontsButton extends ControlContribution {

		Button btn;
		IToolBarManager toolbarMgr;

		public FontsButton(final String str, final IToolBarManager toolbarMgr) {
			super(str);
			this.toolbarMgr = toolbarMgr;
		}

		@Override
		protected Control createControl(final Composite parent) {

			btn = new Button(parent, SWT.NONE | SWT.DROP_DOWN | SWT.READ_ONLY
					| SWT.FLAT | SWT.PUSH);

			btn.setText("Fonts");
			btn.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(final SelectionEvent e) {

					final FontsList fontsList = new FontsList(getVexWidget());
					fontsList.getAllFonts();
					toolbarMgr.update(true);
				}

			});

			return btn;
		}

	}

	/*
	 * Button for associating a stylesheet with the document
	 */

	class StyleSheetButton extends ControlContribution {

		Button btn;
		IToolBarManager toolbarMgr;

		public StyleSheetButton(final String str, final IToolBarManager toolbarMgr) {
			super(str);
			this.toolbarMgr = toolbarMgr;
		}

		@Override
		protected Control createControl(final Composite parent) {

			btn = new Button(parent, SWT.NONE | SWT.DROP_DOWN | SWT.READ_ONLY
					| SWT.FLAT | SWT.PUSH);

			btn.setText("Style");
			btn.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(final SelectionEvent e) {


					final String p = getFileName(); // getFilePathFromPluginForReading("icons/teixlite-p4.css");

					if (p != null) {
						final File f = new File(p);
						final String ssheet = readFileForCSS(f);
						// System.out.println(f.toString());
						StyleSheet s = null;

						try {
							s = new StyleSheetReader().read(ssheet);
							getVexWidget().setStyleSheet(s);
						} catch (final CSSException e1) {
							e1.printStackTrace();
						} catch (final IOException e1) {
							e1.printStackTrace();
						}
					}
				}

			});

			return btn;
		}

	}

	String getFileName() {
		final FileDialog dialog = new FileDialog(getVexWidget().getShell(), SWT.OPEN);
		final String selectedFile = dialog.open();
		if (selectedFile == null) {
			// System.out.println("File is not opened");
			return null;
		}
		if (selectedFile.indexOf(".css") == -1) {

			final MessageBox messageBox = new MessageBox(getVexWidget().getShell(),
					SWT.NONE);
			messageBox.setMessage("Please select a CSS file");
			messageBox.setText("Warning");
			messageBox.open();

			return null;
		}
		final File file = new File(selectedFile);
		String filename = file.getPath();
		filename = filename.replace("\\", "/");
		return filename;
	}

	protected void setState(State state) {
		this.state = state;
	}


}


