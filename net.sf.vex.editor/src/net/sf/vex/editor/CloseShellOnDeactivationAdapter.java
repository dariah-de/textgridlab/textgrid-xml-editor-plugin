/**
 * 
 */
package net.sf.vex.editor;

import org.eclipse.swt.events.ShellAdapter;
import org.eclipse.swt.events.ShellEvent;
import org.eclipse.swt.widgets.Shell;

public final class CloseShellOnDeactivationAdapter extends ShellAdapter {
	public CloseShellOnDeactivationAdapter(Shell shell) {
		shell.addShellListener(this);
	}

	@Override
	public void shellDeactivated(ShellEvent e) {
		super.shellDeactivated(e);
		((Shell) e.getSource()).close();
	}

	public static void installOn(Shell shell) {
		new CloseShellOnDeactivationAdapter(shell);
	}

}