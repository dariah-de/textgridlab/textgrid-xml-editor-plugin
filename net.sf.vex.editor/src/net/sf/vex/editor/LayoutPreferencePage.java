package net.sf.vex.editor;

import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.ColorFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.preference.RadioGroupFieldEditor;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

/**
 * A preference page for vex's appearance
 */
public class LayoutPreferencePage extends FieldEditorPreferencePage implements
		IWorkbenchPreferencePage {

	public static final String PREFERENCE_SHOW_INLINE_MARKERS = "ShowInlineMarkers";

	public static final String PREFERENCE_INLINE_MARKER_DISPLAY = "InlineMarkerDisplay";
	public static final String VALUE_INLINE_MARKER_DISPLAY_LABELED = "Labeled";
	public static final String VALUE_INLINE_MARKER_DISPLAY_UNLABELED = "Unlabeled";

	public static final String PREFERENCE_SHOW_BLOCK_MARKERS = "ShowBlockMarkers";

	public static final String PREFERENCE_BLOCK_MARKER_DISPLAY = "BlockMarkerDisplay";
	public static final String VALUE_BLOCK_MARKER_DISPLAY_INLINE_LIKE = "InlineLike";
	public static final String VALUE_BLOCK_MARKER_DISPLAY_NESTED = "Nested";
	public static final String VALUE_BLOCK_MARKER_DISPLAY_WIDE = "Wide";

	public static final String PREFERENCE_BLOCK_MARKER_COLOR = "BlockMarkerColor";

	public LayoutPreferencePage() {
		super(FieldEditorPreferencePage.GRID);
		IPreferenceStore store = VexPlugin.getInstance().getPreferenceStore();
		setPreferenceStore(store);
	}

	@Override
	protected void createFieldEditors() {

		// Show Inline Markers

		BooleanFieldEditor showInlineMarkers = new BooleanFieldEditor(
				PREFERENCE_SHOW_INLINE_MARKERS, "Show &Inline Markers",
				getFieldEditorParent());
		addField(showInlineMarkers);

		// Inline Marker Display

		RadioGroupFieldEditor inlineMarkerDisplay = new RadioGroupFieldEditor(
				PREFERENCE_INLINE_MARKER_DISPLAY,
				"Inline Marker &Display",
				2,
				new String[][] {
						{ "Labeled", VALUE_INLINE_MARKER_DISPLAY_LABELED },
						{ "Unlabeled", VALUE_INLINE_MARKER_DISPLAY_UNLABELED }, },
				getFieldEditorParent(), true);
		addField(inlineMarkerDisplay);

		// Show Block Markers

		BooleanFieldEditor showBlockMarkers = new BooleanFieldEditor(
				PREFERENCE_SHOW_BLOCK_MARKERS, "Show &Block Markers",
				getFieldEditorParent());
		addField(showBlockMarkers);

		// Block marker color

		ColorFieldEditor blockMarkerColor = new ColorFieldEditor(
				PREFERENCE_BLOCK_MARKER_COLOR, "Block Marker &Color",
				getFieldEditorParent());
		addField(blockMarkerColor);

		// Block Marker Display

		// Not implemented yet.
		//		
		// RadioGroupFieldEditor blockMarkerDisplay = new
		// RadioGroupFieldEditor(PREFERENCE_BLOCK_MARKER_DISPLAY, "Block Marker
		// &Display", 3,
		// new String[][]{
		// {"Like &Inline Elements", VALUE_BLOCK_MARKER_DISPLAY_INLINE_LIKE},
		// {"&Nested", VALUE_BLOCK_MARKER_DISPLAY_NESTED},
		// {"&Wide", VALUE_BLOCK_MARKER_DISPLAY_WIDE}
		//				
		// },getFieldEditorParent(), true);
		// addField(blockMarkerDisplay);

	}

	public void init(IWorkbench workbench) {
		// TODO Auto-generated method stub

	}

}
