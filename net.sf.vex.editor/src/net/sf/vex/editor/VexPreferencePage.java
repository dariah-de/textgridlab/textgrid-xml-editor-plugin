package net.sf.vex.editor;

import org.eclipse.jface.preference.PreferencePage;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

public class VexPreferencePage extends PreferencePage implements
		IWorkbenchPreferencePage {

	public VexPreferencePage() {
	}

	public VexPreferencePage(String title) {
		super(title);
	}

	public VexPreferencePage(String title, ImageDescriptor image) {
		super(title, image);
	}

	@Override
	protected Control createContents(Composite parent) {
		return null;
	}

	public void init(IWorkbench workbench) {
	}

}
