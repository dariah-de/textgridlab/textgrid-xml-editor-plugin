/*
 * class responsible for creating and opening a content assistant for the VexEditor input
 * on pressing "<" a content assistant window is displayed 
 * @author Mohamadou Nassourou <mohamadou.nassourou@uni-wuerzburg.de> for TextGrid
 * 
 * 
 */

package net.sf.vex.editor;

import info.textgrid.lab.core.swtutils.AdapterUtils;

import java.text.MessageFormat;
import java.util.LinkedList;
import java.util.List;

import net.sf.vex.dom.IVexDocument;
import net.sf.vex.dom.IVexElement;
import net.sf.vex.dom.linked.LinkedNode;
import net.sf.vex.swt.VexWidget;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.action.ToolBarManager;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.text.Region;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.MouseMoveListener;
import org.eclipse.swt.events.ShellAdapter;
import org.eclipse.swt.events.ShellEvent;
import org.eclipse.swt.graphics.Cursor;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.texteditor.ITextEditor;
import org.eclipse.wst.sse.core.internal.provisional.text.IStructuredDocument;
import org.eclipse.wst.xml.core.internal.provisional.document.IDOMNode;
import org.eclipse.wst.xml.ui.internal.taginfo.MarkupTagInfoProvider;
import org.w3c.dom.Node;

/*
 * class responsible for creating content assistant
 */
public class GEditorContentAssistant {

	/*** content assistant parameters ***/

	public Shell shellDesc = new Shell(); // SWT.NO_TRIM |
	// SWT.BORDER_SOLID
	// | SWT.LINE_SOLID | SWT.SHADOW_NONE | SWT.ON_TOP);
	public Shell contentAssistShell = new Shell();

	public Browser elementDescription = new Browser(shellDesc, SWT.NONE);

	public int shellDesc_posx = 0;
	public int shellDesc_posy = 0;

	public String string = null;

	/*****/

	public Text text;
	public String result;

	VexWidget vex = null;

	public static IVexDocument doc;

	public boolean removeCharFlag = false;

	public static final String fgStyleSheet = "/* Font definitions */\n" + //$NON-NLS-1$
			"body, h1, h2, h3, h4, h5, h6, p, table, td, caption, th, ul, ol, dl, li, dd, dt {font-family: sans-serif; font-size: 9pt }\n"
			+ //$NON-NLS-1$ 
			"pre				{ font-family: monospace; font-size: 9pt }\n"
			+ //$NON-NLS-1$
			"\n"
			+ //$NON-NLS-1$
			"/* Margins */\n"
			+ //$NON-NLS-1$
			"body	     { overflow: auto; margin-top: 0; margin-bottom: 4; margin-left: 3; margin-right: 0; background-color:InfoBackground;border-style:solid;border-color:black;border-width:1px;}\n"
			+ //$NON-NLS-1$ 
			"h1           { margin-top: 5; margin-bottom: 1 }	\n"
			+ //$NON-NLS-1$
			"h2           { margin-top: 25; margin-bottom: 3 }\n"
			+ //$NON-NLS-1$
			"h3           { margin-top: 20; margin-bottom: 3 }\n"
			+ //$NON-NLS-1$
			"h4           { margin-top: 20; margin-bottom: 3 }\n"
			+ //$NON-NLS-1$
			"h5           { margin-top: 0; margin-bottom: 0 }\n"
			+ //$NON-NLS-1$
			"p            { margin-top: 10px; margin-bottom: 10px }\n"
			+ //$NON-NLS-1$
			"pre	         { margin-left: 6 }\n"
			+ //$NON-NLS-1$
			"ul	         { margin-top: 0; margin-bottom: 10 }\n"
			+ //$NON-NLS-1$ 
			"li	         { margin-top: 0; margin-bottom: 0 } \n"
			+ //$NON-NLS-1$
			"li p	     { margin-top: 0; margin-bottom: 0 } \n"
			+ //$NON-NLS-1$
			"ol	         { margin-top: 0; margin-bottom: 10 }\n"
			+ //$NON-NLS-1$
			"dl	         { margin-top: 0; margin-bottom: 10 }\n"
			+ //$NON-NLS-1$
			"dt	         { margin-top: 0; margin-bottom: 0; font-weight: bold }\n"
			+ //$NON-NLS-1$ 
			"dd	         { margin-top: 0; margin-bottom: 0 }\n"
			+ //$NON-NLS-1$
			"\n"
			+ //$NON-NLS-1$
			"/* Styles and colors */\n"
			+ //$NON-NLS-1$
			"a:link	     { color: #0000FF }\n"
			+ //$NON-NLS-1$
			"a:hover	     { color: #000080 }\n"
			+ //$NON-NLS-1$
			"a:visited    { text-decoration: underline }\n"
			+ //$NON-NLS-1$
			"h4           { font-style: italic }\n"
			+ //$NON-NLS-1$ 
			"strong	     { font-weight: bold }\n"
			+ //$NON-NLS-1$
			"em	         { font-style: italic }\n"
			+ //$NON-NLS-1$
			"var	         { font-style: italic }\n"
			+ //$NON-NLS-1$
			"th	         { font-weight: bold }\n"
			+ //$NON-NLS-1$
			"p.spec       { font-size: 7pt; color: gray; border-top: inset 2pt; }\n"
			+ "";

	public GEditorContentAssistant(VexWidget vex) {
		this.vex = vex;
	}

	/*
	 * inserts selected elements from favorite popup dialog in the document
	 */
	public void Favorite(String string) {


		IWorkbenchPage page;
		page = PlatformUI.getWorkbench().getActiveWorkbenchWindow()
				.getActivePage();
		IEditorPart part = page.getActiveEditor();
		ITextEditor editor = AdapterUtils.getAdapter(part, ITextEditor.class);

		IStructuredDocument doc2 = null;

		if (editor != null) {

			IDocument doc = editor.getDocumentProvider().getDocument(
					editor.getEditorInput());

			doc2 = (IStructuredDocument) editor.getDocumentProvider()
					.getDocument(editor.getEditorInput());

		}

		StringBuffer sb = new StringBuffer();
		IRegion selection = getSelectedSourceRegion();

		String insert = "";

		try {
			if (selection.getLength() == 0) {
				insert = "<" + string + ">" + "</" + string + ">";
				insert = insert.replace(" ", "");

				doc2.replace(selection.getOffset(), 0, insert);
				this.vex.reLayout();

			} else {
				insert = "<" + string + ">" + getSelectedText().trim() + "</"
						+ string + ">";
				doc2.replace(selection.getOffset(), selection.getLength(),
						insert);
				this.vex.reLayout();
			}

			// editor.getSite().getShell().redraw();

		} catch (BadLocationException ex) {

		}

	}

	/*
	 * inserts new element / selected element from content assistant in the
	 * document
	 */
	public void InsertNew(String to_be_inserted, String charFlag) {

		removeCharFlag = true;

		LinkedNode currentLinkedNode = (LinkedNode) this.vex
				.getCurrentNode();
		Node currentNode = currentLinkedNode.getDomNode();
		IStructuredDocument document = ((IDOMNode) currentNode)
				.getStructuredDocument();
		StringBuffer sb = new StringBuffer();
		IRegion selection = getSelectedSourceRegion();

		try {
			if (selection.getLength() == 0) {
				to_be_inserted = to_be_inserted.replace(" ", "");
				String insert = "<" + to_be_inserted + ">" + "</"
						+ to_be_inserted + ">";

				if (to_be_inserted.equalsIgnoreCase("#comment")) {
					insert = "<!-- -->";
				}
				// System.out.println(insert);

				if (charFlag.equals("true"))
					document.replace(selection.getOffset() - 1, 4, insert);
				else
					document.replace(selection.getOffset(), 0, insert);


				charFlag = "false";

				this.vex.reLayout();

			}
		} catch (BadLocationException ex) {

		}

	}

	/*
	 * rename selected element from content assistant
	 */
	public void renameElement(String old_elt, String newStr) {

		LinkedNode currentLinkedNode = (LinkedNode) this.vex
				.getCurrentNode();
		Node currentNode = currentLinkedNode.getDomNode();
		IStructuredDocument document = ((IDOMNode) currentNode)
				.getStructuredDocument();
		// StringBuffer sb = new StringBuffer();
		VexWidget vexWidget = this.vex;
		IVexElement c_element = vexWidget.getCurrentElement();
		// System.out.println("c_element: " + c_element + " : " + string);

		StringBuffer sb = new StringBuffer();
		IRegion selection = getSelectedSourceRegion();

		IVexElement root = doc.getRootElement();
		IVexElement[] child_elements = root.getChildElements();
		String string2 = root.getText();
		String old = old_elt.replace("<>", "").trim();
		// System.out.println(doc);
		// IDOMModel model = null;
		// IDOMElement domElement = (IDOMElement) root;
		// model = domElement.getModel();
		// System.out.println(domElement.getNodeName());

		for (int i = 0; i < child_elements.length; i++) {

			if (child_elements[i].getName().equalsIgnoreCase(old)) {
				// System.out.println(child_elements[i].getName() + " : "
				// + old);

				// model = domElement.getModel();
				// oldName_child = domElement.getNodeName();
				// model.getStructuredDocument().replaceText(this, i,
				// old.length(), newStr);

			}
		}

	}

	/*
	 * remove "<" char after inerting element from content assistant
	 */
	public void deleteInsertingChar() {

		LinkedNode currentLinkedNode = (LinkedNode) this.vex
				.getCurrentNode();
		Node currentNode = currentLinkedNode.getDomNode();
		IStructuredDocument document = ((IDOMNode) currentNode)
				.getStructuredDocument();
		StringBuffer sb = new StringBuffer();
		IRegion selection = getSelectedSourceRegion();

		try {
			document.replace(selection.getOffset() - 1, 4, "");

			this.vex.reLayout();
		} catch (BadLocationException ex) {
		}
	}


	/*
	 * open the tags elements window
	 */

	public Point originalPosition = null;

	public String contentAssistMain(final Display display,
			final String[][] data, final String charFlag) {

		contentAssistShell = new Shell(display, SWT.NO_TRIM | SWT.BORDER_SOLID
				| SWT.LINE_SOLID | SWT.SHADOW_NONE | SWT.ON_TOP);


		
		contentAssistShell.setBounds(10, 10, 200, 260);
		contentAssistShell.setText("Available tags");


		Point absolutePosition = this.vex.getDisplay().getCurrent()
				.getCursorLocation();

		Point pt = contentAssistShell.toDisplay(absolutePosition.x,
				absolutePosition.y);


		contentAssistShell.setLocation(pt.x, pt.y);
		shellDesc_posx = pt.x + 200;
		shellDesc_posy = pt.y;


		contentAssistShell.setBackground(display
				.getSystemColor(SWT.COLOR_WIDGET_LIGHT_SHADOW));
		contentAssistShell.setForeground(display
				.getSystemColor(SWT.COLOR_BLACK));



		final TableViewer tableViewer = new TableViewer(contentAssistShell,
				SWT.BORDER_SOLID | SWT.V_SCROLL | SWT.Selection);

		tableViewer.setContentProvider(new DescriptionContentProvider());

		final Table table = tableViewer.getTable();
		table.setBounds(10, 5, 180, 160);
		table.setLayoutData(new GridData(GridData.FILL_BOTH));

		tableViewer.setInput(new DataElement(data));

		tableViewer
				.addSelectionChangedListener(new ISelectionChangedListener() {

					public void selectionChanged(SelectionChangedEvent event) {
						ISelection sel = tableViewer.getSelection();
						getElementDescription(sel, data);
						string = sel.toString().substring(1,
								sel.toString().length() - 1);

					}
				});

		table.addFocusListener(new FocusListener() {
			public void focusGained(FocusEvent arg0) {

				if (table.getItemCount() > 0) {
					table.select(0);
				}
				if (shellDesc == null || shellDesc.isDisposed()
						|| !shellDesc.isVisible()) {
					contentAssistDescription(display, data[1][0]);
				}
			}

			public void focusLost(FocusEvent event) {
			}

		});

		table.addListener(SWT.DefaultSelection, new Listener() {
			public void handleEvent(Event e) {
				InsertNew(string, charFlag);

				if (contentAssistShell.isVisible()) {
					contentAssistShell.setVisible(false);
					shellDesc.setVisible(false);
				}
			}
		});

// table.addMouseListener(new MouseListener() {
		//
		// @Override
		// public void mouseUp(MouseEvent e) {
		// // TODO Auto-generated method stub
		//
		// }
		//
		// @Override
		// public void mouseDown(MouseEvent e) {
		// // TODO Auto-generated method stub
		//
		// }
		//
		// @Override
		// public void mouseDoubleClick(MouseEvent e) {
		//
		// System.out.println("yes");
		//
		// InsertNew(string);
		//
		// if (contentAssistShell.isVisible()) {
		// contentAssistShell.setVisible(false);
		// shellDesc.setVisible(false);
		// }
		//
		// }
		// });

		Action actionAddNew = new Action("&Insert New") {
			@Override
			public void run() {

				String t = text.getText();
				if (t != "") {
					InsertNew(t, charFlag);

					// System.out.println(contentAssistShell.isVisible());
					if (contentAssistShell.isVisible()) {
						// System.out.println("close");
						contentAssistShell.setVisible(false);
						shellDesc.setVisible(false);
					}
				}
			}
		};
		// actionAddNew.setAccelerator(SWT.CTRL + 'I');

		Action actionRename = new Action("&Rename") {
			@Override
			public void run() {

				// System.out.println(table.getSelectionIndex());

				String old_elt = table.getItem(table.getSelectionIndex())
						.getText();

				table.getItem(table.getSelectionIndex()).setText(
						"<> " + text.getText());

				String newStr = table.getItem(table.getSelectionIndex())
						.getText();

				renameElement(old_elt, newStr);

			}
		};
		// actionRename.setAccelerator(SWT.CTRL + 'R');

		// Action actionDelete = new Action("&Delete") {
		// @Override
		// public void run() {
		// MessageDialog.openInformation(shell, "Delete",
		// "Not yet implemented");
		// }
		// };
		// actionDelete.setAccelerator(SWT.CTRL + 'D');
		//


		contentAssistShell.addShellListener(new ShellAdapter() {
			@Override
			public void shellDeactivated(ShellEvent e) {

				if (PlatformUI.getWorkbench().getDisplay().getCursorControl()
						.getShell().getText().indexOf("Description") == -1) {
					((Shell) e.getSource()).close();
					if (shellDesc != null && !shellDesc.isDisposed()) {
						shellDesc.close();
					}
					if (!removeCharFlag)
						deleteInsertingChar();

					removeCharFlag = false;
				}
			}
		});


		// Make the shell movable by using the mouse pointer.
		contentAssistShell.addMouseListener(new MouseListener() {
			public void mouseDoubleClick(MouseEvent e) {
			}

			public void mouseDown(MouseEvent e) {
				originalPosition = new Point(e.x, e.y);
			}

			public void mouseUp(MouseEvent e) {
				originalPosition = null;
			}
		});
		contentAssistShell.addMouseMoveListener(new MouseMoveListener() {
			public void mouseMove(MouseEvent e) {


				if (originalPosition == null)
					return;

				Point point = display.map(contentAssistShell, null, e.x, e.y);
				contentAssistShell.setLocation(point.x - originalPosition.x,
						point.y - originalPosition.y);
				// System.out.println("Moved from: " + originalPosition + " to "
				// + point);
				shellDesc_posx = point.x - originalPosition.x + 200;
				shellDesc_posy = point.y - originalPosition.y;

				shellDesc.setLocation(shellDesc_posx, shellDesc_posy);

				final Cursor cursor = new Cursor(display, SWT.CURSOR_HAND);
				contentAssistShell.setCursor(cursor);

				shellDesc.setCursor(cursor);

			}
		});


		final ToolBar toolBar = new ToolBar(contentAssistShell, SWT.FLAT
				| SWT.RIGHT);

		toolBar.setBounds(30, 180, 100, 30);

		ToolBarManager manager = new ToolBarManager(toolBar);
		manager.add(actionAddNew);
		manager.add(new Separator());
		manager.add(actionRename);
		// manager.add(new Separator());
		// manager.add(actionDelete);
		// manager.add(new Separator());

		manager.update(true);

		contentAssistShell.addListener(SWT.Traverse, new Listener() {
			public void handleEvent(Event event) {
				switch (event.detail) {
				case SWT.TRAVERSE_ESCAPE:
					if (contentAssistShell.isVisible()) {
						contentAssistShell.setVisible(false);
						shellDesc.setVisible(false);

						deleteInsertingChar();

					}
					event.detail = SWT.TRAVERSE_NONE;
					event.doit = false;
					break;
				}
			}
		});

		text = new Text(contentAssistShell, SWT.BORDER | SWT.SINGLE);
		text.setBounds(10, 220, 180, 25);
		text.setEditable(true);
		text.setVisible(true);
		text.setText("Enter new element");

		contentAssistShell.open();


		while (!contentAssistShell.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}
		return result;

	}

	public void setInfoText(String fragment) {
		StringBuilder sb = new StringBuilder();
		sb.append("<html><head><title>");
		sb.append("Content Model");
		sb.append("</title><style type=\"text/css\">");
		sb.append(fgStyleSheet);
		sb.append("</style></head><body>");
		String elt = "<b>" + fragment.substring(0, 7) + "</b>";
		sb.append(fragment.replace(fragment.substring(0, 7), elt));
		sb.append("</body></html>");

		if (!elementDescription.isDisposed())
			elementDescription.setText(sb.toString());

	}

	public MarkupTagInfoProvider infoProvider = new MarkupTagInfoProvider();

	// public Shell shell;

	public void getFirstElementDescription(String data) {
		if (data.equals("")) {
			setInfoText("&lt;!-- --&gt;");
		} else {
			setInfoText(data);
		}
	}

	public void getElementDescription(ISelection selection, String[][] data) {

		for (int i = 0; i < data.length; i++) {
			for (int j = 0; j < data[i].length; j++) {
				if (selection.toString().contains(data[0][j])) {

					if (data[1][j] == "") {

						data[1][j] = "&lt;!-- --&gt;";
					}
					setInfoText(MessageFormat
							.format(
									"{0}\n\n<p class=\"spec\" style=\"font-size: 5pt;\">{1}</p>\n",
									data[1][j]));
					break;
				}

			}
		}

	}

	/*
	 * open the description window
	 */
	public String contentAssistDescription(Display display, final String data) {

		try {
			Thread.sleep(500L);
		} catch (Exception e) {
		}


		if (contentAssistShell == null || contentAssistShell.isDisposed()
				|| !contentAssistShell.isVisible())
			return null;

		shellDesc = new Shell(display, SWT.NO_TRIM | SWT.BORDER_SOLID
				| SWT.LINE_SOLID | SWT.SHADOW_NONE | SWT.ON_TOP);


		// TODO close shellDesc on de-activation
		shellDesc.setLayout(new GridLayout(1, false));
		shellDesc.setText("Description");
		shellDesc.setBounds(10, 10, 200, 260);

		Point absolutePosition = this.vex.getDisplay().getCurrent()
				.getCursorLocation();

		shellDesc.setLocation(shellDesc_posx, shellDesc_posy);

		elementDescription = new Browser(shellDesc, SWT.NONE);
		elementDescription.setSize(200, 260);
		elementDescription.setLayoutData(new GridData(GridData.FILL_BOTH));

		shellDesc.addShellListener(new ShellAdapter() {
			@Override
			public void shellDeactivated(ShellEvent e) {
				if (PlatformUI.getWorkbench().getDisplay().getCursorControl()
						.getShell().getText().indexOf("Available tags") == -1) {
					((Shell) e.getSource()).close();
					if (contentAssistShell != null
							&& !contentAssistShell.isDisposed()) {
						contentAssistShell.close();
					}
					if (!removeCharFlag)
						deleteInsertingChar();

					removeCharFlag = false;
				}

			}
		});

		shellDesc.addListener(SWT.Traverse, new Listener() {
			public void handleEvent(Event event) {
				switch (event.detail) {
				case SWT.TRAVERSE_ESCAPE:
					if (shellDesc.isVisible())
						shellDesc.setVisible(false);

					if (contentAssistShell.isVisible())
						contentAssistShell.setVisible(false);

					event.detail = SWT.TRAVERSE_NONE;
					event.doit = false;
					break;
				}
			}
		});

		getFirstElementDescription(data);



		shellDesc.setVisible(true);

		return result;
	}

	public IRegion getSelectedSourceRegion() {

		IWorkbenchPage page;
		page = PlatformUI.getWorkbench().getActiveWorkbenchWindow()
				.getActivePage();
		IEditorPart part = page.getActiveEditor();
		ITextEditor editor = AdapterUtils.getAdapter(part, ITextEditor.class);
		if (editor != null) {
			ITextSelection selection = (ITextSelection) editor
					.getSelectionProvider().getSelection();
			int length = selection.getLength();
			int offset = selection.getOffset();

			return new Region(offset, length);

		}

		// else if (this.vex != null) {
		//
		// TextSelectionVex vexSelection = new TextSelectionVex(this.vex);
		// int startOffset = vexSelection.getSourceStartOffset();
		// int endOffset = vexSelection.getSourceEndOffset();
		//
		// return new Region(startOffset, endOffset - startOffset);
		// }
		return null;
	}

	public String getSelectedText() {

		String text = "";

		IWorkbenchPage page;
		page = PlatformUI.getWorkbench().getActiveWorkbenchWindow()
				.getActivePage();
		IEditorPart part = page.getActiveEditor();
		ITextEditor editor = AdapterUtils.getAdapter(part, ITextEditor.class);
		if (editor != null) {
			ITextSelection selection = (ITextSelection) editor
					.getSelectionProvider().getSelection();
			text = selection.getText();

		}
		return text;

	}

	/*
	 * content provider for content assistant
	 */
	class DescriptionContentProvider implements IStructuredContentProvider {

		public void dispose() {
		}

		public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		}

		public Object[] getElements(Object arg0) {
			return ((DataElement) arg0).getElements().toArray();
		}
	}

	class DataElement {
		public List data;

		public DataElement(String[][] dataElement) {

			data = new LinkedList();

			for (int i = 0; i < dataElement[0].length; i++) {
				this.data.add(dataElement[0][i]);
			}
		}

		public List getElements() {
			return this.data;
		}
	}


}