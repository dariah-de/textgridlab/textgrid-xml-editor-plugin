/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2004 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.editor;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.IEditorDescriptor;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IViewSite;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.internal.Workbench;
import org.eclipse.ui.part.IPage;
import org.eclipse.ui.part.IPageBookViewPage;
import org.eclipse.ui.part.IPageSite;
import org.eclipse.ui.part.PageBook;
import org.eclipse.ui.part.PageBookView;

/**
 * A view that shows possible elements to insert.
 */
public class InsertView extends PageBookView {

	/*
	 * Usually, a changing editor is communicated to InsertView thru its
	 * IPartListener implementation. When an InsertionView is freshly created
	 * though, it must look for an already active VexEdtior. If there is an
	 * active VexEditor, it might be the case that it is in the process of
	 * loading (this happens on Eclipse startup especially). Then, the
	 * InsertView must provide a VexEditorListener to get notified when the
	 * editor has finished loading.
	 */
	protected OnStartupListener startListener;

	protected IPage createDefaultPage(PageBook book) {
		IPageBookViewPage page = new IPageBookViewPage() {
			public void createControl(Composite parent) {
				this.label = new Label(parent, SWT.NONE);
				this.label.setText(Messages
						.getString("InsertView.noActiveEditor")); //$NON-NLS-1$
			}

			public void dispose() {
				if (startListener != null) {
					startListener.dispose();
				}
			}

			public Control getControl() {
				return this.label;
			}

			public IPageSite getSite() {
				return this.site;
			}

			public void init(IPageSite site) throws PartInitException {
				this.site = site;
			}

			public void setActionBars(IActionBars actionBars) {
			}

			public void setFocus() {
			}

			private IPageSite site;

			private Label label;
		};

		initPage(page);
		page.createControl(getPageBook());
		return page;
	}

	
	protected PageRec doCreatePage(IWorkbenchPart part) {
		InsertViewPage page = new InsertViewPage((VexEditor) part);
		initPage(page);
		page.createControl(getPageBook());
		return new PageRec(part, page);
	}

	protected void doDestroyPage(IWorkbenchPart part, PageRec pageRecord) {
		pageRecord.page.dispose();
	}

	protected IWorkbenchPart getBootstrapPart() {

		return null;
	}

	protected boolean isImportant(IWorkbenchPart part) {
		return (part instanceof VexEditor);
	}

	public void init(IViewSite site) throws PartInitException {
		super.init(site);

		IEditorPart ep;
		ep = Workbench.getInstance().getActiveWorkbenchWindow().getActivePage()
				.getActiveEditor();
		if ((ep != null) && (ep instanceof VexEditor)) {
			// If there is an active Vex editor which is in the process of loading
			if (!((VexEditor) ep).isLoaded()) {
				// Create a listener which will wait for the loading to finish
				// and
				// then cause the view to update
				startListener = new OnStartupListener();
				startListener.vexed = (VexEditor) ep;
				startListener.vexed.addVexEditorListener(startListener);
			}
			
			//Set Focus on the Vex editor so the user can continue editing
			//TODO Doesn't work

			ep.setFocus();

		}
	}

	/**
	 * OnStartupListener is to be instantiated when an InsertView is newly
	 * created. It is to wait for a vex editor to finish loading.
	 */
	private class OnStartupListener implements IVexEditorListener {

		protected VexEditor vexed;

		public void documentLoaded(VexEditorEvent event) {

			// When vex has loaded a Document, call
			// PageBookView.partActivated)() to force a refresh of the
			// InsertView. This is necessary for startup
			partActivated(vexed);

		}

		protected void dispose() {
			vexed.removeVexEditorListener(this);
			startListener = null;
		}

		public void documentUnloaded(VexEditorEvent event) {

		}

	}
}
