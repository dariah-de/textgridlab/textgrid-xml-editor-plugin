package net.sf.vex.editor;

import net.sf.vex.swt.VexWidget;

import org.eclipse.jface.action.MenuManager;
import org.eclipse.ui.IEditorActionBarContributor;

public interface IVexActionBarContributor extends IEditorActionBarContributor {

	public abstract MenuManager getContextMenuManager();

	public abstract VexEditor getVexEditor();

	public abstract VexWidget getVexWidget();

}