/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2004 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.editor;

import org.eclipse.ui.IFolderLayout;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;


/**
 * Implements the Document perspective.
 */
public class DocumentPerspective implements IPerspectiveFactory {
    
    public void createInitialLayout(IPageLayout layout) {
    	defineActions(layout);
    	defineLayout(layout);
    }
    
    /**
     * Defines the initial actions for a page.  
     */
    public void defineActions(IPageLayout layout) {
    	// Add "new wizards".
    	layout.addNewWizardShortcut("net.sf.vex.editor.NewDocumentWizard");//$NON-NLS-1$
    	layout.addNewWizardShortcut("org.eclipse.ui.wizards.new.folder");//$NON-NLS-1$
    	layout.addNewWizardShortcut("org.eclipse.ui.wizards.new.file");//$NON-NLS-1$

    	layout.addShowViewShortcut(IPageLayout.ID_RES_NAV);
    	layout.addShowViewShortcut(IPageLayout.ID_OUTLINE);
    	layout.addShowViewShortcut(IPageLayout.ID_PROP_SHEET);

//    	layout.addActionSet(IPageLayout.ID_NAVIGATE_ACTION_SET);
    }
    
    /**
     * Defines the initial layout for a page.  
     */
    public void defineLayout(IPageLayout layout) {

    	String editorArea = layout.getEditorArea();

    	IFolderLayout topLeft = layout.createFolder("topLeft", IPageLayout.LEFT, (float)0.2, editorArea);//$NON-NLS-1$
    	topLeft.addView(IPageLayout.ID_RES_NAV);
        
        IFolderLayout topRight = layout.createFolder("topRight", IPageLayout.RIGHT, (float)0.75, editorArea);//$NON-NLS-1$
        topRight.addView(IPageLayout.ID_OUTLINE);
        
        IFolderLayout bottomRight = layout.createFolder("bottomRight", IPageLayout.BOTTOM, (float)0.5, "topRight");//$NON-NLS-1$ //$NON-NLS-2$
        bottomRight.addView(IPageLayout.ID_PROP_SHEET);
        bottomRight.addView("info.textgrid.lab.core.metadataeditor.view");
        
        
    }

}
