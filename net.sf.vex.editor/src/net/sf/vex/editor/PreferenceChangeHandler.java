package net.sf.vex.editor;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import net.sf.vex.core.Color;
import net.sf.vex.core.EditorOptions;
import net.sf.vex.core.EditorOptions.InlineMarkerStyle;
import net.sf.vex.layout.AbstractBlockBox;
import net.sf.vex.layout.BlockElementWidget;
import net.sf.vex.swt.VexWidget;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Preferences.IPropertyChangeListener;
import org.eclipse.core.runtime.Preferences.PropertyChangeEvent;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.resource.DataFormatException;
import org.eclipse.jface.resource.StringConverter;
import org.eclipse.swt.graphics.RGB;

public class PreferenceChangeHandler implements IPropertyChangeListener {

	static PreferenceChangeHandler instance = new PreferenceChangeHandler();

//	private List<VexEditor> editors = new ArrayList<VexEditor>();

	private Map<String, Method> handlerFunctions = new HashMap<String, Method>();

	public PreferenceChangeHandler() {

		registerFunction(LayoutPreferencePage.PREFERENCE_SHOW_BLOCK_MARKERS,
				"setShowBlockMarkers");

		registerFunction(LayoutPreferencePage.PREFERENCE_SHOW_INLINE_MARKERS,
				"setShowInlineMarkers");

		registerFunction(LayoutPreferencePage.PREFERENCE_INLINE_MARKER_DISPLAY,
				"setInlineMarkerDisplay");

		registerFunction(LayoutPreferencePage.PREFERENCE_BLOCK_MARKER_COLOR,
				"setBlockMarkerColor");

	}

	public void setAllPreferences() {

		Iterator<String> names = handlerFunctions.keySet().iterator();

		while (names.hasNext()) {

			String name = names.next();

			applyPreference(name, VexPlugin.getInstance()
					.getPluginPreferences().getString(name));
		}
	}

	/**
	 * Applies a preference, but only if an appropriate method is registered
	 */
	private void applyPreference(String preferenceName, Object preferenceValue) {

		if (handlerFunctions.containsKey(preferenceName)) {

			Object[] callParameters = new Object[] { preferenceValue };

			try {
				handlerFunctions.get(preferenceName).invoke(this,
						callParameters);
			} catch (IllegalArgumentException e1) {
				throwImplException(e1);
			} catch (IllegalAccessException e1) {
				throwImplException(e1);
			} catch (InvocationTargetException e1) {
				throwImplException(e1);
			}

			// TODO defer this multiple preference changes will not each trigger
			// a relayout?
			for (VexEditor e : VexPlugin.getInstance().getRunningEditors())
				if (e.getVexWidget() != null)
					e.getVexWidget().reLayout();
		}
	}

	public void propertyChange(PropertyChangeEvent event) {
		String propertyName = event.getProperty();

		applyPreference(event.getProperty(), event.getNewValue());
	}

	/**
	 * Register a function to be called when a certain property changes
	 * 
	 * @param propertyName
	 *            The name of the property
	 * @param functionName
	 *            The name of the java function
	 */
	private void registerFunction(String propertyName, String functionName) {

		try {
			handlerFunctions.put(propertyName, this.getClass().getMethod(
					functionName, new Class[] { Object.class }));

		} catch (Exception e) {
			throwImplException(e);
		}

	}

	public static PreferenceChangeHandler getInstance() {
		return instance;
	}

//	public void addVexEditor(VexEditor editor) {
//		editors.add(editor);
//	}
//
//	public void removeVexEditor(VexEditor editor) {
//		editors.remove(editor);
//	}

	private void throwImplException(Exception e) {
		throw new RuntimeException("Implementation Error. Method misspelled?",
				e);
	}

	// --------- property handler functions -------------

	/**
	 * Show markers for block styled elements?
	 */
	public void setShowBlockMarkers(Object value) {

		boolean newValue = Boolean.valueOf(value.toString());

		Iterator<VexEditor> i = VexPlugin.getInstance().getRunningEditors()
				.iterator();

		while (i.hasNext()) {
			VexWidget widget = i.next().getVexWidget();
			if (widget != null)
				widget.getEditorOptions().setShowBlockMarkers(newValue);
		}

	}

	/**
	 * Show markers for inline styled elements?
	 */
	public void setShowInlineMarkers(Object value) {

		boolean newValue = Boolean.valueOf(value.toString());

		Iterator<VexEditor> i = VexPlugin.getInstance().getRunningEditors()
				.iterator();

		while (i.hasNext()) {
			VexEditor editor = i.next();
			VexWidget widget = editor.getVexWidget();
			if (widget != null) {
				EditorOptions options = widget.getEditorOptions();
				options.setShowInlineMarkers(newValue);
			}

		}


		
		
	}

	/**
	 * Inline marker display
	 */
	public void setInlineMarkerDisplay(Object value) {

		String newValue = value.toString();

		Iterator<VexEditor> i = VexPlugin.getInstance().getRunningEditors()
				.iterator();

		if (newValue
				.equals(LayoutPreferencePage.VALUE_INLINE_MARKER_DISPLAY_LABELED)) {

			while (i.hasNext()) {
				i.next().getVexWidget().getEditorOptions()
						.setInlineMarkerStyle(InlineMarkerStyle.Labeled);
			}
		} else if (newValue
				.equals(LayoutPreferencePage.VALUE_INLINE_MARKER_DISPLAY_UNLABELED)) {

			while (i.hasNext()) {
				i.next().getVexWidget().getEditorOptions()
						.setInlineMarkerStyle(InlineMarkerStyle.Unlabeled);

			}

		}

	}

	/**
	 * Block marker color
	 */
	public void setBlockMarkerColor(Object value) {

		//TODO Implement this on a per-editor-basis (see above)
		// instead of static variables ? (or rather not?)
		
		try {
			RGB rgb = StringConverter.asRGB((String) value);

			Color color = new Color(rgb.red, rgb.green, rgb.blue);

			AbstractBlockBox.setMarkerColor(color);
			BlockElementWidget.setBgColor(color);
			BlockElementWidget.setFgColor(new Color(255, 255, 255));

		} catch (DataFormatException e) {

			Status error = new Status(IStatus.WARNING, VexPlugin.ID,
					(value == null ? "null" : value)
							+ " does not represent a valid RGB value.", e);
			VexPlugin.getInstance().getLog().log(error);
		}

	}

}
