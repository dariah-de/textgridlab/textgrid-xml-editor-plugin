/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2004 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.editor;

import java.lang.reflect.Field;

import net.sf.vex.dom.IVexElement;
import net.sf.vex.swt.VexWidget;
import net.sf.vex.widget.VexWidgetImpl;

import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.ControlListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.internal.Workbench;
import org.eclipse.ui.part.IPageBookViewPage;
import org.eclipse.ui.part.IPageSite;

/**
 * Page in the insert view.
 */
class InsertViewPage implements IPageBookViewPage {

	public InsertViewPage(VexEditor vexEditor) {
		this.vexEditor = vexEditor;
	}

	public void createControl(Composite parent) {

		this.composite = new ScrolledComposite(parent, SWT.V_SCROLL
				| SWT.BORDER);
		this.composite.setLayout(new FillLayout());
		this.composite.setExpandHorizontal(true);
		this.composite.setExpandVertical(true);

		if (this.vexEditor.isLoaded()) {
			this.createInsertPanel();
		} else {
			this.loadingLabel = new Label(this.composite, SWT.NONE);
			this.loadingLabel.setText("Loading...");
		}

		this.vexEditor.getEditorSite().getSelectionProvider()
				.addSelectionChangedListener(this.selectionListener);
	}

	public void dispose() {
		this.vexEditor.getEditorSite().getSelectionProvider()
				.removeSelectionChangedListener(this.selectionListener);
	}

	public Control getControl() {
		return this.composite;
	}

	public IPageSite getSite() {
		return this.site;
	}

	public void init(IPageSite site) throws PartInitException {
		this.site = site;
	}

	public void setActionBars(IActionBars actionBars) {
	}

	public void setFocus() {
	}

	// ================================================== PRIVATE

	private static Field implField;

	private static Field rootBoxField;

	private static Field caretField;

	private static Field hostComponentField;

	static {
		try {
			implField = VexWidget.class.getDeclaredField("impl");
			implField.setAccessible(true);
			rootBoxField = VexWidgetImpl.class.getDeclaredField("rootBox");
			rootBoxField.setAccessible(true);
			caretField = VexWidgetImpl.class.getDeclaredField("caret");
			caretField.setAccessible(true);
			hostComponentField = VexWidgetImpl.class
					.getDeclaredField("hostComponent");
			hostComponentField.setAccessible(true);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	private IPageSite site;

	private VexEditor vexEditor;

	private VexWidget vexWidget;

	private VexWidgetImpl impl;

	private ScrolledComposite composite;

	private Composite elementList;

	private Label loadingLabel;

	private IVexElement currentElement;

	private class InsertSelectionListener implements SelectionListener {

		protected IAction action;

		public InsertSelectionListener(IAction a) {

			this.action = a;

		}

		public void widgetDefaultSelected(SelectionEvent e) {
			// this.action.run();
		}

		public void widgetSelected(SelectionEvent e) {
			this.action.run();
			Workbench.getInstance().getActiveWorkbenchWindow().getActivePage()
					.getActiveEditor().setFocus();

		}

	}

	private void createInsertPanel() {

		if (this.loadingLabel != null) {
			this.loadingLabel.dispose();
			this.loadingLabel = null;
		}

		this.vexWidget = this.vexEditor.getVexWidget();
		try {
			this.impl = (VexWidgetImpl) implField.get(this.vexWidget);
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Tests if the caret was moved int a different element
		// TODO Replace this by listening to an IPostSelectionProvider
		// which would need to be implemented.
		// Only a temporary solution. May not be correct esp. with more advanced
		// schema languages.

		if (this.currentElement == null
				|| this.vexWidget.getCurrentElement() != this.currentElement) {

			this.currentElement = this.vexWidget.getCurrentElement();

			if (elementList != null) {
				elementList.dispose();
			}

			elementList = new Composite(composite, SWT.NONE);

			RowLayout rl = new RowLayout(SWT.HORIZONTAL);
			rl.wrap = true;

			//rl.justify = true;
			//rl.fill = true;
			
			elementList.setLayout(rl);

			IAction[] validActions = this.vexWidget.getValidInsertActions();

			for (IAction a : validActions) {

				Button elementButton = new Button(elementList, SWT.PUSH);
				elementButton.setText(a.getText());

				elementButton.addSelectionListener(new InsertSelectionListener(
						a));

			}


			
			composite.setContent(elementList);

			   composite.addControlListener(new ControlAdapter() {
				      public void controlResized(ControlEvent e) {
				        Rectangle r = composite.getClientArea();
				        composite.setMinSize(elementList.computeSize(r.width,
				            SWT.DEFAULT));
				        elementList.layout();
				      }
				    });
			
		}

	}

	private ISelectionChangedListener selectionListener = new ISelectionChangedListener() {
		public void selectionChanged(SelectionChangedEvent event) {

			createInsertPanel();

		}
	};

}