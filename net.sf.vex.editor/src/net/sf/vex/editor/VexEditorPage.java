package net.sf.vex.editor;




import info.textgrid.lab.core.model.TextGridObject;
import info.textgrid.lab.core.swtutils.AdapterUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.sf.vex.action.DeleteColumnAction;
import net.sf.vex.action.DeleteRowAction;
import net.sf.vex.action.DuplicateSelectionAction;
import net.sf.vex.action.InsertColumnAfterAction;
import net.sf.vex.action.InsertColumnBeforeAction;
import net.sf.vex.action.InsertRowAboveAction;
import net.sf.vex.action.InsertRowBelowAction;
import net.sf.vex.action.MoveColumnLeftAction;
import net.sf.vex.action.MoveColumnRightAction;
import net.sf.vex.action.MoveRowDownAction;
import net.sf.vex.action.MoveRowUpAction;
import net.sf.vex.action.NextTableCellAction;
import net.sf.vex.action.PasteTextAction;
import net.sf.vex.action.PreviousTableCellAction;
import net.sf.vex.action.RemoveElementAction;
import net.sf.vex.action.RestoreLastSelectionAction;
import net.sf.vex.action.SplitAction;
import net.sf.vex.action.SplitItemAction;
import net.sf.vex.action.linked.ChangeElementAction;
import net.sf.vex.action.linked.InsertElementAction;
import net.sf.vex.css.StyleSheet;
import net.sf.vex.css.StyleSheetReader;
import net.sf.vex.dom.IVexDocument;
import net.sf.vex.dom.IVexElement;
import net.sf.vex.dom.IVexNode;
import net.sf.vex.dom.IWhitespacePolicy;
import net.sf.vex.dom.IWhitespacePolicyFactory;
import net.sf.vex.dom.linked.LinkedDocument;
import net.sf.vex.dom.linked.LinkedDocumentReader;
import net.sf.vex.dom.linked.LinkedNode;
import net.sf.vex.editor.action.VexActionAdapter;
import net.sf.vex.editor.config.DocumentType;
import net.sf.vex.editor.config.Style;
import net.sf.vex.layout.VexAnnotationTracker;
import net.sf.vex.swt.VexWidget;
import net.sf.vex.widget.CssWhitespacePolicy;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.text.DocumentEvent;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IDocumentListener;
import org.eclipse.jface.text.IRegion;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.IKeyBindingService;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.contexts.IContextService;
import org.eclipse.ui.part.EditorPart;
import org.eclipse.ui.progress.UIJob;
import org.eclipse.ui.statushandlers.StatusManager;
import org.eclipse.wst.sse.core.StructuredModelManager;
import org.eclipse.wst.sse.core.internal.provisional.IModelManager;
import org.eclipse.wst.sse.core.internal.provisional.IStructuredModel;
import org.eclipse.wst.sse.core.internal.provisional.text.IStructuredDocument;
import org.eclipse.wst.sse.ui.StructuredTextEditor;
import org.eclipse.wst.xml.core.internal.provisional.document.IDOMDocument;
import org.eclipse.wst.xml.core.internal.provisional.document.IDOMModel;
import org.eclipse.wst.xml.core.internal.provisional.document.IDOMNode;
import org.eclipse.wst.xml.ui.internal.XMLUIPlugin;
import org.w3c.css.sac.CSSException;
import org.w3c.css.sac.InputSource;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.ProcessingInstruction;

/**
 * 
 * This is a version of the {@link VexEditor} that can be embedded in TextGrid's
 * multi-page XML editor. It parses its document model from an {@link IDocument}
 * and uses WST's data model, where possible.
 * 
 * @author Thorsten Vitt <vitt@linglit.tu-darmstadt.de> (tv) for TextGrid
 * 
 */
@SuppressWarnings("restriction")
// well, there is no interface but a restricted one ...
public class VexEditorPage extends VexEditor {

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.sf.vex.editor.VexEditor#getDoc()
	 */
	@Override
	public LinkedDocument getDoc() {
		return (LinkedDocument) super.getDoc();
	}

	protected static final String DEFAULT_DOCTYPE_ID = "-//TEI//DTD TEI Lite XML ver. 1//EN";
	private static final int WARNING_BYTES = 1024 * 1024;
	/**
	 * The source code editor, if {@link #isEmbedded()}
	 */
	private StructuredTextEditor sourceEditor;
	/**
	 * The source {@linkplain IDocument document}, if {@link #isEmbedded()}.
	 */
	private IDocument document;
	private IDOMModel domModel;

	/**
	 * For documents that may perform bad
	 */
	private enum WarningState {
		/**
		 * Don't know whether a warning is neccessary
		 */
		NOT_CHECKED,
		/**
		 * Warned, but our user hasn't clicked the 'Yes, I want' button
		 */
		NOT_CONFIRMED,
		/**
		 * We may load the file, either no warning neccessary or the user has
		 * clicked the "proceed" button
		 */
		MAY_LOAD
	}

	public static class StylesheetInfo {
		public StylesheetInfo(final ProcessingInstruction stylesheetNode, final URI stylesheetURI) {
			super();
			this.stylesheetNode = stylesheetNode;
			this.stylesheetURI = stylesheetURI;
		}
	
		public final ProcessingInstruction stylesheetNode;
		public final URI stylesheetURI;
	}

	private WarningState warningState = WarningState.NOT_CHECKED;
	private VexLoadWarning loadingLabel;

	@Override
	protected void showLabel(final String message) {
		VexLoadWarning warning = getLoadWarning();
		warning.setLoadingText(message);
		// parentControl.layout(true);
	}

	private VexLoadWarning getLoadWarning() {
		if (loadingLabel == null) {
			if (getVexWidget() != null) {
				getVexWidget().dispose();
				setVexWidget(null);
			}
			loadingLabel = new VexLoadWarning(getParentControl(), SWT.NONE);
			loadingLabel.addPaintListener(new PaintListener() {

				public void paintControl(final PaintEvent e) {
					if (loadingLabel.isVisible() && getState() == State.UNLOADED) {
						initAndLoad();
					}
				}
			});

			loadingLabel.addProceedListener(new SelectionAdapter() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					warningState = WarningState.MAY_LOAD;
					initAndLoad();
				}

			});
		}

		if (document != null)
			document.addDocumentListener(new IDocumentListener() {

				public void documentChanged(DocumentEvent event) {
					if (loadingLabel.isVisible()) {
						warningState = WarningState.NOT_CHECKED;
						initAndLoad();
					}
					document.removeDocumentListener(this);
				}

				public void documentAboutToBeChanged(DocumentEvent event) {
				}
			});

		return loadingLabel;
	}

	@Override
	protected void clearLoadingLabel() {
		if (loadingLabel != null) {
			loadingLabel.dispose();
			loadingLabel = null;
		}
	}

	private IWhitespacePolicyFactory wsFactory = new IWhitespacePolicyFactory() {
		public IWhitespacePolicy getPolicy(String publicId) {

			if (getDoctype() == null) {
				/*
				 * DocumentTypeSelectionDialog dlg = DocumentTypeSelectionDialog
				 * .create(getSite().getShell(), publicId); dlg.open();
				 */
				setDoctype(VexEditorPage.this.getDefaultDoctype());
				setUpdateDoctypeDecl(false);

				if (getDoctype() == null) {
					throw new NoRegisteredDoctypeException(null);
				}
			}

			setStyle(VexEditor.findStyleForDoctype(getDoctype().getPublicId()));
			if (getStyle() == null) {
				throw new NoStyleForDoctypeException(getDoctype());
			}

			return new CssWhitespacePolicy(style.getStyleSheet());
		}

	};

	/**
	 * @return the wsFactory
	 */
	@Override
	public IWhitespacePolicyFactory getWsFactory() {
		return wsFactory;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.sf.vex.editor.VexEditor#doSave(org.eclipse.core.runtime.IProgressMonitor )
	 */
	@Override
	public void doSave(IProgressMonitor monitor) {
		// _we_ dont't save, that's the source editor's task.
		if (monitor != null) {
			monitor.done();
		}
	}

	/**
	 * We're just a stupid slave now
	 */
	@Override
	public boolean isDirty() {
		return getSourceEditor().isDirty();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.sf.vex.editor.VexEditor#init(org.eclipse.ui.IEditorSite,
	 *      org.eclipse.ui.IEditorInput)
	 */
	@Override
	public void init(IEditorSite site, IEditorInput input) throws PartInitException {

		this.setSite(site);
		this.setInput(input);

		this.getEditorSite().setSelectionProvider(this.getSelectionProvider());
		this.getEditorSite().getSelectionProvider().addSelectionChangedListener(getSelectionChangedListener());

		// if (input instanceof IFileEditorInput) {
		// ResourcesPlugin.getWorkspace().addResourceChangeListener(this.
		// resourceChangeListener, IResourceChangeEvent.POST_CHANGE);
		// }

		// PreferenceChangeHandler.getInstance().addVexEditor(this);
		VexPlugin.getInstance().registerEditor(this);

	}

	protected DocumentType getDefaultDoctype() {
		DocumentType documentType = DocumentType.getDocumentType(DEFAULT_DOCTYPE_ID);
		return documentType;
	}

	/**
	 * Class constructor for Vex editors that are part of a multi-page editor.
	 * 
	 * @param sourceEditor
	 *            the editor for the source code
	 * @param document
	 *            the corresponding document
	 */
	public VexEditorPage(StructuredTextEditor sourceEditor, IDocument document) {
		super();
		this.sourceEditor = sourceEditor;
		this.document = document;

	}

	/**
	 * @return the preference store of the WST XML UI plugin
	 */
	protected IPreferenceStore getWSTPreferenceStore() {
		return XMLUIPlugin.getDefault().getPreferenceStore();
	}

	/**
	 * @return the source editor for the current XML document.
	 */
	public StructuredTextEditor getSourceEditor() {
		return sourceEditor;
	}

	/**
	 * @return the {@link IDocument} that is edited by the
	 *         {@linkplain #getSourceEditor() source editor}.
	 */
	public IDocument getDocument() {
		return document;
	}

	@Override
	protected void loadInput() {

		
		if (getVexWidget() != null) {
			this.getVexEditorListeners().fireEvent(
					"documentUnloaded", new VexEditorEvent(this)); //$NON-NLS-1$
		}

		this.setLoaded(false);

		if (warningState == WarningState.NOT_CHECKED) {
			performDocumentChecks();
		}

		if (warningState == WarningState.MAY_LOAD) {
		Job loadJob = new Job("Loading WYSIWYM view") {

			@Override
			public IStatus run(final IProgressMonitor monitor) {
				// System.out.println(">>> Starting to load the WYSIWYM model");

				final LinkedDocument vexDocument = LinkedDocumentReader
						.createVexDocument(getDocument(), getWsFactory(),
								monitor);
				setDoc(vexDocument);
				final StyleSheet styleSheet;
					StyleSheet suppliedStyleSheet = null;
				if (vexDocument.getCSSfileLocation() != null) {
						suppliedStyleSheet = loadStyleSheet(vexDocument.getCSSfileLocation());
					}
					;
					if (suppliedStyleSheet != null) {
						styleSheet = suppliedStyleSheet;
				} else if (getStyle() != null)
					styleSheet = getStyle().getStyleSheet();
				else
					styleSheet = null;

				if (styleSheet != null) {
					new Job("Preloading stylesheet ...") {

						private SubMonitor progress;
						private int lastProgress = 0;

						@Override
						protected IStatus run(IProgressMonitor monitor) {
							long start = System.currentTimeMillis();
							System.err.println("Starting preload ...");
							progress = SubMonitor.convert(monitor, getDoc().getLength());

							preload(getDoc().getRootElement());

							System.err.println("Preload took " + (System.currentTimeMillis() - start) + "ms.");
							return Status.OK_STATUS;
						}

						private void preload(IVexElement element) {
							int startOffset = element.getStartOffset();
							progress.worked(startOffset - lastProgress);
							lastProgress = startOffset;

							styleSheet.getStyles(element);
							for (IVexElement child : element.getChildElements()) {
								preload(child);
							}
						}

					}.schedule(Job.SHORT);
				}

				UIJob configureJob = new UIJob("Configuring WYSIWYM view") {

					@Override
					public IStatus runInUIThread(IProgressMonitor monitor) {

						// System.out.println(">>> Starting to configure the WYSIWYM view");

						showVexWidget();
						getVexWidget().setDebugging(isDebugging());
						getVexWidget().setAnnotationTracker(
								new VexAnnotationTracker(getEditorInput(), getSourceEditor().getDocumentProvider()));
						getVexWidget().setDocument(vexDocument, styleSheet);

						if (isUpdateDoctypeDecl()) {
							getDoc().setPublicID(getDoctype().getPublicId());
							getDoc().setSystemID(getDoctype().getSystemId());
							doSave(null);
						}


						setLoaded(true);
						setSavedUndoDepth(getVexWidget().getUndoDepth());
						firePropertyChange(EditorPart.PROP_DIRTY);
						setWasDirty(isDirty());

						getVexEditorListeners()
								.fireEvent(
								"documentLoaded", new VexEditorEvent(VexEditorPage.this)); //$NON-NLS-1$
						return Status.OK_STATUS;
					}
				};
				configureJob.schedule(Job.LONG);
				return Status.OK_STATUS;
			}
		};
		loadJob.setUser(true);
		loadJob.schedule();
		} else {
			// TODO
		}
	}

	private void performDocumentChecks() {
		// check size
		IDocument document = getDocument();
		int length = document.getLength();
		if (length > WARNING_BYTES) {
			warningState = WarningState.NOT_CONFIRMED;
			getLoadWarning().showSizeWarning(WARNING_BYTES);
		}
		
		final IModelManager modelManager = StructuredModelManager.getModelManager();
		final IDOMModel domModel;

		if (document instanceof IStructuredDocument) {
		
			final IStructuredDocument sdocument = (IStructuredDocument) document;
			final IStructuredModel modelForRead = modelManager.getModelForRead(sdocument);
			try {
				if (modelForRead instanceof IDOMModel) {
					domModel = (IDOMModel) modelForRead;
					IDOMDocument dom = domModel.getDocument();
					if (dom != null) {
						Element documentElement = dom.getDocumentElement();
						if (documentElement == null) {
							getLoadWarning().showRootElementError();
							warningState = WarningState.NOT_CHECKED;
						} else {
							IVexNode adapter = AdapterUtils.getAdapter(documentElement, IVexNode.class);
							if (adapter != null) {
								getLoadWarning().showDuplicateError();
								warningState = WarningState.NOT_CHECKED;
							} else if ("TEI".equals(documentElement.getLocalName())
									|| "teiCorpus".equals(documentElement.getLocalName())
									|| "TEI.2".equals(documentElement.getLocalName())) {
								warningState = WarningState.MAY_LOAD;
							} else {
								StylesheetInfo stylesheetInfo = findActiveStylesheet(domModel);
								if (stylesheetInfo != null)
									warningState = WarningState.MAY_LOAD;
								else {
									warningState = WarningState.NOT_CONFIRMED;
									getLoadWarning().showCSSWarning();
								}
							}
						}
					}
				}
			} catch (URISyntaxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				if (modelForRead != null)
					modelForRead.releaseFromRead();
			}
		}
		if (warningState != WarningState.MAY_LOAD)
			setState(State.UNLOADED);
	}

	/**
	 * loads and returns the specified stylesheet, if possible.
	 * 
	 * @param cssfile
	 *            a string describing the stylesheet: {@link URL}, TextGrid-URI
	 *            or path relative to the document location
	 * @return the {@link StyleSheet} or <code>null</code> if something went
	 *         wrong
	 */
	private StyleSheet loadStyleSheet(String cssfile) {
		StyleSheet styleSheet = null;
		if (cssfile != null) {
			try {
				URI cssURI = new URI(cssfile.trim());
				InputStream cssInputStream = null;
				URL cssURL = null;
				if ("textgrid".equals(cssURI.getScheme())) {
					TextGridObject cssObject = TextGridObject.getInstanceOffline(cssURI);
					IFile cssFile = AdapterUtils.getAdapter(cssObject, IFile.class);
					if (cssFile != null) {
						cssInputStream = cssFile.getContents();
					}
				} else {
					URL context = null;
					IFile file = AdapterUtils.getAdapter(getEditorInput(), IFile.class);
					if (file != null) {
						URI fileURI = file.getLocationURI();
						if (fileURI != null)
							context = fileURI.toURL();
					}
					cssURL = new URL(context, cssfile);
					cssInputStream = cssURL.openStream();
				}
				if (cssInputStream != null) {
					InputSource source = new InputSource(new InputStreamReader(cssInputStream, "UTF-8"));
					styleSheet = new StyleSheetReader().read(source, null);
					cssInputStream.close();
				}
			} catch (URISyntaxException e) {
				StatusManager.getManager().handle(
						new Status(IStatus.ERROR, VexPlugin.ID, NLS.bind("Specified stylesheet string, {0}, is not a valid URI.",
								cssfile), e));
			} catch (CoreException e) {
				StatusManager.getManager().handle(e, VexPlugin.ID);
			} catch (UnsupportedEncodingException e) {
				// should not happen -- UTF-8
				StatusManager.getManager().handle(new Status(IStatus.ERROR, VexPlugin.ID, e.getLocalizedMessage(), e));
			} catch (CSSException e) {
				StatusManager.getManager().handle(
						new Status(IStatus.ERROR, VexPlugin.ID, NLS.bind("Error parsing specified stylesheet {0}: {1}", cssfile,
								e.getLocalizedMessage()), e));

			} catch (IOException e) {
				StatusManager.getManager().handle(
						new Status(IStatus.ERROR, VexPlugin.ID, NLS.bind("Error reading specified stylesheet {0}: {1}", cssfile,
								e.getLocalizedMessage()), e));
			}
		}
		return styleSheet;
	}

	/**
	 * reload the vex document, e.g. to re-synchronize with the source editor.
	 * 
	 * reloadInput will first create a new {@link LinkedDocument} based on this
	 * editor's {@linkplain #getDocument() source document} which it will then
	 * compare with the current {@linkplain #getDoc() Vex document}. If these
	 * differ, an error will be logged and Vex' document will be
	 * {@linkplain #reloadInput(LinkedDocument) replaced} with the new document.
	 * 
	 * @param force
	 *            if <code>true</code>, replace the current
	 *            {@link LinkedDocument} with the new way in any case, even if
	 *            both documents are equal.
	 */
	public void reloadInput(boolean force) {
		getStyle().getStyleSheet().clearDocumentCache();
		final LinkedDocument newVexDocument = LinkedDocumentReader.createVexDocument(getDocument(), getWsFactory(), null);
		final IVexDocument oldVexDocument = getDoc();
		IStatus comparison = LinkedDocument.deepCompare((LinkedNode) oldVexDocument.getRootElement(),
				(LinkedNode) newVexDocument.getRootElement());
		if (!comparison.isOK())
			StatusManager.getManager().handle(comparison);

		if (force || !comparison.isOK())
			reloadInput(newVexDocument);
		else
			newVexDocument.dispose();
	}

	protected void reloadInput(final LinkedDocument newVexDocument) {
		final IVexDocument oldVexDocument = getDoc();

		if (getVexWidget() != null) {
			getVexEditorListeners().fireEvent("documentUnloaded", new VexEditorEvent(this)); //$NON-NLS-1$
			int selectionStart = getVexWidget().getSelectionStart();
			int selectionEnd = getVexWidget().getSelectionEnd();
			setLoaded(false);
			setDoc(newVexDocument);
			if (oldVexDocument instanceof LinkedDocument) {
				LinkedDocument oldLinkedDocument = (LinkedDocument) oldVexDocument;
				oldLinkedDocument.dispose();
			}
			VexWidget widget = getVexWidget();
			widget.setDocument(getDoc(), getStyle().getStyleSheet());
			setLoaded(true);
			getVexWidget().moveTo(selectionStart);

			if (selectionEnd > 0 && selectionEnd != selectionStart)
				getVexWidget().moveBy(selectionEnd - selectionStart, true);
		}
	}

	// FIXME this shouldn't be an UIJob (but the reader somewhere does UI
	// stuff), and it should be removed when the editor is closed. So we don't
	// enable this yet.
	protected final Job checkModelJob = new UIJob("Checking WYSIWYM model consistency") {
		
		@Override
		public IStatus runInUIThread(IProgressMonitor monitor) {
			final SubMonitor progress = SubMonitor.convert(monitor, "Checking WYSIWYM model consistency", 100);
			if (progress.isCanceled())
				return Status.CANCEL_STATUS;

			final LinkedDocument newVexDocument = LinkedDocumentReader
					.createVexDocument(getDocument(), getWsFactory(),
							progress.newChild(50));
			final LinkedDocument oldVexDocument = getDoc();
			final IStatus comparison = LinkedDocument.deepCompare((LinkedNode) oldVexDocument.getRootElement(),
					(LinkedNode) newVexDocument.getRootElement());
			if (comparison.isOK())
				newVexDocument.dispose();
			else {
				new UIJob("Reloading WYSIWYM document") {

					@Override
					public IStatus runInUIThread(final IProgressMonitor monitor) {
						if (getVexWidget() == null || getVexWidget().isDisposed())
							return Status.CANCEL_STATUS;
						reloadInput(newVexDocument);
						return Status.OK_STATUS;
					}
				}.schedule();
			}

			this.schedule(5000);
			return Status.OK_STATUS;
		}
	};
	

	/**
	 * Notifies this page that it has been activated.
	 */
	public void notifyActivate() {
		VexWidget widget = getVexWidget();
		if (widget != null) {
			widget.reLayout();
		}
		IContextService contextService = (IContextService) getSite().getWorkbenchWindow().getWorkbench().getService(
				IContextService.class);
		contextService.activateContext("net.sf.vex.editor.VexEditorContext");
	}

	public void selectSourceRegion(IRegion selection) {
		if (getVexWidget() == null || selection.getLength() == 0)
			return; // not initialized yet FIXME: Maybe set selection later?
		getVexWidget().selectSourceRegion(selection);
	}

	@Override
	protected void registerBindings() {
		IKeyBindingService kbs = this.getSite().getKeyBindingService();
		kbs.setScopes(new String[] { "net.sf.vex.editor.VexEditorContext" }); //$NON-NLS-1$
		kbs.registerAction(new VexActionAdapter(this, new ChangeElementAction()));
		kbs.registerAction(new VexActionAdapter(this, new DeleteColumnAction()));
		kbs.registerAction(new VexActionAdapter(this, new DeleteRowAction()));
		kbs.registerAction(new VexActionAdapter(this, new DuplicateSelectionAction()));
		kbs.registerAction(new VexActionAdapter(this, new InsertColumnAfterAction()));
		kbs.registerAction(new VexActionAdapter(this, new InsertColumnBeforeAction()));
		kbs.registerAction(new VexActionAdapter(this, new InsertElementAction()));
		kbs.registerAction(new VexActionAdapter(this, new InsertRowAboveAction()));
		kbs.registerAction(new VexActionAdapter(this, new InsertRowBelowAction()));
		kbs.registerAction(new VexActionAdapter(this, new MoveColumnLeftAction()));
		kbs.registerAction(new VexActionAdapter(this, new MoveColumnRightAction()));
		kbs.registerAction(new VexActionAdapter(this, new MoveRowDownAction()));
		kbs.registerAction(new VexActionAdapter(this, new MoveRowUpAction()));
		kbs.registerAction(new VexActionAdapter(this, new NextTableCellAction()));
		kbs.registerAction(new VexActionAdapter(this, new PasteTextAction()));
		kbs.registerAction(new VexActionAdapter(this, new PreviousTableCellAction()));
		kbs.registerAction(new VexActionAdapter(this, new RemoveElementAction()));
		kbs.registerAction(new VexActionAdapter(this, new RestoreLastSelectionAction()));
		kbs.registerAction(new VexActionAdapter(this, new SplitAction()));
		kbs.registerAction(new VexActionAdapter(this, new SplitItemAction()));
	}

	public void selectDOMNodes(IDOMNode firstNode, IDOMNode lastNode) {
		if (getVexWidget() != null)
			getVexWidget().selectDOMNodes(firstNode, lastNode);

	}

	public void gotoDOMNode(IDOMNode domNode) {
		if (getVexWidget() != null)
			getVexWidget().gotoDOMNode(domNode);
	}

	/**
	 * Sets the stylesheet that is used by this page to the one given by the argument
	 * @param latestURI The stylesheet URI (may be a TextGrid URI), or <code>null</code> to load the default stylesheet
	 */
	public void setStylesheet(String latestURI) {
		if (getVexWidget() != null) {
			StyleSheet styleSheet;
			if (latestURI == null) {
				Style findStyleForDoctype = findStyleForDoctype(getDocumentType().getPublicId());
				styleSheet = findStyleForDoctype.getStyleSheet();
			} else {
				styleSheet = loadStyleSheet(latestURI);
			}
			getVexWidget().setStyleSheet(styleSheet);
		}
	}

	private static Map<String, String> getPseudoAttributes(final String data) {
		final Matcher matcher = Pattern.compile("[a-zA-Z-]+=\"[^\"]*\"").matcher(data);
		final Map<String, String> map = new HashMap<String, String>();
		while (matcher.find()) {
			final String av = matcher.group();
			final String key = av.substring(0, av.indexOf('='));
			final String value = av.substring(av.indexOf('"') + 1, av.lastIndexOf('"')).trim();
			map.put(key, value);
		}
		return map;
	}

	public static StylesheetInfo findActiveStylesheet(final IDOMModel model) throws URISyntaxException {
		final Document domDocument = model.getDocument();
	
		final NodeList childNodes = domDocument.getChildNodes();
		for (int i = 0; i < childNodes.getLength(); i++) {
			final Node node = childNodes.item(i);
			if (node.getNodeType() == Node.PROCESSING_INSTRUCTION_NODE) {
				final ProcessingInstruction pi = (ProcessingInstruction) node;
				if (pi.getTarget().equals("xml-stylesheet")) {
					final Map<String, String> attributes = getPseudoAttributes(pi.getData());
					if (attributes.containsKey("type") && attributes.get("type").equals("text/css")
							&& attributes.containsKey("href")) {
						ProcessingInstruction stylesheetNode = pi;
						URI stylesheetURI = new URI(attributes.get("href"));
						return new StylesheetInfo(stylesheetNode, stylesheetURI);
					}
				}
			}
		}
		return null;
	}

}
