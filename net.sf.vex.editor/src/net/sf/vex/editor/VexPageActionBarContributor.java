/**
 * 
 */
package net.sf.vex.editor;

import net.sf.vex.action.ActionUtils;
import net.sf.vex.action.DeleteColumnAction;
import net.sf.vex.action.DeleteRowAction;
import net.sf.vex.action.InsertColumnAfterAction;
import net.sf.vex.action.InsertColumnBeforeAction;
import net.sf.vex.action.InsertRowAboveAction;
import net.sf.vex.action.InsertRowBelowAction;
import net.sf.vex.action.MoveColumnLeftAction;
import net.sf.vex.action.MoveColumnRightAction;
import net.sf.vex.action.MoveRowDownAction;
import net.sf.vex.action.MoveRowUpAction;
import net.sf.vex.action.linked.InsertElementAction;
import net.sf.vex.dom.DocumentValidationException;
import net.sf.vex.editor.action.VexActionAdapter;
import net.sf.vex.editor.config.Style;
import net.sf.vex.swt.VexWidget;
import net.sf.vex.widget.IVexWidget;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.GroupMarker;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.actions.ActionFactory;
import org.eclipse.ui.part.MultiPageEditorActionBarContributor;

/**
 * This is an action bar contributor for a VexPage that is embedded with the WST
 * XML Editor and uses a LinkedModel.
 * 
 * 
 * In the current version, this class contains a lot of legacy code copied from
 * {@link VexActionBarContributor} which should be replaced with proper Eclipse
 * stuff.
 * 
 * @author Thorsten Vitt <vitt@linglit.tu-darmstadt.de> for TextGrid
 *         (http://www.textgrid.info/) (tv)
 */
public class VexPageActionBarContributor extends
		MultiPageEditorActionBarContributor implements IVexActionBarContributor {

	@Override
	public void dispose() {
	}

	public MenuManager getContextMenuManager() {
		return this.contextMenuManager;
	}

	public VexEditor getVexEditor() {
		return activeEditor == null ? null : (VexEditor) activeEditor
				.getAdapter(VexEditor.class);
	}

	public VexWidget getVexWidget() {
		if (activeEditor instanceof VexEditorPage)
			return ((VexEditorPage) activeEditor).getVexWidget();
		else if (activeEditor != null) {
			return ((VexEditor) (activeEditor).getAdapter(VexEditor.class))
					.getVexWidget();
		}
		return null;
	}

	@Override
	public void init(IActionBars bars, IWorkbenchPage page) {
		super.init(bars, page);

		this.contextMenuManager = new MenuManager();
		this.contextMenuManager.setRemoveAllWhenShown(true);
		this.contextMenuManager.addMenuListener(this.contextMenuListener);

		page.addSelectionListener(this.selectionListener);

		this.globalCopyAction = ActionFactory.COPY.create(page
				.getWorkbenchWindow());
		this.globalCutAction = ActionFactory.CUT.create(page
				.getWorkbenchWindow());
		this.globalDeleteAction = ActionFactory.DELETE.create(page
				.getWorkbenchWindow());
		this.globalPasteAction = ActionFactory.PASTE.create(page
				.getWorkbenchWindow());
		this.globalRedoAction = ActionFactory.REDO.create(page
				.getWorkbenchWindow());
		this.globalUndoAction = ActionFactory.UNDO.create(page
				.getWorkbenchWindow());
	}

	// @Override
	// public void setActiveEditor(IEditorPart activeEditor) {
	//
	// // This can occur if we have an error loading the editor,
	// // in which case Eclipse provides its own part
	// /*
	// * if (!(activeEditor instanceof VexEditor)) { return; }
	// */
	//
	// if (activeEditor.getAdapter(VexEditor.class) == null)
	// return;
	//
	// // TODO introdoce page specific enable / disable methods that can be
	// // called from the MultiPartEditor's setActivePage
	//
	// this.activeEditor = activeEditor;
	//
	// setGlobalActionHandlers();
	// this.enableActions();
	// }

	@Override
	public void setActivePage(IEditorPart activeEditor) {
		if (activeEditor instanceof VexEditorPage) {
			this.activeEditor = activeEditor;
			setGlobalActionHandlers();
			enableActions();
		}
	}
	
	@Override
	public void setActiveEditor(IEditorPart activeEditor) {
		if (activeEditor instanceof VexEditorPage)
			setActivePage(activeEditor);
	}
	
	public void setViewerSpecificContributionsEnabled(boolean enabled) {
		setGlobalActionHandlers();
		enableActions();

	}
	

	private void setGlobalActionHandlers() {
		this.getActionBars().setGlobalActionHandler(ActionFactory.COPY.getId(),
				this.copyAction);

		this.getActionBars().setGlobalActionHandler(ActionFactory.CUT.getId(),
				this.cutAction);

		this.getActionBars().setGlobalActionHandler(
				ActionFactory.DELETE.getId(), this.deleteAction);

		this.getActionBars().setGlobalActionHandler(
				ActionFactory.PASTE.getId(), this.pasteAction);

		this.getActionBars().setGlobalActionHandler(ActionFactory.REDO.getId(),
				this.redoAction);

		this.getActionBars().setGlobalActionHandler(
				ActionFactory.SELECT_ALL.getId(), this.selectAllAction);

		this.getActionBars().setGlobalActionHandler(ActionFactory.UNDO.getId(),
				this.undoAction);
		this.getActionBars().updateActionBars();
	}

	// ===================================================== PRIVATE

	private IEditorPart activeEditor;

	private IAction globalCopyAction;
	private IAction globalCutAction;
	private IAction globalDeleteAction;
	private IAction globalPasteAction;
	private IAction globalRedoAction;
	private IAction globalUndoAction;

	private IAction copyAction = new CopyAction();
	private IAction cutAction = new CutAction();
	private IAction deleteAction = new DeleteAction();
	private IAction pasteAction = new PasteAction();
	private IAction redoAction = new RedoAction();
	private IAction selectAllAction = new SelectAllAction();
	private IAction undoAction = new UndoAction();

	private MenuManager contextMenuManager;
	private IMenuListener contextMenuListener = new IMenuListener() {

		public void menuAboutToShow(IMenuManager manager) {

			boolean showTableActions = false;
			IVexWidget vexWidget = getVexWidget();
			if (vexWidget != null) {
				showTableActions = ActionUtils.getSelectedTableRows(vexWidget)
						.getRows() != null;
			}

			manager.add(globalUndoAction);
			manager.add(globalRedoAction);
			manager.add(new Separator());
			manager.add(new VexActionAdapter(getVexEditor(),
					new InsertElementAction()));

			if (showTableActions) {
				manager.add(new Separator());
				manager.add(new RowMenuManager());
				manager.add(new ColumnMenuManager());
			}

			manager.add(new Separator());
			manager.add(globalCutAction);
			manager.add(globalCopyAction);
			manager.add(globalPasteAction);
			manager.add(new Separator());
			manager.add(globalDeleteAction);
			manager.add(new Separator());
			manager.add(new StyleMenuManager());
			manager
					.add(new GroupMarker(IWorkbenchActionConstants.MB_ADDITIONS));
		}

	};

	private void enableActions() {
		VexWidget widget = this.getVexWidget();
		if (widget == null)
			return;

		this.copyAction.setEnabled(widget != null && widget.hasSelection());
		this.cutAction.setEnabled(widget != null && widget.hasSelection());
		this.deleteAction.setEnabled(widget != null && widget.hasSelection());
		this.redoAction.setEnabled(widget != null && widget.canRedo());
		this.undoAction.setEnabled(widget != null && widget.canUndo());
	}

	private ISelectionListener selectionListener = new ISelectionListener() {
		public void selectionChanged(IWorkbenchPart part, ISelection selection) {
			enableActions();
		}
	};

	private class CopyAction extends Action {
		@Override
		public void run() {
			getVexWidget().copySelection();
		}
	};

	private class CutAction extends Action {
		@Override
		public void run() {
			getVexWidget().cutSelection();
		}
	}

	private class DeleteAction extends Action {
		@Override
		public void run() {
			getVexWidget().deleteSelection();
		}
	};

	private class PasteAction extends Action {
		@Override
		public void run() {
			try {
				getVexWidget().paste();
			} catch (DocumentValidationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	};

	private class SelectAllAction extends Action {
		@Override
		public void run() {
			getVexWidget().selectAll();
		}
	};

	private class SetStyleAction extends Action {
		public SetStyleAction(Style style) {
			super(style.getName(), IAction.AS_RADIO_BUTTON);
			this.style = style;
		}

		@Override
		public void run() {
			getVexEditor().setStyle(style);
		}

		private Style style;
	}

	private class StyleMenuManager extends MenuManager {
		public StyleMenuManager() {
			super(Messages.getString("VexActionBarContributor.styleMenu.name")); //$NON-NLS-1$
			final Action noItemsAction = new Action(Messages
					.getString("VexActionBarContributor.noValidItems")) {
				@Override
				public void run() {
				}
			}; //$NON-NLS-1$
			noItemsAction.setEnabled(false);
			noItemsAction.setChecked(true);
			this.add(noItemsAction);
			this.addMenuListener(new IMenuListener() {
				public void menuAboutToShow(IMenuManager manager) {
					manager.removeAll();
					String publicId = getVexWidget().getDocument()
							.getPublicID();
					Style[] styles = Style.getStylesForDoctype(publicId);
					for (int i = 0; i < styles.length; i++) {
						Action action = new SetStyleAction(styles[i]);
						if (styles[i] == getVexEditor().getStyle()) {
							action.setChecked(true);
						}
						manager.add(action);
					}
				}
			});
		}
	}

	private class RowMenuManager extends MenuManager {
		public RowMenuManager() {
			super(Messages.getString("VexActionBarContributor.rowMenu.name")); //$NON-NLS-1$
			this.add(new VexActionAdapter(getVexEditor(),
					new InsertRowAboveAction()));
			this.add(new VexActionAdapter(getVexEditor(),
					new InsertRowBelowAction()));
			this
					.add(new VexActionAdapter(getVexEditor(),
							new DeleteRowAction()));
			this
					.add(new VexActionAdapter(getVexEditor(),
							new MoveRowUpAction()));
			this.add(new VexActionAdapter(getVexEditor(),
					new MoveRowDownAction()));
		}
	}

	private class ColumnMenuManager extends MenuManager {
		public ColumnMenuManager() {
			super(Messages.getString("VexActionBarContributor.columnMenu.name")); //$NON-NLS-1$
			this.add(new VexActionAdapter(getVexEditor(),
					new InsertColumnBeforeAction()));
			this.add(new VexActionAdapter(getVexEditor(),
					new InsertColumnAfterAction()));
			this.add(new VexActionAdapter(getVexEditor(),
					new DeleteColumnAction()));
			this.add(new VexActionAdapter(getVexEditor(),
					new MoveColumnLeftAction()));
			this.add(new VexActionAdapter(getVexEditor(),
					new MoveColumnRightAction()));
		}
	}

	private class RedoAction extends Action {
		@Override
		public void run() {
			if (getVexWidget().canRedo()) {
				getVexWidget().redo();
			}
		}
	};

	private class UndoAction extends Action {
		@Override
		public void run() {
			if (getVexWidget().canUndo()) {
				getVexWidget().undo();
			}
		}
	}

}
