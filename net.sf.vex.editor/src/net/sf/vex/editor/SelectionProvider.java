/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2004 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.editor;

import net.sf.vex.core.ListenerList;

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;

/**
 * Implementation of ISelectionProvider. This class is also an
 * ISelectionChangedListener; any events received by selectionChanged are
 * relayed to registered listeners.
 */
public class SelectionProvider implements ISelectionProvider,
        ISelectionChangedListener {

    public void addSelectionChangedListener(ISelectionChangedListener listener) {
        this.listeners.add(listener);
    }

    /**
     * Fire a SelectionChangedEvent to all registered listeners.
     * @param e Event to be passed to the listeners' selectionChanged method.
     */
    public void fireSelectionChanged(SelectionChangedEvent e) {
        this.selection = e.getSelection();
        this.listeners.fireEvent("selectionChanged", e); //$NON-NLS-1$
    }
    
    public ISelection getSelection() {
    	if (selection != null)
			return this.selection;
		else
			return StructuredSelection.EMPTY; // returning null may call some
												// SelectionChangedEvent
												// constructors cause to fail
    }

    public void removeSelectionChangedListener(
            ISelectionChangedListener listener) {
        this.listeners.remove(listener);
    }

    public void setSelection(ISelection selection) {
        this.selection = selection;
    }

    public void selectionChanged(SelectionChangedEvent event) {
        this.fireSelectionChanged(event);
    }

    //===================================================== PRIVATE

    ISelection selection;
    private ListenerList listeners = new ListenerList(ISelectionChangedListener.class, SelectionChangedEvent.class);
}
