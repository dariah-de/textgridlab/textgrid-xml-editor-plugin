/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2005 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.editor;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;

import net.sf.vex.action.IVexAction;
import net.sf.vex.action.RemoveElementAction;
import net.sf.vex.editor.action.ChangeElementAction;
import net.sf.vex.swt.VexWidget;
import net.sf.vex.widget.IVexWidget;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.IWorkbenchWindowActionDelegate;

/**
 * An IWorkbenchWindowActionDelegate that defers to to an instance of IVexAction.
 * The IDs of actions in plugin.xml using this delegate must be the classnames 
 * of actions implementing IVexAction. Such classes must have a no-args constructor. 
 */
public class VexActionDelegate implements IWorkbenchWindowActionDelegate {

    private static Map actions = new HashMap();
    
    public VexActionDelegate() {
    }

    public void dispose() {
    }

    public void init(IWorkbenchWindow window) {
        this.window = window;
    }

    public void run(IAction action) {
        IVexAction vexAction = getAction(action.getId());
        if (vexAction == null) {
            return;
        }

        VexWidget vexWidget = this.getVexWidget();
        if (vexWidget != null) {
            vexWidget.setFocus();
            vexAction.run(vexWidget);
        }
    }

    public void selectionChanged(IAction action, ISelection selection) {

        IVexAction vexAction = getAction(action.getId());
        boolean enabled;

        if (vexAction == null) {
            enabled = false;
        } else {
            IVexWidget vexWidget = this.getVexWidget();
            if (vexWidget == null) {
                enabled = false;
            } else {
                enabled = vexAction.isEnabled(vexWidget);
                
                if (action.getId().equals(ChangeElementAction.class.getName())) {
                    String elementName = vexWidget.getCurrentElement().getName();
                    String message = Messages.getString("ChangeElementAction.dynamic.label"); //$NON-NLS-1$
                    action.setText(MessageFormat.format(message, new Object[] { elementName }));
                }

                if (action.getId().equals(RemoveElementAction.class.getName())) {
                    String elementName = vexWidget.getCurrentElement().getName();
                    String message = Messages.getString("RemoveElementAction.dynamic.label"); //$NON-NLS-1$
                    action.setText(MessageFormat.format(message, new Object[] { elementName }));
                }
            }
        }
        
        action.setEnabled(enabled);
    }

    
    private IWorkbenchWindow window;
    
    private VexWidget getVexWidget() {
        IEditorPart editor = this.window.getActivePage().getActiveEditor();
        if (editor != null) {
        	VexEditor vexEditor = (VexEditor) editor.getAdapter(VexEditor.class);
        	if (vexEditor != null)
        		return vexEditor.getVexWidget();
        }
        	
        return null;
    }

    private static IVexAction getAction(String actionId) {
        try {
            IVexAction action = (IVexAction) actions.get(actionId);
            if (action == null) {
                action = (IVexAction) Class.forName(actionId).newInstance();
                actions.put(actionId, action);
            }
            return action;
        } catch (Exception ex) {
            String message = MessageFormat.format(
                    Messages.getString("VexActionDelegate.errorLoadingAction"),   //$NON-NLS-1$
                    new Object[] { actionId, ex.getMessage() });
            VexPlugin.getInstance().log(IStatus.ERROR, message, ex);
            return null;
        }
    }
}
