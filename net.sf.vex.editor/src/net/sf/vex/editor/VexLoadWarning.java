package net.sf.vex.editor;

import java.text.MessageFormat;

import info.textgrid.lab.core.swtutils.CommandLinkHandler;

import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Link;

public class VexLoadWarning extends Composite {

	private static final String ROOT_ELEMENT_ERROR = Messages.getString("VexLoadWarning.NoRootElement"); //$NON-NLS-1$
	private static final String DUPLICATE_ERROR = Messages.getString("VexLoadWarning.OpenInOtherEditor"); //$NON-NLS-1$
	private Composite noProperStylesheet;
	private Button proceedButton;
	private Label loadingLabel;
	private Label tooLargeLabel;
	private Label errorLabel;

	/**
	 * Create the composite.
	 * @param parent
	 * @param style
	 */
	public VexLoadWarning(Composite parent, int style) {
		super(parent, style);
		setLayout(new GridLayout(1, false));

		Label lblWysiwymView = new Label(this, SWT.NONE);
		lblWysiwymView.setFont(JFaceResources.getBannerFont());
		lblWysiwymView.setText(Messages.getString("VexLoadWarning.PageTitle")); //$NON-NLS-1$

		Label introLabel = new Label(this, SWT.WRAP);
		introLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		introLabel.setText(Messages.getString("VexLoadWarning.PageDescription")); //$NON-NLS-1$


		errorLabel = new Label(this, SWT.WRAP);
		errorLabel.setVisible(false);
		errorLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		errorLabel.setText(ROOT_ELEMENT_ERROR);

		tooLargeLabel = new Label(this, SWT.WRAP);
		tooLargeLabel.setVisible(false);
		tooLargeLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
		tooLargeLabel.setText(Messages.getString("VexLoadWarning.SizeWarning")); //$NON-NLS-1$

		noProperStylesheet = new Composite(this, SWT.NONE);
		GridLayout gl_noProperStylesheet = new GridLayout(1, false);
		gl_noProperStylesheet.horizontalSpacing = 0;
		noProperStylesheet.setLayout(gl_noProperStylesheet);
		noProperStylesheet.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		Label lblYourFileHas = new Label(noProperStylesheet, SWT.WRAP);
		lblYourFileHas.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		lblYourFileHas.setText(Messages.getString("VexLoadWarning.NeitherTEINorCSS")); //$NON-NLS-1$

		CommandLinkHandler commandLinkHandler = new CommandLinkHandler();
		Link stylesheetLink = new Link(noProperStylesheet, SWT.NONE);
		stylesheetLink.setText(Messages.getString("VexLoadWarning.SelectCSS")); //$NON-NLS-1$
		stylesheetLink.addSelectionListener(commandLinkHandler);

		Link importLink = new Link(noProperStylesheet, SWT.NONE);
		importLink.setText(Messages.getString("VexLoadWarning.ImportCSS")); //$NON-NLS-1$
		importLink.addSelectionListener(commandLinkHandler);

		proceedButton = new Button(this, SWT.NONE);
		proceedButton.setVisible(false);
		proceedButton.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		proceedButton.setText(Messages.getString("VexLoadWarning.ProceedLabel")); //$NON-NLS-1$

		loadingLabel = new Label(this, SWT.NONE);
		loadingLabel.setVisible(false);
		loadingLabel.setText(Messages.getString("VexLoadWarning.LoadingLabel")); //$NON-NLS-1$

	}

	public void showSizeWarning(final long maxSize) {
		tooLargeLabel.setText(MessageFormat.format(Messages.getString("VexLoadWarning.SizeWarning"), maxSize)); //$NON-NLS-1$
		tooLargeLabel.setVisible(true);
		proceedButton.setVisible(true);
		loadingLabel.setVisible(false);
	}

	public void showCSSWarning() {
		noProperStylesheet.setVisible(true);
		proceedButton.setVisible(true);
	}

	public void indicateLoading() {
		loadingLabel.setVisible(true);
		proceedButton.setEnabled(false);
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

	public void setLoadingText(final String message) {
		loadingLabel.setVisible(true);
		loadingLabel.setText(message);
	}

	public void showRootElementError() {
		errorLabel.setVisible(true);
		errorLabel.setText(ROOT_ELEMENT_ERROR);
		proceedButton.setEnabled(false);
	}

	public void addProceedListener(final SelectionListener selectionAdapter) {
		if (proceedButton != null)
			proceedButton.addSelectionListener(selectionAdapter);
	}

	public void showDuplicateError() {
		errorLabel.setVisible(true);
		errorLabel.setText(DUPLICATE_ERROR);
		proceedButton.setEnabled(false);
	}

}
