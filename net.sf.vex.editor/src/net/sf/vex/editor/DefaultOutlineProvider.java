/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2004 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.editor;

import java.util.ArrayList;
import java.util.List;

import net.sf.vex.css.StyleSheet;
import net.sf.vex.dom.IVexDocument;
import net.sf.vex.dom.IVexElement;
import net.sf.vex.dom.IWhitespacePolicy;
import net.sf.vex.widget.CssWhitespacePolicy;

import org.eclipse.jface.viewers.IBaseLabelProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.Viewer;

/**
 * Default implementation of IOutlineProvider. Simply displays all block-level
 * elements.
 */
public class DefaultOutlineProvider implements IOutlineProvider {

    public void init(VexEditor editor) {
        StyleSheet ss = editor.getVexWidget().getStyleSheet();
        this.whitespacePolicy = new CssWhitespacePolicy(ss);
        this.contentProvider = new ContentProvider();
        this.labelProvider = new LabelProvider() {
            public String getText(Object o) {
                IVexElement e = (IVexElement) o;
                String s = e.getText();
                if (s.length() > 30) {
                    s = s.substring(0, 30) + "..."; //$NON-NLS-1$
                }
                return e.getName() + ": " + s;  //$NON-NLS-1$
            }
        };

    }

    public ITreeContentProvider getContentProvider() {
        return this.contentProvider;
    }

    public IBaseLabelProvider getLabelProvider() {
        return this.labelProvider;
    }

    public IVexElement getOutlineElement(IVexElement child) {
        IVexElement element = child;
        while (element != null) {
            if (this.whitespacePolicy.isBlock(element)) {
                return element;
            }
            element = element.getParent();
        }
        return element;
    }

    //====================================================== PRIVATE
    
    private IWhitespacePolicy whitespacePolicy;
    private ITreeContentProvider contentProvider;
    private IBaseLabelProvider labelProvider;

    private class ContentProvider implements ITreeContentProvider {

        public Object[] getChildren(Object parentElement) {
            List blockChildren = new ArrayList();
            IVexElement[] children = ((IVexElement) parentElement).getChildElements();
            for (int i = 0; i < children.length; i++) {
                if (whitespacePolicy.isBlock(children[i])) {
                    blockChildren.add(children[i]);
                }
            }
            return blockChildren.toArray();
        }

        public Object getParent(Object element) {
            return ((IVexElement) element).getParent();
        }

        public boolean hasChildren(Object o) {
            return this.hasBlockChild((IVexElement) o);
        }

        public Object[] getElements(Object inputElement) {
            return new Object[] { ((IVexDocument) inputElement).getRootElement() };
            //return this.getChildren(inputElement);
        }

        public void dispose() {
        }

        public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
            // TODO Auto-generated method stub

        }

        //====================================================== PRIVATE
        
        private boolean hasBlockChild(IVexElement element) {
            IVexElement[] children = element.getChildElements();
            for (int i = 0; i < children.length; i++) {
                if (whitespacePolicy.isBlock(children[i])) {
                    return true;
                }
            }
            return false;
        }
    }


}
