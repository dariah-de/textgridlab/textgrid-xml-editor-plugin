/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2004 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.editor;

import net.sf.vex.dom.IVexElement;

import org.eclipse.jface.viewers.IBaseLabelProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;

/**
 * Implemented by objects that can provide a document outline.
 */
public interface IOutlineProvider {

    /**
     * Initialize this outline provider. This method is guaranteed to be called
     * befor any other in this class. The document has been fully created
     * by the time this method is called, so it is acceptable to access
     * the Vex Widget and its associated stylesheet and document.
     * 
     * @param editor VexEditor with which this outline page is associated.
     */
    public void init(VexEditor editor);
    
    /**
     * Returns the content provider that supplies elements representing
     * the document outline. 
     */
    public ITreeContentProvider getContentProvider();

    /**
     * Returns the label provider for the outline.
     */
    public IBaseLabelProvider getLabelProvider();

    /**
     * Returns the outline element closest to the given child. If
     * <code>child</code> is an outline element, it is returned directly. 
     * @param child Element for which to find the outline element.
     */
    public IVexElement getOutlineElement(IVexElement child);
}
