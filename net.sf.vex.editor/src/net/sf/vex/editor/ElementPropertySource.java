/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2004 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.editor;

import net.sf.vex.dom.AttributeDefinition;
import net.sf.vex.dom.DocumentValidationException;
import net.sf.vex.dom.IVexElement;
import net.sf.vex.dom.Validator;

import org.eclipse.ui.views.properties.ComboBoxPropertyDescriptor;
import org.eclipse.ui.views.properties.IPropertyDescriptor;
import org.eclipse.ui.views.properties.IPropertySource2;
import org.eclipse.ui.views.properties.PropertyDescriptor;
import org.eclipse.ui.views.properties.TextPropertyDescriptor;


/**
 * Property source that treats element attributes as properties.
 */
public class ElementPropertySource implements IPropertySource2 {
    
    /**
     * Class constructor.
     * @param element Selected element.
     * @param validator Validator used to get attribute definitions for the element.
     * @param multi True if multiple elements are selected. In this case
     * the "id" attribute will not be editable.
     */
    public ElementPropertySource(IVexElement element, Validator validator, boolean multi) {
        this.element = element;
        this.validator = validator;
        this.multi = multi;
    }

    public Object getEditableValue() {
        // TODO Auto-generated method stub
        return null;
    }

    public IPropertyDescriptor[] getPropertyDescriptors() {
        
        // note that elements from DocumentFragments don't have access
        // to their original document, so we get it from the VexWidget
        AttributeDefinition[] attrDefs = this.validator.getAttributeDefinitions(this.element.getName());
        IPropertyDescriptor[] pds = new IPropertyDescriptor[attrDefs.length];
        for (int i = 0; i < attrDefs.length; i++) {
            AttributeDefinition def = attrDefs[i];
            if (this.multi && def.getName().equals(ATTR_ID)) {
                pds[i] = new PropertyDescriptor(def.getName(), def.getName());
            } else if (def.isFixed()) {
                pds[i] = new PropertyDescriptor(def.getName(), def.getName());
            } else if (def.getType() == AttributeDefinition.Type.ENUMERATION) {
                pds[i] = new ComboBoxPropertyDescriptor(def.getName(), def.getName(), this.getEnumValues(def));
            } else {
                pds[i] = new TextPropertyDescriptor(def.getName(), def.getName());
            }
        }
        return pds;
    }

    public Object getPropertyValue(Object id) {
        
        if (this.multi && id.equals(ATTR_ID)) {
            return Messages.getString("ElementPropertySource.multiple"); //$NON-NLS-1$
        }
            
        // note that elements from DocumentFragments don't have access
        // to their original document, so we get it from the VexWidget
        AttributeDefinition def = this.validator.getAttributeDefinition(this.element.getName(), (String) id);
        String value = this.element.getAttribute((String) id);
        if (value == null) {
            value = def.getDefaultValue();
            if (value == null) {
                value = ""; //$NON-NLS-1$
            }
        }
        
        if (def.getType() == AttributeDefinition.Type.ENUMERATION) {
            String[] values = this.getEnumValues(def);
            for (int i = 0; i < values.length; i++) {
                if (values[i].equals(value)) {
                    return new Integer(i);
                }
            }
            return new Integer(0); // TODO: if the actual value is not
            // in the list, we should probably
            // add it
        } else {
            return value;
        }
    }

    public boolean isPropertySet(Object id) {
        // TODO Auto-generated method stub
        return false;
    }

    public void resetPropertyValue(Object id) {
        // TODO Auto-generated method stub
        
    }

    public void setPropertyValue(Object id, Object value) {

        try {
            // note that elements from DocumentFragments don't have access
            // to their original document, so we get it from the VexWidget
            AttributeDefinition def = this.validator.getAttributeDefinition(
                    this.element.getName(), (String) id);

            if (def.getType() == AttributeDefinition.Type.ENUMERATION) {
                int i = ((Integer) value).intValue();
                String s = this.getEnumValues(def)[i];
                if (!def.isRequired() && s.equals("")) { //$NON-NLS-1$
                    this.element.removeAttribute(def.getName());
                } else {
                    this.element.setAttribute(def.getName(), s);
                }
            } else {
                String s = (String) value;
                if (s.equals("")) { //$NON-NLS-1$
                    this.element.removeAttribute(def.getName());
                } else {
                    this.element.setAttribute(def.getName(), s);
                }
            }
        } catch (DocumentValidationException e) {
        }
    }

    public boolean isPropertyResettable(Object id) {
        // TODO Auto-generated method stub
        return true;
    }

    //======================================== PRIVATE
    
    private static final String ATTR_ID = "id"; //$NON-NLS-1$
    
    private IVexElement element;
    private Validator validator;
    private boolean multi;
    
    private String[] getEnumValues(AttributeDefinition def) {
        String[] values = def.getValues();
        if (def.isRequired()) {
            return values;
        } else {
            String[] values2 = new String[values.length+1];
            values2[0] = ""; //$NON-NLS-1$
            System.arraycopy(values, 0, values2, 1, values.length);
            return values2;
        }
    }

}