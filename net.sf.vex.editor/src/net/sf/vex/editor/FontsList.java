/*
 * class responsible for changing fonts, size of the VexEditor input
 * 
 * @author Mohamadou Nassourou <mohamadou.nassourou@uni-wuerzburg.de> for TextGrid
 * 
 * 
 */

package net.sf.vex.editor;

import net.sf.vex.core.FontSpec;
import net.sf.vex.css.StyleSheet;
import net.sf.vex.dom.IVexElement;
import net.sf.vex.swt.VexWidget;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.ShellAdapter;
import org.eclipse.swt.events.ShellEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;

/*
 * responsible for changing fonts
 */

class FontsList {

	public String itemFonts = "";
	public Shell fontsShell = new Shell();

	VexWidget vex = null;



	public FontsList(VexWidget vex) {

		this.vex = vex;

	}

	public String getAllFonts() {


		// Display display = window.getShell().getDisplay(); //
		Display display = this.vex.getDisplay();

		fontsShell = new Shell(display, SWT.BORDER_SOLID | SWT.CLOSE
				| SWT.SHADOW_NONE | SWT.ON_TOP);
		fontsShell.setBounds(10, 10, 180, 60);
		fontsShell.setText("Fonts");
		fontsShell.setLayout(new FillLayout());

		Point absolutePosition = display.getCurrent().getCursorLocation();

		fontsShell.setLocation(absolutePosition.x, absolutePosition.y);

		fontsShell.setBackground(display
				.getSystemColor(SWT.COLOR_WIDGET_LIGHT_SHADOW));
		fontsShell.setForeground(display.getSystemColor(SWT.COLOR_BLACK));

		fontsShell.addShellListener(new ShellAdapter() {
			@Override
			public void shellDeactivated(ShellEvent e) {
				((Shell) e.getSource()).setVisible(false);

			}
		});

		final Combo combo1 = new Combo(fontsShell, SWT.NONE | SWT.DROP_DOWN
				| SWT.READ_ONLY);

		combo1.add("NORMAL");
		combo1.add("BOLD");
		combo1.add("ITALIC");
		combo1.pack();
		combo1.select(0);

		final Combo combo2 = new Combo(fontsShell, SWT.NONE | SWT.DROP_DOWN);
		combo2.add("10");
		combo2.add("12");
		combo2.add("14");
		combo2.add("16");
		combo2.add("18");
		combo2.add("20");
		combo2.pack();
		combo2.select(1);

		combo1.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				ChangeFonts(combo1.getText(), Integer
						.parseInt(combo2.getText()));
			}
		});

		combo2.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				ChangeFonts(combo1.getText(), Integer
						.parseInt(combo2.getText()));
			}
		});

		combo2.addListener(SWT.DefaultSelection, new Listener() {
			public void handleEvent(Event e) {
				ChangeFonts(combo1.getText(), Integer
						.parseInt(combo2.getText()));
			}
		});

		fontsShell.addListener(SWT.Traverse, new Listener() {
			public void handleEvent(Event event) {
				switch (event.detail) {
				case SWT.TRAVERSE_ESCAPE:
					if (fontsShell.isVisible()) {
						fontsShell.setVisible(false);
					}
					event.detail = SWT.TRAVERSE_NONE;
					event.doit = false;
					break;
				}
			}
		});

		fontsShell.setVisible(true);
		fontsShell.setFocus();

		//
		return itemFonts;
	}

	public void DynamicStyleForDocument(StyleSheet css, FontSpec fs) {


		IVexElement root = this.vex.getDocument()
				.getRootElement();
		IVexElement[] children1 = root.getChildElements();



		for (int c = 0; c < children1.length; c++) {
			css.getStyles(children1[c]).setFont(fs);

			IVexElement[] child2 = children1[c].getChildElements();
			if (child2.length > 0) {
				for (int c1 = 0; c1 < child2.length; c1++) {
					css.getStyles(child2[c1]).setFont(fs);

					IVexElement[] child3 = child2[c1].getChildElements();
					if (child3.length > 0) {
						for (int c2 = 0; c2 < child3.length; c2++) {
							css.getStyles(child3[c2]).setFont(fs);

							IVexElement[] child4 = child3[c2]
									.getChildElements();
							if (child4.length > 0) {
								for (int c3 = 0; c3 < child4.length; c3++) {
									css.getStyles(child4[c3]).setFont(fs);

									IVexElement[] child5 = child4[c3]
											.getChildElements();
									if (child5.length > 0) {
										for (int c4 = 0; c4 < child5.length; c4++) {
											css.getStyles(child5[c4]).setFont(
													fs);

											IVexElement[] child6 = child5[c4]
													.getChildElements();
											if (child6.length > 0) {
												for (int c5 = 0; c5 < child6.length; c5++) {
													css.getStyles(child6[c5])
															.setFont(fs);

													IVexElement[] child7 = child6[c5]
															.getChildElements();
													if (child7.length > 0) {
														for (int c6 = 0; c6 < child7.length; c6++) {
															css
																	.getStyles(
																			child7[c6])
																	.setFont(fs);

															IVexElement[] child8 = child7[c6]
																	.getChildElements();
															if (child8.length > 0) {
																for (int c7 = 0; c7 < child7.length; c7++) {
																	css
																			.getStyles(
																					child8[c7])
																			.setFont(
																					fs);
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}

				}
			}
		}

		this.vex.setStyleSheet(css);

	}

	public void ChangeFonts(String f, int s) {

		// vex.createPartControl(window.getActivePage().getActiveEditor()
		// .getEditorSite().getShell());

		// vex.setInput(window.getActivePage().getActiveEditor().getEditorInput());

		System.out.println("root: " + f + "\nvex: " + this.vex);

		String[] fa = new String[2];
		fa[0] = "";
		fa[1] = "";

		int bold = SWT.BOLD;
		int italic = SWT.ITALIC;
		int normal = SWT.NORMAL;
		int temp = 0;

		if (f.equalsIgnoreCase("BOLD"))
			temp = bold;
		else if (f.equalsIgnoreCase("ITALIC"))
			temp = italic;
		else if (f.equalsIgnoreCase("NORMAL"))
			temp = normal;

		FontSpec fs = new FontSpec(fa, temp, s);
		StyleSheet css = this.vex.getStyleSheet();



		DynamicStyleForDocument(css, fs);


	}

}
