/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2005 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.editor.action;

import net.sf.vex.action.IVexAction;
import net.sf.vex.editor.Messages;
import net.sf.vex.editor.VexEditor;
import net.sf.vex.swt.VexWidget;

import org.eclipse.jface.action.Action;

/**
 * Adapts a JFace Action to an IVexAction instance. The ID and definition ID of 
 * the resulting action is set to the classname of the action. Localized
 * strings for the action are pulled from the classname of the given action. For example,
 * if the action is "net.sf.vex.action.PasteTextAction", the following properties
 * are retrieved from plugin.xml:
 * 
 * <dl>
 *   <dt>PasteTextAction.label</dt>
 *   <dd>The action's label.</dd>
 *   <dt>PasteTextAction.tooltip</dt>
 *   <dd>The action's tooltip.</dd>
 * </dl>
 */
public class VexActionAdapter extends Action {

    /**
     * Class constructor.
     * @param editor VexEditor to which the action will apply.
     * @param action IVexAction to be invoked.
     */
    public VexActionAdapter(VexEditor editor, IVexAction action) {
        
        this.editor = editor;
        this.action = action;
        
        String id = action.getClass().getName();
        int i = id.lastIndexOf("."); //$NON-NLS-1$
        String key = id.substring(i+1);

        this.setId(id);
        this.setActionDefinitionId(id);
		this.setText(Messages.getString(key + ".label")); //$NON-NLS-1$
        this.setToolTipText(Messages.getString(key + ".tooltip")); //$NON-NLS-1$
    }

    
    @Override
	public void run() {
        VexWidget vexWidget = editor == null? null: editor.getVexWidget();
        if (vexWidget != null) {
            this.action.run(vexWidget);
        }
    }
    
    private VexEditor editor;
    private IVexAction action;
}
