package net.sf.vex.editor.action;

import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Preferences;
import org.eclipse.core.runtime.preferences.IPreferencesService;

import net.sf.vex.action.IVexAction;
import net.sf.vex.editor.LayoutPreferencePage;
import net.sf.vex.editor.VexEditor;
import net.sf.vex.editor.VexPlugin;
import net.sf.vex.editor.VexPreferencePage;
import net.sf.vex.layout.BlockElementBox;
import net.sf.vex.layout.InlineElementBox;
import net.sf.vex.widget.IVexWidget;

/**
 * Toggles the display of markers around inline elements.
 */
public class ShowBlockElementMarkersAction implements IVexAction {

	public boolean isEnabled(IVexWidget vexWidget) {
		return true;
	}

	public void run(IVexWidget vexWidget) {

		Preferences preferences = VexPlugin.getInstance()
				.getPluginPreferences();

		boolean showMarkers = preferences
				.getBoolean(LayoutPreferencePage.PREFERENCE_SHOW_BLOCK_MARKERS);

		showMarkers = !showMarkers;

		preferences
				.setValue(LayoutPreferencePage.PREFERENCE_SHOW_BLOCK_MARKERS,
						showMarkers);

		VexPlugin.getInstance().savePluginPreferences();
	}
}
