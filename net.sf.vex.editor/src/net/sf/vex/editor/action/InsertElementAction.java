/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2005 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.editor.action;

import net.sf.vex.action.IVexAction;
import net.sf.vex.editor.InsertAssistant;
import net.sf.vex.swt.VexWidget;
import net.sf.vex.widget.IVexWidget;

/**
 * @author john
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class InsertElementAction implements IVexAction {

    public void run(IVexWidget vexWidget) {
        new InsertAssistant().show((VexWidget) vexWidget);
    }

    public boolean isEnabled(IVexWidget vexWidget) {
        return true;
    }

}
