package net.sf.vex.editor.action;

import static info.textgrid.lab.core.swtutils.AdapterUtils.getAdapter;
import net.sf.vex.editor.VexEditorPage;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.ui.handlers.HandlerUtil;

public class ReloadLinkedDocumentHandler extends AbstractHandler implements IHandler {

	public Object execute(ExecutionEvent event) throws ExecutionException {
		VexEditorPage vexEditorPage = getAdapter(HandlerUtil.getActiveEditor(event), VexEditorPage.class);
		vexEditorPage.reloadInput(true);

		return null;
	}

}
