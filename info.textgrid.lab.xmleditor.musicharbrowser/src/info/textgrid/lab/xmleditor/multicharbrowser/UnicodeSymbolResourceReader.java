package info.textgrid.lab.xmleditor.multicharbrowser;

import java.io.File;
import java.io.IOException;
import java.util.Hashtable;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Plugin;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.IJobChangeEvent;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.core.runtime.jobs.JobChangeAdapter;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

/**
 * 
 * Parsing class for deserializing unicode symbol descriptions
 * 
 * @author <Julian Dabbert> <dabbert AT upb.de>
 * 
 */
public class UnicodeSymbolResourceReader {

	/**
	 * This Hashtable holds the id/value pairs of all unicode symbol unicode
	 * characters as parsed from xml file
	 */
	private static Hashtable<String, String> unicodeSymbolsHashtable = new Hashtable<String, String>();

	/**
	 * Getter for the static unicodeSymbolsHashtable
	 * 
	 * @return The Hashtable holds the id/value pairs of all unicode symbol
	 *         unicode characters as parsed from xml file
	 */
	public static Hashtable<String, String> getMusicalSymbolsHashtable() {
		return unicodeSymbolsHashtable;
	}

	/**
	 * Read the id/value pairs from xml file
	 * 
	 * @param bundlePath
	 *            : the path of the bundle containing the source xml file
	 */
	public static void readFromFile() {
		// clear the existing Hashtable
		unicodeSymbolsHashtable.clear();
		StreamWriterUtils.writeResourceToTempFile(((Plugin) Activator.getDefault())
				.getStateLocation().toPortableString(), "Resources",
				"Unicode51XmlSupplement.xml");
		// run the xml parsing in a background Job
		readerBackgroundJob();
	}

	/**
	 * Reads the XML Files for Unicode Description in a Background Job
	 * 
	 * @param bundlePath
	 *            The Path of this Bundle
	 * @param start
	 *            The Hex-Point of the start Position
	 * @param end
	 *            The Hex-Point of the end Position
	 */
	private static void readerBackgroundJob() {
		final Job job = new Job(Messages.UnicodeSymbolResourceReader_0) {
			protected IStatus run(IProgressMonitor monitor) {
				try {
					// load the necessary resource xml file
					File xmlFile = new File(((Plugin) Activator.getDefault())
							.getStateLocation().toPortableString()
							+ "/Unicode51XmlSupplement.xml");
					if (xmlFile.exists()) {
						parseUnicodeXmlBlockFile(xmlFile);
					} else {
						StreamWriterUtils
								.writeLogError(IStatus.ERROR,
										Messages.UnicodeSymbolResourceReader_2,
										xmlFile);
					}
					if (monitor.isCanceled())
						return Status.CANCEL_STATUS;

					return Status.OK_STATUS;
				} finally {
				}
			}
		};

		job.addJobChangeListener(new JobChangeAdapter() {
			public void done(IJobChangeEvent event) {
				if (!event.getResult().isOK()) {
					StreamWriterUtils.writeLogError(IStatus.ERROR,
							Messages.UnicodeSymbolResourceReader_3, event);
				}
			}
		});
		job.setPriority(Job.SHORT);
		job.setSystem(true);
		job.schedule(); // start as soon as possible
	}

	/**
	 * Parses an xml file and maps its content to the static Hashtable
	 * unicodeSymbolsHashtable
	 * 
	 * @param xmlFile
	 *            The name of the xml file to be parsed
	 */
	private static void parseUnicodeXmlBlockFile(File xmlFile) {
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document document = builder.parse(xmlFile);
			// Get list of nodes to given element tag name
			NodeList ndList = document.getElementsByTagName("symbol"); //$NON-NLS-1$
			NamedNodeMap namedNodeMap;
			for (int i = 0; i < ndList.getLength(); i++) {
				namedNodeMap = ndList.item(i).getAttributes();
				unicodeSymbolsHashtable
						.put(namedNodeMap.getNamedItem("id") //$NON-NLS-1$
								.getNodeValue().toString(), namedNodeMap
								.getNamedItem("name").getNodeValue().toString()); //$NON-NLS-1$
			}

		} catch (SAXParseException spe) {
			StreamWriterUtils.writeLogError(IStatus.ERROR, spe.getLineNumber()
					+ ", uri " + spe.getSystemId(), spe); //$NON-NLS-1$
		} catch (SAXException sxe) {
			StreamWriterUtils.writeLogError(IStatus.ERROR, sxe.getMessage(), sxe);
		} catch (ParserConfigurationException pce) {
			StreamWriterUtils.writeLogError(IStatus.ERROR, pce.getMessage(), pce);
		} catch (IOException ioe) {
			StreamWriterUtils.writeLogError(IStatus.ERROR, ioe.getMessage(), ioe);
		}
	}
}
