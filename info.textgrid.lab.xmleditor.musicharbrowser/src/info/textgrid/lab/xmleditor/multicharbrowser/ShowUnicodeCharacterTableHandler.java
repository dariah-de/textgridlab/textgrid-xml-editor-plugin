package info.textgrid.lab.xmleditor.multicharbrowser;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.intro.IIntroManager;
/**
 * The showDictionaryResultView handler extends AbstractHandler, an IHandler base class.
 * @see org.eclipse.core.commands.IHandler
 * @see org.eclipse.core.commands.AbstractHandler
 * 
 * @author <Julian Dabbert> <dabbert AT upb.de>
 */
public class ShowUnicodeCharacterTableHandler extends AbstractHandler {

	/**
	 * the command has been executed, so extract the needed information
	 * from the application context.
	 */
	public Object execute(ExecutionEvent arg0) throws ExecutionException {	
	try {
		IWorkbenchPage activePage = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
		if (activePage == null) 
			MessageDialog
			.openWarning(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), "No open perspective",
					"Currently there is no open perspective to contain the view.");
		else 
			activePage.showView("info.textgrid.lab.xmleditor.multicharbrowser.viewer.UnicodeView");
		
		// close Welcome-Screen
		IIntroManager manager = PlatformUI.getWorkbench().getIntroManager();
		//manager.setIntroStandby(manager.getIntro(), true);
		manager.closeIntro(manager.getIntro());
	} catch (Exception e) {
		IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID,
			    "Could not open Unicode Character Table View!", e);
		Activator.getDefault().getLog().log(status);
	}
	return null;
}

}
