package info.textgrid.lab.xmleditor.multicharbrowser;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.MessageFormat;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.ui.statushandlers.StatusManager;

/**
 * 
 * This class provides static utility methods for all classes in the package
 * Previously "HelperMethods"
 * 
 * @author <Julian Dabbert> <dabbert AT upb.de>
 */
public class StreamWriterUtils {

	/**
	 * Writes an error to the status log
	 * 
	 * @param errorState
	 *            The warninglevel: IStatus.ERROR, -WARNING, -INFO
	 * @param errorString
	 *            The error message
	 * @param ex
	 *            The erroneous object
	 */
	protected static void writeLogError(int errorState, String errorString,
			Object ex) {
		StatusManager.getManager().handle(
				new Status(errorState, Activator.PLUGIN_ID, MessageFormat
						.format(errorString, ex.toString(), ex)));
	}

	/**
	 * Writes a resource file to disk for temporary parsing
	 * 
	 * @param path
	 *            The path to temp dir, default:
	 *            Activator.getDefault().getStateLocation().toPortableString()
	 * @param resourceDir
	 *            The Directory where to find the resource ("Resource" or
	 *            "icons")
	 * @param resourceFile
	 *            The filename within the resource directory ("MusicalSymbolsTextGrid.ttf")
	 */
	protected static void writeResourceToTempFile(String path,
			String resourceDir, String resourceFile) {
		File outFile = new File(path + "/" + resourceFile);
		// write new file only if no previous file exists
		if (!outFile.exists()) {
			InputStream inputStream;
			try {
				inputStream = FileLocator.openStream(Activator.getDefault()
						.getBundle(),
						new Path(resourceDir + "/" + resourceFile), false);
				OutputStream outputStream;
				outputStream = new FileOutputStream(outFile);
				byte buf[] = new byte[1024];
				int len;
				while ((len = inputStream.read(buf)) > 0)
					outputStream.write(buf, 0, len);
				outputStream.close();
				inputStream.close();
			} catch (FileNotFoundException e) {
				StreamWriterUtils.writeLogError(IStatus.ERROR, e.getMessage(), e);
			} catch (IOException ioe) {
				StreamWriterUtils.writeLogError(IStatus.ERROR, ioe.getMessage(),
						ioe);
			}
		}
	}

}
