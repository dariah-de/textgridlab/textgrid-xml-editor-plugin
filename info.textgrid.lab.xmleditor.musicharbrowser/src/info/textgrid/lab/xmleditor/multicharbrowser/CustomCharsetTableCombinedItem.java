package info.textgrid.lab.xmleditor.multicharbrowser;
//
//This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-833 
//See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
//Any modifications to this file will be lost upon recompilation of the source schema. 
//Generated on: 2009.10.16 at 11:26:02 AM CEST 
//

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
* <p>Java class for anonymous complex type.
* 
* <p>The following schema fragment specifies the expected content contained within this class.
* 
* <pre>
* &lt;complexType>
*   &lt;complexContent>
*     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
*       &lt;sequence>
*         &lt;element ref="{}customCharsetTableItem" maxOccurs="unbounded" minOccurs="0"/>
*       &lt;/sequence>
*     &lt;/restriction>
*   &lt;/complexContent>
* &lt;/complexType>
* </pre>
* 
* 
* @author <Julian Dabbert> <dabbert AT upb.de>
* 
*/
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
 "customCharsetTableItem"
})
@XmlRootElement(name = "customCharsetTableCombinedItem")
public class CustomCharsetTableCombinedItem {

 protected List<CustomCharsetTableItem> customCharsetTableItem;

 /**
  * Gets the value of the customCharsetTableItem property.
  * 
  * <p>
  * This accessor method returns a reference to the live list,
  * not a snapshot. Therefore any modification you make to the
  * returned list will be present inside the JAXB object.
  * This is why there is not a <CODE>set</CODE> method for the customCharsetTableItem property.
  * 
  * <p>
  * For example, to add a new item, do as follows:
  * <pre>
  *    getCustomCharsetTableItem().add(newItem);
  * </pre>
  * 
  * 
  * <p>
  * Objects of the following type(s) are allowed in the list
  * {@link CustomCharsetTableItem }
  * 
  * 
  */
 public List<CustomCharsetTableItem> getCustomCharsetTableItem() {
     if (customCharsetTableItem == null) {
         customCharsetTableItem = new ArrayList<CustomCharsetTableItem>();
     }
     return this.customCharsetTableItem;
 }

}
