package info.textgrid.lab.xmleditor.multicharbrowser;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.Vector;

import com.ibm.icu.lang.UCharacter;
import com.ibm.icu.lang.UScript;

/**
 * 
 * Unicode Model for the characters within the unicode character table
 * 
 * @author <Julian Dabbert> <dabbert AT upb.de>
 */
public class UnicodeModel {

	/**
	 * number of columns in the current table
	 */
	private static int columns = 8;

	/**
	 * Two-level Vector representing the Unicode Character Table
	 */
	private static Vector<String[]> chars = new Vector<String[]>(2);

	/**
	 * The last block whose position should be located
	 */
	private static String lastQueryBlock = "";

	/**
	 * The start index of the last queried block
	 */
	private static int lastQueryStart = 0;

	/**
	 * The end index of the last queried block
	 */
	private static int lastQueryEnd = 0;

	/**
	 * The Set of all available Model Viewers
	 */
	private Set<IModelViewer> changeListeners = new HashSet<IModelViewer>();

	/**
	 * String Array containing the Names of all available font families
	 */
	private String[] fonts = java.awt.GraphicsEnvironment
			.getLocalGraphicsEnvironment().getAvailableFontFamilyNames();

	/**
	 * Constructor (never called this fashion)
	 */
	public UnicodeModel(int start, int end) {
		super();
		initVector(start, end);
	}

	/**
	 * Constructor using either block or script
	 * 
	 * @param name
	 *            The Name of the Constructor String, e.g. "MUSICAL_SYMBOLS"
	 * @param scriptOrBlock
	 *            A Discriminator String, either "script" OR "block"
	 * @param numberOfColumns
	 *            Number of Table columns
	 */
	public UnicodeModel(String name, boolean isBlock, int numberOfColumns) {
		super();
		resizeColumns(numberOfColumns);
		if (isBlock) {
			this.initBlockVector(name);
		} else {
			this.initScriptVector(name);
		}
	}

	/**
	 * Constructor using either block or script
	 * 
	 * @param name
	 *            The Name of the Constructor String, e.g. "MUSICAL_SYMBOLS"
	 */
	public UnicodeModel(int numberOfColumns, String[] customChars) {
		super();
		resizeColumns(numberOfColumns);
		initCustomBlockset(customChars);
	}

	/**
	 * Build a custom charset from the selected characters
	 * 
	 * @param customChars
	 *            an integer array with codepoints of unicode characters
	 */
	public void initCustomBlockset(String[] customChars) {
		int codePoint;
		chars.clear();
		Vector<String> vector = new Vector<String>();
		for (int iterator = 0; iterator < customChars.length; iterator++) {
			codePoint = customChars[iterator].codePointAt(0);
			while (Character.isWhitespace(codePoint)
					|| !Character.isDefined(codePoint)
					|| Character.isIdentifierIgnorable(codePoint)
					|| Character.isSpaceChar(codePoint)) {
				codePoint++;
			}
			// vector.add(newString(codePoint));
			vector.add(customChars[iterator]);
		}
		String[] temp = new String[columns];
		int index = 0;
		for (int iter = 0; iter < vector.size(); iter++) {
			if (index < columns) {
				temp[index] = vector.get(iter);
				index++;
			} else {
				chars.add(temp);
				temp = new String[columns];
				temp[0] = vector.get(iter);
				index = 1;
			}
		}
		chars.add(temp);
	}

	/**
	 * Build the table using a Block
	 * 
	 * @param block
	 *            The unicode block to be used for building the table
	 */
	public void initBlockVector(String block) {
		if (block.equals(lastQueryBlock)) {
			chars.clear();
			initVector(lastQueryStart, lastQueryEnd);
		} else {
			chars.clear();
			int index = 0x0000;
			int start = 0x0000, end = 0xffff;
			// what is the first character index number in the searched block?
			while (!UCharacter.UnicodeBlock.of(index).toString()
					.equalsIgnoreCase(block)) {
				index++;
			}
			start = index;
			// what is the first character index number outside of the searched
			// block - where does it end?
			while (UCharacter.UnicodeBlock.of(index).toString()
					.equalsIgnoreCase(block)) {
				index++;
			}
			end = index;
			lastQueryBlock = block;
			lastQueryStart = start;
			lastQueryEnd = end;
			initVector(start, end);
		}
	}

	/**
	 * Build the table using a Script
	 * 
	 * @param script
	 *            The Script to be used for building the table
	 */
	public void initScriptVector(String script) {
		chars.clear();
		String[] temp = new String[columns];
		int index = 0;
		for (int i = 0x0000; i < 0x10ffff; i++) {
			if (UScript.getName(UScript.getScript(i)).equalsIgnoreCase(script)) {
				if (index < columns) {
					temp[index] = newString(i);
					index++;
				} else {
					chars.add(temp);
					temp = new String[columns];
					temp[0] = newString(i);
					index = 1;
				}
			}
		}
	}

	/**
	 * Construct the table using the given unicode characters
	 * 
	 * @param start
	 *            Start point, e.g.0x0000
	 * @param end
	 *            End point, e.g. 0xFFFF
	 */
	public static void initVector(int start, int end) {
		int codePoint = start;
		// int max = 0x10fff;
		for (int j = 0; j < end / columns; j++) {
			String[] s = new String[columns];
			if (codePoint <= end) {
				for (int i = 0; i < columns && codePoint <= end; i++) {
					while (Character.isWhitespace(codePoint)
							|| !Character.isDefined(codePoint)
							|| Character.isIdentifierIgnorable(codePoint)
							|| Character.isSpaceChar(codePoint)) {
						codePoint++;
					}
					s[i] = newString(codePoint);
					codePoint++;
				}
				chars.add(s);
			} else {
				break;
			}
		}
	}

	/**
	 * Change the number of Columns to display
	 * 
	 * @param newColumns
	 *            : the new number of columns
	 */
	public void resizeColumns(int newColumns) {
		columns = newColumns;
	}

	/**
	 * Updates the model using all currently enabled change listeners
	 * 
	 * @param start
	 *            Start position of the Model's elements to be updated
	 * @param end
	 *            End position of the Model's elements to be updated
	 */
	public void updateModel(int start, int end) {
		Iterator<IModelViewer> iterator = changeListeners.iterator();
		while (iterator.hasNext())
			iterator.next().updateModel(start, end);

	}

	/**
	 * Updates the model using all currently enabled change listeners
	 * 
	 * @param name
	 *            Name of the model's element to be updated
	 */
	public void updateModel(String name) {
		Iterator<IModelViewer> iterator = changeListeners.iterator();
		while (iterator.hasNext())
			iterator.next().updateScript(name);
	}

	/**
	 * updates the model using all currently enabled change listeners
	 * 
	 * @param fontName
	 *            fontname that should be updated
	 */
	public void updateFont(String fontName) {
		Iterator<IModelViewer> iterator = changeListeners.iterator();
		while (iterator.hasNext())
			iterator.next().updateFont(fontName);
	}

	/**
	 * Getter for the columns
	 * 
	 * @return The number of Columns of the table
	 */
	public int getColumns() {
		return columns;
	}

	/**
	 * Getter for the Two-level Vector representing the Unicode Character Table
	 * 
	 * @return The Two-level Vector representing the Unicode Character Table
	 */
	public Vector<String[]> getChars() {
		return chars;
	}

	/**
	 * Getter for the String Array containing the Names of all available font
	 * families
	 * 
	 * @return The String Array containing the Names of all available font
	 *         families
	 */
	public String[] getFonts() {
		return fonts;
	}

	/**
	 * Adds a Change listener
	 * 
	 * @param viewer
	 *            the change listener to be added
	 */
	public void addChangeListener(IModelViewer viewer) {
		changeListeners.add(viewer);
	}

	/**
	 * Removes a change listener
	 * 
	 * @param viewer
	 *            The change listener to be removed
	 */
	public void removeChangeListener(IModelViewer viewer) {
		changeListeners.remove(viewer);
	}

	/**
	 * Convert a Unicode Character decimal point to a String character
	 * 
	 * @param codePoint
	 *            The decimal codePoint to be converted
	 * @return The Unicode String to be addressed
	 */
	public static String newString(int codePoint) {
		if (Character.charCount(codePoint) == 1) {
			return String.valueOf((char) codePoint);
		} else {
			return new String(Character.toChars(codePoint));
		}
	}

	/**
	 * Empty main method
	 * 
	 * @param args
	 *            no arguments needed
	 */
	public static void main(String args[]) {
	}
}