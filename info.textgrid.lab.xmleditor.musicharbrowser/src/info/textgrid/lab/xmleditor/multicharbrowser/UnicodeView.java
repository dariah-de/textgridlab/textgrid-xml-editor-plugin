package info.textgrid.lab.xmleditor.multicharbrowser;

import info.textgrid.lab.core.swtutils.AdapterUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import net.sf.vex.core.Log;
import net.sf.vex.dom.DocumentValidationException;
import net.sf.vex.editor.VexEditor;
import net.sf.vex.swt.VexWidget;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.operations.AbstractOperation;
import org.eclipse.core.commands.operations.IOperationHistory;
import org.eclipse.core.commands.operations.IUndoableOperation;
import org.eclipse.core.commands.operations.OperationHistoryFactory;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.ActionContributionItem;
import org.eclipse.jface.action.IContributionItem;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.text.TextSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FontDialog;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.commands.ICommandService;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.ui.texteditor.IDocumentProvider;
import org.eclipse.ui.texteditor.ITextEditor;

import com.ibm.icu.lang.UCharacter;
import com.ibm.icu.lang.UScript;

/**
 * 
 * The Unicode View manages the GUI Elements at the Toolbar. These are the
 * buttons for switching the blockset, the combobox containing either blocks or
 * scripts for selection as well as all the custom character sets and a link to
 * the corresponding editor.
 * 
 * @author <Julian Dabbert> <dabbert AT upb.de>
 * 
 */
public class UnicodeView extends ViewPart {

	/**
	 * This is a reference to the Unicode Viewer handling the Table contents
	 */
	private UnicodeViewer unicodeViewer;

	/**
	 * This is a reference to the Table containing the unicode character symbols
	 */
	private Table table;

	/**
	 * This List contains all symbols in a table row
	 */
	private final ArrayList<String> list = new ArrayList<String>(16);

	/**
	 * This is a command service used for compatiblity reasons only
	 */
	private ICommandService commandService;

	/**
	 * This is a custom control element for embedding a combo box in a toolbar
	 */
	private ComboContributionItem comboContrib;

	/**
	 * This is a reference to the toolbar manager which is used for filling the
	 * toolbar items
	 */
	private IToolBarManager toolBarManager;

	/**
	 * This variable stores the current clicking state the toolbar Toggle
	 * buttons are in
	 */
	private int toggleState;

	/**
	 * This is the Action for the Button "block"
	 */
	private ActionContributionItem buttonToggleModeLeft;

	/**
	 * This is the Action for the Button "script"
	 */
	private ActionContributionItem buttonToggleModeRight;

	/**
	 * This is the Action for the first custom charset in the toolbar
	 */
	private Action customActionLeft;

	/**
	 * This is the Action for the secondcustom charset in the toolbar
	 */
	private Action customActionRight;

	/**
	 * This String contains all Strings contained in the comboContrib comboBox
	 */
	private Vector<String> comboItems = null;
	
	final UnicodeView unicodeView = this;
	
	// bit constants for determining which button is toggled - mutually
	// exclusive
	/**
	 * Bit constant for No active Button
	 */
	public static final int TOGGLE_NONE = 0;

	/**
	 * Bit constant for active Button Blockset
	 */
	public static final int TOGGLE_LEFT = 1;

	/**
	 * Bit constant for active Button Script
	 */
	public static final int TOGGLE_RIGHT = 2;

	/**
	 * Bit constant for active Button Custom Left
	 */
	public static final int TOGGLE_CUSTOM_LEFT = 3;

	/**
	 * Bit constant for active Button Custom Right
	 */
	public static final int TOGGLE_CUSTOM_RIGHT = 4;

	/**
	 * Extracted Method to setup the left toggle button
	 */
	private void setupToggleButtonLeft() {
		buttonToggleModeLeft = new ActionContributionItem(new Action("Block", //$NON-NLS-1$
				SWT.TOGGLE) {
			@Override
			public void run() {
				if (toggleState != TOGGLE_LEFT) {
					// not yet toggled
					comboContrib.updateItems(getBlockVector());
					setToggleState(TOGGLE_LEFT);
					toggleButtons();
					this.setText("Block"); //$NON-NLS-1$
				} else
					// already toggled, noop
					buttonToggleModeLeft.getAction().setChecked(true);
				changeCharacterSet(comboContrib.getLayersCombo().getItem(
						comboContrib.getLayersCombo().getSelectionIndex()),
						true);
			}
		});
		buttonToggleModeLeft.getAction().setChecked(true);
		buttonToggleModeLeft.getAction().setToolTipText(Messages.UnicodeView_2);
		// create an image for the toggleButtonLeft
		ImageDescriptor toggleLeftEclipseImage = ImageDescriptor
				.createFromURL(CustomCharsetResourceReader.findResource(
						"icons", "font-x-generic.png")); //$NON-NLS-1$
		buttonToggleModeLeft.getAction().setImageDescriptor(
				toggleLeftEclipseImage);
	}

	/**
	 * Extracted Method to setup the right toggle button
	 */
	private void setupToggleButtonRight() {
		buttonToggleModeRight = new ActionContributionItem(new Action("Script", //$NON-NLS-1$
				SWT.TOGGLE) {
			@Override
			public void run() {
				if (toggleState != TOGGLE_RIGHT) {
					// not yet toggled
					comboContrib.updateItems(getScriptVector());
					setToggleState(TOGGLE_RIGHT);
					toggleButtons();
					this.setText("Script"); //$NON-NLS-1$
				} else
					// already toggled, noop
					buttonToggleModeRight.getAction().setChecked(true);
				changeCharacterSet(comboContrib.getLayersCombo().getItem(
						comboContrib.getLayersCombo().getSelectionIndex()),
						false);
			}
		});
		buttonToggleModeRight.getAction()
				.setToolTipText(Messages.UnicodeView_6);
		// create an image for the toggleButtonRight
		ImageDescriptor toggleRightEclipseImage = ImageDescriptor
				.createFromURL(CustomCharsetResourceReader.findResource(
						"icons", "text-x-generic.png")); //$NON-NLS-1$
		buttonToggleModeRight.getAction().setImageDescriptor(
				toggleRightEclipseImage);
	}

	/**
	 * Set ComboBox Items to Blocks
	 * 
	 * @return A Vector with all Blockset names
	 */
	protected static Vector<String> getBlockVector() {
		String[] blocks = new String[154];
		for (int i = 0; i < 154; i++)
			blocks[i] = UCharacter.UnicodeBlock.getInstance(i + 1).toString();
		java.util.Arrays.sort(blocks);
		Vector<String> comboItems = new Vector<String>();
		for (String entry : blocks)
			// these both blocks are corrupt, attributed with hangul syllables
			if (!entry.equals("LATIN_EXTENDED_D") //$NON-NLS-1$
					&& !entry.equals("LATIN_EXTENDED_E")) //$NON-NLS-1$
				comboItems.add(entry);
		return comboItems;
	}

	/**
	 * Set ComboBox Items to Scripts
	 */
	private Vector<String> getScriptVector() {
		if (null == this.comboItems)
			initializeScriptVector();
		return this.comboItems;
	}

	/**
	 * Initialize the Vector for the Scripts Combo Box
	 */
	private void initializeScriptVector() {
		// the final vector containing all resulting non-empty script names
		this.comboItems = new Vector<String>();
		Hashtable<String, Boolean> hashtableScriptHasMembers = new Hashtable<String, Boolean>();

		// insert all possible script names
		for (int i = 0; i < UScript.CODE_LIMIT; i++)
			hashtableScriptHasMembers.put(UScript.getName(i),
					new Boolean(false));
		// tag the ones where no char is mapped to
		boolean isFound;
		String currentScript;
		for (int i = 0x0000; i < 0x10ffff; i++) {
			currentScript = UScript.getName(UScript.getScript(i));
			isFound = hashtableScriptHasMembers.get(currentScript)
					.booleanValue();
			if (!isFound) {
				hashtableScriptHasMembers.remove(currentScript);
				hashtableScriptHasMembers.put(currentScript, new Boolean(true));
			}
		}

		// clear out the ones without mapped characters
		Enumeration<String> enums = hashtableScriptHasMembers.keys();
		while (enums.hasMoreElements()) {
			String current = enums.nextElement();
			if (hashtableScriptHasMembers.get(current) != null
					&& hashtableScriptHasMembers.get(current).booleanValue())
				this.comboItems.add(current);
		}
		java.util.Collections.sort(this.comboItems);
	}

	/**
	 * Convert a Unicode Character decimal point to a String character
	 * 
	 * @param codePoint
	 *            The decimal codePoint to be converted
	 * @return The Unicode String to be addressed
	 */
	protected static String newString(final int codePoint) {
		if (Character.charCount(codePoint) == 1)
			return String.valueOf((char) codePoint);
		else
			return new String(Character.toChars(codePoint));
	}

	/**
	 * Change the active charset to the one with the selected name
	 * 
	 * @param newCharSet
	 *            The new Charset name, e.g. "MUSICAL_SYMBOLS"
	 * @param isBlock
	 *            The given charset is a block or a script
	 */
	public void changeCharacterSet(final String newCharSet, final boolean isBlock) {
		this.comboContrib.setComboContribSelection(newCharSet);
		unicodeViewer.changeViewerCharset(newCharSet, isBlock);
	}

	/**
	 * Change the active charset to the one with the selected name
	 * 
	 * @param customChars
	 *            An array with the int representations of the custom symbols
	 */
	public void changeCharacterSet(final String[] customChars) {
		unicodeViewer.changeViewerCharset(customChars);
	}

	/**
	 * Flip the Toggle state on both buttons block and script
	 */
	public void toggleButtons() {
		// set all checked states to false
		IContributionItem[] items = getViewSite().getActionBars()
				.getMenuManager().getItems();
		for (IContributionItem contribItem : items)
			if (contribItem instanceof ActionContributionItem) {
				Action action = (Action) ((ActionContributionItem) contribItem)
						.getAction();
				action.setChecked(false);
			}
		buttonToggleModeLeft.getAction().setChecked(false);
		buttonToggleModeRight.getAction().setChecked(false);
		comboContrib.getComboBox().setEnabled(false);

		// check and enable the specifically active controls
		switch (toggleState) {
		case TOGGLE_LEFT:
			buttonToggleModeLeft.getAction().setChecked(true);
			comboContrib.getComboBox().setEnabled(true);
			break;

		case TOGGLE_RIGHT:
			buttonToggleModeRight.getAction().setChecked(true);
			comboContrib.getComboBox().setEnabled(true);
			break;

		case TOGGLE_CUSTOM_LEFT:
			if (customActionLeft != null)
				customActionLeft.setChecked(true);
			break;

		case TOGGLE_CUSTOM_RIGHT:
			if (customActionRight != null)
				customActionRight.setChecked(true);
			break;

		default:
			// select nothing
			break;
		}
	}

	/**
	 * This is a callback that will allow us to create the unicodeViewer and
	 * initialize it. It contains the GUI elements for the toolbar menu. It
	 * contains the GUI elements for the toolbar menu.
	 */
	@Override
	public void createPartControl(final Composite parent) {
		unicodeViewer = new UnicodeViewer(parent, this);
		table = unicodeViewer.getTable();
		table.setVisible(true);
		toolBarManager = getViewSite().getActionBars().getToolBarManager();

		Action selectFontAction = new Action() {

			@Override
			public void run() {
				FontDialog dialog = new FontDialog(parent.getShell(), SWT.NONE);
				dialog.setText("Unicode Table Font");
				FontData fontData = dialog.open();
				if (fontData != null)
					unicodeViewer.setCurrentFontName(fontData.getName());

			}

		};
		selectFontAction.setDescription("Select font ...");
		selectFontAction.setImageDescriptor(ImageDescriptor
				.createFromURL(CustomCharsetResourceReader.findResource(
						"icons", "preferences-desktop-font.png"))); //$NON-NLS-1$//$NON-NLS-2$

		toolBarManager.add(new ActionContributionItem(selectFontAction));

		// insert two-part toggle switchbutton, initialized on "Block"
		setupToggleButtonLeft();
		toolBarManager.add(buttonToggleModeLeft);
		setupToggleButtonRight();
		toolBarManager.add(buttonToggleModeRight);
		setToggleState(TOGGLE_LEFT);

		// insert the combo box for block/script selection
		comboContrib = new ComboContributionItem(getBlockVector(), this);
		toolBarManager.add(comboContrib);
		toolBarManager.add(new Separator());
		createCustomCharsetItems(true);

		// Initialize the view with often used data
		changeCharacterSet(getBlockVector().get(95), true);
	}

	/**
	 * Create and insert the ToolbarItems for the Custom Charsets
	 */
	public void createCustomCharsetItems(final boolean isFirstRun) {
		// create and fill the expandable menu
		final IMenuManager menuManager = getViewSite().getActionBars()
				.getMenuManager();
		menuManager.removeAll();
		// create an image for the actions
		ImageDescriptor genericImage = new ImageDescriptor() {
			@Override
			public ImageData getImageData() {
				return ImageDescriptor.createFromURL(
						CustomCharsetResourceReader.findResource(
								"icons", "image-missing.png")).getImageData();//$NON-NLS-1$
			}
		};

		CustomCharsetResourceReader.loadCustomCharsetsFromFile();
		Vector<CustomCharsetData> dataVector = CustomCharsetResourceReader
				.getDataVector();

		// parse characters from file
		CustomCharsetData mathematicalSymbolsData = new CustomCharsetData();
		mathematicalSymbolsData.name = "Mathematical Symbols";
		mathematicalSymbolsData.unicodeBlockset = "MATHEMATICAL_OPERATORS";
		mathematicalSymbolsData.isBlock = true;
		mathematicalSymbolsData.imageLocation = "$Bundle$icons/character-sum.png";
		dataVector.insertElementAt(mathematicalSymbolsData, 0);

		CustomCharsetData musicalSymbolsData = new CustomCharsetData();
		musicalSymbolsData.name = "Musical Symbols";
		musicalSymbolsData.unicodeBlockset = "MUSICAL_SYMBOLS";
		musicalSymbolsData.font = "Euterpe";
		musicalSymbolsData.isBlock = true;
		musicalSymbolsData.imageLocation = "$Bundle$icons/character-note.png";
		dataVector.insertElementAt(musicalSymbolsData, 1);

		for (int iterator = 0; iterator < dataVector.size(); iterator++) {
			final CustomCharsetData charset = dataVector.get(iterator);
			Action charsetAction;
			// iteratorPlusThree maps toggle_custom_left to the first iteration,
			// toggle_custom_right to the second and none to all others
			final int iteratorPlusThree = iterator + 3;
			if (null == charset.getCustomCharsetTable())
				charsetAction = new Action(charset.getName(), SWT.TOGGLE) {
					@Override
					public void run() {
						unicodeViewer.changeViewerCharset(charset
								.getUnicodeBlockset(), charset.isIsBlock());
						unicodeViewer.setCurrentFontName(charset.getFont());
						setToggleState(iteratorPlusThree);
						toggleButtons();
						setActionChecked(this);
					}
				};
			else
				charsetAction = new Action(charset.getName(), SWT.TOGGLE) {
					@Override
					public void run() {
						// single chars
						int arraySize = charset.getCustomCharsetTable().customCharsetTableItem
								.size();
						// char combinations
						int arraySizeCombined = 0;
						if (null != charset.getCustomCharsetTable().customCharsetTableCombinedItem)
							arraySizeCombined = charset.getCustomCharsetTable().customCharsetTableCombinedItem
									.size();
						String[] charsetArrayLong = new String[arraySize
								+ arraySizeCombined];
						for (int i = 0; i < arraySize; i++)
							charsetArrayLong[i] = UnicodeModel
									.newString((int) charset
											.getCustomCharsetTable().customCharsetTableItem
											.get(i).point);
						for (int i = 0; i < arraySizeCombined; i++) {
							String content = "";
							CustomCharsetTableCombinedItem customCharsetTableCombinedItem = charset
									.getCustomCharsetTable().customCharsetTableCombinedItem
									.get(i);
							for (int j = 0; j < customCharsetTableCombinedItem
									.getCustomCharsetTableItem().size(); j++)
								content += UnicodeModel
										.newString((int) customCharsetTableCombinedItem
												.getCustomCharsetTableItem()
												.get(j).point);
							charsetArrayLong[i + arraySize] = content;
						}
						unicodeViewer.changeViewerCharset(charsetArrayLong);
						unicodeViewer.setCurrentFontName(charset.getFont());
						setToggleState(iteratorPlusThree);
						toggleButtons();
						setActionChecked(this);
					}
				};
			charsetAction.setToolTipText(charset.getName());
			// icon image
			if (charset.getImageLocation() != null
					&& charset.getImageLocation().length() > 0)
				charsetAction.setImageDescriptor(new ImageDescriptor() {
					@Override
					public ImageData getImageData() {
						if (charset.getName() == "Mathematical Symbols")
							return ImageDescriptor
									.createFromURL(
											CustomCharsetResourceReader
													.findResource(
															"icons", "character-sum.png")).getImageData();//$NON-NLS-1$
						else if (charset.getName() == "Musical Symbols")
							return ImageDescriptor
									.createFromURL(
											CustomCharsetResourceReader
													.findResource(
															"icons", "character-note.png")).getImageData();//$NON-NLS-1$
						else {
							String imagePath = charset.getImageLocation();
							File testFile = new File(imagePath);
							if (testFile.exists())
								return ImageDescriptor
										.createFromURL(
												CustomCharsetResourceReader
														.validateImageLocationPath(charset
																.getImageLocation()))
										.getImageData();
							else
								return ImageDescriptor
										.createFromURL(
												CustomCharsetResourceReader
														.findResource(
																"icons", "image-missing.png")).getImageData();//$NON-NLS-1$
						}
					}
				});
			else
				charsetAction.setImageDescriptor(genericImage);
			menuManager.add(charsetAction);
			// add the first two entries to the toolbar list
			if (iterator < 2 && isFirstRun) {
				toolBarManager.add(charsetAction);
				if (iterator == 0)
					customActionLeft = charsetAction;
				else
					customActionRight = charsetAction;
			}
		}
		menuManager.add(new Separator());
		Action customCharsetEditor = new Action(Messages.UnicodeView_11) {
			@Override
			public void run() {
				// insert the custom charset configurator view
				CustomCharsetDialog customCharsetDialog = new
				 CustomCharsetDialog(Display.getCurrent().getActiveShell(),
				 SWT.MODELESS);
				customCharsetDialog.setUnicodeView(unicodeView);
				customCharsetDialog.open();
			}
		};
		toolBarManager.update(true);
		customCharsetEditor.setEnabled(true);
		// create an image for the custom charset editor
		ImageDescriptor customCharsetEditorImage = new ImageDescriptor() {
			@Override
			public ImageData getImageData() {
				return ImageDescriptor.createFromURL(
						CustomCharsetResourceReader.findResource(
								"icons", "list-add.png")).getImageData();//$NON-NLS-1$
			}
		};
		customCharsetEditor.setImageDescriptor(customCharsetEditorImage);
		// customCharsetEditor.setEnabled(false);
		menuManager.add(customCharsetEditor);
	}

	/**
	 * Sets the check mark of the given Action to enabled, used in the menu
	 * selection
	 * 
	 * @param action
	 *            The Action that should be marked as checked
	 */
	private void setActionChecked(final Action action) {
		action.setChecked(true);
	}

	/**
	 * Simple shortcut to insert a single String into the editor
	 * 
	 * @param toBeInserted
	 *            The String that should be inserted
	 */
	public boolean insertIntoEditor(final TableItem item, final int i) {
		boolean result = insertIntoEditor(item.getText(i));

		/* save this in the ArrayList */
		if (!list.contains(item.getText(i)))
			if (list.size() < 16)
				list.add(item.getText(i));
			else {
				list.remove(0);
				list.add(item.getText(i));
			}
		return result;
	}

	/**
	 * Simple container class for encapsulating the insert action for undo /
	 * redo into a regular text editor
	 * 
	 * @author julian
	 * 
	 */
	public class InsertSymbolIntoTextEditorAction extends AbstractOperation {

		private final String insertString;
		private final ITextEditor editor;

		public InsertSymbolIntoTextEditorAction(final String label,
				final String insertString, final ITextEditor editor) {
			super(label);
			this.editor = editor;
			this.insertString = insertString;
		}

		@Override
		public IStatus execute(final IProgressMonitor monitor, final IAdaptable info)
				throws ExecutionException {
			IDocumentProvider dp = editor.getDocumentProvider();
			IDocument doc = dp.getDocument(editor.getEditorInput());
			ITextSelection selection = (ITextSelection) editor
					.getSelectionProvider().getSelection();
			int offset = selection.getOffset();
			try {
				doc.replace(selection.getOffset(), selection.getLength(),
						insertString);
			} catch (BadLocationException e) {
				StreamWriterUtils.writeLogError(IStatus.WARNING,
						"could not execute insertAction", e);
			}
			editor.getSelectionProvider().setSelection(
					new TextSelection(offset + insertString.length(), 0));
			return null;
		}

		@Override
		public IStatus redo(final IProgressMonitor monitor, final IAdaptable info)
				throws ExecutionException {
			return null;
		}

		@Override
		public IStatus undo(final IProgressMonitor monitor, final IAdaptable info)
				throws ExecutionException {
			return null;
		}
	}

	/**
	 * Simple container class for encapsulating the insert action for undo /
	 * redo into a Vex editor
	 * 
	 * @author julian
	 * 
	 */
	public class InsertSymbolIntoVexEditorAction extends AbstractOperation {

		private final VexEditor editor;
		private final String insertString;

		public InsertSymbolIntoVexEditorAction(final String label,
				final String insertString, final VexEditor editor) {
			super(label);
			this.insertString = insertString;
			this.editor = editor;
		}

		@Override
		public IStatus execute(final IProgressMonitor monitor, final IAdaptable info)
				throws ExecutionException {
			VexWidget widget = editor.getVexWidget();
			widget.insertText(insertString);
			return null;
		}

		@Override
		public IStatus redo(final IProgressMonitor monitor, final IAdaptable info)
				throws ExecutionException {
			return null;
		}

		@Override
		public IStatus undo(final IProgressMonitor monitor, final IAdaptable info)
				throws ExecutionException {
			return null;
		}

	}

	/**
	 * insert the selected Character into the active editor
	 * 
	 * @param customCharsetTableItem
	 *            : the selected table customCharsetTableItem
	 * @param i
	 *            : the position in which table line the char gets inserted
	 */
	public boolean insertIntoEditor(final String insertString) {
		boolean result = false;
		// register operationHistory for undo/redo
		IOperationHistory operationHistory = OperationHistoryFactory
				.getOperationHistory();
		IEditorPart part = getViewSite().getPage().getActiveEditor();
		if (null == part)
			// no editor open -> abort
			Log.logError(Messages.UnicodeView_13);
		else
			try {
				// for VexEditor
				if (part instanceof VexEditor) {
					VexEditor editor = (VexEditor) part;
					if (null != editor) {
						IUndoableOperation insertVexOperation = new InsertSymbolIntoVexEditorAction(
								"insert", insertString, editor);
						operationHistory
								.execute(insertVexOperation, null, null);
						result = true;
					}
				} else {
					// for normal Editor
					ITextEditor editor = AdapterUtils.getAdapter(part,
							ITextEditor.class);
					if (null != editor) {
						IUndoableOperation insertTextOperation = new InsertSymbolIntoTextEditorAction(
								"insert", insertString, editor);
						operationHistory.execute(insertTextOperation, null,
								null);
						result = true;
						editor.setFocus();
					}
				}
			} catch (DocumentValidationException e) {
				Log.logError(e.toString());
			} catch (ExecutionException e) {
				StreamWriterUtils.writeLogError(IStatus.ERROR,
						"Execution exception", e);
			}
		return result;
	}

	/**
	 * Handle a 'close' event by disposing of the view
	 */
	public void handleDispose() {
		this.getSite().getPage().hideView(this);
	}

	/**
	 * Passing the focus request to the unicodeViewer's control.
	 */
	@Override
	public void setFocus() {
		unicodeViewer.getControl().setFocus();
	}

	/**
	 * Getter for the flipButton selection mode
	 * 
	 * @return The state which button is toggled on
	 */
	public int getToggleState() {
		return toggleState;
	}

	/**
	 * Setter for the ToggleState
	 * 
	 * @param toggleStateLeft
	 *            The desired ToggleState to the max of 4; 0 otherwise
	 */
	public void setToggleState(final int newToggleState) {
		if (newToggleState < 5)
			this.toggleState = newToggleState;
		else
			this.toggleState = 0;
	}

	/**
	 * Default setter for CommandService
	 * 
	 * @param commandService
	 *            The new CommandService
	 */
	public void setCommandService(final ICommandService commandService) {
		this.commandService = commandService;
	}

	/**
	 * Default getter for the CommandService
	 * 
	 * @return The current CommandService
	 */
	public ICommandService getCommandService() {
		return commandService;
	}

	/**
	 * The constructor.
	 */
	public UnicodeView() {
	}
}
