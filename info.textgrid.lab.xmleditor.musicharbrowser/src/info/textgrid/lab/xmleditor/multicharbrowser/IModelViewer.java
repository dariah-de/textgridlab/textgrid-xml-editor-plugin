package info.textgrid.lab.xmleditor.multicharbrowser;

/**
 * 
 * Generic Interface for a modelviewer
 *
 * @author <Julian Dabbert> <dabbert AT upb.de>
 */
public interface IModelViewer {
	public void updateModel(int start,int end);
	public void updateScript(String name);
	public void updateFont(String fontName);
}
