package info.textgrid.lab.xmleditor.multicharbrowser;

import java.util.Observable;
import java.util.Observer;
import java.util.Vector;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DropTarget;
import org.eclipse.swt.dnd.DropTargetEvent;
import org.eclipse.swt.dnd.DropTargetListener;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

/**
 * 
 * This class is a dialog for editing the Custom Charset Data XML File. Its
 * purpose is to give the user a simple way to create their own custom charset
 * for the specific context they are working in
 * 
 * @author <Julian Dabbert> <dabbert AT upb.de>
 */
public class CustomCharsetDialog extends Dialog implements Observer {

	/**
	 * This is the surrounding shell
	 */
	private Shell shell;

	/**
	 * This is a group containing the selected entries' Data
	 */
	private Group groupContainer;

	/**
	 * This textbox contains the current and future name of the selected data
	 * object
	 */
	private Text textName, textCustom;

	/**
	 * This button is used for control in the dialog
	 */
	private Button closeButton, radioBlock, radioCustom;

	/**
	 * This label is used for describing the given context data
	 */
	private Label textNameLabel, labelIconDrawingField, labelIcon;

	/**
	 * This combobox contains all possible blocksets if selected
	 */
	private Combo comboBlock;

	/**
	 * This is a reference to an active fileDialog for selecting resources in
	 * the file system
	 */
	private FileDialog fileSelector;

	/**
	 * The data vector containing all currently active custom charset data, with
	 * unsaved changes in memory only until written to xml on user's request
	 */
	private Vector<CustomCharsetData> dataVector;

	/**
	 * The currently selected custom charset object in the list
	 */
	private CustomCharsetData currentCharsetData;

	/**
	 * A context menu for the entries in the list
	 */
	private Menu contextMenu;

	/**
	 * This is the list containing all displayed custom charset data objects
	 */
	private List charsetList;

	/**
	 * This is a variable storing the last selected entry in the list, used for
	 * updating changes to it as soon as a new entry is selected
	 */
	private int previousSelectedListEntry;

	/**
	 * This is a central tuning point for the standard margin of visual elements
	 */
	private final int borderOffset = 10;

	/**
	 * This is a platform-dependent compensation value for graphical components
	 * (Textfield with borders)
	 */
	private int offsetCorrection = 0;

	private UnicodeView unicodeView;

	public UnicodeView getUnicodeView() {
		return unicodeView;
	}

	public void setUnicodeView(UnicodeView unicodeView) {
		this.unicodeView = unicodeView;
	}

	/**
	 * Calculates the offsetCorrection value
	 * 
	 * @return The platform-dependent offset correction value
	 */
	private void calculateOffsetCorrection() {
		// OS switchgate
		if (Platform.getOS().equals("macosx")) {
			// Mac: 0 is correct
		} else {
			if (Platform.getOS().equals("win32")) {
				offsetCorrection = 4;
			} else {
				// Linux: 0 is correct
			}
		}
	}

	/**
	 * Creates a control with the Icon
	 * 
	 * @param parent
	 *            The surrounding composite
	 * @param charsetList
	 *            The list of custom charsets
	 */
	private void createIconControl(Composite parent) {
		FormData formDataIconButton = new FormData();
		formDataIconButton.top = new FormAttachment(0, borderOffset);
		formDataIconButton.left = new FormAttachment(30, borderOffset);
		labelIconDrawingField = new Label(parent, SWT.NONE);
		String imageLocation = currentCharsetData.getImageLocation();
		// handle invalid icon paths
		Image image = new Image(null, ImageDescriptor.createFromURL(
				CustomCharsetResourceReader
						.validateImageLocationPath(imageLocation))
				.getImageData());
		labelIconDrawingField.setToolTipText(dataVector.get(
				charsetList.getSelectionIndex()).getImageLocation());
		labelIconDrawingField.setImage(image);
		labelIconDrawingField.setLayoutData(formDataIconButton);
		labelIconDrawingField.addMouseListener(new MouseListener() {

			public void mouseUp(MouseEvent e) {
			}

			/**
			 * Add a file Selection Dialog for the selection of the image
			 */
			public void mouseDown(MouseEvent e) {
				fileSelector = new FileDialog(Display.getCurrent()
						.getActiveShell());
				fileSelector.setText(Messages.CustomCharsetDialog_0);
				fileSelector.setFilterNames(new String[] {
						Messages.CustomCharsetDialog_1,
						Messages.CustomCharsetDialog_2,
						Messages.CustomCharsetDialog_3 });
				fileSelector.setFilterExtensions(new String[] { "*.png", //$NON-NLS-1$
						"*.jpg", "*.*" }); //$NON-NLS-1$ //$NON-NLS-2$
				String dialogResult = fileSelector.open();
				if (dialogResult != null) {
					labelIconDrawingField
							.setImage(new Image(null, dialogResult));
					labelIconDrawingField.setToolTipText(dialogResult);
					currentCharsetData.setImageLocation(dialogResult);
				}
			}

			public void mouseDoubleClick(MouseEvent e) {
				mouseDown(e);
			}
		});
	}

	/**
	 * Creates a label as drawing space for the selected entries icon
	 * 
	 * @param parent
	 *            The surrounding composite
	 */
	private void createLabelIcon(Composite parent) {
		FormData formDataIconLabel = new FormData();
		formDataIconLabel.top = new FormAttachment(0, borderOffset);
		formDataIconLabel.left = new FormAttachment(0, borderOffset);
		labelIcon = new Label(parent, SWT.NONE);
		labelIcon.setText(Messages.CustomCharsetDialog_7);
		labelIcon.setLayoutData(formDataIconLabel);
	}

	/**
	 * Creates the Toolbar entries in the toolbar
	 * 
	 * @param toolbar
	 *            The toolbar which should contain the new entries
	 */
	private void createToolbarEntries(ToolBar toolbar) {
		// add new entry
		ToolItem toolItemAddEntry = new ToolItem(toolbar, SWT.PUSH | SWT.BORDER);
		toolItemAddEntry.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent e) {
				createNewCharset();
			}

			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
		toolItemAddEntry.setImage(new Image(null, ImageDescriptor
				.createFromURL(
						CustomCharsetResourceReader.findResource(
								"icons", "list-add.png")).getImageData())); //$NON-NLS-1$
		toolItemAddEntry.setToolTipText(Messages.CustomCharsetDialog_9);

		// remove selected entry
		ToolItem toolItemRemoveEntry = new ToolItem(toolbar, SWT.PUSH
				| SWT.BORDER);
		toolItemRemoveEntry.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent e) {
				removeSelectedCharset();
			}

			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
		toolItemRemoveEntry.setImage(new Image(null, ImageDescriptor
				.createFromURL(
						CustomCharsetResourceReader.findResource(
								"icons", "list-remove.png")).getImageData())); //$NON-NLS-1$
		toolItemRemoveEntry.setToolTipText(Messages.CustomCharsetDialog_11);

		// insert a separator
		new ToolItem(toolbar, SWT.SEPARATOR);

		// save current set to disk
		/*
		 * ToolItem toolItemSaveEntry = new ToolItem(toolbar, SWT.PUSH |
		 * SWT.BORDER); toolItemSaveEntry.addSelectionListener(new
		 * SelectionListener() {
		 * 
		 * public void widgetSelected(SelectionEvent e) {
		 * saveChangesInCharset(); }
		 * 
		 * public void widgetDefaultSelected(SelectionEvent e) { } });
		 * toolItemSaveEntry.setImage(new Image(null, ImageDescriptor
		 * .createFromURL( CustomCharsetResourceReader.findResource( "icons",
		 * "document-save.png")).getImageData())); //$NON-NLS-1$
		 * toolItemSaveEntry.setToolTipText(Messages.CustomCharsetDialog_13);
		 */

		// refresh: load set from file
		ToolItem toolItemRefreshEntry = new ToolItem(toolbar, SWT.PUSH
				| SWT.BORDER);
		toolItemRefreshEntry.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent e) {
				refreshCharsetList();
			}

			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
		toolItemRefreshEntry.setImage(new Image(null, ImageDescriptor
				.createFromURL(
						CustomCharsetResourceReader.findResource(
								"icons", "view-refresh.png")).getImageData())); //$NON-NLS-1$
		toolItemRefreshEntry.setToolTipText(Messages.CustomCharsetDialog_15);

		// insert a separator
		new ToolItem(toolbar, SWT.SEPARATOR);

		// insert saveAs
		ToolItem toolItemSaveAsEntry = new ToolItem(toolbar, SWT.PUSH
				| SWT.BORDER);
		toolItemSaveAsEntry.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent e) {
				fileSelector = new FileDialog(Display.getCurrent()
						.getActiveShell(), SWT.SAVE);
				fileSelector.setText(Messages.CustomCharsetDialog_16);
				fileSelector.setFilterNames(new String[] {
						Messages.CustomCharsetDialog_21 });
				fileSelector
				.setFilterExtensions(new String[] { "*.xml" });
				String dialogResult = fileSelector.open();
				if (dialogResult != null) {
					String temp = CustomCharsetResourceReader.customCharsetFilePath;
					CustomCharsetResourceReader.customCharsetFilePath = dialogResult;
					saveChangesInCharset();
					CustomCharsetResourceReader.customCharsetFilePath = temp;
				}
			}

			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
		toolItemSaveAsEntry
				.setImage(new Image(
						null,
						ImageDescriptor
								.createFromURL(
										CustomCharsetResourceReader
												.findResource(
														"icons", "document-save-as.png")).getImageData())); //$NON-NLS-1$
		toolItemSaveAsEntry.setToolTipText(Messages.CustomCharsetDialog_18
				+ CustomCharsetResourceReader.bundlePath + ")"); //$NON-NLS-1$

		ToolItem toolItemLoadEntry = new ToolItem(toolbar, SWT.PUSH
				| SWT.BORDER);
		toolItemLoadEntry.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent e) {
				fileSelector = new FileDialog(Display.getCurrent()
						.getActiveShell());
				fileSelector.setText(Messages.CustomCharsetDialog_20);
				fileSelector.setFilterNames(new String[] {
						Messages.CustomCharsetDialog_21 });
				fileSelector
						.setFilterExtensions(new String[] { "*.xml"}); //$NON-NLS-1$ //$NON-NLS-2$
				String dialogResult = fileSelector.open();
				if (dialogResult != null) {
					String temp = CustomCharsetResourceReader.customCharsetFilePath;
					CustomCharsetResourceReader.customCharsetFilePath = dialogResult;
					refreshCharsetList();
					CustomCharsetResourceReader.customCharsetFilePath = temp;
				}
			}

			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
		toolItemLoadEntry.setImage(new Image(null, ImageDescriptor
				.createFromURL(
						CustomCharsetResourceReader.findResource(
								"icons", "document-open.png")).getImageData())); //$NON-NLS-1$
		toolItemLoadEntry.setToolTipText(Messages.CustomCharsetDialog_26
				+ CustomCharsetResourceReader.bundlePath + ")"); //$NON-NLS-1$
	}

	/**
	 * Create a label for the name textbox
	 * 
	 * @param parent
	 *            The underlying Composite
	 * 
	 */
	private void createTextNameLabel(Composite parent) {
		FormData formDataTextNameLabel = new FormData();
		formDataTextNameLabel.top = new FormAttachment(labelIconDrawingField,
				borderOffset);
		formDataTextNameLabel.left = new FormAttachment(0, borderOffset);
		textNameLabel = new Label(parent, SWT.NONE);
		textNameLabel.setText(Messages.CustomCharsetDialog_28);
		textNameLabel.setLayoutData(formDataTextNameLabel);
	}

	/**
	 * Create the Textbox for the Chustom Charset's Name
	 * 
	 * @param parent
	 *            The underlying Composite
	 */
	private void createTextName(Composite parent) {
		textName = new Text(parent, SWT.SINGLE | SWT.BORDER);
		FormData formDataTextName = new FormData();
		// OS switchgate
		formDataTextName.top = new FormAttachment(labelIconDrawingField,
				borderOffset - 7 + offsetCorrection);
		formDataTextName.left = new FormAttachment(30, borderOffset);
		formDataTextName.right = new FormAttachment(100, -borderOffset);
		textName.setText(currentCharsetData.getName());
		textName.setToolTipText(Messages.CustomCharsetDialog_29);
		textName.setLayoutData(formDataTextName);
	}

	/**
	 * Create the Radio Button for selecting the Blockset
	 * 
	 * @param parent
	 *            The underlying Composite
	 */
	private void createRadioBlock(Composite parent) {
		FormData formDataRadioBlock = new FormData();
		formDataRadioBlock.top = new FormAttachment(textNameLabel, borderOffset);
		formDataRadioBlock.left = new FormAttachment(0, borderOffset);
		radioBlock = new Button(parent, SWT.RADIO);
		radioBlock.setSelection(true);
		radioBlock.setText(Messages.CustomCharsetDialog_30);
		radioBlock.setToolTipText(Messages.CustomCharsetDialog_31);
		radioBlock.setLayoutData(formDataRadioBlock);
		radioBlock.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent e) {
				textCustom.setEnabled(false);
				comboBlock.setEnabled(true);
			}

			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
	}

	/**
	 * Create the Combo for the Blocksets
	 * 
	 * @param parent
	 *            The underlying Composite
	 */
	private void createComboBlock(Composite parent) {
		FormData formDataComboBlock = new FormData();
		formDataComboBlock.top = new FormAttachment(textNameLabel, borderOffset);
		formDataComboBlock.left = new FormAttachment(30, borderOffset);
		formDataComboBlock.right = new FormAttachment(100, -borderOffset);
		comboBlock = new Combo(parent, SWT.READ_ONLY);
		for (String blockEntry : UnicodeView.getBlockVector())
			comboBlock.add(blockEntry);
		comboBlock.setToolTipText(Messages.CustomCharsetDialog_32);
		comboBlock.setLayoutData(formDataComboBlock);
	}

	/**
	 * Create the Radio Button for the Selection of Custom Chars
	 * 
	 * @param parent
	 *            The underlying Composite
	 */
	private void createRadioCustom(Composite parent) {
		FormData formDataRadioCustom = new FormData();
		formDataRadioCustom.top = new FormAttachment(radioBlock,
				borderOffset + 5);
		formDataRadioCustom.left = new FormAttachment(0, borderOffset);
		radioCustom = new Button(parent, SWT.RADIO);
		radioCustom.setSelection(false);
		radioCustom.setText(Messages.CustomCharsetDialog_33);
		radioCustom.setToolTipText(Messages.CustomCharsetDialog_34);
		radioCustom.setLayoutData(formDataRadioCustom);
		radioCustom.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent e) {
				textCustom.setEnabled(true);
				comboBlock.setEnabled(false);
			}

			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
	}

	/**
	 * Create the Custom Text Box
	 * 
	 * @param parent
	 *            The underlying Composite
	 */
	private void createTextCustom(Composite parent) {
		textCustom = new Text(parent, SWT.SINGLE | SWT.BORDER);
		FormData formDataTextCustom = new FormData();
		// OS switchgate
		formDataTextCustom.top = new FormAttachment(radioBlock, borderOffset
				+ offsetCorrection);
		formDataTextCustom.left = new FormAttachment(30, borderOffset);
		formDataTextCustom.right = new FormAttachment(100, -borderOffset);
		textCustom.setToolTipText(Messages.CustomCharsetDialog_35);
		textCustom.setLayoutData(formDataTextCustom);
		DropTarget dt = new DropTarget(textCustom, DND.DROP_COPY
				| DND.DROP_DEFAULT | DND.DROP_MOVE);
		dt.setTransfer(new Transfer[] { TextTransfer.getInstance() });
		dt.addDropListener(new DropTargetListener() {

			public void dropAccept(DropTargetEvent event) {
			}

			public void drop(DropTargetEvent event) {
			}

			public void dragOver(DropTargetEvent event) {
				event.feedback = DND.FEEDBACK_SELECT | DND.FEEDBACK_SCROLL;
			}

			public void dragOperationChanged(DropTargetEvent event) {
			}

			public void dragLeave(DropTargetEvent event) {
			}

			public void dragEnter(DropTargetEvent event) {
				if (event.detail == DND.DROP_DEFAULT) {
					event.detail = (event.operations & DND.DROP_COPY) != 0 ? DND.DROP_COPY
							: DND.DROP_NONE;
				}
				for (int i = 0, n = event.dataTypes.length; i < n; i++) {
					if (TextTransfer.getInstance().isSupportedType(
							event.dataTypes[i])) {
						event.currentDataType = event.dataTypes[i];
					}
				}
			}
		});
	}

	/**
	 * Create the close button
	 * 
	 * @param parent
	 *            The underlying Composite
	 */
	private void createCloseButton(Composite parent) {
		FormData formDataCloseButton = new FormData();
		formDataCloseButton.right = new FormAttachment(100, -borderOffset);
		formDataCloseButton.bottom = new FormAttachment(100, -borderOffset);
		closeButton = new Button(parent, SWT.PUSH);
		parent.getShell().setDefaultButton(closeButton);
		closeButton.setText(Messages.CustomCharsetDialog_36);
		closeButton.setToolTipText(Messages.CustomCharsetDialog_37);
		closeButton.addSelectionListener(new SelectionAdapter() {
			/**
			 * Close the view, i.e. dispose of the active shell
			 */
			@Override
			public void widgetSelected(SelectionEvent e) {
				try {
					saveChangesInCharset();
					closeDialog();
				} catch (Throwable e1) {
					StreamWriterUtils.writeLogError(IStatus.ERROR, e1.getMessage(),
							e1);
				}
			}
		});
		closeButton.setLayoutData(formDataCloseButton);
	}

	/**
	 * Handles the event of a change in the List Selection
	 */
	private void handleListSelection() {
		if (null != currentCharsetData) {
			// write back the modificatons to the last opened CustomCharsetData
			// object
			if (previousSelectedListEntry > dataVector.size()) {
				previousSelectedListEntry = -1;
			}
			if (previousSelectedListEntry >= 0) {
				dataVector.set(previousSelectedListEntry, currentCharsetData);
			}
			previousSelectedListEntry = charsetList.getSelectionIndex();
			// open the currently selected object for presentation and
			// manipulation
			currentCharsetData = dataVector.get(previousSelectedListEntry);
			charsetList.setItem(charsetList.getSelectionIndex(),
					currentCharsetData.getName());
			textName.setText(currentCharsetData.getName());
			// The location of the image for use with the currentCharsetData
			// object
			// is actually stored in the tooltip
			labelIconDrawingField
					.setImage(new Image(
							null,
							ImageDescriptor
									.createFromURL(
											CustomCharsetResourceReader
													.validateImageLocationPath(currentCharsetData
															.getImageLocation()))
									.getImageData()));
			labelIconDrawingField.setToolTipText(currentCharsetData
					.getImageLocation());
			if (currentCharsetData.isIsBlock()) {
				radioBlock.setSelection(true);
				radioCustom.setSelection(false);
				comboBlock.setEnabled(true);
				textCustom.setEnabled(false);
				for (int i = 0; i < comboBlock.getItemCount(); i++) {
					if (comboBlock.getItem(i).equals(
							currentCharsetData.getUnicodeBlockset())) {
						comboBlock.select(i);
						textCustom.setText(""); //$NON-NLS-1$
						break;
					}
				}
			} else {
				String entryString = ""; //$NON-NLS-1$
				for (CustomCharsetTableItem entryInt : currentCharsetData
						.getCustomCharsetTable().getCustomCharsetTableItem()) {
					entryString += Integer.toHexString((int) entryInt.point)
							+ ";"; //$NON-NLS-1$
				}
				for (CustomCharsetTableCombinedItem entryCombination : currentCharsetData
						.getCustomCharsetTable()
						.getCustomCharsetTableCombinedItem()) {
					for (CustomCharsetTableItem entryInt : entryCombination
							.getCustomCharsetTableItem()) {
						entryString += Integer
								.toHexString((int) entryInt.point)
								+ "&"; //$NON-NLS-1$
					}
					entryString += ";";
				}
				textCustom.setText(entryString);
				radioBlock.setSelection(false);
				radioCustom.setSelection(true);
				comboBlock.setEnabled(false);
				comboBlock.setText(""); //$NON-NLS-1$
				textCustom.setEnabled(true);
			}
		}
	}

	/**
	 * Saves all current CustomCharsetData objects to xml file
	 */
	private void saveChangesInCharset() {
		// save single charset
		writeChangesToCustomCharsetData();
		dataVector.set(charsetList.getSelectionIndex(), currentCharsetData);
		// serialize the current setup
		CustomCharsetResourceReader.writeCustomCharsetsFile(dataVector);
		charsetList.setItem(charsetList.getSelectionIndex(), currentCharsetData
				.getName());
		refreshCharsetList();
	}

	/**
	 * Writes the made modifications to the object in the memory
	 */
	private void writeChangesToCustomCharsetData() {
		currentCharsetData.setName(textName.getText());
		charsetList
				.setItem(charsetList.getSelectionIndex(), textName.getText());
		String imageLocationString = labelIconDrawingField.getToolTipText();
		if (imageLocationString
				.contains(CustomCharsetResourceReader.bundlePath))
			imageLocationString = imageLocationString.replace(
					CustomCharsetResourceReader.bundlePath, "$Bundle$"); //$NON-NLS-1$
		currentCharsetData.setImageLocation(imageLocationString);
		currentCharsetData.setIsBlock(radioBlock.getSelection());
		if (radioBlock.getSelection()) {
			currentCharsetData.setUnicodeBlockset(comboBlock.getItem(
					comboBlock.getSelectionIndex()).toString());
		} else {
			CustomCharsetTable customCharsetTable = new CustomCharsetTable();
			String[] customCharArrayString = textCustom.getText().split(";"); //$NON-NLS-1$
			String currentSymbolString;
			for (int i = 0; i < customCharArrayString.length; i++) {
				if (customCharArrayString[i] != "") {
					String[] currentSymbolArray = customCharArrayString[i]
							.trim().split("&");
					if (currentSymbolArray.length <= 1) {
						// only a single item in the cell
						CustomCharsetTableItem myItem = new CustomCharsetTableItem();
						currentSymbolString = currentSymbolArray[0];
						int parsedInt = -1;

						try {
							parsedInt = Integer.parseInt(currentSymbolString,
									16);
						} catch (NumberFormatException e) {
							// string is no hex-string, but a symbol
							parsedInt = Integer.parseInt(Integer
									.toHexString((int) currentSymbolString
											.codePointAt(0)), 16);
						}

						myItem = insertSymbolIntoTableItem(myItem, ""
								+ parsedInt);
						customCharsetTable.getCustomCharsetTableItem().add(
								myItem);

					} else {
						// a set of characters that belong together
						CustomCharsetTableCombinedItem myCombinedItem = new CustomCharsetTableCombinedItem();
						for (String currentSymbol : currentSymbolArray) {
							CustomCharsetTableItem myItem = new CustomCharsetTableItem();
							myItem = insertSymbolIntoTableItem(myItem, ""
									+ Integer.parseInt(currentSymbol, 16));
							myCombinedItem.getCustomCharsetTableItem().add(
									myItem);
						}
						customCharsetTable.getCustomCharsetTableCombinedItem()
								.add(myCombinedItem);
					}

				}
			}
			currentCharsetData.setCustomCharsetTable(customCharsetTable);
		}
	}

	/**
	 * @param myItem
	 * @param currentSymbol
	 * @return
	 */
	private CustomCharsetTableItem insertSymbolIntoTableItem(
			CustomCharsetTableItem myItem, String currentSymbol) {
		try {
			int point = -1;
			// is it formatted like (U+345435)? ->
			if (currentSymbol.trim().startsWith("U+")
					|| currentSymbol.trim().startsWith("/u")) {
				currentSymbol = currentSymbol.trim().substring(2);
				point = Integer.parseInt(currentSymbol.trim(), 16);
			} else {
				point = Integer.parseInt(currentSymbol.trim());
			}
			if (point >= 0) {
				myItem.setPoint(point);
			}
		} catch (NumberFormatException e) {
			// String is no number, but instead a String
			// containing the special char
			myItem.setPoint(currentSymbol.trim().codePointAt(0));
		}
		return myItem;
	}

	/**
	 * creates a new empty charset
	 */
	private void createNewCharset() {
		// create empty new Charset
		CustomCharsetData sample = new CustomCharsetData();
		sample.setName(Messages.CustomCharsetDialog_49);
		sample.setIsBlock(true);
		sample.setImageLocation("");
		sample.setUnicodeBlockset("MUSICAL_SYMBOLS"); //$NON-NLS-1$
		dataVector.add(sample);
		charsetList.add(sample.getName());
		charsetList.select(charsetList.getItemCount() - 1);
		try {
			writeChangesToCustomCharsetData();
		} catch (NullPointerException e) {
			StreamWriterUtils.writeLogError(IStatus.ERROR, e.toString(), e);
		}
		handleListSelection();
	}

	/**
	 * Deletes the currently selected charset
	 */
	private void removeSelectedCharset() {
		// remove the selected Charset
		int lastSelectionIndex = charsetList.getSelectionIndex();
		for (int i = lastSelectionIndex; i < dataVector.size() - 1; i++)
			dataVector.setElementAt(dataVector.get(i + 1), i);
		dataVector.remove(dataVector.size() - 1);
		charsetList.remove(lastSelectionIndex);
		lastSelectionIndex = Math.max(0, lastSelectionIndex - 1);
		charsetList.select(lastSelectionIndex);
		// set to invalid
		previousSelectedListEntry = -1;
		writeChangesToCustomCharsetData();
		handleListSelection();
	}

	/**
	 * Refreshes the view
	 */
	private void refreshCharsetList() {
		// update the current view of charsets
		charsetList.removeAll();
		CustomCharsetResourceReader.loadCustomCharsetsFromFile();
		dataVector = CustomCharsetResourceReader.getDataVector();
		for (CustomCharsetData dataEntry : dataVector)
			charsetList.add(dataEntry.getName());
		charsetList.select(0);
		handleListSelection();
	}

	/**
	 * Method inherited from Dialog. This will create and initalize GUI. Here it
	 * creates the central custom charset list and generates the additional
	 * controls via method calls
	 * 
	 * @param parent
	 *            Composite widget.
	 */
	public Control createDialogArea(Composite parent) {
		charsetList = new List(parent, SWT.BORDER | SWT.SINGLE | SWT.V_SCROLL);
		// initialize the List
		CustomCharsetResourceReader.loadCustomCharsetsFromFile();
		dataVector = CustomCharsetResourceReader.getDataVector();
		if (dataVector.size() == 0) {
			createNewCharset();
		}
		FormLayout layoutList = new FormLayout();
		parent.setLayout(layoutList);
		FormData formDataList = new FormData();
		formDataList.top = new FormAttachment(0, borderOffset);
		formDataList.left = new FormAttachment(0, borderOffset);
		formDataList.right = new FormAttachment(100, -borderOffset);
		formDataList.bottom = new FormAttachment(100, -(borderOffset + 200));

		// Read the custom charset Items into the charsetList
		for (CustomCharsetData dataEntry : dataVector)
			charsetList.add(dataEntry.getName());
		previousSelectedListEntry = -1;
		charsetList.select(0);
		currentCharsetData = dataVector.get(0);
		charsetList.addMouseListener(new MouseListener() {

			public void mouseUp(MouseEvent e) {
			}

			public void mouseDown(MouseEvent e) {
				// 1 == MouseLeft, 3 == MouseRight
				if (e.button == 1)
					// set selection
					writeChangesToCustomCharsetData();
				handleListSelection();
				if (e.button == 3) {
					// Insert Context Menu
					contextMenu.setLocation(Display.getCurrent()
							.getCursorLocation());
					contextMenu.setVisible(true);
				}
			}

			public void mouseDoubleClick(MouseEvent e) {
				mouseDown(e);
			}
		});
		charsetList.setLayoutData(formDataList);

		FormData formDataToolbar = new FormData();
		formDataToolbar.top = new FormAttachment(charsetList, 0);
		formDataToolbar.left = new FormAttachment(0, borderOffset);
		ToolBar toolbar = new ToolBar(parent, SWT.FLAT);
		createToolbarEntries(toolbar);
		toolbar.setLayoutData(formDataToolbar);
		toolbar.pack();

		FormData formDataGroup = new FormData();
		formDataGroup.top = new FormAttachment(toolbar, 0);
		formDataGroup.left = new FormAttachment(0, borderOffset);
		formDataGroup.right = new FormAttachment(100, -borderOffset);
		formDataGroup.bottom = new FormAttachment(100, -borderOffset * 5);
		groupContainer = new Group(parent, SWT.NONE);
		groupContainer.setText(Messages.CustomCharsetDialog_52);
		groupContainer.setLayoutData(formDataGroup);
		groupContainer.setLayout(new FormLayout());

		calculateOffsetCorrection();
		createLabelIcon(groupContainer);
		createIconControl(groupContainer);
		createTextNameLabel(groupContainer);
		createTextName(groupContainer);
		createRadioBlock(groupContainer);
		createComboBlock(groupContainer);
		createRadioCustom(groupContainer);
		createTextCustom(groupContainer);
		contextMenu = new Menu(parent);
		MenuItem menuItemRemove = new MenuItem(contextMenu, SWT.PUSH);
		menuItemRemove.setText(Messages.CustomCharsetDialog_53);
		menuItemRemove.setImage(new Image(null, ImageDescriptor.createFromURL(
				CustomCharsetResourceReader.findResource(
						"icons", "list-remove.png")).getImageData()));//$NON-NLS-1$
		menuItemRemove.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent e) {
				removeSelectedCharset();
			}

			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
		createCloseButton(parent);

		// initialize the data on the selected customCharsetTableItem 0
		handleListSelection();
		return parent;
	}

	/**
	 * Show the dialog.
	 */
	public Object open() {
		Shell parent = this.getParent();

		if (shell == null) {
			shell = new Shell(parent, SWT.TITLE | SWT.DIALOG_TRIM);
		}
		shell.setText(Messages.CustomCharsetDialog_55);

		createDialogArea(shell);
		shell.setSize(380, 480);
		shell.open();

		Display display = parent.getDisplay();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}

		return this;
	}

	/**
	 * Closes the current dialog window
	 */
	public void closeDialog() {
		getUnicodeView().createCustomCharsetItems(false);
		Display.getCurrent().getActiveShell().dispose();
	}

	/**
	 * inherited update method, not evaluated
	 */
	public void update(Observable o, Object arg) {

	}

	/**
	 * Constructor for this Dialog
	 * 
	 * @param parent
	 *            The underlying Base Shell
	 * @param style
	 *            The Style for this Dialog
	 */
	public CustomCharsetDialog(Shell parent, int style) {
		super(parent, style);
	}
}
