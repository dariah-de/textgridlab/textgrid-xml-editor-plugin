/**
 * 
 * This package contains the Unicode Character Table, a browser for inserting 
 * Unicode Characters into an open Text or XML document. 
 * The user can select any set from all given Unicode Blocks or Unicode Scripts
 * for inserting a character into the opened document.
 * Additional support is given via an editor for defining custom charsets, which
 * consist of user-specified characters from any unicode position.
 * The targeted core audience are music scientists that need to embed musical 
 * symbols into their TEI documents, which is why the TrueTypeFont MusicalSymbolsTextGrid is used
 * as default font for all symbols. Nevertheless, all other audiences are supported 
 * as the host OS should select the matching font for each embedded symbol where one
 * is available.    
 * 
 * <h4>References to other XML editor / Vex plug-ins</h4> <h5>
 * {@link net.sf.vex.editor}</h5>
 * <p>
 * The vex editor is used for managing the actual insertion process in 
 * {@link info.textgrid.lab.xmleditor.multicharbrowser.UnicodeView}
 * </p>
 * 
 * @author <Julian Dabbert> <dabbert AT upb.de>
 * 
 */
package info.textgrid.lab.xmleditor.multicharbrowser;