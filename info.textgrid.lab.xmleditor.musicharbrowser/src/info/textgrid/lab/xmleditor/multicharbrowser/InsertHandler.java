package info.textgrid.lab.xmleditor.multicharbrowser;

import net.sf.vex.dom.DocumentValidationException;
import net.sf.vex.editor.VexEditor;
import net.sf.vex.swt.VexWidget;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.common.NotDefinedException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.TextSelection;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.editors.text.TextEditor;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.texteditor.IDocumentProvider;
import org.eclipse.ui.texteditor.ITextEditor;

/**
 * Handler to inser Text into the active editor
 *
 * @author <Julian Dabbert> <dabbert AT upb.de>
 */
public class InsertHandler extends AbstractHandler {

	public Object execute(ExecutionEvent arg0) throws ExecutionException {
		
		String s;
		try {
			s=arg0.getCommand().getName().substring(7);
			IEditorPart part =HandlerUtil.getActiveEditor(arg0);
			try{
				if(part instanceof VexEditor){									
				VexEditor editor = (VexEditor)part;
				VexWidget widget = editor.getVexWidget();
				widget.insertText(s);
				editor.setFocus();								
			}else{
		//for normal Editor
				ITextEditor editor = (ITextEditor)part;							
				IDocumentProvider dp = editor.getDocumentProvider();
				IDocument doc = dp.getDocument(editor.getEditorInput());
				TextSelection selection = (TextSelection) ((TextEditor)part).getSelectionProvider().getSelection();
				int offset = selection.getOffset();
				try {
					// replace selection with the result of format.
				      doc.replace(selection.getOffset(), selection.getLength(),s);
				 } catch (BadLocationException e) {
					 StreamWriterUtils.writeLogError(IStatus.ERROR, e.getMessage(), e);			    
				 }		 
				 editor.getSelectionProvider().setSelection(new TextSelection(offset+s.length(),0));
				 editor.setFocus();
			}
				}catch(DocumentValidationException e) {
				}	 

		} catch (NotDefinedException e1) {
			StreamWriterUtils.writeLogError(IStatus.ERROR, e1.getMessage(), e1);
		} 

		return null;
	}

}
