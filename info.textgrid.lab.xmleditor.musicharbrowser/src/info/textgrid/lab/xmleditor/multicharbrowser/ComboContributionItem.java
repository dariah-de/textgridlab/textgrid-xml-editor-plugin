package info.textgrid.lab.xmleditor.multicharbrowser;

import java.util.List;

import org.eclipse.jface.action.ContributionItem;
import org.eclipse.jface.action.IContributionItem;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

/**
 * 
 * This is a Helper Class for the ComboBox in the ToolBar. It embeds the
 * combobox and provides methods to fill it with elements as well as handling
 * events on it
 * 
 * @author <Julian Dabbert> <dabbert AT upb.de>
 */

public class ComboContributionItem extends ContributionItem implements
		IContributionItem {

	/**
	 * The internal combobox for the blocksets rsp the script elements
	 */
	private Combo layersCombo;

	/**
	 * A list with strings that is currently displayed in the comboBox
	 */
	private List<String> texts;

	/**
	 * The comboBox cast as a control for compatiblity reasons
	 */
	private Control comboBox;

	/**
	 * The linked unicode View, used for callback functions
	 */
	private UnicodeView unicodeView;

	/**
	 * saves the position of the last selected block for user comfort
	 */
	private int selectedBlock = 95;

	/**
	 * saves the position of the last selected script for user comfort
	 */
	private int selectedScript = 38;

	/**
	 * This is the surrounding toolitem in which the combobox is embedded
	 */
	public ToolItem item;

	/**
	 * Create an Wrapper for a ComboBox within a ToolBar
	 * 
	 * @param texts
	 *            The items for the ComboBox
	 * @param unicodeView
	 *            referenced unicodeView Object for actions
	 */
	public ComboContributionItem(List<String> texts, UnicodeView unicodeView) {
		this.texts = texts;
		this.setId("ComboContributionItem");
		this.unicodeView = unicodeView;
	}

	/**
	 * The generic fill method for only one parameter
	 * 
	 * @param parent
	 *            The surrounding unicodeView Object
	 */
	public void fill(Composite parent) {
		createLayersCombo(parent);
	}

	/**
	 * This is the filler method for the combobox into the ToolBar
	 * 
	 * @param parent
	 *            The underlying toolbar
	 * @param index
	 *            Not used
	 */
	public void fill(ToolBar parent, int index) {
		item = new ToolItem(parent, SWT.SEPARATOR);
		comboBox = createLayersCombo(parent);
		comboBox.pack();
		updateItems(this.texts);
		item.setWidth(125);
		item.setControl(comboBox);
		parent.pack(true);
		parent.layout(true, true);
	}

	/**
	 * This method creates the embedded ComboBox
	 * 
	 * @param parent
	 *            the surrounding toolbar
	 * @return The combo object
	 */
	private Control createLayersCombo(Composite parent) {
		layersCombo = new Combo(parent, SWT.READ_ONLY);
		for (int i = 0; i < texts.size(); i++) {
			layersCombo.add((String) texts.get(i));
		}
		layersCombo.select(this.selectedBlock);
		layersCombo.setLayoutData(new GridData(GridData.FILL,
				GridData.BEGINNING, true, false));
		layersCombo.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// find out whether a script or a block is selected
				if (unicodeView.getToggleState() == UnicodeView.TOGGLE_LEFT) {
					selectedBlock = layersCombo.getSelectionIndex();
					unicodeView.changeCharacterSet(layersCombo
							.getItem(layersCombo.getSelectionIndex()), true);
				} else {
					if (unicodeView.getToggleState() == UnicodeView.TOGGLE_RIGHT) {
						selectedScript = layersCombo.getSelectionIndex();
						unicodeView.changeCharacterSet(layersCombo
								.getItem(layersCombo.getSelectionIndex()),
								false);
					}
				}
			}
		});
		return layersCombo;
	}

	/**
	 * Switch between the sets Block and Script
	 * 
	 * @param texts
	 *            The new String list with which to fill the combo
	 */
	public void updateItems(List<String> texts) {
		if (null != layersCombo) {
			this.texts = texts;
			layersCombo.removeAll();
			for (String entry : texts) {
				layersCombo.add(entry);
			}
			// set the Selector to the last selected Block
			// dont forget: this method is called onswitch!
			int entryIndex = 0;
			if (unicodeView.getToggleState() != UnicodeView.TOGGLE_LEFT
					&& layersCombo.getItemCount() >= selectedBlock) {
				entryIndex = selectedBlock;
			} else {
				if (unicodeView.getToggleState() != UnicodeView.TOGGLE_RIGHT
						&& layersCombo.getItemCount() >= selectedScript) {
					entryIndex = selectedScript;
				}
			}
			setComboContribSelection(entryIndex);
		}
	}

	/**
	 * Set the selected entry in the current content
	 * 
	 * @param entryIndex
	 *            The entry of the combobox entries to be selected
	 */
	public void setComboContribSelection(int entryIndex) {
		if (unicodeView.getToggleState() != UnicodeView.TOGGLE_LEFT
				&& layersCombo.getItemCount() >= entryIndex) {
			selectedBlock = entryIndex;
			layersCombo.select(selectedBlock);
		} else {
			if (unicodeView.getToggleState() != UnicodeView.TOGGLE_RIGHT
					&& layersCombo.getItemCount() >= entryIndex) {
				selectedScript = entryIndex;
				layersCombo.select(selectedScript);
			}
		}
	}
	
	/**
	 * selects entry by string
	 * @param entryString
	 */
	public void setComboContribSelection(String entryString) {
		String[] blockArray = layersCombo.getItems();
		for(int i = 0; i < layersCombo.getItemCount(); i++) {
			if(blockArray[i].equalsIgnoreCase(entryString)) {
				setComboContribSelection(i);
				break;
			}
		}
	}

	/**
	 * This is a getter for the comboBox control
	 * 
	 * @return The comboBox cast as a control for compatiblity reasons
	 */
	public Control getComboBox() {
		return comboBox;
	}

	/**
	 * Getter for the comboBox element
	 * 
	 * @return The internal Combo element
	 */
	public Combo getLayersCombo() {
		return layersCombo;
	}
}
