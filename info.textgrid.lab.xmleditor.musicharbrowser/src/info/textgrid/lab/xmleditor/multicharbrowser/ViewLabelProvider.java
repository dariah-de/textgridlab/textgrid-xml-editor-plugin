package info.textgrid.lab.xmleditor.multicharbrowser;

import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;

/**
 * 
 * This class provides a Label to a given object
 *
 * @author <Julian Dabbert> <dabbert AT upb.de>
 */
public class ViewLabelProvider extends LabelProvider implements ITableLabelProvider {
	public String getColumnText(Object obj, int index) {
		String result = null;
		try {
			result = getText(((String[])obj)[index]);
		} catch (ArrayIndexOutOfBoundsException e) {
			result = "";
		}
		return result;
	}
	public Image getColumnImage(Object obj, int index) {
		return null;

	}

}