package info.textgrid.lab.xmleditor.multicharbrowser;

import java.text.MessageFormat;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Plugin;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.resource.FontRegistry;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableCursor;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DragSource;
import org.eclipse.swt.dnd.DragSourceEvent;
import org.eclipse.swt.dnd.DragSourceListener;
import org.eclipse.swt.dnd.RTFTransfer;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.graphics.Region;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.statushandlers.StatusManager;

import com.ibm.icu.lang.UCharacter;

/**
 * 
 * The view uses a label provider to define how model objects should be
 * presented in the view. Each view can present the same model objects using
 * different labels and icons, if needed. Alternatively, a single label provider
 * can be shared between views in order to ensure that objects of the same type
 * are presented in the same way everywhere.
 * 
 * @author <Julian Dabbert> <dabbert AT upb.de>
 */
public class UnicodeViewer {

	/**
	 * The Table containing all Unicode Characters of the chosen Set
	 */
	private Table table;

	/**
	 * This Tableviewer enables visual representation of the Table
	 */
	private TableViewer tableViewer;

	/**
	 * This is a cursor to operate on the table
	 */
	private TableCursor tableCursor;

	/**
	 * This TextBox enables the search for a specific unicode point
	 */
	private Text searchBox;

	/**
	 * This Button controls the user workflow on the Dialog
	 */
	private Button insertButton, mruButton;

	/**
	 * This label describes the use of the control on its right-hand side
	 */
	private Label labelUnicode, labelPreview, labelDescription,
			labelNameUnicode, labelNameDescription;

	/**
	 * Determines whether the currently selected Character set is a block (in
	 * contrast to a script)
	 */
	private boolean currentSetIsBlock = true;

	/**
	 * This is a set of string characters for the current unicode character
	 * table
	 */
	private String currentUnicodeSet = UCharacter.UnicodeBlock.AEGEAN_NUMBERS
			.toString();

	/**
	 * This is a Unicode Model which stores content and dimensions of a unicode
	 * charset for representation in the table
	 */
	private UnicodeModel chars = new UnicodeModel(currentUnicodeSet,
			currentSetIsBlock, 16);

	/**
	 * This is the number of columns of the table in the current size
	 */
	private final int columns = chars.getColumns();

	/**
	 * This is the currently used Unicode View, managing the unicode charset
	 * dialog window
	 */
	private static UnicodeView unicodeView;

	/**
	 * This is the currently selected table row
	 */
	private TableItem selectedTableItem = null;

	/**
	 * This is the currently selected item in the currently selected table row
	 */
	private int selectedItemColumnPos = -1;

	/**
	 * This is a placeholder for the surrounding swt composite
	 */
	private final Composite parentComposite;

	/**
	 * This indicates whether the current Character set consists of custom
	 * characters (in opposite to a block or a script)
	 */
	private boolean currentIsCustom = false;

	/**
	 * This is an array containing the decimal codes of characters when the
	 * current set is a custom character set
	 */
	private String[] currentCustomChars = null;

	/**
	 * This is a context menu for special operations on a selected table item
	 */
	private Menu menuTableItemContext;

	/**
	 * This is a menu item for copying the selected charactewr into the
	 * clipboard
	 */
	private MenuItem copyToClipboard;

	/**
	 * This is a menu containing a list of most recently used Characters
	 */
	private Menu menuMruList;

	/**
	 * This is a List containing the most recently used characters as well as
	 * their last timestamp of use
	 */
	private LinkedHashMap<String, Long> mruItemsMap;

	/**
	 * This is a drawing rectangle that surrounds the enlarged version of the
	 * currently selected unicode character for better visual detection purposes
	 */
	private Rectangle labelPreviewDrawingRectangle;

	/**
	 * This is the Formlayout for the whole dialog
	 */
	private FormLayout layout;

	/**
	 * This is a central tuning point for the standard margin of visual elements
	 */
	private final int borderOffset = 7;

	/**
	 * This String specifies the name of the currently selected font
	 */
	private String currentFontName = "MusicalSymbolsTextGrid";

	/**
	 * This is the amount of columns in the current size of the table in the
	 * UnicodeViewer, useful for resizing the table
	 */
	private static int currentColumns = 8;

	/**
	 * Add all visual Elements into the panel using dedicated methods
	 * 
	 * @param composite
	 */
	private void addChildControls(final Composite parent) {
		// initialize the layout
		layout = new FormLayout();
		parent.setLayout(layout);

		// load the necessary music font euterpe
		loadSelectedFont();

		// load the supplement symbol descriptions
		UnicodeSymbolResourceReader.readFromFile();

		// init the controls in separate functions
		createPreviewDrawingRectangle();
		setupControls(parent);
		createSearchBox(parent);
		createLabel(parent);
		createTable(parent);
		createTableViewer();
		createInsertButton(parent);
		createMruButton(parent);

		tableViewer.setContentProvider(new ViewContentProvider());
		tableViewer.setLabelProvider(new ViewLabelProvider());
		tableViewer.setInput(chars);
	}

	/**
	 * Switch between Test case (no current display existent) and real
	 * application (has existing display)
	 */
	private void createPreviewDrawingRectangle() {
		if (Display.getCurrent() != null
				&& Display.getCurrent().getActiveShell() != null)
			labelPreviewDrawingRectangle = Display.getCurrent()
					.getActiveShell().getClientArea();
		else
			labelPreviewDrawingRectangle = new Shell().getClientArea();
	}

	/**
	 * Internal class ViewContentProvider for Compatiblity reasons
	 * 
	 * @author julian
	 * 
	 */
	public class ViewContentProvider implements IStructuredContentProvider,
			IModelViewer {
		@Override
		public void inputChanged(final Viewer v, final Object oldInput, final Object newInput) {
			if (newInput != null)
				((UnicodeModel) newInput).addChangeListener(this);
			if (oldInput != null)
				((UnicodeModel) oldInput).removeChangeListener(this);
		}

		@Override
		public void dispose() {
		}

		@Override
		public Object[] getElements(final Object parent) {
			// return content of one row in the table
			return chars.getChars().toArray();
		}

		@Override
		public void updateModel(final int start, final int end) {
			chars = new UnicodeModel(start, end);
			tableViewer.setInput(chars);
		}

		@Override
		public void updateScript(final String name) {
			chars = new UnicodeModel(name, currentSetIsBlock, 16);
			tableViewer.setInput(chars);
		};

		@Override
		public void updateFont(final String fontName) {
		}
	}

	/**
	 * Creates a filter box to find the searched character
	 * 
	 * @param parent
	 */
	private void createSearchBox(final Composite parent) {
		searchBox = new Text(parent, SWT.SEARCH | SWT.ICON_SEARCH
				| SWT.ICON_CANCEL);
		final SelectionAdapter searchBoxSelectionAdapter = new SelectionAdapter() {
			@Override
			public void widgetDefaultSelected(final SelectionEvent e) {
				if (e.detail == SWT.CANCEL
						|| searchBox.getText().isEmpty()
						|| searchBox.getText().equals(
								Messages.SEARCH_BOX_INFO_TEXT))
					searchBox.setText("");
				else {
					String searchText = searchBox.getText();
					int searchStringAsHexInt = -1;
					boolean done = false;
					if (searchText.length() == 1)
						// special mode: find definition of symbol
						searchStringAsHexInt = searchText.codePointAt(0);
					else {
						Pattern p = Pattern.compile("([0-9a-f])");
						Matcher matcher = p.matcher(searchText.toLowerCase());
						String workingString = "";
						while (matcher.find())
							workingString += matcher.group();
						while (workingString.length() < 4) {
							// append leading zeroes
							workingString = "0" + workingString;
							searchBox.setText(workingString);
						}
						// select the searched character in the table
						searchStringAsHexInt = Integer.parseInt(workingString,
								16);
					}
					String suspectedBlockName = (UCharacter.UnicodeBlock
							.of(searchStringAsHexInt)).toString();
					if (suspectedBlockName.equals("LATIN_1_SUPPLEMENT")
							|| suspectedBlockName.equals(null))
						StreamWriterUtils.writeLogError(IStatus.WARNING,
								"Unicode Block unknown",
								new IllegalArgumentException("Not implemented."));
					// change the charset to the blockset containing the
					// requested character
					unicodeView.changeCharacterSet(suspectedBlockName, true);
					for (TableItem ti : table.getItems()) {
						int dataArrayLength = ((String[]) ti.getData()).length;
						for (int i = 0; i < dataArrayLength; i++)
							try {
								int firstStringCharAsInt = ti.getText(i)
										.charAt(0);
								int secondStringCharAsInt = (ti.getText()
										.length() > 1) ? ti.getText(i)
										.charAt(1) + 7116 : 0;
								// add 7116 as compensation for compound string
								if (searchStringAsHexInt == firstStringCharAsInt
										+ secondStringCharAsInt) {
									processCharacterSelection(tableCursor, ti,
											i);
									done = true;
									break;
								}
							} catch (IndexOutOfBoundsException ioube) {
								StreamWriterUtils.writeLogError(
										IStatus.WARNING, ioube.toString(),
										ioube);
								break;
							}
						if (done)
							break;
					}
				}
				// don't have to selectAll because the focus gets stolen anyway
				// searchBox.selectAll();
			}
		};
		FormData formDataSearchBox = new FormData();
		formDataSearchBox.right = new FormAttachment(100, -borderOffset);
		formDataSearchBox.left = new FormAttachment(0, borderOffset);
		formDataSearchBox.top = new FormAttachment(0, borderOffset);
		searchBox.setLayoutData(formDataSearchBox);
		searchBox.setForeground(new Color(null, 0, 0, 0));
		searchBox.setText(Messages.SEARCH_BOX_INFO_TEXT);
		searchBox.setToolTipText(Messages.SEARCH_BOX_INFO_TOOLTIP);
		searchBox.selectAll();
		searchBox.addSelectionListener(searchBoxSelectionAdapter);
	}

	/**
	 * Create and initialize the unicode character table
	 * 
	 * @param parent
	 *            : The surrounding GUI composite
	 */
	private void createTable(final Composite parent) {
		int style = SWT.SINGLE | SWT.BORDER | SWT.HIDE_SELECTION;
		table = new Table(parent, style);
		tableCursor = new TableCursor(table, SWT.CHECK);

		// format table
		FormData formDataTable = new FormData();
		formDataTable.right = new FormAttachment(100, -borderOffset);
		formDataTable.left = new FormAttachment(0, borderOffset);
		formDataTable.top = new FormAttachment(searchBox, borderOffset);// 4,
		// borderOffset);
		formDataTable.bottom = new FormAttachment(100, -140);
		table.setLayoutData(formDataTable);
		table.setHeaderVisible(false);
		table.setLinesVisible(true);

		// Euterpe for win32 Machines - "Andagii" for Mac (optional)
		//		table.setFont(new Font(null, currentFontName, 18, SWT.NONE)); //$NON-NLS-1$
		loadCurrentSelectedFont();
		// resize the row height using a MeasureItem listener
		table.addListener(SWT.MeasureItem, new Listener() {
			@Override
			public void handleEvent(final Event event) {
				// height cannot be directly set per row
				event.height = 40;
			}
		});

		// create a context menu for the cursor
		menuTableItemContext = new Menu(parent);
		copyToClipboard = new MenuItem(menuTableItemContext, SWT.MENU);
		copyToClipboard.setText("Copy to clipboard");
		copyToClipboard.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(final SelectionEvent e) {
				// insert the selected char into the clipboard
				if (null != selectedTableItem) {
					Clipboard clipBoard = new Clipboard(Display.getCurrent());
					String insertion = selectedTableItem
							.getText(selectedItemColumnPos);
					clipBoard.setContents(
							new String[] { insertion, insertion },
							new Transfer[] { TextTransfer.getInstance(),
									RTFTransfer.getInstance() });
					clipBoard.dispose();
					saveMruListItem(insertion);
				}
			}

			@Override
			public void widgetDefaultSelected(final SelectionEvent e) {
			}
		});
		tableCursor.setMenu(menuTableItemContext);
		tableCursor.addKeyListener(new KeyListener() {

			@Override
			public void keyReleased(final KeyEvent e) {
			}

			@Override
			public void keyPressed(final KeyEvent e) {
				if (e.keyCode == 13)
					if (null != selectedTableItem && selectedItemColumnPos >= 0)
						insertSelectedTableItemIntoEditor();
				if (e.keyCode == SWT.ARROW_UP || e.keyCode == SWT.ARROW_DOWN
						|| e.keyCode == SWT.ARROW_LEFT
						|| e.keyCode == SWT.ARROW_RIGHT) {
					if (!(e.keyCode == SWT.ARROW_RIGHT && tableCursor
							.getColumn() + 1 > UnicodeViewer.currentColumns)) {
						// only apply this if we are within the current bounds
						// of the table
						selectedTableItem = tableCursor.getRow();
						selectedItemColumnPos = tableCursor.getColumn();
					}
					processCharacterSelection(tableCursor, selectedTableItem,
							selectedItemColumnPos);
				}
			}
		});
		tableCursor.setForeground(Display.getCurrent().getSystemColor(
				SWT.COLOR_BLACK));
		tableCursor.setBackground(Display.getCurrent().getSystemColor(
				SWT.COLOR_LIST_SELECTION));

		// set tableColumn
		for (int i = 0; i < columns; i++) {
			TableColumn col = new TableColumn(table, SWT.CENTER, 0);
			col.setWidth(40);
		}

		// Insert a Listener for the Resizing and Redrawing of the Table
		table.addListener(SWT.Resize, new Listener() {

			@Override
			public void handleEvent(final Event event) {
				if (table.getHorizontalBar().isVisible())
					table.getHorizontalBar().setVisible(false);
				int viewPanelWidth = parentComposite.getSize().x;
				// draw up to 8 columns
				int optimalNumberOfColumns = Math.min(
						(viewPanelWidth - 81) / 40 + 1, 16);
				if (UnicodeViewer.currentColumns != optimalNumberOfColumns) {
					// the table has a changed optimal Drawing Size, create
					// it anew
					if (currentIsCustom)
						chars = new UnicodeModel(optimalNumberOfColumns,
								currentCustomChars);
					else
						chars = new UnicodeModel(currentUnicodeSet,
								currentSetIsBlock, optimalNumberOfColumns);
					// update the current amount of columns
					UnicodeViewer.currentColumns = optimalNumberOfColumns;
					tableViewer.setInput(chars);

				}
			}
		});
		table.addListener(SWT.EraseItem, new Listener() {
			@Override
			public void handleEvent(final Event event) {
				event.detail &= ~SWT.HOT;
				if ((event.detail & SWT.SELECTED) != 0) {
					GC gc = event.gc;
					Rectangle area = table.getClientArea();
					/*
					 * If you wish to paint the selection beyond the end of last
					 * column, you must change the clipping region.
					 */
					int columnCount = table.getColumnCount();
					if (event.index == columnCount - 1 || columnCount == 0) {
						int width = area.x + area.width - event.x;
						if (width > 0) {
							Region region = new Region();
							gc.getClipping(region);
							region.add(event.x, event.y, width, event.height);
							gc.setClipping(region);
							region.dispose();
						}
					}
					gc.setAdvanced(true);
					if (gc.getAdvanced())
						gc.setAlpha(127);
					Rectangle rect = event.getBounds();
					Color foreground = gc.getForeground();
					Color background = gc.getBackground();
					gc.setForeground(table.getDisplay().getSystemColor(
							SWT.COLOR_TITLE_INACTIVE_FOREGROUND));
					gc.setBackground(table.getDisplay().getSystemColor(
							SWT.COLOR_LIST_BACKGROUND));
					gc.fillGradientRectangle(0, rect.y, 500, rect.height, false);
					// restore colors for subsequent drawing
					gc.setForeground(foreground);
					gc.setBackground(background);
					event.detail &= ~SWT.SELECTED;
				}
			}
		});

		// MouseListener for user input on the table
		table.addListener(SWT.MouseDown, new Listener() {
			@Override
			public void handleEvent(final Event event) {
				// find out the user-selected table area
				Rectangle clientArea = table.getClientArea();
				Point pt = new Point(event.x, event.y);
				int index = table.getTopIndex();
				while (index < table.getItemCount()) {
					boolean visible = false;
					final TableItem item = table.getItem(index);
					// move through all columns to find the matching cell
					for (int selectedItemColumn = 0; selectedItemColumn < table
							.getColumnCount(); selectedItemColumn++) {
						Rectangle rect = item.getBounds(selectedItemColumn);
						if (rect.contains(pt)) {
							// selected cell has been found, handle the
							// selection
							processCharacterSelection(tableCursor, item,
									selectedItemColumn);
							return;
						}
						if (!visible && rect.intersects(clientArea))
							visible = true;
					}
					if (!visible)
						return;
					index++;
				}
			}
		});

		// Add Drag'n'Drop support for the character table
		DragSourceListener dsl = (new DragSourceListener() {

			@Override
			public void dragStart(final DragSourceEvent event) {
				event.data = selectedTableItem.getText(selectedItemColumnPos);
			}

			@Override
			public void dragSetData(final DragSourceEvent event) {
				event.data = selectedTableItem.getText(selectedItemColumnPos);
			}

			@Override
			public void dragFinished(final DragSourceEvent event) {
				saveMruListItem(selectedTableItem
						.getText(selectedItemColumnPos));
			}
		});
		DragSource dragSourceTable = new DragSource(table, DND.DROP_COPY);
		dragSourceTable
				.setTransfer(new Transfer[] { TextTransfer.getInstance() });
		dragSourceTable.addDragListener(dsl);
		DragSource dragSourceCursor = new DragSource(tableCursor, DND.DROP_COPY);
		dragSourceCursor.setTransfer(new Transfer[] { TextTransfer
				.getInstance() });
		dragSourceCursor.addDragListener(dsl);
	}

	/**
	 * Instantiate a tableviewer object
	 */
	private void createTableViewer() {
		tableViewer = new TableViewer(table);
	}

	/**
	 * Add the "insert" Button
	 * 
	 * @param parent
	 *            the parent composite
	 */
	private void createInsertButton(final Composite parent) {
		insertButton = new Button(parent, SWT.PUSH);
		// format insertButton
		FormData formDataInsertButton = new FormData();
		formDataInsertButton.right = new FormAttachment(100, -borderOffset);
		formDataInsertButton.bottom = new FormAttachment(100, -borderOffset);
		insertButton.setLayoutData(formDataInsertButton);
		insertButton.setText(Messages.UnicodeViewer_5);
		insertButton.setToolTipText(Messages.UnicodeViewer_6);
		// the run() method is not called when not run standalone, therefore
		// this method copying the run method
		insertButton.addSelectionListener(new SelectionAdapter() {
			// Close the view, i.e. dispose of the composite's parent
			@Override
			public void widgetSelected(final SelectionEvent e) {
				if (null != selectedTableItem && selectedItemColumnPos >= 0)
					insertSelectedTableItemIntoEditor();
			}
		});
		// make this the default button in the current shell
		// parent.getShell().setDefaultButton(insertButton);
	}

	/**
	 * Add the "most recently used" Button
	 * 
	 * @param parent
	 */
	private void createMruButton(final Composite parent) {
		mruButton = new Button(parent, SWT.PUSH);
		FormData formDataMruButton = new FormData();
		formDataMruButton.left = new FormAttachment(0, borderOffset);
		// 50,-(mruButton.getSize().x + 18));
		formDataMruButton.bottom = new FormAttachment(100, -borderOffset);
		mruButton.moveAbove(insertButton);
		mruButton.setLayoutData(formDataMruButton);
		mruButton.setText(Messages.UnicodeViewer_7);
		mruButton.setToolTipText(Messages.UnicodeViewer_8);
		mruButton.setEnabled(true);
		menuMruList = new Menu(parent);
		String[] mruStringList = Activator.getDefault().getPreferenceStore().getString("MruStringListEntries").split(","); //$NON-NLS-1$ //$NON-NLS-2$
		// mruItemsMap contains the Map with the 10 most recently used items
		mruItemsMap = new LinkedHashMap<String, Long>(10, 0.75f, true);
		for (String entry : mruStringList) {
			// the content of the List is: symbol # LastInsertionTime
			String symbol = entry.split("#")[0]; //$NON-NLS-1$
			Long time = (entry.split("#").length > 1) ? Long.parseLong(entry //$NON-NLS-1$
					.split("#")[1]) : System.currentTimeMillis(); //$NON-NLS-1$
			mruItemsMap.put(symbol, time);
		}
		fillMruMenuItems();
		// show the menu on either mouseclick above the arrowButton
		mruButton.addMouseListener(new MouseListener() {
			@Override
			public void mouseDown(final MouseEvent e) {
				menuMruList.setLocation(Display.getCurrent()
						.getCursorLocation());
				menuMruList.setVisible(true);
			}

			@Override
			public void mouseDoubleClick(final MouseEvent e) {
			}

			@Override
			public void mouseUp(final MouseEvent e) {
			}

		});
	}

	/**
	 * Builds the Context menu for the MRU Entries
	 */
	private void fillMruMenuItems() {
		menuMruList = new Menu(menuMruList.getParent());
		MenuItem menuItemMruHeader = new MenuItem(menuMruList, SWT.NONE);
		menuItemMruHeader.setText(Messages.UnicodeViewer_17);
		menuItemMruHeader.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(final SelectionEvent e) {
				clearMruListItem();
			}

			@Override
			public void widgetDefaultSelected(final SelectionEvent e) {

			}
		});
		new MenuItem(menuMruList, SWT.SEPARATOR);
		for (Object valueObject : mruItemsMap.keySet().toArray())
			if (valueObject instanceof String) {
				final String mruEntry = (String) valueObject;
				if (mruEntry.length() > 0) {
					MenuItem menuItemMruEntry = new MenuItem(menuMruList,
							SWT.NONE, 2);
					menuItemMruEntry.setText(mruEntry);
					menuItemMruEntry
							.addSelectionListener(new SelectionListener() {

								@Override
								public void widgetSelected(final SelectionEvent e) {
									insertStringIntoEditor(mruEntry);
								}

								@Override
								public void widgetDefaultSelected(
										final SelectionEvent e) {

								}
							});
				}
			}
		mruButton.setMenu(menuMruList);
	}

	/**
	 * Saves a single unicode Char as String into the Storage
	 * 
	 * @param mruListItem
	 *            The single unicode special char to be stores as mru
	 *            customCharsetTableItem
	 */
	private void saveMruListItem(final String mruListItem) {
		String storeMruContent = ""; //$NON-NLS-1$
		for (Object valueObject : mruItemsMap.keySet().toArray())
			if (valueObject instanceof String) {
				final String mruEntry = (String) valueObject;
				if (mruEntry.length() > 0) {
					storeMruContent += mruEntry + "#" //$NON-NLS-1$
							+ mruItemsMap.get(mruEntry) + ","; //$NON-NLS-1$
					mruItemsMap.put(mruEntry, System.currentTimeMillis());
				}
			}
		// update the last-used-time
		if (!mruItemsMap.containsKey(mruListItem))
			mruItemsMap.put(mruListItem, System.currentTimeMillis());
		else {
			mruItemsMap.remove(mruListItem);
			mruItemsMap.put(mruListItem, System.currentTimeMillis());
		}
		if (mruItemsMap.size() > 10) {
			// find and replace the entry whose use time is eldest
			Long smallestSoFar = System.currentTimeMillis();
			Long iter;
			for (Object obj : mruItemsMap.values().toArray())
				if (obj instanceof Long) {
					iter = (Long) obj;
					if (smallestSoFar > iter)
						smallestSoFar = iter;
				}
			Iterator<Entry<String, Long>> iterator = mruItemsMap.entrySet()
					.iterator();
			while (iterator.hasNext()) {
				Entry<String, Long> nextEntry = iterator.next();
				if (nextEntry.getValue().compareTo(smallestSoFar) == 0) {
					mruItemsMap.remove(nextEntry.getKey());
					break;
				}

			}
		}
		fillMruMenuItems();
		Activator.getDefault().getPreferenceStore().putValue("MruStringListEntries", storeMruContent);
	}

	/**
	 * This method clears the menu of all recently used characters, leaving it
	 * blank
	 */
	private void clearMruListItem() {
		Activator.getDefault().getPreferenceStore().setValue("MruStringListEntries", "");
		mruItemsMap.clear();
		fillMruMenuItems();
	}

	/**
	 * Processes a selection of a character on the table
	 * 
	 * @param cursor
	 *            The selection cursor on the table
	 * @param customCharsetTableItem
	 *            the selected TableItem
	 * @param selectedItemColumn
	 *            the column that has been selected
	 */
	private void processCharacterSelection(final TableCursor cursor,
			final TableItem item, final int selectedItemColumn) {
		if (item.getText(selectedItemColumn).length() > 0) {
			char[] cs = item.getText(selectedItemColumn).toCharArray();
			String uCharDef;
			String postfix;
			int selectedCharacter = item.getText(selectedItemColumn).charAt(0);
			if (cs.length == 1) {
				postfix = Integer.toHexString(selectedCharacter) + "(" //$NON-NLS-1$
						+ selectedCharacter + ")"; //$NON-NLS-1$
				if (selectedCharacter < 16)
					uCharDef = "U+000" + postfix; //$NON-NLS-1$
				else if (selectedCharacter < 256)
					uCharDef = "U+00" + postfix; //$NON-NLS-1$
				else if (selectedCharacter < 256 * 16)
					uCharDef = "U+0" + postfix; //$NON-NLS-1$
				else
					uCharDef = "U+" + postfix; //$NON-NLS-1$
			} else {
				cs[0] = item.getText(selectedItemColumn).charAt(0);
				cs[1] = item.getText(selectedItemColumn).charAt(1);
				uCharDef = "U+" //$NON-NLS-1$
						+ Integer.toHexString(Character.toCodePoint(cs[0],
								cs[1]))
						+ Messages.UnicodeViewer_35
						+ Integer.toHexString(cs[0])
						+ Messages.UnicodeViewer_36
						+ Integer.toHexString(cs[1]);
			}
			table.showSelection();
			// Set matching Texts for the user selection to the label
			updateSelectionLabels(cursor, item, selectedItemColumn, uCharDef,
					selectedCharacter);
			// return;
		}
	}

	/**
	 * updates the labels according to the selected character symbol
	 * 
	 * @param cursor
	 *            The character table cursor
	 * @param customCharsetTableItem
	 *            The Table Row containing the selected character
	 * @param selectedItemColumn
	 *            The Table column containing the selected character
	 * @param uCharDef
	 *            The unicode Character Definition
	 * @param selectedCharacter
	 *            The selected Character as hex representation
	 */
	private void updateSelectionLabels(final TableCursor cursor,
			final TableItem item, final int selectedItemColumn, final String uCharDef,
			final int selectedCharacter) {
		String unicodeCharId = uCharDef.substring(2,
				Math.max(6, Math.max(uCharDef.indexOf(" "), uCharDef //$NON-NLS-1$
						.indexOf("(")))).toUpperCase(); //$NON-NLS-1$
		String unicodeName = UCharacter.getExtendedName(selectedCharacter);
		if (unicodeName.contains("surrogate"))
			// the standard java method UCharacter.getExtendedName could not
			// find the proper description, so look it up in the XML file
			unicodeName = UnicodeSymbolResourceReader
					.getMusicalSymbolsHashtable().get(unicodeCharId);
		int windowWidth = getTable().getParent().getSize().x;
		// The label size should not be too long, so cut it in half when it
		// exceeds windowWidth / 12
		if (unicodeName.length() > windowWidth / 12) {
			String labelNameHalves = unicodeName.substring(0, windowWidth / 24)
					+ "..." //$NON-NLS-1$
					+ unicodeName.substring(unicodeName.length() - windowWidth
							/ 24);
			labelDescription.setText(labelNameHalves);
		} else
			labelDescription.setText(unicodeName);
		labelDescription
				.setToolTipText(Messages.UnicodeViewer_41 + unicodeName);
		labelUnicode.setText(unicodeCharId);
		labelUnicode.setToolTipText(Messages.UnicodeViewer_42 + uCharDef
				+ Messages.UnicodeViewer_43 + selectedCharacter);
		labelPreview.setText("" + item.getText(selectedItemColumn)); //$NON-NLS-1$
		labelPreview.setToolTipText(unicodeName);

		// show the selection in the table
		table.showItem(item);
		cursor.setSelection(item, selectedItemColumn);
		cursor.setVisible(true);
		cursor.setFocus();
		cursor.setToolTipText(unicodeName);

		// store the selected table customCharsetTableItem
		selectedTableItem = item;
		selectedItemColumnPos = selectedItemColumn;
	}

	/**
	 * Implements the execution logic for the buttons
	 * 
	 * @param parent
	 */
	private void setupControls(final Composite parent) {
		// prepare the hashtable
		chars.updateModel(currentUnicodeSet);
	}

	/**
	 * Return the 'close' Button
	 */
	private void createLabel(final Composite parent) {
		// format labelPreview
		labelPreview = new Label(parent, SWT.NONE);
		// using a paint listener in the local Composite disables the detach
		// ability in eclipse. Workaround: declare a class-wide member for the
		// area on the label (see top declaration of
		// labelPreviewDrawingRectangle)
		labelPreview.addPaintListener(new PaintListener() {
			@Override
			public void paintControl(final PaintEvent e) {
				GC gc = e.gc;
				gc.setForeground(Display.getCurrent().getSystemColor(
						SWT.COLOR_DARK_GRAY));
				gc.drawRectangle(labelPreviewDrawingRectangle.x,
						labelPreviewDrawingRectangle.y, 74, 84);
			}
		});
		FormData formDataLabelPreview = new FormData();
		FormData formDataLabelUnicode = new FormData();
		FormData formDataLabelDescription = new FormData();
		formDataLabelUnicode.right = new FormAttachment(labelUnicode, 50,
				SWT.RIGHT);
		formDataLabelPreview.left = new FormAttachment(0, 9);
		formDataLabelPreview.bottom = new FormAttachment(100, -47);
		formDataLabelPreview.width = 75;
		formDataLabelPreview.height = 85;
		labelPreview.setAlignment(SWT.CENTER);
		labelPreview.setLayoutData(formDataLabelPreview);
		labelPreview.setFont(new Font(null, currentFontName, 44, SWT.NONE));
		labelPreview.setBackground(Display.getCurrent().getSystemColor(
				SWT.COLOR_LIST_BACKGROUND));

		// format labelUnicode
		labelNameUnicode = new Label(parent, SWT.NONE);
		FormData formDataLabelNameUnicode = new FormData();
		formDataLabelNameUnicode.right = new FormAttachment(0, 440);
		formDataLabelNameUnicode.left = new FormAttachment(labelPreview, 82,
				SWT.LEFT);
		formDataLabelNameUnicode.bottom = new FormAttachment(100, -120);
		labelNameUnicode.setLayoutData(formDataLabelNameUnicode);
		labelNameUnicode.setText(Messages.UnicodeViewer_46);
		labelUnicode = new Label(parent, SWT.NONE);
		formDataLabelUnicode.right = new FormAttachment(0, 440);
		formDataLabelUnicode.left = new FormAttachment(labelPreview, 82,
				SWT.LEFT);
		formDataLabelUnicode.bottom = new FormAttachment(100, -100);
		labelUnicode.setLayoutData(formDataLabelUnicode);
		labelUnicode.setText(""); //$NON-NLS-1$

		// format labelDescription
		labelNameDescription = new Label(parent, SWT.NONE);
		FormData formDataLabelNameDescription = new FormData();
		formDataLabelNameDescription.right = new FormAttachment(0, 440);
		formDataLabelNameDescription.left = new FormAttachment(labelPreview,
				82, SWT.LEFT);
		formDataLabelNameDescription.bottom = new FormAttachment(100, -67);
		labelNameDescription.setLayoutData(formDataLabelNameDescription);
		labelNameDescription.setText(Messages.UnicodeViewer_48);
		labelDescription = new Label(parent, SWT.NONE);
		formDataLabelUnicode.right = new FormAttachment(0, 440);
		formDataLabelDescription.left = new FormAttachment(labelPreview, 82,
				SWT.LEFT);
		formDataLabelDescription.bottom = new FormAttachment(100, -47);
		formDataLabelDescription.width = 300;
		labelDescription.setLayoutData(formDataLabelDescription);
		labelDescription.setText(""); //$NON-NLS-1$
	}

	/**
	 * Load the MusicalSymbolsTextGrid font for musical symbols.
	 * MusicalSymbolsTextGrid is under the SIL Open Font License and may be
	 * redistributed.
	 */
	private void loadSelectedFont() {
		// load the bundle path
		try {
			// Use a neutral temp writing location for extracting the
			// three resources .ttf, unicodeSupplement.xml & CustomCharsets.xml
			if (Activator.getDefault() instanceof Plugin) {
				Plugin plugin = Activator.getDefault();
				String path = plugin.getStateLocation().toPortableString();
				String resourceDir = "Resources";
				String resourceFile = "MusicalSymbolsTextGrid.ttf";
				StreamWriterUtils.writeResourceToTempFile(path, resourceDir,
						resourceFile);
				if (!Display.getCurrent().loadFont(path + "/" + resourceFile))
					StatusManager.getManager().handle(new Status(IStatus.ERROR, Activator.PLUGIN_ID,
							"Could not load our font -- we didn't get an Exception, though ..."));
				
			}
		} catch (Exception ex) {
			StreamWriterUtils
					.writeLogError(
							IStatus.ERROR,
							"Could not resolve internal resource {0} in {1}\nThis is probably a bug, please file a bug report.",
							ex);
		}
	}

	private enum FontType {
		TABLE(18, 12, SWT.NONE), CURSOR(18, 12, SWT.BOLD), PREVIEW(44, 36,
				SWT.NONE);

		private int height;
		private int winHeight;
		private int style;

		FontType(final int height, final int winHeight, final int style) {
			this.height = height;
			this.winHeight = winHeight;
			this.style = style;
		}

		public FontData getFontData(final String name) {
			return new FontData(name,
					"win32".equals(Platform.getOS()) ? winHeight : height,
					style);
		}
	};

	private FontRegistry fontRegistry;

	private Font getFont(final FontType type) {
		if (fontRegistry == null)
			fontRegistry = new FontRegistry(getControl().getDisplay(), true);
		final String fontKey = type + "." + currentFontName;
		if (!fontRegistry.hasValueFor(fontKey)) {
			FontData fontData = type.getFontData(currentFontName);
			fontRegistry.put(fontKey, new FontData[] { fontData });
		}
		return fontRegistry.get(fontKey);
	}

	/**
	 * load the currently selected font name as default
	 */
	private void loadCurrentSelectedFont() {

		table.setFont(getFont(FontType.TABLE));
		tableCursor.setFont(getFont(FontType.CURSOR));
		labelPreview.setFont(getFont(FontType.PREVIEW));

		table.redraw();
		tableCursor.redraw();
		labelPreview.redraw();

		StatusManager.getManager().handle(new Status(IStatus.INFO, Activator.PLUGIN_ID,
				MessageFormat.format("Set font {0} (and caused a redraw)", currentFontName)));
	}

	/**
	 * Run and wait for a close event
	 * 
	 * @param shell
	 *            Instance of Shell
	 */
	private void run(final Shell shell) {
		Display display = shell.getDisplay();
		while (!shell.isDisposed())
			if (!display.readAndDispatch())
				display.sleep();
	}

	/**
	 * Change the active charset to the one with the selected name
	 * 
	 * @param newCharSet
	 *            The new Charset name, e.g. "MUSICAL_SYMBOLS"
	 */
	public void changeViewerCharset(final String newCharSet, final boolean isBlock) {
		loadCurrentSelectedFont();
		currentUnicodeSet = newCharSet;
		currentSetIsBlock = isBlock;
		currentIsCustom = false;
		tableViewer.setInput(chars);
		setupControls(this.parentComposite);
		table.getHorizontalBar().setVisible(false);
		int viewPanelWidth = parentComposite.getSize().x;
		// draw up to 8 columns
		int optimalNumberOfColumns = Math.min((viewPanelWidth - 81) / 40 + 1,
				16);
		// the table has a changed optimal Drawing Size, create it
		// anew
		chars = new UnicodeModel(currentUnicodeSet, currentSetIsBlock,
				optimalNumberOfColumns);
		tableViewer.setInput(chars);
		// update the current amount of columns
		UnicodeViewer.currentColumns = optimalNumberOfColumns;

	}

	/**
	 * Change the active charset to the one with the selected name
	 * 
	 * @param newCharSet
	 *            The new Charset name, e.g. "MUSICAL_SYMBOLS"
	 */
	public void changeViewerCharset(final String[] customChars) {
		loadCurrentSelectedFont();
		currentIsCustom = true;
		currentCustomChars = customChars;
		tableViewer.setInput(chars);
		setupControls(this.parentComposite);
		table.getHorizontalBar().setVisible(false);
		int viewPanelWidth = parentComposite.getSize().x;
		// draw up to 8 columns
		int optimalNumberOfColumns = Math.min((viewPanelWidth - 81) / 40 + 1,
				16);
		// the table has a changed optimal Drawing Size, create it
		// anew
		chars = new UnicodeModel(optimalNumberOfColumns, customChars);
		tableViewer.setInput(chars);
		// update the current amount of columns
		UnicodeViewer.currentColumns = optimalNumberOfColumns;
	}

	/**
	 * Set the current font name new, if currentFontName != null
	 * 
	 * @param currentFontName
	 *            The new font name if != null
	 */
	public void setCurrentFontName(final String currentFontName) {
		if (null != currentFontName)
			this.currentFontName = currentFontName;
		if (tableViewer != null && !tableViewer.getTable().isDisposed()) {
			this.loadCurrentSelectedFont();
			tableViewer.refresh(true);
		}
		if (labelPreview != null && !labelPreview.isDisposed())
			labelPreview.redraw();
	}

	/**
	 * Passing the focus request to the viewer's control.
	 */
	public void setFocus() {
		tableViewer.getControl().setFocus();
	}

	/**
	 * Getter for the table
	 * 
	 * @return the character table
	 */
	public Table getTable() {
		return table;
	}

	/**
	 * Return the surrounding parent composite
	 */
	public Control getControl() {
		return table.getParent();
	}

	/**
	 * The empty selectionChanged method, for compatiblity reasons only
	 * 
	 * @param part
	 *            Not used
	 * @param selection
	 *            Not used
	 */
	public void selectionChanged(final IWorkbenchPart part, final ISelection selection) {
	}

	/**
	 * Getter for the insert button
	 * 
	 * @return The insert Button for this form
	 */
	public Button getInsertButton() {
		return insertButton;
	}

	/**
	 * Close the window and dispose of resources
	 */
	public void close() {
		Shell shell = table.getShell();
		if (shell != null && !shell.isDisposed())
			shell.dispose();
	}

	/**
	 * Release resources: Tell the label provider to release its resources
	 */
	public void dispose() {
		UnicodeViewer.currentColumns = -1;
		tableViewer.getLabelProvider().dispose();
	}

	/**
	 * Insert a single char into the text file and save this char into the most
	 * recently used list
	 */
	public boolean insertSelectedTableItemIntoEditor() {
		boolean result = unicodeView.insertIntoEditor(selectedTableItem,
				selectedItemColumnPos);
		saveMruListItem(selectedTableItem.getText(selectedItemColumnPos));
		return result;
	}

	/**
	 * Insert a string into the text file and save this char into the most
	 * recently used list
	 */
	public boolean insertStringIntoEditor(final String insertString) {
		boolean result = unicodeView.insertIntoEditor(insertString);
		saveMruListItem(insertString);
		return result;
	}

	/**
	 * standalone demo shell - pointless due to lacking use case
	 * 
	 * @param args
	 */
	public static void main(final String[] args) {
		Shell shell = new Shell();
		shell.setText(Messages.UnicodeViewer_50);
		// Create a composite to hold the children
		Composite composite = new Composite(shell, SWT.NONE);
		final UnicodeViewer unicodeViewer = new UnicodeViewer(composite,
				new UnicodeView());
		unicodeViewer.getControl().addDisposeListener(new DisposeListener() {

			@Override
			public void widgetDisposed(final DisposeEvent e) {
				unicodeViewer.dispose();
			}

		});
		// Ask the shell to display its content
		shell.open();
		unicodeViewer.run(shell);
	}

	/**
	 * The constructor.
	 */
	public UnicodeViewer(final Composite parent, final UnicodeView unicodeview) {
		UnicodeViewer.unicodeView = unicodeview;
		parentComposite = parent;
		this.addChildControls(parent);
	}
}
