package info.textgrid.lab.xmleditor.multicharbrowser;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Vector;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.UnmarshalException;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Plugin;
import org.eclipse.swt.widgets.Display;
import org.xml.sax.SAXException;

/**
 * 
 * This class organizes serialization and deserialization of Custom Charset Data
 * objects from an XML profile
 * 
 * @author <Julian Dabbert> <dabbert AT upb.de>
 */
public class CustomCharsetResourceReader {

	/**
	 * This is the path of the bundle resources, used in various locations in
	 * this project
	 */
	public static String bundlePath = Activator.getDefault().getBundle()
			.getLocation(); //$NON-NLS-1$

	/**
	 * locates the resource of the specified path from within the package
	 * 
	 * @param resourceDirectory
	 *            The directory of the resource, e.g. "icons"
	 * @param requestedBundleResource
	 *            The filename, e.g."sample.png"
	 * @return The path of the resource from within the exported bundle
	 * @deprecated use {@link #findResource(String, String)} instead
	 */
	public static String findResourcePath(String resourceDirectory,
			String requestedBundleResource) {
		String path = "";
		try {
			path = FileLocator.resolve(
					FileLocator.find(Activator.getDefault().getBundle(),
							new Path(resourceDirectory)
									.append(requestedBundleResource), null))
					.getFile();
		} catch (IOException e) {
			StreamWriterUtils.writeLogError(IStatus.ERROR, e.getMessage(), e);
		}
		return path;
	}

	/**
	 * locates the resource of the specified path from within the package
	 * 
	 * @param resourceDirectory
	 *            The directory of the resource, e.g. "icons"
	 * @param requestedBundleResource
	 *            The filename, e.g."sample.png"
	 * @return The path of the resource from within the exported bundle or null,
	 *         if this fails.
	 */
	public static URL findResource(final String resourceDirectory,
			String requestedResource) {
		try {
			return FileLocator.resolve(FileLocator.find(Activator.getDefault()
					.getBundle(), new Path(resourceDirectory)
					.append(requestedResource), null));
		} catch (IOException e) {
			StreamWriterUtils
					.writeLogError(
							IStatus.ERROR,
							"Could not resolve internal resource {0} in {1}: {2}\nThis is probably a bug, please file a bug report.",
							e);
		}
		return null;
	}

	/**
	 * This is the path to the currently selected xml file - it may change as
	 * the user selects another xml profile file
	 */
	public static String customCharsetFilePath = ((Plugin) Activator
			.getDefault()).getStateLocation().toPortableString()
			+ "/CustomCharsetDataSet.xml"; //$NON-NLS-1$

	/**
	 * This is the Vector containing all the Custom Charset Data objects read
	 * from the profile file
	 */
	private static Vector<CustomCharsetData> dataVector;

	/**
	 * Allocates the file with the custom Charsets
	 */
	public static boolean loadCustomCharsetsFromFile() {
		dataVector = new Vector<CustomCharsetData>();
		File xmlFile = new File(customCharsetFilePath);
		if (!xmlFile.exists()) {
			StreamWriterUtils.writeResourceToTempFile(((Plugin) Activator
					.getDefault()).getStateLocation().toPortableString(),
					"Resources", "CustomCharsetDataSet.xml");//$NON-NLS-1$
		}
		dataVector = readCustomCharsetsFile();
		for (CustomCharsetData dataEntry : dataVector) {
			if (dataEntry.getImageLocation().contains("$Bundle$")) //$NON-NLS-1$
				dataEntry.setImageLocation(CustomCharsetResourceReader
						.findResource(
								"icons",
								dataEntry.getImageLocation().substring(
										dataEntry.getImageLocation()
												.lastIndexOf("/"))).getFile());
		}
		return true;
	}

	/**
	 * Serializes the Custom Charset away using JAXB.
	 * 
	 * @param dataVector
	 *            The Vector containing all single CustomCharsetDataSet
	 */
	public static void writeCustomCharsetsFile(
			Vector<CustomCharsetData> dataVector) {
		for (CustomCharsetData dataEntry : dataVector) {
			if (dataEntry.getImageLocation().contains(
					CustomCharsetResourceReader.bundlePath))
				dataEntry.setImageLocation(dataEntry.getImageLocation()
						.replace(CustomCharsetResourceReader.bundlePath,
								"$Bundle$")); //$NON-NLS-1$
		}
		CustomCharsetDataSet customCharsetDataSet = new CustomCharsetDataSet();
		customCharsetDataSet.customCharsetData = dataVector;
		JAXBContext jc;
		try {
			jc = JAXBContext
					.newInstance("info.textgrid.lab.xmleditor.multicharbrowser"); //$NON-NLS-1$
			Marshaller marshaller = jc.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,
					new Boolean(true));
			try {
				marshaller.marshal(customCharsetDataSet, new FileOutputStream(
						new File(customCharsetFilePath)));
			} catch (FileNotFoundException e) {
				StreamWriterUtils.writeLogError(IStatus.ERROR, e.getMessage(), e);
			}
		} catch (JAXBException e1) {
			StreamWriterUtils.writeLogError(IStatus.ERROR, e1.getMessage(), e1);
		}
	}

	/**
	 * read the custom charsets file via jaxb
	 * 
	 * @return The Container for the set of custom charsets
	 */
	public static Vector<CustomCharsetData> readCustomCharsetsFile() {
		if (Activator.getDefault() instanceof Plugin) {
			Plugin plugin = (Plugin) Activator.getDefault();
			String path = plugin.getStateLocation().toPortableString();
			String resourceDir = "Resources";
			String resourceFile = "CustomCharsetSchema.xsd";
			StreamWriterUtils.writeResourceToTempFile(path, resourceDir,
					resourceFile);
			Display.getCurrent().loadFont(path + "/" + resourceFile); //$NON-NLS-1$

			JAXBContext jc;
			try {
				jc = JAXBContext
						.newInstance("info.textgrid.lab.xmleditor.multicharbrowser"); //$NON-NLS-1$
				Unmarshaller unmarshaller;
				SchemaFactory sf = SchemaFactory
						.newInstance(javax.xml.XMLConstants.W3C_XML_SCHEMA_NS_URI);
				Schema schema = sf
						.newSchema(new File(path + "/" + resourceFile)); //$NON-NLS-1$
				unmarshaller = jc.createUnmarshaller();
				CustomCharsetDataSet instance;
				// Sometimes, in JAR environments the unmarshaller complains
				// about errors in the (valid) schema. To compensate, in these
				// cases the schema is being ignored
				try {
					unmarshaller.setSchema(schema);
					instance = (CustomCharsetDataSet) unmarshaller
							.unmarshal(new File(customCharsetFilePath));
				} catch (UnmarshalException e) {
					StreamWriterUtils.writeLogError(IStatus.WARNING,
							"Schema file collision: " + e.toString()
									+ " Compensation successful.", e);
					unmarshaller.setSchema(null);
					instance = (CustomCharsetDataSet) unmarshaller
							.unmarshal(new File(customCharsetFilePath));
				}
				Vector<CustomCharsetData> myDataVector = new Vector<CustomCharsetData>();
				for (CustomCharsetData ccd : instance.customCharsetData)
					myDataVector.add(ccd);
				return myDataVector;
			} catch (JAXBException e) {
				StreamWriterUtils.writeLogError(IStatus.ERROR, e.toString(), e);
			} catch (SAXException e) {
				StreamWriterUtils.writeLogError(IStatus.ERROR, e.toString(), e);
			}
		}
		return new Vector<CustomCharsetData>();
	}

	/**
	 * Reallocates an array with a new size, and copies the contents of the old
	 * array to the new array.
	 * 
	 * @param oldArray
	 *            the old array, to be reallocated.
	 * @param newSize
	 *            the new array size.
	 * @return A new array with the same contents.
	 */
	@SuppressWarnings("rawtypes")
	public static int[] resizeArray(int[] previousArray, int newSize) {
		int previousSize = java.lang.reflect.Array.getLength(previousArray);
		Class elementType = previousArray.getClass().getComponentType();
		Object newArray = java.lang.reflect.Array.newInstance(elementType,
				newSize);
		int preserveLength = Math.min(previousSize, newSize);
		if (preserveLength > 0)
			System.arraycopy(previousArray, 0, newArray, 0, preserveLength);
		return (int[]) newArray;
	}

	/**
	 * Getter for the data Vector
	 * 
	 * @return Vector containing all the Custom Charset Data objects read from
	 *         the profile file
	 */
	public static Vector<CustomCharsetData> getDataVector() {
		return dataVector;
	}

	/**
	 * This method checks whether the user selected valid data and replaces it
	 * in the invalid case with default icons
	 * 
	 * @param imageLocation
	 *            The path to be validated
	 * @return A valid image path
	 */
	public static URL validateImageLocationPath(String imageLocation) {
		if (imageLocation == "") //$NON-NLS-1$
			return findResource("icons", "sample.gif"); //$NON-NLS-1$
		if (imageLocation.contains("$Bundle$")) { //$NON-NLS-1$
			return findResource("icons", imageLocation.substring(imageLocation
					.lastIndexOf("/")));
		}
		if (!new File(imageLocation).exists())
			return findResource("icons", "image-missing.png"); //$NON-NLS-1$
		try {
			URL find = FileLocator.find(new URL("file:/" + imageLocation));
			if(null == find) {
				return findResource("icons", "sample.gif"); //$NON-NLS-1$
			}
			return FileLocator
					.resolve(find);
		} catch (MalformedURLException e) {
			StreamWriterUtils.writeLogError(IStatus.ERROR, e.toString(), e);
		} catch (IOException e) {
			StreamWriterUtils.writeLogError(IStatus.ERROR, e.toString(), e);
		}
		return null;
	}
}
