package info.textgrid.lab.xmleditor.multicharbrowser;

import org.eclipse.osgi.util.NLS;

/**
 * NLS Messages for the multicharbrowser
 *
 * @author <Julian Dabbert> <dabbert AT upb.de>
 * 
 */

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "info.textgrid.lab.xmleditor.multicharbrowser.messages"; //$NON-NLS-1$
	public static final String SEARCH_BOX_INFO_TOOLTIP = "Enter a Unicode Codepoint to get its Block shown in the table. Example: U+2200 or 2200";
	public static final String SEARCH_BOX_INFO_TEXT = "Type unicode point, e.g. 2200";
	public static String CustomCharsetDialog_0;
	public static String CustomCharsetDialog_1;
	public static String CustomCharsetDialog_11;
	public static String CustomCharsetDialog_13;
	public static String CustomCharsetDialog_15;
	public static String CustomCharsetDialog_16;
	public static String CustomCharsetDialog_18;
	public static String CustomCharsetDialog_2;
	public static String CustomCharsetDialog_20;
	public static String CustomCharsetDialog_21;
	public static String CustomCharsetDialog_22;
	public static String CustomCharsetDialog_26;
	public static String CustomCharsetDialog_28;
	public static String CustomCharsetDialog_29;
	public static String CustomCharsetDialog_3;
	public static String CustomCharsetDialog_30;
	public static String CustomCharsetDialog_31;
	public static String CustomCharsetDialog_32;
	public static String CustomCharsetDialog_33;
	public static String CustomCharsetDialog_34;
	public static String CustomCharsetDialog_35;
	public static String CustomCharsetDialog_36;
	public static String CustomCharsetDialog_37;
	public static String CustomCharsetDialog_49;
	public static String CustomCharsetDialog_52;
	public static String CustomCharsetDialog_53;
	public static String CustomCharsetDialog_55;
	public static String CustomCharsetDialog_7;
	public static String CustomCharsetDialog_9;
	public static String CustomCharsetResourceReader_4;
	public static String UnicodeSymbolResourceReader_0;
	public static String UnicodeSymbolResourceReader_2;
	public static String UnicodeSymbolResourceReader_3;
	public static String UnicodeView_11;
	public static String UnicodeView_13;
	public static String UnicodeView_2;
	public static String UnicodeView_6;
	public static String UnicodeViewer_17;
	public static String UnicodeViewer_35;
	public static String UnicodeViewer_36;
	public static String UnicodeViewer_41;
	public static String UnicodeViewer_42;
	public static String UnicodeViewer_43;
	public static String UnicodeViewer_46;
	public static String UnicodeViewer_48;
	public static String UnicodeViewer_5;
	public static String UnicodeViewer_50;
	public static String UnicodeViewer_6;
	public static String UnicodeViewer_7;
	public static String UnicodeViewer_8;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
