#!/bin/sh

for x in target/update-*.tar.gz; do
  SRC=$x
done

BASENAME=`basename $SRC .tar.gz`

echo "Uploading $BASENAME.tar.gz"

scp $SRC jkrasnay@vex.sourceforge.net:/home/groups/v/ve/vex

ssh jkrasnay@vex.sourceforge.net <<EOF 
  cd /home/groups/v/ve/vex 
  tar zxvf $BASENAME.tar.gz
  #rm $BASENAME.tar.gz
  rm update
  ln -s $BASENAME update
EOF


