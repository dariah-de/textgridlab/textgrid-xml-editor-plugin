<project default="help">

  <!--
        
          User-specific build settings in ~/vex-build.properties
          
          sourceforge.user      - Username on Sourceforge
          sourceforge.password  - Password on Sourceforge
          platform.cache.dir    - Local directory where copies of the Eclipse platform
                                  have been downloaded.
          proxy.host            - Proxy host for downloads.
          proxy.port            - Proxy port for downloads.
                                  
  -->
  <property file="${user.home}/vex-build.properties" />

  <!-- Feature version comes from here -->
  <xmlproperty file="feature.xml" />
  <property name="version" value="${feature(version)}"/>

  <!-- Version of the Eclipse platform on which Vex runs -->
  <property name="target.version" value="3.0.1" />

  <!-- Build number (timestamp) of the above target.version.
             This is part of the URL required to download the prebuilt Eclipse platform -->
  <property name="target.build" value="200406251208-FIX-FOR-3.0.1" />

  
  <!-- ZIP file containing the feature and all its plugins -->
  <property name="feature.zip" 
            value="${user.home}/vex-feature-${version}.zip"/>

  <tstamp />

  <target name="help" description="Help summary">
    <echo>This build file contains targets for building and
    uploading to Sourceforge the various Vex distribution files.
    Please run ant -projecthelp for more details.</echo>
  </target>
  
  <target name="confirm.built">
    <available property="build.exists" file="${feature.zip}"/>

    <fail unless="build.exists">

  Could not find ${feature.zip}.

  This file must be manually created from the Eclipse workbench
  as follows.


  - in Package Explorer, right-click and select Export...

  - choose "Deployable features" and click Next >

  - select the net.sf.vex feature

  - select "Deploy as: single ZIP file"

  - in the "File name:" box, enter ${feature.zip}

  - click finish

  If the build fails due to classes not being found, check your plugin
  dependencies. Also, if plugin B needs a JAR in plugin A, you need to
  explicitly add it to the Extra Classpath Entries under the Build tab
  in the plugin.xml for plugin B.

    </fail>

  </target>

  <target name="build.all" 
          description="Build update site and all platform exes.">
	  
    <antcall target="build.update.site" />
    <antcall target="build.all.exes" />

  </target>

  <target name="build.update.site" 
          depends="confirm.built"
	  description="Builds a ZIP file for the update site.">

    <delete dir="target/unpack" />
    <mkdir dir="target/unpack" />
    <unzip src="${feature.zip}" dest="target/unpack"/>
    
    <delete dir="target/update" />
    
    <mkdir dir="target/update/plugins" />
    <zip destfile="target/update/plugins/net.sf.vex.dita_${version}.jar"
         basedir="target/unpack/plugins/net.sf.vex.dita_${version}"/>
    <zip destfile="target/update/plugins/net.sf.vex.docbook_${version}.jar"
         basedir="target/unpack/plugins/net.sf.vex.docbook_${version}"/>
    <zip destfile="target/update/plugins/net.sf.vex.editor_${version}.jar"
         basedir="target/unpack/plugins/net.sf.vex.editor_${version}"/>
    <zip destfile="target/update/plugins/net.sf.vex.help_${version}.jar"
         basedir="target/unpack/plugins/net.sf.vex.help_${version}"/>
    <zip destfile="target/update/plugins/net.sf.vex.samples_${version}.jar"
         basedir="target/unpack/plugins/net.sf.vex.samples_${version}"/>
    <zip destfile="target/update/plugins/net.sf.vex.toolkit_${version}.jar"
         basedir="target/unpack/plugins/net.sf.vex.toolkit_${version}"/>
    <zip destfile="target/update/plugins/net.sf.vex.xhtml_${version}.jar"
         basedir="target/unpack/plugins/net.sf.vex.xhtml_${version}"/>
    <zip destfile="target/update/plugins/net.sf.vex_${version}.jar"
         basedir="target/unpack/plugins/net.sf.vex_${version}"/>
    
    <mkdir dir="target/update/features" />
    <zip destfile="target/update/features/net.sf.vex_${version}.jar"
         basedir="target/unpack/features/net.sf.vex_${version}"/>

    <echo file="target/update/site.xml"><![CDATA[<?xml version="1.0"?>
<site>
  <feature id="net.sf.vex"
           version="${version}"
           url="features/net.sf.vex_${version}.jar">
    <category name="vex"/>
  </feature>
  <category-def name="vex" label="Vex XML Editor"/>
</site>
]]>
</echo>
    
    <tar compression="gzip" destfile="target/update-${DSTAMP}-${TSTAMP}.tar.gz">
      <tarfileset dir="target/update" prefix="update-${DSTAMP}-${TSTAMP}" />
    </tar>

    <echo>
    Created target/update-${DSTAMP}-${TSTAMP}.tar.gz
    Upload the update site using the upload-site.sh script
    </echo>
        
  </target>

  <!-- Initialize platform-specific properties -->
  <target name="init.platform">
    <condition property="src.ext" value="tar.gz">
      <equals arg1="${target.os}" arg2="macosx-carbon" />
    </condition>
    <property name="src.ext" value="zip" />
    <condition property="dest.ext" value="zip">
      <equals arg1="${target.os}" arg2="win32" />
    </condition>
    <property name="dest.ext" value="tar.gz" />
    <property name="platform.zip"
    value="eclipse-platform-${target.version}-${target.os}.${src.ext}" />
    <property name="target.zip"
    value="vex-${version}-${target.os}.${dest.ext}" />
  </target>

  <target name="download" depends="init.platform"
          description="Download the Eclipse platform">
    <fail unless="target.os">Please set target.os, e.g. ant
    -Dtarget.os=win32 download</fail>
    <mkdir dir="target" />
    <mkdir dir="target/downloads" />
    <setproxy proxyhost="proxy.host" proxyport="proxy.port" />
    <get src="http://download.eclipse.org/downloads/drops/S-${target.version}-${target.build}/${platform.zip}"
    dest="target/downloads/${platform.zip}" usetimestamp="true"
    verbose="true" />
  </target>

  <target name="download.all"
  description="Download all supported Eclipse platforms">
    <antcall target="download">
      <param name="target.os" value="linux-gtk" />
    </antcall>
    <antcall target="download">
      <param name="target.os" value="win32" />
    </antcall>
    <antcall target="download">
      <param name="target.os" value="macosx-carbon" />
    </antcall>
  </target>

  <!-- Build the executable for a specific platform -->
  <target name="build.exe"
          depends="confirm.built, init.platform"
          description="Make an exe for the given target OS">

    <fail unless="target.os">
Please set target.os, e.g. ant -Dtarget.os=win32 build.exe
    </fail>
    
    <fail unless="platform.cache.dir">
Please set platform.cache.dir in
~/vex-build.properties to the directory where you've
downloaded the Eclipse platform binaries
     </fail>
    
    <available file="${platform.cache.dir}/${platform.zip}"
               property="eclipse.platform.available" />
               
    <fail unless="eclipse.platform.available">
Please download the applicable Eclipse platform, 
e.g. run 'ant -Dtarget.os=${target.os} download'
    </fail>
    
    <delete dir="target/${target.os}" />
    <mkdir dir="target/${target.os}" />
    <antcall target="unpack.${src.ext}" />
    <move todir="target/${target.os}/vex">
      <fileset dir="target/${target.os}/eclipse" />
    </move>
    <unzip src="${feature.zip}" dest="target/${target.os}/vex" />
    <copy file="target/${target.os}/vex/plugins/net.sf.vex_${version}/configuration/config.ini"
          todir="target/${target.os}/vex/configuration" />
    <antcall target="fix.${target.os}" />
    <antcall target="pack.${dest.ext}" />
  </target>


  <target name="build.all.exes"
          description="Make exe's for all supported Eclipse platforms">

    <copy file="${feature.zip}" tofile="target/vex-${version}-plugins.zip"/>
    <antcall target="build.exe">
      <param name="target.os" value="linux-gtk" />
    </antcall>
    <antcall target="build.exe">
      <param name="target.os" value="win32" />
    </antcall>
    <!--
    <antcall target="build.exe">
      <param name="target.os" value="macosx-carbon" />
    </antcall>
    -->
  </target>

  <target name="pack.zip">
    <zip zipfile="target/${target.zip}">
      <fileset dir="target/${target.os}" includes="vex/**/*" />
    </zip>
  </target>

  <target name="pack.tar.gz">
    <tar tarfile="target/${target.zip}" compression="gzip">
      <tarfileset dir="target/${target.os}" mode="755">
        <include name="vex/vex" />
      </tarfileset>
      <tarfileset dir="target/${target.os}">
        <include name="vex/**/*" />
        <exclude name="vex/vex" />
      </tarfileset>
    </tar>
  </target>

  <target name="unpack.zip">
    <unzip src="${platform.cache.dir}/${platform.zip}"
           dest="target/${target.os}" />
  </target>

  <target name="unpack.tar.gz">
    <untar compression="gzip"
           src="${platform.cache.dir}/${platform.zip}"
           dest="target/${target.os}" />
  </target>

  <target name="fix.linux-gtk">
    <move file="target/${target.os}/vex/eclipse"
          tofile="target/${target.os}/vex/vex" />
    <chmod file="target/${target.os}/vex/vex" perm="+x"/>
  </target>

  <target name="fix.macosx-carbon"></target>

  <target name="fix.win32">
    <move file="target/${target.os}/vex/eclipse.exe"
          tofile="target/${target.os}/vex/vex.exe" />
  </target>
  
  <target name="clean" description="Delete all generated files">
    <delete dir="target"/>
  </target>

</project>
