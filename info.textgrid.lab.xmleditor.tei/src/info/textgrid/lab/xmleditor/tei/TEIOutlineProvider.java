/*
 * Created on 22.08.2007
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package info.textgrid.lab.xmleditor.tei;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.eclipse.jface.viewers.IBaseLabelProvider;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.Viewer;

import net.sf.vex.dom.IVexDocument;
import net.sf.vex.dom.IVexElement;
import net.sf.vex.editor.IOutlineProvider;
import net.sf.vex.editor.VexEditor;


public class TEIOutlineProvider implements IOutlineProvider {

	public ITreeContentProvider getContentProvider() {
		return this.contentProvider;
	}

	public IBaseLabelProvider getLabelProvider() {
		return this.labelProvider;
	}

	public IVexElement getOutlineElement(IVexElement child) {
		IVexElement element = child;
		while ((element.getParent() != null) && !isTitledElement(element)) {
			element = element.getParent();
		}

		return element;
	}

	public void init(VexEditor editor) {
	}

	private final ITreeContentProvider contentProvider = new ITreeContentProvider() {

		public void dispose() {
		}

		public void inputChanged(final Viewer viewer, final Object oldInput,
				final Object newInput) {
		}

		public Object[] getChildren(final Object parentElement) {
			return getOutlineChildren((IVexElement) parentElement);
		}

		public Object getParent(final Object element) {
			final IVexElement parent = ((IVexElement) element).getParent();
			if (parent == null) {
				return element;
			} else {
				return getOutlineElement(parent);
			}
		}

		public boolean hasChildren(final Object element) {
			return getOutlineChildren((IVexElement) element).length > 0;
		}

		public Object[] getElements(final Object inputElement) {
			final IVexDocument document = (IVexDocument) inputElement;
			return new Object[] { document.getRootElement() };
		}

	};
	
    /**
     * Returns an array of the children of the given element that represent
     * nodes in the outline. 
     * 
     * @param element
     * @return
     */
    private IVexElement[] getOutlineChildren(final IVexElement element) {
        final List children = new ArrayList();
        final IVexElement[] childElements = element.getChildElements();
        for (int i = 0; i < childElements.length; i++) {
            if (titledElements.contains(childElements[i].getName())) {
                children.add(childElements[i]);
            }
        }
        return (IVexElement[]) children.toArray(new IVexElement[children.size()]);
    }
    
    
    private final ILabelProvider labelProvider = new LabelProvider() {
    	public String getText(final Object o) {
    		final IVexElement e = (IVexElement) o;
    		IVexElement titleChild = findChild(e, "title");
    		if (titleChild != null) {
    			return titleChild.getText();
    		} else {

    			titleChild = findChild(e, "head");
    			if (titleChild != null) {
    				return titleChild.getText();
    			} else {


    				IVexElement teiHeaderChild = findChild(e, "teiHeader");
    				if (teiHeaderChild != null) {
    					IVexElement fileDescChild = findChild(teiHeaderChild, "fileDesc");
    					if (fileDescChild != null) {
    						IVexElement titleStmtChild= findChild(fileDescChild, "titleStmt");
    						if (titleStmtChild != null) {
    							titleChild = findChild(titleStmtChild, "title");
    							if (titleChild != null) {
    								return titleChild.getText();
    							}

    						}                    

    					}                    


    				}
    			}
    		}
    		return e.getName();
    	}
    };

    /**
     * Returns a value from a map of tag names that contain title elements 
     * The tags all have a similar characteristic. Their content model
     * includes a <title/> element as the first or second child.
     * 
     * For simplicity, we assume that the document is valid w.r.t. the DTD
     * and thus the 'title' element is optional and is one of the first two
     * elements inside the given element. 
     * 
     * @param element
     * @return
     */
    private boolean isTitledElement( final IVexElement e ) {
    	
    	if( titledElements.contains(e.getName() )
     			|| e.getParent() == null ) {
    		final IVexElement [] children = e.getChildElements();
    		if( (children.length > 0 && children[0].getName().equals("title"))
    		 || (children.length > 1 && children[1].getName().equals("title"))
    		 || (children.length > 0 && children[0].getName().equals("head"))
    		 || (children.length > 1 && children[1].getName().equals("head"))
    		 
    		  ) {
    		    	return true;
    		    }
    		}	
    	return false;
    }

    /**
     * Finds the first child of the given element with the given name. Returns
     * null if none found. We should move this to XPath when we gain that
     * facility.
     */
    private IVexElement findChild(IVexElement parent, String childName) {
        IVexElement[] children = parent.getChildElements();
        for (int i = 0; i < children.length; i++) {
            IVexElement child = children[i];
            if (child.getName().equals(childName)) {
                return child;
            }
        }
        return null;
    }
    
    private final static int MAPSIZE = 101; // prime greater than 65/.80
    
    private final static HashSet titledElements = new HashSet(MAPSIZE);
    
    static {
 // Comment on each line indicates tags content model if different than title
 // We are only interested in location of title tag within Content Model
 // so any extra following content is ignored.

// In general, it is impossible to keep this sort of table accurate and 
// up-to-date for more than the most basic of cases. It would be better 
// to extract this information from the DTD for the document.

// For the moment, it is observed that this imperfect method works for 
// vast majority of DocBook documents.

    	titledElements.add("TEI.2"); 
    	titledElements.add("div1");
    	titledElements.add("div2"); 
    	titledElements.add("div3");
    	titledElements.add("div4");
    	titledElements.add("div5");
    	titledElements.add("body");
    	titledElements.add("text");
//    	

    	

    }


}
