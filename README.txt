========================
 TextGridLab XML Editor
========================

:Author: Thorsten Vitt
:Date: $Date$


This directory contains the source code for TextGridLab's XML
editor. All folders except for `runtime-test-workspace` contain
Eclipse projects.

The XML editor itself is built from two existing XML editors: WST's,
currently in Version 2, and Vex, plus our own modifications to Vex and
integration and glue code.

The mpeditor subdirectory with the plugin
`info.textgrid.lab.xmleditor.mpeditor` is the code containing the
actual editor part. Please see the JavaDocs in this plugin, and
especially the package docs, for an introduction.


Directory Contents
==================

Main editor plugins
-------------------

mpeditor:
  The three-page XML editor (Design, Source, WYSIWYM) as well as
  general and containing code.

org.eclipse.wst.xml.core:
  A slightly modified copy of WST's corresponding plugin, version
  R2.0.2. Please see the SVN history for the code details of the
  change, it has been introduced to support schemas loaded from the
  Grid via EFS.

vex-editor:
  Contains the (mostly Eclipse RCP specific) editor parts for Vex etc.

vex-toolkit:
  The plugin containing most parts of Vex: Here you can find the data
  model, including our linked one, the layout implementation and the
  basic implementation of most editing code. 


Various Auxillary Directories
-----------------------------

runtime-test-workspace:
  An Eclipse workspace containing layout test code. Will probably
  moved some time.

sac:
  The CSS library (SAC)

utils:
  Some general utility code (that could be used outside the XML editor
  and will probably be moved).

vex-textgrid-doc:
  Some retro documentation on how Vex works.

vex-tei:
  TEI support for Vex. Mainly the stylesheet inside here is used. Also
  provides a default schema (TEI-ALL).


Currently Unused
----------------

vex-docbook:
  Vex support for DocBook.

vex-feature:
  A feature to build Vex standalone. Currently unused.

vex-help:
  Help files for standalone Vex. Currently outdated.

vex-product:
  A product to build Vex standalone. Currently unused.

vex-samples:
  Sample files for Vex.

vex-dita:
  Vex support for DITA. Currently not used in TextGridLab.

vex-xhtml:
  XHTML support for Vex. 

toolkit-tests:
  Old and unmaintained unit tests


charbrowser:
  Contains a Unicode character browser, currently not fully working.

