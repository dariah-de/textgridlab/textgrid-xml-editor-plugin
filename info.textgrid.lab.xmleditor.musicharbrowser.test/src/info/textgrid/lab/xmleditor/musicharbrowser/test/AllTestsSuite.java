package info.textgrid.lab.xmleditor.musicharbrowser.test;

import junit.framework.Test;
import junit.framework.TestSuite;

public class AllTestsSuite {

	public static Test suite() {
		TestSuite suite = new TestSuite(
				"Test for info.textgrid.lab.xmleditor.musicharbrowser.test");
		//$JUnit-BEGIN$
		suite.addTestSuite(CustomCharsetResourceReaderTest.class);
		suite.addTestSuite(UnicodeViewTest.class);
		suite.addTestSuite(UnicodeViewerTest.class);
		suite.addTestSuite(CustomCharsetDialogTest.class);
		//$JUnit-END$
		return suite;
	}

}
