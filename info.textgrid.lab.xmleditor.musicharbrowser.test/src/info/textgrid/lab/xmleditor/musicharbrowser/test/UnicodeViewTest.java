package info.textgrid.lab.xmleditor.musicharbrowser.test;

import info.textgrid.lab.xmleditor.multicharbrowser.UnicodeView;
import junit.framework.TestCase;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class UnicodeViewTest extends TestCase{
	private UnicodeView unicodeView;

	@Before
	public void setUp() throws Exception {
		unicodeView = new UnicodeView(); 
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public final void testSetToggleState() {
		for(int i=0; i < 5; i++) {
			unicodeView.setToggleState(i);
			assertEquals(unicodeView.getToggleState(), i);
			for(int j=0; j < 5; j++) {
				if(i!=j)
					assertTrue(!(unicodeView.getToggleState() == j));
			}
		}
	}

	@Test
	public final void testUnicodeView() {
		assertNotNull(unicodeView);
	}

}
