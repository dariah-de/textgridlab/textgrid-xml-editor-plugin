/**
 * 
 */
package info.textgrid.lab.xmleditor.musicharbrowser.test;

import info.textgrid.lab.xmleditor.multicharbrowser.CustomCharsetDialog;
import junit.framework.TestCase;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author julian
 *
 */
public class CustomCharsetDialogTest extends TestCase {
	
	private CustomCharsetDialog customCharsetDialog;
	private Shell parent;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		parent = new Shell();
		customCharsetDialog = new CustomCharsetDialog(parent, SWT.None);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link info.textgrid.lab.xmleditor.multicharbrowser.CustomCharsetDialog#createDialogArea(org.eclipse.swt.widgets.Composite)}.
	 */
	@Test
	public final void testCreateDialogArea() {
		Control control = customCharsetDialog.createDialogArea(parent);
		assertNotNull(control);
	}

	/**
	 * Test method for {@link info.textgrid.lab.xmleditor.multicharbrowser.CustomCharsetDialog#open()}.
	 */
	@Test
	public final void testOpen() {
		Object obj = customCharsetDialog.open();
		assertNotNull(obj);
		customCharsetDialog.closeDialog();
	}

	/**
	 * Test method for {@link info.textgrid.lab.xmleditor.multicharbrowser.CustomCharsetDialog#CustomCharsetDialog(org.eclipse.swt.widgets.Shell, int)}.
	 */
	@Test
	public final void testCustomCharsetDialog() {
		assertNotNull(customCharsetDialog);
	}

}
