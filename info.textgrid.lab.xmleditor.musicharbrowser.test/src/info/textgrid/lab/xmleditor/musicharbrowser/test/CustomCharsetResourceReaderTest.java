package info.textgrid.lab.xmleditor.musicharbrowser.test;

import info.textgrid.lab.xmleditor.multicharbrowser.CustomCharsetData;
import info.textgrid.lab.xmleditor.multicharbrowser.CustomCharsetResourceReader;

import java.util.Vector;

import junit.framework.TestCase;

public class CustomCharsetResourceReaderTest extends TestCase {

	protected void setUp() throws Exception {
		super.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public final void testGetDataVector() {
		Vector<CustomCharsetData> testVector = CustomCharsetResourceReader.getDataVector();
//		assertEquals(testVector, null);
		CustomCharsetResourceReader.loadCustomCharsetsFromFile();
		testVector = CustomCharsetResourceReader.getDataVector();
		assertNotNull(testVector);
		assertTrue(testVector.size() > 0);
		
	}

	public final void testReadCustomCharsetsFileNow() {
		assertTrue(CustomCharsetResourceReader.loadCustomCharsetsFromFile());
	}

	public final void testResizeArrayIntArrayInt() {
		int[] testArray = {1,2,3,4};
		assertTrue(testArray.length == 4 );
		testArray = CustomCharsetResourceReader.resizeArray(testArray, 5);
		testArray[4] = 0;
		assertTrue(testArray.length == 5 );
	}

}
