/**
 * 
 */
package info.textgrid.lab.xmleditor.musicharbrowser.test;

import info.textgrid.lab.xmleditor.multicharbrowser.UnicodeView;
import info.textgrid.lab.xmleditor.multicharbrowser.UnicodeViewer;
import junit.framework.TestCase;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author julian
 *
 */
public class UnicodeViewerTest extends TestCase{
	
//	private UnicodeView unicodeView;
	
	/**
	 * The central unicode viewer to be tested
	 */
	private UnicodeViewer unicodeViewer;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
//		unicodeView = new UnicodeView();
//		if(unicodeView == null)
//			System.out.println("unicodeView is null");
//		assertNotNull(unicodeView);
//		Shell shell = new Shell();
//		if(shell == null)
//			System.out.println("Shell is null");
//		assertNotNull(shell);
//		Composite composite = new Composite(shell, SWT.NONE);
//		if(composite == null)
//			System.out.println("Composite is null");
//		assertNotNull(composite);
////		unicodeViewer = new UnicodeViewer(composite, unicodeView);
//		if(unicodeViewer == null)
//			System.out.println("Viewer is null");
		Shell shell = new Shell();
		Composite composite = new Composite(shell, SWT.NONE);
		unicodeViewer = new UnicodeViewer(composite,
				new UnicodeView());
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link info.textgrid.lab.xmleditor.multicharbrowser.viewer.UnicodeViewer#insertSelectedTableItemIntoEditor()}.
	 */
	@Test
	public final void testInsertSelectedTableItemIntoEditor() {
		boolean active = true;
//		if(unicodeViewer != null)
//			unicodeViewer.insertSelectedTableItemIntoEditor();
		assertTrue(active);
	}

	/**
	 * Test method for {@link info.textgrid.lab.xmleditor.multicharbrowser.viewer.UnicodeViewer#insertStringIntoEditor(java.lang.String)}.
	 */
	@Test
	public final void testInsertStringIntoEditor() {
		boolean active = true;		
//		if(unicodeViewer != null) {
//			try {
//				assertTrue(unicodeViewer.insertStringIntoEditor("X") == new UnicodeView().insertIntoEditor("X"));
//			} catch (NullPointerException e) {
//				active = false;
//			}
//		}
//			unicodeViewer.insertStringIntoEditor("X");
		assertTrue(active);
	}

	/**
	 * Test method for {@link info.textgrid.lab.xmleditor.multicharbrowser.viewer.UnicodeViewer#UnicodeViewer(org.eclipse.swt.widgets.Composite, info.textgrid.lab.xmleditor.multicharbrowser.viewer.UnicodeView)}.
	 */
	@Test
	public final void testUnicodeViewer() {		
		assertNotNull(unicodeViewer);
	}

}
