package info.textgrid.lab.xmleditor.charbrowser.viewer;


import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import org.eclipse.core.commands.Command;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.commands.IParameter;
import org.eclipse.core.commands.Parameterization;
import org.eclipse.core.commands.ParameterizedCommand;
import org.eclipse.core.commands.common.NotDefinedException;
import org.eclipse.jface.bindings.Binding;
import org.eclipse.jface.bindings.keys.KeyBinding;
import org.eclipse.jface.bindings.keys.KeySequence;
import org.eclipse.jface.bindings.keys.ParseException;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.commands.ICommandService;
import org.eclipse.ui.internal.Workbench;
import org.eclipse.ui.internal.keys.WorkbenchKeyboard;
import org.eclipse.ui.keys.IBindingService;

/** *//**
 * ShortCutAssist Util
 * <p>
 * this class can modify KeyAssist in runtime
 */

public class ShortCutAssist {
    private final static String SCHEME_NAME = "org.eclipse.ui.defaultAcceleratorConfiguration";

    private final static String CONTEXT_NAME = "org.eclipse.ui.contexts.dialogAndWindow";

    private final static String CATEGORY_NAME = "info.textgrid.lab.xmleditor.charbrowser.category";

    private Workbench workbench;

    private ICommandService commandService;

    private IBindingService bindingService;

    private Binding[] oldbindings;

    private HashMap<String, KeyBinding> newbindings;
    private Binding[] removeBindings;

    public ShortCutAssist(IWorkbench workbench) {

        this.workbench = (Workbench) workbench;

        bindingService = (IBindingService) workbench
                .getAdapter(IBindingService.class);
        commandService = (ICommandService) workbench
                .getAdapter(ICommandService.class);

        oldbindings = bindingService.getBindings();

        newbindings = new HashMap();

   }

    /** *//**
     * 
     * @param commandId
     *         The map of command identifiers to commands must not be null
     * @param commandName
     *         Show name in the Ke	yAssist list must not be null
     * @param keySequence
     *         Short key String must not be null
     * @param description
    * @return
     */
    public KeyBinding addShortKey(String commandId, String commandName,
            String keySequence, String description){
//            , String parameterId, String character) {

    	KeySequence seq = null;
        KeyBinding binding = null;
        Parameterization parm = null;
        ParameterizedCommand parmCommand = null;
        final Command command = commandService.getCommand(commandId);
        
        if (!command.isDefined()){
//        	System.out.println("!command.isDefined()");
        	command.define(commandName, description, commandService
                    .getCategory(CATEGORY_NAME), null);
        }
        if(!command.isHandled()){
//            System.out.println("!command.isHandled()");
            IHandler handler = new InsertHandler();
            command.setHandler(handler);
//            System.out.println("command.setHandler");
          }

        try {
            seq = KeySequence.getInstance(keySequence);
        } catch (ParseException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        
        binding = new KeyBinding(seq, new ParameterizedCommand(command, null),
                SCHEME_NAME, CONTEXT_NAME, "", "", null, Binding.USER);
		//

        newbindings.put(keySequence, binding);
        
        return binding;
    }

    public void removeAllShortKey() {
        newbindings.clear();
    }

    public void removeShortKey(String keySequence) {
        newbindings.remove(keySequence);
    }

    public void OpenKeyAssist() {
        recomputeBindings();
        WorkbenchKeyboard workbenchkeyboard = new WorkbenchKeyboard(
                this.workbench);
        workbenchkeyboard.openMultiKeyAssistShell();
    }

    public void recomputeBindings() {
    	
        Binding[] bindings = new Binding[oldbindings.length
                + newbindings.size()];
        System.arraycopy(oldbindings, 0, bindings, 0, oldbindings.length);

        Set set = newbindings.keySet();
        Iterator iter = set.iterator();

        for (int i = oldbindings.length; i < bindings.length; i++) {
            if (iter.hasNext())
                bindings[i] = (Binding) newbindings.get(iter.next());
        }

        try {
            bindingService.savePreferences(bindingService
                    .getScheme(SCHEME_NAME), bindings);

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void recomputeBindingsRemove(String keySequence){
//    	System.out.println("recomputeBindingsRemove!");
    	oldbindings = bindingService.getBindings();
    	Binding[] bindings = new Binding[oldbindings.length 
    	          -1];
    	KeySequence seq =   null;
    	try {
            seq = KeySequence.getInstance(keySequence);
        } catch (ParseException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
       	int m=0;
    	for(int i = 0; i< oldbindings.length; i++){

    			if( !oldbindings[i].getTriggerSequence().toString().equals(seq.toString())){    			  				    					
    				bindings[m] = oldbindings[i];
    				m++;
    			}
    	}
        try {
            bindingService.savePreferences(bindingService
                    .getScheme(SCHEME_NAME), bindings);

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    
    public void printAllKeys(){
    	Binding[] bindings = bindingService.getBindings();
    
    	for(int i =0; i<bindings.length; i++)
    		
    		System.out.println(bindings[i]);
    	
    }
    public HashMap<String, KeyBinding> getNewbindings(){
//  	   	Set s = newbindings.keySet();
//        Iterator iter = s.iterator();
//        while(iter.hasNext())
//        	System.out.println(newbindings.get(iter.next()));
    	return newbindings;
    }
    public void setNewbindings(HashMap<String, KeyBinding> bindings){
    	newbindings = bindings;
    }

}
