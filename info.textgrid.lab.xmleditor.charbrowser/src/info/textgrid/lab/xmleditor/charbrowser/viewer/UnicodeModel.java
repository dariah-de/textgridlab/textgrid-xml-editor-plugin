package info.textgrid.lab.xmleditor.charbrowser.viewer;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.Vector;
/*import java.awt.Font;*/

import org.eclipse.jface.resource.FontRegistry;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.*;

import com.ibm.icu.lang.*;


public class UnicodeModel {
	private static final int columns = 8 ;
	private Vector chars = new Vector(2);
	private Set changeListeners = new HashSet();
//	FontRegistry fontRegistry;
    java.awt.GraphicsEnvironment eq = java.awt.GraphicsEnvironment.getLocalGraphicsEnvironment();
    private String[] fonts = eq.getAvailableFontFamilyNames();
   
	/**
	 * Constructor
	 */
	public UnicodeModel(int start, int end) {
		super();
		this.initVector(start,end);
//		updateFont("arial");
//		this.initFonts();
	}
	
	public UnicodeModel(String name, String scriptorblock){
		super();
		if (scriptorblock=="script")this.initScriptVector(name);
		if (scriptorblock=="block") this.initBlockVector(name);
//		this.initFonts();
	}
	public void initBlockVector(String block){
		int index = 0x0000;
		int start = 0x0000, end = 0xffff;
		while(!UCharacter.UnicodeBlock.of(index).toString().equalsIgnoreCase(block)){
			index++;
		}
		start = index;
		while(UCharacter.UnicodeBlock.of(index).toString().equalsIgnoreCase(block)){
			index++;
		}
		end = index;
		this.initVector(start, end);
	}
	public void initScriptVector(String script){
		String[] temp = new String[columns];
		int index = 0;
		for(int i = 0x0000; i< 0x10ffff; i++){
			if(UScript.getName(UScript.getScript(i)).equalsIgnoreCase(script)){
				if(index<columns){
					temp[index] = newString(i);
					index++;
				}else{
//					for(int j = 0 ; j< temp.length; j++){
//						System.out.print(temp[j]+"\t");
//					}
//					System.out.println();
					chars.add(temp);
					temp= new String[columns];
					temp[0] = newString(i);
					index =1;
				}
			}				
		}
	}
	public void initVector(int start,int end){
				int codePoint = start;
//				int max = 0x10fff;
				for(int j=0; j< end/columns; j++){
					String[] s = new String[columns]; 
					if(codePoint <= end){
					for(int i=0; i<columns; i++){
						while(Character.isWhitespace(codePoint)||!Character.isDefined(codePoint)||
								Character.isIdentifierIgnorable(codePoint)||Character.isSpaceChar(codePoint)){
							codePoint++;
							
						}
						s[i] = newString(codePoint);
						codePoint++;
					}
					chars.add(s);
					}else{
						break;
					}
				}
				/*String[] s =new String[15];
				s[0]= newString((int)0x10000);
				s[1] = newString(0x10001);
				chars.add(s);
				char[] chars1 = {'\uD840','\uDC00'};
				char[] chars2 = Character.toChars(0x10bff);
				char[] chars3 = Character.toChars(0x10c00);
				System.out.println(chars1);
				System.out.println(Integer.toHexString((int)chars2[0])+"   "+Integer.toHexString((int)chars2[1]));
				System.out.println(Integer.toHexString((int)chars3[0])+"   "+Integer.toHexString((int)chars3[1]));*/
	}
//	public void initFonts(){
//		fontRegistry = new FontRegistry();
//		for(int i=0; i<fonts.length; i++)
//			fontRegistry.put(fonts[i], new FontData[]{new FontData(fonts[i], 20, SWT.NORMAL)});
//	}
//	public Font getFont(String symbolicName){
//		return fontRegistry.get(symbolicName);
//	}
//	public void putFont(String symbolicName, FontData[] fontData){
//		fontRegistry.put(symbolicName, fontData);
//	}
	public void updateModel(int start, int end){
//		initVector(start);
		Iterator iterator = changeListeners.iterator();
		while (iterator.hasNext())
			((IModelViewer) iterator.next()).updateModel(start,end);
	
	}
	public void updateModel(String name, String scriptorblock){
		Iterator iterator = changeListeners.iterator();
		while (iterator.hasNext())
			((IModelViewer) iterator.next()).updateScript(name,scriptorblock);
	}
	public void updateFont(String fontName){
		Iterator iterator = changeListeners.iterator();
		while (iterator.hasNext())
			((IModelViewer) iterator.next()).updateFont(fontName);
	}
	public int getColumns(){
		return columns;
	}
	public Vector getChars(){
		return chars;
	}
//	public void setFontRegistry(FontRegistry fontRegistry){
//		fontRegistry = fontRegistry;
//	}
	public String[] getFonts(){
		return fonts;
	}
	static String newString(int codePoint) {
	    if (Character.charCount(codePoint) == 1) {
	        return String.valueOf((char)codePoint);
	    } else {
	        return new String(Character.toChars(codePoint));
	    }
	}
	public void addChangeListener(IModelViewer viewer){
		changeListeners.add(viewer);
	}
	public void removeChangeListener(IModelViewer viewer){
		changeListeners.remove(viewer);
	}
	public static void main(String args[]){

	}
}