package info.textgrid.lab.xmleditor.charbrowser.viewer;

import net.sf.vex.dom.DocumentValidationException;
import net.sf.vex.editor.VexEditor;
import net.sf.vex.swt.VexWidget;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.common.NotDefinedException;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.TextSelection;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.editors.text.TextEditor;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.texteditor.IDocumentProvider;
import org.eclipse.ui.texteditor.ITextEditor;

public class InsertHandler extends AbstractHandler {
//	private String character;
//	public InsertHandler(String character){
//		this.character  =  character;
//		
//	}

	public Object execute(ExecutionEvent arg0) throws ExecutionException {
		
//		System.out.println(arg0.getParameter("info.textgrid.lab.xmleditor.charbrowser.CharParameterValues"));
//		IParameter charParameter;

			
//			charParameter = arg0.getCommand().getParameter("info.textgrid.lab.xmleditor.charbrowser.CharParameterValues");
//			IParameterValues parmValues = charParameter.getValues();
		
			
//			System.out.println(arg0.);
//			System.out.println("InsertHandler Performed!!!!!!!!!!!");

//		} catch (ParameterValuesException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	
//		CharParameterValues par = new CharParameterValues();
//		try {
//			par.setInitializationData(null, "char1", "j");
//		} catch (CoreException e1) {
//			// TODO Auto-generated catch block
//			System.out.println("CoreException ");
//			e1.printStackTrace();
//		}
//		
//		final Map parameters = par.getParameterValues();
//		final Object value = parameters.get("char1");
//		System.out.println(value.toString());
//		String s = "K";
//		String s = character;
//		if(s==null)

		String s;
		try {
			
//			System.out.println(arg0.getCommand().getName());
//			System.out.println(arg0.getCommand().getName().substring(7));
			s=arg0.getCommand().getName().substring(7);
			IEditorPart part =HandlerUtil.getActiveEditor(arg0);
			try{
				if(part instanceof VexEditor){									
				VexEditor editor = (VexEditor)part;
				VexWidget widget = editor.getVexWidget();
				widget.insertText(s);
				editor.setFocus();								
			}else{
		//for normal Editor
				ITextEditor editor = (ITextEditor)part;							
				IDocumentProvider dp = editor.getDocumentProvider();
				IDocument doc = dp.getDocument(editor.getEditorInput());
				TextSelection selection = (TextSelection) ((TextEditor)part).getSelectionProvider().getSelection();
				int offset = selection.getOffset();
				try {
					// replace selection with the result of format.
				      doc.replace(selection.getOffset(), selection.getLength(),s);
				 } catch (BadLocationException e) {
				     e.printStackTrace();							    
				 }		 
				 editor.getSelectionProvider().setSelection(new TextSelection(offset+s.length(),0));
				 editor.setFocus();
			}
				}catch(DocumentValidationException e) {
				}	 

		} catch (NotDefinedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} 
		// TODO Auto-generated method stub
		return null;
	}

}
