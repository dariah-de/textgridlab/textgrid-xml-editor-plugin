package info.textgrid.lab.xmleditor.charbrowser.viewer;


import info.textgrid.lab.core.swtutils.AdapterUtils;
import info.textgrid.lab.xmleditor.charbrowser.Activator;

import java.util.ArrayList;
import java.util.HashMap;

import net.sf.vex.dom.DocumentValidationException;
import net.sf.vex.editor.VexEditor;
import net.sf.vex.swt.VexWidget;

import org.eclipse.core.commands.Command;
import org.eclipse.core.commands.common.NotDefinedException;
import org.eclipse.jface.bindings.Binding;
import org.eclipse.jface.bindings.keys.KeyBinding;
import org.eclipse.jface.bindings.keys.KeySequence;
import org.eclipse.jface.bindings.keys.ParseException;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.text.TextSelection;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.commands.ICommandService;
import org.eclipse.ui.editors.text.TextEditor;
import org.eclipse.ui.internal.Workbench;
import org.eclipse.ui.keys.IBindingService;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.ui.texteditor.IDocumentProvider;
import org.eclipse.ui.texteditor.ITextEditor;


/**
 * @author amy
 *
 */
public class UnicodeView extends ViewPart {
	private UnicodeViewer viewer;
//	private myAction doubleClickAction;
	private Table table, table2;
	private ArrayList<String> list = new ArrayList<String>(16);
//	private IMemento memento;
//	private static final String STORE_CHARS = "CHARS";
//	private Action action;
//	private DialogSettings settings = new DialogSettings("root");
//	private String location = Activator.getDefault().getStateLocation()+"/chars.xml";
//	private static final String INSERT_ID = "info.textgrid.lab.xmleditor.charbrowser.insertCommand"; //$NON-NLS-1$
//	private InsertHandler insertHandler;
	
//	private IDialogSettings dset = Activator.getDefault().getDialogSettings();
//	private IDialogSettings dSection = dset.addNewSection("Unicode");
//	private final static String SCHEME_NAME = "org.eclipse.ui.defaultAcceleratorConfiguration";
//	private final static String CONTEXT_NAME = "org.eclipse.ui.contexts.dialogAndWindow";
//		 private final static String CATEGORY_ID = "info.textgrid.lab.xmleditor.charbrowser.category";
		 private ICommandService commandService;
		 private HashMap<String, KeyBinding> newbindings;
//			private IWorkbenchWindow window;
	/**
	 * The constructor.
	 */
	public UnicodeView() {
	}

//	public void saveState(IMemento memento) {
//	      super.saveState(memento);
////	      ISelection sel = viewer.getSelection();
////	      IStructuredSelection ss = (IStructuredSelection) sel;
//	      StringBuffer buf = new StringBuffer();
//	      for (Iterator it = list.iterator(); it.hasNext();) {
//	         buf.append(it.next());
//	         buf.append(',');
//	      }
//	      memento.putString(STORE_CHARS, buf.toString());
//	   }
	/**
	 * @param character
	 * @param keySequence
	 */
	public void saveHotKeyCommands(String character, String keySequence){
      	IPreferenceStore store = Activator.getDefault().getPreferenceStore();

      	String stores="";
      	stores+= Activator.getDefault().getPreferenceStore().getString("hotkeys")+
      					character+"#"+keySequence+",";
      	
  		store.setValue("hotkeys",stores);
  		System.out.println("store:  "+stores);
      	Activator.getDefault().savePluginPreferences();
	}
	
	/**
	 * 
	 */
	public void saveSettings(){
		String[] s = new String[list.size()];
		for(int i = 0; i<list.size(); i++){
			s[i] = list.get(i);
		}
//	      settings.put(STORE_CHARS, s);
//	      try{	    	  
//			settings.save(location);			
//	      	}catch(IOException e ){
//	    	  System.err.println(e.getStackTrace());
//	      	}
	      	
	      	String stores = "";
	      	for(int j = 0; j<s.length; j++){
	      		stores+=s[j]+",";
	      	}
	      	IPreferenceStore store = Activator.getDefault().getPreferenceStore();
	      	
	      	store.setValue("Chars",stores);
	      	Activator.getDefault().savePluginPreferences();

	}
//	public void init(IViewSite site, IMemento memento)
//	    throws PartInitException {
//	    super.init(site, memento);
//	    this.memento = memento;
//	 }
	

	/**
	 * 
	 */
	public void loadHotKeyCommands(){
		Workbench workbench = (Workbench)getSite().getWorkbenchWindow().getWorkbench();
	     commandService = (ICommandService) workbench
        .getAdapter(ICommandService.class);
	     
			ShortCutAssist keyAssist = new ShortCutAssist(workbench);
//			keyAssist.printAllKeys();
			
		String[] s = Activator.getDefault().getPreferenceStore().getString("hotkeys").split(",");
		if(s==null)
			System.out.println("no hot key!");
		else{
//			System.out.println("hot key!"+s.length);
			for(int i = 0; i < s.length; i ++){
//				System.out.println(s[i]);
				if(s[i].contains("#")){
					String character = s[i].split("#")[0];
					String keySequence = s[i].split("#")[1];
					Command comm=commandService.getCommand("info.textgrid.lab.xmleditor.charbrowser.insertCommand_"+character );
		
					if(!comm.isDefined()){
								keyAssist.recomputeBindingsRemove(keySequence);
								
								performInsertHandler(character,
					    		  "info.textgrid.lab.xmleditor.charbrowser.insertCommand_"+character,
					    		  "Insert_"+character,
					    		  keySequence);
	
					}
				}
			}
		}
	}
	
	
	/**
	 * 
	 */
	public void loadSettings(){
		String[] s = Activator.getDefault().getPreferenceStore().getString("Chars").split(",");
		for(int i = 0; i < s.length; i ++){
			list.add(s[i]);
		}

		if(list!=null)		
			viewer.setUsedChars(list);
	}
	
	/**
	 * This is a callback that will allow us
	 * to create the viewer and initialize it.
	 */	
	public void createPartControl(Composite parent) {
		viewer = new UnicodeViewer(parent);
		table = viewer.getTable();
		table2 = viewer.getTable2();
		loadSettings();
		loadHotKeyCommands();
		
		/*listener to add MouseDoubleClick Inserting event*/
		table.addListener (SWT.MouseDoubleClick, new Listener () {			
			public void handleEvent (Event event) {
				Rectangle clientArea = table.getClientArea ();
				Point pt = new Point (event.x, event.y);
				int index = table.getTopIndex ();
				while (index < table.getItemCount ()) {
					boolean visible = false;
					final TableItem item = table.getItem (index);
					for (int i=0; i<table.getColumnCount (); i++) {
						Rectangle rect = item.getBounds (i);
						if (rect.contains (pt)) {							
							//starts here, to insert character into active Editor							
							IEditorPart part = getViewSite().getPage().getActiveEditor();
							//for VexEditor
							try{
								if(part instanceof VexEditor){									
								VexEditor editor = (VexEditor)part;
								VexWidget widget = editor.getVexWidget();
								widget.insertText(item.getText (i));
								editor.setFocus();								
							}else{
								
							//for normal Editor
								ITextEditor editor = AdapterUtils.getAdapter(part, ITextEditor.class);
								if (editor != null) {
								IDocumentProvider dp = editor.getDocumentProvider();
								IDocument doc = dp.getDocument(editor.getEditorInput());
								ITextSelection selection = (ITextSelection) editor.getSelectionProvider().getSelection();
//								int length = selection.getLength();
								int offset = selection.getOffset();
//								System.out.println("selection.getLength()="+length+"\t"+"selection.getOffset="+offset);
								try {
									// replace selection with the result of format.
								      doc.replace(selection.getOffset(), selection.getLength(),item.getText (i));
								 } catch (BadLocationException e) {
								     e.printStackTrace();							    
								 }
								 //selection.offset+1					 
								 editor.getSelectionProvider().setSelection(new TextSelection(offset+1,0));
								 //set the Focus back to the active Editor
								 //otherwise the Focus stays at the View
								 editor.setFocus();
								}
							}
								}catch(DocumentValidationException e) {
								}
								
								/*save this in the ArrayList*/
//								System.out.println(list.contains((Object)item.getText(i)));
								if(!list.contains(item.getText(i))){
									if(list.size()<16){	
										list.add(item.getText(i));
									}else {
										list.remove(0);
										list.add(item.getText(i));
									}
								}
//								String s ="";
//								for(int j = 0; j< list.size(); j++){
//									if(list.get(list.size()-1-j)!=null)s+= list.get(list.size()-1-j);
//									
//								}
										viewer.setUsedChars(list);										
										saveSettings();

							return;
						}
						if (!visible && rect.intersects (clientArea)) {
							visible = true;
						}
					}
					if (!visible) return;
					index++;
				}
			}
		});	
		table2.addListener(SWT.MouseDoubleClick, new Listener(){
			public void handleEvent (Event event) {
				Rectangle clientArea = table2.getClientArea ();
				Point pt = new Point (event.x, event.y);
				int index = table2.getTopIndex ();
				while (index < table2.getItemCount ()) {
					boolean visible = false;
					final TableItem item = table2.getItem (index);
					for (int i=0; i<table2.getColumnCount (); i++) {
						Rectangle rect = item.getBounds (i);
						if (rect.contains (pt)) {		
							IEditorPart part = getViewSite().getPage().getActiveEditor();
							//for VexEditor
							try{
								if(part instanceof VexEditor){									
								VexEditor editor = (VexEditor)part;
								VexWidget widget = editor.getVexWidget();
								widget.insertText(item.getText (i));
								editor.setFocus();								
							}else{
							//for normal Editor
								ITextEditor editor = (ITextEditor)part;							
								IDocumentProvider dp = editor.getDocumentProvider();
								IDocument doc = dp.getDocument(editor.getEditorInput());
								TextSelection selection = (TextSelection) ((TextEditor)part).getSelectionProvider().getSelection();
								int offset = selection.getOffset();
								try {
									// replace selection with the result of format.
								      doc.replace(selection.getOffset(), selection.getLength(),item.getText (i));
								 } catch (BadLocationException e) {
								     e.printStackTrace();							    
								 }
								 //selection.offset+1					 
								  editor.getSelectionProvider().setSelection(new TextSelection(offset+item.getText(i).length(),0));
								 //set the Focus back to the active Editor
								 //otherwise the Focus stays at the View
								 editor.setFocus();
							}
								}catch(DocumentValidationException e) {
								}
								
							return;
						}
						if (!visible && rect.intersects (clientArea)) {
							visible = true;
						}
					}
					if (!visible) return;
					index++;
				}
			}
		});
		viewer.getCloseButton().addSelectionListener(new SelectionAdapter() {	       	
			// Close the view i.e. dispose of the composite's parent
			public void widgetSelected(SelectionEvent e) {
				handleDispose();
			}
		});
		viewer.getHotKeysButton().addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent e) {
				String c = viewer.getPreview().getText().substring(1);			
				
			     myDialog dlg = new myDialog(Display.getCurrent().getActiveShell(),
			             "ShortCut",  "set the ShortCut for "+c+":",
			             "", new LengthValidator());
			     dlg.open();		
			     String keySequence = dlg.getValue();
			      
			     while(checkKeySequence(keySequence)== false) {
						MessageDialog.openInformation(
				  				Display.getCurrent().getActiveShell(),
				  				"Warning",
				  				"The shortcut exists already! \n please try another one");
					    dlg = new myDialog(Display.getCurrent().getActiveShell(),
					             "asd",  "set the ShortCut for "+c+":",
					             "afda", new LengthValidator());
					     dlg.open();
					    
					     keySequence = dlg.getValue();
			      }			      
			     	if(checkKeySequence(keySequence)== true){
//			     	System.out.println(keySequence);
			    	  performInsertHandler(c,
			    		  "info.textgrid.lab.xmleditor.charbrowser.insertCommand_"+c,
			    		  "Insert_"+c,
			    		  keySequence);
			    	  saveHotKeyCommands(c, keySequence);
			     	}
			}
		});
	}
    
	/**
	 * @param character
	 * @param commandId
	 * @param commandName
	 * @param keyStroke
	 */
	public void performInsertHandler(String character, String commandId, String commandName, String keyStroke){
		Workbench workbench = (Workbench)getSite().getWorkbenchWindow().getWorkbench();
	     commandService = (ICommandService) workbench
         .getAdapter(ICommandService.class);
	     
		ShortCutAssist keyAssist = new ShortCutAssist(workbench);
		keyAssist.addShortKey(commandId, commandName, keyStroke, null);
//		keyAssist.OpenKeyAssist();//this contains recomputeBindings
		keyAssist.recomputeBindings();
		newbindings = keyAssist.getNewbindings();
	}
	
    /**
     * @param keySequence
     * @return boolean
     */
    public  boolean checkKeySequence(String keySequence){
		Workbench workbench = (Workbench)getSite().getWorkbenchWindow().getWorkbench();
        IBindingService bindingService = (IBindingService) workbench
        .getAdapter(IBindingService.class);
        Binding[] bindings = bindingService.getBindings();
        KeySequence seq = null;
    	int i = 0;
        try {
            seq = KeySequence.getInstance(keySequence);
        } catch (ParseException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
    	while(i<bindings.length&&!bindings[i].getTriggerSequence().toString().equalsIgnoreCase(seq.toString())){

    		i++;
    	}
        if(i<bindings.length){
        	System.out.println("the shortcut exists already!");
        	return false;
        }
        	
        else{
        	System.out.println("shortcut added!");
        	return true;
        }
        	
    }
	
	/**
	 * 
	 */
	public void printCommands(){
		Command[] commands = commandService.getDefinedCommands();
		for(int i = 0; i <commands.length ; i++){
			try {
				System.out.println(commands[i].getName());
			} catch (NotDefinedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}


	public void selectionChanged(IWorkbenchPart part, ISelection selection) {
		if (selection instanceof IStructuredSelection) {			
		}
	}

	/**
	 * Handle a 'close' event by disposing of the view
	 */
	public void handleDispose() {	
		this.getSite().getPage().hideView(this);		
	}
	/**
	 * Passing the focus request to the viewer's control.
	 */
	public void setFocus() {
		viewer.getControl().setFocus();
	}

	/*Unicode Character*/ 
	static String newString(int codePoint) {
	    if (Character.charCount(codePoint) == 1) {
	        return String.valueOf((char)codePoint);
	    } else {
	        return new String(Character.toChars(codePoint));
	    }
	}

}


