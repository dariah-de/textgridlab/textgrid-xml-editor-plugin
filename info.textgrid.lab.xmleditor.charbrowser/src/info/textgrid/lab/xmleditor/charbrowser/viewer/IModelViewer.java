package info.textgrid.lab.xmleditor.charbrowser.viewer;

public interface IModelViewer {
	public void updateModel(int start,int end);
	public void updateScript(String name, String scriptorblock);
	public void updateFont(String fontName);
}
