package info.textgrid.lab.xmleditor.charbrowser.viewer;


import java.util.ArrayList;

import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.bindings.keys.KeyStroke;
import org.eclipse.jface.bindings.keys.ParseException;
import org.eclipse.jface.dialogs.IInputValidator;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IWorkbenchPart;

import com.ibm.icu.impl.UCharacterProperty;
import com.ibm.icu.lang.UCharacter;
import com.ibm.icu.lang.UScript;


//import net.sf.vex.editor.*;
//import net.sf.vex.swt.*;
/**  
 * <p>
 * The view uses a label provider to define how model
 * objects should be presented in the view. Each
 * view can present the same model objects using
 * different labels and icons, if needed. Alternatively,
 * a single label provider can be shared between views
 * in order to ensure that objects of the same type are
 * presented in the same way everywhere.
 * <p>
 */

/**
 * @author amy
 *
 */
public class UnicodeViewer{
	
	private Table table, table2;
	private TableViewer tableViewer;

	private Button closeButton, hotKeysButton;
	private Button okButton;
	private Combo combo1;
	private Combo combo2;
	private Combo combo3;
	private String keySequence = "";
//	private Font font;
	private Label label, preview, searchLabel, blockLabel, scriptLabel, fontLabel,used;
	private Text text, usedChars;
//	private HashMap subsets;
//	private UnicodeModel chars = new UnicodeModel(0x0000,0xffff);
	private UnicodeModel chars = new UnicodeModel("latin","script");
//	private UnicodeModel chars = new UnicodeModel("script","latin");
	private int columns = chars.getColumns(); 
    private static final UCharacterProperty prop= UCharacterProperty.getInstance();
	private ArrayList<String> list = new ArrayList<String>(2*chars.getColumns());

	
	/**
	 * The constructor.
	 */
	 public UnicodeViewer(Composite parent){
		 this.addChildControls(parent);
		 
	 }
	 private void addChildControls(Composite composite) {
		 
			GridData gridData = new GridData (GridData.HORIZONTAL_ALIGN_FILL | GridData.FILL_BOTH);
			composite.setLayoutData(gridData );
			
			GridLayout layout = new GridLayout();
			layout.numColumns = 3;
			layout.marginWidth = 10;
			layout.marginHeight = 10;
			composite.setLayout (layout);	
			
			createCombo(composite);
			createTable(composite);			
			createLabel(composite);
			createTable2(composite);
			createButtons(composite);
			createTableViewer();
			tableViewer.setContentProvider(new ViewContentProvider()); 
			tableViewer.setLabelProvider(new ViewLabelProvider());
			tableViewer.setInput(chars);

	 }
	 
	 public	 class ViewContentProvider implements IStructuredContentProvider, IModelViewer {
			public void inputChanged(Viewer v, Object oldInput, Object newInput) {
				if (newInput != null)
					((UnicodeModel) newInput).addChangeListener(this);
				if (oldInput != null)
					((UnicodeModel) oldInput).removeChangeListener(this);
			}
			public void dispose() {
			}
			public Object[] getElements(Object parent) {
				return  chars.getChars().toArray();//return content of ONE ROW in the table
			}
			public void updateModel(int start,int end){
				chars = new UnicodeModel(start,end);
				
				tableViewer.setInput(chars);			
			}
			public void updateScript(String name, String scriptorblock){
				chars = new UnicodeModel(name, scriptorblock);
				tableViewer.setInput(chars);
			};
			public void updateFont(String fontName){
				
				preview.setFont(new Font(null,combo2.getText(), 50, 0));
				table.setFont(new Font(null,combo2.getText(), 15, 0));
				
//				table.setFont(new Font(null,combo2.getText(), 20, 0));
			}
		}
	public class ViewLabelProvider extends LabelProvider implements ITableLabelProvider {
			public String getColumnText(Object obj, int index) {				
				return getText(((String[])obj)[index]);
			}
			public Image getColumnImage(Object obj, int index) {
				return null;

			}

		}



	private void createTable(Composite parent){
		int style = SWT.SINGLE | SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL|
					SWT.FULL_SELECTION | SWT.HIDE_SELECTION;
		table = new Table(parent,style);
		table.setLinesVisible(true);
		table.setHeaderVisible(true);

		GridData gridData = new GridData ();
		gridData.horizontalAlignment = GridData.FILL;
		gridData.grabExcessVerticalSpace = true;
		gridData.widthHint = 50; //funktioniert????????
		gridData.horizontalSpan = 3;

		table.setLayoutData(gridData);		
		table.setFont(new Font(null,"Andagii", 15, 0));

		//set tableColumn
		for(int i = 0; i<columns; i++){
//		layout.addColumnData(new ColumnWeightData(10,true));
		TableColumn col = new TableColumn(table, SWT.LEFT, 0);		
		col.setWidth(40);
		}
	

		table.addListener(SWT.MouseDown, new Listener(){
			public void handleEvent (Event event) {
				Rectangle clientArea = table.getClientArea ();
				Point pt = new Point (event.x, event.y);
				int index = table.getTopIndex ();
				while (index < table.getItemCount ()) {
					boolean visible = false;
					final TableItem item = table.getItem (index);
					for (int i=0; i<table.getColumnCount (); i++) {
						Rectangle rect = item.getBounds (i);
						if (rect.contains (pt)) {
							char[] cs = item.getText(i).toCharArray();
							
							String s;
							String ss;
							int x = (int)item.getText(i).charAt(0);	
							if(cs.length==1){
															
								ss = Integer.toHexString(x)+"("+x+")";//+Character.UnicodeBlock.of(x);
								if(x<16)
									s = "U+000"+ss;								
								else if(x<256)
									s = "U+00"+ss;
								else if(x<256*16)
									s = "U+0"+ss;
								else 
									s = "U+"+ss;
							}else{
								cs[0] = item.getText(i).charAt(0);
								cs[1] = item.getText(i).charAt(1);
								s = "U+"+Integer.toHexString(Character.toCodePoint(cs[0], cs[1]))
								+"    High:"+Integer.toHexString((int)cs[0])
								+"   Low:"+Integer.toHexString((int)cs[1]);
							}
//							label.setText(s+"  script: "+UScript.getName(UScript.getScript(x))
							label.setText(s);
							String	character = newString(x);
							preview.setText(" "+character);
	
							return;
						}
						if (!visible && rect.intersects (clientArea)) {
							visible = true;
						}
					}
					if (!visible) return;
					index++;
				}
			}
		});
//		table.addMouseListener(new MouseAdapter(){
//	           public void MouseDown (MouseEvent e){
//	               if(e.button ==3)
//	           }    
//	       });

	
	}
	
	private void fillContextMenu(IMenuManager manager) {
		
	}
	private void createTable2(Composite parent){
		int style = SWT.SINGLE | SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL|
		SWT.FULL_SELECTION | SWT.HIDE_SELECTION;
		table2 = new Table(parent,style);
		table2.setLinesVisible(true);
//		table2.setHeaderVisible(true);
		
		GridData gridData = new GridData ();
		gridData.horizontalAlignment = GridData.FILL;
//		gridData.grabExcessVerticalSpace = true;
//		gridData.widthHint = 50; //funktioniert????????
		gridData.horizontalSpan = 3;
		gridData.heightHint = 26;
		TableItem item0 = new TableItem (table2, SWT.NONE);
		TableItem item1 = new TableItem (table2, SWT.NONE);
		for(int i = 0; i<columns; i++){
			TableColumn col = new TableColumn(table2, SWT.LEFT, 0);		
			col.setWidth(40);
//			item.setText();
			}

		table2.setLayoutData(gridData);		
		table2.setFont(new Font(null,"Andagii", 15, 0));
		table2.addListener(SWT.MouseDown, new Listener(){
			public void handleEvent (Event event) {
				Rectangle clientArea = table2.getClientArea ();
				Point pt = new Point (event.x, event.y);
				int index = table2.getTopIndex ();
				while (index < table2.getItemCount ()) {
					boolean visible = false;
					final TableItem item = table2.getItem (index);
					for (int i=0; i<table2.getColumnCount (); i++) {
						Rectangle rect = item.getBounds (i);
						if (rect.contains (pt)) {
							preview.setText(" "+item.getText(i));
	
							return;
						}
						if (!visible && rect.intersects (clientArea)) {
							visible = true;
						}
					}
					if (!visible) return;
					index++;
				}
			}
		});
	}
	private void createTableViewer(){
		tableViewer = new TableViewer(table);
	}	
	
	

	public void selectionChanged(IWorkbenchPart part, ISelection selection) {
		if (selection instanceof IStructuredSelection) {
//			Object first = ((IStructuredSelection)selection).getFirstElement();
			
		}
	}



	static String newString(int codePoint) {
	    if (Character.charCount(codePoint) == 1) {
	        return String.valueOf((char)codePoint);
	    } else {
	        return new String(Character.toChars(codePoint));
	    }
	}


	/**
	 * Passing the focus request to the viewer's control.
	 */
	public void setFocus() {
		tableViewer.getControl().setFocus();
	}

	public Table getTable(){
		return table;
	}
	public Table getTable2(){
		return table2;
	}
	/**
	 * Return the parent composite
	 */
	public Control getControl() {
		return table.getParent();
	}
	

	/**
	 * Add the "Add", "Delete" and "Close" buttons
	 * @param parent the parent composite
	 */
	private void createButtons(Composite parent) {
			
		//	Create and configure the "Close" button
		closeButton = new Button(parent, SWT.PUSH);
		closeButton.setText("Close");
		GridData gridData = new GridData (GridData.HORIZONTAL_ALIGN_BEGINNING);
		gridData.widthHint = 80; 
		closeButton.setLayoutData(gridData); 		
	}
	/**
	 * @param parent
	 */
	private void createCombo(Composite parent){

		blockLabel = new Label(parent, SWT.NONE);
		blockLabel.setText("choose Block");
		scriptLabel = new Label(parent, SWT.WRAP);
		scriptLabel.setText("choose Script");
		fontLabel = new Label(parent, SWT.WRAP);
		fontLabel.setText("choose Font");
		
		combo1 = new Combo(parent, SWT.NONE | SWT.CENTER);
		GridData gridData = new GridData (GridData.HORIZONTAL_ALIGN_BEGINNING);
		gridData.widthHint = 100;
		combo1.setLayoutData(gridData);
		
		String[] blocks = new String[154];
		for(int i  = 0; i < 154 ; i++ ){
				blocks[i] = UCharacter.UnicodeBlock.getInstance(i+1).toString();
			
		}
		
		 java.util.Arrays.sort(blocks);
		combo1.setItems(blocks);
//		
		combo1.addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent e){
				
				chars.updateModel(combo1.getText(), "block");
				combo3.deselectAll();				
			}
		});
		

		combo3 = new Combo(parent, SWT.NONE | SWT.CENTER);
		combo3.setLayoutData(gridData);	

		String[] scripts = new String[UScript.CODE_LIMIT]; 
		for(int i=0; i< UScript.CODE_LIMIT; i++){
				scripts[i] = UScript.getName(i);
			
		}
		combo3.setText("Latin");  //?????
		java.util.Arrays.sort(scripts);
		combo3.setItems(scripts);
		combo3.addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent e){
				
				chars.updateModel(combo3.getText(), "script");
				combo1.deselectAll();
			}
		});
		/*
		 * combo2 : fonts
		 */		

		combo2 = new Combo(parent, SWT.NONE | SWT.CENTER);
		combo2.setLayoutData(gridData);	

	    combo2.setText("Andagii");
	    String[] ss = chars.getFonts();
	    java.util.Arrays.sort(ss);
		combo2.setItems(ss);
		combo2.addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent e){
				chars.updateFont(combo2.getText());				
			}
		});
	}

	/**
	 * Return the 'close' Button
	 */
	private void createLabel(Composite parent){		
		preview = new Label(parent,SWT.WRAP);
//		label.setText("U+"+);
		GridData gridData1 = new GridData ();
		gridData1.horizontalAlignment = GridData.FILL;
		gridData1.horizontalSpan = 1;
		gridData1.heightHint =90;
//		gridData.verticalSpan = 2;
//		gridData.heightHint = 20;
		gridData1.widthHint = 50; // funktioniert nicht??
		preview.setLayoutData(gridData1);
//		preview.setSize(40, 40);		
		preview.setFont(new Font(null, "Andagii", 50, 0));
		
		label = new Label(parent,SWT.WRAP);//|SWT.BORDER
//		label.setText("U+"+);
		GridData gridData = new GridData ();
		gridData.horizontalAlignment = GridData.FILL;
		gridData.horizontalSpan = 1;
//		gridData.verticalSpan = 2;
//		gridData.heightHint = 20;
		//gridData.widthHint = 100;
		label.setLayoutData(gridData);
		
		hotKeysButton = new Button(parent, SWT.PUSH);
		hotKeysButton.setText("set it to HotKey");
		GridData gridDataButton = new GridData();
		gridDataButton.horizontalSpan = 1;
		hotKeysButton.setLayoutData(gridDataButton);	

		used = new Label(parent,SWT.WRAP);//|SWT.BORDER
		GridData gridData2 = new GridData ();
		gridData2.horizontalAlignment = GridData.FILL;
		gridData2.horizontalSpan = 3;
		used.setLayoutData(gridData2);
		used.setText("last used Characters:");

	}

	private void hookContextMenu() {
		MenuManager menuMgr = new MenuManager("#PopupMenu");
		menuMgr.setRemoveAllWhenShown(true);
		menuMgr.addMenuListener(new IMenuListener() {
			public void menuAboutToShow(IMenuManager manager) {
//				UnicodeView.this.fillContextMenu(manager);
			}
		});
		Menu menu = menuMgr.createContextMenu(this.getControl());
		this.getControl().setMenu(menu);
//		getSite().registerContextMenu(menuMgr, viewer);
	}

	public Button getCloseButton() {
		return closeButton;
	}
	public Button getOkButton(){
		return okButton;
	}
	/*
	 * Close the window and dispose of resources
	 */
	public void close() {
		Shell shell = table.getShell();

		if (shell != null && !shell.isDisposed())
			shell.dispose();
	}	

	/**
	 * Release resources
	 */
	public void dispose() {
		
		// Tell the label provider to release its ressources
		tableViewer.getLabelProvider().dispose();
	}
	
	public String[] scripts(){
		String[] scripts = new String[UScript.CODE_LIMIT];
		for(int i = 0; i<UScript.CODE_LIMIT; i++){
			scripts[i] = UScript.getName(i);
		}
		return scripts;
	}
	


	public static void main (String[] args) {
		
		
		Shell shell = new Shell();
		shell.setText("unicode View");

		// Set layout for shell
		GridLayout layout = new GridLayout();
		shell.setLayout(layout);
		
		// Create a composite to hold the children
		Composite composite = new Composite(shell, SWT.NONE);
		final UnicodeViewer unicodeViewer = new UnicodeViewer(composite);
		

		unicodeViewer.getControl().addDisposeListener(new DisposeListener() {

			public void widgetDisposed(DisposeEvent e) {
				unicodeViewer.dispose();			
			}
			
		});

		// Ask the shell to display its content
		shell.open();
		unicodeViewer.run(shell);
		}

	/**
	 * Run and wait for a close event
	 * @param shell Instance of Shell
	 */
	private void run(Shell shell) {

		
		
		// Add a listener for the close button
		closeButton.addSelectionListener(new SelectionAdapter() {
       	
			// Close the view i.e. dispose of the composite's parent
			public void widgetSelected(SelectionEvent e) {
				table.getParent().getParent().dispose();
			}
		});
		
		Display display = shell.getDisplay();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}
	}
//	public Text getUsedChars() {
//		return usedChars;
//	}
	public void setUsedChars(ArrayList<String> list) {
		if(list.size()<=8){
			for(int m = 0; m<list.size(); m++){
				table2.getItem(0).setText(m,list.get(list.size()-1-m));	
			}					
		}
		else if(list.size()<=16){
			for(int m = 0 ; m<list.size(); m++)
				if(m<8)
					table2.getItem(0).setText(m, list.get(list.size()-1-m));
				else
					table2.getItem(1).setText(m-8,list.get(list.size()-1-m));
//			System.out.println(list.size());
		}
	}
	public Label getPreview() {
		return preview;
	}
	public String getKeySequence() {
		return keySequence;
	}
	public Button getHotKeysButton() {
		return hotKeysButton;
	}
}

 /**
 * @author amy
 *
 */
class LengthValidator implements IInputValidator {
	  /**
	   * Validates the String. Returns null for no error, or an error message
	   * 
	   * @param newText the String to validate
	   * @return String
	   */
		private String keyStroke;
		/*TODO*/
		public String isValid(String keySequence) {
			if(keySequence.contains("Space")||
					keySequence.equalsIgnoreCase("Backspace"))
				return "not allowed!";
	    // Input must be OK
	    return null;
	  }
}