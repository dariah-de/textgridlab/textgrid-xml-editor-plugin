<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">

<xsl:template match="text()
    [following-sibling::c[@type='$,' or @type='$.' or type='$)']]
    [matches(., '\s+$')]">
    <xsl:value-of select="replace(., '\s+$', '')"/>
</xsl:template>

    <xsl:template match="text()
        [preceding-sibling::c[@type='$(']]
        [matches(., '^\s+')]">
    </xsl:template>
        
<xsl:template match="*">
    <xsl:element name="{local-name()}" namespace="http://www.tei-c.org/ns/1.0">
        <xsl:copy-of select="@*"/>
        <xsl:apply-templates/>
    </xsl:element>
</xsl:template>
    
<xsl:template match="comment()|processing-instruction()">
    <xsl:copy/>
</xsl:template>

</xsl:stylesheet>