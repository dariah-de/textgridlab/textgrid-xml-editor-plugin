package info.textgrid.lab.xmleditor.vex.toolkit.tests;

import org.eclipse.ui.plugin.AbstractUIPlugin;

public class TestPlugin extends AbstractUIPlugin {
	
	private static TestPlugin defaultInstance = null; 

	public TestPlugin() {
		super();
		defaultInstance = this;
	}
	
	public static TestPlugin getDefault() {
		return defaultInstance;
	}

}
