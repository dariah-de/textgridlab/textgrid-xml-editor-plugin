/**
 * LinkedDocumentReaderTest.java, part of project info.textgrid.lab.xmleditor.vex.toolkit.tests
 * 
 * (C) 2008 by Thorsten Vitt for TextGrid
 */
package net.sf.vex.toolkit.dom.linked.tests;

import static org.junit.Assert.fail;
import info.textgrid.lab.xmleditor.mpeditor.MPXmlEditorPart;
import info.textgrid.lab.xmleditor.vex.toolkit.tests.TestPlugin;

import java.io.IOException;
import java.io.InputStream;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.FileEditorInput;
import org.junit.Test;

/**
 * @author Thorsten Vitt <vitt@linglit.tu-darmstadt.de> (tv) for TextGrid
 * 
 */
public class LinkedDocumentReaderTest {

	/**
	 * Returns the example {@link IFile}, creating it from the enclosed
	 * resources if neccessary.
	 * 
	 * @return
	 * @throws CoreException
	 * @throws IOException
	 */
	public IFile getExampleFile() throws CoreException, IOException {

		IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
		IFile werther = root.getFileForLocation(new Path("/test/werther.xml"));

		if (werther == null || !werther.exists()) {
			IProject project = root.getProject("test");

			if (!project.exists()) 
				project.create(null);
			if (!project.isOpen())
				project.open(null);

			InputStream inStream = FileLocator.openStream(TestPlugin
					.getDefault().getBundle(),
					new Path("resources/werther.xml"), false);

			werther = root.getFileForLocation(new Path("/test/werther.xml"));
			if (werther == null) {
				werther = project.getFile("werther.xml");
				werther.create(inStream, true, null);
			} else
				werther.setContents(inStream, true, false, null);
			inStream.close();

		}

		return werther;
	}

	/**
	 * Test method for
	 * {@link net.sf.vex.dom.linked.LinkedDocumentReader#read(org.eclipse.wst.xml.core.internal.provisional.document.IDOMModel)}
	 * .
	 * 
	 * @throws IOException
	 * @throws CoreException
	 * @throws PartInitException
	 * @throws InterruptedException
	 */
	@SuppressWarnings("restriction")
	@Test
	public void testReadIDOMModel() throws PartInitException, CoreException,
			IOException, InterruptedException {

		IWorkbenchPage page = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getActivePage();
		
		IEditorPart editor = page.openEditor(new FileEditorInput(
				getExampleFile()),
				"info.textgrid.lab.xmleditor.mpeditor.MPEditorPart");
		MPXmlEditorPart mpEditor = (MPXmlEditorPart) editor;
		mpEditor.setActivePage(mpEditor.getWysiwymEditorIndex());

		fail("Not yet implemented"); // TODO
	}

}
