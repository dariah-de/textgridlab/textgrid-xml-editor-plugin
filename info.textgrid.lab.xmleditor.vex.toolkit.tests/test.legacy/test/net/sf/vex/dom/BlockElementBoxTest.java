/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2004 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package test.net.sf.vex.dom;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import junit.framework.TestCase;
import net.sf.vex.core.Graphics;
import net.sf.vex.css.StyleSheet;
import net.sf.vex.css.StyleSheetReader;
import net.sf.vex.dom.DocumentReader;
import net.sf.vex.dom.IVexDocument;
import net.sf.vex.layout.BlockElementBox;
import net.sf.vex.layout.Box;
import net.sf.vex.layout.CssBoxFactory;
import net.sf.vex.layout.LayoutContext;
import test.net.sf.vex.layout.FakeGraphics;

public class BlockElementBoxTest extends TestCase {
    
    private Graphics g;
    private LayoutContext context;
    
    public BlockElementBoxTest() throws Exception {
        
        StyleSheetReader ssReader = new StyleSheetReader();
        StyleSheet ss = ssReader.read(this.getClass().getResource("test.css"));

        this.g = new FakeGraphics();
        
        this.context = new LayoutContext();
        this.context.setBoxFactory(new CssBoxFactory());
        this.context.setGraphics(this.g);
        this.context.setStyleSheet(ss);
    
    }
    
    public void testPositioning() throws Exception {
        
        String docString = "<root><small/><medium/><large/></root>";
        DocumentReader docReader = new DocumentReader();
        docReader.setDebugging(true);
        IVexDocument doc = docReader.read(docString);
        BlockElementBox box = new BlockElementBox(context, null, doc.getRootElement());
        
        Method createChildren = BlockElementBox.class.getDeclaredMethod("createChildren", new Class[] { LayoutContext.class });
        createChildren.setAccessible(true);
        createChildren.invoke(box, new Object[] { this.context });
        
        Box[] children = box.getChildren();
        assertEquals(3, children.length);
        assertEquals(1 + 10, this.getGap(box, 0));
        assertEquals(30 + 3 + 300 + 2 + 20, this.getGap(box, 1));
        assertEquals(60 + 6 + 600 + 3 + 30, this.getGap(box, 2));
        assertEquals(90 + 9, this.getGap(box, 3));
    }
    
    public int getGap(BlockElementBox box, int n) throws SecurityException, NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException {
        Method getGap = BlockElementBox.class.getDeclaredMethod("getGap", new Class[] { Integer.TYPE });
        getGap.setAccessible(true);
        return ((Integer) getGap.invoke(box, new Object[] { new Integer(n) })).intValue();
    }
}
