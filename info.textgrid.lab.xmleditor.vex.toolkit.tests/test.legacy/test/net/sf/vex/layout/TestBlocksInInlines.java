/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2005 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package test.net.sf.vex.layout;

import java.net.URL;

import junit.framework.TestCase;
import net.sf.vex.css.StyleSheet;
import net.sf.vex.css.StyleSheetReader;
import net.sf.vex.dom.IVexDocument;
import net.sf.vex.dom.impl.Document;
import net.sf.vex.dom.impl.Element;
import net.sf.vex.dom.impl.RootElement;
import net.sf.vex.layout.LayoutContext;
import net.sf.vex.layout.RootBox;

/**
 * Tests proper function of a block-level element within an inline element.
 * These must be layed out as a block child of the containing block element.
 */
public class TestBlocksInInlines extends TestCase {

    FakeGraphics g;
    LayoutContext context;

    public TestBlocksInInlines() throws Exception {
        URL url = this.getClass().getResource("test.css");
        StyleSheetReader reader = new StyleSheetReader();
        StyleSheet ss = reader.read(url);
        
        this.g = new FakeGraphics();
        
        this.context = new LayoutContext();
        this.context.setBoxFactory(new TestBoxFactory());
        this.context.setGraphics(this.g);
        this.context.setStyleSheet(ss);
    }

    /*FIXME this test is broken
    public void testBlockInInline() throws Exception {
        RootElement root = new RootElement("root");
        IVexDocument doc = new Document(root);

        doc.insertText(1, "one  five");
        doc.insertElement(5, new Element("b"));
        doc.insertText(6, "two  four");
        doc.insertElement(10, new Element("p"));
        doc.insertText(11, "three");
        
        RootBox rootBox = new RootBox(this.context, root, 500);
        rootBox.layout(this.context, 0, Integer.MAX_VALUE);

        
    }
    */
}
