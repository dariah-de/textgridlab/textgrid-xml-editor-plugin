/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2004 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package test.net.sf.vex.layout;

import java.net.URL;
import junit.framework.TestCase;
import net.sf.vex.css.StyleSheet;
import net.sf.vex.css.StyleSheetReader;
import net.sf.vex.dom.IVexDocument;
import net.sf.vex.dom.impl.Document;
import net.sf.vex.dom.impl.Element;
import net.sf.vex.dom.impl.RootElement;
import net.sf.vex.layout.BlockElementBox;
import net.sf.vex.layout.BlockPseudoElementBox;
import net.sf.vex.layout.Box;
import net.sf.vex.layout.LayoutContext;
import net.sf.vex.layout.RootBox;


public class TestBlockElementBox extends TestCase {
    
    FakeGraphics g;
    LayoutContext context;

    public TestBlockElementBox() throws Exception {
        URL url = this.getClass().getResource("test.css");
        StyleSheetReader reader = new StyleSheetReader();
        StyleSheet ss = reader.read(url);
        
        this.g = new FakeGraphics();
        
        this.context = new LayoutContext();
        this.context.setBoxFactory(new TestBoxFactory());
        this.context.setGraphics(this.g);
        this.context.setStyleSheet(ss);
    }

    /* FIXME this test is broken
     
    public void testBeforeAfter() throws Exception {
        RootElement root = new RootElement("root");
        IVexDocument doc = new Document(root);
        doc.insertElement(1, new Element("beforeBlock"));
        
        RootBox rootBox = new RootBox(this.context, root, 500);
        rootBox.layout(this.context, 0, Integer.MAX_VALUE);

        Box[] children;
        BlockElementBox beb;
        
        children = rootBox.getChildren();
        assertEquals(1, children.length);
        assertEquals(BlockElementBox.class, children[0].getClass());
        beb = (BlockElementBox) children[0];
        assertEquals(root, beb.getElement());
        
        children = beb.getChildren();
        assertEquals(2, children.length);
        assertEquals(BlockPseudoElementBox.class, children[0].getClass());
        assertEquals(BlockElementBox.class, children[1].getClass());
        beb = (BlockElementBox) children[1];
        assertEquals("beforeBlock", beb.getElement().getName());
        
    }
    */
}
