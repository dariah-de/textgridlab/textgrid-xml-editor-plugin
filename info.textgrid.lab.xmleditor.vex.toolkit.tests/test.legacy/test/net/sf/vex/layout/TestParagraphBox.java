/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2004 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package test.net.sf.vex.layout;

import java.net.URL;

import junit.framework.TestCase;
import net.sf.vex.css.StyleSheet;
import net.sf.vex.css.StyleSheetReader;
import net.sf.vex.layout.CssBoxFactory;
import net.sf.vex.layout.LayoutContext;

public class TestParagraphBox extends TestCase {

    FakeGraphics g;
    LayoutContext context;
    
    public TestParagraphBox() throws Exception {
        
        URL url = this.getClass().getResource("test.css");
        StyleSheetReader reader = new StyleSheetReader();
        StyleSheet ss = reader.read(url);
        
        this.g = new FakeGraphics();
        
        this.context = new LayoutContext();
        this.context.setBoxFactory(new CssBoxFactory());
        this.context.setGraphics(this.g);
        this.context.setStyleSheet(ss);
    }

    /*
    public void testWordWrap() throws Exception {
        RootElement root = new RootElement("root");
        Document doc = new Document(root);
        
        Styles styles = this.context.getStyleSheet().getStyles(root);
        
        FontMetrics fm = this.g.getFontMetrics();
        
        // Test Case 1: check the offsets 
        //
        // UPPER CASE indicates static text
        // lower case indicates document text
        // [ ] represent element start and end         
        //
        // BLACK WHITE GRAY
        // RED [orange] YELLOW   (line is 1:8, last=false)
        // BLACK WHITE GRAY
        // [blue] GREEN [pink]     (line is 9:20 last=true)
        // BLACK WHITE GRAY
        //
        // Document looks like this (# chars are element sentinels
        //    2     8      16  20
        //   /     /       /   /    
        // ##orange##blue##pink##
        //           \   \ 
        //            10  14
        //
        
    }
    */
}


