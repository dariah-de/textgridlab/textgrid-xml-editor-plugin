/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2004 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package test.net.sf.vex.layout;

import net.sf.vex.dom.IVexElement;
import net.sf.vex.layout.BlockElementBox;
import net.sf.vex.layout.Box;
import net.sf.vex.layout.CssBoxFactory;
import net.sf.vex.layout.LayoutContext;
import net.sf.vex.layout.SpaceBox;


/**
 * A box factory that, for an element named &lt;space&gt;, returns a SpaceBox
 * with height and width given by attributes of those names, e.g.
 * &lt;space height="100" width="200"/&gt;
 */
public class TestBoxFactory extends CssBoxFactory {
    
    public Box createBox(LayoutContext context, IVexElement element,
            BlockElementBox parent, int width) {
        
        if (element.getName().equals("space")) {
            int w = 0;
            int h = 0;
            try {
                w = Integer.parseInt(element.getAttribute("width"));
            } catch (NumberFormatException ex) {
            }
            try {
                h = Integer.parseInt(element.getAttribute("height"));
            } catch (NumberFormatException ex) {
            }
            return new SpaceBox(w, h);
        }
        // TODO Auto-generated method stub
        return super.createBox(context, element, parent, width);
    }
}
