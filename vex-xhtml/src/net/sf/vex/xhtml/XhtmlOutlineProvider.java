/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2004 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.sf.vex.xhtml;

import java.util.ArrayList;
import java.util.List;

import net.sf.vex.dom.IVexDocument;
import net.sf.vex.dom.IVexElement;
import net.sf.vex.editor.IOutlineProvider;
import net.sf.vex.editor.VexEditor;

import org.eclipse.jface.viewers.IBaseLabelProvider;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.Viewer;

/**
 * Provides an outline of the sections of an XHTML document.
 */
public class XhtmlOutlineProvider implements IOutlineProvider {

    public void init(VexEditor editor) {
    }

    public ITreeContentProvider getContentProvider() {
        return this.contentProvider;
    }

    public IBaseLabelProvider getLabelProvider() {
        return this.labelProvider;
    }

    public IVexElement getOutlineElement(IVexElement child) {
        IVexElement element = child;
        while (element.getParent() != null) {
            
            // TODO: compare to all structural element names
            
            String name = element.getName();
            if (name.equals("h1") ||
                    name.equals("h2") ||
                    name.equals("h3") ||
                    name.equals("h4") ||
                    name.equals("h5") ||
                    name.equals("h6")) {
                return element;
            }
            
            element = element.getParent();
        }
        return element;
    }
    
    //===================================================== PRIVATE
    
    private ITreeContentProvider contentProvider = new ITreeContentProvider() {

        public void dispose() {
        }

        public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
        }

        public Object[] getChildren(Object parentElement) {
            return getOutlineChildren((IVexElement) parentElement);
        }

        public Object getParent(Object element) {
            IVexElement parent = ((IVexElement) element).getParent();
            if (parent == null) {
                return element;
            } else {
                return getOutlineElement(parent);
            }
        }

        public boolean hasChildren(Object element) {
            return getOutlineChildren((IVexElement) element).length > 0;
        }

        public Object[] getElements(Object inputElement) {
            IVexDocument document = (IVexDocument) inputElement;
            return new Object[] { document.getRootElement() };
        }
        
    };
    
    /**
     * Returns an array of the children of the given element that represent
     * nodes in the outline. These are structural elements such as "section".
     * @param element
     * @return
     */
    private IVexElement[] getOutlineChildren(IVexElement element) {
        List children = new ArrayList();
        if (element.getParent() == null) {
            IVexElement body = findChild(element, "body");
            if (body != null) {
                IVexElement[] childElements = body.getChildElements();
                
                // First, find the lowest numbered h tag available
                String lowH = "h6";
                for (int i = 0; i < childElements.length; i++) {
                    IVexElement child = childElements[i];
                    if (isHTag(child) && child.getName().compareTo(lowH) < 0) {
                        lowH = child.getName();
                    }
                }
                
                // Now, get all body children at that level
                for (int i = 0; i < childElements.length; i++) {
                    IVexElement child = childElements[i];
                    if (child.getName().equals(lowH)) {
                        children.add(child);
                    }
                }
            }
        } else {
            if (isHTag(element)) {
                // get siblings with the next lower number
                // between this element and the next element at the same level
                int level = Integer.parseInt(element.getName().substring(1));
                String childName = "h" + (level+1);
                IVexElement[] childElements = element.getParent().getChildElements();
                boolean foundSelf = false;
                for (int i = 0; i < childElements.length; i++) {
                    IVexElement child = childElements[i];
                    if (child == element) {
                        foundSelf = true;
                    } else if (!foundSelf) {
                        continue;
                    } else if (child.getName().equals(childName)) {
                        children.add(child);
                    } else if (child.getName().equals(element.getName())) {
                        // terminate at next sibling at same level
                        break;
                    }
                }
            }
        }
        return (IVexElement[]) children.toArray(new IVexElement[children.size()]);
    }

    
    private ILabelProvider labelProvider = new LabelProvider() {
        public String getText(Object o) {
            String text;
            IVexElement element = (IVexElement) o;
            if (element.getParent() == null) {
                text = "html";
                IVexElement head = findChild(element, "head");
                if (head != null) {
                    IVexElement title = findChild(head, "title");
                    if (title != null) {
                        text = title.getText();
                    }
                }
            } else {
                text = element.getText();
            }
            return text;
        }
    };
    
    private IVexElement findChild(IVexElement parent, String childName) {
        IVexElement[] children = parent.getChildElements();
        for (int i = 0; i < children.length; i++) {
            IVexElement child = children[i];
            if (child.getName().equals(childName)) {
                return child;
            }
        }
        return null;
    }
    
    private boolean isHTag(IVexElement element) {
        String name = element.getName();
        return name.equals("h1") ||
                name.equals("h2") ||
                name.equals("h3") ||
                name.equals("h4") ||
                name.equals("h5") ||
                name.equals("h6");
    }

}
