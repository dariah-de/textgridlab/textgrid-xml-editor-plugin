/* 
 * VEX, a visual editor for XML
 *
 * Copyright (c) 2004 John Krasnay
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package test.net.sf.vex.eclipse;

import java.util.Collection;

import junit.framework.TestCase;
import net.sf.vex.editor.Association;

public class AssociationTest extends TestCase {

    public void testAddRemove() throws Exception {
        
        Association assoc = new Association();
        
        Collection c;
        
        c = assoc.getRightsForLeft("l0");
        assertNotNull(c);
        assertEquals(0, c.size());
        
        c = assoc.getLeftsForRight("r0");
        assertNotNull(c);
        assertEquals(0, c.size());
        
        assoc.add("l0", "r0");
        
        c = assoc.getRightsForLeft("l0");
        assertNotNull(c);
        assertEquals(1, c.size());
        assertEquals("r0", c.toArray()[0]);
        
        c = assoc.getLeftsForRight("r0");
        assertNotNull(c);
        assertEquals(1, c.size());
        assertEquals("l0", c.toArray()[0]);
        
        assoc.add("l0", "r1");
        assoc.add("l1", "r0");
        assoc.add("l1", "r1");
        
        c = assoc.getRightsForLeft("l0");
        assertNotNull(c);
        assertEquals(2, c.size());
        assertTrue(c.contains("r0"));
        assertTrue(c.contains("r1"));
        
        c = assoc.getRightsForLeft("l1");
        assertNotNull(c);
        assertEquals(2, c.size());
        assertTrue(c.contains("r0"));
        assertTrue(c.contains("r1"));
        
        c = assoc.getLeftsForRight("r0");
        assertNotNull(c);
        assertEquals(2, c.size());
        assertTrue(c.contains("l0"));
        assertTrue(c.contains("l1"));
        
        c = assoc.getLeftsForRight("r1");
        assertNotNull(c);
        assertEquals(2, c.size());
        assertTrue(c.contains("l0"));
        assertTrue(c.contains("l1"));
        
        assoc.remove("l0", "r0");

        c = assoc.getRightsForLeft("l0");
        assertNotNull(c);
        assertEquals(1, c.size());
        assertTrue(c.contains("r1"));
        
        c = assoc.getRightsForLeft("l1");
        assertNotNull(c);
        assertEquals(2, c.size());
        assertTrue(c.contains("r0"));
        assertTrue(c.contains("r1"));
        
        c = assoc.getLeftsForRight("r0");
        assertNotNull(c);
        assertEquals(1, c.size());
        assertTrue(c.contains("l1"));
        
        c = assoc.getLeftsForRight("r1");
        assertNotNull(c);
        assertEquals(2, c.size());
        assertTrue(c.contains("l0"));
        assertTrue(c.contains("l1"));
        
        assoc.remove("l0", "r1");
        
        c = assoc.getRightsForLeft("l0");
        assertNotNull(c);
        assertEquals(0, c.size());
        
        c = assoc.getRightsForLeft("l1");
        assertNotNull(c);
        assertEquals(2, c.size());
        assertTrue(c.contains("r0"));
        assertTrue(c.contains("r1"));
        
        c = assoc.getLeftsForRight("r0");
        assertNotNull(c);
        assertEquals(1, c.size());
        assertTrue(c.contains("l1"));
        
        c = assoc.getLeftsForRight("r1");
        assertNotNull(c);
        assertEquals(1, c.size());
        assertTrue(c.contains("l1"));
        
        assoc.remove("l1", "r0");
        
        c = assoc.getRightsForLeft("l0");
        assertNotNull(c);
        assertEquals(0, c.size());
        
        c = assoc.getRightsForLeft("l1");
        assertNotNull(c);
        assertEquals(1, c.size());
        assertTrue(c.contains("r1"));
        
        c = assoc.getLeftsForRight("r0");
        assertNotNull(c);
        assertEquals(0, c.size());
        
        c = assoc.getLeftsForRight("r1");
        assertNotNull(c);
        assertEquals(1, c.size());
        assertTrue(c.contains("l1"));
        
        assoc.remove("l1", "r1");
        
        c = assoc.getRightsForLeft("l0");
        assertNotNull(c);
        assertEquals(0, c.size());
        
        c = assoc.getRightsForLeft("l1");
        assertNotNull(c);
        assertEquals(0, c.size());
        
        c = assoc.getLeftsForRight("r0");
        assertNotNull(c);
        assertEquals(0, c.size());
        
        c = assoc.getLeftsForRight("r1");
        assertNotNull(c);
        assertEquals(0, c.size());
    }
    
    public void testRemoveLeft() throws Exception {

        Association assoc = new Association();
        
        Collection c;
        
        assoc.add("l0", "r0");
        assoc.add("l0", "r1");
        assoc.add("l1", "r0");
        assoc.add("l1", "r1");

        
        // These should be no-ops
        assoc.removeLeft("r0");
        assoc.removeLeft("r1");
        assoc.removeRight("l0");
        assoc.removeRight("l1");
        
        c = assoc.getRightsForLeft("l0");
        assertNotNull(c);
        assertEquals(2, c.size());
        assertTrue(c.contains("r0"));
        assertTrue(c.contains("r1"));
        
        c = assoc.getRightsForLeft("l1");
        assertNotNull(c);
        assertEquals(2, c.size());
        assertTrue(c.contains("r0"));
        assertTrue(c.contains("r1"));
        
        c = assoc.getLeftsForRight("r0");
        assertNotNull(c);
        assertEquals(2, c.size());
        assertTrue(c.contains("l0"));
        assertTrue(c.contains("l1"));
        
        c = assoc.getLeftsForRight("r1");
        assertNotNull(c);
        assertEquals(2, c.size());
        assertTrue(c.contains("l0"));
        assertTrue(c.contains("l1"));
        

        assoc.removeLeft("l0");
        
        c = assoc.getRightsForLeft("l0");
        assertNotNull(c);
        assertEquals(0, c.size());
        
        c = assoc.getRightsForLeft("l1");
        assertNotNull(c);
        assertEquals(2, c.size());
        assertTrue(c.contains("r0"));
        assertTrue(c.contains("r1"));
        
        c = assoc.getLeftsForRight("r0");
        assertNotNull(c);
        assertEquals(1, c.size());
        assertTrue(c.contains("l1"));
        
        c = assoc.getLeftsForRight("r1");
        assertNotNull(c);
        assertEquals(1, c.size());
        assertTrue(c.contains("l1"));
        
        
        assoc.removeLeft("l1");

        c = assoc.getRightsForLeft("l0");
        assertNotNull(c);
        assertEquals(0, c.size());
        
        c = assoc.getRightsForLeft("l1");
        assertNotNull(c);
        assertEquals(0, c.size());
        
        c = assoc.getLeftsForRight("r0");
        assertNotNull(c);
        assertEquals(0, c.size());
        
        c = assoc.getLeftsForRight("r1");
        assertNotNull(c);
        assertEquals(0, c.size());
    }


    public void testRemoveRight() throws Exception {

        Association assoc = new Association();
        
        Collection c;
        
        assoc.add("l0", "r0");
        assoc.add("l0", "r1");
        assoc.add("l1", "r0");
        assoc.add("l1", "r1");

        assoc.removeRight("r0");
        
        c = assoc.getRightsForLeft("l0");
        assertNotNull(c);
        assertEquals(1, c.size());
        assertTrue(c.contains("r1"));
        
        c = assoc.getRightsForLeft("l1");
        assertNotNull(c);
        assertEquals(1, c.size());
        assertTrue(c.contains("r1"));
        
        c = assoc.getLeftsForRight("r0");
        assertNotNull(c);
        assertEquals(0, c.size());
        
        c = assoc.getLeftsForRight("r1");
        assertNotNull(c);
        assertEquals(2, c.size());
        assertTrue(c.contains("l0"));
        assertTrue(c.contains("l1"));
        
        
        assoc.removeRight("r1");

        c = assoc.getRightsForLeft("l0");
        assertNotNull(c);
        assertEquals(0, c.size());
        
        c = assoc.getRightsForLeft("l1");
        assertNotNull(c);
        assertEquals(0, c.size());
        
        c = assoc.getLeftsForRight("r0");
        assertNotNull(c);
        assertEquals(0, c.size());
        
        c = assoc.getLeftsForRight("r1");
        assertNotNull(c);
        assertEquals(0, c.size());
    }
}
