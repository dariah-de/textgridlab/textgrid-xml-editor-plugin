package info.textgrid.lab.xmleditor.relaxng.provisional;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "info.textgrid.lab.xmleditor.relaxng.provisional.messages"; //$NON-NLS-1$
	public static String ImportRNGConfigurator_Convert;
	public static String ImportRNGConfigurator_Failed_to_convert;
	public static String ImportRNGConfigurator_Importing_X;
	public static String ImportRNGConfigurator_Leave_as_is;
	public static String ImportRNGConfigurator_RNG_Not_Supported;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
