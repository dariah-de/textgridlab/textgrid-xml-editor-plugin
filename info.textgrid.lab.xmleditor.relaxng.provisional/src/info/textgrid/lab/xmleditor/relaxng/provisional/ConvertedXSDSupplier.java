package info.textgrid.lab.xmleditor.relaxng.provisional;

import info.textgrid.lab.core.importexport.model.ISpecialImportEntrySupplier;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

class ConvertedXSDSupplier implements ISpecialImportEntrySupplier {

	private final File xsdFile;
	private final File relaxNGFile;

	public ConvertedXSDSupplier(final File relaxNGFile, final File xsdFile) {
		this.relaxNGFile = relaxNGFile;
		this.xsdFile = xsdFile;
	}

	@Override
	public InputStream getInputStream() throws IOException {
		return new FileInputStream(xsdFile);
	}

	@Override
	public File getFile() {
		return relaxNGFile;
	}

}
