package info.textgrid.lab.xmleditor.relaxng.provisional;

import info.textgrid._import.RewriteMethod;
import info.textgrid.lab.core.importexport.model.AbstractImportEntryConfigurator;
import info.textgrid.lab.core.importexport.model.IImportEntryConfigurator;
import info.textgrid.lab.core.importexport.model.ImportEntry;
import info.textgrid.lab.core.importexport.model.RewriteSetup;
import info.textgrid.lab.core.model.TGContentType;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FilenameUtils;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.statushandlers.StatusManager;

import com.thaiopensource.relaxng.translate.Driver;

/**
 * This import configurator uses Trang to convert Relax NG files to XML Schema
 * files on import if desired.
 */
public class ImportRNGConfigurator extends AbstractImportEntryConfigurator implements IImportEntryConfigurator {

	@Override
	public void configureImport(final ImportEntry entry, final IProgressMonitor monitor) {
		if (FilenameUtils.isExtension(entry.getLocalData(), new String[] { "rng", "rnc" })) { //$NON-NLS-1$ //$NON-NLS-2$
			if (ask(NLS.bind(Messages.ImportRNGConfigurator_Importing_X, entry.getLocalData()),
					NLS.bind(
							Messages.ImportRNGConfigurator_RNG_Not_Supported,
							entry.getLocalData()), 1, Messages.ImportRNGConfigurator_Leave_as_is, Messages.ImportRNGConfigurator_Convert) == 1) {
				File xsdFile;
				try {
					xsdFile = File.createTempFile("textgridlab", ".xsd"); //$NON-NLS-1$ //$NON-NLS-2$
					final Driver trang = new Driver();
					trang.run(new String[] { "-O", "xsd", entry.getLocalFile().getAbsolutePath(), xsdFile.getAbsolutePath() }); //$NON-NLS-1$ //$NON-NLS-2$
					xsdFile.deleteOnExit();

					entry.getObject().setContentType(TGContentType.of("text/xsd+xml")); //$NON-NLS-1$
					entry.setSupplier(new ConvertedXSDSupplier(entry.getLocalFile(), xsdFile));
					entry.setRewriteSetup(RewriteSetup.of(RewriteMethod.XML, "internal:schema#xsd"));

				} catch (final IOException e) {
					StatusManager.getManager().handle(
							new Status(IStatus.ERROR, RelaxNGPlugin.PLUGIN_ID, NLS.bind(Messages.ImportRNGConfigurator_Failed_to_convert,
									entry.getLocalData()), e));
				}

			} else {
				if (FilenameUtils.isExtension(entry.getLocalData(), "rnc")) //$NON-NLS-1$
					entry.getObject().setContentType(TGContentType.of("application/relax-ng-compact-syntax")); //$NON-NLS-1$
				else
					entry.getObject().setContentType(TGContentType.of("text/xml")); //$NON-NLS-1$
			}
		}
	}

}
