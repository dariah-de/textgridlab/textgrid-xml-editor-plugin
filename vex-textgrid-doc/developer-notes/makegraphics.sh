#!/bin/sh
NODEFONTNAME=Courier
NODEFONTSIZE=14
NODEFONTPACKAGESIZE=10
COMMONOPTIONS=""

javadoc $COMMONOPTIONS -qualify -postfixpackage -docletpath /opt/doclets/UMLGraph-4.5/lib/UmlGraph.jar -doclet gr.spinellis.umlgraph.doclet.UmlGraph -outputencoding utf8 -output vexwidget.dot -public -nodefontname $NODEFONTNAME -nodefontsize $NODEFONTSIZE -nodefontpackagesize $NODEFONTPACKAGESIZE  -inferrel  /mnt/data/d/vex-workspace/vex-editor/src/net/sf/vex/editor/VexEditor.java   /mnt/data/d/vex-workspace/vex-toolkit/src/net/sf/vex/widget/VexWidgetImpl.java /mnt/data/d/vex-workspace/vex-toolkit/src/net/sf/vex/widget/IVexWidget.java /mnt/data/d/vex-workspace/vex-toolkit/src/net/sf/vex/swt/VexWidget.java  \
&& dot -Tsvg -o vexwidget.svg vexwidget.dot \
\
&& javadoc $COMMONOPTIONS  -qualify -postfixpackage -docletpath /opt/doclets/UMLGraph-4.5/lib/UmlGraph.jar -doclet gr.spinellis.umlgraph.doclet.UmlGraph -outputencoding utf8 -output vexgraphik.dot -public -nodefontname $NODEFONTNAME -nodefontsize $NODEFONTSIZE -nodefontpackagesize $NODEFONTPACKAGESIZE   -inferrel  /mnt/data/d/vex-workspace/vex-toolkit/src/net/sf/vex/core/Graphics.java /mnt/data/d/vex-workspace/vex-toolkit/src/net/sf/vex/swt/SwtGraphics.java  /mnt/data/d/vex-workspace/vex-toolkit/src/net/sf/vex/swing/AwtGraphics.java  /mnt/data/d/vex-workspace/vex-toolkit/src/test/net/sf/vex/layout/FakeGraphics.java \
&& dot -Tsvg -o vexgraphik.svg vexgraphik.dot && \
\
javadoc $COMMONOPTIONS  -docletpath /opt/doclets/UMLGraph-4.5/lib/UmlGraph.jar -doclet gr.spinellis.umlgraph.doclet.UmlGraph -outputencoding utf8 -output vexboxhierarchy.dot -public -nodefontname $NODEFONTNAME -nodefontsize $NODEFONTSIZE -nodefontpackagesize $NODEFONTPACKAGESIZE  \
/mnt/data/d/vex-workspace/vex-toolkit/src/net/sf/vex/layout/Box.java \
/mnt/data/d/vex-workspace/vex-toolkit/src/net/sf/vex/layout/BlockBox.java \
/mnt/data/d/vex-workspace/vex-toolkit/src/net/sf/vex/layout/AbstractBox.java \
/mnt/data/d/vex-workspace/vex-toolkit/src/net/sf/vex/layout/InlineBox.java \
/mnt/data/d/vex-workspace/vex-toolkit/src/net/sf/vex/layout/RootBox.java \
/mnt/data/d/vex-workspace/vex-toolkit/src/net/sf/vex/layout/ParagraphBox.java \
/mnt/data/d/vex-workspace/vex-toolkit/src/net/sf/vex/layout/TextBox.java \
&& dot -Tsvg -o vexboxhierarchy.svg vexboxhierarchy.dot \
\
\
 && \
javadoc $COMMONOPTIONS -rotate=90 -docletpath /opt/doclets/UMLGraph-4.5/lib/UmlGraph.jar -doclet gr.spinellis.umlgraph.doclet.UmlGraph -outputencoding utf8 -output vexboxhierarchy_full.dot -public   -nodefontname $NODEFONTNAME -nodefontsize $NODEFONTSIZE -nodefontpackagesize $NODEFONTPACKAGESIZE \
/mnt/data/d/vex-workspace/vex-toolkit/src/net/sf/vex/layout/*Box*.java \
&& dot -Tsvg -o vexboxhierarchy_full.svg vexboxhierarchy_full.dot && \
\
\
javadoc $COMMONOPTIONS -inferrel -inferdep  -qualify -postfixpackage  -docletpath /opt/doclets/UMLGraph-4.5/lib/UmlGraph.jar -doclet gr.spinellis.umlgraph.doclet.UmlGraph -outputencoding utf8 -output vexactions.dot -public -nodefontname $NODEFONTNAME -nodefontsize $NODEFONTSIZE -nodefontpackagesize $NODEFONTPACKAGESIZE \
/mnt/data/d/vex-workspace/vex-toolkit/src/net/sf/vex/action/IVexAction.java \
/mnt/data/d/vex-workspace/vex-toolkit/src/net/sf/vex/action/AbstractVexAction.java \
/mnt/data/d/vex-workspace/vex-toolkit/src/net/sf/vex/action/DeleteRowAction.java \
/mnt/data/d/vex-workspace/vex-toolkit/src/net/sf/vex/action/PasteTextAction.java \
/mnt/data/d/vex-workspace/vex-toolkit/src/net/sf/vex/action/RemoveElementAction.java \
/mnt/data/d/vex-workspace/vex-editor/src/net/sf/vex/editor/VexActionDelegate.java \
/mnt/data/d/vex-workspace/vex-editor/src/net/sf/vex/editor/action/VexActionAdapter.java \
&& dot -Tsvg -o vexactions.svg vexactions.dot \
